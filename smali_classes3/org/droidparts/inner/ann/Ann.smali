.class public abstract Lorg/droidparts/inner/ann/Ann;
.super Ljava/lang/Object;
.source "Ann.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/lang/annotation/Annotation;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final ATTRIBUTE:Ljava/lang/String; = "attribute"

.field protected static final CLICK:Ljava/lang/String; = "click"

.field protected static final EAGER:Ljava/lang/String; = "eager"

.field protected static final ID:Ljava/lang/String; = "id"

.field protected static final KEY:Ljava/lang/String; = "key"

.field protected static final NAME:Ljava/lang/String; = "name"

.field protected static final NULLABLE:Ljava/lang/String; = "nullable"

.field protected static final OPTIONAL:Ljava/lang/String; = "optional"

.field protected static final TAG:Ljava/lang/String; = "tag"

.field protected static final UNIQUE:Ljava/lang/String; = "unique"

.field protected static final VALUE:Ljava/lang/String; = "value"

.field private static hackSuccess:Z


# instance fields
.field private elements:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    sput-boolean v0, Lorg/droidparts/inner/ann/Ann;->hackSuccess:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/annotation/Annotation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lorg/droidparts/inner/ann/Ann;, "Lorg/droidparts/inner/ann/Ann<TT;>;"
    .local p1, "annotation":Ljava/lang/annotation/Annotation;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-boolean v1, Lorg/droidparts/inner/ann/Ann;->hackSuccess:Z

    if-eqz v1, :cond_0

    .line 45
    :try_start_0
    invoke-static {p1}, Lorg/droidparts/inner/AnnotationElementsReader;->getElements(Ljava/lang/annotation/Annotation;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lorg/droidparts/inner/ann/Ann;->elements:Ljava/util/HashMap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    .line 48
    const/4 v1, 0x0

    sput-boolean v1, Lorg/droidparts/inner/ann/Ann;->hackSuccess:Z

    goto :goto_0
.end method


# virtual methods
.method protected final cleanup()V
    .locals 1

    .prologue
    .line 62
    .local p0, "this":Lorg/droidparts/inner/ann/Ann;, "Lorg/droidparts/inner/ann/Ann<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/droidparts/inner/ann/Ann;->elements:Ljava/util/HashMap;

    .line 63
    return-void
.end method

.method protected final getElement(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    .local p0, "this":Lorg/droidparts/inner/ann/Ann;, "Lorg/droidparts/inner/ann/Ann<TT;>;"
    iget-object v0, p0, Lorg/droidparts/inner/ann/Ann;->elements:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final hackSuccess()Z
    .locals 1

    .prologue
    .line 54
    .local p0, "this":Lorg/droidparts/inner/ann/Ann;, "Lorg/droidparts/inner/ann/Ann<TT;>;"
    sget-boolean v0, Lorg/droidparts/inner/ann/Ann;->hackSuccess:Z

    return v0
.end method
