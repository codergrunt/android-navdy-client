.class Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;
.super Ljava/lang/Object;
.source "ArrayCollectionConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/inner/converter/ArrayCollectionConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Wrapper"
.end annotation


# instance fields
.field private final arr:Lorg/json/JSONArray;

.field private final nodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/w3c/dom/Node;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/json/JSONArray;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "arr"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/w3c/dom/Node;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "nodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/w3c/dom/Node;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput-object p1, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->arr:Lorg/json/JSONArray;

    .line 171
    iput-object p2, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->nodes:Ljava/util/ArrayList;

    .line 172
    return-void
.end method


# virtual methods
.method public convert(Ljava/lang/Object;Lorg/droidparts/inner/converter/Converter;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .param p1, "item"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Lorg/droidparts/inner/converter/Converter",
            "<TV;>;",
            "Ljava/lang/Class",
            "<TV;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 188
    .local p2, "conv":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TV;>;"
    .local p3, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-virtual {p0}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->isJSON()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    .end local p1    # "item":Ljava/lang/Object;
    :goto_0
    return-object p1

    .restart local p1    # "item":Ljava/lang/Object;
    :cond_0
    move-object v0, p1

    .line 191
    check-cast v0, Lorg/w3c/dom/Node;

    .line 192
    .local v0, "n":Lorg/w3c/dom/Node;
    invoke-static {v0}, Lorg/droidparts/inner/PersistUtils;->getNodeText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    .line 193
    .local v1, "txt":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p2, p3, v2, v1}, Lorg/droidparts/inner/converter/Converter;->parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(I)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 179
    invoke-virtual {p0}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->isJSON()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->arr:Lorg/json/JSONArray;

    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->nodes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public isJSON()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->arr:Lorg/json/JSONArray;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->isJSON()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->arr:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->nodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public makeSerializer(Ljava/lang/Class;)Lorg/droidparts/persist/serializer/AbstractSerializer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lorg/droidparts/model/Model;",
            ">(",
            "Ljava/lang/Class",
            "<TM;>;)",
            "Lorg/droidparts/persist/serializer/AbstractSerializer",
            "<TM;**>;"
        }
    .end annotation

    .prologue
    .local p1, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TM;>;"
    const/4 v1, 0x0

    .line 199
    invoke-virtual {p0}, Lorg/droidparts/inner/converter/ArrayCollectionConverter$Wrapper;->isJSON()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    new-instance v0, Lorg/droidparts/persist/serializer/JSONSerializer;

    invoke-direct {v0, p1, v1}, Lorg/droidparts/persist/serializer/JSONSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    .line 202
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/droidparts/persist/serializer/XMLSerializer;

    invoke-direct {v0, p1, v1}, Lorg/droidparts/persist/serializer/XMLSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    goto :goto_0
.end method
