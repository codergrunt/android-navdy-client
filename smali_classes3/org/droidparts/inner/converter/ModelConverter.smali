.class public Lorg/droidparts/inner/converter/ModelConverter;
.super Lorg/droidparts/inner/converter/Converter;
.source "ModelConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/droidparts/inner/converter/Converter",
        "<",
        "Lorg/droidparts/model/Model;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/droidparts/inner/converter/Converter;-><init>()V

    return-void
.end method


# virtual methods
.method public canHandle(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isModel(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDBColumnType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, " BLOB"

    return-object v0
.end method

.method protected bridge synthetic parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2, p3}, Lorg/droidparts/inner/converter/ModelConverter;->parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method protected parseFromString(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Lorg/droidparts/model/Model;
    .locals 2
    .param p3, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Lorg/droidparts/model/Model;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Ljava/lang/String;",
            ")",
            "Lorg/droidparts/model/Model;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    new-instance v0, Lorg/droidparts/persist/serializer/JSONSerializer;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/droidparts/persist/serializer/JSONSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/droidparts/persist/serializer/JSONSerializer;->deserialize(Lorg/json/JSONObject;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # Landroid/content/ContentValues;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    move-object v5, p5

    check-cast v5, Lorg/droidparts/model/Model;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/inner/converter/ModelConverter;->putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Lorg/droidparts/model/Model;)V

    return-void
.end method

.method public putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Lorg/droidparts/model/Model;)V
    .locals 3
    .param p3, "cv"    # Landroid/content/ContentValues;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "val"    # Lorg/droidparts/model/Model;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Lorg/droidparts/model/Model;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Landroid/content/ContentValues;",
            "Ljava/lang/String;",
            "Lorg/droidparts/model/Model;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "valueType":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 50
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 51
    .local v2, "oos":Ljava/io/ObjectOutputStream;
    invoke-virtual {v2, p5}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 52
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 53
    .local v0, "arr":[B
    invoke-virtual {p3, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 54
    return-void
.end method

.method public bridge synthetic putToJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # Lorg/json/JSONObject;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    move-object v5, p5

    check-cast v5, Lorg/droidparts/model/Model;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/inner/converter/ModelConverter;->putToJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;Lorg/droidparts/model/Model;)V

    return-void
.end method

.method public putToJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;Lorg/droidparts/model/Model;)V
    .locals 4
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .param p5, "val"    # Lorg/droidparts/model/Model;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Lorg/droidparts/model/Model;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Lorg/droidparts/model/Model;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-virtual {p5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 75
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    new-instance v2, Lorg/droidparts/persist/serializer/JSONSerializer;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lorg/droidparts/persist/serializer/JSONSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    invoke-virtual {v2, p5}, Lorg/droidparts/persist/serializer/JSONSerializer;->serialize(Lorg/droidparts/model/Model;)Lorg/json/JSONObject;

    move-result-object v1

    .line 76
    .local v1, "valStr":Lorg/json/JSONObject;
    invoke-virtual {p3, p4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77
    return-void
.end method

.method public bridge synthetic readFromCursor(Ljava/lang/Class;Ljava/lang/Class;Landroid/database/Cursor;I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # Landroid/database/Cursor;
    .param p4, "x3"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/droidparts/inner/converter/ModelConverter;->readFromCursor(Ljava/lang/Class;Ljava/lang/Class;Landroid/database/Cursor;I)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public readFromCursor(Ljava/lang/Class;Ljava/lang/Class;Landroid/database/Cursor;I)Lorg/droidparts/model/Model;
    .locals 4
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "columnIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Lorg/droidparts/model/Model;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Landroid/database/Cursor;",
            "I)",
            "Lorg/droidparts/model/Model;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    const/4 v2, 0x0

    .line 61
    .local v2, "model":Lorg/droidparts/model/Model;
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 62
    .local v0, "arr":[B
    if-eqz v0, :cond_0

    .line 63
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 64
    .local v1, "bais":Ljava/io/ByteArrayInputStream;
    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 65
    .local v3, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "model":Lorg/droidparts/model/Model;
    check-cast v2, Lorg/droidparts/model/Model;

    .line 67
    .end local v1    # "bais":Ljava/io/ByteArrayInputStream;
    .end local v3    # "ois":Ljava/io/ObjectInputStream;
    .restart local v2    # "model":Lorg/droidparts/model/Model;
    :cond_0
    return-object v2
.end method

.method public bridge synthetic readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # Lorg/json/JSONObject;
    .param p4, "x3"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/droidparts/inner/converter/ModelConverter;->readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Lorg/droidparts/model/Model;
    .locals 2
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Lorg/droidparts/model/Model;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Lorg/droidparts/model/Model;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    new-instance v0, Lorg/droidparts/persist/serializer/JSONSerializer;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/droidparts/persist/serializer/JSONSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    invoke-virtual {p3, p4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/droidparts/persist/serializer/JSONSerializer;->deserialize(Lorg/json/JSONObject;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic readFromXML(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Class;
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # Lorg/w3c/dom/Node;
    .param p4, "x3"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/droidparts/inner/converter/ModelConverter;->readFromXML(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public readFromXML(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/droidparts/model/Model;
    .locals 2
    .param p3, "node"    # Lorg/w3c/dom/Node;
    .param p4, "nodeListItemTagHint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<",
            "Lorg/droidparts/model/Model;",
            ">;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            ")",
            "Lorg/droidparts/model/Model;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<Lorg/droidparts/model/Model;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    new-instance v0, Lorg/droidparts/persist/serializer/XMLSerializer;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/droidparts/persist/serializer/XMLSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    invoke-virtual {v0, p3}, Lorg/droidparts/persist/serializer/XMLSerializer;->deserialize(Lorg/w3c/dom/Node;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method
