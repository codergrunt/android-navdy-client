.class public Lorg/droidparts/inner/reader/ValueReader;
.super Ljava/lang/Object;
.source "ValueReader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getVal(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;Lorg/droidparts/inner/ann/FieldSpec;)Ljava/lang/Object;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "root"    # Landroid/view/View;
    .param p2, "target"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/inject/InjectAnn",
            "<*>;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    iget-object v0, p3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/inject/InjectAnn;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    .line 36
    .local v8, "annType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v0, p3, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    .line 37
    .local v5, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v0, p3, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    .line 39
    .local v6, "fieldName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 40
    .local v9, "val":Ljava/lang/Object;
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectDependencyAnn;

    if-ne v8, v0, :cond_1

    .line 41
    invoke-static {p0, v5}, Lorg/droidparts/inner/reader/DependencyReader;->readVal(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    .line 71
    .end local v9    # "val":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v9

    .line 42
    .restart local v9    # "val":Ljava/lang/Object;
    :cond_1
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;

    if-ne v8, v0, :cond_2

    .line 43
    iget-object v7, p3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;

    .line 44
    .local v7, "ann2":Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;
    iget-object v0, v7, Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;->key:Ljava/lang/String;

    iget-boolean v1, v7, Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;->optional:Z

    invoke-static {p2, v0, v1}, Lorg/droidparts/inner/reader/BundleExtraReader;->readVal(Ljava/lang/Object;Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v9

    .line 45
    goto :goto_0

    .end local v7    # "ann2":Lorg/droidparts/inner/ann/inject/InjectBundleExtraAnn;
    :cond_2
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectResourceAnn;

    if-ne v8, v0, :cond_3

    .line 46
    iget-object v7, p3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/inject/InjectResourceAnn;

    .line 47
    .local v7, "ann2":Lorg/droidparts/inner/ann/inject/InjectResourceAnn;
    iget v0, v7, Lorg/droidparts/inner/ann/inject/InjectResourceAnn;->id:I

    invoke-static {p0, v0, v5}, Lorg/droidparts/inner/reader/ResourceReader;->readVal(Landroid/content/Context;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    .line 48
    goto :goto_0

    .end local v7    # "ann2":Lorg/droidparts/inner/ann/inject/InjectResourceAnn;
    :cond_3
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;

    if-ne v8, v0, :cond_4

    .line 49
    iget-object v7, p3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;

    .line 50
    .local v7, "ann2":Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;
    iget-object v0, v7, Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;->name:Ljava/lang/String;

    invoke-static {p0, v0, v5}, Lorg/droidparts/inner/reader/SystemServiceReader;->readVal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    .line 51
    goto :goto_0

    .end local v7    # "ann2":Lorg/droidparts/inner/ann/inject/InjectSystemServiceAnn;
    :cond_4
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectViewAnn;

    if-ne v8, v0, :cond_5

    .line 52
    iget-object v7, p3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/inject/InjectViewAnn;

    .line 53
    .local v7, "ann2":Lorg/droidparts/inner/ann/inject/InjectViewAnn;
    iget v2, v7, Lorg/droidparts/inner/ann/inject/InjectViewAnn;->id:I

    iget-boolean v3, v7, Lorg/droidparts/inner/ann/inject/InjectViewAnn;->click:Z

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-static/range {v0 .. v6}, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->readVal(Landroid/content/Context;Landroid/view/View;IZLjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 55
    goto :goto_0

    .end local v7    # "ann2":Lorg/droidparts/inner/ann/inject/InjectViewAnn;
    :cond_5
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;

    if-ne v8, v0, :cond_7

    .line 56
    iget-object v7, p3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v7, Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;

    .line 57
    .local v7, "ann2":Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;
    invoke-static {}, Lorg/droidparts/inner/reader/LegacyReader;->isSupportAvaliable()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {p2}, Lorg/droidparts/inner/reader/LegacyReader;->isSupportObject(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 59
    iget v0, v7, Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;->id:I

    invoke-static {p2, v0, v6}, Lorg/droidparts/inner/reader/LegacyReader;->getFragment(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    goto :goto_0

    .line 61
    :cond_6
    iget v0, v7, Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;->id:I

    invoke-static {p2, v0, v6}, Lorg/droidparts/inner/reader/FragmentsReader;->getFragment(Ljava/lang/Object;ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v9

    .local v9, "val":Landroid/app/Fragment;
    goto :goto_0

    .line 63
    .end local v7    # "ann2":Lorg/droidparts/inner/ann/inject/InjectFragmentAnn;
    .local v9, "val":Ljava/lang/Object;
    :cond_7
    const-class v0, Lorg/droidparts/inner/ann/inject/InjectParentActivityAnn;

    if-ne v8, v0, :cond_0

    .line 64
    invoke-static {}, Lorg/droidparts/inner/reader/LegacyReader;->isSupportAvaliable()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {p2}, Lorg/droidparts/inner/reader/LegacyReader;->isSupportObject(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 66
    invoke-static {p2}, Lorg/droidparts/inner/reader/LegacyReader;->getParentActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v9

    .local v9, "val":Landroid/app/Activity;
    goto :goto_0

    .line 68
    .local v9, "val":Ljava/lang/Object;
    :cond_8
    invoke-static {p2}, Lorg/droidparts/inner/reader/FragmentsReader;->getParentActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v9

    .local v9, "val":Landroid/app/Activity;
    goto :goto_0
.end method
