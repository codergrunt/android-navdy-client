.class public interface abstract Lorg/droidparts/inner/ManifestMetaData$LogLevel;
.super Ljava/lang/Object;
.source "ManifestMetaData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/inner/ManifestMetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LogLevel"
.end annotation


# static fields
.field public static final ASSERT:Ljava/lang/String; = "assert"

.field public static final DEBUG:Ljava/lang/String; = "debug"

.field public static final DISABLE:Ljava/lang/String; = "disable"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final INFO:Ljava/lang/String; = "info"

.field public static final VERBOSE:Ljava/lang/String; = "verbose"

.field public static final WARN:Ljava/lang/String; = "warn"
