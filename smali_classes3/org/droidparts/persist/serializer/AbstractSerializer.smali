.class public abstract Lorg/droidparts/persist/serializer/AbstractSerializer;
.super Ljava/lang/Object;
.source "AbstractSerializer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelType:",
        "Lorg/droidparts/model/Model;",
        "Obj:",
        "Ljava/lang/Object;",
        "Arr:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final SUB:Ljava/lang/String; = "->\u001d"


# instance fields
.field protected final cls:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TModelType;>;"
        }
    .end annotation
.end field

.field private ctx:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Ljava/lang/Class;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TModelType;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lorg/droidparts/persist/serializer/AbstractSerializer;, "Lorg/droidparts/persist/serializer/AbstractSerializer<TModelType;TObj;TArr;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TModelType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lorg/droidparts/persist/serializer/AbstractSerializer;->cls:Ljava/lang/Class;

    .line 36
    if-eqz p2, :cond_0

    .line 37
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/persist/serializer/AbstractSerializer;->ctx:Landroid/content/Context;

    .line 38
    invoke-static {p2, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 40
    :cond_0
    return-void
.end method

.method protected static getNestedKeyParts(Ljava/lang/String;)Landroid/util/Pair;
    .locals 5
    .param p0, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    const-string v4, "->\u001d"

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 53
    .local v0, "firstSep":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 54
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "subKey":Ljava/lang/String;
    const-string v4, "->\u001d"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "leftKey":Ljava/lang/String;
    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 59
    .end local v1    # "leftKey":Ljava/lang/String;
    .end local v3    # "subKey":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract deserialize(Ljava/lang/Object;)Lorg/droidparts/model/Model;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TObj;)TModelType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract deserializeAll(Ljava/lang/Object;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TArr;)",
            "Ljava/util/ArrayList",
            "<TModelType;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    .local p0, "this":Lorg/droidparts/persist/serializer/AbstractSerializer;, "Lorg/droidparts/persist/serializer/AbstractSerializer<TModelType;TObj;TArr;>;"
    iget-object v0, p0, Lorg/droidparts/persist/serializer/AbstractSerializer;->ctx:Landroid/content/Context;

    return-object v0
.end method
