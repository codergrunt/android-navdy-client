.class public abstract Lorg/droidparts/AbstractDependencyProvider;
.super Ljava/lang/Object;
.source "AbstractDependencyProvider.java"


# instance fields
.field private final ctx:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/AbstractDependencyProvider;->ctx:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/droidparts/AbstractDependencyProvider;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method public final getDB()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lorg/droidparts/AbstractDependencyProvider;->getDBOpenHelper()Lorg/droidparts/persist/sql/AbstractDBOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public abstract getDBOpenHelper()Lorg/droidparts/persist/sql/AbstractDBOpenHelper;
.end method
