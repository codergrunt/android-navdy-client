.class public abstract Lorg/droidparts/concurrent/task/SimpleAsyncTask;
.super Lorg/droidparts/concurrent/task/AsyncTask;
.source "SimpleAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Result:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/droidparts/concurrent/task/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "TResult;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/droidparts/concurrent/task/AsyncTaskResultListener;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/droidparts/concurrent/task/AsyncTaskResultListener",
            "<TResult;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lorg/droidparts/concurrent/task/SimpleAsyncTask;, "Lorg/droidparts/concurrent/task/SimpleAsyncTask<TResult;>;"
    .local p2, "resultListener":Lorg/droidparts/concurrent/task/AsyncTaskResultListener;, "Lorg/droidparts/concurrent/task/AsyncTaskResultListener<TResult;>;"
    invoke-direct {p0, p1, p2}, Lorg/droidparts/concurrent/task/AsyncTask;-><init>(Landroid/content/Context;Lorg/droidparts/concurrent/task/AsyncTaskResultListener;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected abstract onExecute()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected bridge synthetic onExecute([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lorg/droidparts/concurrent/task/SimpleAsyncTask;, "Lorg/droidparts/concurrent/task/SimpleAsyncTask<TResult;>;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/concurrent/task/SimpleAsyncTask;->onExecute([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs onExecute([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lorg/droidparts/concurrent/task/SimpleAsyncTask;, "Lorg/droidparts/concurrent/task/SimpleAsyncTask<TResult;>;"
    invoke-virtual {p0}, Lorg/droidparts/concurrent/task/SimpleAsyncTask;->onExecute()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
