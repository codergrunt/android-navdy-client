.class Lcom/nokia/maps/ct$a;
.super Ljava/lang/Object;
.source "NativeMap.java"

# interfaces
.implements Ljava/util/Map$Entry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nokia/maps/ct;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Map$Entry",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/nokia/maps/ct;

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/nokia/maps/ct;Lcom/nokia/maps/ct$b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nokia/maps/ct$b",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 180
    invoke-interface {p2}, Lcom/nokia/maps/ct$b;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/nokia/maps/ct$b;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/nokia/maps/ct$a;-><init>(Lcom/nokia/maps/ct;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 181
    return-void
.end method

.method public constructor <init>(Lcom/nokia/maps/ct;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 183
    iput-object p1, p0, Lcom/nokia/maps/ct$a;->a:Lcom/nokia/maps/ct;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p2, p0, Lcom/nokia/maps/ct$a;->b:Ljava/lang/Object;

    .line 185
    iput-object p3, p0, Lcom/nokia/maps/ct$a;->c:Ljava/lang/Object;

    .line 186
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/nokia/maps/ct$a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/nokia/maps/ct$a;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TV;"
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
