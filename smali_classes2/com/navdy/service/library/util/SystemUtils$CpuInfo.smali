.class public Lcom/navdy/service/library/util/SystemUtils$CpuInfo;
.super Ljava/lang/Object;
.source "SystemUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/util/SystemUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CpuInfo"
.end annotation


# instance fields
.field private plist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;",
            ">;"
        }
    .end annotation
.end field

.field private system:I

.field private usr:I


# direct methods
.method public constructor <init>(IILjava/util/ArrayList;)V
    .locals 0
    .param p1, "usr"    # I
    .param p2, "system"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p3, "plist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->usr:I

    .line 68
    iput p2, p0, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->system:I

    .line 69
    iput-object p3, p0, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->plist:Ljava/util/ArrayList;

    .line 70
    return-void
.end method


# virtual methods
.method public getCpuSystem()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->system:I

    return v0
.end method

.method public getCpuUser()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->usr:I

    return v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/service/library/util/SystemUtils$CpuInfo;->plist:Ljava/util/ArrayList;

    return-object v0
.end method
