.class public final Lcom/navdy/service/library/util/LogUtils;
.super Ljava/lang/Object;
.source "LogUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x8000

.field private static final CRASH_DUMP_FILE:Ljava/lang/String; = "crash.log"

.field private static final CRASH_DUMP_STAGING:Ljava/lang/String; = "staging"

.field private static final CRLF:[B

.field private static LOG_FILES:[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile; = null

.field private static LOG_FILES_FOR_SNAPSHOT:[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile; = null

.field private static final LOG_MARKER:Ljava/lang/String; = "--------- beginning of main"

.field private static final SYSTEM_LOG_PATH:Ljava/lang/String; = ".logs"

.field private static final TAG:Ljava/lang/String;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/util/LogUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/util/LogUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 32
    const-class v0, Lcom/navdy/service/library/util/LogUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    .line 45
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "a.log"

    invoke-direct {v1, v2, v5}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "obd.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "mfi.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "lighting.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "dial.log"

    invoke-direct {v1, v2, v5}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v5

    const/4 v1, 0x5

    new-instance v2, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v3, "obdRaw.log"

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/util/LogUtils;->LOG_FILES:[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    .line 54
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "a.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "obd.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "mfi.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "lighting.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v2, "dial.log"

    invoke-direct {v1, v2, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v1, v0, v5

    const/4 v1, 0x5

    new-instance v2, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    const-string v3, "obdRaw.log"

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;-><init>(Ljava/lang/String;I)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/util/LogUtils;->LOG_FILES_FOR_SNAPSHOT:[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    .line 64
    const-string v0, "\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyComprehensiveSystemLogs(Ljava/lang/String;)V
    .locals 1
    .param p0, "stagingPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 285
    sget-object v0, Lcom/navdy/service/library/util/LogUtils;->LOG_FILES:[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    invoke-static {p0, v0}, Lcom/navdy/service/library/util/LogUtils;->copySystemLogs(Ljava/lang/String;[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;)V

    .line 286
    return-void
.end method

.method public static copySnapshotSystemLogs(Ljava/lang/String;)V
    .locals 1
    .param p0, "stagingPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 289
    sget-object v0, Lcom/navdy/service/library/util/LogUtils;->LOG_FILES_FOR_SNAPSHOT:[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;

    invoke-static {p0, v0}, Lcom/navdy/service/library/util/LogUtils;->copySystemLogs(Ljava/lang/String;[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;)V

    .line 290
    return-void
.end method

.method public static copySystemLogs(Ljava/lang/String;[Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;)V
    .locals 12
    .param p0, "stagingPath"    # Ljava/lang/String;
    .param p1, "logFilesRequirement"    # [Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 293
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".logs"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 294
    .local v6, "srcPath":Ljava/lang/String;
    array-length v9, p1

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_2

    aget-object v3, p1, v8

    .line 295
    .local v3, "logFile":Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;
    iget-object v1, v3, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;->name:Ljava/lang/String;

    .line 297
    .local v1, "filename":Ljava/lang/String;
    iget v0, v3, Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;->maxFiles:I

    .line 298
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 299
    move-object v4, v1

    .line 300
    .local v4, "logFileName":Ljava/lang/String;
    if-lez v2, :cond_0

    .line 301
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 303
    :cond_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 304
    .local v5, "logfile1":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 305
    .local v7, "targetFile1":Ljava/lang/String;
    invoke-static {v5, v7}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 298
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 294
    .end local v4    # "logFileName":Ljava/lang/String;
    .end local v5    # "logfile1":Ljava/lang/String;
    .end local v7    # "targetFile1":Ljava/lang/String;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 308
    .end local v0    # "count":I
    .end local v1    # "filename":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "logFile":Lcom/navdy/service/library/util/LogUtils$RotatingLogFile;
    :cond_2
    return-void
.end method

.method public static declared-synchronized createCrashDump(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Thread;Ljava/lang/Throwable;Z)V
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "crashThread"    # Ljava/lang/Thread;
    .param p3, "throwable"    # Ljava/lang/Throwable;
    .param p4, "nativeCrash"    # Z

    .prologue
    .line 109
    const-class v13, Lcom/navdy/service/library/util/LogUtils;

    monitor-enter v13

    const/4 v4, 0x0

    .line 112
    .local v4, "crashInfo":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v12

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    .line 113
    .local v7, "processName":Ljava/lang/String;
    sget-object v12, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    const-string v14, "creating CrashDump"

    invoke-static {v12, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    new-instance v8, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, "staging"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .local v8, "stagingDir":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 116
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 118
    :cond_0
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 119
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 122
    .local v9, "stagingPath":Ljava/lang/String;
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, "crash.log"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v5, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 123
    .end local v4    # "crashInfo":Ljava/io/FileOutputStream;
    .local v5, "crashInfo":Ljava/io/FileOutputStream;
    :try_start_1
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move/from16 v2, p4

    invoke-static {v5, v0, v7, v1, v2}, Lcom/navdy/service/library/util/LogUtils;->writeCrashInfo(Ljava/io/FileOutputStream;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Thread;Z)V

    .line 124
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 125
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 126
    const/4 v4, 0x0

    .line 130
    .end local v5    # "crashInfo":Ljava/io/FileOutputStream;
    .restart local v4    # "crashInfo":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-static {v9}, Lcom/navdy/service/library/util/LogUtils;->copyComprehensiveSystemLogs(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 136
    :goto_0
    :try_start_3
    const-string v11, ""

    .line 137
    .local v11, "tag":Ljava/lang/String;
    if-eqz p4, :cond_1

    .line 138
    const-string v11, "_native"

    .line 140
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, "_crash_"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, ".zip"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v6, v9}, Lcom/navdy/service/library/util/LogUtils;->zipStaging(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 149
    :try_start_4
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 151
    .end local v6    # "fileName":Ljava/lang/String;
    .end local v7    # "processName":Ljava/lang/String;
    .end local v8    # "stagingDir":Ljava/io/File;
    .end local v9    # "stagingPath":Ljava/lang/String;
    .end local v11    # "tag":Ljava/lang/String;
    :goto_1
    monitor-exit v13

    return-void

    .line 131
    .restart local v7    # "processName":Ljava/lang/String;
    .restart local v8    # "stagingDir":Ljava/io/File;
    .restart local v9    # "stagingPath":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 132
    .local v10, "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v12, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    const-string v14, "copySystemLogs"

    invoke-static {v12, v14, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 146
    .end local v7    # "processName":Ljava/lang/String;
    .end local v8    # "stagingDir":Ljava/io/File;
    .end local v9    # "stagingPath":Ljava/lang/String;
    .end local v10    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v10

    .line 147
    .restart local v10    # "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_6
    sget-object v12, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    const-string v14, "createCrashDump"

    invoke-static {v12, v14, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 149
    :try_start_7
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 109
    .end local v10    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v12

    monitor-exit v13

    throw v12

    .line 149
    :catchall_1
    move-exception v12

    :goto_3
    :try_start_8
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v12
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .end local v4    # "crashInfo":Ljava/io/FileOutputStream;
    .restart local v5    # "crashInfo":Ljava/io/FileOutputStream;
    .restart local v7    # "processName":Ljava/lang/String;
    .restart local v8    # "stagingDir":Ljava/io/File;
    .restart local v9    # "stagingPath":Ljava/lang/String;
    :catchall_2
    move-exception v12

    move-object v4, v5

    .end local v5    # "crashInfo":Ljava/io/FileOutputStream;
    .restart local v4    # "crashInfo":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 146
    .end local v4    # "crashInfo":Ljava/io/FileOutputStream;
    .restart local v5    # "crashInfo":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v10

    move-object v4, v5

    .end local v5    # "crashInfo":Ljava/io/FileOutputStream;
    .restart local v4    # "crashInfo":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static declared-synchronized createKernelCrashDump(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "kernelCrashFiles"    # [Ljava/lang/String;

    .prologue
    .line 353
    const-class v11, Lcom/navdy/service/library/util/LogUtils;

    monitor-enter v11

    :try_start_0
    new-instance v6, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "staging"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 354
    .local v6, "stagingDir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 355
    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 357
    :cond_0
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/io/File;)Z

    .line 359
    const/4 v2, 0x0

    .line 360
    .local v2, "count":I
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 361
    .local v7, "stagingPath":Ljava/lang/String;
    move-object/from16 v0, p2

    array-length v12, v0

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v12, :cond_2

    aget-object v8, p2, v10

    .line 362
    .local v8, "str":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 363
    .local v3, "f":Ljava/io/File;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 364
    .local v5, "fname":Ljava/lang/String;
    invoke-static {v8, v5}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 365
    sget-object v13, Lcom/navdy/service/library/util/LogUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "copied:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 366
    add-int/lit8 v2, v2, 0x1

    .line 361
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 370
    .end local v3    # "f":Ljava/io/File;
    .end local v5    # "fname":Ljava/lang/String;
    .end local v8    # "str":Ljava/lang/String;
    :cond_2
    if-lez v2, :cond_3

    .line 372
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "kernel_crash_"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, ".zip"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 373
    .local v4, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v4, v7}, Lcom/navdy/service/library/util/LogUtils;->zipStaging(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    sget-object v10, Lcom/navdy/service/library/util/LogUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "kernel dump created:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 380
    .end local v4    # "fileName":Ljava/lang/String;
    :goto_1
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-static {v0, v10}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    .end local v2    # "count":I
    .end local v6    # "stagingDir":Ljava/io/File;
    .end local v7    # "stagingPath":Ljava/lang/String;
    :goto_2
    monitor-exit v11

    return-void

    .line 376
    .restart local v2    # "count":I
    .restart local v6    # "stagingDir":Ljava/io/File;
    .restart local v7    # "stagingPath":Ljava/lang/String;
    :cond_3
    :try_start_1
    sget-object v10, Lcom/navdy/service/library/util/LogUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "no kernel crash files"

    invoke-virtual {v10, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 381
    .end local v2    # "count":I
    .end local v6    # "stagingDir":Ljava/io/File;
    .end local v7    # "stagingPath":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 382
    .local v9, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v10, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    const-string v12, "createKernelCrashDump"

    invoke-static {v10, v12, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 353
    .end local v9    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v10

    monitor-exit v11

    throw v10
.end method

.method public static declared-synchronized dumpLog(Landroid/content/Context;Ljava/lang/String;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 72
    const-class v13, Lcom/navdy/service/library/util/LogUtils;

    monitor-enter v13

    :try_start_0
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 74
    invoke-static/range {p0 .. p1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 76
    :cond_0
    const/4 v10, 0x0

    .line 77
    .local v10, "process":Ljava/lang/Process;
    const/4 v4, 0x0

    .line 78
    .local v4, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    const/4 v2, 0x0

    .line 79
    .local v2, "bufferedInputStream":Ljava/io/BufferedInputStream;
    const/4 v11, 0x0

    .line 80
    .local v11, "processInputStream":Ljava/io/InputStream;
    const v12, 0x8000

    new-array v1, v12, [B

    .line 81
    .local v1, "buffer":[B
    new-instance v8, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 83
    .local v8, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/BufferedOutputStream;

    const v12, 0x8000

    invoke-direct {v5, v8, v12}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 84
    .end local v4    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .local v5, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :try_start_2
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v12

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "logcat"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "-d"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string v16, "-v"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const-string v16, "time"

    aput-object v16, v14, v15

    invoke-virtual {v12, v14}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v10

    .line 85
    invoke-virtual {v10}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    .line 86
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v11}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 88
    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .local v3, "bufferedInputStream":Ljava/io/BufferedInputStream;
    :goto_0
    :try_start_3
    invoke-virtual {v3, v1}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v9

    .local v9, "len":I
    if-lez v9, :cond_2

    .line 89
    const/4 v12, 0x0

    invoke-virtual {v5, v1, v12, v9}, Ljava/io/BufferedOutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 93
    .end local v9    # "len":I
    :catchall_0
    move-exception v12

    move-object v2, v3

    .end local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    move-object v4, v5

    .end local v5    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :goto_1
    :try_start_4
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 94
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 95
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 96
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 97
    invoke-static {v11}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 98
    if-eqz v10, :cond_1

    .line 100
    :try_start_5
    invoke-virtual {v10}, Ljava/lang/Process;->destroy()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 103
    :cond_1
    :goto_2
    :try_start_6
    throw v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 72
    .end local v1    # "buffer":[B
    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v4    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v10    # "process":Ljava/lang/Process;
    .end local v11    # "processInputStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v12

    monitor-exit v13

    throw v12

    .line 91
    .restart local v1    # "buffer":[B
    .restart local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v5    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "len":I
    .restart local v10    # "process":Ljava/lang/Process;
    .restart local v11    # "processInputStream":Ljava/io/InputStream;
    :cond_2
    :try_start_7
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 93
    :try_start_8
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 94
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 95
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 96
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 97
    invoke-static {v11}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 98
    if-eqz v10, :cond_3

    .line 100
    :try_start_9
    invoke-virtual {v10}, Ljava/lang/Process;->destroy()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 106
    :cond_3
    :goto_3
    monitor-exit v13

    return-void

    .line 101
    :catch_0
    move-exception v6

    .line 102
    .local v6, "e":Ljava/lang/Throwable;
    :try_start_a
    sget-object v12, Lcom/navdy/service/library/util/LogUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v12, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 101
    .end local v3    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .end local v5    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v6    # "e":Ljava/lang/Throwable;
    .end local v9    # "len":I
    .restart local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v4    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catch_1
    move-exception v6

    .line 102
    .restart local v6    # "e":Ljava/lang/Throwable;
    sget-object v14, Lcom/navdy/service/library/util/LogUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v14, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_2

    .line 93
    .end local v6    # "e":Ljava/lang/Throwable;
    :catchall_2
    move-exception v12

    goto :goto_1

    .end local v4    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catchall_3
    move-exception v12

    move-object v4, v5

    .end local v5    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v4    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_1
.end method

.method private static getSystemLog()Ljava/lang/String;
    .locals 6

    .prologue
    .line 251
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".logs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "srcPath":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "a.log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 253
    .local v2, "logfile":Ljava/lang/String;
    const/4 v0, 0x0

    .line 255
    .local v0, "contents":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 259
    :goto_0
    return-object v0

    .line 256
    :catch_0
    move-exception v1

    .line 257
    .local v1, "e":Ljava/io/IOException;
    sget-object v4, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    const-string v5, "Failed to grab system log"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static systemLogStr(ILjava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "bytes"    # I
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 265
    invoke-static {}, Lcom/navdy/service/library/util/LogUtils;->getSystemLog()Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "contents":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 268
    .local v2, "llen":I
    move v1, v2

    .line 269
    .local v1, "end":I
    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 271
    .local v3, "slen":I
    invoke-virtual {v0, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 272
    .local v5, "strIdx":I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 275
    add-int v6, v5, v3

    add-int/lit8 v6, v6, 0xa

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 278
    .end local v3    # "slen":I
    .end local v5    # "strIdx":I
    :cond_0
    sub-int v6, v1, p0

    const/4 v7, 0x0

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 279
    .local v4, "start":I
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 281
    .end local v1    # "end":I
    .end local v2    # "llen":I
    .end local v4    # "start":I
    :cond_1
    return-object v0
.end method

.method private static writeCrashInfo(Ljava/io/FileOutputStream;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Thread;Z)V
    .locals 16
    .param p0, "fileOutputStream"    # Ljava/io/FileOutputStream;
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .param p2, "processName"    # Ljava/lang/String;
    .param p3, "crashThread"    # Ljava/lang/Thread;
    .param p4, "nativeCrash"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 156
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    .line 157
    .local v7, "runtime":Ljava/lang/Runtime;
    new-instance v2, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 160
    .local v2, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "process: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 161
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 162
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "time: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 163
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 164
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "serial: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 165
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 166
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "device: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 167
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 168
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "model: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 169
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 170
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "hw: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 171
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 172
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "id: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 173
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 174
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "version: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 175
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 176
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "bootloader: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 177
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 178
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "board: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 179
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 180
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "brand: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 181
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 182
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "manufacturer: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 183
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 184
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cpu_abi: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 185
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 186
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "cpu_abi2: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 187
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 188
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "processors: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 189
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 190
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 193
    if-eqz p4, :cond_0

    .line 194
    const-string v11, "------ native crash -----"

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 198
    :goto_0
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 199
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "thread name crashed:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 200
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 201
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 202
    new-instance v9, Ljava/io/StringWriter;

    invoke-direct {v9}, Ljava/io/StringWriter;-><init>()V

    .line 203
    .local v9, "sw":Ljava/io/StringWriter;
    new-instance v6, Lcom/navdy/service/library/log/FastPrintWriter;

    const/4 v11, 0x0

    const/16 v12, 0x100

    invoke-direct {v6, v9, v11, v12}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    .line 204
    .local v6, "pw":Ljava/io/PrintWriter;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 205
    invoke-virtual {v6}, Ljava/io/PrintWriter;->flush()V

    .line 206
    invoke-virtual {v9}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 207
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 208
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 211
    const-string v11, "------ state -----"

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 212
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 213
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "total:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 214
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 215
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "free:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 216
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 217
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "max:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 218
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 219
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "native heap size: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 220
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 221
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "native heap allocated: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 222
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 223
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "native heap free: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Landroid/os/Debug;->getNativeHeapFreeSize()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 224
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 225
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 228
    invoke-static {}, Ljava/lang/Thread;->getAllStackTraces()Ljava/util/Map;

    move-result-object v8

    .line 229
    .local v8, "stackTraces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "------ thread stackstraces count = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " -----"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 230
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 231
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 232
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 233
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Thread;

    .line 234
    .local v10, "t":Ljava/lang/Thread;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "\""

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, "\""

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 235
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 236
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "    Thread.State: "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v10}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 237
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 238
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/StackTraceElement;

    .line 239
    .local v4, "elements":[Ljava/lang/StackTraceElement;
    array-length v13, v4

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v13, :cond_1

    aget-object v3, v4, v11

    .line 240
    .local v3, "element":Ljava/lang/StackTraceElement;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "       at "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 241
    sget-object v14, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v14}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 239
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 196
    .end local v3    # "element":Ljava/lang/StackTraceElement;
    .end local v4    # "elements":[Ljava/lang/StackTraceElement;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    .end local v6    # "pw":Ljava/io/PrintWriter;
    .end local v8    # "stackTraces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    .end local v9    # "sw":Ljava/io/StringWriter;
    .end local v10    # "t":Ljava/lang/Thread;
    :cond_0
    const-string v11, "------ crash -----"

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    goto/16 :goto_0

    .line 243
    .restart local v4    # "elements":[Ljava/lang/StackTraceElement;
    .restart local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    .restart local v6    # "pw":Ljava/io/PrintWriter;
    .restart local v8    # "stackTraces":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    .restart local v9    # "sw":Ljava/io/StringWriter;
    .restart local v10    # "t":Ljava/lang/Thread;
    :cond_1
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 244
    sget-object v11, Lcom/navdy/service/library/util/LogUtils;->CRLF:[B

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V

    goto/16 :goto_1

    .line 247
    .end local v4    # "elements":[Ljava/lang/StackTraceElement;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Thread;[Ljava/lang/StackTraceElement;>;"
    .end local v10    # "t":Ljava/lang/Thread;
    :cond_2
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    .line 248
    return-void
.end method

.method public static zipStaging(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "stagingPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 311
    sget-object v18, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "zip started:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const/4 v6, 0x0

    .line 313
    .local v6, "fos":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 314
    .local v4, "fis":Ljava/io/FileInputStream;
    const/16 v16, 0x0

    .line 316
    .local v16, "zos":Ljava/util/zip/ZipOutputStream;
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 317
    .local v10, "l1":J
    const/16 v18, 0x4000

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 318
    .local v2, "buffer":[B
    new-instance v7, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v17, Ljava/util/zip/ZipOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v7}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 320
    .end local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    .local v17, "zos":Ljava/util/zip/ZipOutputStream;
    :try_start_2
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    .local v14, "srcFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v3

    .line 322
    .local v3, "files":[Ljava/io/File;
    if-eqz v3, :cond_2

    .line 323
    const/4 v8, 0x0

    .local v8, "i":I
    move-object v5, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/lang/Object;
    :goto_0
    :try_start_3
    array-length v0, v3

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v8, v0, :cond_1

    .line 324
    new-instance v4, Ljava/io/FileInputStream;

    aget-object v18, v3, v8

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 325
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :try_start_4
    new-instance v18, Ljava/util/zip/ZipEntry;

    .end local v5    # "fis":Ljava/lang/Object;
    aget-object v19, v3, v8

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v18}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 327
    :goto_1
    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    .local v9, "length":I
    if-lez v9, :cond_0

    .line 328
    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v9}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 337
    .end local v3    # "files":[Ljava/io/File;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "i":I
    .end local v9    # "length":I
    .end local v14    # "srcFile":Ljava/io/File;
    :catch_0
    move-exception v15

    move-object/from16 v16, v17

    .end local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v6, v7

    .line 338
    .end local v2    # "buffer":[B
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "l1":J
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .local v15, "t":Ljava/lang/Throwable;
    :goto_2
    :try_start_5
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 339
    const/4 v6, 0x0

    .line 340
    invoke-static/range {p0 .. p1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 341
    sget-object v18, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    const-string v19, "zip error"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 342
    throw v15
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 344
    .end local v15    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v18

    :goto_3
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 345
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 346
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v18

    .line 330
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "files":[Ljava/io/File;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "i":I
    .restart local v9    # "length":I
    .restart local v10    # "l1":J
    .restart local v14    # "srcFile":Ljava/io/File;
    .restart local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    :cond_0
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/util/zip/ZipOutputStream;->closeEntry()V

    .line 331
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 332
    const/4 v4, 0x0

    .line 323
    add-int/lit8 v8, v8, 0x1

    move-object v5, v4

    .restart local v5    # "fis":Ljava/lang/Object;
    goto :goto_0

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "length":I
    :cond_1
    move-object v4, v5

    .line 335
    .end local v5    # "fis":Ljava/lang/Object;
    .end local v8    # "i":I
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 336
    .local v12, "l2":J
    sget-object v18, Lcom/navdy/service/library/util/LogUtils;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "zip finished:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " time:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sub-long v20, v12, v10

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 344
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 345
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 346
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 348
    return-void

    .line 344
    .end local v3    # "files":[Ljava/io/File;
    .end local v12    # "l2":J
    .end local v14    # "srcFile":Ljava/io/File;
    .end local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    :catchall_1
    move-exception v18

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    :catchall_2
    move-exception v18

    move-object/from16 v16, v17

    .end local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v3    # "files":[Ljava/io/File;
    .restart local v5    # "fis":Ljava/lang/Object;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "i":I
    .restart local v14    # "srcFile":Ljava/io/File;
    .restart local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    :catchall_3
    move-exception v18

    move-object/from16 v16, v17

    .end local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v4, v5

    .restart local v4    # "fis":Ljava/io/FileInputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 337
    .end local v2    # "buffer":[B
    .end local v3    # "files":[Ljava/io/File;
    .end local v5    # "fis":Ljava/lang/Object;
    .end local v8    # "i":I
    .end local v10    # "l1":J
    .end local v14    # "srcFile":Ljava/io/File;
    :catch_1
    move-exception v15

    goto :goto_2

    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "l1":J
    :catch_2
    move-exception v15

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v3    # "files":[Ljava/io/File;
    .restart local v5    # "fis":Ljava/lang/Object;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v8    # "i":I
    .restart local v14    # "srcFile":Ljava/io/File;
    .restart local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    :catch_3
    move-exception v15

    move-object/from16 v16, v17

    .end local v17    # "zos":Ljava/util/zip/ZipOutputStream;
    .restart local v16    # "zos":Ljava/util/zip/ZipOutputStream;
    move-object v4, v5

    .restart local v4    # "fis":Ljava/io/FileInputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2
.end method
