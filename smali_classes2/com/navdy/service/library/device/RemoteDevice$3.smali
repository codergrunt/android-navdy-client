.class Lcom/navdy/service/library/device/RemoteDevice$3;
.super Ljava/lang/Object;
.source "RemoteDevice.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDevice$EventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/RemoteDevice;->dispatchConnectFailureEvent(Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/RemoteDevice;

.field final synthetic val$cause:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 509
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDevice$3;->this$0:Lcom/navdy/service/library/device/RemoteDevice;

    iput-object p2, p0, Lcom/navdy/service/library/device/RemoteDevice$3;->val$cause:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/RemoteDevice$Listener;)V
    .locals 1
    .param p1, "source"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "listener"    # Lcom/navdy/service/library/device/RemoteDevice$Listener;

    .prologue
    .line 512
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice$3;->val$cause:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    invoke-interface {p2, p1, v0}, Lcom/navdy/service/library/device/RemoteDevice$Listener;->onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    .line 513
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 509
    check-cast p1, Lcom/navdy/service/library/device/RemoteDevice;

    check-cast p2, Lcom/navdy/service/library/device/RemoteDevice$Listener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/service/library/device/RemoteDevice$3;->dispatchEvent(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/RemoteDevice$Listener;)V

    return-void
.end method
