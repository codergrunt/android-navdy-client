.class public final enum Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
.super Ljava/lang/Enum;
.source "RemoteDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/RemoteDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

.field public static final enum CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

.field public static final enum DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

.field public static final enum LINK_ESTABLISHED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 75
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .line 78
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    const-string v1, "LINK_ESTABLISHED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->LINK_ESTABLISHED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .line 80
    new-instance v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    .line 73
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->DISCONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->LINK_ESTABLISHED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->CONNECTED:Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->$VALUES:[Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 73
    const-class v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->$VALUES:[Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    invoke-virtual {v0}, [Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/device/RemoteDevice$LinkStatus;

    return-object v0
.end method
