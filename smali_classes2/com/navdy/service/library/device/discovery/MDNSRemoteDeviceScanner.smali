.class public Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;
.super Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;
.source "MDNSRemoteDeviceScanner.java"


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected discoveryTimer:Ljava/util/Timer;

.field protected mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

.field protected mDiscoveryRunning:Z

.field protected mHandler:Landroid/os/Handler;

.field protected mNsdManager:Landroid/net/nsd/NsdManager;

.field protected mResolveListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/nsd/NsdManager$ResolveListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mResolvingService:Landroid/net/nsd/NsdServiceInfo;

.field protected final mServiceType:Ljava/lang/String;

.field protected mUnresolvedServices:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/net/nsd/NsdServiceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object p2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolveListeners:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mUnresolvedServices:Ljava/util/Queue;

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mHandler:Landroid/os/Handler;

    .line 43
    return-void
.end method


# virtual methods
.method public getNewResolveListener()Landroid/net/nsd/NsdManager$ResolveListener;
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$3;-><init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;)V

    return-object v0
.end method

.method public initNsd()V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->initializeDiscoveryListener()V

    .line 109
    return-void
.end method

.method public initializeDiscoveryListener()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mContext:Landroid/content/Context;

    const-string v1, "servicediscovery"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/nsd/NsdManager;

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$2;-><init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

    .line 166
    :cond_1
    return-void
.end method

.method protected resolveComplete()V
    .locals 2

    .prologue
    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolvingService:Landroid/net/nsd/NsdServiceInfo;

    .line 182
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mUnresolvedServices:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mUnresolvedServices:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/nsd/NsdServiceInfo;

    .line 184
    .local v0, "service":Landroid/net/nsd/NsdServiceInfo;
    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->resolveService(Landroid/net/nsd/NsdServiceInfo;)V

    .line 186
    .end local v0    # "service":Landroid/net/nsd/NsdServiceInfo;
    :cond_0
    return-void
.end method

.method protected resolveService(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 2
    .param p1, "service"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 169
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolvingService:Landroid/net/nsd/NsdServiceInfo;

    if-nez v1, :cond_0

    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolvingService:Landroid/net/nsd/NsdServiceInfo;

    .line 171
    invoke-virtual {p0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->getNewResolveListener()Landroid/net/nsd/NsdManager$ResolveListener;

    move-result-object v0

    .line 172
    .local v0, "resolveListener":Landroid/net/nsd/NsdManager$ResolveListener;
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mResolveListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    invoke-virtual {v1, p1, v0}, Landroid/net/nsd/NsdManager;->resolveService(Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$ResolveListener;)V

    .line 178
    .end local v0    # "resolveListener":Landroid/net/nsd/NsdManager$ResolveListener;
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mUnresolvedServices:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public startScan()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 47
    sget-object v1, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting scan: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->initNsd()V

    .line 50
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    if-nez v1, :cond_0

    .line 51
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t scan: no NsdManager: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x0

    .line 74
    :goto_0
    return v0

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryRunning:Z

    if-eqz v1, :cond_2

    .line 56
    :cond_1
    sget-object v1, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t start: already started "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_2
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    .line 63
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    new-instance v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;

    invoke-direct {v2, p0}, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;-><init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;)V

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method public stopScan()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 79
    sget-object v3, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stopping scan: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 81
    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    if-nez v3, :cond_0

    .line 82
    sget-object v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t stop: already stopped: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 104
    :goto_0
    return v1

    .line 86
    :cond_0
    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    if-eqz v3, :cond_1

    .line 87
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 88
    iput-object v6, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->discoveryTimer:Ljava/util/Timer;

    move v1, v2

    .line 89
    goto :goto_0

    .line 92
    :cond_1
    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

    if-eqz v3, :cond_3

    .line 94
    :try_start_0
    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    iget-object v4, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

    invoke-virtual {v3, v4}, Landroid/net/nsd/NsdManager;->stopServiceDiscovery(Landroid/net/nsd/NsdManager$DiscoveryListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_2
    :goto_1
    iput-object v6, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

    .line 101
    iput-boolean v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryRunning:Z

    :cond_3
    move v1, v2

    .line 104
    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/Exception;
    iget-boolean v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryRunning:Z

    if-eqz v3, :cond_2

    .line 97
    sget-object v3, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Problem stopping nsd service discovery"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
