.class Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;
.super Ljava/lang/Thread;
.source "SocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/SocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectThread"
.end annotation


# instance fields
.field private mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

.field final synthetic this$0:Lcom/navdy/service/library/device/connection/SocketConnection;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/SocketConnection;)V
    .locals 1

    .prologue
    .line 130
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 131
    const-string v0, "ConnectThread"

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->setName(Ljava/lang/String;)V

    .line 132
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v0, v0, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[SOCK]Cancelling connect, closing socket"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/connection/SocketConnection;->access$100(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    .line 191
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    .line 135
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "BEGIN mConnectThread"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 137
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;->UNKNOWN:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .line 142
    .local v0, "cause":Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    invoke-static {v4}, Lcom/navdy/service/library/device/connection/SocketConnection;->access$000(Lcom/navdy/service/library/device/connection/SocketConnection;)Lcom/navdy/service/library/network/SocketFactory;

    move-result-object v4

    invoke-interface {v4}, Lcom/navdy/service/library/network/SocketFactory;->build()Lcom/navdy/service/library/network/SocketAdapter;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    .line 143
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-interface {v4}, Lcom/navdy/service/library/network/SocketAdapter;->connect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    iget-object v5, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    monitor-enter v5

    .line 181
    :try_start_1
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    const/4 v6, 0x0

    iput-object v6, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;

    .line 182
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 184
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v5, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/device/connection/SocketConnection;->connected(Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 185
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to connect - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 148
    instance-of v4, v1, Ljava/net/SocketException;

    if-eqz v4, :cond_0

    .line 149
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "message":Ljava/lang/String;
    const-string v4, "ETIMEDOUT"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 151
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;->CONNECTION_TIMED_OUT:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .line 158
    .end local v2    # "message":Ljava/lang/String;
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    monitor-enter v5

    .line 159
    :try_start_2
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Connect failed, closing socket"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 160
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v6, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-static {v4, v6}, Lcom/navdy/service/library/device/connection/SocketConnection;->access$100(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 161
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->mmSocket:Lcom/navdy/service/library/network/SocketAdapter;

    .line 162
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v3, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 163
    .local v3, "oldStatus":Lcom/navdy/service/library/device/connection/Connection$Status;
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    sget-object v6, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v6, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 164
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 166
    sget-object v4, Lcom/navdy/service/library/device/connection/SocketConnection$1;->$SwitchMap$com$navdy$service$library$device$connection$Connection$Status:[I

    invoke-virtual {v3}, Lcom/navdy/service/library/device/connection/Connection$Status;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 174
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    iget-object v4, v4, Lcom/navdy/service/library/device/connection/SocketConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected state during connection failure: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 152
    .end local v3    # "oldStatus":Lcom/navdy/service/library/device/connection/Connection$Status;
    .restart local v2    # "message":Ljava/lang/String;
    :cond_1
    const-string v4, "ECONNREFUSED"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 153
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;->CONNECTION_REFUSED:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    goto :goto_1

    .line 164
    .end local v2    # "message":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 168
    .restart local v3    # "oldStatus":Lcom/navdy/service/library/device/connection/Connection$Status;
    :pswitch_0
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    invoke-static {v4, v0}, Lcom/navdy/service/library/device/connection/SocketConnection;->access$200(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    goto/16 :goto_0

    .line 171
    :pswitch_1
    iget-object v4, p0, Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;->this$0:Lcom/navdy/service/library/device/connection/SocketConnection;

    sget-object v5, Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;->NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/device/connection/SocketConnection;->dispatchDisconnectEvent(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    goto/16 :goto_0

    .line 182
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "oldStatus":Lcom/navdy/service/library/device/connection/Connection$Status;
    :catchall_1
    move-exception v4

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    .line 166
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
