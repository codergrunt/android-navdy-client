.class Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;
.super Ljava/lang/Object;
.source "ConnectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingConnectHandler"
.end annotation


# instance fields
.field private connection:Lcom/navdy/service/library/device/connection/Connection;

.field private device:Lcom/navdy/service/library/device/RemoteDevice;

.field final synthetic this$0:Lcom/navdy/service/library/device/connection/ConnectionService;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionService;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection;)V
    .locals 0
    .param p2, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p3, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 525
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 526
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->device:Lcom/navdy/service/library/device/RemoteDevice;

    .line 527
    iput-object p3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->connection:Lcom/navdy/service/library/device/connection/Connection;

    .line 528
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 533
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->device:Lcom/navdy/service/library/device/RemoteDevice;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 534
    .local v0, "newDevice":Z
    :goto_0
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_1

    const-string v1, "connect"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->device:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 535
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->device:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 537
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->connection:Lcom/navdy/service/library/device/connection/Connection;

    if-eqz v1, :cond_2

    .line 540
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->connection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/RemoteDevice;->setActiveConnection(Lcom/navdy/service/library/device/connection/Connection;)Z

    .line 552
    :goto_2
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->access$502(Lcom/navdy/service/library/device/connection/ConnectionService;Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;)Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;

    .line 553
    return-void

    .line 533
    .end local v0    # "newDevice":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 534
    .restart local v0    # "newDevice":Z
    :cond_1
    const-string v1, "reconnect"

    goto :goto_1

    .line 542
    :cond_2
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 544
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService;->onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V

    goto :goto_2

    .line 546
    :cond_3
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    if-eqz v0, :cond_4

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    :goto_3
    invoke-virtual {v2, v1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 547
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$PendingConnectHandler;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v1, v1, Lcom/navdy/service/library/device/connection/ConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->connect()Z

    goto :goto_2

    .line 546
    :cond_4
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    goto :goto_3
.end method
