.class public final enum Lcom/navdy/service/library/device/connection/ConnectionService$State;
.super Ljava/lang/Enum;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/device/connection/ConnectionService$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum CONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum DESTROYED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum DISCONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum LISTENING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum PAIRING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

.field public static final enum START:Lcom/navdy/service/library/device/connection/ConnectionService$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 610
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->START:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 611
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 612
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v5}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 613
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v6}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 614
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "RECONNECTING"

    invoke-direct {v0, v1, v7}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 615
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "CONNECTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 616
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "DISCONNECTING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 617
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "DISCONNECTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 618
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "PAIRING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->PAIRING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 619
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "LISTENING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->LISTENING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 620
    new-instance v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    const-string v1, "DESTROYED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionService$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DESTROYED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    .line 609
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/navdy/service/library/device/connection/ConnectionService$State;

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->START:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->SEARCHING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionService$State;->RECONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->CONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DISCONNECTED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->PAIRING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->LISTENING:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionService$State;->DESTROYED:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->$VALUES:[Lcom/navdy/service/library/device/connection/ConnectionService$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 609
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/device/connection/ConnectionService$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 609
    const-class v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/device/connection/ConnectionService$State;
    .locals 1

    .prologue
    .line 609
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionService$State;->$VALUES:[Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v0}, [Lcom/navdy/service/library/device/connection/ConnectionService$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/device/connection/ConnectionService$State;

    return-object v0
.end method
