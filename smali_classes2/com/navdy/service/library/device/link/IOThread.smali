.class public Lcom/navdy/service/library/device/link/IOThread;
.super Ljava/lang/Thread;
.source "IOThread.java"


# static fields
.field private static final SHUTDOWN_TIMEOUT:I = 0x3e8


# instance fields
.field protected volatile closing:Z

.field protected final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 9
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/service/library/device/link/IOThread;->logger:Lcom/navdy/service/library/log/Logger;

    .line 10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/device/link/IOThread;->closing:Z

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 15
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/service/library/device/link/IOThread;->closing:Z

    .line 16
    invoke-virtual {p0}, Lcom/navdy/service/library/device/link/IOThread;->interrupt()V

    .line 19
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-virtual {p0, v2, v3}, Lcom/navdy/service/library/device/link/IOThread;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/link/IOThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    iget-object v1, p0, Lcom/navdy/service/library/device/link/IOThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Thread still alive after join"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 26
    :cond_0
    return-void

    .line 20
    :catch_0
    move-exception v0

    .line 21
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/navdy/service/library/device/link/IOThread;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Interrupted"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isClosing()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/navdy/service/library/device/link/IOThread;->closing:Z

    return v0
.end method
