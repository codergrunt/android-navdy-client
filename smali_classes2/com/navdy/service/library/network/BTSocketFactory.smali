.class public Lcom/navdy/service/library/network/BTSocketFactory;
.super Ljava/lang/Object;
.source "BTSocketFactory.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketFactory;


# instance fields
.field private final address:Ljava/lang/String;

.field private final secure:Z

.field private final serviceUUID:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ServiceAddress;)V
    .locals 2
    .param p1, "address"    # Lcom/navdy/service/library/device/connection/ServiceAddress;

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getService()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/network/BTSocketFactory;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/UUID;)V
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "serviceUUID"    # Ljava/util/UUID;

    .prologue
    .line 20
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/service/library/network/BTSocketFactory;-><init>(Ljava/lang/String;Ljava/util/UUID;Z)V

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/UUID;Z)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "serviceUUID"    # Ljava/util/UUID;
    .param p3, "secure"    # Z

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/navdy/service/library/network/BTSocketFactory;->address:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/navdy/service/library/network/BTSocketFactory;->serviceUUID:Ljava/util/UUID;

    .line 26
    iput-boolean p3, p0, Lcom/navdy/service/library/network/BTSocketFactory;->secure:Z

    .line 27
    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/network/SocketAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 36
    .local v0, "adapter":Landroid/bluetooth/BluetoothAdapter;
    iget-object v2, p0, Lcom/navdy/service/library/network/BTSocketFactory;->address:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 37
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    iget-boolean v2, p0, Lcom/navdy/service/library/network/BTSocketFactory;->secure:Z

    if-eqz v2, :cond_0

    .line 38
    new-instance v2, Lcom/navdy/service/library/network/BTSocketAdapter;

    iget-object v3, p0, Lcom/navdy/service/library/network/BTSocketFactory;->serviceUUID:Ljava/util/UUID;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/network/BTSocketAdapter;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    .line 40
    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/navdy/service/library/network/BTSocketAdapter;

    iget-object v3, p0, Lcom/navdy/service/library/network/BTSocketFactory;->serviceUUID:Ljava/util/UUID;

    invoke-virtual {v1, v3}, Landroid/bluetooth/BluetoothDevice;->createInsecureRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/network/BTSocketAdapter;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    goto :goto_0
.end method
