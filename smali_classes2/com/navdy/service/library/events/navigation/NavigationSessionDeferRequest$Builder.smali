.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationSessionDeferRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public delay:Ljava/lang/Integer;

.field public expireTimestamp:Ljava/lang/Long;

.field public originDisplay:Ljava/lang/Boolean;

.field public requestId:Ljava/lang/String;

.field public routeId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 112
    if-nez p1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->requestId:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->routeId:Ljava/lang/String;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->delay:Ljava/lang/Integer;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->originDisplay:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->expireTimestamp:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->checkRequiredFields()V

    .line 165
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    move-result-object v0

    return-object v0
.end method

.method public delay(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    .locals 0
    .param p1, "delay"    # Ljava/lang/Integer;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->delay:Ljava/lang/Integer;

    .line 143
    return-object p0
.end method

.method public expireTimestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    .locals 0
    .param p1, "expireTimestamp"    # Ljava/lang/Long;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->expireTimestamp:Ljava/lang/Long;

    .line 159
    return-object p0
.end method

.method public originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    .locals 0
    .param p1, "originDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    .line 151
    return-object p0
.end method

.method public requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->requestId:Ljava/lang/String;

    .line 125
    return-object p0
.end method

.method public routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest$Builder;->routeId:Ljava/lang/String;

    .line 135
    return-object p0
.end method
