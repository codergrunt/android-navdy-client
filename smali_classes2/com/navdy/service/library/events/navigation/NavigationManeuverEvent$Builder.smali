.class public final Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationManeuverEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public currentRoad:Ljava/lang/String;

.field public distance_to_pending_turn:Ljava/lang/String;

.field public eta:Ljava/lang/String;

.field public etaUtcTime:Ljava/lang/Long;

.field public navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public pending_street:Ljava/lang/String;

.field public pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public speed:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 133
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 137
    if-nez p1, :cond_0

    .line 146
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->currentRoad:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->currentRoad:Ljava/lang/String;

    .line 139
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 140
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->distance_to_pending_turn:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->distance_to_pending_turn:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_street:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->pending_street:Ljava/lang/String;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->eta:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->eta:Ljava/lang/String;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->speed:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->speed:Ljava/lang/String;

    .line 144
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 145
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->etaUtcTime:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->etaUtcTime:Ljava/lang/Long;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->checkRequiredFields()V

    .line 215
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    move-result-object v0

    return-object v0
.end method

.method public currentRoad(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "currentRoad"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->currentRoad:Ljava/lang/String;

    .line 153
    return-object p0
.end method

.method public distance_to_pending_turn(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "distance_to_pending_turn"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->distance_to_pending_turn:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public eta(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "eta"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->eta:Ljava/lang/String;

    .line 185
    return-object p0
.end method

.method public etaUtcTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "etaUtcTime"    # Ljava/lang/Long;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->etaUtcTime:Ljava/lang/Long;

    .line 209
    return-object p0
.end method

.method public navigationState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "navigationState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 201
    return-object p0
.end method

.method public pending_street(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "pending_street"    # Ljava/lang/String;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->pending_street:Ljava/lang/String;

    .line 177
    return-object p0
.end method

.method public pending_turn(Lcom/navdy/service/library/events/navigation/NavigationTurn;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "pending_turn"    # Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 161
    return-object p0
.end method

.method public speed(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .locals 0
    .param p1, "speed"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->speed:Ljava/lang/String;

    .line 193
    return-object p0
.end method
