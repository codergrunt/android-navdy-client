.class public final enum Lcom/navdy/service/library/events/preferences/ScrollableSide;
.super Ljava/lang/Enum;
.source "ScrollableSide.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/ScrollableSide;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/ScrollableSide;

.field public static final enum LEFT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

.field public static final enum RIGHT:Lcom/navdy/service/library/events/preferences/ScrollableSide;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/service/library/events/preferences/ScrollableSide;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->LEFT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/preferences/ScrollableSide;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->RIGHT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .line 7
    new-array v0, v4, [Lcom/navdy/service/library/events/preferences/ScrollableSide;

    sget-object v1, Lcom/navdy/service/library/events/preferences/ScrollableSide;->LEFT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/preferences/ScrollableSide;->RIGHT:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->$VALUES:[Lcom/navdy/service/library/events/preferences/ScrollableSide;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->value:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/ScrollableSide;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/ScrollableSide;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->$VALUES:[Lcom/navdy/service/library/events/preferences/ScrollableSide;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/ScrollableSide;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/ScrollableSide;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/navdy/service/library/events/preferences/ScrollableSide;->value:I

    return v0
.end method
