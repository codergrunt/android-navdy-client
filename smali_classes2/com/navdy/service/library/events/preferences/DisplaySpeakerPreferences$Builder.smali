.class public final Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplaySpeakerPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public feedback_sound:Ljava/lang/Boolean;

.field public master_sound:Ljava/lang/Boolean;

.field public notification_sound:Ljava/lang/Boolean;

.field public serial_number:Ljava/lang/Long;

.field public volume_level:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 114
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 118
    if-nez p1, :cond_0

    .line 124
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 120
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->master_sound:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->master_sound:Ljava/lang/Boolean;

    .line 121
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->volume_level:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->volume_level:Ljava/lang/Integer;

    .line 122
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->feedback_sound:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->feedback_sound:Ljava/lang/Boolean;

    .line 123
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->notification_sound:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->notification_sound:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->checkRequiredFields()V

    .line 170
    new-instance v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;-><init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    move-result-object v0

    return-object v0
.end method

.method public feedback_sound(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    .locals 0
    .param p1, "feedback_sound"    # Ljava/lang/Boolean;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->feedback_sound:Ljava/lang/Boolean;

    .line 156
    return-object p0
.end method

.method public master_sound(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    .locals 0
    .param p1, "master_sound"    # Ljava/lang/Boolean;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->master_sound:Ljava/lang/Boolean;

    .line 140
    return-object p0
.end method

.method public notification_sound(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    .locals 0
    .param p1, "notification_sound"    # Ljava/lang/Boolean;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->notification_sound:Ljava/lang/Boolean;

    .line 164
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 132
    return-object p0
.end method

.method public volume_level(Ljava/lang/Integer;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    .locals 0
    .param p1, "volume_level"    # Ljava/lang/Integer;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->volume_level:Ljava/lang/Integer;

    .line 148
    return-object p0
.end method
