.class public final Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplaySpeakerPreferencesUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

.field public serial_number:Ljava/lang/Long;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 98
    if-nez p1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 100
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 101
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 102
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->checkRequiredFields()V

    .line 140
    new-instance v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;-><init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    move-result-object v0

    return-object v0
.end method

.method public preferences(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;
    .locals 0
    .param p1, "preferences"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .line 134
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 126
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 110
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 118
    return-object p0
.end method
