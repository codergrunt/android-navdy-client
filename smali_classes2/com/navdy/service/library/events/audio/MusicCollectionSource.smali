.class public final enum Lcom/navdy/service/library/events/audio/MusicCollectionSource;
.super Ljava/lang/Enum;
.source "MusicCollectionSource.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final enum COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final enum COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final enum COLLECTION_SOURCE_ANDROID_SPOTIFY:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final enum COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final enum COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const-string v1, "COLLECTION_SOURCE_UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const-string v1, "COLLECTION_SOURCE_ANDROID_LOCAL"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const-string v1, "COLLECTION_SOURCE_IOS_MEDIA_PLAYER"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const-string v1, "COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    const-string v1, "COLLECTION_SOURCE_ANDROID_SPOTIFY"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_SPOTIFY:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 7
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_IOS_MEDIA_PLAYER:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_SPOTIFY:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->value:I

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/MusicCollectionSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->value:I

    return v0
.end method
