.class public final Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCollectionFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCollectionFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionFilter;",
        ">;"
    }
.end annotation


# instance fields
.field public field:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionFilter;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCollectionFilter;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 68
    if-nez p1, :cond_0

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->field:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->field:Ljava/lang/String;

    .line 70
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->value:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->value:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCollectionFilter;
    .locals 2

    .prologue
    .line 91
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionFilter$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionFilter;

    move-result-object v0

    return-object v0
.end method

.method public field(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;
    .locals 0
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->field:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->value:Ljava/lang/String;

    .line 86
    return-object p0
.end method
