.class public final enum Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
.super Ljava/lang/Enum;
.source "PhoneStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/PhoneStatus;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_CONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_DISCONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_HELD:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field public static final enum PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_IDLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_RINGING"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_OFFHOOK"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_DIALING"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_HELD"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_HELD:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_CONNECTING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_CONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 15
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const-string v1, "PHONE_DISCONNECTING"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DISCONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 7
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_HELD:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_CONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DISCONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->$VALUES:[Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->value:I

    .line 21
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->$VALUES:[Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->value:I

    return v0
.end method
