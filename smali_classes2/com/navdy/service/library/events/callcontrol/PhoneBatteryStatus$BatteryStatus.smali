.class public final enum Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
.super Ljava/lang/Enum;
.source "PhoneBatteryStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BatteryStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

.field public static final enum BATTERY_EXTREMELY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

.field public static final enum BATTERY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

.field public static final enum BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 125
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    const-string v1, "BATTERY_OK"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 129
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    const-string v1, "BATTERY_LOW"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 133
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    const-string v1, "BATTERY_EXTREMELY_LOW"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_EXTREMELY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 120
    new-array v0, v5, [Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_EXTREMELY_LOW:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->$VALUES:[Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 138
    iput p3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->value:I

    .line 139
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    const-class v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->$VALUES:[Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->value:I

    return v0
.end method
