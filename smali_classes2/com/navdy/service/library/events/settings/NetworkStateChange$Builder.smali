.class public final Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NetworkStateChange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/settings/NetworkStateChange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/settings/NetworkStateChange;",
        ">;"
    }
.end annotation


# instance fields
.field public cellNetwork:Ljava/lang/Boolean;

.field public networkAvailable:Ljava/lang/Boolean;

.field public reachability:Ljava/lang/Boolean;

.field public wifiNetwork:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 91
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/settings/NetworkStateChange;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 95
    if-nez p1, :cond_0

    .line 100
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->networkAvailable:Ljava/lang/Boolean;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->cellNetwork:Ljava/lang/Boolean;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->wifiNetwork:Ljava/lang/Boolean;

    .line 99
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->reachability:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/settings/NetworkStateChange;-><init>(Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;Lcom/navdy/service/library/events/settings/NetworkStateChange$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->build()Lcom/navdy/service/library/events/settings/NetworkStateChange;

    move-result-object v0

    return-object v0
.end method

.method public cellNetwork(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
    .locals 0
    .param p1, "cellNetwork"    # Ljava/lang/Boolean;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->cellNetwork:Ljava/lang/Boolean;

    .line 115
    return-object p0
.end method

.method public networkAvailable(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
    .locals 0
    .param p1, "networkAvailable"    # Ljava/lang/Boolean;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->networkAvailable:Ljava/lang/Boolean;

    .line 107
    return-object p0
.end method

.method public reachability(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
    .locals 0
    .param p1, "reachability"    # Ljava/lang/Boolean;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->reachability:Ljava/lang/Boolean;

    .line 132
    return-object p0
.end method

.method public wifiNetwork(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;
    .locals 0
    .param p1, "wifiNetwork"    # Ljava/lang/Boolean;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/NetworkStateChange$Builder;->wifiNetwork:Ljava/lang/Boolean;

    .line 123
    return-object p0
.end method
