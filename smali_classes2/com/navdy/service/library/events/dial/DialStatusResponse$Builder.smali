.class public final Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DialStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/dial/DialStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/dial/DialStatusResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public batteryLevel:Ljava/lang/Integer;

.field public isConnected:Ljava/lang/Boolean;

.field public isPaired:Ljava/lang/Boolean;

.field public macAddress:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/dial/DialStatusResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/dial/DialStatusResponse;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 106
    if-nez p1, :cond_0

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->isPaired:Ljava/lang/Boolean;

    .line 108
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->isConnected:Ljava/lang/Boolean;

    .line 109
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->name:Ljava/lang/String;

    .line 110
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->macAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->macAddress:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->batteryLevel:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->batteryLevel:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public batteryLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    .locals 0
    .param p1, "batteryLevel"    # Ljava/lang/Integer;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->batteryLevel:Ljava/lang/Integer;

    .line 151
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/dial/DialStatusResponse;
    .locals 2

    .prologue
    .line 156
    new-instance v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/dial/DialStatusResponse;-><init>(Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;Lcom/navdy/service/library/events/dial/DialStatusResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->build()Lcom/navdy/service/library/events/dial/DialStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public isConnected(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    .locals 0
    .param p1, "isConnected"    # Ljava/lang/Boolean;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->isConnected:Ljava/lang/Boolean;

    .line 127
    return-object p0
.end method

.method public isPaired(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    .locals 0
    .param p1, "isPaired"    # Ljava/lang/Boolean;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->isPaired:Ljava/lang/Boolean;

    .line 119
    return-object p0
.end method

.method public macAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    .locals 0
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->macAddress:Ljava/lang/String;

    .line 143
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->name:Ljava/lang/String;

    .line 135
    return-object p0
.end method
