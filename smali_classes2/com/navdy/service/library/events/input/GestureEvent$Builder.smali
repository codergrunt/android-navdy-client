.class public final Lcom/navdy/service/library/events/input/GestureEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GestureEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/input/GestureEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/input/GestureEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public gesture:Lcom/navdy/service/library/events/input/Gesture;

.field public x:Ljava/lang/Integer;

.field public y:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/input/GestureEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/input/GestureEvent;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 72
    if-nez p1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    iput-object v0, p0, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 74
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->x:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->x:Ljava/lang/Integer;

    .line 75
    iget-object v0, p1, Lcom/navdy/service/library/events/input/GestureEvent;->y:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->y:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/input/GestureEvent;
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->checkRequiredFields()V

    .line 96
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/GestureEvent$Builder;Lcom/navdy/service/library/events/input/GestureEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->build()Lcom/navdy/service/library/events/input/GestureEvent;

    move-result-object v0

    return-object v0
.end method

.method public gesture(Lcom/navdy/service/library/events/input/Gesture;)Lcom/navdy/service/library/events/input/GestureEvent$Builder;
    .locals 0
    .param p1, "gesture"    # Lcom/navdy/service/library/events/input/Gesture;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->gesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 80
    return-object p0
.end method

.method public x(Ljava/lang/Integer;)Lcom/navdy/service/library/events/input/GestureEvent$Builder;
    .locals 0
    .param p1, "x"    # Ljava/lang/Integer;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->x:Ljava/lang/Integer;

    .line 85
    return-object p0
.end method

.method public y(Ljava/lang/Integer;)Lcom/navdy/service/library/events/input/GestureEvent$Builder;
    .locals 0
    .param p1, "y"    # Ljava/lang/Integer;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/service/library/events/input/GestureEvent$Builder;->y:Ljava/lang/Integer;

    .line 90
    return-object p0
.end method
