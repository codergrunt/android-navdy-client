.class public final Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccelerateShutdown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;",
        ">;"
    }
.end annotation


# instance fields
.field public reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 54
    if-nez p1, :cond_0

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    iput-object v0, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;-><init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->build()Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    move-result-object v0

    return-object v0
.end method

.method public reason(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;)Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;
    .locals 0
    .param p1, "reason"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .line 60
    return-object p0
.end method
