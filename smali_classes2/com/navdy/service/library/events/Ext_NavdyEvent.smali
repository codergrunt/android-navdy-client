.class public final Lcom/navdy/service/library/events/Ext_NavdyEvent;
.super Ljava/lang/Object;
.source "Ext_NavdyEvent.java"


# static fields
.field public static final accelerateShutdown:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;",
            ">;"
        }
    .end annotation
.end field

.field public static final audioStatus:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/AudioStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoCompleteRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/AutoCompleteRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoCompleteResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/AutoCompleteResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final calendarEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/calendars/CalendarEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final calendarEventUpdates:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;",
            ">;"
        }
    .end annotation
.end field

.field public static final callEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/CallEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final callStateUpdateRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final cancelSpeechRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/CancelSpeechRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final cannedMessagesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/glances/CannedMessagesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final cannedMessagesUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final clearGlances:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/glances/ClearGlances;",
            ">;"
        }
    .end annotation
.end field

.field public static final connectionRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/connection/ConnectionRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final connectionStateChange:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/connection/ConnectionStateChange;",
            ">;"
        }
    .end annotation
.end field

.field public static final connectionStatus:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/connection/ConnectionStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final contactRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/contacts/ContactRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final contactResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/contacts/ContactResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final coordinate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field public static final dashboardPreferences:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/DashboardPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public static final dateTimeConfiguration:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/settings/DateTimeConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public static final destinationSelectedRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/DestinationSelectedRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final destinationSelectedResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/DestinationSelectedResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final deviceInfo:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final dialBondRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/dial/DialBondRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final dialBondResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/dial/DialBondResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final dialSimulationEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/input/DialSimulationEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final dialStatusRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/dial/DialStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final dialStatusResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/dial/DialStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final disconnectRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/connection/DisconnectRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final dismissScreen:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/ui/DismissScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final displaySpeakerPreferencesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final displaySpeakerPreferencesUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final driveRecordingsRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final driveRecordingsResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final driverPreferencesUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final driverProfilePreferencesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final favoriteContactsRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final favoriteContactsResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final favoriteDestinationUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final favoriteDestinationsRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/FavoriteDestinationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final fileListRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/FileListRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final fileListResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/FileListResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final fileTransferData:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/FileTransferData;",
            ">;"
        }
    .end annotation
.end field

.field public static final fileTransferRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/FileTransferRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final fileTransferResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/FileTransferResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final fileTransferStatus:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/FileTransferStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final gestureEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/input/GestureEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final getNavigationSessionState:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;",
            ">;"
        }
    .end annotation
.end field

.field public static final glanceEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/glances/GlanceEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final inputPreferencesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final inputPreferencesUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final launchAppEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/input/LaunchAppEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final linkPropertiesChanged:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;",
            ">;"
        }
    .end annotation
.end field

.field public static final mediaRemoteKeyEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicArtworkRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicArtworkRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicArtworkResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicArtworkResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicCapabilitiesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicCapabilitiesResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicCollectionRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicCollectionRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicCollectionResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicCollectionSourceUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicTrackInfo:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicTrackInfoRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfoRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationManeuverEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationPreferencesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationPreferencesUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationRouteCancelRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationRouteRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationRouteResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationRouteStatus:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationSessionDeferRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationSessionRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationSessionResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationSessionRouteChange:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;",
            ">;"
        }
    .end annotation
.end field

.field public static final navigationSessionStatusEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final networkLinkReady:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/connection/NetworkLinkReady;",
            ">;"
        }
    .end annotation
.end field

.field public static final networkStateChange:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/settings/NetworkStateChange;",
            ">;"
        }
    .end annotation
.end field

.field public static final notification:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/notification/NotificationEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationListRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/notification/NotificationListRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationListResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/notification/NotificationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationPreferencesUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationsPreferencesRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationsStatusRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final notificationsStatusUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final nowPlayingUpdateRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/NowPlayingUpdateRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final obdStatusRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/obd/ObdStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final obdStatusResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/obd/ObdStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final phoneBatteryStatus:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final phoneEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/PhoneEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final phoneStatusRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final phoneStatusResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final photoRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/photo/PhotoRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final photoResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/photo/PhotoResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final photoUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/photo/PhotoUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final photoUpdateQuery:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;",
            ">;"
        }
    .end annotation
.end field

.field public static final photoUpdateQueryResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final photoUpdateRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final ping:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/Ping;",
            ">;"
        }
    .end annotation
.end field

.field public static final placeTypeSearchRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final placeTypeSearchResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final placesSearchRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/PlacesSearchRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final placesSearchResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/PlacesSearchResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final previewFileRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/PreviewFileRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final previewFileResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/file/PreviewFileResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final readSettingsRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/settings/ReadSettingsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final readSettingsResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/settings/ReadSettingsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendedDestinationsRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/destination/RecommendedDestinationsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendedDestinationsUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final resumeMusicRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/ResumeMusicRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final routeManeuverRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final routeManeuverResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final showCustomNotification:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/notification/ShowCustomNotification;",
            ">;"
        }
    .end annotation
.end field

.field public static final showScreen:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/ui/ShowScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final smsMessageRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/messaging/SmsMessageRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final smsMessageResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/messaging/SmsMessageResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final speechRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/SpeechRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final speechRequestStatus:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/SpeechRequestStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final startDrivePlaybackEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final startDrivePlaybackResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final startDriveRecordingEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final startDriveRecordingResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final stopDrivePlaybackEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final stopDriveRecordingEvent:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final stopDriveRecordingResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final suggestedDestination:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/places/SuggestedDestination;",
            ">;"
        }
    .end annotation
.end field

.field public static final telephonyRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final telephonyResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final transmitLocation:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/location/TransmitLocation;",
            ">;"
        }
    .end annotation
.end field

.field public static final tripUpdate:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/TripUpdate;",
            ">;"
        }
    .end annotation
.end field

.field public static final tripUpdateAck:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/location/TripUpdateAck;",
            ">;"
        }
    .end annotation
.end field

.field public static final updateSettings:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/settings/UpdateSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final voiceAssistRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/VoiceAssistRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final voiceAssistResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/VoiceAssistResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final voiceSearchRequest:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/VoiceSearchRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final voiceSearchResponse:Lcom/squareup/wire/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 144
    const-class v0, Lcom/navdy/service/library/events/location/Coordinate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 145
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.coordinate"

    .line 146
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x65

    .line 147
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->coordinate:Lcom/squareup/wire/Extension;

    .line 149
    const-class v0, Lcom/navdy/service/library/events/Notification;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 150
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notification"

    .line 151
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x66

    .line 152
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notification:Lcom/squareup/wire/Extension;

    .line 154
    const-class v0, Lcom/navdy/service/library/events/callcontrol/CallEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 155
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.callEvent"

    .line 156
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x67

    .line 157
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->callEvent:Lcom/squareup/wire/Extension;

    .line 159
    const-class v0, Lcom/navdy/service/library/events/file/FileListRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 160
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.fileListRequest"

    .line 161
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x68

    .line 162
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->fileListRequest:Lcom/squareup/wire/Extension;

    .line 164
    const-class v0, Lcom/navdy/service/library/events/file/FileListResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 165
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.fileListResponse"

    .line 166
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x69

    .line 167
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->fileListResponse:Lcom/squareup/wire/Extension;

    .line 169
    const-class v0, Lcom/navdy/service/library/events/file/PreviewFileRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 170
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.previewFileRequest"

    .line 171
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x6a

    .line 172
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->previewFileRequest:Lcom/squareup/wire/Extension;

    .line 174
    const-class v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 175
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.previewFileResponse"

    .line 176
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x6b

    .line 177
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->previewFileResponse:Lcom/squareup/wire/Extension;

    .line 179
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 180
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationManeuverEvent"

    .line 181
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x6c

    .line 182
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationManeuverEvent:Lcom/squareup/wire/Extension;

    .line 184
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 185
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationRouteRequest"

    .line 186
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x6d

    .line 187
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationRouteRequest:Lcom/squareup/wire/Extension;

    .line 189
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 190
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationRouteResponse"

    .line 191
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x6e

    .line 192
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationRouteResponse:Lcom/squareup/wire/Extension;

    .line 194
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 195
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationSessionRequest"

    .line 196
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x6f

    .line 197
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationSessionRequest:Lcom/squareup/wire/Extension;

    .line 199
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 200
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationSessionResponse"

    .line 201
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x70

    .line 202
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationSessionResponse:Lcom/squareup/wire/Extension;

    .line 204
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 205
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationSessionStatusEvent"

    .line 206
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x71

    .line 207
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationSessionStatusEvent:Lcom/squareup/wire/Extension;

    .line 209
    const-class v0, Lcom/navdy/service/library/events/ui/DismissScreen;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 210
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dismissScreen"

    .line 211
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x72

    .line 212
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dismissScreen:Lcom/squareup/wire/Extension;

    .line 214
    const-class v0, Lcom/navdy/service/library/events/ui/ShowScreen;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 215
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.showScreen"

    .line 216
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x73

    .line 217
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->showScreen:Lcom/squareup/wire/Extension;

    .line 219
    const-class v0, Lcom/navdy/service/library/events/input/GestureEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 220
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.gestureEvent"

    .line 221
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x74

    .line 222
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->gestureEvent:Lcom/squareup/wire/Extension;

    .line 224
    const-class v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 225
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.placesSearchRequest"

    .line 226
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x75

    .line 227
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->placesSearchRequest:Lcom/squareup/wire/Extension;

    .line 229
    const-class v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 230
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.placesSearchResponse"

    .line 231
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x76

    .line 232
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 233
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->placesSearchResponse:Lcom/squareup/wire/Extension;

    .line 234
    const-class v0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 235
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.readSettingsRequest"

    .line 236
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x77

    .line 237
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->readSettingsRequest:Lcom/squareup/wire/Extension;

    .line 239
    const-class v0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 240
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.readSettingsResponse"

    .line 241
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x78

    .line 242
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->readSettingsResponse:Lcom/squareup/wire/Extension;

    .line 244
    const-class v0, Lcom/navdy/service/library/events/settings/UpdateSettings;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 245
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.updateSettings"

    .line 246
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x79

    .line 247
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 248
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->updateSettings:Lcom/squareup/wire/Extension;

    .line 249
    const-class v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 250
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.connectionStateChange"

    .line 251
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x7a

    .line 252
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 253
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->connectionStateChange:Lcom/squareup/wire/Extension;

    .line 254
    const-class v0, Lcom/navdy/service/library/events/audio/SpeechRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 255
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.speechRequest"

    .line 256
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x7b

    .line 257
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->speechRequest:Lcom/squareup/wire/Extension;

    .line 259
    const-class v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 260
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.telephonyRequest"

    .line 261
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x7c

    .line 262
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->telephonyRequest:Lcom/squareup/wire/Extension;

    .line 264
    const-class v0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 265
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.telephonyResponse"

    .line 266
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x7d

    .line 267
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->telephonyResponse:Lcom/squareup/wire/Extension;

    .line 269
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 270
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationEvent"

    .line 271
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x7e

    .line 272
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationEvent:Lcom/squareup/wire/Extension;

    .line 274
    const-class v0, Lcom/navdy/service/library/events/DeviceInfo;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 275
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.deviceInfo"

    .line 276
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x7f

    .line 277
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->deviceInfo:Lcom/squareup/wire/Extension;

    .line 279
    const-class v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 280
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.phoneEvent"

    .line 281
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x80

    .line 282
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->phoneEvent:Lcom/squareup/wire/Extension;

    .line 284
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 285
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.voiceAssistRequest"

    .line 286
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x81

    .line 287
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 288
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->voiceAssistRequest:Lcom/squareup/wire/Extension;

    .line 289
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 290
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.photoRequest"

    .line 291
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x82

    .line 292
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 293
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoRequest:Lcom/squareup/wire/Extension;

    .line 294
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 295
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.photoResponse"

    .line 296
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x83

    .line 297
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 298
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoResponse:Lcom/squareup/wire/Extension;

    .line 299
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationListRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 300
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationListRequest"

    .line 301
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x84

    .line 302
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationListRequest:Lcom/squareup/wire/Extension;

    .line 304
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationListResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 305
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationListResponse"

    .line 306
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x85

    .line 307
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationListResponse:Lcom/squareup/wire/Extension;

    .line 309
    const-class v0, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 310
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.showCustomNotification"

    .line 311
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x86

    .line 312
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->showCustomNotification:Lcom/squareup/wire/Extension;

    .line 314
    const-class v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 315
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dateTimeConfiguration"

    .line 316
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x87

    .line 317
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dateTimeConfiguration:Lcom/squareup/wire/Extension;

    .line 319
    const-class v0, Lcom/navdy/service/library/events/file/FileTransferResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 320
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.fileTransferResponse"

    .line 321
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x88

    .line 322
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->fileTransferResponse:Lcom/squareup/wire/Extension;

    .line 324
    const-class v0, Lcom/navdy/service/library/events/file/FileTransferRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 325
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.fileTransferRequest"

    .line 326
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x89

    .line 327
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 328
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->fileTransferRequest:Lcom/squareup/wire/Extension;

    .line 329
    const-class v0, Lcom/navdy/service/library/events/file/FileTransferData;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 330
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.fileTransferData"

    .line 331
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x8a

    .line 332
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 333
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->fileTransferData:Lcom/squareup/wire/Extension;

    .line 334
    const-class v0, Lcom/navdy/service/library/events/file/FileTransferStatus;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 335
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.fileTransferStatus"

    .line 336
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x8b

    .line 337
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 338
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->fileTransferStatus:Lcom/squareup/wire/Extension;

    .line 339
    const-class v0, Lcom/navdy/service/library/events/Ping;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 340
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.ping"

    .line 341
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x8c

    .line 342
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 343
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->ping:Lcom/squareup/wire/Extension;

    .line 344
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationsStatusUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 345
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationsStatusUpdate"

    .line 346
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x8d

    .line 347
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationsStatusUpdate:Lcom/squareup/wire/Extension;

    .line 349
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 350
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationsStatusRequest"

    .line 351
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x8e

    .line 352
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 353
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationsStatusRequest:Lcom/squareup/wire/Extension;

    .line 354
    const-class v0, Lcom/navdy/service/library/events/audio/MusicTrackInfoRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 355
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicTrackInfoRequest"

    .line 356
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x8f

    .line 357
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 358
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicTrackInfoRequest:Lcom/squareup/wire/Extension;

    .line 359
    const-class v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 360
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicTrackInfo"

    .line 361
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x90

    .line 362
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicTrackInfo:Lcom/squareup/wire/Extension;

    .line 364
    const-class v0, Lcom/navdy/service/library/events/audio/MusicEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 365
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicEvent"

    .line 366
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x91

    .line 367
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 368
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicEvent:Lcom/squareup/wire/Extension;

    .line 369
    const-class v0, Lcom/navdy/service/library/events/dial/DialStatusRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 370
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dialStatusRequest"

    .line 371
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x92

    .line 372
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dialStatusRequest:Lcom/squareup/wire/Extension;

    .line 374
    const-class v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 375
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dialStatusResponse"

    .line 376
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x93

    .line 377
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 378
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dialStatusResponse:Lcom/squareup/wire/Extension;

    .line 379
    const-class v0, Lcom/navdy/service/library/events/dial/DialBondRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 380
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dialBondRequest"

    .line 381
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x94

    .line 382
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 383
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dialBondRequest:Lcom/squareup/wire/Extension;

    .line 384
    const-class v0, Lcom/navdy/service/library/events/dial/DialBondResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 385
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dialBondResponse"

    .line 386
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x95

    .line 387
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 388
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dialBondResponse:Lcom/squareup/wire/Extension;

    .line 389
    const-class v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 390
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.driverProfilePreferencesRequest"

    .line 391
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x96

    .line 392
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->driverProfilePreferencesRequest:Lcom/squareup/wire/Extension;

    .line 394
    const-class v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 395
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.driverPreferencesUpdate"

    .line 396
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x97

    .line 397
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->driverPreferencesUpdate:Lcom/squareup/wire/Extension;

    .line 399
    const-class v0, Lcom/navdy/service/library/events/location/TransmitLocation;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 400
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.transmitLocation"

    .line 401
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x98

    .line 402
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->transmitLocation:Lcom/squareup/wire/Extension;

    .line 404
    const-class v0, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 405
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.disconnectRequest"

    .line 406
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x99

    .line 407
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 408
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->disconnectRequest:Lcom/squareup/wire/Extension;

    .line 409
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 410
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.voiceAssistResponse"

    .line 411
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x9a

    .line 412
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 413
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->voiceAssistResponse:Lcom/squareup/wire/Extension;

    .line 414
    const-class v0, Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 415
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.getNavigationSessionState"

    .line 416
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x9b

    .line 417
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 418
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->getNavigationSessionState:Lcom/squareup/wire/Extension;

    .line 419
    const-class v0, Lcom/navdy/service/library/events/navigation/RouteManeuverRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 420
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.routeManeuverRequest"

    .line 421
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x9c

    .line 422
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 423
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->routeManeuverRequest:Lcom/squareup/wire/Extension;

    .line 424
    const-class v0, Lcom/navdy/service/library/events/navigation/RouteManeuverResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 425
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.routeManeuverResponse"

    .line 426
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x9d

    .line 427
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->routeManeuverResponse:Lcom/squareup/wire/Extension;

    .line 429
    const-class v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 430
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationPreferencesRequest"

    .line 431
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x9e

    .line 432
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationPreferencesRequest:Lcom/squareup/wire/Extension;

    .line 434
    const-class v0, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 435
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationPreferencesUpdate"

    .line 436
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0x9f

    .line 437
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 438
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationPreferencesUpdate:Lcom/squareup/wire/Extension;

    .line 439
    const-class v0, Lcom/navdy/service/library/events/input/DialSimulationEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 440
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dialSimulationEvent"

    .line 441
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa0

    .line 442
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 443
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dialSimulationEvent:Lcom/squareup/wire/Extension;

    .line 444
    const-class v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 445
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.mediaRemoteKeyEvent"

    .line 446
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa1

    .line 447
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->mediaRemoteKeyEvent:Lcom/squareup/wire/Extension;

    .line 449
    const-class v0, Lcom/navdy/service/library/events/contacts/ContactRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 450
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.contactRequest"

    .line 451
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa2

    .line 452
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 453
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->contactRequest:Lcom/squareup/wire/Extension;

    .line 454
    const-class v0, Lcom/navdy/service/library/events/contacts/ContactResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 455
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.contactResponse"

    .line 456
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa3

    .line 457
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 458
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->contactResponse:Lcom/squareup/wire/Extension;

    .line 459
    const-class v0, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 460
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.favoriteContactsRequest"

    .line 461
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa4

    .line 462
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 463
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->favoriteContactsRequest:Lcom/squareup/wire/Extension;

    .line 464
    const-class v0, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 465
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.favoriteContactsResponse"

    .line 466
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa5

    .line 467
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->favoriteContactsResponse:Lcom/squareup/wire/Extension;

    .line 469
    const-class v0, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 470
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.favoriteDestinationsRequest"

    .line 471
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa6

    .line 472
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->favoriteDestinationsRequest:Lcom/squareup/wire/Extension;

    .line 474
    const-class v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 475
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.favoriteDestinationUpdate"

    .line 476
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa7

    .line 477
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 478
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->favoriteDestinationUpdate:Lcom/squareup/wire/Extension;

    .line 479
    const-class v0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 480
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.recommendedDestinationsRequest"

    .line 481
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa8

    .line 482
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->recommendedDestinationsRequest:Lcom/squareup/wire/Extension;

    .line 484
    const-class v0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 485
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.recommendedDestinationsUpdate"

    .line 486
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xa9

    .line 487
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 488
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->recommendedDestinationsUpdate:Lcom/squareup/wire/Extension;

    .line 489
    const-class v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 490
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.connectionRequest"

    .line 491
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xaa

    .line 492
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 493
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->connectionRequest:Lcom/squareup/wire/Extension;

    .line 494
    const-class v0, Lcom/navdy/service/library/events/connection/ConnectionStatus;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 495
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.connectionStatus"

    .line 496
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xab

    .line 497
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 498
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->connectionStatus:Lcom/squareup/wire/Extension;

    .line 499
    const-class v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 500
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.phoneStatusRequest"

    .line 501
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xac

    .line 502
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 503
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->phoneStatusRequest:Lcom/squareup/wire/Extension;

    .line 504
    const-class v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 505
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.phoneStatusResponse"

    .line 506
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xad

    .line 507
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 508
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->phoneStatusResponse:Lcom/squareup/wire/Extension;

    .line 509
    const-class v0, Lcom/navdy/service/library/events/input/LaunchAppEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 510
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.launchAppEvent"

    .line 511
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xae

    .line 512
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 513
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->launchAppEvent:Lcom/squareup/wire/Extension;

    .line 514
    const-class v0, Lcom/navdy/service/library/events/preferences/InputPreferencesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 515
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.inputPreferencesRequest"

    .line 516
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xaf

    .line 517
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 518
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->inputPreferencesRequest:Lcom/squareup/wire/Extension;

    .line 519
    const-class v0, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 520
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.inputPreferencesUpdate"

    .line 521
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb0

    .line 522
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 523
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->inputPreferencesUpdate:Lcom/squareup/wire/Extension;

    .line 524
    const-class v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 525
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.phoneBatteryStatus"

    .line 526
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb1

    .line 527
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 528
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->phoneBatteryStatus:Lcom/squareup/wire/Extension;

    .line 529
    const-class v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 530
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.smsMessageRequest"

    .line 531
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb2

    .line 532
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 533
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->smsMessageRequest:Lcom/squareup/wire/Extension;

    .line 534
    const-class v0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 535
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.smsMessageResponse"

    .line 536
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb3

    .line 537
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 538
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->smsMessageResponse:Lcom/squareup/wire/Extension;

    .line 539
    const-class v0, Lcom/navdy/service/library/events/obd/ObdStatusRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 540
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.obdStatusRequest"

    .line 541
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb4

    .line 542
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 543
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->obdStatusRequest:Lcom/squareup/wire/Extension;

    .line 544
    const-class v0, Lcom/navdy/service/library/events/obd/ObdStatusResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 545
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.obdStatusResponse"

    .line 546
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb5

    .line 547
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 548
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->obdStatusResponse:Lcom/squareup/wire/Extension;

    .line 549
    const-class v0, Lcom/navdy/service/library/events/TripUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 550
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.tripUpdate"

    .line 551
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb6

    .line 552
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 553
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->tripUpdate:Lcom/squareup/wire/Extension;

    .line 554
    const-class v0, Lcom/navdy/service/library/events/location/TripUpdateAck;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 555
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.tripUpdateAck"

    .line 556
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb7

    .line 557
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 558
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->tripUpdateAck:Lcom/squareup/wire/Extension;

    .line 559
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 560
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.photoUpdateRequest"

    .line 561
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb8

    .line 562
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 563
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoUpdateRequest:Lcom/squareup/wire/Extension;

    .line 564
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 565
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.photoUpdate"

    .line 566
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xb9

    .line 567
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 568
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoUpdate:Lcom/squareup/wire/Extension;

    .line 569
    const-class v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 570
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.displaySpeakerPreferencesRequest"

    .line 571
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xba

    .line 572
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 573
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->displaySpeakerPreferencesRequest:Lcom/squareup/wire/Extension;

    .line 574
    const-class v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 575
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.displaySpeakerPreferencesUpdate"

    .line 576
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xbb

    .line 577
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 578
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->displaySpeakerPreferencesUpdate:Lcom/squareup/wire/Extension;

    .line 579
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 580
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationRouteStatus"

    .line 581
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xbc

    .line 582
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 583
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationRouteStatus:Lcom/squareup/wire/Extension;

    .line 584
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 585
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationRouteCancelRequest"

    .line 586
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xbd

    .line 587
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 588
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationRouteCancelRequest:Lcom/squareup/wire/Extension;

    .line 589
    const-class v0, Lcom/navdy/service/library/events/preferences/NotificationPreferencesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 590
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationsPreferencesRequest"

    .line 591
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xbe

    .line 592
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 593
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationsPreferencesRequest:Lcom/squareup/wire/Extension;

    .line 594
    const-class v0, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 595
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.notificationPreferencesUpdate"

    .line 596
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xbf

    .line 597
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 598
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->notificationPreferencesUpdate:Lcom/squareup/wire/Extension;

    .line 599
    const-class v0, Lcom/navdy/service/library/events/debug/StartDriveRecordingEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 600
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.startDriveRecordingEvent"

    .line 601
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc0

    .line 602
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 603
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->startDriveRecordingEvent:Lcom/squareup/wire/Extension;

    .line 604
    const-class v0, Lcom/navdy/service/library/events/debug/StopDriveRecordingEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 605
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.stopDriveRecordingEvent"

    .line 606
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc1

    .line 607
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 608
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->stopDriveRecordingEvent:Lcom/squareup/wire/Extension;

    .line 609
    const-class v0, Lcom/navdy/service/library/events/debug/DriveRecordingsRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 610
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.driveRecordingsRequest"

    .line 611
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc2

    .line 612
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 613
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->driveRecordingsRequest:Lcom/squareup/wire/Extension;

    .line 614
    const-class v0, Lcom/navdy/service/library/events/debug/DriveRecordingsResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 615
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.driveRecordingsResponse"

    .line 616
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc3

    .line 617
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 618
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->driveRecordingsResponse:Lcom/squareup/wire/Extension;

    .line 619
    const-class v0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 620
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.startDrivePlaybackEvent"

    .line 621
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc4

    .line 622
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 623
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->startDrivePlaybackEvent:Lcom/squareup/wire/Extension;

    .line 624
    const-class v0, Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 625
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.stopDrivePlaybackEvent"

    .line 626
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc5

    .line 627
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 628
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->stopDrivePlaybackEvent:Lcom/squareup/wire/Extension;

    .line 629
    const-class v0, Lcom/navdy/service/library/events/debug/StartDriveRecordingResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 630
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.startDriveRecordingResponse"

    .line 631
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc6

    .line 632
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 633
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->startDriveRecordingResponse:Lcom/squareup/wire/Extension;

    .line 634
    const-class v0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 635
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.startDrivePlaybackResponse"

    .line 636
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc7

    .line 637
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 638
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->startDrivePlaybackResponse:Lcom/squareup/wire/Extension;

    .line 639
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 640
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationSessionRouteChange"

    .line 641
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc8

    .line 642
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 643
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationSessionRouteChange:Lcom/squareup/wire/Extension;

    .line 644
    const-class v0, Lcom/navdy/service/library/events/settings/NetworkStateChange;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 645
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.networkStateChange"

    .line 646
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xc9

    .line 647
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 648
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->networkStateChange:Lcom/squareup/wire/Extension;

    .line 649
    const-class v0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 650
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.stopDriveRecordingResponse"

    .line 651
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xca

    .line 652
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 653
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->stopDriveRecordingResponse:Lcom/squareup/wire/Extension;

    .line 654
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 655
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.navigationSessionDeferRequest"

    .line 656
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xcb

    .line 657
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 658
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->navigationSessionDeferRequest:Lcom/squareup/wire/Extension;

    .line 659
    const-class v0, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 660
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.callStateUpdateRequest"

    .line 661
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xcc

    .line 662
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 663
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->callStateUpdateRequest:Lcom/squareup/wire/Extension;

    .line 664
    const-class v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 665
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.autoCompleteRequest"

    .line 666
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xcd

    .line 667
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 668
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->autoCompleteRequest:Lcom/squareup/wire/Extension;

    .line 669
    const-class v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 670
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.autoCompleteResponse"

    .line 671
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xce

    .line 672
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 673
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->autoCompleteResponse:Lcom/squareup/wire/Extension;

    .line 674
    const-class v0, Lcom/navdy/service/library/events/glances/GlanceEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 675
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.glanceEvent"

    .line 676
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xcf

    .line 677
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 678
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->glanceEvent:Lcom/squareup/wire/Extension;

    .line 679
    const-class v0, Lcom/navdy/service/library/events/audio/NowPlayingUpdateRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 680
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.nowPlayingUpdateRequest"

    .line 681
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd0

    .line 682
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 683
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->nowPlayingUpdateRequest:Lcom/squareup/wire/Extension;

    .line 684
    const-class v0, Lcom/navdy/service/library/events/audio/CancelSpeechRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 685
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.cancelSpeechRequest"

    .line 686
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd1

    .line 687
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 688
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->cancelSpeechRequest:Lcom/squareup/wire/Extension;

    .line 689
    const-class v0, Lcom/navdy/service/library/events/connection/NetworkLinkReady;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 690
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.networkLinkReady"

    .line 691
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd2

    .line 692
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 693
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->networkLinkReady:Lcom/squareup/wire/Extension;

    .line 694
    const-class v0, Lcom/navdy/service/library/events/places/SuggestedDestination;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 695
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.suggestedDestination"

    .line 696
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd3

    .line 697
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 698
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->suggestedDestination:Lcom/squareup/wire/Extension;

    .line 699
    const-class v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 700
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.calendarEvent"

    .line 701
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd4

    .line 702
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 703
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->calendarEvent:Lcom/squareup/wire/Extension;

    .line 704
    const-class v0, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 705
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.calendarEventUpdates"

    .line 706
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd5

    .line 707
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 708
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->calendarEventUpdates:Lcom/squareup/wire/Extension;

    .line 709
    const-class v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 710
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.speechRequestStatus"

    .line 711
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd6

    .line 712
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 713
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->speechRequestStatus:Lcom/squareup/wire/Extension;

    .line 714
    const-class v0, Lcom/navdy/service/library/events/audio/AudioStatus;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 715
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.audioStatus"

    .line 716
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd7

    .line 717
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 718
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->audioStatus:Lcom/squareup/wire/Extension;

    .line 719
    const-class v0, Lcom/navdy/service/library/events/connection/LinkPropertiesChanged;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 720
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.linkPropertiesChanged"

    .line 721
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd8

    .line 722
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 723
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->linkPropertiesChanged:Lcom/squareup/wire/Extension;

    .line 724
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceSearchRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 725
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.voiceSearchRequest"

    .line 726
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xd9

    .line 727
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 728
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->voiceSearchRequest:Lcom/squareup/wire/Extension;

    .line 729
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 730
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.voiceSearchResponse"

    .line 731
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xda

    .line 732
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 733
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->voiceSearchResponse:Lcom/squareup/wire/Extension;

    .line 734
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 735
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicCollectionRequest"

    .line 736
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xdb

    .line 737
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 738
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicCollectionRequest:Lcom/squareup/wire/Extension;

    .line 739
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 740
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicCollectionResponse"

    .line 741
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xdc

    .line 742
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 743
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicCollectionResponse:Lcom/squareup/wire/Extension;

    .line 744
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 745
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicCapabilitiesRequest"

    .line 746
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xdd

    .line 747
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 748
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicCapabilitiesRequest:Lcom/squareup/wire/Extension;

    .line 749
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 750
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicCapabilitiesResponse"

    .line 751
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xde

    .line 752
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 753
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicCapabilitiesResponse:Lcom/squareup/wire/Extension;

    .line 754
    const-class v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 755
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicArtworkRequest"

    .line 756
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xdf

    .line 757
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 758
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicArtworkRequest:Lcom/squareup/wire/Extension;

    .line 759
    const-class v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 760
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicArtworkResponse"

    .line 761
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe0

    .line 762
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 763
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicArtworkResponse:Lcom/squareup/wire/Extension;

    .line 764
    const-class v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 765
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.dashboardPreferences"

    .line 766
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe1

    .line 767
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 768
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->dashboardPreferences:Lcom/squareup/wire/Extension;

    .line 769
    const-class v0, Lcom/navdy/service/library/events/glances/ClearGlances;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 770
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.clearGlances"

    .line 771
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe2

    .line 772
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 773
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->clearGlances:Lcom/squareup/wire/Extension;

    .line 774
    const-class v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 775
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.placeTypeSearchRequest"

    .line 776
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe3

    .line 777
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 778
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->placeTypeSearchRequest:Lcom/squareup/wire/Extension;

    .line 779
    const-class v0, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 780
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.placeTypeSearchResponse"

    .line 781
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe4

    .line 782
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 783
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->placeTypeSearchResponse:Lcom/squareup/wire/Extension;

    .line 784
    const-class v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 785
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.accelerateShutdown"

    .line 786
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe5

    .line 787
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 788
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->accelerateShutdown:Lcom/squareup/wire/Extension;

    .line 789
    const-class v0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 790
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.destinationSelectedRequest"

    .line 791
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe6

    .line 792
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 793
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->destinationSelectedRequest:Lcom/squareup/wire/Extension;

    .line 794
    const-class v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 795
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.destinationSelectedResponse"

    .line 796
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe7

    .line 797
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 798
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->destinationSelectedResponse:Lcom/squareup/wire/Extension;

    .line 799
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 800
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.photoUpdateQuery"

    .line 801
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe8

    .line 802
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 803
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoUpdateQuery:Lcom/squareup/wire/Extension;

    .line 804
    const-class v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 805
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.photoUpdateQueryResponse"

    .line 806
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xe9

    .line 807
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 808
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->photoUpdateQueryResponse:Lcom/squareup/wire/Extension;

    .line 809
    const-class v0, Lcom/navdy/service/library/events/audio/ResumeMusicRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 810
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.resumeMusicRequest"

    .line 811
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xea

    .line 812
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 813
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->resumeMusicRequest:Lcom/squareup/wire/Extension;

    .line 814
    const-class v0, Lcom/navdy/service/library/events/glances/CannedMessagesRequest;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 815
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.cannedMessagesRequest"

    .line 816
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xeb

    .line 817
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 818
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->cannedMessagesRequest:Lcom/squareup/wire/Extension;

    .line 819
    const-class v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 820
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.cannedMessagesUpdate"

    .line 821
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xec

    .line 822
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 823
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->cannedMessagesUpdate:Lcom/squareup/wire/Extension;

    .line 824
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;

    const-class v1, Lcom/navdy/service/library/events/NavdyEvent;

    .line 825
    invoke-static {v0, v1}, Lcom/squareup/wire/Extension;->messageExtending(Ljava/lang/Class;Ljava/lang/Class;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const-string v1, "null.musicCollectionSourceUpdate"

    .line 826
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setName(Ljava/lang/String;)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    const/16 v1, 0xed

    .line 827
    invoke-virtual {v0, v1}, Lcom/squareup/wire/Extension$Builder;->setTag(I)Lcom/squareup/wire/Extension$Builder;

    move-result-object v0

    .line 828
    invoke-virtual {v0}, Lcom/squareup/wire/Extension$Builder;->buildOptional()Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Ext_NavdyEvent;->musicCollectionSourceUpdate:Lcom/squareup/wire/Extension;

    .line 824
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    return-void
.end method
