.class public final enum Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
.super Ljava/lang/Enum;
.source "GlanceEvent.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/glances/GlanceEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GlanceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final enum GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final enum GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final enum GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final enum GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final enum GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public static final enum GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 191
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "GLANCE_TYPE_CALENDAR"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 192
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "GLANCE_TYPE_EMAIL"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 193
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "GLANCE_TYPE_MESSAGE"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 194
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "GLANCE_TYPE_SOCIAL"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 195
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "GLANCE_TYPE_GENERIC"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 196
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    const-string v1, "GLANCE_TYPE_FUEL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 189
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_CALENDAR:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_GENERIC:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_FUEL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->$VALUES:[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 201
    iput p3, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->value:I

    .line 202
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 189
    const-class v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->$VALUES:[Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->value:I

    return v0
.end method
