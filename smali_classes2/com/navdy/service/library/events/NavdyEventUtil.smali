.class public Lcom/navdy/service/library/events/NavdyEventUtil;
.super Ljava/lang/Object;
.source "NavdyEventUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;
    }
.end annotation


# static fields
.field private static final EXTENSION_BASE:I = 0x65

.field private static classToExtensionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class;",
            "Lcom/squareup/wire/Extension;",
            ">;"
        }
    .end annotation
.end field

.field private static minTag:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static tagToExtensionMap:[Lcom/squareup/wire/Extension;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/events/NavdyEventUtil;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEventUtil;->classToExtensionMap:Ljava/util/HashMap;

    .line 20
    invoke-static {}, Lcom/navdy/service/library/events/NavdyEventUtil;->cacheExtensions()[Lcom/squareup/wire/Extension;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/NavdyEventUtil;->tagToExtensionMap:[Lcom/squareup/wire/Extension;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static applyDefaults(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "message":Lcom/squareup/wire/Message;, "TT;"
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 109
    .local v0, "klass":Ljava/lang/Class;
    invoke-static {v0}, Lcom/navdy/service/library/events/NavdyEventUtil;->getDefault(Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v1

    .line 111
    .local v1, "messageWithDefaults":Lcom/squareup/wire/Message;
    invoke-static {p0, v1}, Lcom/navdy/service/library/events/NavdyEventUtil;->merge(Lcom/squareup/wire/Message;Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v2

    return-object v2
.end method

.method private static cacheExtensions()[Lcom/squareup/wire/Extension;
    .locals 14

    .prologue
    .line 24
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/squareup/wire/Extension;>;"
    const v8, 0x7fffffff

    sput v8, Lcom/navdy/service/library/events/NavdyEventUtil;->minTag:I

    .line 27
    const/4 v4, -0x1

    .line 29
    .local v4, "maxTag":I
    const-class v8, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    invoke-virtual {v8}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v9

    array-length v10, v9

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v10, :cond_3

    aget-object v2, v9, v8

    .line 30
    .local v2, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v11

    const-class v12, Lcom/squareup/wire/Extension;

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 32
    const/4 v11, 0x0

    :try_start_0
    invoke-virtual {v2, v11}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wire/Extension;

    .line 33
    .local v1, "extension":Lcom/squareup/wire/Extension;
    sget-object v11, Lcom/navdy/service/library/events/NavdyEventUtil;->classToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/squareup/wire/Extension;->getMessageType()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v11, v12, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-virtual {v1}, Lcom/squareup/wire/Extension;->getTag()I

    move-result v7

    .line 35
    .local v7, "tag":I
    sget v11, Lcom/navdy/service/library/events/NavdyEventUtil;->minTag:I

    if-ge v7, v11, :cond_0

    .line 36
    sput v7, Lcom/navdy/service/library/events/NavdyEventUtil;->minTag:I

    .line 38
    :cond_0
    if-le v7, v4, :cond_1

    .line 39
    move v4, v7

    .line 41
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .end local v1    # "extension":Lcom/squareup/wire/Extension;
    .end local v7    # "tag":I
    :cond_2
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v11, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Skipping field "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 47
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v2    # "field":Ljava/lang/reflect/Field;
    :cond_3
    sget v8, Lcom/navdy/service/library/events/NavdyEventUtil;->minTag:I

    sub-int v8, v4, v8

    add-int/lit8 v6, v8, 0x1

    .line 48
    .local v6, "size":I
    new-array v5, v6, [Lcom/squareup/wire/Extension;

    .line 49
    .local v5, "result":[Lcom/squareup/wire/Extension;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wire/Extension;

    .line 50
    .restart local v1    # "extension":Lcom/squareup/wire/Extension;
    invoke-virtual {v1}, Lcom/squareup/wire/Extension;->getTag()I

    move-result v9

    sget v10, Lcom/navdy/service/library/events/NavdyEventUtil;->minTag:I

    sub-int/2addr v9, v10

    aput-object v1, v5, v9

    goto :goto_2

    .line 52
    .end local v1    # "extension":Lcom/squareup/wire/Extension;
    :cond_4
    return-object v5
.end method

.method public static eventFromMessage(Lcom/squareup/wire/Message;)Lcom/navdy/service/library/events/NavdyEvent;
    .locals 5
    .param p0, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 79
    invoke-static {p0}, Lcom/navdy/service/library/events/NavdyEventUtil;->findExtension(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Extension;

    move-result-object v0

    .line 80
    .local v0, "extension":Lcom/squareup/wire/Extension;
    if-nez v0, :cond_0

    .line 81
    sget-object v2, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not a valid NavdyEvent protobuf message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 82
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not a valid NavdyEvent protobuf message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 85
    :cond_0
    const-class v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v0}, Lcom/squareup/wire/Extension;->getTag()I

    move-result v3

    add-int/lit8 v3, v3, -0x64

    invoke-static {v2, v3}, Lcom/squareup/wire/Message;->enumFromInt(Ljava/lang/Class;I)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 86
    .local v1, "type":Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    new-instance v2, Lcom/navdy/service/library/events/NavdyEvent$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/NavdyEvent$Builder;-><init>()V

    .line 87
    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->type(Lcom/navdy/service/library/events/NavdyEvent$MessageType;)Lcom/navdy/service/library/events/NavdyEvent$Builder;

    move-result-object v2

    .line 88
    invoke-virtual {v2, v0, p0}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->setExtension(Lcom/squareup/wire/Extension;Ljava/lang/Object;)Lcom/navdy/service/library/events/NavdyEvent$Builder;

    move-result-object v2

    .line 89
    invoke-virtual {v2}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->build()Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v2

    return-object v2
.end method

.method private static findExtension(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Extension;
    .locals 4
    .param p0, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    const/4 v2, 0x0

    .line 63
    iget-object v3, p0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    if-eqz v3, :cond_0

    .line 64
    iget-object v3, p0, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ordinal()I

    move-result v3

    add-int/lit8 v1, v3, 0x65

    .line 65
    .local v1, "tag":I
    sget v3, Lcom/navdy/service/library/events/NavdyEventUtil;->minTag:I

    sub-int v0, v1, v3

    .line 66
    .local v0, "offset":I
    if-ltz v0, :cond_0

    sget-object v3, Lcom/navdy/service/library/events/NavdyEventUtil;->tagToExtensionMap:[Lcom/squareup/wire/Extension;

    array-length v3, v3

    if-lt v0, v3, :cond_1

    .line 71
    .end local v0    # "offset":I
    .end local v1    # "tag":I
    :cond_0
    :goto_0
    return-object v2

    .line 69
    .restart local v0    # "offset":I
    .restart local v1    # "tag":I
    :cond_1
    sget-object v2, Lcom/navdy/service/library/events/NavdyEventUtil;->tagToExtensionMap:[Lcom/squareup/wire/Extension;

    aget-object v2, v2, v0

    goto :goto_0
.end method

.method private static findExtension(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Extension;
    .locals 2
    .param p0, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 59
    sget-object v0, Lcom/navdy/service/library/events/NavdyEventUtil;->classToExtensionMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Extension;

    return-object v0
.end method

.method private static getBuilder(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/lang/Class;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 252
    .local p0, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    if-nez p0, :cond_0

    .line 253
    const/4 v0, 0x0

    .line 256
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "$Builder"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDefault(Ljava/lang/Class;)Lcom/squareup/wire/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/service/library/events/NavdyEventUtil;->getDefault(Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;

    move-result-object v0

    return-object v0
.end method

.method public static getDefault(Ljava/lang/Class;Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;)Lcom/squareup/wire/Message;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/navdy/service/library/events/NavdyEventUtil$Initializer",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p1, "initializer":Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;, "Lcom/navdy/service/library/events/NavdyEventUtil$Initializer<TT;>;"
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 128
    if-nez p0, :cond_0

    move-object v7, v8

    .line 186
    :goto_0
    return-object v7

    .line 133
    :cond_0
    const/4 v1, 0x0

    .line 134
    .local v1, "builderType":Ljava/lang/Class;
    const/4 v4, 0x0

    .line 136
    .local v4, "msgBuilder":Lcom/squareup/wire/Message$Builder;, "Lcom/squareup/wire/Message$Builder<TT;>;"
    :try_start_0
    invoke-static {p0}, Lcom/navdy/service/library/events/NavdyEventUtil;->getBuilder(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 137
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v1, v7}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v7

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v7, v10}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4

    .line 150
    :goto_1
    if-eqz v1, :cond_1

    if-nez v4, :cond_2

    :cond_1
    move-object v7, v8

    .line 151
    goto :goto_0

    .line 138
    :catch_0
    move-exception v2

    .line 139
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    sget-object v7, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 140
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v2

    .line 141
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v7, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 142
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v2

    .line 143
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    sget-object v7, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 144
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_3
    move-exception v2

    .line 145
    .local v2, "e":Ljava/lang/InstantiationException;
    sget-object v7, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 146
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_4
    move-exception v2

    .line 147
    .local v2, "e":Ljava/lang/IllegalAccessException;
    sget-object v7, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 157
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v8

    array-length v10, v8

    move v7, v9

    :goto_2
    if-ge v7, v10, :cond_6

    aget-object v3, v8, v7

    .line 160
    .local v3, "field":Ljava/lang/reflect/Field;
    const/4 v5, 0x0

    .line 163
    .local v5, "resultingValue":Ljava/lang/Object;
    :try_start_1
    const-class v9, Lcom/squareup/wire/Message;

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 164
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9}, Lcom/navdy/service/library/events/NavdyEventUtil;->getDefault(Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    .line 171
    .end local v5    # "resultingValue":Ljava/lang/Object;
    :cond_3
    :goto_3
    if-eqz v5, :cond_4

    .line 172
    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 157
    :cond_4
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 165
    .restart local v5    # "resultingValue":Ljava/lang/Object;
    :cond_5
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v9

    invoke-static {v9}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v9

    if-nez v9, :cond_3

    .line 166
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v11, "$"

    invoke-virtual {v9, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 167
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "DEFAULT_"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 168
    .local v6, "staticField":Ljava/lang/reflect/Field;
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v5

    goto :goto_3

    .line 175
    .end local v5    # "resultingValue":Ljava/lang/Object;
    .end local v6    # "staticField":Ljava/lang/reflect/Field;
    :catch_5
    move-exception v2

    .line 176
    .restart local v2    # "e":Ljava/lang/IllegalAccessException;
    sget-object v9, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 177
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_6
    move-exception v2

    .line 178
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    sget-object v9, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 182
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    .end local v3    # "field":Ljava/lang/reflect/Field;
    :cond_6
    if-eqz p1, :cond_7

    .line 183
    invoke-virtual {p1, v4}, Lcom/navdy/service/library/events/NavdyEventUtil$Initializer;->build(Lcom/squareup/wire/Message$Builder;)Lcom/squareup/wire/Message;

    move-result-object v7

    goto/16 :goto_0

    .line 186
    :cond_7
    invoke-virtual {v4}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v7

    goto/16 :goto_0
.end method

.method public static merge(Lcom/squareup/wire/Message;Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "msg":Lcom/squareup/wire/Message;, "TT;"
    .local p1, "defaultMsg":Lcom/squareup/wire/Message;, "TT;"
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 191
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v10, v11

    .line 247
    :goto_0
    return-object v10

    .line 196
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    .line 198
    .local v9, "type":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/squareup/wire/Message;>;"
    const/4 v7, 0x0

    .line 199
    .local v7, "msgBuilder":Lcom/squareup/wire/Message$Builder;, "Lcom/squareup/wire/Message$Builder<TT;>;"
    const/4 v3, 0x0

    .line 200
    .local v3, "defaultMsgBuilder":Lcom/squareup/wire/Message$Builder;, "Lcom/squareup/wire/Message$Builder<TT;>;"
    const/4 v1, 0x0

    .line 202
    .local v1, "builderType":Ljava/lang/Class;
    :try_start_0
    invoke-static {v9}, Lcom/navdy/service/library/events/NavdyEventUtil;->getBuilder(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    .line 203
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v13, 0x0

    aput-object v9, v10, v13

    invoke-virtual {v1, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v10

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p0, v13, v14

    invoke-virtual {v10, v13}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    move-object v7, v0

    .line 204
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v13, 0x0

    aput-object v9, v10, v13

    invoke-virtual {v1, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v10

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p1, v13, v14

    invoke-virtual {v10, v13}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    .line 217
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v7, :cond_2

    if-nez v3, :cond_3

    :cond_2
    move-object v10, v11

    .line 218
    goto :goto_0

    .line 205
    :catch_0
    move-exception v4

    .line 206
    .local v4, "e":Ljava/lang/InstantiationException;
    sget-object v10, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 207
    .end local v4    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v4

    .line 208
    .local v4, "e":Ljava/lang/IllegalAccessException;
    sget-object v10, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 209
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v4

    .line 210
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v10, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 211
    .end local v4    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_3
    move-exception v4

    .line 212
    .local v4, "e":Ljava/lang/NoSuchMethodException;
    sget-object v10, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 213
    .end local v4    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v4

    .line 214
    .local v4, "e":Ljava/lang/ClassNotFoundException;
    sget-object v10, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 224
    .end local v4    # "e":Ljava/lang/ClassNotFoundException;
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v11

    array-length v13, v11

    move v10, v12

    :goto_2
    if-ge v10, v13, :cond_7

    aget-object v5, v11, v10

    .line 227
    .local v5, "field":Ljava/lang/reflect/Field;
    :try_start_1
    invoke-virtual {v5, v7}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 228
    .local v6, "messageFieldValue":Ljava/lang/Object;
    invoke-virtual {v5, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 229
    .local v2, "defaultMessageFieldValue":Ljava/lang/Object;
    const/4 v8, 0x0

    .line 232
    .local v8, "resultingValue":Ljava/lang/Object;
    const-class v12, Lcom/squareup/wire/Message;

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 233
    check-cast v6, Lcom/squareup/wire/Message;

    .end local v6    # "messageFieldValue":Ljava/lang/Object;
    check-cast v2, Lcom/squareup/wire/Message;

    .end local v2    # "defaultMessageFieldValue":Ljava/lang/Object;
    invoke-static {v6, v2}, Lcom/navdy/service/library/events/NavdyEventUtil;->merge(Lcom/squareup/wire/Message;Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v8

    .line 238
    .end local v8    # "resultingValue":Ljava/lang/Object;
    :cond_4
    :goto_3
    if-eqz v8, :cond_5

    .line 239
    invoke-virtual {v5, v7, v8}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 224
    :cond_5
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 234
    .restart local v2    # "defaultMessageFieldValue":Ljava/lang/Object;
    .restart local v6    # "messageFieldValue":Ljava/lang/Object;
    .restart local v8    # "resultingValue":Ljava/lang/Object;
    :cond_6
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v12

    invoke-static {v12}, Ljava/lang/reflect/Modifier;->isFinal(I)Z

    move-result v12

    if-nez v12, :cond_4

    .line 235
    invoke-static {v6, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v8

    goto :goto_3

    .line 242
    .end local v2    # "defaultMessageFieldValue":Ljava/lang/Object;
    .end local v6    # "messageFieldValue":Ljava/lang/Object;
    .end local v8    # "resultingValue":Ljava/lang/Object;
    :catch_5
    move-exception v4

    .line 243
    .local v4, "e":Ljava/lang/IllegalAccessException;
    sget-object v12, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 247
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    .end local v5    # "field":Ljava/lang/reflect/Field;
    :cond_7
    invoke-virtual {v7}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object v10

    goto/16 :goto_0
.end method

.method public static messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;
    .locals 4
    .param p0, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 94
    invoke-static {p0}, Lcom/navdy/service/library/events/NavdyEventUtil;->findExtension(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Extension;

    move-result-object v0

    .line 95
    .local v0, "extension":Lcom/squareup/wire/Extension;
    if-nez v0, :cond_0

    .line 96
    sget-object v1, Lcom/navdy/service/library/events/NavdyEventUtil;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find extension for event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 97
    const/4 v1, 0x0

    .line 99
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/navdy/service/library/events/NavdyEvent;->getExtension(Lcom/squareup/wire/Extension;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wire/Message;

    goto :goto_0
.end method
