.class public abstract Lcom/navdy/client/debug/navigation/NavigationManager;
.super Ljava/lang/Object;
.source "NavigationManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;)Z
    .locals 7
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "streetAddress"    # Ljava/lang/String;
    .param p4, "display"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 18
    const/4 v4, 0x0

    sget-object v5, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v0

    return v0
.end method

.method public abstract startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;)Z
    .locals 8
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 22
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;)Z
    .locals 9
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p6, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 26
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Z)Z
    .locals 10
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p6, "requestId"    # Ljava/lang/String;
    .param p7, "useStreetAddress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 30
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZ)Z
    .locals 11
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p6, "requestId"    # Ljava/lang/String;
    .param p7, "useStreetAddress"    # Z
    .param p8, "initiatedOnHud"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "ZZ)Z"
        }
    .end annotation

    .prologue
    .line 34
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-virtual/range {v0 .. v10}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZ)Z

    move-result v0

    return v0
.end method

.method public startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z
    .locals 12
    .param p1, "coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p6, "requestId"    # Ljava/lang/String;
    .param p7, "useStreetAddress"    # Z
    .param p8, "initiatedOnHud"    # Z
    .param p9, "destination"    # Lcom/navdy/service/library/events/destination/Destination;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 38
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    const/4 v5, 0x0

    sget-object v6, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    invoke-virtual/range {v0 .. v11}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z

    move-result v0

    return v0
.end method

.method public abstract startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Z)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation
.end method

.method public abstract startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZ)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "ZZ)Z"
        }
    .end annotation
.end method

.method public abstract startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ")Z"
        }
    .end annotation
.end method
