.class public Lcom/navdy/client/debug/util/S3Constants;
.super Ljava/lang/Object;
.source "S3Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/util/S3Constants$BuildSource;,
        Lcom/navdy/client/debug/util/S3Constants$BuildType;
    }
.end annotation


# static fields
.field public static final BUCKET_NAME:Ljava/lang/String; = "gesture-data"

.field public static final DB_FILE:Ljava/lang/String; = "db.sqlite3"

.field public static final LOOKUP_FOLDER_PREFIX:Ljava/lang/String; = "lookup"

.field public static final NAVDY_BUILDS_S3_BUCKET:Ljava/lang/String; = "navdy-builds"

.field public static final NAVDY_PROD_RELEASE_S3_BUCKET:Ljava/lang/String; = "navdy-prod-release"

.field public static final OTA_LOOKUP_FILE_NAME:Ljava/lang/String; = "latest.json"

.field public static final OTA_RELEASE_NOTES_FILE_NAME_FORMAT:Ljava/lang/String; = "release_notes_%s.json"

.field public static final S3_FILE_DELIMITER:Ljava/lang/String; = "/"

.field public static final S3_OTA_BETA_PREFIX:Ljava/lang/String; = "OTA/beta"

.field public static final S3_OTA_RELEASE_PREFIX:Ljava/lang/String; = "OTA/release"

.field public static final S3_OTA_STABLE_PREFIX:Ljava/lang/String; = "OTA"

.field public static final S3_OTA_UNSTABLE_PREFIX:Ljava/lang/String; = "OTA/unstable"

.field public static final S3_PROTOCOL:Ljava/lang/String; = "s3://"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultSourceForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 2
    .param p0, "buildType"    # Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .prologue
    .line 95
    invoke-static {p0}, Lcom/navdy/client/debug/util/S3Constants;->getSourcesForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static getSourcesForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 5
    .param p0, "type"    # Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 77
    sget-object v0, Lcom/navdy/client/debug/util/S3Constants$1;->$SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType:[I

    invoke-virtual {p0}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 79
    :pswitch_0
    new-array v0, v4, [Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->BETA:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->RELEASE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v3

    goto :goto_0

    .line 81
    :pswitch_1
    new-array v0, v4, [Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->UNSTABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->STABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v3

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
