.class Lcom/navdy/client/debug/SubmitTicketFragment$3$1;
.super Ljava/lang/Object;
.source "SubmitTicketFragment.java"

# interfaces
.implements Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/SubmitTicketFragment$3;->onSuccess(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

.field final synthetic val$attachments:Ljava/util/ArrayList;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/SubmitTicketFragment$3;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/debug/SubmitTicketFragment$3;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iput-object p2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->val$key:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 6
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 293
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iget-object v1, v1, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iget-object v3, v3, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v4, 0x7f08048f

    invoke-virtual {v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Issue created "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->val$key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    .line 295
    .local v0, "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iget-object v2, v2, Lcom/navdy/client/debug/SubmitTicketFragment$3;->val$appContext:Landroid/content/Context;

    iget-object v3, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 297
    .end local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    :cond_0
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 285
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iget-object v1, v1, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iget-object v3, v3, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v4, 0x7f08048f

    invoke-virtual {v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Issue created and log files are uploaded :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->val$key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    .line 287
    .local v0, "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$3;

    iget-object v2, v2, Lcom/navdy/client/debug/SubmitTicketFragment$3;->val$appContext:Landroid/content/Context;

    iget-object v3, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 289
    .end local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    :cond_0
    return-void
.end method
