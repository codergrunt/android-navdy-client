.class public Lcom/navdy/client/debug/MainDebugActivity;
.super Lcom/navdy/client/debug/common/BaseDebugActivity;
.source "MainDebugActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/MainDebugActivity$MainFragment;
    }
.end annotation


# static fields
.field public static final EXTRA_OPEN_DIAL_SIM:Ljava/lang/String; = "openDialSimulator"

.field public static final MAIN_TAG:Ljava/lang/String; = "main"

.field public static final SPLASH_TAG:Ljava/lang/String; = "splash"

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/AppInstance;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/navdy/client/debug/common/BaseDebugActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public checkForUpdates()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/MainDebugActivity;->checkForUpdates(Lnet/hockeyapp/android/UpdateManagerListener;)V

    .line 98
    return-void
.end method

.method public checkForUpdates(Lnet/hockeyapp/android/UpdateManagerListener;)V
    .locals 3
    .param p1, "listener"    # Lnet/hockeyapp/android/UpdateManagerListener;

    .prologue
    .line 92
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08055a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "HOCKEY_APP_ID":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Lnet/hockeyapp/android/UpdateManager;->register(Landroid/app/Activity;Ljava/lang/String;Lnet/hockeyapp/android/UpdateManagerListener;Z)V

    .line 94
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 60
    invoke-super {p0, p1}, Lcom/navdy/client/debug/common/BaseDebugActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/navdy/client/debug/MainDebugActivity;->setVolumeControlStream(I)V

    .line 62
    const v4, 0x7f060002

    invoke-static {p0, v4, v3}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;IZ)V

    .line 63
    const v4, 0x7f030020

    invoke-virtual {p0, v4}, Lcom/navdy/client/debug/MainDebugActivity;->setContentView(I)V

    .line 64
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/debug/MainDebugActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 65
    iget-object v4, p0, Lcom/navdy/client/debug/MainDebugActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v0, 0x1

    .line 66
    .local v0, "hasConnection":Z
    :goto_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/client/ota/OTAUpdateService;->isLaunchedByOtaUpdateService(Landroid/content/Intent;)Z

    move-result v2

    .line 67
    .local v2, "launchedFromOtaNotification":Z
    if-nez v0, :cond_0

    if-eqz v2, :cond_4

    :cond_0
    new-instance v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;

    invoke-direct {v1}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;-><init>()V

    .line 68
    .local v1, "initialFragment":Landroid/app/Fragment;
    :goto_1
    if-nez p1, :cond_1

    .line 69
    new-instance v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;

    .end local v1    # "initialFragment":Landroid/app/Fragment;
    invoke-direct {v1}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;-><init>()V

    .line 70
    .restart local v1    # "initialFragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const v5, 0x7f1000c6

    const-string v6, "main"

    .line 71
    invoke-virtual {v4, v5, v1, v6}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 72
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 74
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->isDeveloperBuild()Z

    move-result v4

    if-nez v4, :cond_1

    .line 75
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity;->checkForUpdates()V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "openDialSimulator"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-class v4, Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-static {v3, v4}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    .line 81
    :cond_2
    return-void

    .end local v0    # "hasConnection":Z
    .end local v1    # "initialFragment":Landroid/app/Fragment;
    .end local v2    # "launchedFromOtaNotification":Z
    :cond_3
    move v0, v3

    .line 65
    goto :goto_0

    .line 67
    .restart local v0    # "hasConnection":Z
    .restart local v2    # "launchedFromOtaNotification":Z
    :cond_4
    new-instance v1, Lcom/navdy/client/debug/SplashFragment;

    invoke-direct {v1}, Lcom/navdy/client/debug/SplashFragment;-><init>()V

    goto :goto_1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/navdy/client/debug/common/BaseDebugActivity;->onResume()V

    .line 87
    const-string v0, "Settings_Debug"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 88
    return-void
.end method
