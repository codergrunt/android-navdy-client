.class Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;
.super Ljava/lang/Object;
.source "RemoteFileTransferManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/file/RemoteFileTransferManager;->onFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

.field final synthetic val$data:Lcom/navdy/service/library/events/file/FileTransferData;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/file/RemoteFileTransferManager;Lcom/navdy/service/library/events/file/FileTransferData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .prologue
    .line 394
    iput-object p1, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    iput-object p2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->val$data:Lcom/navdy/service/library/events/file/FileTransferData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 398
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->val$data:Lcom/navdy/service/library/events/file/FileTransferData;

    iget-object v3, v3, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v3}, Lokio/ByteString;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V

    .line 399
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 400
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->val$data:Lcom/navdy/service/library/events/file/FileTransferData;

    iget-object v2, v2, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 401
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 402
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 403
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;-><init>()V

    .line 404
    .local v0, "builder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 405
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .line 406
    invoke-static {v3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$200(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 407
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->postOnFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V

    .line 408
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$300(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    .end local v0    # "builder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 416
    :goto_0
    return-void

    .line 410
    :catch_0
    move-exception v1

    .line 411
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Exception while writing the data to the file"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 412
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 414
    iget-object v2, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v2}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/navdy/client/debug/file/RemoteFileTransferManager$2;->this$0:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-static {v3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->access$100(Lcom/navdy/client/debug/file/RemoteFileTransferManager;)Ljava/io/FileOutputStream;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    throw v2
.end method
