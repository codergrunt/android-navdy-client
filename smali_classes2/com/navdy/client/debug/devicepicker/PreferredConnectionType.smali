.class public final enum Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
.super Ljava/lang/Enum;
.source "PreferredConnectionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

.field public static final enum BT:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

.field public static final enum HTTP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

.field public static final enum TCP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;


# instance fields
.field private type:Lcom/navdy/service/library/device/connection/ConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    const-string v1, "TCP"

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;-><init>(Ljava/lang/String;ILcom/navdy/service/library/device/connection/ConnectionType;)V

    sput-object v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->TCP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    new-instance v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    const-string v1, "BT"

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;-><init>(Ljava/lang/String;ILcom/navdy/service/library/device/connection/ConnectionType;)V

    sput-object v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->BT:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    new-instance v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    const-string v1, "HTTP"

    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionType;->HTTP:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;-><init>(Ljava/lang/String;ILcom/navdy/service/library/device/connection/ConnectionType;)V

    sput-object v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->HTTP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    sget-object v1, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->TCP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->BT:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->HTTP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->$VALUES:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 0
    .param p3, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/device/connection/ConnectionType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput-object p3, p0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->type:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 16
    return-void
.end method

.method static fromConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;)Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    .locals 5
    .param p0, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 27
    invoke-static {}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->values()[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 28
    .local v0, "preferredType":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    invoke-virtual {v0}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v4

    if-ne v4, p0, :cond_0

    .line 32
    .end local v0    # "preferredType":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    :goto_1
    return-object v0

    .line 27
    .restart local v0    # "preferredType":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    .end local v0    # "preferredType":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->$VALUES:[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    return-object v0
.end method


# virtual methods
.method public getPriority()I
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->type:Lcom/navdy/service/library/device/connection/ConnectionType;

    return-object v0
.end method
