.class Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;
.super Ljava/lang/Object;
.source "HudSettingsFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/HudSettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingSpinnerListener"
.end annotation


# instance fields
.field private setting:Ljava/lang/String;

.field final synthetic this$0:Lcom/navdy/client/debug/HudSettingsFragment;

.field private value:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/HudSettingsFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "setting"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p2, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->setting:Ljava/lang/String;

    .line 150
    iput-object p3, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->value:Ljava/lang/String;

    .line 151
    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "selectedValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->value:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->this$0:Lcom/navdy/client/debug/HudSettingsFragment;

    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->setting:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;->value:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/navdy/client/debug/HudSettingsFragment;->access$000(Lcom/navdy/client/debug/HudSettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
