.class public Lcom/navdy/client/debug/CustomNotificationFragment;
.super Landroid/app/ListFragment;
.source "CustomNotificationFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field protected mListItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/CustomNotificationFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 34
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/CustomNotificationFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 35
    return-void
.end method

.method private sendEvent(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "event"    # Lcom/squareup/wire/Message;

    .prologue
    .line 265
    iget-object v1, p0, Lcom/navdy/client/debug/CustomNotificationFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 266
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 269
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->values()[Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/navdy/client/debug/CustomNotificationFragment;->mListItems:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/CustomNotificationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1090016

    const v3, 0x1020014

    .line 42
    invoke-static {}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->values()[Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 41
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/CustomNotificationFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 43
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    const v1, 0x7f03009c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 49
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 50
    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 117
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 118
    const/4 v1, 0x0

    invoke-virtual {p1, p3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 119
    iget-object v1, p0, Lcom/navdy/client/debug/CustomNotificationFragment;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 121
    .local v0, "clickedItem":Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;
    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment$1;->$SwitchMap$com$navdy$client$debug$CustomNotificationFragment$ListItem:[I

    invoke-virtual {v0}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 259
    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unhandled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 262
    :goto_0
    return-void

    .line 123
    :pswitch_0
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "INCOMING_PHONE_CALL_323"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 126
    :pswitch_1
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "END_PHONE_CALL_323"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 129
    :pswitch_2
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "PHONE_BATTERY_LOW"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 132
    :pswitch_3
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "PHONE_BATTERY_EXTREMELY_LOW"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 135
    :pswitch_4
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "PHONE_BATTERY_OK"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 138
    :pswitch_5
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TEXT_NOTIFICAION_WITH_REPLY_408"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 141
    :pswitch_6
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TEXT_NOTIFICAION_WITH_REPLY_999"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 144
    :pswitch_7
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TEXT_NOTIFICAION_WITH_NO_REPLY_510"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 147
    :pswitch_8
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_CONNECTED"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 150
    :pswitch_9
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_DISCONNECTED"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 153
    :pswitch_a
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_FORGOTTEN_SINGLE"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 156
    :pswitch_b
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_FORGOTTEN_MULTIPLE"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 159
    :pswitch_c
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_BATTERY_LOW"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 162
    :pswitch_d
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_BATTERY_EXTREMELY_LOW"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 165
    :pswitch_e
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_BATTERY_VERY_LOW"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 168
    :pswitch_f
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "DIAL_BATTERY_OK"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 171
    :pswitch_10
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "MUSIC"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 174
    :pswitch_11
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "VOICE_ASSIST"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 177
    :pswitch_12
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "BRIGHTNESS"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 180
    :pswitch_13
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TRAFFIC_REROUTE"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 183
    :pswitch_14
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_TRAFFIC_REROUTE"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 186
    :pswitch_15
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TRAFFIC_JAM"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 189
    :pswitch_16
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_TRAFFIC_JAM"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 192
    :pswitch_17
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TRAFFIC_INCIDENT"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 195
    :pswitch_18
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_TRAFFIC_INCIDENT"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 198
    :pswitch_19
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "TRAFFIC_DELAY"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 201
    :pswitch_1a
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_TRAFFIC_DELAY"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 204
    :pswitch_1b
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "PHONE_CONNECTED"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 207
    :pswitch_1c
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "PHONE_DISCONNECTED"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 210
    :pswitch_1d
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "PHONE_APP_DISCONNECTED"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 213
    :pswitch_1e
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_ALL_TOAST"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 216
    :pswitch_1f
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_ALL_TOAST_AND_CURRENT"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 219
    :pswitch_20
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_CURRENT_TOAST"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 222
    :pswitch_21
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "GOOGLE_CALENDAR_1"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 225
    :pswitch_22
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "APPLE_CALENDAR_1"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 228
    :pswitch_23
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "LOW_FUEL_LEVEL"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 231
    :pswitch_24
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "LOW_FUEL_LEVEL_NOCHECK"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 234
    :pswitch_25
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "LOW_FUEL_LEVEL_CLEAR"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 237
    :pswitch_26
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "OBD_LOW_FUEL_LEVEL"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 240
    :pswitch_27
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "CLEAR_OBD_LOW_FUEL_LEVEL"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 243
    :pswitch_28
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "FUEL_ADDED_TEST"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 246
    :pswitch_29
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "FIND_GAS_STATION"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 249
    :pswitch_2a
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "FIND_ROUTE_TO_CLOSEST_GAS_STATION"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 252
    :pswitch_2b
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "GENERIC_1"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 255
    :pswitch_2c
    new-instance v1, Lcom/navdy/service/library/events/notification/ShowCustomNotification;

    const-string v2, "GENERIC_2"

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/notification/ShowCustomNotification;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/CustomNotificationFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto/16 :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
    .end packed-switch
.end method
