.class Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$8;
.super Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;
.source "NavigationDemoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 511
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$8;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {p0}, Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onSpeedExceeded(Ljava/lang/String;F)V
    .locals 6
    .param p1, "roadName"    # Ljava/lang/String;
    .param p2, "speedLimit"    # F

    .prologue
    .line 514
    invoke-super {p0, p1, p2}, Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;->onSpeedExceeded(Ljava/lang/String;F)V

    .line 515
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$8;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$1300(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "%d m/s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    float-to-int v5, p2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    return-void
.end method

.method public onSpeedExceededEnd(Ljava/lang/String;F)V
    .locals 2
    .param p1, "roadName"    # Ljava/lang/String;
    .param p2, "speedLimit"    # F

    .prologue
    .line 520
    invoke-super {p0, p1, p2}, Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;->onSpeedExceededEnd(Ljava/lang/String;F)V

    .line 521
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$8;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$1300(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    return-void
.end method
