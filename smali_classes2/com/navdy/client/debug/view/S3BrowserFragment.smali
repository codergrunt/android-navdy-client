.class public abstract Lcom/navdy/client/debug/view/S3BrowserFragment;
.super Landroid/app/Fragment;
.source "S3BrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;,
        Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;,
        Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;,
        Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;
    }
.end annotation


# static fields
.field private static final REPORT_ON_DOWNLOAD_STEP:I = 0x3e8

.field private static final S3_DOWNLOAD_TIMEOUT:I = 0x493e0

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mAdapter:Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field mBackButton:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100184
    .end annotation
.end field

.field protected mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

.field protected mContext:Landroid/content/Context;

.field mList:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10027d
    .end annotation
.end field

.field private mPrefix:Ljava/lang/String;

.field mRefreshButton:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10027e
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/view/S3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/navdy/client/debug/view/S3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/view/S3BrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/client/debug/view/S3BrowserFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/view/S3BrowserFragment;)Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAdapter:Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    return-object v0
.end method

.method private back()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 133
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    const/16 v2, 0x2f

    iget-object v3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v0

    .line 137
    .local v0, "parentDir":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    .line 142
    :goto_0
    new-instance v1, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 143
    return-void

    .line 140
    :cond_1
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mPrefix:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected abstract getS3BucketName()Ljava/lang/String;
.end method

.method public onBack(Landroid/view/View;)V
    .locals 0
    .param p1, "button"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100184
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/debug/view/S3BrowserFragment;->back()V

    .line 105
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p0}, Lcom/navdy/client/debug/view/S3BrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mContext:Landroid/content/Context;

    .line 83
    const-string v0, ""

    .line 84
    .local v0, "AWS_ACCOUNT_ID":Ljava/lang/String;
    const-string v1, ""

    .line 86
    .local v1, "AWS_SECRET":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 87
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f080551

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f080552

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 95
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    :goto_0
    new-instance v3, Lcom/amazonaws/auth/BasicAWSCredentials;

    invoke-direct {v3, v0, v1}, Lcom/amazonaws/auth/BasicAWSCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .local v3, "credentials":Lcom/amazonaws/auth/BasicAWSCredentials;
    new-instance v5, Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-direct {v5, v3}, Lcom/amazonaws/services/s3/AmazonS3Client;-><init>(Lcom/amazonaws/auth/AWSCredentials;)V

    iput-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    .line 99
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 100
    return-void

    .line 89
    .end local v3    # "credentials":Lcom/amazonaws/auth/BasicAWSCredentials;
    :catch_0
    move-exception v4

    .line 90
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v5, Lcom/navdy/client/debug/view/S3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load meta-data, NameNotFound: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v4

    .line 92
    .local v4, "e":Ljava/lang/NullPointerException;
    sget-object v5, Lcom/navdy/client/debug/view/S3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to load meta-data, NullPointer: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    const v1, 0x7f030097

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 115
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 117
    new-instance v1, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/view/S3BrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAdapter:Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    .line 118
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mList:Landroid/widget/ListView;

    new-instance v2, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 119
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAdapter:Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 121
    return-object v0
.end method

.method protected abstract onFileSelected(Ljava/lang/String;)V
.end method

.method public onRefresh(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10027e
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 128
    new-instance v0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 130
    return-void
.end method

.method protected storeS3ObjectInTemp(Ljava/lang/String;Ljava/io/File;Lcom/amazonaws/event/ProgressListener;)V
    .locals 7
    .param p1, "s3Key"    # Ljava/lang/String;
    .param p2, "output"    # Ljava/io/File;
    .param p3, "downloadProgressListener"    # Lcom/amazonaws/event/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 148
    new-instance v2, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-direct {v2, v0}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;-><init>(Lcom/amazonaws/services/s3/AmazonS3;)V

    .line 150
    .local v2, "transferManager":Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 151
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    const v1, 0x7f08013e

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 153
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mBackButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->invalidate()V

    .line 154
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->invalidate()V

    .line 156
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/debug/view/S3BrowserFragment$1;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;Ljava/lang/String;Ljava/io/File;Lcom/amazonaws/event/ProgressListener;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 216
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method
