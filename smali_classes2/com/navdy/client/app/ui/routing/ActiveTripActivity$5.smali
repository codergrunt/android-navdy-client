.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 234
    return-void
.end method

.method public onReady(Lcom/here/android/mpa/mapping/Map;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 224
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getHereMapMarker()Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$202(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 226
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$300(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$5;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v2, Lcom/here/android/mpa/mapping/Map$Animation;->LINEAR:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocationAndDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 231
    return-void
.end method
