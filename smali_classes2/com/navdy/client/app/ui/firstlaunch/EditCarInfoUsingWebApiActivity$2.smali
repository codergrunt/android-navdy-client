.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;
.super Ljava/lang/Object;
.source "EditCarInfoUsingWebApiActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initCarSelectorScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

.field final synthetic val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

.field final synthetic val$make:Landroid/widget/Spinner;

.field final synthetic val$model:Landroid/widget/Spinner;

.field final synthetic val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

.field final synthetic val$year:Landroid/widget/Spinner;

.field final synthetic val$yearAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

.field final synthetic val$yearString:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/widget/Spinner;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Lcom/navdy/client/app/framework/util/CarMdClient;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$make:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$year:Landroid/widget/Spinner;

    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$yearAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    iput-object p5, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$model:Landroid/widget/Spinner;

    iput-object p6, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    iput-object p7, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    iput-object p8, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$yearString:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "selectedMakePosition"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemSelected selectedMakePosition: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$make:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 181
    .local v0, "selectedMake":Ljava/lang/String;
    if-gtz p3, :cond_0

    .line 182
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$year:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$yearAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 183
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$model:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 240
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$make:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt p3, v1, :cond_1

    .line 187
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)V

    .line 190
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showProgressDialog()V

    .line 191
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;)V

    invoke-virtual {v1, v0, v2}, Lcom/navdy/client/app/framework/util/CarMdClient;->getYears(Ljava/lang/String;Lcom/navdy/client/app/framework/util/CarMdCallBack;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
