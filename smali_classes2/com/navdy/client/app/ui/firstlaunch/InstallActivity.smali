.class public Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "InstallActivity.java"


# static fields
.field static final EXTRA_MOUNT:Ljava/lang/String; = "extra_mount"

.field static final EXTRA_STEP:Ljava/lang/String; = "extra_step"


# instance fields
.field private comingFromSettings:Z

.field private installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

.field private isFirstTimeInPager:Z

.field private startingStep:I

.field private viewPager:Landroid/support/v4/view/ViewPager;

.field private weHadCarInfo:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 46
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    .line 47
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->isFirstTimeInPager:Z

    .line 50
    iput-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->weHadCarInfo:Z

    .line 51
    const v0, 0x7f030074

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    .line 53
    iput-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->comingFromSettings:Z

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;)Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static getVideoUrlForStep(I)Ljava/lang/String;
    .locals 9
    .param p0, "step"    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 243
    packed-switch p0, :pswitch_data_0

    .line 269
    :pswitch_0
    const/4 v3, -0x1

    .line 273
    .local v3, "videoUrlIndex":I
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 274
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 275
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Unable to start video player. context is null !"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 294
    :goto_1
    return-object v5

    .line 245
    .end local v1    # "context":Landroid/content/Context;
    .end local v3    # "videoUrlIndex":I
    :pswitch_1
    const/4 v3, 0x0

    .line 246
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 248
    .end local v3    # "videoUrlIndex":I
    :pswitch_2
    const/4 v3, 0x1

    .line 249
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 251
    .end local v3    # "videoUrlIndex":I
    :pswitch_3
    const/4 v3, 0x2

    .line 252
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 254
    .end local v3    # "videoUrlIndex":I
    :pswitch_4
    const/4 v3, 0x3

    .line 255
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 257
    .end local v3    # "videoUrlIndex":I
    :pswitch_5
    const/4 v3, 0x4

    .line 258
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 260
    .end local v3    # "videoUrlIndex":I
    :pswitch_6
    const/4 v3, 0x5

    .line 261
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 263
    .end local v3    # "videoUrlIndex":I
    :pswitch_7
    const/4 v3, 0x6

    .line 264
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 266
    .end local v3    # "videoUrlIndex":I
    :pswitch_8
    const/4 v3, 0x7

    .line 267
    .restart local v3    # "videoUrlIndex":I
    goto :goto_0

    .line 278
    .restart local v1    # "context":Landroid/content/Context;
    :cond_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 279
    .local v2, "resources":Landroid/content/res/Resources;
    if-nez v2, :cond_1

    .line 280
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Unable to start video player. resources are null !"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 284
    :cond_1
    const v6, 0x7f09000b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 285
    .local v4, "videoUrls":[Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "box"

    const-string v8, "Old_Box"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 286
    .local v0, "box":Ljava/lang/String;
    const-string v6, "Old_Box"

    invoke-static {v0, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 287
    const v6, 0x7f09000c

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 290
    :cond_2
    array-length v6, v4

    if-lez v6, :cond_3

    if-ltz v3, :cond_3

    array-length v6, v4

    if-le v3, v6, :cond_4

    .line 291
    :cond_3
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Unable to start video player. the video url array is null !"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 294
    :cond_4
    aget-object v5, v4, v3

    goto :goto_1

    .line 243
    :pswitch_data_0
    .packed-switch 0x7f03006e
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private onMountClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 1
    .param p1, "mountType"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 464
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->setMountTypeAndForceRefreshAdapterOnLensCheck(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 465
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onBackClick(Landroid/view/View;)V

    .line 466
    return-void
.end method

.method private onWorkedClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 1
    .param p1, "mountType"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 488
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->setMountTypeAndForceRefreshAdapterOnLensCheck(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 489
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onNextClick(Landroid/view/View;)V

    .line 490
    return-void
.end method

.method private setMountTypeAndForceRefreshAdapterOnLensCheck(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 2
    .param p1, "mountType"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->hideCustomDialog()V

    .line 494
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    .line 495
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setCurrentMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 497
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 499
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 500
    return-void
.end method

.method public static startVideoPlayerForThisStep(ILandroid/app/Activity;)V
    .locals 5
    .param p0, "step"    # I
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 227
    invoke-static {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getVideoUrlForStep(I)Ljava/lang/String;

    move-result-object v2

    .line 228
    .local v2, "videoUrl":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unable to start video player. the video url is null !"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 237
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 234
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "video_url"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static userHasFinishedInstall()Z
    .locals 3

    .prologue
    .line 503
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 504
    .local v0, "settingsPrefs":Landroid/content/SharedPreferences;
    const-string v1, "finished_install"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public hideCustomDialog()V
    .locals 2

    .prologue
    .line 360
    const v1, 0x7f1001d7

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 361
    .local v0, "dialog":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 362
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364
    :cond_0
    return-void
.end method

.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onBackPressed()V

    .line 186
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 159
    const v2, 0x7f1001d7

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 160
    .local v1, "dialog":Landroid/view/View;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 161
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_3

    .line 164
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 166
    .local v0, "currentItem":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    .line 167
    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->isOnStepAfterCablePicker(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    .line 168
    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->isOnStepAfterLocatingObd(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    :cond_1
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0

    .line 173
    :cond_2
    add-int/lit8 v2, v0, -0x1

    if-ltz v2, :cond_3

    .line 174
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 178
    .end local v0    # "currentItem":I
    :cond_3
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCancelClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->hideCustomDialog()V

    .line 470
    return-void
.end method

.method public onContactSupportClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->hideCustomDialog()V

    .line 450
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 451
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "default_problem_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 452
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 453
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 58
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v7, 0x7f03006b

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 62
    .local v1, "i":Landroid/content/Intent;
    const-string v7, "comming_from_settings"

    invoke-virtual {v1, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->comingFromSettings:Z

    .line 65
    const v7, 0x7f10018f

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/support/v4/view/ViewPager;

    iput-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 67
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    if-nez v7, :cond_0

    .line 68
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Missing UI element: ViewPager !!!"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v7, v9}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 74
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v8, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$1;

    invoke-direct {v8, p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;)V

    invoke-virtual {v7, v8}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 94
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v7, v10}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getScreenAtPosition(I)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "screen":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 96
    invoke-static {v5}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 99
    :cond_1
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v7, v8}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 102
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 103
    .local v2, "in":Landroid/content/Intent;
    const-string v7, "extra_step"

    const v8, 0x7f030074

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    .line 104
    const-string v7, "extra_mount"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, "mount":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 107
    invoke-static {v3}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getMountTypeForValue(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v6

    .line 108
    .local v6, "type":Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v7, v6}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setCurrentMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 109
    sget-object v7, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-eq v6, v7, :cond_2

    .line 110
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    .line 111
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "should_ask_what_mount_worked"

    .line 112
    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 113
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 118
    .end local v6    # "type":Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    :cond_2
    iget v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    const v8, 0x7f030076

    if-eq v7, v8, :cond_3

    iget v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    const v8, 0x7f030075

    if-ne v7, v8, :cond_4

    .line 120
    :cond_3
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    sget-object v8, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v7, v8}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setCurrentMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 122
    :cond_4
    iget v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    const v8, 0x7f030072

    if-ne v7, v8, :cond_5

    .line 123
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v7}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v7

    sget-object v8, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne v7, v8, :cond_5

    .line 124
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    sget-object v8, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v7, v8}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setCurrentMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 129
    :cond_5
    iget v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v8}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCount()I

    move-result v8

    if-lt v7, v8, :cond_6

    .line 130
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v7, v9}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setHasPickedMount(Z)V

    .line 132
    :cond_6
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v7}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v7

    invoke-static {v7}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getInstallFlow(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)[I

    move-result-object v0

    .line 133
    .local v0, "currentFlow":[I
    iget v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startingStep:I

    invoke-static {v7, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getPositionForStep(I[I)I

    move-result v4

    .line 134
    .local v4, "position":I
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v7, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_0
.end method

.method public onLocateObdClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 212
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 214
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_next_step"

    const-string v2, "installation_flow"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 216
    return-void
.end method

.method public onMediumTallMountClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 456
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onMountClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 457
    return-void
.end method

.method public onMediumWorkedClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 478
    const-string v0, "User_Picked_Medium_Mount"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 479
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->MEDIUM:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onWorkedClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 480
    return-void
.end method

.method public onMyLensIsOkClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 317
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->setHasPickedMount(Z)V

    .line 319
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "should_ask_what_mount_worked"

    const/4 v3, 0x0

    .line 320
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 323
    .local v0, "shouldAskWhatEndedUpWorking":Z
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showDialogForWhatEndedUpWorking()V

    .line 338
    :goto_0
    return-void

    .line 326
    :cond_0
    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$2;->$SwitchMap$com$navdy$client$app$framework$models$MountInfo$MountType:[I

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 328
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onShortWorkedClick(Landroid/view/View;)V

    goto :goto_0

    .line 331
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onMediumWorkedClick(Landroid/view/View;)V

    goto :goto_0

    .line 334
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onTallWorkedClick(Landroid/view/View;)V

    goto :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onMyLensIsTooHighClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 300
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 301
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "should_ask_what_mount_worked"

    const/4 v2, 0x1

    .line 302
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 303
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 305
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$2;->$SwitchMap$com$navdy$client$app$framework$models$MountInfo$MountType:[I

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 314
    :goto_0
    return-void

    .line 307
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showDialogForTooHighWithLowMount()V

    goto :goto_0

    .line 311
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showDialogForTooHighWithMediumTallMount()V

    goto :goto_0

    .line 305
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onMyLensIsTooLowClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 341
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 342
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "should_ask_what_mount_worked"

    const/4 v2, 0x1

    .line 343
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 344
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 346
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$2;->$SwitchMap$com$navdy$client$app$framework$models$MountInfo$MountType:[I

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 355
    :goto_0
    return-void

    .line 348
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showDialogForTooLowWithLowMount()V

    goto :goto_0

    .line 352
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showDialogForTooLowWithMediumTallMount()V

    goto :goto_0

    .line 346
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onNextClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 193
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_0

    .line 194
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 195
    .local v0, "currentItem":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->isOnStepBeforeCablePicker(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 196
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 198
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 209
    .end local v0    # "currentItem":I
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 199
    .restart local v0    # "currentItem":I
    :cond_1
    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 200
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 201
    :cond_2
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->comingFromSettings:Z

    if-eqz v2, :cond_3

    .line 202
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->finish()V

    goto :goto_0

    .line 204
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 139
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 140
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->hideSystemUI()V

    .line 143
    iget-boolean v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->weHadCarInfo:Z

    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->weHaveCarInfo()Z

    move-result v5

    if-eq v4, v5, :cond_0

    .line 144
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->weHaveCarInfo()Z

    move-result v4

    iput-boolean v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->weHadCarInfo:Z

    .line 145
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->notifyDataSetChanged()V

    .line 148
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 149
    .local v0, "currentItem":I
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCount()I

    move-result v1

    .line 150
    .local v1, "pageCount":I
    add-int/lit8 v4, v1, -0x1

    if-ge v0, v4, :cond_2

    const/4 v2, 0x1

    .line 152
    .local v2, "weAreNotOnTheLastPage":Z
    :goto_0
    iget-boolean v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->isFirstTimeInPager:Z

    if-eqz v4, :cond_1

    if-eqz v2, :cond_1

    .line 153
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->isFirstTimeInPager:Z

    .line 155
    :cond_1
    return-void

    .end local v2    # "weAreNotOnTheLastPage":Z
    :cond_2
    move v2, v3

    .line 150
    goto :goto_0
.end method

.method public onShortMountClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 460
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onMountClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 461
    return-void
.end method

.method public onShortWorkedClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 473
    const-string v0, "User_Picked_Short_Mount"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 474
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onWorkedClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 475
    return-void
.end method

.method public onTallWorkedClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 483
    const-string v0, "User_Picked_Tall_Mount"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 484
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onWorkedClick(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V

    .line 485
    return-void
.end method

.method public onWatchClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startVideoPlayerForCurrentStep()V

    .line 190
    return-void
.end method

.method public showDialogForTooHighWithLowMount()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 367
    const v1, 0x7f08026f

    const v2, 0x7f0804c9

    const/4 v3, 0x1

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showThisDialog(IIZZZ)V

    .line 368
    return-void
.end method

.method public showDialogForTooHighWithMediumTallMount()V
    .locals 4

    .prologue
    .line 381
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 382
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "should_ask_what_mount_worked"

    const/4 v3, 0x1

    .line 383
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 384
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 385
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 386
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_use_case"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 387
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 388
    return-void
.end method

.method public showDialogForTooLowWithLowMount()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 371
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 372
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "should_ask_what_mount_worked"

    .line 373
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 374
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 375
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_use_case"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 377
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startActivity(Landroid/content/Intent;)V

    .line 378
    return-void
.end method

.method public showDialogForTooLowWithMediumTallMount()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 391
    const v1, 0x7f080270

    const v2, 0x7f080253

    const/4 v5, 0x0

    move-object v0, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showThisDialog(IIZZZ)V

    .line 392
    return-void
.end method

.method public showDialogForWhatEndedUpWorking()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 395
    const v1, 0x7f0804fc

    const v2, 0x7f0804fd

    move-object v0, p0

    move v4, v3

    move v5, v3

    move v7, v6

    move v8, v6

    move v9, v3

    invoke-virtual/range {v0 .. v9}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showThisDialog(IIZZZZZZZ)V

    .line 396
    return-void
.end method

.method public showThisDialog(IIZZZ)V
    .locals 10
    .param p1, "titleResId"    # I
    .param p2, "descResId"    # I
    .param p3, "showContactUs"    # Z
    .param p4, "showMediumTallMount"    # Z
    .param p5, "showShortMount"    # Z

    .prologue
    const/4 v6, 0x0

    .line 403
    const/4 v9, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v7, v6

    move v8, v6

    invoke-virtual/range {v0 .. v9}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->showThisDialog(IIZZZZZZZ)V

    .line 404
    return-void
.end method

.method public showThisDialog(IIZZZZZZZ)V
    .locals 5
    .param p1, "titleResId"    # I
    .param p2, "descResId"    # I
    .param p3, "showContactUs"    # Z
    .param p4, "showMediumTallMount"    # Z
    .param p5, "showShortMount"    # Z
    .param p6, "showShortWorked"    # Z
    .param p7, "showMediumWorked"    # Z
    .param p8, "showTallWorked"    # Z
    .param p9, "showCancel"    # Z

    .prologue
    const/4 v4, 0x0

    .line 415
    const v3, 0x7f1001d7

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 416
    .local v1, "dialog":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 417
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 419
    :cond_0
    const v3, 0x7f1001d8

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 420
    .local v2, "title":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 421
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 422
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    .line 424
    :cond_1
    const v3, 0x7f1001d9

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 425
    .local v0, "desc":Landroid/widget/TextView;
    if-eqz v0, :cond_2

    .line 426
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 427
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 429
    :cond_2
    const v3, 0x7f1001da

    invoke-virtual {p0, v3, p5}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 430
    const v3, 0x7f1001a6

    invoke-virtual {p0, v3, p4}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 431
    const v3, 0x7f1001db

    invoke-virtual {p0, v3, p3}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 432
    const v3, 0x7f1001dc

    invoke-virtual {p0, v3, p9}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 434
    const v3, 0x7f1001dd

    invoke-virtual {p0, v3, p6}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 435
    const v3, 0x7f1001de

    invoke-virtual {p0, v3, p7}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 436
    const v3, 0x7f1001df

    invoke-virtual {p0, v3, p8}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->updateThisButton(IZ)V

    .line 437
    return-void
.end method

.method public startVideoPlayerForCurrentStep()V
    .locals 4

    .prologue
    .line 220
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 221
    .local v1, "currentPosition":I
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->installPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType()Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v0

    .line 222
    .local v0, "currentMountType":Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    invoke-static {v1, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getStepForCurrentPosition(ILcom/navdy/client/app/framework/models/MountInfo$MountType;)I

    move-result v2

    .line 223
    .local v2, "currentStep":I
    invoke-static {v2, p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->startVideoPlayerForThisStep(ILandroid/app/Activity;)V

    .line 224
    return-void
.end method

.method public updateThisButton(IZ)V
    .locals 2
    .param p1, "buttonResId"    # I
    .param p2, "show"    # Z

    .prologue
    .line 440
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 441
    .local v0, "contactSupport":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 442
    if-eqz p2, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 444
    :cond_0
    return-void

    .line 442
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method
