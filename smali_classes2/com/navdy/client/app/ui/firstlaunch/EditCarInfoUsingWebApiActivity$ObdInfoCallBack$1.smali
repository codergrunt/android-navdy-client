.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;
.super Lcom/navdy/client/app/framework/util/CarMdCallBack;
.source "EditCarInfoUsingWebApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->processResponse(Lokhttp3/ResponseBody;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    .prologue
    .line 457
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    invoke-direct {p0}, Lcom/navdy/client/app/framework/util/CarMdCallBack;-><init>()V

    return-void
.end method


# virtual methods
.method public processFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 473
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v0, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$1500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Unable to parse car md obd location photo response."

    invoke-virtual {v0, v1, p1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 475
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->access$1400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V

    .line 476
    return-void
.end method

.method public processResponse(Lokhttp3/ResponseBody;)V
    .locals 5
    .param p1, "response"    # Lokhttp3/ResponseBody;

    .prologue
    .line 461
    :try_start_0
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object v1

    .line 462
    .local v1, "input":Ljava/io/InputStream;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 463
    .local v2, "obdImage":Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->saveObdImageToInternalStorage(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->access$1400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V

    .line 469
    .end local v1    # "input":Ljava/io/InputStream;
    .end local v2    # "obdImage":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 464
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$1300(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "Something went wrong while trying to process the response for the OBD photo."

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 467
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->access$1400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    invoke-static {v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->access$1400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;)V

    throw v3
.end method
