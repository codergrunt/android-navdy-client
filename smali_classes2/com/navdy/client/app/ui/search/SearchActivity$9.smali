.class Lcom/navdy/client/app/ui/search/SearchActivity$9;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->setEditTextListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 666
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 8
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 675
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1900(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 677
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 679
    .local v1, "string":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 681
    .local v2, "textIsEmpty":Z
    if-eqz v2, :cond_0

    .line 682
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->EMPTY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-static {v3, v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2000(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 687
    :goto_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 691
    if-eqz v2, :cond_1

    .line 692
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$602(Lcom/navdy/client/app/ui/search/SearchActivity;Z)Z

    .line 693
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2100(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 695
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2200(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 696
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/ImageButton;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 697
    new-instance v3, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {v3, v4, v6}, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/ui/search/SearchActivity$1;)V

    new-array v4, v5, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 727
    :goto_1
    return-void

    .line 684
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-static {v3, v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2000(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    goto :goto_0

    .line 699
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3, v5}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$602(Lcom/navdy/client/app/ui/search/SearchActivity;Z)Z

    .line 700
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2100(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 702
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2400(Lcom/navdy/client/app/ui/search/SearchActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 703
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2400(Lcom/navdy/client/app/ui/search/SearchActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_1

    .line 707
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureOnMainThread()V

    .line 709
    const/16 v0, 0x1f4

    .line 711
    .local v0, "keyListenerDelay":I
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2500(Lcom/navdy/client/app/ui/search/SearchActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 712
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2500(Lcom/navdy/client/app/ui/search/SearchActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 713
    const/4 v0, 0x0

    .line 716
    :cond_3
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$9;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$2800(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/search/SearchActivity$9$1;

    invoke-direct {v4, p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity$9$1;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity$9;Ljava/lang/String;)V

    int-to-long v6, v0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 668
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 671
    return-void
.end method
