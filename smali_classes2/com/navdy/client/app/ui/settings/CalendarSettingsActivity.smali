.class public Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "CalendarSettingsActivity.java"


# instance fields
.field private adapter:Lcom/navdy/client/app/ui/settings/CalendarAdapter;

.field private permissionScreen:Landroid/view/View;

.field private recyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v2, 0x7f0300ef

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->setContentView(I)V

    .line 30
    new-instance v2, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v3, 0x7f0802b2

    .line 31
    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 34
    const v2, 0x7f100386

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->permissionScreen:Landroid/view/View;

    .line 35
    const v2, 0x7f10027d

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 37
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_0

    .line 38
    new-instance v2, Lcom/navdy/client/app/ui/settings/CalendarAdapter;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/settings/CalendarAdapter;-><init>()V

    iput-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/CalendarAdapter;

    .line 39
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/CalendarAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 40
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 41
    .local v1, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 42
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 45
    .end local v1    # "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    :cond_0
    const v2, 0x7f10012f

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "desc":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 47
    const v2, 0x7f080193

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    :cond_1
    return-void
.end method

.method public onDescriptionClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 78
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->goToSystemSettingsAppInfoForOurApp(Landroid/app/Activity;)V

    .line 79
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 73
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->forceSuggestionFullRefresh()V

    .line 74
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 75
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 53
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/CalendarAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/settings/CalendarAdapter;->getItemCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 55
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->permissionScreen:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->permissionScreen:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 69
    :cond_1
    :goto_0
    return-void

    .line 62
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->permissionScreen:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->permissionScreen:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 65
    :cond_3
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0
.end method
