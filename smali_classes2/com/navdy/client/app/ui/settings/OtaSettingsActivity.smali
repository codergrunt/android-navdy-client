.class public Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "OtaSettingsActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/navdy/client/ota/OTAUpdateUIClient;


# static fields
.field public static final ARGUMENT_CONFIRMATION_MESSAGE:Ljava/lang/String; = "message_confirmation"

.field public static final DIALOG_DOWNLOAD_UNSTABLE_CONFIRMATION:I = 0x2

.field public static final DIALOG_DOWNLOAD_USING_MOBILE_DATA:I = 0x1

.field public static final DIALOG_ERROR:I = 0x3

.field private static final MAX_LINES_FOR_HIDDEN_DESCRIPTION:I = 0x2

.field private static final SCREEN_PERCENTAGE_FOR_IMAGE:D = 0.35

.field private static final START_INDEX_OF_HEX_COLOR:I = 0x3


# instance fields
.field private appVersion:Landroid/widget/TextView;

.field private buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

.field private buildTypeUnstableSwitch:Landroid/widget/Switch;

.field private button:Landroid/widget/Button;

.field private description:Landroid/widget/TextView;

.field private downloadVersion:Landroid/widget/TextView;

.field private hardwareSupportTitle:Landroid/widget/TextView;

.field private hudVersion:Landroid/widget/TextView;

.field private image:Landroid/widget/ImageView;

.field private isBound:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private moreOrLess:Landroid/widget/TextView;

.field private otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

.field private progressBar:Landroid/widget/ProgressBar;

.field private progressBar2:Landroid/widget/ProgressBar;

.field private progressBarContainer:Landroid/widget/RelativeLayout;

.field private progressData:Landroid/widget/TextView;

.field private progressPercentage:Landroid/widget/TextView;

.field private sharedPrefs:Landroid/content/SharedPreferences;

.field private showingMoreInfo:Z

.field private subtitle:Landroid/widget/TextView;

.field private subtitle2:Landroid/widget/TextView;

.field private title:Landroid/widget/TextView;

.field private viewsInitialized:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    .line 72
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->isBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 94
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showingMoreInfo:Z

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/model/UpdateInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1002(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$1100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUnstableSource(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateService$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
    .param p1, "x1"    # Lcom/navdy/client/ota/OTAUpdateService$State;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->cancelAndRecheckForUpdate(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Lcom/navdy/client/ota/OTAUpdateService$State;
    .param p3, "x3"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setDownloadProgressPercentage(IJ)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUploadProgressPercentage(IJ)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setupView()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/OTAUpdateServiceInterface;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private cancelAndRecheckForUpdate(Lcom/navdy/client/ota/OTAUpdateService$State;)V
    .locals 2
    .param p1, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;

    .prologue
    .line 774
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    invoke-virtual {p1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 787
    :goto_0
    :pswitch_0
    return-void

    .line 776
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->cancelDownload()V

    .line 777
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    goto :goto_0

    .line 780
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->cancelUpload()V

    .line 781
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->resetUpdate()V

    .line 782
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    goto :goto_0

    .line 774
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private connectToService()V
    .locals 4

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 234
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 235
    .local v1, "serviceIntent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 236
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->isBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 238
    :cond_0
    return-void
.end method

.method private disconnectService()V
    .locals 4

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 242
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->isBound:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    :try_start_0
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v1

    .line 246
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to unbind from OTA Service."

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getCurrentHUDVersionText()Ljava/lang/String;
    .locals 4

    .prologue
    .line 614
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v2, :cond_1

    .line 615
    const v2, 0x7f08011b

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 616
    .local v1, "resourceText":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v2}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getHUDBuildVersionText()Ljava/lang/String;

    move-result-object v0

    .line 617
    .local v0, "hudBuildVersionText":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 618
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 623
    .end local v0    # "hudBuildVersionText":Ljava/lang/String;
    .end local v1    # "resourceText":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 620
    .restart local v0    # "hudBuildVersionText":Ljava/lang/String;
    .restart local v1    # "resourceText":Ljava/lang/String;
    :cond_0
    const v2, 0x7f08011c

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 623
    .end local v0    # "hudBuildVersionText":Ljava/lang/String;
    .end local v1    # "resourceText":Ljava/lang/String;
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method private getHUDVersionText()Ljava/lang/String;
    .locals 5

    .prologue
    .line 628
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v2, :cond_1

    .line 629
    const v2, 0x7f08011a

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 630
    .local v1, "resourceText":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v2}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getHUDBuildVersionText()Ljava/lang/String;

    move-result-object v0

    .line 631
    .local v0, "hudBuildVersionText":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 632
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v4}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getHUDBuildVersionText()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 637
    .end local v0    # "hudBuildVersionText":Ljava/lang/String;
    .end local v1    # "resourceText":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 634
    .restart local v0    # "hudBuildVersionText":Ljava/lang/String;
    .restart local v1    # "resourceText":Ljava/lang/String;
    :cond_0
    const v2, 0x7f0804da

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 637
    .end local v0    # "hudBuildVersionText":Ljava/lang/String;
    .end local v1    # "resourceText":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getLastKnownUploadSize()J
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v0, :cond_0

    .line 602
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-ne v0, v1, :cond_0

    .line 603
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->lastKnownUploadSize()J

    move-result-wide v0

    .line 606
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private getState()Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getOTAUpdateState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    .line 583
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    goto :goto_0
.end method

.method private getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 588
    const/4 v0, 0x0

    .line 589
    .local v0, "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v1, :cond_0

    .line 590
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v0

    .line 593
    :cond_0
    if-nez v0, :cond_1

    .line 594
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v0

    .line 596
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateInfo status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 597
    return-object v0
.end method

.method private isCheckingForUpdate()Z
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->isCheckingForUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setBuildSources()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 696
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildType()Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v7

    if-eq v6, v7, :cond_4

    .line 697
    :cond_0
    sget-object v6, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Setting build source view"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 698
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildSource()Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v0

    .line 699
    .local v0, "buildSource":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    sget-object v6, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Build source :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 700
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildSources()[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v4

    .line 701
    .local v4, "sources":[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildType()Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .line 702
    sget-object v6, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Build Type :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-virtual {v8}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 703
    const/4 v1, 0x0

    .line 704
    .local v1, "i":I
    const/4 v2, 0x0

    .line 705
    .local v2, "index":I
    array-length v7, v4

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_2

    aget-object v3, v4, v6

    .line 706
    .local v3, "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    if-ne v0, v3, :cond_1

    .line 707
    move v2, v1

    .line 709
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 705
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 711
    .end local v3    # "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :cond_2
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    if-nez v2, :cond_3

    const/4 v5, 0x1

    :cond_3
    invoke-virtual {v6, v5}, Landroid/widget/Switch;->setChecked(Z)V

    .line 712
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    new-instance v6, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$8;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 750
    .end local v0    # "buildSource":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .end local v1    # "i":I
    .end local v2    # "index":I
    .end local v4    # "sources":[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :cond_4
    return-void
.end method

.method private setDownloadProgressPercentage(IJ)V
    .locals 2
    .param p1, "percentage"    # I
    .param p2, "totalDownloaded"    # J

    .prologue
    .line 372
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setThisProgressPercentage(Landroid/widget/ProgressBar;IJ)V

    .line 373
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar2:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 374
    return-void
.end method

.method private setThisProgressPercentage(Landroid/widget/ProgressBar;IJ)V
    .locals 9
    .param p1, "progressBar"    # Landroid/widget/ProgressBar;
    .param p2, "percentage"    # I
    .param p3, "doneSoFar"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 385
    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 386
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressPercentage:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%1$d%%"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v1

    .line 388
    .local v1, "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    if-eqz v1, :cond_0

    .line 389
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 390
    .local v0, "applicationContext":Landroid/content/Context;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressData:Landroid/widget/TextView;

    const-string v3, "%1$s / %2$s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 391
    invoke-static {v0, p3, p4}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-wide v6, v1, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    .line 392
    invoke-static {v0, v6, v7}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 390
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    .end local v0    # "applicationContext":Landroid/content/Context;
    :cond_0
    return-void
.end method

.method private setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 6
    .param p1, "checkingForUpdate"    # Z
    .param p2, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;
    .param p3, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    .line 438
    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;J)V

    .line 439
    return-void
.end method

.method private setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;J)V
    .locals 10
    .param p1, "checkingForUpdate"    # Z
    .param p2, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;
    .param p3, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;
    .param p4, "lastKnownUpdateSize"    # J

    .prologue
    .line 442
    sget-object v8, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setUIState; checkingForUpdate="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "; state="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "; updateInfo.versionName="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz p3, :cond_0

    iget-object v7, p3, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    :goto_0
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "; lastKnownUpdateSize="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 446
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 447
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_1

    .line 551
    :goto_1
    return-void

    .line 442
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    const-string v7, "updateInfo is null!!!"

    goto :goto_0

    .line 450
    .restart local v0    # "context":Landroid/content/Context;
    :cond_1
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->hudVersion:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getHUDVersionText()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 452
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBarContainer:Landroid/widget/RelativeLayout;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 453
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->downloadVersion:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 454
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->hardwareSupportTitle:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 455
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 456
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 457
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 459
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setBuildSources()V

    .line 460
    iget-boolean v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showingMoreInfo:Z

    invoke-direct {p0, v7}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showReleaseNotes(Z)V

    .line 462
    if-eqz p1, :cond_2

    .line 463
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f080100

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 464
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f080110

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 465
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f0201fb

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 466
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f0800ff

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    .line 467
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 474
    :cond_2
    sget-object v7, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    invoke-virtual {p2}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    goto :goto_1

    .line 476
    :pswitch_0
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f0804df

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 477
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f0802e0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 478
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 479
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getCurrentHUDVersionText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f02020e

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 481
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f0800fd

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 485
    :pswitch_1
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f0804e0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 486
    if-eqz p3, :cond_3

    .line 487
    iget-wide v8, p3, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    invoke-static {v0, v8, v9}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 488
    .local v3, "shortFileSize":Ljava/lang/String;
    const v7, 0x7f0804e3

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 490
    .local v2, "resourceText":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Lcom/navdy/client/ota/model/UpdateInfo;->getFormattedVersionName(Lcom/navdy/client/ota/model/UpdateInfo;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v3, v7, v8

    invoke-static {v2, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 491
    .local v6, "updateVersionText":Ljava/lang/String;
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 496
    .end local v2    # "resourceText":Ljava/lang/String;
    .end local v3    # "shortFileSize":Ljava/lang/String;
    .end local v6    # "updateVersionText":Ljava/lang/String;
    :goto_2
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 497
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getCurrentHUDVersionText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f02020f

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 499
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f08013d

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 494
    :cond_3
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f080308

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 503
    :pswitch_2
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f08013f

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 504
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f0804f8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 505
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 506
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f020211

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 507
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f0800e7

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    .line 508
    invoke-virtual {p0, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->makeProgressBarVisibleAndSetInfoVersion(Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 509
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->downloadVersion:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 510
    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setDownloadProgressPercentage(IJ)V

    goto/16 :goto_1

    .line 514
    :pswitch_3
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f0803bb

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 515
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f0803bc

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 516
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 517
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f020215

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 518
    invoke-virtual {p0, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->makeProgressBarVisibleAndSetInfoVersion(Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 519
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUploadProgressPercentage(IJ)V

    .line 520
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressData:Landroid/widget/TextView;

    const v8, 0x7f0804f1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 521
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressPercentage:Landroid/widget/TextView;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f0800e7

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 526
    :pswitch_4
    if-eqz p3, :cond_4

    iget-wide v4, p3, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    .line 527
    .local v4, "total":J
    :goto_3
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_5

    long-to-float v7, p4

    long-to-float v8, v4

    div-float/2addr v7, v8

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v7, v8

    float-to-int v1, v7

    .line 528
    .local v1, "percentage":I
    :goto_4
    invoke-direct {p0, v1, p4, p5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUploadProgressPercentage(IJ)V

    .line 529
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f0804c3

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 530
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f0804c4

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 531
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 532
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f020218

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 533
    invoke-virtual {p0, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->makeProgressBarVisibleAndSetInfoVersion(Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 534
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    invoke-direct {p0, v7, v8, v9}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUploadProgressPercentage(IJ)V

    .line 535
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f0800e7

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 526
    .end local v1    # "percentage":I
    .end local v4    # "total":J
    :cond_4
    const-wide/16 v4, 0x0

    goto :goto_3

    .line 527
    .restart local v4    # "total":J
    :cond_5
    const/4 v1, 0x0

    goto :goto_4

    .line 539
    .end local v4    # "total":J
    :pswitch_5
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    const v8, 0x7f0803b9

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 540
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    const v8, 0x7f0803ba

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 541
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 542
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    const v8, 0x7f020214

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 543
    if-eqz p3, :cond_6

    .line 544
    const/16 v7, 0x64

    iget-wide v8, p3, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    invoke-direct {p0, v7, v8, v9}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUploadProgressPercentage(IJ)V

    .line 545
    invoke-virtual {p0, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->makeProgressBarVisibleAndSetInfoVersion(Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 547
    :cond_6
    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    const v8, 0x7f0800e7

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_1

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setUnstableSource(Z)V
    .locals 8
    .param p1, "unstable"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 755
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildSources()[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v1

    .line 756
    .local v1, "sources":[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    if-eqz p1, :cond_0

    move v2, v3

    :goto_0
    aget-object v0, v1, v2

    .line 757
    .local v0, "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->persistUserPreferredBuildSource(Lcom/navdy/client/debug/util/S3Constants$BuildSource;)V

    .line 758
    sget-object v2, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Set to unstable source ? :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 759
    sget-object v2, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting to the source  :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 760
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_STATUS"

    sget-object v6, Lcom/navdy/client/app/ui/settings/SettingsConstants;->OTA_STATUS_DEFAULT:Ljava/lang/String;

    .line 761
    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_VERSION"

    const/4 v6, -0x1

    .line 762
    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_URL"

    const-string v6, ""

    .line 763
    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_DESCRIPTION"

    const-string v6, ""

    .line 764
    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_SIZE"

    const-wide/16 v6, 0x0

    .line 765
    invoke-interface {v2, v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_IS_INCREMENTAL"

    .line 766
    invoke-interface {v2, v5, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "OTA_FROM_VERSION"

    .line 767
    invoke-interface {v2, v5, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 768
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 769
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    const/4 v3, 0x0

    invoke-direct {p0, v4, v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 770
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setupView()V

    .line 771
    return-void

    .end local v0    # "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :cond_0
    move v2, v4

    .line 756
    goto :goto_0
.end method

.method private setUploadProgressPercentage(IJ)V
    .locals 2
    .param p1, "percentage"    # I
    .param p2, "totalUploaded"    # J

    .prologue
    .line 377
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 378
    const/4 p1, 0x1

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar2:Landroid/widget/ProgressBar;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setThisProgressPercentage(Landroid/widget/ProgressBar;IJ)V

    .line 381
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 382
    return-void
.end method

.method private setupView()V
    .locals 6

    .prologue
    .line 434
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->isCheckingForUpdate()Z

    move-result v1

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v2

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v3

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getLastKnownUploadSize()J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;J)V

    .line 435
    return-void
.end method

.method private showReleaseNotes(Z)V
    .locals 7
    .param p1, "more"    # Z

    .prologue
    const/16 v6, 0x8

    .line 790
    sget-object v3, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Show release notes, more : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 791
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v2

    .line 793
    .local v2, "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    :try_start_0
    invoke-static {v2}, Lcom/navdy/client/ota/model/UpdateInfo;->extractReleaseNotes(Lcom/navdy/client/ota/model/UpdateInfo;)Ljava/lang/String;

    move-result-object v1

    .line 794
    .local v1, "releaseNotesText":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 795
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 796
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->moreOrLess:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 797
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->hardwareSupportTitle:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 798
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 800
    if-eqz p1, :cond_1

    .line 801
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->expandAnimationForTextView(Landroid/widget/TextView;)V

    .line 802
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    const v4, 0x7fffffff

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 803
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->moreOrLess:Landroid/widget/TextView;

    const v4, 0x7f080288

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 817
    .end local v1    # "releaseNotesText":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 805
    .restart local v1    # "releaseNotesText":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->collapseAnimationForTextView(Landroid/widget/TextView;)V

    .line 806
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 807
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 808
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->moreOrLess:Landroid/widget/TextView;

    const v4, 0x7f0802cd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 811
    .end local v1    # "releaseNotesText":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 812
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "exception found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 813
    sget-object v3, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "No release notes found"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 814
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->moreOrLess:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 815
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private startDownload()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f080160

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 641
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 642
    .local v0, "appContext":Landroid/content/Context;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v3, :cond_0

    .line 643
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SystemUtils;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 644
    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 645
    .local v2, "title":Ljava/lang/String;
    const v3, 0x7f0800b4

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 646
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {p0, v5, v2, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 664
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 649
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 650
    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 651
    .restart local v2    # "title":Ljava/lang/String;
    const v3, 0x7f0802fe

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 652
    .restart local v1    # "message":Ljava/lang/String;
    invoke-virtual {p0, v5, v2, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 655
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_2
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 656
    const v3, 0x7f080273

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 657
    .restart local v2    # "title":Ljava/lang/String;
    const v3, 0x7f080274

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 658
    .restart local v1    # "message":Ljava/lang/String;
    invoke-virtual {p0, v4, v2, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 660
    .end local v1    # "message":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v4, v7, v7}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setUIState(ZLcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 661
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v3}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->downloadOTAUpdate()V

    goto :goto_0
.end method


# virtual methods
.method public adjustImageContainerHeight()V
    .locals 8

    .prologue
    .line 829
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 830
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 831
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 832
    .local v2, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v3

    const-wide v6, 0x3fd6666666666666L    # 0.35

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->intValue()I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 833
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 838
    .end local v0    # "displayMetrics":Landroid/util/DisplayMetrics;
    .end local v2    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    :goto_0
    return-void

    .line 835
    :catch_0
    move-exception v1

    .line 836
    .local v1, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "There was a problem changing the layout height of the ota container: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public collapseAnimationForTextView(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 824
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 825
    return-void
.end method

.method public createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 668
    invoke-super {p0, p1, p2}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    .local v1, "dialog":Landroid/app/Dialog;
    move-object v0, v1

    .line 669
    check-cast v0, Landroid/app/AlertDialog;

    .line 670
    .local v0, "alert":Landroid/app/AlertDialog;
    if-eqz v1, :cond_0

    .line 671
    packed-switch p1, :pswitch_data_0

    :cond_0
    move-object v0, v1

    .line 690
    .end local v0    # "alert":Landroid/app/AlertDialog;
    :goto_0
    return-object v0

    .line 673
    .restart local v0    # "alert":Landroid/app/AlertDialog;
    :pswitch_0
    const/4 v2, -0x1

    const v3, 0x7f08031e

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 681
    const/4 v2, -0x2

    const v3, 0x7f0800e5

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$7;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$7;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 671
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public expandAnimationForTextView(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 820
    const v0, 0x7fffffff

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 821
    return-void
.end method

.method public makeProgressBarVisibleAndSetInfoVersion(Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 11
    .param p1, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    const v9, 0x7f0f005b

    const/4 v10, 0x0

    .line 554
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "makeProgressBarVisibleAndSetInfoVersion: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 555
    if-eqz p1, :cond_1

    .line 556
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBarContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 561
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-gt v6, v7, :cond_0

    .line 562
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v6}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/LayerDrawable;

    .line 563
    .local v4, "progressBar1LayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    const v6, 0x7f100402

    invoke-virtual {v4, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 564
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    const v6, 0x7f100403

    invoke-virtual {v4, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 565
    .local v2, "progress":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar2:Landroid/widget/ProgressBar;

    invoke-virtual {v6}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/LayerDrawable;

    .line 566
    .local v5, "progressBar2LayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    const v6, 0x7f100404

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 567
    .local v1, "background2":Landroid/graphics/drawable/Drawable;
    const v6, 0x7f100405

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 569
    .local v3, "progress2":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v9}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 570
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0f00ae

    invoke-static {v6, v7}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 571
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v9}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 572
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0f0012

    invoke-static {v6, v7}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 574
    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    .end local v1    # "background2":Landroid/graphics/drawable/Drawable;
    .end local v2    # "progress":Landroid/graphics/drawable/Drawable;
    .end local v3    # "progress2":Landroid/graphics/drawable/Drawable;
    .end local v4    # "progressBar1LayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    .end local v5    # "progressBar2LayerDrawable":Landroid/graphics/drawable/LayerDrawable;
    :cond_0
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->downloadVersion:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 575
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->downloadVersion:Landroid/widget/TextView;

    const-string v7, "v%1$s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/navdy/client/ota/model/UpdateInfo;->getFormattedVersionName(Lcom/navdy/client/ota/model/UpdateInfo;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    :cond_1
    return-void
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 192
    sget-object v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 220
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported state for a button click: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 194
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    goto :goto_0

    .line 197
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->startDownload()V

    goto :goto_0

    .line 200
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->cancelDownload()V

    .line 201
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0, v2}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->toggleAutoDownload(Z)V

    .line 202
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    goto :goto_0

    .line 206
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->cancelUpload()V

    .line 207
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0, v2}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->toggleAutoDownload(Z)V

    .line 208
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->resetUpdate()V

    .line 209
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    goto :goto_0

    .line 216
    :pswitch_4
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->resetUpdate()V

    .line 217
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    const v0, 0x7f0300f9

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setContentView(I)V

    .line 104
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f0802bd

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 107
    const v0, 0x7f10007f

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f100181

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f1003ca

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f10007b

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    .line 111
    const v0, 0x7f10023c

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBarContainer:Landroid/widget/RelativeLayout;

    .line 112
    const v0, 0x7f100161

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 113
    const v0, 0x7f1003ce

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar2:Landroid/widget/ProgressBar;

    .line 114
    const v0, 0x7f1003cf

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressPercentage:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f1003d0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressData:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f100132

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    .line 117
    const v0, 0x7f1003d2

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f1003d3

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->moreOrLess:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f1003d8

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->hudVersion:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f10021d

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->appVersion:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f1003d4

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    .line 122
    const v0, 0x7f1003d1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->hardwareSupportTitle:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f1003cc

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->downloadVersion:Landroid/widget/TextView;

    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->title:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->subtitle2:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->image:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->downloadVersion:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBarContainer:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressBar2:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressPercentage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->progressData:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->button:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->description:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->moreOrLess:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->hudVersion:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->appVersion:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    if-nez v0, :cond_1

    .line 142
    :cond_0
    new-instance v0, Landroid/content/res/Resources$NotFoundException;

    const-string v1, "Layout element"

    invoke-direct {v0, v1}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 148
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setBuildSources()V

    .line 150
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->appVersion:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getAppVersionString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "OTA_UPDATE_UI"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->viewsInitialized:Z

    .line 162
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v0, :cond_2

    .line 163
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->setupView()V

    .line 165
    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 842
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onDestroy()V

    .line 843
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 844
    return-void
.end method

.method public onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V
    .locals 8
    .param p1, "progress"    # Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;
    .param p2, "totalDownloaded"    # J
    .param p4, "percent"    # B

    .prologue
    .line 294
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;

    move-object v1, p0

    move v2, p4

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;BLcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;J)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 339
    return-void
.end method

.method public onErrorCheckingForUpdate(Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V
    .locals 2
    .param p1, "error"    # Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 277
    return-void
.end method

.method public onMoreInfoClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showingMoreInfo:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showingMoreInfo:Z

    .line 227
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showingMoreInfo:Z

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showReleaseNotes(Z)V

    .line 228
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onPause()V

    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->unregisterUIClient()V

    .line 182
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->disconnectService()V

    .line 184
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 170
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->connectToService()V

    .line 172
    const-string v0, "Settings_Update"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->adjustImageContainerHeight()V

    .line 175
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 399
    if-eqz p2, :cond_1

    .line 400
    check-cast p2, Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .end local p2    # "service":Landroid/os/IBinder;
    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .line 401
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1, p0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->registerUIClient(Lcom/navdy/client/ota/OTAUpdateUIClient;)V

    .line 403
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->isCheckingForUpdate()Z

    move-result v1

    if-nez v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getOTAUpdateState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    .line 405
    .local v0, "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq v0, v1, :cond_0

    .line 406
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    .line 410
    .end local v0    # "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->viewsInitialized:Z

    if-eqz v1, :cond_1

    .line 411
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$5;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$5;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 419
    :cond_1
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .line 424
    return-void
.end method

.method public onStateChanged(Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 2
    .param p1, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;
    .param p2, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 290
    return-void
.end method

.method public onUnstableVersionClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 847
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 848
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->buildTypeUnstableSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->performClick()Z

    .line 850
    :cond_0
    return-void
.end method

.method public onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JB)V
    .locals 8
    .param p1, "progress"    # Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;
    .param p2, "totalUploaded"    # J
    .param p4, "percent"    # B

    .prologue
    .line 343
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$4;

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$4;-><init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;BJ)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 369
    return-void
.end method

.method protected saveChanges()V
    .locals 2

    .prologue
    .line 428
    const v0, 0x7f08045b

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 429
    const-string v0, "Ota_Settings_Changed"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 430
    return-void
.end method
