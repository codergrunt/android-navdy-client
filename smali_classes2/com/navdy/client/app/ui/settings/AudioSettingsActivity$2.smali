.class Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;
.super Ljava/lang/Object;
.source "AudioSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

.field final synthetic val$resultantArray:[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    .prologue
    .line 483
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->val$resultantArray:[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 486
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 487
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->access$102(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;Z)Z

    .line 488
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->val$resultantArray:[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;

    aget-object v1, v2, p2

    .line 489
    .local v1, "voiceLabel":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    if-eqz v1, :cond_2

    .line 490
    sget-object v2, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Selected voice :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->voice:Landroid/speech/tts/Voice;

    invoke-virtual {v4}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 491
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    iget-object v2, v2, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getTextToSpeech()Landroid/speech/tts/TextToSpeech;

    move-result-object v2

    iget-object v3, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->voice:Landroid/speech/tts/Voice;

    invoke-virtual {v2, v3}, Landroid/speech/tts/TextToSpeech;->setVoice(Landroid/speech/tts/Voice;)I

    .line 492
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    iget-object v2, v2, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->getTextToSpeech()Landroid/speech/tts/TextToSpeech;

    move-result-object v2

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->getVoice()Landroid/speech/tts/Voice;

    move-result-object v0

    .line 493
    .local v0, "voice":Landroid/speech/tts/Voice;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTxtVoiceName:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v2, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->label:Ljava/lang/String;

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    iget-object v3, v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;->voice:Landroid/speech/tts/Voice;

    invoke-virtual {v3}, Landroid/speech/tts/Voice;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSelectedVoice:Ljava/lang/String;

    .line 499
    .end local v0    # "voice":Landroid/speech/tts/Voice;
    .end local v1    # "voiceLabel":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    :cond_0
    :goto_1
    return-void

    .line 493
    .restart local v0    # "voice":Landroid/speech/tts/Voice;
    .restart local v1    # "voiceLabel":Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 496
    .end local v0    # "voice":Landroid/speech/tts/Voice;
    :cond_2
    sget-object v2, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Selected voice does not exists , total :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$2;->val$resultantArray:[Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$VoiceLabel;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Index :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method
