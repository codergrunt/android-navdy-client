.class public Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "MessagingSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;
    }
.end annotation


# instance fields
.field private adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

.field private repliesRecycler:Landroid/support/v7/widget/RecyclerView;

.field private replyClickListener:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;)Lcom/navdy/client/app/ui/settings/RepliesAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method private convertArrayToString(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "replies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 214
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-object v3

    .line 219
    :cond_1
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 220
    .local v0, "array":Lorg/json/JSONArray;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 221
    .local v2, "object":Lorg/json/JSONObject;
    const-string v4, "replies"

    invoke-virtual {v2, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 222
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 223
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v2    # "object":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 224
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static convertStringToArray(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "repliesString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 177
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v4, v6

    .line 191
    :cond_0
    :goto_0
    return-object v4

    .line 181
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v4, "replies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 184
    .local v3, "object":Lorg/json/JSONObject;
    const-string v5, "replies"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 185
    .local v2, "jsonArray":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 186
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 189
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "object":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v4, v6

    .line 191
    goto :goto_0
.end method

.method public static getRepliesFromSharedPrefs()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v1, "replies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 147
    .local v5, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v6, "messaging_replies"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "repliesString":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 150
    .local v0, "context":Landroid/content/Context;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 151
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 152
    .local v4, "resources":Landroid/content/res/Resources;
    const v6, 0x7f090002

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "repliesArray":[Ljava/lang/String;
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 164
    const-string v6, "\ud83d\udc4d"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v6, "\ud83d\udc4e"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v6, "\ud83d\ude00"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v6, "\ud83d\ude1e"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v6, "\u2764"

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    .end local v2    # "repliesArray":[Ljava/lang/String;
    .end local v4    # "resources":Landroid/content/res/Resources;
    :goto_0
    return-object v1

    .line 170
    :cond_0
    invoke-static {v3}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->convertStringToArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method private saveRepliesToSharedPrefsAndSendToHud(Ljava/util/ArrayList;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "replies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 198
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->convertArrayToString(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "repliesString":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 200
    const/4 v4, 0x0

    .line 209
    :goto_0
    return v4

    .line 202
    :cond_0
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "messaging_replies"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 205
    const-string v4, "nav_serial_number"

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementSerialNumber(Ljava/lang/String;)J

    move-result-wide v2

    .line 208
    .local v2, "serialNumber":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendMessagingSettingsToTheHud(Ljava/util/ArrayList;Ljava/lang/Long;)Z

    .line 209
    const/4 v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 120
    invoke-super {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 122
    const/16 v2, 0x1092

    if-ne p2, v2, :cond_3

    .line 123
    const-string v2, "extra_new_message"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "newMessage":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    .end local v0    # "newMessage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 127
    .restart local v0    # "newMessage":Ljava/lang/String;
    :cond_1
    const-string v2, "extra_old_message"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "oldMessage":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-virtual {v2, v1, v0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->replace(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->somethingChanged:Z

    .line 139
    .end local v0    # "newMessage":Ljava/lang/String;
    .end local v1    # "oldMessage":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->somethingChanged:Z

    if-eqz v2, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->saveChanges()V

    goto :goto_0

    .line 130
    :cond_3
    const/16 v2, 0x1091

    if-ne p2, v2, :cond_2

    .line 131
    const-string v2, "extra_new_message"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .restart local v0    # "newMessage":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 133
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->add(Ljava/lang/String;)V

    .line 134
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->somethingChanged:Z

    .line 135
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->repliesRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->getItemCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_1
.end method

.method public onAddReplyClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 231
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/MessagingSettingsEditDialogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 232
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x1091

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 233
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v3, 0x7f0300f5

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->setContentView(I)V

    .line 53
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v4, 0x7f0802bc

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 56
    const v3, 0x7f1003b9

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->repliesRecycler:Landroid/support/v7/widget/RecyclerView;

    .line 59
    invoke-static {}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->getRepliesFromSharedPrefs()Ljava/util/ArrayList;

    move-result-object v2

    .line 62
    .local v2, "replies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;)V

    iput-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->replyClickListener:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;

    .line 99
    new-instance v3, Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->replyClickListener:Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;

    new-instance v5, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;)V

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v3, v2, v4, v5, v6}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;-><init>(Ljava/util/ArrayList;Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity$ReplyClickListener;Ljava/lang/Runnable;Lcom/navdy/client/app/framework/util/ImageCache;)V

    iput-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    .line 105
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->repliesRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_0

    .line 106
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->repliesRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 107
    new-instance v0, Landroid/support/v7/widget/helper/ItemTouchHelper;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->getItemTouchHelperCallback()Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v7/widget/helper/ItemTouchHelper;-><init>(Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;)V

    .line 108
    .local v0, "itemTouchHelper":Landroid/support/v7/widget/helper/ItemTouchHelper;
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->repliesRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/helper/ItemTouchHelper;->attachToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 109
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v1, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 110
    .local v1, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 111
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->repliesRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 115
    .end local v0    # "itemTouchHelper":Landroid/support/v7/widget/helper/ItemTouchHelper;
    .end local v1    # "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    :cond_0
    const-string v3, "Settings_Messaging"

    invoke-static {v3}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method protected saveChanges()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->adapter:Lcom/navdy/client/app/ui/settings/RepliesAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/settings/RepliesAdapter;->getList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->saveRepliesToSharedPrefsAndSendToHud(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    const v0, 0x7f08043d

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 242
    :goto_0
    return-void

    .line 240
    :cond_0
    const v0, 0x7f08043c

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method
