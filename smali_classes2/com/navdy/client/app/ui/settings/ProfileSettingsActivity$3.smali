.class Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;
.super Ljava/lang/Object;
.source "ProfileSettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->doProfileOnCreate(Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 135
    if-nez p2, :cond_0

    .line 136
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->access$200(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Z

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v2, v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_0
.end method
