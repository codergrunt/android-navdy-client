.class Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;
.super Ljava/lang/Object;
.source "CalendarGlobalSwitchViewHolder.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;-><init>(Landroid/view/View;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;

.field final synthetic val$checkedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;->this$0:Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;->val$checkedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 46
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 47
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    if-nez v0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "calendars_enabled"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 53
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;->val$checkedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/CalendarGlobalSwitchViewHolder$2;->val$checkedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-interface {v1, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    goto :goto_0
.end method
