.class public Lcom/navdy/client/app/ui/settings/ZendeskConstants;
.super Ljava/lang/Object;
.source "ZendeskConstants.java"


# static fields
.field public static final ACTION_FETCH_HUD_LOG_UPLOAD_DELETE:Ljava/lang/String; = "fetch_hud_log_upload_delete"

.field public static final ACTION_UPLOAD_AWAIT_HUD_LOG:Ljava/lang/String; = "upload_await_hud_log"

.field public static final ACTION_UPLOAD_DELETE:Ljava/lang/String; = "upload_delete"

.field public static final DISPLAY_LOG:Ljava/lang/String; = "display_log"

.field public static final DISPLAY_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DISPLAY_SOFTWARE_VERSION:Ljava/lang/Long;

.field public static final LOCALE:Ljava/lang/Long;

.field public static final MOBILE_APP_NAME:Ljava/lang/Long;

.field public static final MOBILE_APP_VERSION:Ljava/lang/Long;

.field public static final MOBILE_DEVICE_MAKE:Ljava/lang/Long;

.field public static final MOBILE_DEVICE_MODEL:Ljava/lang/Long;

.field public static final MOBILE_OS_VERSION:Ljava/lang/Long;

.field public static final PROBLEM_TYPES:Ljava/lang/Long;

.field public static final TICKET_ACTION:Ljava/lang/String; = "ticket_action"

.field public static final TICKET_ATTACHMENTS_FILENAME:Ljava/lang/String; = "attachments.zip"

.field public static final TICKET_DESCRIPTION:Ljava/lang/String; = "ticket_description"

.field public static final TICKET_EMAIL:Ljava/lang/String; = "ticket_email"

.field public static final TICKET_HAS_DISPLAY_LOG:Ljava/lang/String; = "ticket_has_display_log"

.field public static final TICKET_JSON_FILENAME:Ljava/lang/String; = "ticket_contents.txt"

.field public static final TICKET_TYPE:Ljava/lang/String; = "ticket_type"

.field public static final TICKET_VIN:Ljava/lang/String; = "ticket_vin"

.field public static final VEHICLE_MAKE:Ljava/lang/Long;

.field public static final VEHICLE_MODEL:Ljava/lang/Long;

.field public static final VEHICLE_YEAR:Ljava/lang/Long;

.field public static final VIN:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    const-wide/32 v0, 0x1710f6d

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_APP_VERSION:Ljava/lang/Long;

    .line 12
    const-wide/32 v0, 0x1710f3b

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_APP_NAME:Ljava/lang/Long;

    .line 13
    const-wide/32 v0, 0x171b1c8

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VEHICLE_YEAR:Ljava/lang/Long;

    .line 14
    const-wide/32 v0, 0x171b13c

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VEHICLE_MODEL:Ljava/lang/Long;

    .line 15
    const-wide/32 v0, 0x1724ee1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VEHICLE_MAKE:Ljava/lang/Long;

    .line 16
    const-wide/32 v0, 0x171b1d2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->VIN:Ljava/lang/Long;

    .line 17
    const-wide/32 v0, 0x171b1be

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->DISPLAY_SOFTWARE_VERSION:Ljava/lang/Long;

    .line 18
    const-wide/32 v0, 0x17189a7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->DISPLAY_SERIAL_NUMBER:Ljava/lang/Long;

    .line 19
    const-wide/32 v0, 0x171b1e6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->PROBLEM_TYPES:Ljava/lang/Long;

    .line 20
    const-wide/32 v0, 0x1710f81

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->LOCALE:Ljava/lang/Long;

    .line 21
    const-wide/32 v0, 0x175aa9b

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_OS_VERSION:Ljava/lang/Long;

    .line 22
    const-wide/32 v0, 0x175aa87

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_DEVICE_MAKE:Ljava/lang/Long;

    .line 23
    const-wide/32 v0, 0x1764c7e

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/ZendeskConstants;->MOBILE_DEVICE_MODEL:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
