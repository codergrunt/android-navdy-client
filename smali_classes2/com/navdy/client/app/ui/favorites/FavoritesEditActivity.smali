.class public Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "FavoritesEditActivity.java"


# static fields
.field public static final EXTRA_FAVORITE:Ljava/lang/String; = "favorite"


# instance fields
.field private destination:Lcom/navdy/client/app/framework/models/Destination;

.field private googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field private label:Landroid/widget/EditText;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private myToolbar:Landroid/support/v7/widget/Toolbar;

.field private offlineBanner:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    .line 52
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->logger:Lcom/navdy/service/library/log/Logger;

    .line 63
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->saveOnExit:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static startFavoriteEditActivity(Landroid/app/Activity;Landroid/os/Parcelable;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "favorite"    # Landroid/os/Parcelable;

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "favorite"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x3

    .line 75
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v5, 0x7f030056

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->setContentView(I)V

    .line 77
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 79
    new-instance v5, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v6, 0x7f08014b

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    .line 82
    const v5, 0x7f10014d

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->label:Landroid/widget/EditText;

    .line 83
    const v5, 0x7f10010d

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    .local v0, "address":Landroid/widget/TextView;
    const v5, 0x7f1000aa

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    .line 86
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const v6, 0x7f100147

    .line 87
    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 90
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 91
    .local v2, "in":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 92
    const-string v5, "favorite"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/navdy/client/app/framework/models/Destination;

    iput-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 96
    :cond_0
    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v5, :cond_1

    .line 97
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Destination;->getAddressLines()Ljava/util/ArrayList;

    move-result-object v1

    .line 98
    .local v1, "addressLines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "\n"

    invoke-static {v1, v5}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 99
    .local v4, "multilineAddress":Ljava/lang/String;
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    .end local v1    # "addressLines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "multilineAddress":Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->label:Landroid/widget/EditText;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v5, :cond_3

    .line 102
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget v5, v5, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    if-eq v5, v7, :cond_2

    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget v5, v5, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v6, -0x2

    if-ne v5, v6, :cond_7

    .line 104
    :cond_2
    const v5, 0x7f10014e

    invoke-virtual {p0, v5}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 105
    .local v3, "labelText":Landroid/widget/TextView;
    if-eqz v3, :cond_3

    .line 106
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->label:Landroid/widget/EditText;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 107
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget v5, v5, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    if-ne v5, v7, :cond_6

    .line 108
    const v5, 0x7f08023f

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 112
    :goto_0
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    .end local v3    # "labelText":Landroid/widget/TextView;
    :cond_3
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v5

    if-nez v5, :cond_4

    .line 138
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 142
    :cond_4
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v5, :cond_5

    .line 143
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v6, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$2;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 179
    :cond_5
    return-void

    .line 110
    .restart local v3    # "labelText":Landroid/widget/TextView;
    :cond_6
    const v5, 0x7f080503

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 115
    .end local v3    # "labelText":Landroid/widget/TextView;
    :cond_7
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->label:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v5, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->label:Landroid/widget/EditText;

    new-instance v6, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$1;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$1;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 193
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 194
    .local v0, "applicationContext":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "1"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 196
    iget-object v2, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f100421

    const v4, 0x7f080121

    .line 197
    invoke-interface {v2, v5, v3, v5, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f0200f1

    .line 198
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v3, 0x2

    .line 199
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 200
    return v6
.end method

.method public onDelete(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isPersisted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to delete a favorite that is not in DB: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 223
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->showProgressDialog()V

    .line 225
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$3;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 246
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 205
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 213
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 207
    :sswitch_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->onBackPressed()V

    goto :goto_0

    .line 210
    :sswitch_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->onDelete(Landroid/view/View;)V

    goto :goto_0

    .line 205
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f100421 -> :sswitch_1
    .end sparse-switch
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 304
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 305
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 185
    const-string v0, "Edit_Favorites"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method protected saveChanges()V
    .locals 3

    .prologue
    .line 250
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->saveOnExit:Z

    if-nez v1, :cond_0

    .line 301
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget v1, v1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-nez v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v2, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->reloadSelfFromDatabaseAsync(Ljava/lang/Runnable;)V

    .line 269
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->label:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "labelString":Ljava/lang/String;
    new-instance v1, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;-><init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;Ljava/lang/String;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 300
    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
