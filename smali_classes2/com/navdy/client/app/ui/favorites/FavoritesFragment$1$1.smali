.class Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;
.super Ljava/lang/Object;
.source "FavoritesFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;

.field final synthetic val$activity:Landroid/support/v4/app/FragmentActivity;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$setDestinationTracker:Lcom/navdy/client/app/tracking/SetDestinationTracker;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;Lcom/navdy/client/app/tracking/SetDestinationTracker;Lcom/navdy/client/app/framework/models/Destination;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->this$1:Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;

    iput-object p2, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$setDestinationTracker:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    iput-object p3, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p4, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$setDestinationTracker:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->tagSetDestinationEvent(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 88
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->this$1:Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->access$200(Lcom/navdy/client/app/ui/favorites/FavoritesFragment;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 90
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    instance-of v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment$1$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 95
    :cond_0
    return-void
.end method
