.class public Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "AppNotificationsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;
    }
.end annotation


# instance fields
.field private listAdaptor:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

.field private packageManager:Landroid/content/pm/PackageManager;

.field private sharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->listAdaptor:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->packageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->removeAppsThatWeDontWant(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->listAdaptor:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;)Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->listAdaptor:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    return-object p1
.end method

.method private removeAppsThatWeDontWant(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ApplicationInfo;

    .line 146
    .local v2, "info":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->packageManager:Landroid/content/pm/PackageManager;

    iget-object v5, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 148
    invoke-static {v4}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isWhiteListedApp(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 149
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 156
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_1
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f030029

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->setContentView(I)V

    .line 41
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v2, 0x7f080314

    .line 42
    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 45
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->packageManager:Landroid/content/pm/PackageManager;

    .line 46
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 48
    const v1, 0x7f1000f7

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 49
    .local v0, "appList":Landroid/widget/ListView;
    if-eqz v0, :cond_0

    .line 50
    const v1, 0x7f1000f8

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 52
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 57
    new-instance v0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;-><init>(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 59
    const-string v0, "Notification_Glances"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 60
    return-void
.end method
