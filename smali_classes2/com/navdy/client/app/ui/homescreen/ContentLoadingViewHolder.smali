.class Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ContentLoadingViewHolder.java"


# instance fields
.field private logger:Lcom/navdy/service/library/log/Logger;

.field private loopAnimation:Landroid/graphics/drawable/AnimationDrawable;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 16
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 17
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    .line 22
    const v1, 0x7f1000b1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 23
    .local v0, "image":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 26
    :cond_0
    return-void
.end method


# virtual methods
.method startAnimation()V
    .locals 3

    .prologue
    .line 29
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting animation - loopAnimation: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " isRunning: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 33
    :cond_0
    return-void

    .line 29
    :cond_1
    const-string v0, "null"

    goto :goto_0
.end method

.method stopAnimation()V
    .locals 3

    .prologue
    .line 36
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopping animation - loopAnimation: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " isRunning: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/ContentLoadingViewHolder;->loopAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 40
    :cond_0
    return-void

    .line 36
    :cond_1
    const-string v0, "null"

    goto :goto_0
.end method
