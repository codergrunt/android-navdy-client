.class public Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "PermissionFailedActivity.java"


# static fields
.field public static final EXTRA_BUTTON_RES:Ljava/lang/String; = "extra_button_res"

.field public static final EXTRA_DESCRIPTION_RES:Ljava/lang/String; = "extra_description_res"

.field public static final EXTRA_TITLE_RES:Ljava/lang/String; = "extra_title_res"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onButtonClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->finish()V

    .line 63
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 26
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v8, 0x7f0300ba

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->setContentView(I)V

    .line 29
    const v8, 0x7f10007f

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 30
    .local v6, "title":Landroid/widget/TextView;
    const v8, 0x7f10012f

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 31
    .local v3, "description":Landroid/widget/TextView;
    const v8, 0x7f10016c

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 32
    .local v0, "blueText":Landroid/widget/TextView;
    const v8, 0x7f10016d

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 34
    .local v1, "button":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 36
    .local v5, "intent":Landroid/content/Intent;
    const-string v8, "extra_title_res"

    const v9, 0x7f08019e

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 37
    .local v7, "titleRes":I
    const-string v8, "extra_description_res"

    const v9, 0x7f08019d

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 38
    .local v4, "descriptionRes":I
    const-string v8, "extra_button_res"

    const v9, 0x7f08019c

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 40
    .local v2, "buttonTextRes":I
    if-eqz v6, :cond_0

    .line 41
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 42
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 44
    :cond_0
    if-eqz v3, :cond_1

    .line 45
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    :cond_1
    if-eqz v0, :cond_2

    .line 49
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 51
    :cond_2
    if-eqz v1, :cond_3

    .line 52
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 53
    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 55
    :cond_3
    return-void
.end method

.method public onDescriptionClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->goToSystemSettingsAppInfoForOurApp(Landroid/app/Activity;)V

    .line 59
    return-void
.end method
