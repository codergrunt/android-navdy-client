.class public Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;
.super Landroid/support/v7/widget/AppCompatTextView;
.source "UnitSystemTextView.java"

# interfaces
.implements Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final context:Landroid/content/Context;

.field private currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

.field private currentValue:D

.field private final i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->context:Landroid/content/Context;

    .line 38
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getInstance()Lcom/navdy/client/app/framework/i18n/I18nManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    .line 40
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 41
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/i18n/I18nManager;->addListener(Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;)V

    .line 42
    return-void
.end method

.method private round(D)D
    .locals 5
    .param p1, "value"    # D

    .prologue
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    .line 73
    mul-double v0, p1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public isInEditMode()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->i18nManager:Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/i18n/I18nManager;->removeListener(Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;)V

    .line 69
    invoke-super {p0}, Landroid/support/v7/widget/AppCompatTextView;->onDetachedFromWindow()V

    .line 70
    return-void
.end method

.method public onUnitSystemChanged(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V
    .locals 3
    .param p1, "unitSystem"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .prologue
    .line 60
    sget-object v0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unit system changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", setting new value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 62
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 63
    iget-wide v0, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->currentValue:D

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setDistance(D)V

    .line 64
    return-void
.end method

.method public setDistance(D)V
    .locals 7
    .param p1, "meters"    # D

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    if-ne v1, v2, :cond_0

    .line 48
    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v2, p1

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->round(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "convertedValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->context:Landroid/content/Context;

    const v2, 0x7f080272

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    :goto_0
    iput-wide p1, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->currentValue:D

    .line 56
    return-void

    .line 51
    .end local v0    # "convertedValue":Ljava/lang/String;
    :cond_0
    const-wide v2, 0x3f445c700fd4d6a9L    # 6.21371E-4

    mul-double/2addr v2, p1

    invoke-direct {p0, v2, v3}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->round(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 52
    .restart local v0    # "convertedValue":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->context:Landroid/content/Context;

    const v2, 0x7f0802c8

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
