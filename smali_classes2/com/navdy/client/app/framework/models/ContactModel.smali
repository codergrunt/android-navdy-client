.class public Lcom/navdy/client/app/framework/models/ContactModel;
.super Ljava/lang/Object;
.source "ContactModel.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public addresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Address;",
            ">;"
        }
    .end annotation
.end field

.field public favorite:Z

.field public id:J

.field public lookupKey:Ljava/lang/String;

.field public lookupTimestamp:J

.field public name:Ljava/lang/String;

.field public phoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/PhoneNumberModel;",
            ">;"
        }
    .end annotation
.end field

.field public photo:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/ContactModel;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/ContactModel;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->lookupTimestamp:J

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public addAddress(Lcom/navdy/client/app/framework/models/Address;)V
    .locals 1
    .param p1, "address"    # Lcom/navdy/client/app/framework/models/Address;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method public addAddresses(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 87
    return-void
.end method

.method public addPhoneNumber(Lcom/navdy/client/app/framework/models/PhoneNumberModel;)V
    .locals 1
    .param p1, "number"    # Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public addPhoneNumbers(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/PhoneNumberModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "numbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    if-eqz p1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 97
    :cond_0
    return-void
.end method

.method public deduplicatePhoneNumbers()V
    .locals 9

    .prologue
    .line 118
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    if-nez v5, :cond_0

    .line 137
    :goto_0
    return-void

    .line 121
    :cond_0
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 123
    .local v2, "numbers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    .line 124
    .local v3, "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    iget-object v6, v3, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 126
    :try_start_0
    iget-object v6, v3, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SystemUtils;->convertToNumber(Ljava/lang/String;)J

    move-result-wide v0

    .line 127
    .local v0, "number":J
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 128
    .end local v0    # "number":J
    :catch_0
    move-exception v4

    .line 129
    .local v4, "t":Ljava/lang/Exception;
    sget-object v6, Lcom/navdy/client/app/framework/models/ContactModel;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to convert this to a phone number: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 130
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 129
    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 135
    .end local v3    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    .end local v4    # "t":Ljava/lang/Exception;
    :cond_2
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 136
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 50
    instance-of v5, p1, Lcom/navdy/client/app/framework/models/ContactModel;

    if-nez v5, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v4

    :cond_1
    move-object v3, p1

    .line 54
    check-cast v3, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 56
    .local v3, "otherContact":Lcom/navdy/client/app/framework/models/ContactModel;
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    iget-object v6, v3, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ne v5, v6, :cond_0

    .line 61
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    .line 62
    .local v0, "address":Lcom/navdy/client/app/framework/models/Address;
    const/4 v2, 0x0

    .line 63
    .local v2, "found":Z
    iget-object v6, v3, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/Address;

    .line 64
    .local v1, "address2":Lcom/navdy/client/app/framework/models/Address;
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Address;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 65
    const/4 v2, 0x1

    goto :goto_1

    .line 68
    .end local v1    # "address2":Lcom/navdy/client/app/framework/models/Address;
    :cond_4
    if-nez v2, :cond_2

    goto :goto_0

    .line 73
    .end local v0    # "address":Lcom/navdy/client/app/framework/models/Address;
    .end local v2    # "found":Z
    :cond_5
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iget-object v6, v3, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    iget-wide v8, v3, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    long-to-int v0, v0

    return v0
.end method

.method public setID(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 103
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    return-void
.end method

.method public setLookupKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->lookupKey:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setLookupTimestampToNow()V
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/ContactModel;->lookupTimestamp:J

    .line 111
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setPhoto(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "photo"    # Landroid/graphics/Bitmap;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    .line 115
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContactModel{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lookupKey=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->lookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lookupTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/ContactModel;->lookupTimestamp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", addresses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phoneNumbers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", photo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", favorite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/client/app/framework/models/ContactModel;->favorite:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
