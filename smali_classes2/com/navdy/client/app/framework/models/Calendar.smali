.class public Lcom/navdy/client/app/framework/models/Calendar;
.super Ljava/lang/Object;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
    }
.end annotation


# instance fields
.field public accountName:Ljava/lang/String;

.field public accountType:Ljava/lang/String;

.field public calendarColor:I

.field public displayName:Ljava/lang/String;

.field public id:J

.field public ownerAccount:Ljava/lang/String;

.field public type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

.field public visible:Z


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;)V
    .locals 2
    .param p1, "type"    # Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->calendarColor:I

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountName:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountType:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->ownerAccount:Ljava/lang/String;

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    .line 38
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Calendar;->type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "type"    # Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
    .param p2, "id"    # J
    .param p4, "displayName"    # Ljava/lang/String;
    .param p5, "calendarColor"    # I
    .param p6, "accountName"    # Ljava/lang/String;
    .param p7, "accountType"    # Ljava/lang/String;
    .param p8, "ownerAccount"    # Ljava/lang/String;
    .param p9, "visible"    # Z

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->calendarColor:I

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountName:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountType:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->ownerAccount:Ljava/lang/String;

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    .line 47
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Calendar;->type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .line 48
    iput-wide p2, p0, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    .line 49
    iput-object p4, p0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    .line 50
    iput p5, p0, Lcom/navdy/client/app/framework/models/Calendar;->calendarColor:I

    .line 51
    iput-object p6, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountName:Ljava/lang/String;

    .line 52
    iput-object p7, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountType:Ljava/lang/String;

    .line 53
    iput-object p8, p0, Lcom/navdy/client/app/framework/models/Calendar;->ownerAccount:Ljava/lang/String;

    .line 54
    iput-boolean p9, p0, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
    .param p2, "displayName"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->calendarColor:I

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountName:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountType:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->ownerAccount:Ljava/lang/String;

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    .line 42
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Calendar;->type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .line 43
    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Calendar{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->type:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", displayName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", calendarColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->calendarColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", calendarName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->accountType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ownerAccount=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->ownerAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", visible=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
