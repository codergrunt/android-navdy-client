.class public Lcom/navdy/client/app/framework/models/Destination;
.super Ljava/lang/Object;
.source "Destination.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;,
        Lcom/navdy/client/app/framework/models/Destination$SearchType;,
        Lcom/navdy/client/app/framework/models/Destination$Type;,
        Lcom/navdy/client/app/framework/models/Destination$Precision;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation
.end field

.field private static final DESTINATION_NOT_FOUND:I = -0x1

.field private static final DISTANCE_LIMIT:F = 5.0f

.field public static final IDENTIFIER_CONTACT_ID_SEPARATOR:Ljava/lang/String; = "|"

.field public static final IDENTIFIER_NAMESPACE_CONTACT:Ljava/lang/String; = "contact:"

.field public static final IDENTIFIER_NAMESPACE_DB_ID:Ljava/lang/String; = "db-id:"

.field public static final IDENTIFIER_NAMESPACE_PLACE_ID:Ljava/lang/String; = "google-places-id:"

.field private static final PLACE_ID_REFRESH_LIMIT:J = 0x9a7ec800L

.field private static final VERBOSE:Z

.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field city:Ljava/lang/String;

.field public contactLookupKey:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field private countryCode:Ljava/lang/String;

.field public displayLat:D

.field public displayLong:D

.field public doNotSuggest:Z

.field private favoriteLabel:Ljava/lang/String;

.field private favoriteOrder:I

.field public favoriteType:I

.field private hasBeenProcessed:Z

.field public id:I

.field private isCalendarEvent:Z

.field public lastContactLookup:J

.field public lastKnownContactId:J

.field lastPlaceIdRefresh:J

.field public lastRoutedDate:J

.field public name:Ljava/lang/String;

.field public navigationLat:D

.field public navigationLong:D

.field public placeDetailJson:Ljava/lang/String;

.field public placeId:Ljava/lang/String;

.field public precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

.field public rawAddressNotForDisplay:Ljava/lang/String;

.field public rawAddressVariations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public searchResultType:I

.field public state:Ljava/lang/String;

.field public streetName:Ljava/lang/String;

.field public streetNumber:Ljava/lang/String;

.field public type:Lcom/navdy/client/app/framework/models/Destination$Type;

.field zipCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    .line 561
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$1;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination$1;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 138
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 139
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 140
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 143
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 144
    const-string v0, "US"

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 145
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 146
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    .line 156
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 157
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 158
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 160
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    .line 315
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;DDDDLjava/lang/String;Ljava/lang/String;)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "displayLat"    # D
    .param p5, "displayLong"    # D
    .param p7, "navigationLat"    # D
    .param p9, "navigationLong"    # D
    .param p11, "name"    # Ljava/lang/String;
    .param p12, "rawAddressNotForDisplay"    # Ljava/lang/String;

    .prologue
    .line 391
    const-wide/16 v4, 0x0

    const-string v16, ""

    const-string v17, ""

    const-string v18, ""

    const-string v19, ""

    const-string v20, ""

    const-string v21, ""

    const-string v22, ""

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 392
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;DDDDLjava/lang/String;Ljava/lang/String;J)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "displayLat"    # D
    .param p5, "displayLong"    # D
    .param p7, "navigationLat"    # D
    .param p9, "navigationLong"    # D
    .param p11, "name"    # Ljava/lang/String;
    .param p12, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p13, "lastRoutedDate"    # J

    .prologue
    .line 401
    const-wide/16 v4, 0x0

    const-string v16, ""

    const-string v17, ""

    const-string v18, ""

    const-string v19, ""

    const-string v20, ""

    const-string v21, ""

    const-string v22, ""

    const/16 v23, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-wide/from16 v24, p13

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 402
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;DDDDLjava/lang/String;Ljava/lang/String;JLcom/navdy/client/app/framework/models/Destination$Type;)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "displayLat"    # D
    .param p5, "displayLong"    # D
    .param p7, "navigationLat"    # D
    .param p9, "navigationLong"    # D
    .param p11, "name"    # Ljava/lang/String;
    .param p12, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p13, "lastRoutedDate"    # J
    .param p15, "type"    # Lcom/navdy/client/app/framework/models/Destination$Type;

    .prologue
    .line 406
    const-wide/16 v4, 0x0

    const-string v16, ""

    const-string v17, ""

    const-string v18, ""

    const-string v19, ""

    const-string v20, ""

    const-string v21, ""

    const-string v22, ""

    const/16 v23, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-wide/from16 v24, p13

    move-object/from16 v31, p15

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 407
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;DDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "displayLat"    # D
    .param p5, "displayLong"    # D
    .param p7, "navigationLat"    # D
    .param p9, "navigationLong"    # D
    .param p11, "name"    # Ljava/lang/String;
    .param p12, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p13, "favoriteLabel"    # Ljava/lang/String;
    .param p14, "favoriteOrder"    # I
    .param p15, "favoriteType"    # I

    .prologue
    .line 415
    const-wide/16 v4, 0x0

    const-string v16, ""

    const-string v17, ""

    const-string v18, ""

    const-string v19, ""

    const-string v20, ""

    const-string v21, ""

    const-string v22, ""

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-wide/from16 v12, p9

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v26, p13

    move/from16 v27, p14

    move/from16 v28, p15

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 416
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "lastPlaceIdRefresh"    # J
    .param p5, "displayLat"    # D
    .param p7, "displayLong"    # D
    .param p9, "navigationLat"    # D
    .param p11, "navigationLong"    # D
    .param p13, "name"    # Ljava/lang/String;
    .param p14, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p15, "streetNumber"    # Ljava/lang/String;
    .param p16, "streetName"    # Ljava/lang/String;
    .param p17, "city"    # Ljava/lang/String;
    .param p18, "state"    # Ljava/lang/String;
    .param p19, "zipCode"    # Ljava/lang/String;
    .param p20, "country"    # Ljava/lang/String;
    .param p21, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 396
    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 397
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "lastPlaceIdRefresh"    # J
    .param p5, "displayLat"    # D
    .param p7, "displayLong"    # D
    .param p9, "navigationLat"    # D
    .param p11, "navigationLong"    # D
    .param p13, "name"    # Ljava/lang/String;
    .param p14, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p15, "streetNumber"    # Ljava/lang/String;
    .param p16, "streetName"    # Ljava/lang/String;
    .param p17, "city"    # Ljava/lang/String;
    .param p18, "state"    # Ljava/lang/String;
    .param p19, "zipCode"    # Ljava/lang/String;
    .param p20, "country"    # Ljava/lang/String;
    .param p21, "countryCode"    # Ljava/lang/String;
    .param p22, "lastRoutedDate"    # J

    .prologue
    .line 411
    const/16 v23, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-wide/from16 v24, p22

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 412
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 37
    .param p1, "id"    # I
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "lastPlaceIdRefresh"    # J
    .param p5, "displayLat"    # D
    .param p7, "displayLong"    # D
    .param p9, "navigationLat"    # D
    .param p11, "navigationLong"    # D
    .param p13, "name"    # Ljava/lang/String;
    .param p14, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p15, "streetNumber"    # Ljava/lang/String;
    .param p16, "streetName"    # Ljava/lang/String;
    .param p17, "city"    # Ljava/lang/String;
    .param p18, "state"    # Ljava/lang/String;
    .param p19, "zipCode"    # Ljava/lang/String;
    .param p20, "country"    # Ljava/lang/String;
    .param p21, "countryCode"    # Ljava/lang/String;
    .param p22, "favoriteLabel"    # Ljava/lang/String;
    .param p23, "favoriteOrder"    # I
    .param p24, "favoriteType"    # I

    .prologue
    .line 420
    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move/from16 v2, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    move-wide/from16 v12, p11

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v26, p22

    move/from16 v27, p23

    move/from16 v28, p24

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 421
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "placeIdentifier"    # Ljava/lang/String;
    .param p3, "lastPlaceIdRefresh"    # J
    .param p5, "displayLat"    # D
    .param p7, "displayLong"    # D
    .param p9, "navigationLat"    # D
    .param p11, "navigationLong"    # D
    .param p13, "name"    # Ljava/lang/String;
    .param p14, "rawAddressNotForDisplay"    # Ljava/lang/String;
    .param p15, "streetNumber"    # Ljava/lang/String;
    .param p16, "streetName"    # Ljava/lang/String;
    .param p17, "city"    # Ljava/lang/String;
    .param p18, "state"    # Ljava/lang/String;
    .param p19, "zipCode"    # Ljava/lang/String;
    .param p20, "country"    # Ljava/lang/String;
    .param p21, "countryCode"    # Ljava/lang/String;
    .param p22, "doNotSuggest"    # Z
    .param p23, "lastRoutedDate"    # J
    .param p25, "favoriteLabel"    # Ljava/lang/String;
    .param p26, "favoriteOrder"    # I
    .param p27, "favoriteType"    # I
    .param p28, "placeDetailJson"    # Ljava/lang/String;
    .param p29, "precisionLevel"    # Lcom/navdy/client/app/framework/models/Destination$Precision;
    .param p30, "type"    # Lcom/navdy/client/app/framework/models/Destination$Type;
    .param p31, "contactLookupKey"    # Ljava/lang/String;
    .param p32, "lastKnownContatId"    # J
    .param p34, "lastContactLookup"    # J

    .prologue
    .line 448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 137
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 138
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 139
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 140
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 141
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 142
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 143
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 144
    const-string v2, "US"

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 145
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 146
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    .line 156
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 157
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 158
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    iput v2, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 160
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 167
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    .line 449
    iput p1, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 450
    iput-wide p3, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    .line 451
    iput-wide p5, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 452
    iput-wide p7, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 453
    iput-wide p9, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 454
    iput-wide p11, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 455
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 456
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 457
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 458
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 459
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 460
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 461
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 462
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 463
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 464
    move/from16 v0, p22

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    .line 465
    move-wide/from16 v0, p23

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 466
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 467
    move/from16 v0, p26

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 468
    move/from16 v0, p27

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 469
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 470
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 471
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 472
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 473
    move-wide/from16 v0, p32

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    .line 474
    move-wide/from16 v0, p34

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    .line 475
    invoke-virtual {p0, p2}, Lcom/navdy/client/app/framework/models/Destination;->setIdentifier(Ljava/lang/String;)V

    .line 476
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 138
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 139
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 140
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 143
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 144
    const-string v0, "US"

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 145
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 146
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    .line 156
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 157
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 158
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 160
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 320
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    .line 321
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 324
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 325
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 328
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 329
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    .line 335
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 337
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 338
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 339
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 340
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 341
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/models/Destination$Precision;->get(I)Lcom/navdy/client/app/framework/models/Destination$Precision;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 342
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/models/Destination$Type;->get(I)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 343
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 344
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 345
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    .line 346
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    .line 347
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 348
    return-void

    :cond_0
    move v0, v2

    .line 334
    goto :goto_0

    :cond_1
    move v1, v2

    .line 343
    goto :goto_1
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 443
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/models/Destination;->mergeWith(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 444
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 19
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 435
    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    const-wide/16 v16, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v2}, Lcom/navdy/client/app/framework/models/Destination;->fromProtobufPlaceType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v18

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v18}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;DDDDLjava/lang/String;Ljava/lang/String;JLcom/navdy/client/app/framework/models/Destination$Type;)V

    .line 436
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v2}, Lcom/navdy/client/app/framework/models/Destination;->fromProtobufPlaceType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/models/Destination$Type;->CALENDAR_EVENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-ne v2, v3, :cond_0

    .line 437
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    .line 439
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V
    .locals 14
    .param p1, "navigationRouteResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 424
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 426
    invoke-static {p1}, Lcom/navdy/client/app/framework/models/Destination;->getLatitude(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)D

    move-result-wide v4

    .line 427
    invoke-static {p1}, Lcom/navdy/client/app/framework/models/Destination;->getLongitude(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)D

    move-result-wide v6

    .line 428
    invoke-static {p1}, Lcom/navdy/client/app/framework/models/Destination;->getLatitude(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)D

    move-result-wide v8

    .line 429
    invoke-static {p1}, Lcom/navdy/client/app/framework/models/Destination;->getLongitude(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)D

    move-result-wide v10

    iget-object v12, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    iget-object v13, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    move-object v1, p0

    .line 424
    invoke-direct/range {v1 .. v13}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;DDDDLjava/lang/String;Ljava/lang/String;)V

    .line 432
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 37
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "rawAddressNotForDisplay"    # Ljava/lang/String;

    .prologue
    .line 386
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-string v16, ""

    const-string v17, ""

    const-string v18, ""

    const-string v19, ""

    const-string v20, ""

    const-string v21, ""

    const-string v22, ""

    const/16 v23, 0x0

    const-wide/16 v24, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const-string v29, ""

    sget-object v30, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v31, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    const/16 v32, 0x0

    const-wide/16 v33, -0x1

    const-wide/16 v35, -0x1

    move-object/from16 v1, p0

    move-object/from16 v14, p1

    move-object/from16 v15, p2

    invoke-direct/range {v1 .. v36}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 387
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->persistPlaceDetailInfoAndUpdateLists()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->updateDoNotSuggest()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/models/Destination;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->saveToDb()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/models/Destination;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->deleteFromDb()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/models/Destination;I)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "x1"    # I

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/models/Destination;->saveDestinationAsFavorite(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/models/Destination;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->deleteFavoriteFromDb()I

    move-result v0

    return v0
.end method

.method public static convertDestinationsToProto(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1588
    .local p0, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1589
    :cond_0
    const/4 v1, 0x0

    .line 1600
    :cond_1
    return-object v1

    .line 1593
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1595
    .local v1, "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 1596
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_3

    .line 1597
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private deDuplicatePhoneNumbersWhileKeepingOrder(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1533
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_2

    :cond_0
    move-object v2, p1

    .line 1561
    :cond_1
    return-object v2

    .line 1537
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 1539
    .local v2, "dedupped":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 1540
    const/4 v3, 0x0

    .line 1541
    .local v3, "foundDuplicate":Z
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    .line 1543
    .local v7, "number":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, v7}, Lcom/navdy/client/app/framework/models/Destination;->getComparablePhoneNumber(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1544
    .local v6, "num":Ljava/lang/String;
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1539
    :cond_3
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1548
    :cond_4
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    if-ge v5, v4, :cond_5

    .line 1549
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1550
    .local v0, "compareTo":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->getComparablePhoneNumber(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1551
    .local v1, "compareToNum":Ljava/lang/String;
    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1552
    const/4 v3, 0x1

    .line 1556
    .end local v0    # "compareTo":Ljava/lang/Object;, "TT;"
    .end local v1    # "compareToNum":Ljava/lang/String;
    :cond_5
    if-nez v3, :cond_3

    .line 1557
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1548
    .restart local v0    # "compareTo":Ljava/lang/Object;, "TT;"
    .restart local v1    # "compareToNum":Ljava/lang/String;
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method private deleteFavoriteFromDb()I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2873
    const/4 v0, 0x0

    .line 2876
    .local v0, "favoritesCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 2877
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 2879
    .local v1, "newOrder":I
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 2882
    iget v2, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 2884
    .local v2, "oldOrder":I
    sget-object v3, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Moving favorite from position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2885
    iput v1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 2887
    invoke-static {v2, v1}, Lcom/navdy/client/app/framework/models/Destination;->shiftListForMove(II)V

    .line 2889
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->shouldDeleteDestinationEntryAsWell()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2890
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->deleteFromDb()I

    move-result v3

    .line 2897
    :goto_0
    return v3

    .line 2879
    .end local v1    # "newOrder":I
    .end local v2    # "oldOrder":I
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v3

    .line 2893
    .restart local v1    # "newOrder":I
    .restart local v2    # "oldOrder":I
    :cond_0
    const-string v3, ""

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 2894
    iput v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 2895
    iput v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 2897
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->updateOnlyFavoriteFieldsInDb()I

    move-result v3

    goto :goto_0
.end method

.method private deleteFromDb()I
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2185
    const/4 v3, 0x0

    .line 2186
    .local v3, "success":Z
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 2187
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2188
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    .line 2189
    .local v2, "nbDeleted":I
    if-eqz v1, :cond_2

    .line 2190
    sget-object v6, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const-string v7, "%s=?"

    new-array v8, v4, [Ljava/lang/Object;

    const-string v9, "_id"

    aput-object v9, v8, v5

    .line 2191
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-array v8, v4, [Ljava/lang/String;

    iget v9, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 2193
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    .line 2190
    invoke-virtual {v1, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2194
    if-lez v2, :cond_1

    move v3, v4

    .line 2198
    :goto_0
    if-nez v3, :cond_3

    .line 2199
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2200
    const v4, 0x7f0804bb

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 2211
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v3, v5

    .line 2194
    goto :goto_0

    .line 2196
    :cond_2
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to get content resolver !"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 2203
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2204
    const-string v4, "Deleting_A_Favorite"

    invoke-static {v4}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 2205
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 2207
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2208
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    goto :goto_1
.end method

.method public static equals(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z
    .locals 2
    .param p0, "d1"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "d2"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 839
    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-eqz p0, :cond_6

    if-eqz p1, :cond_6

    :cond_1
    if-eq p0, p1, :cond_5

    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    iget v1, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-eq v0, v1, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 845
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 847
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 849
    :cond_4
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/models/Destination;->haveMatchingAddresses(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static fromProtobufPlaceType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 2
    .param p0, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 2991
    if-nez p0, :cond_0

    .line 2992
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 3031
    :goto_0
    return-object v0

    .line 2995
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$13;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 3031
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 2997
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->AIRPORT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 2999
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ATM:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3001
    :pswitch_2
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BANK:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3003
    :pswitch_3
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BAR:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3005
    :pswitch_4
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->CALENDAR_EVENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3007
    :pswitch_5
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->COFFEE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3009
    :pswitch_6
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3011
    :pswitch_7
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3013
    :pswitch_8
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->GAS_STATION:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3015
    :pswitch_9
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->GYM:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3017
    :pswitch_a
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3019
    :pswitch_b
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARK:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3021
    :pswitch_c
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARKING:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3023
    :pswitch_d
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3025
    :pswitch_e
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->SCHOOL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3027
    :pswitch_f
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 3029
    :pswitch_10
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 2995
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private getActiveTripPinAsset()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2398
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset(ZZZ)I

    move-result v0

    return v0
.end method

.method private getAllContentValues()Landroid/content/ContentValues;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 2078
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->getContentValues(Z)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method private getBadgeAsset(ZZZZ)I
    .locals 6
    .param p1, "isOverWhiteBg"    # Z
    .param p2, "isGrey"    # Z
    .param p3, "recentTakesPrecedence"    # Z
    .param p4, "isActiveTrip"    # Z

    .prologue
    const v0, 0x7f020142

    const v1, 0x7f020141

    const v2, 0x7f020137

    const v3, 0x7f020136

    .line 2495
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isHome()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2496
    if-eqz p2, :cond_1

    const v0, 0x7f02012b

    .line 2550
    :cond_0
    :goto_0
    return v0

    .line 2496
    :cond_1
    const v0, 0x7f020129

    goto :goto_0

    .line 2497
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isWork()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2498
    if-eqz p2, :cond_3

    const v0, 0x7f020147

    goto :goto_0

    :cond_3
    const v0, 0x7f020145

    goto :goto_0

    .line 2499
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isContact()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2500
    if-nez p2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2501
    :cond_5
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2502
    if-eqz p2, :cond_6

    const v0, 0x7f020123

    goto :goto_0

    :cond_6
    const v0, 0x7f020121

    goto :goto_0

    .line 2505
    :cond_7
    if-eqz p3, :cond_9

    if-nez p4, :cond_9

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2506
    if-eqz p2, :cond_8

    move v0, v2

    goto :goto_0

    :cond_8
    move v0, v3

    goto :goto_0

    .line 2509
    :cond_9
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$13;->$SwitchMap$com$navdy$client$app$framework$models$Destination$Type:[I

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Destination$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2513
    if-nez p4, :cond_b

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2514
    if-eqz p2, :cond_a

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_a
    move v2, v3

    goto :goto_1

    .line 2516
    :cond_b
    invoke-static {p1, p2}, Lcom/navdy/client/app/framework/models/Destination;->getDefaultBadgeAsset(ZZ)I

    move-result v0

    goto :goto_0

    .line 2518
    :pswitch_0
    if-nez p2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2520
    :pswitch_1
    if-eqz p2, :cond_c

    const v0, 0x7f020119

    goto :goto_0

    :cond_c
    const v0, 0x7f020118

    goto :goto_0

    .line 2522
    :pswitch_2
    if-eqz p2, :cond_d

    const v0, 0x7f020125

    goto :goto_0

    :cond_d
    const v0, 0x7f020124

    goto :goto_0

    .line 2524
    :pswitch_3
    if-eqz p2, :cond_e

    const v0, 0x7f020111

    goto :goto_0

    :cond_e
    const v0, 0x7f020110

    goto :goto_0

    .line 2526
    :pswitch_4
    if-eqz p2, :cond_f

    const v0, 0x7f020131

    goto/16 :goto_0

    :cond_f
    const v0, 0x7f020130

    goto/16 :goto_0

    .line 2528
    :pswitch_5
    if-eqz p2, :cond_10

    const v0, 0x7f020140

    goto/16 :goto_0

    :cond_10
    const v0, 0x7f02013f

    goto/16 :goto_0

    .line 2530
    :pswitch_6
    if-eqz p2, :cond_11

    const v0, 0x7f020113

    goto/16 :goto_0

    :cond_11
    const v0, 0x7f020112

    goto/16 :goto_0

    .line 2532
    :pswitch_7
    if-eqz p2, :cond_12

    const v0, 0x7f020115

    goto/16 :goto_0

    :cond_12
    const v0, 0x7f020114

    goto/16 :goto_0

    .line 2534
    :pswitch_8
    if-eqz p2, :cond_13

    const v0, 0x7f02013b

    goto/16 :goto_0

    :cond_13
    const v0, 0x7f02013a

    goto/16 :goto_0

    .line 2536
    :pswitch_9
    if-eqz p2, :cond_14

    const v0, 0x7f02013e

    goto/16 :goto_0

    :cond_14
    const v0, 0x7f02013d

    goto/16 :goto_0

    .line 2538
    :pswitch_a
    if-eqz p2, :cond_15

    const v0, 0x7f02011e

    goto/16 :goto_0

    :cond_15
    const v0, 0x7f02011d

    goto/16 :goto_0

    .line 2540
    :pswitch_b
    if-eqz p2, :cond_16

    const v0, 0x7f020139

    goto/16 :goto_0

    :cond_16
    const v0, 0x7f020138

    goto/16 :goto_0

    .line 2542
    :pswitch_c
    if-eqz p2, :cond_17

    const v0, 0x7f020128

    goto/16 :goto_0

    :cond_17
    const v0, 0x7f020127

    goto/16 :goto_0

    .line 2544
    :pswitch_d
    if-eqz p2, :cond_18

    const v0, 0x7f02012f

    goto/16 :goto_0

    :cond_18
    const v0, 0x7f02012e

    goto/16 :goto_0

    .line 2546
    :pswitch_e
    if-eqz p2, :cond_19

    const v0, 0x7f02012d

    goto/16 :goto_0

    :cond_19
    const v0, 0x7f02012c

    goto/16 :goto_0

    .line 2548
    :pswitch_f
    if-eqz p2, :cond_1a

    const v0, 0x7f020120

    goto/16 :goto_0

    :cond_1a
    const v0, 0x7f02011f

    goto/16 :goto_0

    .line 2550
    :pswitch_10
    if-eqz p2, :cond_1b

    const v0, 0x7f020117

    goto/16 :goto_0

    :cond_1b
    const v0, 0x7f020116

    goto/16 :goto_0

    .line 2509
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private getComparablePhoneNumber(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3
    .param p1, "number"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 1565
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    if-nez v2, :cond_1

    instance-of v2, p1, Lcom/navdy/service/library/events/contacts/Contact;

    if-nez v2, :cond_1

    .line 1581
    .end local p1    # "number":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v1

    .line 1572
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_1
    instance-of v2, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    if-eqz v2, :cond_2

    .line 1573
    check-cast p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;

    .end local p1    # "number":Ljava/lang/Object;
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/PhoneNumber;->number:Ljava/lang/String;

    .line 1577
    .local v0, "num":Ljava/lang/String;
    :goto_1
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1581
    const-string v1, "[^0-9+]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1575
    .end local v0    # "num":Ljava/lang/String;
    .restart local p1    # "number":Ljava/lang/Object;
    :cond_2
    check-cast p1, Lcom/navdy/service/library/events/contacts/Contact;

    .end local p1    # "number":Ljava/lang/Object;
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    .restart local v0    # "num":Ljava/lang/String;
    goto :goto_1
.end method

.method private getContentValues(Z)Landroid/content/ContentValues;
    .locals 6
    .param p1, "includeExtras"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 2094
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2095
    .local v0, "destinationValues":Landroid/content/ContentValues;
    const-string v1, "place_id"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096
    const-string v1, "last_place_id_refresh"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2097
    const-string v1, "displat"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2098
    const-string v1, "displong"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2099
    const-string v1, "navlat"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2100
    const-string v1, "navlong"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2101
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2102
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 2104
    :cond_0
    const-string v1, "place_name"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2106
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 2107
    const-string v1, "address"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2109
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2110
    const-string v1, "street_number"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2112
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2113
    const-string v1, "street_name"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2115
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2116
    const-string v1, "city"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2118
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2119
    const-string v1, "state"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2121
    :cond_4
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2122
    const-string v1, "zip_code"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2124
    :cond_5
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2125
    const-string v1, "country"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2127
    :cond_6
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2128
    const-string v1, "countryCode"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2130
    :cond_7
    if-eqz p1, :cond_8

    .line 2132
    const-string v2, "do_not_suggest"

    iget-boolean v1, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    if-eqz v1, :cond_d

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2134
    const-string v1, "last_routed_date"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2136
    const-string v1, "favorite_label"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2137
    const-string v1, "favorite_listing_order"

    iget v2, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2138
    const-string v1, "is_special"

    iget v2, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2140
    :cond_8
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2141
    const-string v1, "place_detail_json"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2143
    :cond_9
    const-string v1, "precision_level"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$Precision;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2144
    const-string v1, "type"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$Type;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2145
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 2146
    const-string v1, "contact_lookup_key"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148
    :cond_a
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_b

    .line 2149
    const-string v1, "last_known_contact_id"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2151
    :cond_b
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_c

    .line 2152
    const-string v1, "last_contact_lookup"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2154
    :cond_c
    return-object v0

    .line 2132
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private getContentValuesForAddress()Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 2327
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2328
    .local v0, "addressValues":Landroid/content/ContentValues;
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2329
    const-string v1, "street_number"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2331
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2332
    const-string v1, "street_name"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2335
    const-string v1, "city"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2337
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2338
    const-string v1, "state"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2340
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2341
    const-string v1, "zip_code"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2343
    :cond_4
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2344
    const-string v1, "country"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    :cond_5
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2347
    const-string v1, "countryCode"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2349
    :cond_6
    return-object v0
.end method

.method private getContentValuesForCoords()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 2313
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2314
    .local v0, "coordValues":Landroid/content/ContentValues;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2315
    const-string v1, "navlat"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2316
    const-string v1, "navlong"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2317
    const-string v1, "precision_level"

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$Precision;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2319
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2320
    const-string v1, "displat"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2321
    const-string v1, "displong"

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2323
    :cond_1
    return-object v0
.end method

.method public static getCoordinateBoundingBoxSelectionClause(DD)Landroid/util/Pair;
    .locals 12
    .param p0, "latitude"    # D
    .param p2, "longitude"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2356
    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    move-wide v0, p0

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/MapUtils;->getBoundingBox(DDD)[Landroid/location/Location;

    move-result-object v6

    .line 2357
    .local v6, "boundingBox":[Landroid/location/Location;
    array-length v0, v6

    if-ne v0, v9, :cond_0

    .line 2358
    new-instance v0, Landroid/util/Pair;

    const-string v1, "((%s >= ? AND %s <= ?) AND (%s >= ? AND %s <= ?)) OR ((%s >= ? AND %s <= ?) AND (%s >= ? AND %s <= ?))"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "displat"

    aput-object v3, v2, v7

    const-string v3, "displat"

    aput-object v3, v2, v8

    const-string v3, "displong"

    aput-object v3, v2, v9

    const-string v3, "displong"

    aput-object v3, v2, v10

    const-string v3, "navlat"

    aput-object v3, v2, v11

    const/4 v3, 0x5

    const-string v4, "navlat"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "navlong"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "navlong"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    aget-object v3, v6, v7

    .line 2369
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    aget-object v3, v6, v8

    .line 2370
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    aget-object v3, v6, v7

    .line 2371
    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    aget-object v3, v6, v8

    .line 2372
    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    aget-object v3, v6, v7

    .line 2373
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    const/4 v3, 0x5

    aget-object v4, v6, v8

    .line 2374
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    aget-object v4, v6, v7

    .line 2375
    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    aget-object v4, v6, v8

    .line 2376
    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2379
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static getDefaultBadgeAsset(ZZ)I
    .locals 1
    .param p0, "isOverWhiteBg"    # Z
    .param p1, "isLight"    # Z

    .prologue
    .line 2555
    if-eqz p0, :cond_1

    .line 2556
    if-eqz p1, :cond_0

    const v0, 0x7f020134

    .line 2558
    :goto_0
    return v0

    .line 2556
    :cond_0
    const v0, 0x7f020133

    goto :goto_0

    .line 2558
    :cond_1
    const v0, 0x7f02010f

    goto :goto_0
.end method

.method private static getLatitude(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)D
    .locals 5
    .param p0, "navigationRouteResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 536
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    .line 537
    .local v2, "routeLatLongs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const-wide/16 v0, 0x0

    .line 539
    .local v0, "latitude":D
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 540
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v0, v3

    .line 543
    :cond_0
    return-wide v0
.end method

.method private static getLongitude(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)D
    .locals 5
    .param p0, "navigationRouteResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 547
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    .line 548
    .local v2, "routeLatLongs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const-wide/16 v0, 0x0

    .line 550
    .local v0, "longitude":D
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 551
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v0, v3

    .line 554
    :cond_0
    return-wide v0
.end method

.method private getPhoneNumbers()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1414
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1416
    .local v4, "phoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1417
    sget-object v5, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No phone number because no placeDetailJson: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and no contactLookupKey: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for destination: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1439
    :cond_0
    :goto_0
    return-object v4

    .line 1422
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1424
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1425
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v5, "formatted_phone_number"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1426
    .local v3, "number":Ljava/lang/String;
    sget-object v5, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "formatted_phone_number = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1427
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1428
    const-string v5, "formatted_phone_number"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1429
    sget-object v5, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "formatted_phone_number = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1431
    :cond_2
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1432
    new-instance v5, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    invoke-direct {v5}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;-><init>()V

    invoke-virtual {v5, v3}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;

    move-result-object v0

    .line 1433
    .local v0, "builder":Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    invoke-virtual {v0}, Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;->build()Lcom/navdy/service/library/events/contacts/PhoneNumber;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1435
    .end local v0    # "builder":Lcom/navdy/service/library/events/contacts/PhoneNumber$Builder;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "number":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1436
    .local v1, "e":Lorg/json/JSONException;
    sget-object v5, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to parse placeDetailJson: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private getPinAsset(ZZZ)I
    .locals 4
    .param p1, "isSelected"    # Z
    .param p2, "recentTakesPrecedence"    # Z
    .param p3, "isActiveTrip"    # Z

    .prologue
    const v1, 0x7f0201a5

    const v0, 0x7f0201a4

    .line 2408
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isHome()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2409
    if-eqz p1, :cond_1

    const v0, 0x7f020194

    .line 2462
    :cond_0
    :goto_0
    return v0

    .line 2409
    :cond_1
    const v0, 0x7f020195

    goto :goto_0

    .line 2410
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isWork()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2411
    if-eqz p1, :cond_3

    const v0, 0x7f0201b0

    goto :goto_0

    :cond_3
    const v0, 0x7f0201b1

    goto :goto_0

    .line 2412
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isContact()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2413
    if-eqz p1, :cond_5

    const v0, 0x7f0201ae

    goto :goto_0

    :cond_5
    const v0, 0x7f0201af

    goto :goto_0

    .line 2414
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2415
    if-eqz p1, :cond_7

    const v0, 0x7f02018e

    goto :goto_0

    :cond_7
    const v0, 0x7f02018f

    goto :goto_0

    .line 2418
    :cond_8
    if-eqz p2, :cond_9

    if-nez p3, :cond_9

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2419
    if-nez p1, :cond_0

    move v0, v1

    goto :goto_0

    .line 2422
    :cond_9
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$13;->$SwitchMap$com$navdy$client$app$framework$models$Destination$Type:[I

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination$Type;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2427
    if-nez p3, :cond_a

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2428
    if-nez p1, :cond_0

    move v0, v1

    goto :goto_0

    .line 2430
    :cond_a
    if-eqz p1, :cond_b

    const v0, 0x7f02019c

    goto :goto_0

    :cond_b
    const v0, 0x7f02019d

    goto :goto_0

    .line 2432
    :pswitch_0
    if-eqz p1, :cond_c

    const v0, 0x7f020186

    goto :goto_0

    :cond_c
    const v0, 0x7f020187

    goto :goto_0

    .line 2434
    :pswitch_1
    if-eqz p1, :cond_d

    const v0, 0x7f020190

    goto :goto_0

    :cond_d
    const v0, 0x7f020191

    goto :goto_0

    .line 2436
    :pswitch_2
    if-eqz p1, :cond_e

    const v0, 0x7f02017e

    goto :goto_0

    :cond_e
    const v0, 0x7f02017f

    goto/16 :goto_0

    .line 2438
    :pswitch_3
    if-eqz p1, :cond_f

    const v0, 0x7f02019a

    goto/16 :goto_0

    :cond_f
    const v0, 0x7f02019b

    goto/16 :goto_0

    .line 2440
    :pswitch_4
    if-eqz p1, :cond_10

    const v0, 0x7f0201ac

    goto/16 :goto_0

    :cond_10
    const v0, 0x7f0201ad

    goto/16 :goto_0

    .line 2442
    :pswitch_5
    if-eqz p1, :cond_11

    const v0, 0x7f020180

    goto/16 :goto_0

    :cond_11
    const v0, 0x7f020181

    goto/16 :goto_0

    .line 2444
    :pswitch_6
    if-eqz p1, :cond_12

    const v0, 0x7f020182

    goto/16 :goto_0

    :cond_12
    const v0, 0x7f020183

    goto/16 :goto_0

    .line 2446
    :pswitch_7
    if-eqz p1, :cond_13

    const v0, 0x7f0201a8

    goto/16 :goto_0

    :cond_13
    const v0, 0x7f0201a9

    goto/16 :goto_0

    .line 2448
    :pswitch_8
    if-eqz p1, :cond_14

    const v0, 0x7f0201aa

    goto/16 :goto_0

    :cond_14
    const v0, 0x7f0201ab

    goto/16 :goto_0

    .line 2450
    :pswitch_9
    if-eqz p1, :cond_15

    const v0, 0x7f02018a

    goto/16 :goto_0

    :cond_15
    const v0, 0x7f02018b

    goto/16 :goto_0

    .line 2452
    :pswitch_a
    if-eqz p1, :cond_16

    const v0, 0x7f0201a6

    goto/16 :goto_0

    :cond_16
    const v0, 0x7f0201a7

    goto/16 :goto_0

    .line 2454
    :pswitch_b
    if-eqz p1, :cond_17

    const v0, 0x7f020192

    goto/16 :goto_0

    :cond_17
    const v0, 0x7f020193

    goto/16 :goto_0

    .line 2456
    :pswitch_c
    if-eqz p1, :cond_18

    const v0, 0x7f020198

    goto/16 :goto_0

    :cond_18
    const v0, 0x7f020199

    goto/16 :goto_0

    .line 2458
    :pswitch_d
    if-eqz p1, :cond_19

    const v0, 0x7f020196

    goto/16 :goto_0

    :cond_19
    const v0, 0x7f020197

    goto/16 :goto_0

    .line 2460
    :pswitch_e
    if-eqz p1, :cond_1a

    const v0, 0x7f02018c

    goto/16 :goto_0

    :cond_1a
    const v0, 0x7f02018d

    goto/16 :goto_0

    .line 2462
    :pswitch_f
    if-eqz p1, :cond_1b

    const v0, 0x7f020184

    goto/16 :goto_0

    :cond_1b
    const v0, 0x7f020185

    goto/16 :goto_0

    .line 2422
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private getPlaceDetailInfoContentValues()Landroid/content/ContentValues;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 2073
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->getContentValues(Z)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method private getRawAddressNotForDisplay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    return-object v0
.end method

.method private static haveMatchingAddresses(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z
    .locals 4
    .param p0, "d1"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "d2"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    const/4 v1, 0x1

    .line 853
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 873
    :goto_0
    return v1

    .line 857
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 858
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 859
    .local v0, "rawAddressVariation":Ljava/lang/String;
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 865
    .end local v0    # "rawAddressVariation":Ljava/lang/String;
    :cond_2
    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    .line 866
    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 867
    .restart local v0    # "rawAddressVariation":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 873
    .end local v0    # "rawAddressVariation":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isFavoriteDestination(I)Z
    .locals 1
    .param p0, "favoriteType"    # I

    .prologue
    .line 2688
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isFavoriteDestination(Ljava/lang/String;)Z
    .locals 9
    .param p0, "placeId"    # Ljava/lang/String;

    .prologue
    .line 2696
    const/4 v8, 0x0

    .line 2697
    .local v8, "isFavoriteDestination":Z
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 2698
    .local v6, "context":Landroid/content/Context;
    const/4 v7, 0x0

    .line 2700
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "is_special"

    aput-object v4, v2, v3

    const-string v3, "place_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2706
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2707
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 2710
    :cond_0
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 2712
    return v8

    .line 2710
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0
.end method

.method private isInferredDestination()Z
    .locals 4

    .prologue
    .line 2947
    new-instance v1, Landroid/util/Pair;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "destination_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2950
    .local v1, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 2952
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    .line 2953
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    .line 2955
    :goto_0
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    return v2

    .line 2953
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2955
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v2
.end method

.method private makeSureWeHaveDestinationInDb()V
    .locals 5

    .prologue
    .line 1842
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isPersisted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1843
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->findInDb()I

    move-result v2

    iput v2, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 1845
    iget v2, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-lez v2, :cond_1

    .line 1846
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found this ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1859
    :cond_0
    :goto_0
    return-void

    .line 1848
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Couldn\'t find a matching destination in the db"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1849
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->saveToDb()Landroid/net/Uri;

    move-result-object v1

    .line 1850
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 1852
    :try_start_0
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1853
    :catch_0
    move-exception v0

    .line 1854
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to parse destination id from the URI path segment"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static mergeAndDeduplicateLists(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 9
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "list1":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    .local p1, "list2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/4 v8, 0x0

    .line 900
    if-nez p0, :cond_0

    .line 901
    new-instance p0, Ljava/util/ArrayList;

    .end local p0    # "list1":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-direct {p0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 903
    .restart local p0    # "list1":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_0
    if-nez p1, :cond_1

    .line 904
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "list2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-direct {p1, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 907
    .restart local p1    # "list2":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    add-int v3, v6, v7

    .line 909
    .local v3, "initialCapacity":I
    if-nez v3, :cond_3

    .line 910
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 933
    :cond_2
    return-object v2

    .line 913
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 915
    .local v2, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-interface {v2, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 917
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/Destination;

    .line 918
    .local v1, "destination2":Lcom/navdy/client/app/framework/models/Destination;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 919
    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/4 v5, 0x0

    .line 921
    .local v5, "merged":Z
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 922
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0, v6}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 923
    .local v0, "destination1":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 924
    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->mergeWith(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 925
    const/4 v5, 0x1

    .line 929
    .end local v0    # "destination1":Lcom/navdy/client/app/framework/models/Destination;
    :cond_6
    if-nez v5, :cond_4

    .line 930
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private mergeWith(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 12
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 946
    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-gtz v3, :cond_0

    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-lez v3, :cond_0

    .line 947
    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    iput v3, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 949
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 950
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 952
    :cond_1
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_2

    .line 953
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    .line 955
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 956
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 957
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 959
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 960
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 961
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 963
    :cond_4
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 964
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 966
    :cond_5
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 967
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 969
    :cond_6
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 970
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 972
    :cond_7
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 973
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 975
    :cond_8
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 976
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 978
    :cond_9
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 979
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 981
    :cond_a
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 982
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 984
    :cond_b
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 985
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 987
    :cond_c
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 988
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 990
    :cond_d
    iget-boolean v3, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    if-nez v3, :cond_e

    .line 991
    iget-boolean v3, p1, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    iput-boolean v3, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    .line 993
    :cond_e
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_f

    .line 994
    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 996
    :cond_f
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_10

    .line 997
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 999
    :cond_10
    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    if-gtz v3, :cond_11

    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    if-lez v3, :cond_11

    .line 1000
    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    iput v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 1002
    :cond_11
    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    if-nez v3, :cond_12

    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    if-eqz v3, :cond_12

    .line 1003
    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    iput v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 1005
    :cond_12
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 1006
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_13

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    iget-wide v8, p1, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    cmp-long v3, v6, v8

    if-gez v3, :cond_14

    .line 1009
    :cond_13
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 1013
    :cond_14
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v6, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    if-ne v3, v6, :cond_15

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v6, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    if-eq v3, v6, :cond_15

    .line 1014
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 1016
    :cond_15
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    sget-object v6, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-ne v3, v6, :cond_16

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    sget-object v6, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-eq v3, v6, :cond_16

    .line 1017
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 1020
    :cond_16
    iget-boolean v3, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    if-nez v3, :cond_17

    iget-boolean v3, p1, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    if-eqz v3, :cond_20

    :cond_17
    move v3, v5

    :goto_0
    iput-boolean v3, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 1021
    iget-boolean v3, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    if-nez v3, :cond_18

    iget-boolean v3, p1, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    if-eqz v3, :cond_19

    :cond_18
    move v4, v5

    :cond_19
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    .line 1023
    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    cmp-long v3, v4, v10

    if-lez v3, :cond_1a

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    cmp-long v3, v4, v6

    if-gtz v3, :cond_1b

    :cond_1a
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 1025
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1c

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    cmp-long v3, v4, v10

    if-gtz v3, :cond_1c

    .line 1027
    :cond_1b
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 1028
    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    .line 1029
    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    .line 1032
    :cond_1c
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    if-eqz v3, :cond_22

    .line 1033
    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1d
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1034
    .local v2, "rawAddressVariation":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1035
    .local v1, "found":Z
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    if-eqz v4, :cond_21

    .line 1036
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1037
    .local v0, "addressVariation":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1038
    const/4 v1, 0x1

    .line 1045
    .end local v0    # "addressVariation":Ljava/lang/String;
    :cond_1f
    :goto_2
    if-nez v1, :cond_1d

    .line 1046
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v1    # "found":Z
    .end local v2    # "rawAddressVariation":Ljava/lang/String;
    :cond_20
    move v3, v4

    .line 1020
    goto :goto_0

    .line 1043
    .restart local v1    # "found":Z
    .restart local v2    # "rawAddressVariation":Ljava/lang/String;
    :cond_21
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    goto :goto_2

    .line 1050
    .end local v1    # "found":Z
    .end local v2    # "rawAddressVariation":Ljava/lang/String;
    :cond_22
    return-void
.end method

.method private persistPlaceDetailInfoAndUpdateLists()V
    .locals 1

    .prologue
    .line 1702
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->persistPlaceDetailInfo()I

    move-result v0

    .line 1704
    .local v0, "nbRows":I
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->updateDestinationLists(I)V

    .line 1705
    return-void
.end method

.method public static placesSearchResultToDestinationObject(Lcom/navdy/service/library/events/places/PlacesSearchResult;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p0, "placesSearchResult"    # Lcom/navdy/service/library/events/places/PlacesSearchResult;
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 2962
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2963
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    iput-object v0, p1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 2965
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2966
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 2968
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v0, :cond_2

    .line 2969
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 2970
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 2972
    :cond_2
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v0, :cond_3

    .line 2973
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 2974
    iget-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v0, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 2976
    :cond_3
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, p1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 2977
    return-void
.end method

.method private varargs saveContentValuesToDb([Landroid/content/ContentValues;)I
    .locals 11
    .param p1, "contentValues"    # [Landroid/content/ContentValues;

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    const/4 v5, -0x1

    .line 2278
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->makeSureWeHaveDestinationInDb()V

    .line 2279
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2281
    .local v0, "allContentValues":Landroid/content/ContentValues;
    array-length v8, p1

    move v6, v7

    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v2, p1, v6

    .line 2282
    .local v2, "contentValueGroup":Landroid/content/ContentValues;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 2283
    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 2281
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2286
    .end local v2    # "contentValueGroup":Landroid/content/ContentValues;
    :cond_1
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v6

    if-ge v6, v10, :cond_2

    .line 2287
    sget-object v6, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "No content values were found for updating the DB"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 2309
    :goto_1
    return v5

    .line 2290
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    .line 2291
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2292
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v1, :cond_3

    .line 2293
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2295
    .local v4, "destinationValues":Landroid/content/ContentValues;
    const-string v5, "navlat"

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2296
    const-string v5, "navlong"

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2297
    const-string v5, "displat"

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2298
    const-string v5, "displong"

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2300
    sget-object v5, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    iget v9, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 2304
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    .line 2300
    invoke-virtual {v1, v5, v0, v6, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2307
    .end local v4    # "destinationValues":Landroid/content/ContentValues;
    :cond_3
    sget-object v6, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Unable to get contentResolver !"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private saveDestinationAsFavorite(I)I
    .locals 10
    .param p1, "specialType"    # I

    .prologue
    const/4 v4, -0x1

    .line 2742
    const/4 v1, 0x0

    .line 2745
    .local v1, "favoritesCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 2746
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2748
    .local v0, "count":I
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 2752
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isPersisted()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v7

    if-eqz v7, :cond_3

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 2755
    .local v6, "oldOrder":I
    :goto_0
    iput p1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 2757
    if-ne p1, v4, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isContact()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2758
    const/4 v7, -0x4

    iput v7, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 2762
    :cond_0
    const/4 v7, -0x3

    if-ne p1, v7, :cond_4

    .line 2763
    const/4 v5, 0x0

    .line 2775
    .local v5, "newOrder":I
    :goto_1
    sget-object v7, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Moving favorite from position "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " to "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2776
    iput v5, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 2778
    invoke-static {v6, v5}, Lcom/navdy/client/app/framework/models/Destination;->shiftListForMove(II)V

    .line 2780
    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-static {v7}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2781
    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 2782
    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    iput-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 2787
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isPersisted()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2788
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->findInDb()I

    move-result v3

    .line 2789
    .local v3, "id":I
    if-lez v3, :cond_2

    .line 2790
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->reloadSelfFromDatabase()V

    .line 2795
    .end local v3    # "id":I
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isPersisted()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2796
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->updateOnlyFavoriteFieldsInDb()I

    move-result v4

    .line 2800
    .local v4, "nbRows":I
    :goto_3
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v7}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 2801
    return v4

    .line 2748
    .end local v0    # "count":I
    .end local v4    # "nbRows":I
    .end local v5    # "newOrder":I
    .end local v6    # "oldOrder":I
    :catchall_0
    move-exception v7

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v7

    .restart local v0    # "count":I
    :cond_3
    move v6, v0

    .line 2752
    goto :goto_0

    .line 2764
    .restart local v6    # "oldOrder":I
    :cond_4
    const/4 v7, -0x2

    if-ne p1, v7, :cond_6

    .line 2765
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getHome()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 2766
    .local v2, "home":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v2, :cond_5

    iget v7, v2, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    if-nez v7, :cond_5

    .line 2767
    const/4 v5, 0x1

    .restart local v5    # "newOrder":I
    goto :goto_1

    .line 2769
    .end local v5    # "newOrder":I
    :cond_5
    const/4 v5, 0x0

    .restart local v5    # "newOrder":I
    goto :goto_1

    .line 2772
    .end local v2    # "home":Lcom/navdy/client/app/framework/models/Destination;
    .end local v5    # "newOrder":I
    :cond_6
    move v5, v0

    .restart local v5    # "newOrder":I
    goto :goto_1

    .line 2784
    :cond_7
    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    iput-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    goto :goto_2

    .line 2798
    :cond_8
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->saveToDb()Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_9

    const/4 v4, 0x1

    .restart local v4    # "nbRows":I
    :cond_9
    goto :goto_3
.end method

.method private saveToDb()Landroid/net/Uri;
    .locals 6

    .prologue
    .line 2044
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 2045
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2046
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_3

    .line 2047
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getAllContentValues()Landroid/content/ContentValues;

    move-result-object v2

    .line 2048
    .local v2, "destinationValues":Landroid/content/ContentValues;
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 2049
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 2050
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 2052
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2054
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 2056
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2060
    const-string v4, "Saving_A_Favorite"

    invoke-static {v4}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 2062
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 2068
    .end local v2    # "destinationValues":Landroid/content/ContentValues;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_2
    :goto_0
    return-object v3

    .line 2066
    :cond_3
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Unable to get contentResolver !"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 2068
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private saveToDbAsync()V
    .locals 3

    .prologue
    .line 2029
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$8;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/Destination$8;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 2035
    return-void
.end method

.method private static selectionForShift(II)Landroid/util/Pair;
    .locals 5
    .param p0, "startOrder"    # I
    .param p1, "endOrder"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2933
    new-instance v0, Landroid/util/Pair;

    const-string v1, "favorite_listing_order >= ? AND favorite_listing_order <= ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 2935
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method private setAddressFieldsFromHereAddress(Lcom/here/android/mpa/search/Address;)V
    .locals 3
    .param p1, "hereAddress"    # Lcom/here/android/mpa/search/Address;

    .prologue
    .line 1283
    if-nez p1, :cond_0

    .line 1284
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "address is null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1303
    :goto_0
    return-void

    .line 1288
    :cond_0
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getCountryName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 1289
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1290
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->convertIso3ToIso2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 1292
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getCity()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 1293
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 1294
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getStreet()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 1295
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getState()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 1296
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1297
    .local v0, "hereAddressText":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1298
    const-string v1, "\n\n"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1299
    const-string v1, "\n"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1301
    :cond_2
    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 1302
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Address;->getHouseNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    goto :goto_0
.end method

.method private static shiftListForMove(II)V
    .locals 9
    .param p0, "oldOrder"    # I
    .param p1, "newOrder"    # I

    .prologue
    .line 2904
    if-ge p1, p0, :cond_0

    .line 2905
    move v6, p1

    .line 2906
    .local v6, "startOrder":I
    add-int/lit8 v3, p0, -0x1

    .line 2907
    .local v3, "endOrder":I
    const/4 v5, 0x1

    .line 2917
    .local v5, "shift":I
    :goto_0
    const/4 v1, 0x0

    .line 2919
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {v6, v3}, Lcom/navdy/client/app/framework/models/Destination;->selectionForShift(II)Landroid/util/Pair;

    move-result-object v7

    invoke-static {v7}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v1

    .line 2920
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2921
    .local v0, "contentValues":Landroid/content/ContentValues;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 2922
    invoke-static {v1, v4}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 2923
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    const-string v7, "favorite_listing_order"

    iget v8, v2, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    add-int/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2924
    invoke-direct {v2, v0}, Lcom/navdy/client/app/framework/models/Destination;->updateDb(Landroid/content/ContentValues;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2921
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2908
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v3    # "endOrder":I
    .end local v4    # "i":I
    .end local v5    # "shift":I
    .end local v6    # "startOrder":I
    :cond_0
    if-le p1, p0, :cond_1

    .line 2909
    add-int/lit8 v6, p0, 0x1

    .line 2910
    .restart local v6    # "startOrder":I
    move v3, p1

    .line 2911
    .restart local v3    # "endOrder":I
    const/4 v5, -0x1

    .restart local v5    # "shift":I
    goto :goto_0

    .line 2913
    .end local v3    # "endOrder":I
    .end local v5    # "shift":I
    .end local v6    # "startOrder":I
    :cond_1
    sget-object v7, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "No need to shift list"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2929
    :goto_2
    return-void

    .line 2927
    .restart local v0    # "contentValues":Landroid/content/ContentValues;
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v3    # "endOrder":I
    .restart local v4    # "i":I
    .restart local v5    # "shift":I
    .restart local v6    # "startOrder":I
    :cond_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    goto :goto_2

    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v4    # "i":I
    :catchall_0
    move-exception v7

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v7
.end method

.method private shouldDeleteDestinationEntryAsWell()Z
    .locals 1

    .prologue
    .line 2164
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->isInferredDestination()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateAllColumnsOrInsertNewEntryInDbAsync()V
    .locals 1

    .prologue
    .line 1933
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->updateAllColumnsOrInsertNewEntryInDbAsync(Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    .line 1934
    return-void
.end method

.method private updateAllColumnsOrInsertNewEntryInDbAsync(Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    .prologue
    .line 1941
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/models/Destination$6;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1950
    return-void
.end method

.method private updateDb(Landroid/content/ContentValues;)I
    .locals 9
    .param p1, "cv"    # Landroid/content/ContentValues;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1912
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->makeSureWeHaveDestinationInDb()V

    .line 1914
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1915
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1916
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    .line 1917
    .local v2, "nbRows":I
    if-eqz v1, :cond_0

    .line 1918
    sget-object v3, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "%s=?"

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "_id"

    aput-object v6, v5, v7

    .line 1919
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/String;

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 1920
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    .line 1918
    invoke-virtual {v1, v3, p1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1921
    sget-object v3, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "update returned "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rows."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1925
    :goto_0
    return v2

    .line 1923
    :cond_0
    sget-object v3, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unable to get content resolver !"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateDestinationLists(I)V
    .locals 8
    .param p1, "nbRows"    # I

    .prologue
    const/4 v7, 0x1

    .line 1718
    if-lez p1, :cond_5

    .line 1719
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v0

    .line 1722
    .local v0, "isFavoriteDestination":Z
    if-nez v0, :cond_1

    if-gt p1, v7, :cond_0

    iget v4, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-gtz v4, :cond_1

    .line 1726
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination(Ljava/lang/String;)Z

    move-result v0

    .line 1728
    :cond_1
    if-eqz v0, :cond_2

    .line 1730
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 1734
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getSuggestions()Ljava/util/ArrayList;

    move-result-object v3

    .line 1735
    .local v3, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const/4 v1, 0x0

    .line 1736
    .local v1, "isSuggestion":Z
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 1737
    .local v2, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    if-eqz v2, :cond_3

    iget-object v5, v2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v5, :cond_3

    iget-object v5, v2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v5, v5, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 1739
    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1740
    const/4 v1, 0x1

    goto :goto_0

    .line 1743
    .end local v2    # "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_4
    if-eqz v1, :cond_5

    .line 1745
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildSuggestionList(Z)V

    .line 1748
    .end local v0    # "isFavoriteDestination":Z
    .end local v1    # "isSuggestion":Z
    .end local v3    # "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    :cond_5
    return-void
.end method

.method private updateDoNotSuggest()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 1997
    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-gtz v3, :cond_0

    .line 1998
    sget-object v3, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Trying to updateDoNotSuggest on an object that has no ID!"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 2020
    :goto_0
    return-void

    .line 2002
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 2003
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2005
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_1

    .line 2006
    sget-object v3, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unable to get a content resolver"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 2010
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 2011
    .local v1, "contentValues":Landroid/content/ContentValues;
    const-string v3, "do_not_suggest"

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2013
    sget-object v3, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget v7, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 2018
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 2013
    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public deleteFavoriteFromDbAsync(Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    .prologue
    .line 2857
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$12;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/models/Destination$12;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 2866
    return-void
.end method

.method public deleteFromDbAsync()V
    .locals 3

    .prologue
    .line 2172
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$9;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/Destination$9;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 2178
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 878
    instance-of v0, p1, Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_0

    .line 879
    check-cast p1, Lcom/navdy/client/app/framework/models/Destination;

    .end local p1    # "o":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/models/Destination;->equals(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v0

    .line 881
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public findInDb()I
    .locals 10

    .prologue
    .line 1809
    const/4 v6, 0x0

    .line 1810
    .local v6, "c":Landroid/database/Cursor;
    const/4 v9, -0x1

    .line 1812
    .local v9, "id":I
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 1813
    .local v8, "context":Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1814
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getComparisonSelectionClause()Landroid/util/Pair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1816
    .local v7, "comparisonSelectionClause":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    if-nez v7, :cond_0

    .line 1817
    const/4 v1, -0x1

    .line 1832
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1834
    :goto_0
    return v1

    .line 1820
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1827
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1828
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1829
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found this destination in the db under id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1832
    :cond_1
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move v1, v9

    .line 1834
    goto :goto_0

    .line 1832
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v7    # "comparisonSelectionClause":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v8    # "context":Landroid/content/Context;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method public getAddressForDisplay()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1140
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getAddressLines()Ljava/util/ArrayList;

    move-result-object v0

    .line 1141
    .local v0, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1142
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v1

    .line 1144
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    goto :goto_0
.end method

.method public getAddressLines()Ljava/util/ArrayList;
    .locals 20
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1056
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1059
    .local v9, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/framework/models/Destination;->hasDetailedAddress()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 1060
    invoke-static/range {p0 .. p0}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->getAddressFormatFor(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;

    move-result-object v2

    .line 1061
    .local v2, "addressFormat":Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    if-eqz v2, :cond_2

    .line 1062
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getLastKnownCountryCode()Ljava/lang/String;

    move-result-object v8

    .line 1063
    .local v8, "lastKnownCountryCode":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-static {v15, v8}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1064
    iget-object v9, v2, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->localLines:Ljava/util/ArrayList;

    .line 1069
    :goto_0
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v6, v15, :cond_1

    .line 1070
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v15, v0}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->insertAddressPartsInFormat(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v6, v15}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1069
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1066
    .end local v6    # "i":I
    :cond_0
    iget-object v9, v2, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->foreignLines:Ljava/util/ArrayList;

    goto :goto_0

    .restart local v6    # "i":I
    :cond_1
    move-object v10, v9

    .line 1136
    .end local v2    # "addressFormat":Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    .end local v6    # "i":I
    .end local v8    # "lastKnownCountryCode":Ljava/lang/String;
    :goto_2
    return-object v10

    .line 1077
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v15}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    .line 1079
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v15}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 1081
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    const-string v16, ","

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1082
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    const-string v16, ", *"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 1083
    .local v14, "splitAddress":[Ljava/lang/String;
    array-length v15, v14

    if-lez v15, :cond_3

    .line 1084
    new-instance v10, Ljava/util/ArrayList;

    invoke-static {v14}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1088
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_2

    .line 1093
    .end local v10    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14    # "splitAddress":[Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 1094
    .local v4, "context":Landroid/content/Context;
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 1095
    .local v12, "res":Landroid/content/res/Resources;
    const v15, 0x7f0e0011

    invoke-virtual {v12, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    .line 1096
    .local v11, "nbChars":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-ge v15, v11, :cond_5

    .line 1097
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .end local v4    # "context":Landroid/content/Context;
    .end local v11    # "nbChars":I
    .end local v12    # "res":Landroid/content/res/Resources;
    :cond_4
    :goto_3
    move-object v10, v9

    .line 1136
    goto :goto_2

    .line 1099
    .restart local v4    # "context":Landroid/content/Context;
    .restart local v11    # "nbChars":I
    .restart local v12    # "res":Landroid/content/res/Resources;
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1102
    .local v3, "addressParts":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const/4 v15, 0x0

    aget-object v15, v3, v15

    invoke-direct {v5, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1103
    .local v5, "firstLineSb":Ljava/lang/StringBuilder;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 1106
    .local v13, "secondLineSb":Ljava/lang/StringBuilder;
    const/4 v7, 0x1

    .line 1107
    .local v7, "index":I
    :goto_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v15

    if-ge v15, v11, :cond_6

    array-length v15, v3

    if-ge v7, v15, :cond_6

    .line 1108
    const-string v15, " "

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1109
    aget-object v15, v3, v7

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1110
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1114
    :cond_6
    :goto_5
    array-length v15, v3

    if-ge v7, v15, :cond_7

    .line 1115
    aget-object v15, v3, v7

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1116
    add-int/lit8 v7, v7, 0x1

    .line 1117
    array-length v15, v3

    if-ge v7, v15, :cond_6

    .line 1118
    const-string v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 1122
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1123
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v15

    if-lez v15, :cond_4

    .line 1124
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1130
    .end local v3    # "addressParts":[Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "firstLineSb":Ljava/lang/StringBuilder;
    .end local v7    # "index":I
    .end local v11    # "nbChars":I
    .end local v12    # "res":Landroid/content/res/Resources;
    .end local v13    # "secondLineSb":Ljava/lang/StringBuilder;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1131
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 1132
    .restart local v4    # "context":Landroid/content/Context;
    const v15, 0x7f080279

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1133
    const v15, 0x7f08028e

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3
.end method

.method public getBadgeAsset()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2467
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1, v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method public getBadgeAssetForActiveTrip()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2471
    const/4 v0, 0x1

    invoke-direct {p0, v1, v1, v1, v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method public getBadgeAssetForAutoComplete()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2487
    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v1, v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method public getBadgeAssetForPendingTrip()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2475
    invoke-direct {p0, v1, v0, v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method getBadgeAssetForRecent()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2483
    invoke-direct {p0, v1, v1, v0, v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method public getBadgeAssetForSearchResults()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2491
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1, v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method public getBadgeAssetForSuggestion()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2479
    const/4 v0, 0x1

    invoke-direct {p0, v1, v1, v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset(ZZZZ)I

    move-result v0

    return v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getComparisonSelectionClause()Landroid/util/Pair;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1870
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1872
    const-string v2, "place_id=?"

    .line 1873
    .local v2, "selection":Ljava/lang/String;
    new-array v3, v7, [Ljava/lang/String;

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    aput-object v5, v3, v6

    .line 1900
    .local v3, "selectionArgs":[Ljava/lang/String;
    :goto_0
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .end local v2    # "selection":Ljava/lang/String;
    .end local v3    # "selectionArgs":[Ljava/lang/String;
    :goto_1
    return-object v5

    .line 1874
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1876
    const-string v2, "(place_id is not null AND place_id=?) OR (place_id is null AND address=?)"

    .line 1880
    .restart local v2    # "selection":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v3, v5, [Ljava/lang/String;

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    aput-object v5, v3, v6

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    aput-object v5, v3, v7

    .restart local v3    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 1881
    .end local v2    # "selection":Ljava/lang/String;
    .end local v3    # "selectionArgs":[Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1883
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "address=?"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1884
    .local v4, "selectionBuilder":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1885
    .local v0, "args":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1887
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    .line 1888
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1889
    .local v1, "rawAddressVariation":Ljava/lang/String;
    const-string v6, " OR "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1892
    .end local v1    # "rawAddressVariation":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1894
    .restart local v2    # "selection":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v3, v5, [Ljava/lang/String;

    .line 1895
    .restart local v3    # "selectionArgs":[Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "selectionArgs":[Ljava/lang/String;
    check-cast v3, [Ljava/lang/String;

    .line 1896
    .restart local v3    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 1897
    .end local v0    # "args":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "selection":Ljava/lang/String;
    .end local v3    # "selectionArgs":[Ljava/lang/String;
    .end local v4    # "selectionBuilder":Ljava/lang/StringBuilder;
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public getContacts()Ljava/util/ArrayList;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1454
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1456
    .local v8, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    sget-object v17, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "contactLookupKey = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", lastKnownContactId = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 1458
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 1460
    .local v9, "contentResolver":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    invoke-static {v9, v0, v1, v2}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactIdFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    .line 1461
    .local v6, "contactId":J
    const-wide/16 v18, 0x0

    cmp-long v17, v6, v18

    if-gez v17, :cond_0

    .line 1467
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    .line 1470
    :cond_0
    invoke-static {v9, v6, v7}, Lcom/navdy/client/app/framework/util/ContactsManager;->getPhoneNumbers(Landroid/content/ContentResolver;J)Ljava/util/List;

    move-result-object v13

    .line 1472
    .local v13, "numbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_5

    .line 1473
    invoke-static {v9, v6, v7}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactName(Landroid/content/ContentResolver;J)Ljava/lang/String;

    move-result-object v5

    .line 1475
    .local v5, "contactName":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1476
    .local v15, "primary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1477
    .local v11, "mobile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1478
    .local v10, "home":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1479
    .local v16, "work":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1481
    .local v14, "other":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_1
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    .line 1482
    .local v12, "num":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    sget-object v18, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "number = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " for destination: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1483
    invoke-virtual {v12}, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->isFax()Z

    move-result v18

    if-nez v18, :cond_1

    .line 1487
    new-instance v18, Lcom/navdy/service/library/events/contacts/Contact$Builder;

    invoke-direct/range {v18 .. v18}, Lcom/navdy/service/library/events/contacts/Contact$Builder;-><init>()V

    .line 1488
    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->name(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v18

    iget-object v0, v12, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 1489
    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v18

    .line 1490
    invoke-virtual {v12}, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->getPhoneNumberProtobufType()Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->numberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v18

    iget-object v0, v12, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->customType:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 1491
    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->label(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;

    move-result-object v4

    .line 1492
    .local v4, "builder":Lcom/navdy/service/library/events/contacts/Contact$Builder;
    iget-boolean v0, v12, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->isPrimary:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    .line 1493
    invoke-virtual {v4}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1497
    :cond_2
    iget v0, v12, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 1508
    const/16 v18, 0x0

    invoke-virtual {v4}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v19

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1499
    :pswitch_0
    invoke-virtual {v4}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1502
    :pswitch_1
    invoke-virtual {v4}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1505
    :pswitch_2
    invoke-virtual {v4}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1514
    .end local v4    # "builder":Lcom/navdy/service/library/events/contacts/Contact$Builder;
    .end local v12    # "num":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :cond_3
    invoke-virtual {v8, v15}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1515
    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1516
    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1517
    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1518
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1519
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/navdy/client/app/framework/models/Destination;->deDuplicatePhoneNumbersWhileKeepingOrder(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 1524
    .end local v5    # "contactName":Ljava/lang/String;
    .end local v6    # "contactId":J
    .end local v9    # "contentResolver":Landroid/content/ContentResolver;
    .end local v10    # "home":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v11    # "mobile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v13    # "numbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    .end local v14    # "other":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v15    # "primary":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    .end local v16    # "work":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_4
    :goto_1
    return-object v8

    .line 1521
    .restart local v6    # "contactId":J
    .restart local v9    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v13    # "numbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    :cond_5
    sget-object v17, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "No phone numbers for contact lookup key: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 1497
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayCoordinate()Lcom/navdy/service/library/events/location/Coordinate;
    .locals 4

    .prologue
    .line 616
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 617
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 618
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 619
    invoke-virtual {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayLat()D
    .locals 2

    .prologue
    .line 599
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    return-wide v0
.end method

.method public getDisplayLng()D
    .locals 2

    .prologue
    .line 608
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    return-wide v0
.end method

.method public getFavoriteLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 767
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isHome()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 768
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 769
    .local v0, "context":Landroid/content/Context;
    const v1, 0x7f08023f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 774
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    return-object v1

    .line 770
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isWork()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 771
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 772
    .restart local v0    # "context":Landroid/content/Context;
    const v1, 0x7f080503

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 774
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFavoriteOrder()I
    .locals 1

    .prologue
    .line 784
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    return v0
.end method

.method public getFavoriteType()I
    .locals 1

    .prologue
    .line 793
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    return v0
.end method

.method public getFavoriteTypeForProto()Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .locals 1

    .prologue
    .line 802
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    packed-switch v0, :pswitch_data_0

    .line 810
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 811
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CUSTOM:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 813
    :goto_0
    return-object v0

    .line 804
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_HOME:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 806
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_WORK:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 808
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CONTACT:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 813
    :cond_0
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    goto :goto_0

    .line 802
    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getHereAddress()Lcom/here/android/mpa/search/Address;
    .locals 4

    .prologue
    .line 3039
    new-instance v1, Lcom/here/android/mpa/search/Address;

    invoke-direct {v1}, Lcom/here/android/mpa/search/Address;-><init>()V

    .line 3040
    .local v1, "hereAddress":Lcom/here/android/mpa/search/Address;
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3041
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setText(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 3043
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 3044
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setCountryName(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 3046
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 3047
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 3048
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->convertIso2ToIso3(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3049
    .local v0, "countryCode3":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lcom/here/android/mpa/search/Address;->setCountryCode(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 3054
    .end local v0    # "countryCode3":Ljava/lang/String;
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 3055
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setState(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 3057
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 3058
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setCity(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 3060
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 3061
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setPostalCode(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 3063
    :cond_5
    return-object v1

    .line 3051
    :cond_6
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setCountryCode(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    goto :goto_0
.end method

.method public getHereMapMarker()Lcom/here/android/mpa/mapping/MapMarker;
    .locals 14
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2564
    new-instance v3, Lcom/here/android/mpa/common/Image;

    invoke-direct {v3}, Lcom/here/android/mpa/common/Image;-><init>()V

    .line 2566
    .local v3, "image":Lcom/here/android/mpa/common/Image;
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getActiveTripPinAsset()I

    move-result v10

    invoke-virtual {v3, v10}, Lcom/here/android/mpa/common/Image;->setImageResource(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2574
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2575
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 2576
    .local v4, "latitude":D
    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 2582
    .local v6, "longitude":D
    :goto_0
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 2583
    .local v2, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    new-instance v8, Lcom/here/android/mpa/mapping/MapMarker;

    invoke-direct {v8, v2, v3}, Lcom/here/android/mpa/mapping/MapMarker;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/Image;)V

    .line 2584
    .local v8, "mapMarker":Lcom/here/android/mpa/mapping/MapMarker;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 2585
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 2586
    .local v9, "res":Landroid/content/res/Resources;
    const v10, 0x7f0e000d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    invoke-virtual {v8, v10}, Lcom/here/android/mpa/mapping/MapMarker;->setZIndex(I)Lcom/here/android/mpa/mapping/MapObject;

    .line 2587
    new-instance v10, Landroid/graphics/PointF;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/Image;->getWidth()J

    move-result-wide v12

    long-to-float v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    invoke-virtual {v3}, Lcom/here/android/mpa/common/Image;->getHeight()J

    move-result-wide v12

    long-to-float v12, v12

    invoke-direct {v10, v11, v12}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v10}, Lcom/here/android/mpa/mapping/MapMarker;->setAnchorPoint(Landroid/graphics/PointF;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 2588
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v4    # "latitude":D
    .end local v6    # "longitude":D
    .end local v8    # "mapMarker":Lcom/here/android/mpa/mapping/MapMarker;
    .end local v9    # "res":Landroid/content/res/Resources;
    :goto_1
    return-object v8

    .line 2567
    :catch_0
    move-exception v1

    .line 2568
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 2569
    const/4 v8, 0x0

    goto :goto_1

    .line 2578
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 2579
    .restart local v4    # "latitude":D
    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .restart local v6    # "longitude":D
    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 581
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    return v0
.end method

.method public getImageEnum()Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    .locals 2

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteTypeForProto()Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_CONTACT:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    if-ne v0, v1, :cond_0

    .line 820
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 822
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->PLACE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    goto :goto_0
.end method

.method public getLastRoutedDate()J
    .locals 2

    .prologue
    .line 748
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    return-wide v0
.end method

.method public getMultilineAddress()Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1246
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNameForDisplay()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2980
    const-string v0, ""

    .line 2981
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2982
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 2984
    :cond_0
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2985
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 2987
    :cond_1
    return-object v0
.end method

.method public getNavigationCoordinate()Lcom/navdy/service/library/events/location/Coordinate;
    .locals 4

    .prologue
    .line 640
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;-><init>()V

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 641
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 642
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;

    move-result-object v0

    .line 643
    invoke-virtual {v0}, Lcom/navdy/service/library/events/location/Coordinate$Builder;->build()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationLat()D
    .locals 2

    .prologue
    .line 623
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    return-wide v0
.end method

.method public getNavigationLong()D
    .locals 2

    .prologue
    .line 631
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    return-wide v0
.end method

.method public getPinAsset()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2386
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset(ZZZ)I

    move-result v0

    return v0
.end method

.method public getPlaceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaceIndentifier()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1346
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1347
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "google-places-id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1353
    :goto_0
    return-object v0

    .line 1349
    :cond_0
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1350
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "contact:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1353
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "db-id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRecentPinAsset()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2394
    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v0}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset(ZZZ)I

    move-result v0

    return v0
.end method

.method public getSearchResultType()I
    .locals 1

    .prologue
    .line 758
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    return v0
.end method

.method public getSplitAddress()Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1151
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getAddressLines()Ljava/util/ArrayList;

    move-result-object v0

    .line 1153
    .local v0, "addressLines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_0

    .line 1154
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 1155
    .local v1, "firstLine":Ljava/lang/String;
    const-string v2, ""

    .line 1161
    .local v2, "secondLine":Ljava/lang/String;
    :goto_0
    new-instance v3, Landroid/util/Pair;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 1157
    .end local v1    # "firstLine":Ljava/lang/String;
    .end local v2    # "secondLine":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1158
    .restart local v1    # "firstLine":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "secondLine":Ljava/lang/String;
    goto :goto_0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getStreetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    return-object v0
.end method

.method public getStreetNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1193
    const/4 v0, 0x0

    .line 1195
    .local v0, "destinationTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1197
    iget v1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v2, -0x3

    if-ne v1, v2, :cond_2

    .line 1198
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08023f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1206
    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1207
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 1209
    :cond_1
    return-object v0

    .line 1199
    :cond_2
    iget v1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_3

    .line 1200
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080503

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1201
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1202
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTitleAndSubtitle()Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1169
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 1171
    .local v1, "destinationTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getAddressLines()Ljava/util/ArrayList;

    move-result-object v2

    .line 1173
    .local v2, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1174
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "destinationTitle":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1175
    .restart local v1    # "destinationTitle":Ljava/lang/String;
    invoke-static {v2, v5}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v0

    .line 1185
    .local v0, "destinationSubtitle":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1186
    invoke-static {v0}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1188
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 1176
    .end local v0    # "destinationSubtitle":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1177
    invoke-static {v2, v4}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isTitleIsInTheAddress(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 1178
    invoke-static {v1, v3}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isTitleIsInTheAddress(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1179
    :cond_2
    invoke-static {v2, v5}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "destinationSubtitle":Ljava/lang/String;
    goto :goto_0

    .line 1180
    .end local v0    # "destinationSubtitle":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 1181
    invoke-static {v2, v4}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "destinationSubtitle":Ljava/lang/String;
    goto :goto_0

    .line 1183
    .end local v0    # "destinationSubtitle":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .restart local v0    # "destinationSubtitle":Ljava/lang/String;
    goto :goto_0
.end method

.method public getUnselectedPinAsset()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2390
    invoke-direct {p0, v0, v0, v0}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset(ZZZ)I

    move-result v0

    return v0
.end method

.method public getZipCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    return-object v0
.end method

.method public handleNewCoordsAndAddress(DDDDLcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .locals 9
    .param p1, "displayLat"    # D
    .param p3, "displayLong"    # D
    .param p5, "navigationLat"    # D
    .param p7, "navigationLong"    # D
    .param p9, "address"    # Lcom/here/android/mpa/search/Address;
    .param p10, "precision"    # Lcom/navdy/client/app/framework/models/Destination$Precision;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 2243
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 2245
    invoke-static/range {p5 .. p8}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v5

    .line 2248
    .local v5, "newNavIsValid":Z
    if-eqz p9, :cond_1

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasDetailedAddress()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v5, :cond_1

    .line 2249
    :cond_0
    move-object/from16 v0, p9

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->setAddressFieldsFromHereAddress(Lcom/here/android/mpa/search/Address;)V

    .line 2253
    :cond_1
    if-eqz v5, :cond_3

    .line 2254
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 2255
    iput-wide p3, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 2256
    iput-wide p5, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 2257
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 2259
    sget-object v6, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    move-object/from16 v0, p10

    if-eq v0, v6, :cond_2

    .line 2260
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 2262
    :cond_2
    sget-object v6, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Navigation latLng from Here: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5, p6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; precision="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p10

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 2266
    :cond_3
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 2268
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getContentValuesForCoords()Landroid/content/ContentValues;

    move-result-object v3

    .line 2269
    .local v3, "coordValues":Landroid/content/ContentValues;
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getContentValuesForAddress()Landroid/content/ContentValues;

    move-result-object v2

    .line 2270
    .local v2, "addrValues":Landroid/content/ContentValues;
    const/4 v6, 0x2

    new-array v6, v6, [Landroid/content/ContentValues;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    invoke-direct {p0, v6}, Lcom/navdy/client/app/framework/models/Destination;->saveContentValuesToDb([Landroid/content/ContentValues;)I

    move-result v4

    .line 2271
    .local v4, "nbRows":I
    if-gtz v4, :cond_4

    .line 2272
    sget-object v6, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error while handling new coords and address from here. Unable to update the DB with values: coordValues: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and addrValues:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 2274
    :cond_4
    return-void
.end method

.method public hasAlreadyBeenProcessed()Z
    .locals 1

    .prologue
    .line 826
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    return v0
.end method

.method public hasDetailedAddress()Z
    .locals 1

    .prologue
    .line 1250
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 1251
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 1252
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOneValidSetOfCoordinates()Z
    .locals 1

    .prologue
    .line 2217
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStreetNumber()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1259
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1279
    :cond_0
    :goto_0
    return v4

    .line 1263
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 1264
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getSplitAddress()Landroid/util/Pair;

    move-result-object v2

    .line 1265
    .local v2, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 1267
    .local v3, "streetAddress":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1268
    const-string v0, "^[0-9]+[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]"

    .line 1269
    .local v0, "digitRegex":Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    .line 1271
    .local v1, "numbersExist":Z
    if-nez v1, :cond_0

    .line 1279
    .end local v0    # "digitRegex":Ljava/lang/String;
    .end local v1    # "numbersExist":Z
    .end local v2    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "streetAddress":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public hasValidDisplayCoordinates()Z
    .locals 4

    .prologue
    .line 2221
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    return v0
.end method

.method public hasValidNavCoordinates()Z
    .locals 4

    .prologue
    .line 2225
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 886
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    return v0
.end method

.method public isCalendarEvent()Z
    .locals 2

    .prologue
    .line 2653
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->CALENDAR_EVENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContact()Z
    .locals 2

    .prologue
    .line 2657
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDoNotSuggest()Z
    .locals 1

    .prologue
    .line 738
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    return v0
.end method

.method public isFavoriteDestination()Z
    .locals 1

    .prologue
    .line 2683
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    invoke-static {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination(I)Z

    move-result v0

    return v0
.end method

.method public isHome()Z
    .locals 2

    .prologue
    .line 2666
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPersisted()Z
    .locals 1

    .prologue
    .line 890
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaceDetailsInfoStale()Z
    .locals 6

    .prologue
    .line 1609
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 1610
    .local v0, "now":J
    const-wide v4, 0x9a7ec800L

    sub-long v2, v0, v4

    .line 1611
    .local v2, "refreshLimit":J
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    cmp-long v4, v4, v2

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isRecentDestination()Z
    .locals 4

    .prologue
    .line 2601
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWork()Z
    .locals 2

    .prologue
    .line 2674
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public persistPlaceDetailInfo()I
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 1755
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1772
    :goto_0
    return v3

    .line 1759
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1760
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1761
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 1763
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getPlaceDetailInfoContentValues()Landroid/content/ContentValues;

    move-result-object v2

    .line 1764
    .local v2, "destinationValues":Landroid/content/ContentValues;
    sget-object v3, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "place_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 1770
    .end local v2    # "destinationValues":Landroid/content/ContentValues;
    :cond_1
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Unable to get contentResolver !"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public persistPlaceDetailInfoAndUpdateListsAsync()V
    .locals 4

    .prologue
    .line 1692
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/Destination$3;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x1

    sget-object v3, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;

    .line 1698
    return-void
.end method

.method public refreshPlaceIdDataAndUpdateListsAsync()V
    .locals 1

    .prologue
    .line 1620
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->refreshPlaceIdDataAndUpdateListsAsync(Ljava/lang/Runnable;)V

    .line 1621
    return-void
.end method

.method public refreshPlaceIdDataAndUpdateListsAsync(Ljava/lang/Runnable;)V
    .locals 5
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 1625
    invoke-static {}, Lcom/navdy/client/app/framework/util/NetworkUtils;->isNetworkAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1626
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to refresh google place details due to lack of internet access."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1627
    if-eqz p1, :cond_0

    .line 1628
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 1689
    :cond_0
    :goto_0
    return-void

    .line 1634
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1635
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "No place ID so no refresh of google place details."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1636
    if-eqz p1, :cond_0

    .line 1637
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1643
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isPlaceDetailsInfoStale()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1644
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "No need to refresh google place details at this point."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1645
    if-eqz p1, :cond_0

    .line 1646
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 1651
    :cond_3
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Refresh google place details for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1653
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/models/Destination$2;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    .line 1687
    .local v1, "googleSearchListener":Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V

    .line 1688
    .local v0, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runDetailsSearchWebApi(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reloadSelfFromDatabase()V
    .locals 3

    .prologue
    .line 1781
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 1783
    .local v0, "dbDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-nez v0, :cond_0

    .line 1784
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "reloadSelfFromDatabase: destination not found in DB"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1789
    :goto_0
    return-void

    .line 1788
    :cond_0
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->mergeWith(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0
.end method

.method public reloadSelfFromDatabaseAsync(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "onFinish"    # Ljava/lang/Runnable;

    .prologue
    .line 1792
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/models/Destination$5;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1799
    return-void
.end method

.method public saveDestinationAsFavoritesAsync(ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
    .locals 2
    .param p1, "specialType"    # I
    .param p2, "callback"    # Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    .prologue
    .line 2721
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$11;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/framework/models/Destination$11;-><init>(Lcom/navdy/client/app/framework/models/Destination;ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 2734
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Destination$11;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2735
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .param p1, "city"    # Ljava/lang/String;

    .prologue
    .line 691
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 692
    return-void
.end method

.method public setCoordsToSame(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2
    .param p1, "latlng"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 2232
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 2233
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 2234
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 2235
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 2236
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 2238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    .line 2239
    return-void
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 0
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 718
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 719
    return-void
.end method

.method public setCountryCode(Ljava/lang/String;)V
    .locals 3
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 727
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 728
    invoke-static {p1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->convertIso3ToIso2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 729
    .local v0, "iso2":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 730
    move-object p1, v0

    .line 733
    .end local v0    # "iso2":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    .line 734
    return-void
.end method

.method public setDisplayLat(D)V
    .locals 1
    .param p1, "displayLat"    # D

    .prologue
    .line 603
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 604
    return-void
.end method

.method public setDisplayLong(D)V
    .locals 1
    .param p1, "displayLong"    # D

    .prologue
    .line 612
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 613
    return-void
.end method

.method public setDoNotSuggest(Z)V
    .locals 0
    .param p1, "doNotSuggest"    # Z

    .prologue
    .line 743
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    .line 744
    return-void
.end method

.method public setFavoriteLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "favoriteLabel"    # Ljava/lang/String;

    .prologue
    .line 779
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    .line 780
    return-void
.end method

.method public setFavoriteOrder(I)V
    .locals 0
    .param p1, "favoriteOrder"    # I

    .prologue
    .line 788
    iput p1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    .line 789
    return-void
.end method

.method public setFavoriteType(I)V
    .locals 0
    .param p1, "favoriteType"    # I

    .prologue
    .line 798
    iput p1, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 799
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 585
    iput p1, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 586
    return-void
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 8
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 1357
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1402
    :goto_0
    return-void

    .line 1362
    :cond_0
    const-string v5, "google-places-id:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1363
    const-string v5, "google-places-id:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    goto :goto_0

    .line 1368
    :cond_1
    const-string v5, "contact:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "|"

    .line 1369
    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1371
    const-string v5, "contact:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1372
    .local v4, "tmp":Ljava/lang/String;
    const-string v5, "|"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1374
    .local v1, "i":I
    if-lez v1, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1375
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1377
    .local v2, "idString":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    int-to-long v6, v5

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1383
    .end local v2    # "idString":Ljava/lang/String;
    :cond_2
    :goto_1
    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1384
    .local v3, "lookupString":Ljava/lang/String;
    const-string v5, "null"

    invoke-static {v3, v5}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1385
    iput-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 1388
    :cond_3
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    goto :goto_0

    .line 1393
    .end local v1    # "i":I
    .end local v3    # "lookupString":Ljava/lang/String;
    .end local v4    # "tmp":Ljava/lang/String;
    :cond_4
    const-string v5, "db-id:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1394
    const-string v5, "db-id:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 1398
    :cond_5
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1399
    :catch_0
    move-exception v0

    .line 1400
    .local v0, "e":Ljava/lang/NumberFormatException;
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    goto/16 :goto_0

    .line 1378
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "i":I
    .restart local v2    # "idString":Ljava/lang/String;
    .restart local v4    # "tmp":Ljava/lang/String;
    :catch_1
    move-exception v5

    goto :goto_1
.end method

.method public setIsCalendarEvent(Z)V
    .locals 0
    .param p1, "is"    # Z

    .prologue
    .line 2649
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    .line 2650
    return-void
.end method

.method public setLastRoutedDate(J)V
    .locals 1
    .param p1, "lastRoutedDate"    # J

    .prologue
    .line 753
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 754
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 651
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 652
    return-void
.end method

.method public setNavigationLat(D)V
    .locals 1
    .param p1, "navigationLat"    # D

    .prologue
    .line 627
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 628
    return-void
.end method

.method public setNavigationLong(D)V
    .locals 1
    .param p1, "navigationLong"    # D

    .prologue
    .line 635
    iput-wide p1, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 636
    return-void
.end method

.method public setPlaceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 595
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 596
    return-void
.end method

.method public setRawAddressNotForDisplay(Ljava/lang/String;)V
    .locals 1
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 664
    invoke-static {p1}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 665
    return-void
.end method

.method public setSearchResultType(I)V
    .locals 0
    .param p1, "searchResultType"    # I

    .prologue
    .line 763
    iput p1, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 764
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 700
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 701
    return-void
.end method

.method public setStreetName(Ljava/lang/String;)V
    .locals 0
    .param p1, "streetName"    # Ljava/lang/String;

    .prologue
    .line 682
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 683
    return-void
.end method

.method public setStreetNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "streetNumber"    # Ljava/lang/String;

    .prologue
    .line 673
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 674
    return-void
.end method

.method public setZipCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "zipCode"    # Ljava/lang/String;

    .prologue
    .line 709
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 710
    return-void
.end method

.method public toJsonString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 500
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "lastPlaceIdRfrshTime":Ljava/lang/String;
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "lastRoutedDateTime":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "{\"id\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"placeId\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"name\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"Type\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 506
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination$Type;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"precisionLevel\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 507
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination$Precision;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"address\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"displayLat\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"displayLong\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"navigationLat\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"navigationLong\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"streetNumber\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"streetName\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"city\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"state\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"zipCode\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"country\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "),"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"doNotSuggest\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"lastRoutedDate\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"favoriteLabel\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"favoriteOrder\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"favoriteType\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"searchResultType\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"lastPlaceIdRefresh\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"placeDetailJson\":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"contactLookupKey\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"lastKnownContactId\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \"lastContactLookup\":\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;
    .locals 12

    .prologue
    .line 1314
    .line 1315
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteTypeForProto()Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-result-object v1

    .line 1318
    .local v1, "favoriteType":Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v6

    .line 1319
    .local v6, "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {v6}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->cleanUpDebugPrefix(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object v6

    .line 1320
    iget-object v2, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1321
    .local v2, "firstLine":Ljava/lang/String;
    iget-object v5, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    .line 1323
    .local v5, "secondLine":Ljava/lang/String;
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getPhoneNumbers()Ljava/util/ArrayList;

    move-result-object v3

    .line 1325
    .local v3, "phoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getContacts()Ljava/util/ArrayList;

    move-result-object v0

    .line 1327
    .local v0, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getPlaceIndentifier()Ljava/lang/String;

    move-result-object v4

    .line 1329
    .local v4, "placeIdentifier":Ljava/lang/String;
    new-instance v7, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>()V

    new-instance v8, Lcom/navdy/service/library/events/location/LatLong;

    iget-wide v10, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 1330
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    iget-wide v10, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    new-instance v8, Lcom/navdy/service/library/events/location/LatLong;

    iget-wide v10, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 1331
    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    iget-wide v10, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 1332
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1333
    invoke-virtual {v7, v2}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1334
    invoke-virtual {v7, v5}, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 1335
    invoke-virtual {v8}, Lcom/navdy/client/app/framework/models/Destination$Type;->toProtobufPlaceType()Lcom/navdy/service/library/events/places/PlaceType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1336
    invoke-virtual {v7, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    iget v8, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 1337
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1338
    invoke-virtual {v7, v4}, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_id(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 1339
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1340
    invoke-virtual {v7, v3}, Lcom/navdy/service/library/events/destination/Destination$Builder;->phoneNumbers(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1341
    invoke-virtual {v7, v0}, Lcom/navdy/service/library/events/destination/Destination$Builder;->contacts(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v7

    .line 1342
    invoke-virtual {v7}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v7

    return-object v7
.end method

.method public toShortString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 490
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v0

    .line 491
    .local v0, "lastPlaceIdRfrshTime":Ljava/lang/String;
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v1

    .line 492
    .local v1, "lastRoutedDateTime":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Destination{id=%d,\tplaceId=%s,\tlastPlaceIdRefresh=%s,\tname=%s,\tType=%s,\tprecisionLevel=%s\t,address=%s,\tdisplayLat=%s,\tdisplayLong=%s,\tnavigationLat=%s,\tnavigationLong=%s,\thasDetailedAddress=%s,\tdoNotSuggest=%s,\tlastRoutedDate=%s,\tfavoriteLabel=%s,\tfavoriteType=%d,\tisCalendarEvent=%s,\tsearchResultType=%d}"

    const/16 v4, 0x12

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 494
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v0, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Destination$Type;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Destination$Precision;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 495
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x9

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xa

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasDetailedAddress()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xc

    iget-boolean v6, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xd

    aput-object v1, v4, v5

    const/16 v5, 0xe

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 496
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x10

    iget-boolean v6, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x11

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 492
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 480
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v0

    .line 481
    .local v0, "lastPlaceIdRfrshTime":Ljava/lang/String;
    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, "lastRoutedDateTime":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Destination{id=%d,\tplaceId=%s,\tlastPlaceIdRefresh=%s,\tname=%s,\tType=%s,\tprecisionLevel=%s\t,address=%s,\tdisplayLat=%s,\tdisplayLong=%s,\tnavigationLat=%s,\tnavigationLong=%s,\tstreetNumber=%s,\tstreetName=%s,\tcity=%s,\tstate=%s,\tzipCode=%s,\tcountry=%s (%s),\tdoNotSuggest=%s,\tlastRoutedDate=%s,\tfavoriteLabel=%s,\tfavoriteOrder=%d,\tfavoriteType=%d,\tisCalendarEvent=%s,\tsearchResultType=%d,\tcontactLookupKey=%s,\tlastKnownContactId=%d,\tlastContactLookup=%d}"

    const/16 v4, 0x1c

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 484
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v0, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Destination$Type;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Destination$Precision;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 485
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x9

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xa

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xb

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x10

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x11

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x12

    iget-boolean v6, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x13

    aput-object v1, v4, v5

    const/16 v5, 0x14

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x15

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x16

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    .line 486
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x17

    iget-boolean v6, p0, Lcom/navdy/client/app/framework/models/Destination;->isCalendarEvent:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x18

    iget v6, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x19

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 482
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public updateAllColumnsOrInsertNewEntryInDb()Landroid/net/Uri;
    .locals 6
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1961
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Saving this destination to the DB: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1963
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1965
    iget v1, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    int-to-long v2, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 1967
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->findInDb()I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 1969
    iget v1, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    int-to-long v2, v1

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1971
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->saveToDb()Landroid/net/Uri;

    move-result-object v1

    .line 1978
    :goto_0
    return-object v1

    .line 1976
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->getAllContentValues()Landroid/content/ContentValues;

    move-result-object v0

    .line 1977
    .local v0, "destinationValues":Landroid/content/ContentValues;
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->updateDb(Landroid/content/ContentValues;)I

    .line 1978
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    iget v2, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public updateDestinationListsAsync()V
    .locals 4

    .prologue
    .line 1708
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/Destination$4;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x1

    sget-object v3, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;

    .line 1714
    return-void
.end method

.method public updateDoNotSuggestAsync()V
    .locals 3

    .prologue
    .line 1985
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$7;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/Destination$7;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1991
    return-void
.end method

.method public updateLastRoutedDateInDb()I
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2621
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    .line 2622
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 2624
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Destination;->makeSureWeHaveDestinationInDb()V

    .line 2626
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2628
    .local v3, "recentValues":Landroid/content/ContentValues;
    const-string v4, "last_routed_date"

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2629
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 2630
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2631
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "%s=?"

    new-array v6, v9, [Ljava/lang/Object;

    const-string v7, "_id"

    aput-object v7, v6, v8

    .line 2634
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/String;

    iget v7, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 2635
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    .line 2631
    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2636
    .local v2, "nbUpdatedRows":I
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updated "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " entries"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 2638
    if-lez v2, :cond_1

    .line 2639
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->forceSuggestionFullRefresh()V

    .line 2641
    :cond_1
    return v2
.end method

.method public updateLastRoutedDateInDbAsync()V
    .locals 3

    .prologue
    .line 2608
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$10;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/Destination$10;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 2614
    return-void
.end method

.method public updateOnlyFavoriteFieldsInDb()I
    .locals 1

    .prologue
    .line 2818
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/models/Destination;->updateOnlyFavoriteFieldsInDb(Z)I

    move-result v0

    return v0
.end method

.method public updateOnlyFavoriteFieldsInDb(Z)I
    .locals 5
    .param p1, "sendToDisplay"    # Z

    .prologue
    .line 2834
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2835
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v3, "place_name"

    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2836
    const-string v3, "favorite_label"

    iget-object v4, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2837
    const-string v3, "favorite_listing_order"

    iget v4, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2838
    const-string v3, "is_special"

    iget v4, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2840
    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/models/Destination;->updateDb(Landroid/content/ContentValues;)I

    move-result v2

    .line 2842
    .local v2, "nbRows":I
    if-lez v2, :cond_1

    .line 2843
    if-eqz p1, :cond_0

    .line 2844
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 2850
    :cond_0
    :goto_0
    return v2

    .line 2847
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 2848
    .local v0, "context":Landroid/content/Context;
    sget-object v3, Lcom/navdy/client/app/framework/models/Destination;->logger:Lcom/navdy/service/library/log/Logger;

    const v4, 0x7f0804bc

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 352
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 354
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastPlaceIdRefresh:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 355
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 356
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 357
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 358
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeDouble(D)V

    .line 359
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 362
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->countryCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 368
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 369
    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 370
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 371
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteOrder:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 372
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 373
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$Precision;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 376
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$Type;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 377
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/models/Destination;->hasBeenProcessed:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 378
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 379
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 380
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 381
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 382
    return-void

    :cond_0
    move v0, v2

    .line 368
    goto :goto_0

    :cond_1
    move v1, v2

    .line 377
    goto :goto_1
.end method
