.class public Lcom/navdy/client/app/framework/models/Trip;
.super Ljava/lang/Object;
.source "Trip.java"


# static fields
.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public arrivedAtDestination:J

.field public destinationId:I

.field public endLat:D

.field public endLong:D

.field public endOdometer:I

.field public endTime:J

.field public id:I

.field public offset:I

.field public startLat:D

.field public startLong:D

.field public startOdometer:I

.field public startTime:J

.field public tripNumber:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/Trip;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Trip;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(IJJIIDDJIDDJI)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "tripNumber"    # J
    .param p4, "startTime"    # J
    .param p6, "offset"    # I
    .param p7, "startOdometer"    # I
    .param p8, "startLat"    # D
    .param p10, "startLong"    # D
    .param p12, "endTime"    # J
    .param p14, "endOdometer"    # I
    .param p15, "endLat"    # D
    .param p17, "endLong"    # D
    .param p19, "arrivedAtDestination"    # J
    .param p21, "destinationId"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/navdy/client/app/framework/models/Trip;->id:I

    .line 38
    iput-wide p2, p0, Lcom/navdy/client/app/framework/models/Trip;->tripNumber:J

    .line 39
    iput-wide p4, p0, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    .line 40
    iput p6, p0, Lcom/navdy/client/app/framework/models/Trip;->offset:I

    .line 41
    iput p7, p0, Lcom/navdy/client/app/framework/models/Trip;->startOdometer:I

    .line 42
    iput-wide p8, p0, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    .line 43
    iput-wide p10, p0, Lcom/navdy/client/app/framework/models/Trip;->startLong:D

    .line 44
    iput-wide p12, p0, Lcom/navdy/client/app/framework/models/Trip;->endTime:J

    .line 45
    move/from16 v0, p14

    iput v0, p0, Lcom/navdy/client/app/framework/models/Trip;->endOdometer:I

    .line 46
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    .line 47
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    .line 48
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lcom/navdy/client/app/framework/models/Trip;->arrivedAtDestination:J

    .line 49
    move/from16 v0, p21

    iput v0, p0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    .line 50
    return-void
.end method

.method public static saveLastTripUpdate(Landroid/content/Context;Lcom/navdy/service/library/events/TripUpdate;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "arrivedAtDestination":I
    iget-object v4, p1, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 117
    const/4 v0, 0x1

    .line 120
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 122
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const/4 v3, -0x1

    .line 123
    .local v3, "nbRows":I
    if-eqz v1, :cond_1

    .line 124
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 127
    .local v2, "contentValues":Landroid/content/ContentValues;
    const-string v4, "end_time"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 128
    const-string v4, "end_odometer"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    const-string v4, "end_lat"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 130
    const-string v4, "end_long"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 131
    const-string v4, "arrived_at_destination"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 132
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "%s=?"

    new-array v6, v9, [Ljava/lang/Object;

    const-string v7, "trip_number"

    aput-object v7, v6, v8

    .line 133
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v9, [Ljava/lang/String;

    iget-object v7, p1, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    .line 134
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    .line 132
    invoke-virtual {v1, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 136
    .end local v2    # "contentValues":Landroid/content/ContentValues;
    :cond_1
    return v3
.end method

.method public static saveStartingTripUpdate(Landroid/content/Context;Lcom/navdy/service/library/events/TripUpdate;)Landroid/net/Uri;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    .line 82
    sget-object v4, Lcom/navdy/client/app/framework/models/Trip;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inserting trip_number: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 85
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 86
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 88
    .local v1, "contentValues":Landroid/content/ContentValues;
    const/4 v2, 0x0

    .line 89
    .local v2, "destinationId":I
    iget-object v4, p1, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    iget-object v4, p1, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 93
    :cond_0
    iget-object v4, p1, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/SystemUtils;->getTimeZoneAndDaylightSavingOffset(Ljava/lang/Long;)I

    move-result v3

    .line 96
    .local v3, "offset":I
    const-string v4, "trip_number"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 97
    const-string v4, "start_time"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 98
    const-string v4, "start_time_zone_n_dst"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 99
    const-string v4, "start_odometer"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 100
    const-string v4, "start_lat"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 101
    const-string v4, "start_long"

    iget-object v5, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v5, v5, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 102
    const-string v4, "destination_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 103
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 105
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    .end local v2    # "destinationId":I
    .end local v3    # "offset":I
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public saveToDb(Landroid/content/Context;)Landroid/net/Uri;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 54
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 55
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 56
    .local v1, "tripValues":Landroid/content/ContentValues;
    const-string v2, "trip_number"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->tripNumber:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 57
    const-string v2, "start_time"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 58
    const-string v2, "start_time_zone_n_dst"

    iget v3, p0, Lcom/navdy/client/app/framework/models/Trip;->offset:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 59
    const-string v2, "start_odometer"

    iget v3, p0, Lcom/navdy/client/app/framework/models/Trip;->startOdometer:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 60
    const-string v2, "start_lat"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 61
    const-string v2, "start_long"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->startLong:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 62
    const-string v2, "end_time"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->endTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 63
    const-string v2, "end_odometer"

    iget v3, p0, Lcom/navdy/client/app/framework/models/Trip;->endOdometer:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 64
    const-string v2, "end_lat"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 65
    const-string v2, "end_long"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 66
    const-string v2, "arrived_at_destination"

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/Trip;->arrivedAtDestination:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 67
    const-string v2, "destination_id"

    iget v3, p0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 68
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 72
    .end local v1    # "tripValues":Landroid/content/ContentValues;
    :goto_0
    return-object v2

    .line 70
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/models/Trip;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to get contentResolver !"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 72
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDestinationIdAndSaveToDb(I)I
    .locals 11
    .param p1, "destId"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 144
    sget-object v4, Lcom/navdy/client/app/framework/models/Trip;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updating trip with destination id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 148
    .local v2, "context":Landroid/content/Context;
    iput p1, p0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    .line 149
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 150
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v3, -0x1

    .line 151
    .local v3, "nbRows":I
    if-eqz v0, :cond_0

    .line 152
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 155
    .local v1, "contentValues":Landroid/content/ContentValues;
    const-string v4, "destination_id"

    iget v5, p0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "%s=?"

    new-array v6, v8, [Ljava/lang/Object;

    const-string v7, "trip_number"

    aput-object v7, v6, v10

    .line 158
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/String;

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Trip;->tripNumber:J

    .line 159
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    .line 156
    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 161
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    :cond_0
    return v3
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trip{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Trip;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\ttripNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->tripNumber:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tstartTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\toffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Trip;->offset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tstartOdometer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Trip;->startOdometer:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tstartLat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tstartLong="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->startLong:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tendTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->endTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tendOdometer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Trip;->endOdometer:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tendLat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tendLong="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tarrivedAtDestination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/Trip;->arrivedAtDestination:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\tdestinationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Trip;->destinationId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
