.class public Lcom/navdy/client/app/framework/models/Suggestion;
.super Ljava/lang/Object;
.source "Suggestion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    }
.end annotation


# static fields
.field private static final NO_RESOURCE:I = -0x1


# instance fields
.field public calculateSuggestedRoute:Z

.field public destination:Lcom/navdy/client/app/framework/models/Destination;

.field public event:Lcom/navdy/client/app/framework/models/CalendarEvent;

.field private type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "type"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->calculateSuggestedRoute:Z

    .line 70
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 71
    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 72
    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;Lcom/navdy/client/app/framework/models/CalendarEvent;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "type"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .param p3, "event"    # Lcom/navdy/client/app/framework/models/CalendarEvent;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->calculateSuggestedRoute:Z

    .line 76
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 77
    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 78
    iput-object p3, p0, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 79
    return-void
.end method

.method private getSuggestionType()Lcom/navdy/service/library/events/destination/Destination$SuggestionType;
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isCalendar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 234
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_RECENT:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    goto :goto_0
.end method

.method private isColorful()Z
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isTip()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isCalendar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isRecommendation()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public canBeRoutedTo()Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 83
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/navdy/client/app/framework/models/Suggestion;

    if-nez v2, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 86
    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 87
    .local v0, "other":Lcom/navdy/client/app/framework/models/Suggestion;
    iget-object v2, v0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/models/Destination;->equals(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBadgeAsset(Z)I
    .locals 3
    .param p1, "isSelected"    # Z

    .prologue
    .line 174
    if-eqz p1, :cond_0

    .line 175
    const v1, 0x7f0201c9

    .line 218
    :goto_0
    return v1

    .line 178
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$1;->$SwitchMap$com$navdy$client$app$framework$models$Suggestion$SuggestionType:[I

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 218
    const/4 v1, -0x1

    goto :goto_0

    .line 180
    :pswitch_0
    const v1, 0x7f0200c0

    goto :goto_0

    .line 183
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 184
    .local v0, "isPending":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 185
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForPendingTrip()I

    move-result v1

    goto :goto_0

    .line 183
    .end local v0    # "isPending":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 187
    .restart local v0    # "isPending":Z
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForSuggestion()I

    move-result v1

    goto :goto_0

    .line 189
    .end local v0    # "isPending":Z
    :pswitch_2
    const v1, 0x7f020143

    goto :goto_0

    .line 192
    :pswitch_3
    const v1, 0x7f020144

    goto :goto_0

    .line 195
    :pswitch_4
    const v1, 0x7f0201c0

    goto :goto_0

    .line 197
    :pswitch_5
    const v1, 0x7f020162

    goto :goto_0

    .line 199
    :pswitch_6
    const v1, 0x7f020176

    goto :goto_0

    .line 201
    :pswitch_7
    const v1, 0x7f020126

    goto :goto_0

    .line 203
    :pswitch_8
    const v1, 0x7f020118

    goto :goto_0

    .line 205
    :pswitch_9
    const v1, 0x7f02013c

    goto :goto_0

    .line 207
    :pswitch_a
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isColorful()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f020121

    goto :goto_0

    :cond_3
    const v1, 0x7f020123

    goto :goto_0

    .line 209
    :pswitch_b
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isColorful()Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f020129

    goto :goto_0

    :cond_4
    const v1, 0x7f02012b

    goto :goto_0

    .line 211
    :pswitch_c
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isColorful()Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f020145

    goto :goto_0

    :cond_5
    const v1, 0x7f020147

    goto :goto_0

    .line 213
    :pswitch_d
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset()I

    move-result v1

    goto :goto_0

    .line 215
    :pswitch_e
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForRecent()I

    move-result v1

    goto/16 :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    return-object v0
.end method

.method public hasInfoButton()Z
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->OTA:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isTip()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->hashCode()I

    move-result v0

    .line 93
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 94
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/models/Suggestion;->calculateSuggestedRoute:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    add-int v0, v3, v2

    .line 95
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 96
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 92
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 93
    goto :goto_1

    :cond_3
    move v2, v1

    .line 94
    goto :goto_2
.end method

.method public isActiveTrip()Z
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCalendar()Z
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPendingTrip()Z
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecommendation()Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTip()Z
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->DEMO_VIDEO:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GLANCES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_MICROPHONE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->VOICE_SEARCH:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_NOW:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HUD_LOCAL_MUSIC_BROWSER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->TRY_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_HOME:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_WORK:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->OTA:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_FAVORITE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldSendToHud()Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isCalendar()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isRecommendation()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldShowRightChevron()Z
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GLANCES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_MICROPHONE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->VOICE_SEARCH:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_NOW:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HUD_LOCAL_MUSIC_BROWSER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->TRY_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_HOME:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_WORK:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_FAVORITE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;
    .locals 2

    .prologue
    .line 223
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$Builder;

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 224
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>(Lcom/navdy/service/library/events/destination/Destination;)V

    .line 225
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->getSuggestionType()Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v0

    .line 226
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Suggestion;->isRecommendation()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Suggestion{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->type:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", calculateSuggestedRoute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->calculateSuggestedRoute:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", destination="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
