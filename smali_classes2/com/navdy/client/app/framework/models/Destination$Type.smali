.class public final enum Lcom/navdy/client/app/framework/models/Destination$Type;
.super Ljava/lang/Enum;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/models/Destination$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum AIRPORT:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum ATM:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum BANK:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum BAR:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum CALENDAR_EVENT:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum COFFEE:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum GAS_STATION:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum GYM:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum PARK:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum PARKING:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum SCHOOL:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

.field public static final enum UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 220
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 221
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 222
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "CALENDAR_EVENT"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->CALENDAR_EVENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 223
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 224
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "GAS_STATION"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->GAS_STATION:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 225
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "AIRPORT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->AIRPORT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 226
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "PARKING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARKING:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 227
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "TRANSIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 228
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "ATM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ATM:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 229
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "BANK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BANK:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 230
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "SCHOOL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->SCHOOL:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 231
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "STORE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 232
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "COFFEE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->COFFEE:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 233
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "RESTAURANT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 234
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "GYM"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->GYM:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 235
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "PARK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARK:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 236
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "HOSPITAL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 237
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "ENTERTAINMENT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 238
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    const-string v1, "BAR"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BAR:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 219
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/navdy/client/app/framework/models/Destination$Type;

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->CALENDAR_EVENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->GAS_STATION:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->AIRPORT:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->PARKING:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->ATM:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->BANK:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->SCHOOL:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->COFFEE:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->GYM:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->PARK:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Type;->BAR:Lcom/navdy/client/app/framework/models/Destination$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->$VALUES:[Lcom/navdy/client/app/framework/models/Destination$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 219
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static get(I)Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 245
    invoke-static {}, Lcom/navdy/client/app/framework/models/Destination$Type;->values()[Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 219
    const-class v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination$Type;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->$VALUES:[Lcom/navdy/client/app/framework/models/Destination$Type;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/models/Destination$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/models/Destination$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination$Type;->ordinal()I

    move-result v0

    return v0
.end method

.method public toProtobufPlaceType()Lcom/navdy/service/library/events/places/PlaceType;
    .locals 2

    .prologue
    .line 249
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$13;->$SwitchMap$com$navdy$client$app$framework$models$Destination$Type:[I

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 253
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    .line 287
    :goto_0
    return-object v0

    .line 255
    :pswitch_0
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_CONTACT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 257
    :pswitch_1
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_CALENDAR_EVENT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 259
    :pswitch_2
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 261
    :pswitch_3
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_AIRPORT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 263
    :pswitch_4
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 265
    :pswitch_5
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_TRANSIT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 267
    :pswitch_6
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 269
    :pswitch_7
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_BANK:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 271
    :pswitch_8
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_SCHOOL:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 273
    :pswitch_9
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 275
    :pswitch_a
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 277
    :pswitch_b
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 279
    :pswitch_c
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GYM:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 281
    :pswitch_d
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARK:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 283
    :pswitch_e
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 285
    :pswitch_f
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ENTERTAINMENT:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 287
    :pswitch_10
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_BAR:Lcom/navdy/service/library/events/places/PlaceType;

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method
