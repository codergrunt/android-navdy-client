.class public final enum Lcom/navdy/client/app/framework/models/Destination$Precision;
.super Ljava/lang/Enum;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Precision"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/models/Destination$Precision;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/models/Destination$Precision;

.field public static final enum IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

.field public static final enum PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

.field public static final enum UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;


# instance fields
.field level:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 188
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Precision;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/client/app/framework/models/Destination$Precision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 189
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Precision;

    const-string v1, "IMPRECISE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/client/app/framework/models/Destination$Precision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 190
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$Precision;

    const-string v1, "PRECISE"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/client/app/framework/models/Destination$Precision;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 187
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/models/Destination$Precision;

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Precision;->IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->$VALUES:[Lcom/navdy/client/app/framework/models/Destination$Precision;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "level"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 195
    iput p3, p0, Lcom/navdy/client/app/framework/models/Destination$Precision;->level:I

    .line 196
    return-void
.end method

.method public static get(I)Lcom/navdy/client/app/framework/models/Destination$Precision;
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 203
    invoke-static {}, Lcom/navdy/client/app/framework/models/Destination$Precision;->values()[Lcom/navdy/client/app/framework/models/Destination$Precision;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static get(Z)Lcom/navdy/client/app/framework/models/Destination$Precision;
    .locals 1
    .param p0, "isImprecise"    # Z

    .prologue
    .line 207
    if-eqz p0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->PRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$Precision;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 187
    const-class v0, Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination$Precision;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/models/Destination$Precision;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->$VALUES:[Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/models/Destination$Precision;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/models/Destination$Precision;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination$Precision;->level:I

    return v0
.end method

.method public isPrecise()Z
    .locals 1

    .prologue
    .line 215
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Precision;->IMPRECISE:Lcom/navdy/client/app/framework/models/Destination$Precision;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
