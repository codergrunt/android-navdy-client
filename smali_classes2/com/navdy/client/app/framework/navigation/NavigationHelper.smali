.class Lcom/navdy/client/app/framework/navigation/NavigationHelper;
.super Ljava/lang/Object;
.source "NavigationHelper.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final hudNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/navdy/client/debug/navigation/HUDNavigationManager;

    invoke-direct {v0}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->hudNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    .line 50
    return-void
.end method

.method private searchRoutesOnHud(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;ZZLcom/navdy/service/library/events/destination/Destination;)V
    .locals 15
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "navigationCoordinate"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p5, "destinationId"    # Ljava/lang/String;
    .param p6, "favoriteType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "display"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p8, "geocodeAddressOnHud"    # Z
    .param p9, "initiatedOnHud"    # Z
    .param p10, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 129
    new-instance v2, Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v0, p4

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    move-object/from16 v0, p4

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const-string v10, "Google"

    invoke-direct/range {v2 .. v10}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 131
    .local v2, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 132
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Requesting new route for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 133
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sending route to HUD:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " address: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " destinationId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " favoriteType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " coordinate:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " display: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " streetAddress:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " useStreetAddress: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p8

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " initiatedOnHud: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p9

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 134
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 135
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->hudNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    const/4 v6, 0x0

    move-object v4, v2

    move-object/from16 v5, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p1

    move/from16 v12, p8

    move/from16 v13, p9

    move-object/from16 v14, p10

    invoke-virtual/range {v3 .. v14}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;ZZLcom/navdy/service/library/events/destination/Destination;)Z

    .line 136
    return-void
.end method

.method private sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;I)V

    .line 140
    return-void
.end method

.method private sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;I)V
    .locals 7
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .param p3, "simulationSpeed"    # I

    .prologue
    .line 143
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attempting to change state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 144
    if-nez p1, :cond_0

    .line 145
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sendStateChangeRequest failed because new NavigationSessionState is null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 171
    :goto_0
    return-void

    .line 148
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 149
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Device is no longer valid. Creating new Device Connection"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_1
    if-nez p2, :cond_2

    .line 153
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Didn\'t get any route as chosen"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_2
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .line 159
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    iget-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    iget-object v3, p2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    .line 161
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v1, 0x0

    .line 162
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 164
    .local v0, "request":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v6

    .line 166
    .local v6, "success":Z
    if-eqz v6, :cond_3

    .line 167
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "State change request sent successfully."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 169
    :cond_3
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "State change request failed to send."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method cancelRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 99
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v0, p1}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 100
    return-void
.end method

.method cancelRouteCalculation(Ljava/lang/String;)V
    .locals 4
    .param p1, "cancelHandle"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 88
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    invoke-direct {v0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "navigationRouteCancelRequest":Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v1

    .line 91
    .local v1, "success":Z
    if-eqz v1, :cond_0

    .line 92
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "cancelRouteCalculation sent successfully."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "cancelRouteCalculation failed to send."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method getNavigationSessionState()V
    .locals 4

    .prologue
    .line 103
    new-instance v0, Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;

    invoke-direct {v0}, Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;-><init>()V

    .line 104
    .local v0, "request":Lcom/navdy/service/library/events/navigation/GetNavigationSessionState;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v1

    .line 106
    .local v1, "success":Z
    if-eqz v1, :cond_0

    .line 107
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "getNavigationSessionState sent successfully"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "getNavigationSessionState failed to send"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method declared-synchronized navigateToDestination(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Z)V
    .locals 12
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "requestId"    # Ljava/lang/String;
    .param p3, "initiatedOnHud"    # Z

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "navigateToDestination: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v8, 0x1

    .line 69
    .local v8, "geocodeStreetAddressOnHud":Z
    :goto_0
    if-eqz v8, :cond_0

    .line 70
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No valid coordinates found. Requesting for HUD to geocode the street address for Navigation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 75
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getNameForDisplay()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v4, v0, v1, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget v0, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 78
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 79
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteTypeForProto()Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-result-object v6

    iget-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v10, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 80
    invoke-static {v0, v1, v10, v11}, Lcom/navdy/client/app/framework/map/MapUtils;->buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v7

    .line 83
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v10

    move-object v0, p0

    move-object v1, p2

    move v9, p3

    .line 73
    invoke-direct/range {v0 .. v10}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->searchRoutesOnHud(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Lcom/navdy/service/library/events/location/Coordinate;ZZLcom/navdy/service/library/events/destination/Destination;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 67
    .end local v8    # "geocodeStreetAddressOnHud":Z
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
