.class Lcom/navdy/client/app/framework/search/NavdySearch$2;
.super Landroid/os/AsyncTask;
.source "NavdySearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/search/NavdySearch;->runOnlineSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    iput-object p2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->val$query:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 263
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->val$query:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$100(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)V

    .line 264
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 260
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 5
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 269
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    new-instance v2, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch$2;)V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 285
    .local v0, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    iget-object v3, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->val$query:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$500(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, "newQuery":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->val$query:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->val$query:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runTextSearchWebApi(Ljava/lang/String;)V

    .line 292
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$200(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Query was a \'the nearest\' kind of query. We will seach for the closest: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runNearbySearchWebApi(Ljava/lang/String;)V

    goto :goto_0
.end method
