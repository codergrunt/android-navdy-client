.class Lcom/navdy/client/app/framework/search/NavdySearch$2$1;
.super Ljava/lang/Object;
.source "NavdySearch.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/search/NavdySearch$2;->onPostExecute(Ljava/lang/Void;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/search/NavdySearch$2;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/search/NavdySearch$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/search/NavdySearch$2;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;->this$1:Lcom/navdy/client/app/framework/search/NavdySearch$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 3
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "googleResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->NONE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    if-eq p3, v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;->this$1:Lcom/navdy/client/app/framework/search/NavdySearch$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v0}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$200(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received Google places search error while running online search: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;->this$1:Lcom/navdy/client/app/framework/search/NavdySearch$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v0}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$300(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/client/app/framework/search/SearchResults;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/SearchResults;->setGoogleResults(Ljava/util/List;)V

    .line 281
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;->this$1:Lcom/navdy/client/app/framework/search/NavdySearch$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v0}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$400(Lcom/navdy/client/app/framework/search/NavdySearch;)V

    .line 282
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch$2$1;->this$1:Lcom/navdy/client/app/framework/search/NavdySearch$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/NavdySearch$2;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v0}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$300(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/client/app/framework/search/SearchResults;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/search/SearchResults;->setGoogleResults(Ljava/util/List;)V

    goto :goto_0
.end method
