.class final Lcom/navdy/client/app/framework/util/MusicUtils$5;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/MusicUtils;->sendPlaybackIntents(Ljava/lang/String;Lcom/navdy/service/library/events/audio/MusicEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$event:Lcom/navdy/service/library/events/audio/MusicEvent;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$5;->val$packageName:Ljava/lang/String;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$5;->val$event:Lcom/navdy/service/library/events/audio/MusicEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private broadcastPlayEventViaIntent(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 416
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 417
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/16 v3, 0x7e

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 418
    .local v1, "playButtonEvent":Landroid/view/KeyEvent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    const-string v2, "android.intent.extra.KEY_EVENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 420
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 421
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 393
    const/4 v1, 0x0

    .local v1, "retries":I
    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_1

    .line 394
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->playbackState:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    sget-object v3, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    if-eq v2, v3, :cond_2

    .line 396
    const-string v2, "com.spotify.music"

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/MusicUtils$5;->val$packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 397
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$5;->val$event:Lcom/navdy/service/library/events/audio/MusicEvent;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/MusicUtils;->access$400(Lcom/navdy/service/library/events/audio/MusicEvent;)V

    .line 402
    :goto_1
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 399
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$5;->val$packageName:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/navdy/client/app/framework/util/MusicUtils$5;->broadcastPlayEventViaIntent(Ljava/lang/String;)V

    goto :goto_1

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 412
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Failed to start music"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 413
    :goto_2
    return-void

    .line 408
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Started music"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_2
.end method
