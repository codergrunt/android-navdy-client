.class public Lcom/navdy/client/app/framework/util/PackagedResource;
.super Ljava/lang/Object;
.source "PackagedResource.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x4000

.field private static final MD5_SUFFIX:Ljava/lang/String; = ".md5"

.field private static final lockObject:Ljava/lang/Object;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private basePath:Ljava/lang/String;

.field private destinationFilename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/PackagedResource;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/PackagedResource;->lockObject:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "destinationFilename"    # Ljava/lang/String;
    .param p2, "basePath"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->basePath:Ljava/lang/String;

    .line 37
    return-void
.end method

.method private static extractFile(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V
    .locals 6
    .param p0, "zis"    # Ljava/util/zip/ZipInputStream;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "buffer"    # [B

    .prologue
    .line 162
    const/4 v1, 0x0

    .line 166
    .local v1, "fout":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .local v2, "fout":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_1
    invoke-virtual {p0, p2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v0

    .local v0, "count":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 169
    const/4 v4, 0x0

    invoke-virtual {v2, p2, v4, v0}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 173
    .end local v0    # "count":I
    :catch_0
    move-exception v3

    move-object v1, v2

    .line 174
    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    .local v3, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v4, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "error extracting file"

    invoke-virtual {v4, v5, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 176
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 178
    .end local v3    # "t":Ljava/lang/Throwable;
    :goto_2
    return-void

    .line 172
    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v0    # "count":I
    .restart local v2    # "fout":Ljava/io/FileOutputStream;
    :cond_0
    :try_start_3
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 176
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 177
    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 176
    .end local v0    # "count":I
    :catchall_0
    move-exception v4

    :goto_3
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v4

    .end local v1    # "fout":Ljava/io/FileOutputStream;
    .restart local v2    # "fout":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fout":Ljava/io/FileOutputStream;
    .restart local v1    # "fout":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 173
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private static unpack(Ljava/lang/String;Ljava/io/InputStream;)Z
    .locals 10
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 117
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 119
    .local v2, "destDir":Ljava/io/File;
    const/16 v8, 0x4000

    new-array v0, v8, [B

    .line 121
    .local v0, "buffer":[B
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 122
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 124
    .local v1, "couldCreateDirs":Z
    if-nez v1, :cond_0

    .line 125
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "unpack could not create destination dirs"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 129
    .end local v1    # "couldCreateDirs":Z
    :cond_0
    new-instance v7, Ljava/util/zip/ZipInputStream;

    new-instance v8, Ljava/io/BufferedInputStream;

    invoke-direct {v8, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v7, v8}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 133
    .local v7, "zis":Ljava/util/zip/ZipInputStream;
    :goto_0
    :try_start_0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    .local v6, "ze":Ljava/util/zip/ZipEntry;
    if-eqz v6, :cond_3

    .line 134
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, "filePath":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_2

    .line 138
    invoke-static {v7, v5, v0}, Lcom/navdy/client/app/framework/util/PackagedResource;->extractFile(Ljava/util/zip/ZipInputStream;Ljava/lang/String;[B)V

    .line 149
    :cond_1
    :goto_1
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->closeEntry()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v4

    .line 152
    .local v4, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Error expanding zipEntry"

    invoke-virtual {v8, v9, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    const/4 v8, 0x0

    .line 155
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 158
    .end local v4    # "e":Ljava/lang/Throwable;
    :goto_2
    return v8

    .line 141
    .restart local v5    # "filePath":Ljava/lang/String;
    .restart local v6    # "ze":Ljava/util/zip/ZipEntry;
    :cond_2
    :try_start_2
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    .local v3, "dir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 144
    .restart local v1    # "couldCreateDirs":Z
    if-nez v1, :cond_1

    .line 145
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "unpack could not create destination dirs"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 155
    .end local v1    # "couldCreateDirs":Z
    .end local v3    # "dir":Ljava/io/File;
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    :catchall_0
    move-exception v8

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .restart local v6    # "ze":Ljava/util/zip/ZipEntry;
    :cond_3
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 158
    const/4 v8, 0x1

    goto :goto_2
.end method

.method public static unpackZip(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "zipname"    # Ljava/lang/String;

    .prologue
    .line 103
    const/4 v1, 0x0

    .line 106
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {p0, v2}, Lcom/navdy/client/app/framework/util/PackagedResource;->unpack(Ljava/lang/String;Ljava/io/InputStream;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 112
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :goto_0
    return v3

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    sget-object v3, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Error reading resource"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 110
    const/4 v3, 0x0

    .line 112
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    :goto_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3

    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_2

    .line 108
    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_1
.end method

.method public static unpackZipFromResource(Ljava/lang/String;I)Z
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "resId"    # I

    .prologue
    .line 89
    const/4 v1, 0x0

    .line 92
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 93
    invoke-static {p0, v1}, Lcom/navdy/client/app/framework/util/PackagedResource;->unpack(Ljava/lang/String;Ljava/io/InputStream;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 98
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    :goto_0
    return v2

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Throwable;
    :try_start_1
    sget-object v2, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error reading resource"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    const/4 v2, 0x0

    .line 98
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method


# virtual methods
.method public updateFromResources(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I
    .param p3, "hashResId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/navdy/client/app/framework/util/PackagedResource;->updateFromResources(Landroid/content/Context;IIZ)V

    .line 41
    return-void
.end method

.method public updateFromResources(Landroid/content/Context;IIZ)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I
    .param p3, "hashResId"    # I
    .param p4, "unzip"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 44
    sget-object v9, Lcom/navdy/client/app/framework/util/PackagedResource;->lockObject:Ljava/lang/Object;

    monitor-enter v9

    .line 45
    const/4 v1, 0x1

    .line 46
    .local v1, "copyFile":Z
    const/4 v4, 0x0

    .line 47
    .local v4, "md5InputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 48
    .local v6, "resources":Landroid/content/res/Resources;
    new-instance v2, Ljava/io/File;

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->basePath:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".md5"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .local v2, "md5File":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->basePath:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->destinationFilename:Ljava/lang/String;

    invoke-direct {v0, v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 52
    .local v0, "configFile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v6, p3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 53
    const-string v8, "UTF-8"

    invoke-static {v4, v8}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "md5InApk":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 55
    const/4 v4, 0x0

    .line 57
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_2

    if-nez p4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-eqz v8, :cond_2

    .line 59
    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->convertFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "md5OnDevice":Ljava/lang/String;
    invoke-static {v5, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 61
    const/4 v1, 0x0

    .line 62
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "no update required signature matches:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    .end local v5    # "md5OnDevice":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_3

    .line 83
    :try_start_3
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 86
    :goto_1
    return-void

    .line 64
    .restart local v5    # "md5OnDevice":Ljava/lang/String;
    :cond_1
    :try_start_4
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "update required, device:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " apk:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 66
    .end local v5    # "md5OnDevice":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 67
    .local v7, "t":Ljava/lang/Throwable;
    :try_start_5
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v8, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 83
    .end local v3    # "md5InApk":Ljava/lang/String;
    .end local v7    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    :try_start_6
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .line 85
    .end local v0    # "configFile":Ljava/io/File;
    .end local v2    # "md5File":Ljava/io/File;
    .end local v6    # "resources":Landroid/content/res/Resources;
    :catchall_1
    move-exception v8

    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v8

    .line 70
    .restart local v0    # "configFile":Ljava/io/File;
    .restart local v2    # "md5File":Ljava/io/File;
    .restart local v3    # "md5InApk":Ljava/lang/String;
    .restart local v6    # "resources":Landroid/content/res/Resources;
    :cond_2
    :try_start_7
    sget-object v8, Lcom/navdy/client/app/framework/util/PackagedResource;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "update required, no file"

    invoke-virtual {v8, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/io/InputStream;)I

    .line 76
    if-eqz p4, :cond_4

    .line 78
    iget-object v8, p0, Lcom/navdy/client/app/framework/util/PackagedResource;->basePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/navdy/client/app/framework/util/PackagedResource;->unpackZip(Ljava/lang/String;Ljava/lang/String;)Z

    .line 79
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 81
    :cond_4
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-static {v8, v10}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;[B)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 83
    :try_start_8
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 85
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1
.end method
