.class public final enum Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;
.super Ljava/lang/Enum;
.source "GmsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/GmsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GmsState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

.field public static final enum MISSING:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

.field public static final enum OUT_OF_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

.field public static final enum UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    const-string v1, "UP_TO_DATE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    .line 29
    new-instance v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    const-string v1, "OUT_OF_DATE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->OUT_OF_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    .line 30
    new-instance v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    const-string v1, "MISSING"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->MISSING:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    sget-object v1, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->OUT_OF_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->MISSING:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->$VALUES:[Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->$VALUES:[Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    return-object v0
.end method
