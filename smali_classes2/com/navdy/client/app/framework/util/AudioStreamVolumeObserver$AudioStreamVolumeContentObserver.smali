.class Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;
.super Landroid/database/ContentObserver;
.source "AudioStreamVolumeObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudioStreamVolumeContentObserver"
.end annotation


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mAudioStreamType:I

.field private mLastVolume:I

.field private final mListener:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/media/AudioManager;ILcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "audioManager"    # Landroid/media/AudioManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "audioStreamType"    # I
    .param p4, "listener"    # Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 44
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioManager:Landroid/media/AudioManager;

    .line 45
    iput p3, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioStreamType:I

    .line 46
    iput-object p4, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mListener:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;

    .line 48
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioStreamType:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mLastVolume:I

    .line 49
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 53
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    .line 54
    invoke-static {}, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Unable to get the audio manager service."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioStreamType:I

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 59
    .local v0, "currentVolume":I
    iget v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mLastVolume:I

    if-eq v0, v1, :cond_0

    .line 60
    iput v0, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mLastVolume:I

    .line 62
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mListener:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mListener:Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;

    iget v2, p0, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$AudioStreamVolumeContentObserver;->mAudioStreamType:I

    invoke-interface {v1, v2, v0}, Lcom/navdy/client/app/framework/util/AudioStreamVolumeObserver$OnAudioStreamVolumeChangedListener;->onAudioStreamVolumeChanged(II)V

    goto :goto_0
.end method
