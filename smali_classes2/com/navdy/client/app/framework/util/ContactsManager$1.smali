.class final Lcom/navdy/client/app/framework/util/ContactsManager$1;
.super Ljava/lang/Object;
.source "ContactsManager.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/ContactsManager;->loadContactPhoto(JZ)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$contentResolver:Landroid/content/ContentResolver;

.field final synthetic val$highRes:Z

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Z)V
    .locals 0

    .prologue
    .line 696
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/ContactsManager$1;->val$contentResolver:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/ContactsManager$1;->val$uri:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/navdy/client/app/framework/util/ContactsManager$1;->val$highRes:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 700
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ContactsManager$1;->val$contentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ContactsManager$1;->val$uri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/util/ContactsManager$1;->val$highRes:Z

    invoke-static {v0, v1, v2}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
