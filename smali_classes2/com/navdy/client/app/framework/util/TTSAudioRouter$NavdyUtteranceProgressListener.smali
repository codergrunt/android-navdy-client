.class Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "TTSAudioRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/TTSAudioRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NavdyUtteranceProgressListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;


# direct methods
.method private constructor <init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter;Lcom/navdy/client/app/framework/util/TTSAudioRouter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .param p2, "x1"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$1;

    .prologue
    .line 515
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;-><init>(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 542
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NavdyTTS_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$500(Lcom/navdy/client/app/framework/util/TTSAudioRouter;ZLjava/lang/String;)V

    .line 545
    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 4
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 549
    if-eqz p1, :cond_0

    const-string v1, "NavdyTTS_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 550
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$300(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 551
    .local v0, "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$602(Lcom/navdy/client/app/framework/util/TTSAudioRouter;Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;)Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 552
    sget-object v2, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onError removed:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_1

    const-string v1, "true"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 554
    .end local v0    # "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$700(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$MessageHandler;->sendEmptyMessage(I)Z

    .line 555
    return-void

    .line 552
    .restart local v0    # "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    :cond_1
    const-string v1, "false"

    goto :goto_0
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 5
    .param p1, "utteranceId"    # Ljava/lang/String;

    .prologue
    .line 519
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "NavdyTTS_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 520
    sget-object v1, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStart:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 521
    const-string v1, "NavdyTTS_SPEECH_DELAY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 522
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$202(Lcom/navdy/client/app/framework/util/TTSAudioRouter;J)J

    .line 524
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$300(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;

    .line 525
    .local v0, "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->sendNotification:Z

    if-eqz v1, :cond_1

    .line 526
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$400(Lcom/navdy/client/app/framework/util/TTSAudioRouter;)Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    move-result-object v1

    new-instance v2, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    iget-object v3, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->originalId:Ljava/lang/String;

    sget-object v4, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;->SPEECH_REQUEST_STARTING:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    invoke-direct {v2, v3, v4}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sendSpeechNotification(Lcom/navdy/service/library/events/audio/SpeechRequestStatus;)V

    .line 530
    .end local v0    # "info":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
    :cond_1
    return-void
.end method

.method public onStop(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "utteranceId"    # Ljava/lang/String;
    .param p2, "interrupted"    # Z

    .prologue
    .line 534
    invoke-super {p0, p1, p2}, Landroid/speech/tts/UtteranceProgressListener;->onStop(Ljava/lang/String;Z)V

    .line 535
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "NavdyTTS_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$NavdyUtteranceProgressListener;->this$0:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->access$500(Lcom/navdy/client/app/framework/util/TTSAudioRouter;ZLjava/lang/String;)V

    .line 538
    :cond_0
    return-void
.end method
