.class public Lcom/navdy/client/app/framework/util/SuggestionManager;
.super Ljava/lang/Object;
.source "SuggestionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;
    }
.end annotation


# static fields
.field private static final MAX_CALENDAR_SUGGESTIONS:I = 0x2

.field private static final MAX_EARLY_FOR_CAL_SUGG:J

.field private static final MAX_RECENT_SUGGESTIONS:I = 0x17

.field private static final MAX_RECOMMENDATIONS:I = 0x1

.field private static final SUGGESTION_DISTANCE_LIMIT:F = 200.0f

.field private static cachedHistory:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static cachedImportantTips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static cachedRecommendations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static cachedUnimportantTips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static history:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static importantTips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static isBuildingSuggestionList:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static lastListGeneration:J

.field private static lastUserLocation:Landroid/location/Location;

.field private static listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private static ota:Lcom/navdy/client/app/framework/models/Suggestion;

.field private static recommendations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field

.field private static trip:Lcom/navdy/client/app/framework/models/Suggestion;

.field private static unimportantTips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x14

    .line 59
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/SuggestionManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 67
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->MAX_EARLY_FOR_CAL_SUGG:J

    .line 69
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->isBuildingSuggestionList:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 71
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastListGeneration:J

    .line 72
    new-instance v0, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastUserLocation:Landroid/location/Location;

    .line 74
    sput-object v5, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 75
    sput-object v5, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->history:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedImportantTips:Ljava/util/ArrayList;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedRecommendations:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedUnimportantTips:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedHistory:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->listeners:Ljava/util/ArrayList;

    .line 91
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->setTrip(Lcom/navdy/client/app/framework/models/Suggestion;)V

    return-void
.end method

.method static synthetic access$100(Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->setRecommendations(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$200(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 58
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->finishSuggestionBuild(Z)V

    return-void
.end method

.method static synthetic access$300(Ljava/util/ArrayList;Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildMachineLearnedRecommendation(Ljava/util/ArrayList;Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    return-void
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addToSuggestions(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700()J
    .locals 2

    .prologue
    .line 58
    sget-wide v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->MAX_EARLY_FOR_CAL_SUGG:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->makeSuggestion(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$902(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Ljava/util/ArrayList;

    .prologue
    .line 58
    sput-object p0, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static declared-synchronized addListener(Ljava/lang/ref/WeakReference;)V
    .locals 2
    .param p0    # Ljava/lang/ref/WeakReference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;>;"
    const-class v1, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit v1

    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static addThisHeader(ILjava/util/ArrayList;)V
    .locals 1
    .param p0, "stringRes"    # I
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1038
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildHeaderFor(I)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    .line 1039
    .local v0, "headerSuggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1040
    return-void
.end method

.method private static addToSuggestions(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z
    .locals 1
    .param p0, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 959
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addToSuggestions(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;Z)Z

    move-result v0

    return v0
.end method

.method private static addToSuggestions(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;Z)Z
    .locals 3
    .param p0, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .param p2, "onlyCheckForDuplicates"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const/4 v0, 0x0

    .line 977
    if-nez p1, :cond_1

    .line 978
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Can\'t add to suggestions list because the list is null!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1002
    :cond_0
    :goto_0
    return v0

    .line 980
    :cond_1
    if-nez p0, :cond_2

    .line 981
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Can\'t add to suggestions list because the suggestion is null!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 983
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-nez v1, :cond_3

    .line 984
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Can\'t add to suggestions list because the destination inside the suggestion is null!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 986
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/models/Destination;->doNotSuggest:Z

    if-eqz v1, :cond_4

    .line 987
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Can\'t add to suggestions list because the destination inside the suggestion is marked as doNotSuggest."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 989
    :cond_4
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->isDuplicate(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 991
    if-nez p2, :cond_6

    .line 992
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_5

    .line 993
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Can\'t add to suggestions list because the list is full."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 995
    :cond_5
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->isTooCloseToCurrentLocation(Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 996
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Can\'t add to suggestions list because the destination is too close."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 1001
    :cond_6
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static addToSuggestionsOnlyCheckForDuplicates(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z
    .locals 1
    .param p0, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 968
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addToSuggestions(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;Z)Z

    move-result v0

    return v0
.end method

.method private static assignDestinationIdsToTripsWithKnownDestinations()V
    .locals 20

    .prologue
    .line 607
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 609
    .local v13, "trips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Trip;>;"
    const/4 v2, 0x0

    .line 611
    .local v2, "c":Landroid/database/Cursor;
    :try_start_0
    new-instance v14, Landroid/util/Pair;

    const-string v15, "destination_id == 0 AND end_lat != 0 AND end_long != 0"

    const/16 v16, 0x0

    invoke-direct/range {v14 .. v16}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v14}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v2

    .line 616
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 617
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-ge v7, v14, :cond_0

    .line 618
    invoke-static {v2, v7}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v12

    .line 619
    .local v12, "trip":Lcom/navdy/client/app/framework/models/Trip;
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 617
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 623
    .end local v7    # "i":I
    .end local v12    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_0
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 626
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/navdy/client/app/framework/models/Trip;

    .line 627
    .restart local v12    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    const/4 v5, 0x0

    .line 633
    .local v5, "destinationCursor":Landroid/database/Cursor;
    :try_start_1
    iget-wide v0, v12, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    move-wide/from16 v16, v0

    iget-wide v0, v12, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    move-wide/from16 v18, v0

    .line 634
    invoke-static/range {v16 .. v19}, Lcom/navdy/client/app/framework/models/Destination;->getCoordinateBoundingBoxSelectionClause(DD)Landroid/util/Pair;

    move-result-object v10

    .line 636
    .local v10, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {v10}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v5

    .line 637
    if-eqz v5, :cond_8

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v15

    if-eqz v15, :cond_8

    .line 638
    const/4 v3, 0x0

    .line 639
    .local v3, "closestDestination":Lcom/navdy/client/app/framework/models/Destination;
    const/high16 v11, -0x40800000    # -1.0f

    .line 640
    .local v11, "shortestDistance":F
    :goto_2
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v15

    if-nez v15, :cond_7

    .line 642
    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v15

    invoke-static {v5, v15}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    .line 644
    .local v4, "d":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v8, Landroid/location/Location;

    const-string v15, ""

    invoke-direct {v8, v15}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 645
    .local v8, "loc1":Landroid/location/Location;
    new-instance v9, Landroid/location/Location;

    const-string v15, ""

    invoke-direct {v9, v15}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 646
    .local v9, "loc2":Landroid/location/Location;
    const/high16 v6, -0x40800000    # -1.0f

    .line 647
    .local v6, "distance":F
    iget-wide v0, v12, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 648
    iget-wide v0, v12, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 649
    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmpl-double v15, v16, v18

    if-nez v15, :cond_1

    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmpl-double v15, v16, v18

    if-eqz v15, :cond_5

    .line 650
    :cond_1
    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 651
    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 652
    invoke-virtual {v8, v9}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v6

    .line 659
    :cond_2
    :goto_3
    const/high16 v15, 0x43480000    # 200.0f

    cmpg-float v15, v6, v15

    if-gez v15, :cond_4

    if-eqz v3, :cond_3

    const/high16 v15, -0x40800000    # -1.0f

    cmpl-float v15, v6, v15

    if-eqz v15, :cond_4

    cmpg-float v15, v6, v11

    if-gez v15, :cond_4

    .line 663
    :cond_3
    move-object v3, v4

    .line 664
    move v11, v6

    .line 667
    :cond_4
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 680
    .end local v3    # "closestDestination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v4    # "d":Lcom/navdy/client/app/framework/models/Destination;
    .end local v6    # "distance":F
    .end local v8    # "loc1":Landroid/location/Location;
    .end local v9    # "loc2":Landroid/location/Location;
    .end local v10    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v11    # "shortestDistance":F
    :catchall_0
    move-exception v14

    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v14

    .line 623
    .end local v5    # "destinationCursor":Landroid/database/Cursor;
    .end local v12    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :catchall_1
    move-exception v14

    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v14

    .line 653
    .restart local v3    # "closestDestination":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v4    # "d":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v5    # "destinationCursor":Landroid/database/Cursor;
    .restart local v6    # "distance":F
    .restart local v8    # "loc1":Landroid/location/Location;
    .restart local v9    # "loc2":Landroid/location/Location;
    .restart local v10    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local v11    # "shortestDistance":F
    .restart local v12    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_5
    :try_start_2
    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmpl-double v15, v16, v18

    if-nez v15, :cond_6

    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmpl-double v15, v16, v18

    if-eqz v15, :cond_2

    .line 654
    :cond_6
    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 655
    iget-wide v0, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 656
    invoke-virtual {v8, v9}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v6

    goto :goto_3

    .line 669
    .end local v4    # "d":Lcom/navdy/client/app/framework/models/Destination;
    .end local v6    # "distance":F
    .end local v8    # "loc1":Landroid/location/Location;
    .end local v9    # "loc2":Landroid/location/Location;
    :cond_7
    if-eqz v3, :cond_8

    .line 671
    iget v15, v3, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {v12, v15}, Lcom/navdy/client/app/framework/models/Trip;->setDestinationIdAndSaveToDb(I)I

    .line 673
    iget-wide v0, v3, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    move-wide/from16 v16, v0

    iget-wide v0, v12, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    move-wide/from16 v18, v0

    cmp-long v15, v16, v18

    if-lez v15, :cond_8

    .line 674
    iget-wide v0, v12, Lcom/navdy/client/app/framework/models/Trip;->startTime:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v3, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 675
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->updateLastRoutedDateInDb()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 680
    .end local v3    # "closestDestination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v11    # "shortestDistance":F
    :cond_8
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 684
    .end local v5    # "destinationCursor":Landroid/database/Cursor;
    .end local v10    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v12    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_9
    return-void
.end method

.method private static buildCalendarAndMachineLearnedSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 8
    .param p0, "callback"    # Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    .prologue
    .line 704
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 705
    .local v2, "now":J
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartLocation()Landroid/location/Location;

    move-result-object v0

    .line 706
    .local v0, "currentPhoneLocation":Landroid/location/Location;
    sget-wide v4, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastListGeneration:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0x493e0

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    .line 707
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->isCloseToLastRecommendation(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 708
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Using cached Calendar events and Machine learned recommendation"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 709
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;

    invoke-interface {p0, v1}, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V

    .line 729
    :goto_0
    return-void

    .line 712
    :cond_0
    sput-wide v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastListGeneration:J

    .line 713
    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastUserLocation:Landroid/location/Location;

    .line 714
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Building new Calendar events and Machine learned recommendation"

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 717
    new-instance v1, Lcom/navdy/client/app/framework/util/SuggestionManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/util/SuggestionManager$4;-><init>(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildCalendarSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    goto :goto_0
.end method

.method public static buildCalendarSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 6
    .param p0, "callback"    # Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    .prologue
    const/4 v5, 0x1

    .line 743
    new-instance v0, Ljava/util/ArrayList;

    const/16 v3, 0x14

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 744
    .local v0, "calendarSuggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 745
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v3, "calendars_enabled"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 748
    .local v1, "calendarSuggestionsAreEnabled":Z
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveCalendarPermission()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 749
    sget-object v3, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "============ Calendar Events ============"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 750
    new-instance v3, Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/app/framework/util/SuggestionManager$5;-><init>(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;Ljava/util/ArrayList;)V

    invoke-static {v3, v5}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->readCalendarEvent(Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;Z)V

    .line 863
    :goto_0
    return-void

    .line 861
    :cond_0
    invoke-interface {p0, v0}, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private static buildHeaderFor(I)Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 5
    .param p0, "stringRes"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1043
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1044
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1045
    .local v2, "resString":Ljava/lang/String;
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    .local v1, "headerDestination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->SECTION_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    return-object v3
.end method

.method private static buildHistorySuggestions()V
    .locals 6

    .prologue
    .line 578
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->assignDestinationIdsToTripsWithKnownDestinations()V

    .line 581
    const/4 v0, 0x0

    .line 583
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    new-instance v3, Landroid/util/Pair;

    const-string v4, "do_not_suggest != 1"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v3}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getRecentsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    .line 586
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 589
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-static {v0, v3}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    .line 590
    .local v1, "recent":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v3, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-static {v1, v3}, Lcom/navdy/client/app/framework/util/SuggestionManager;->makeSuggestion(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v2

    .line 591
    .local v2, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    sget-object v3, Lcom/navdy/client/app/framework/util/SuggestionManager;->history:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    sget-object v3, Lcom/navdy/client/app/framework/util/SuggestionManager;->history:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x17

    if-ge v3, v4, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 595
    .end local v1    # "recent":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 597
    return-void

    .line 595
    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3
.end method

.method private static buildImportantTipsSuggestions(Landroid/content/SharedPreferences;)V
    .locals 6
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v5, 0x0

    .line 410
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 413
    .local v0, "context":Landroid/content/Context;
    if-eqz p0, :cond_0

    const-string v2, "user_watched_the_demo"

    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 416
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 417
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f08027a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 418
    const v2, 0x7f0804f3

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 419
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->DEMO_VIDEO:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_0
    if-eqz p0, :cond_1

    const-string v2, "hud_gesture"

    const/4 v3, 0x1

    .line 424
    invoke-interface {p0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "user_tried_gestures_once_before"

    .line 427
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 430
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 431
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f08027d

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 432
    const v2, 0x7f08027f

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 433
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->TRY_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    if-eqz p0, :cond_2

    const-string v2, "user_already_saw_microphone_tip"

    .line 438
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "hud_voice_search_capable"

    .line 441
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 444
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveMicrophonePermission()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 445
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 446
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f0804ed

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 447
    const v2, 0x7f0804ee

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 448
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->VOICE_SEARCH:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    :goto_0
    if-eqz p0, :cond_3

    const-string v2, "user_already_saw_google_now_tip"

    .line 459
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    .line 462
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 463
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f0801eb

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 464
    const v2, 0x7f0801ec

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 465
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_NOW:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_3
    if-eqz p0, :cond_4

    const-string v2, "user_already_saw_music_tip"

    .line 470
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "hud_music_capable"

    .line 473
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 476
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 477
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f080248

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 478
    const v2, 0x7f080249

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 479
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HUD_LOCAL_MUSIC_BROWSER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_4
    if-eqz p0, :cond_5

    const-string v2, "glances"

    .line 484
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "user_enabled_glances_once_before"

    .line 487
    invoke-interface {p0, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_5

    .line 490
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 491
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f080155

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 492
    const v2, 0x7f080156

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 493
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GLANCES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_5
    return-void

    .line 450
    :cond_6
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 451
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v2, 0x7f080159

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 452
    const v2, 0x7f08015a

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 453
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    new-instance v3, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_MICROPHONE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v3, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private static buildMachineLearnedRecommendation(Ljava/util/ArrayList;Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 3
    .param p1, "callback"    # Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;",
            "Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 875
    .local p0, "calendarEventsAndRecommendations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 877
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/framework/util/SuggestionManager$6;

    invoke-direct {v2, p0, p1}, Lcom/navdy/client/app/framework/util/SuggestionManager$6;-><init>(Ljava/util/ArrayList;Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 899
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Calling the Machine learning algorithm for a destination recommendation."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 900
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->suggestDestination(Landroid/content/Context;Z)V

    .line 901
    return-void
.end method

.method public static declared-synchronized buildSuggestionList(Z)V
    .locals 4
    .param p0, "sendToHud"    # Z
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 332
    const-class v2, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 334
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Building suggestion list."

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 335
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->isBuildingSuggestionList:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Already in the process of building the suggestion list. Ignoring request."

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    :goto_0
    monitor-exit v2

    return-void

    .line 340
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->isBuildingSuggestionList:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 342
    const/4 v1, 0x0

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 344
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    .line 346
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    .line 347
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0x14

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->history:Ljava/util/ArrayList;

    .line 349
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedImportantTips:Ljava/util/ArrayList;

    .line 350
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedRecommendations:Ljava/util/ArrayList;

    .line 351
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedUnimportantTips:Ljava/util/ArrayList;

    .line 352
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0x14

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedHistory:Ljava/util/ArrayList;

    .line 354
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 356
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getTripSuggestion()Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v1

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 357
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildImportantTipsSuggestions(Landroid/content/SharedPreferences;)V

    .line 358
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildUnimportantTipsSuggestions(Landroid/content/SharedPreferences;)V

    .line 359
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildHistorySuggestions()V

    .line 361
    new-instance v1, Lcom/navdy/client/app/framework/util/SuggestionManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/util/SuggestionManager$3;-><init>(Z)V

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildCalendarAndMachineLearnedSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 332
    .end local v0    # "sharedPreferences":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static buildUnimportantTipsSuggestions(Landroid/content/SharedPreferences;)V
    .locals 11
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 508
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 510
    .local v1, "context":Landroid/content/Context;
    if-eqz p0, :cond_2

    .line 512
    const-string v6, "hud_gesture"

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "user_already_saw_gesture_tip"

    .line 515
    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_0

    .line 518
    new-instance v2, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 519
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v6, 0x7f080153

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 520
    const v6, 0x7f080154

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 521
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    new-instance v7, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v8, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v7, v2, v8}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getHome()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v4

    .line 526
    .local v4, "home":Lcom/navdy/client/app/framework/models/Destination;
    if-nez v4, :cond_1

    const-string v6, "user_already_saw_add_home_tip"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_1

    .line 529
    new-instance v2, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 530
    .restart local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v6, 0x7f0800ad

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 531
    const v6, 0x7f08049e

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 532
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    new-instance v7, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v8, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_HOME:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v7, v2, v8}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getWork()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v5

    .line 536
    .local v5, "work":Lcom/navdy/client/app/framework/models/Destination;
    if-nez v5, :cond_2

    const-string v6, "user_already_saw_add_work_tip"

    invoke-interface {p0, v6, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-nez v6, :cond_2

    .line 539
    new-instance v2, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 540
    .restart local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    const v6, 0x7f0800b1

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 541
    const v6, 0x7f08049f

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 542
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    new-instance v7, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v8, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_WORK:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v7, v2, v8}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v4    # "home":Lcom/navdy/client/app/framework/models/Destination;
    .end local v5    # "work":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    const/4 v3, 0x0

    .line 548
    .local v3, "favoritesCursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v6, Landroid/util/Pair;

    const-string v7, "is_special != ? AND is_special != ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, -0x3

    .line 554
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, -0x2

    .line 555
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 548
    invoke-static {v6}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v3

    .line 558
    if-eqz v3, :cond_3

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-gtz v6, :cond_3

    .line 559
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getAddFavoriteSuggestion()Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    .line 560
    .local v0, "addFavoriteSuggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 563
    .end local v0    # "addFavoriteSuggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_3
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 565
    return-void

    .line 563
    :catchall_0
    move-exception v6

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v6
.end method

.method private static declared-synchronized finishSuggestionBuild(Z)V
    .locals 4
    .param p0, "sendToHud"    # Z

    .prologue
    .line 917
    const-class v2, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedImportantTips:Ljava/util/ArrayList;

    .line 918
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedRecommendations:Ljava/util/ArrayList;

    .line 919
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedUnimportantTips:Ljava/util/ArrayList;

    .line 920
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->history:Ljava/util/ArrayList;

    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedHistory:Ljava/util/ArrayList;

    .line 922
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getSuggestions()Ljava/util/ArrayList;

    move-result-object v0

    .line 924
    .local v0, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    if-eqz p0, :cond_0

    .line 925
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersionAndSendTheseSuggestionsToDisplayAsync(Ljava/util/List;)V

    .line 928
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->suggestionListIsEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 932
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getAddFavoriteSuggestion()Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 933
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getSuggestions()Ljava/util/ArrayList;

    move-result-object v0

    .line 936
    :cond_1
    sget-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->isBuildingSuggestionList:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 938
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->notifyListenersIfAny(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 939
    monitor-exit v2

    return-void

    .line 917
    .end local v0    # "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized forceSuggestionFullRefresh()V
    .locals 4

    .prologue
    .line 169
    const-class v1, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v1

    const-wide/16 v2, 0x0

    :try_start_0
    sput-wide v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastListGeneration:J

    .line 170
    new-instance v0, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastUserLocation:Landroid/location/Location;

    .line 171
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->forceCalendarRefresh()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    monitor-exit v1

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getAddFavoriteSuggestion()Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 4

    .prologue
    .line 306
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 307
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 308
    .local v0, "context":Landroid/content/Context;
    const v2, 0x7f0800ac

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 309
    const v2, 0x7f080499

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 310
    new-instance v2, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v3, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_FAVORITE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v2, v1, v3}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    return-object v2
.end method

.method public static getEmptySuggestionState()Ljava/util/ArrayList;
    .locals 5
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getTripSuggestion()Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v2

    .line 295
    .local v2, "trip":Lcom/navdy/client/app/framework/models/Suggestion;
    if-eqz v2, :cond_0

    .line 296
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    :goto_0
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion;

    const/4 v3, 0x0

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v0, v3, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 301
    .local v0, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    return-object v1

    .line 298
    .end local v0    # "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_0
    const v3, 0x7f08056c

    invoke-static {v3, v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addThisHeader(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public static declared-synchronized getSuggestions()Ljava/util/ArrayList;
    .locals 9
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    const-class v7, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v7

    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0x17

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 232
    .local v5, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    if-eqz v6, :cond_0

    .line 233
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_0
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedImportantTips:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 237
    const v6, 0x7f0804ad

    invoke-static {v6, v5}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addThisHeader(ILjava/util/ArrayList;)V

    .line 238
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedImportantTips:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_1
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    if-nez v6, :cond_4

    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedRecommendations:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 242
    const v6, 0x7f080490

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildHeaderFor(I)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v1

    .line 243
    .local v1, "recommendationHeader":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    const/4 v4, 0x0

    .line 245
    .local v4, "successCount":I
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedRecommendations:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 246
    .local v2, "s":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-static {v2, v5}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addToSuggestions(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z

    move-result v3

    .line 247
    .local v3, "success":Z
    if-eqz v3, :cond_2

    .line 248
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 251
    .end local v2    # "s":Lcom/navdy/client/app/framework/models/Suggestion;
    .end local v3    # "success":Z
    :cond_3
    if-gtz v4, :cond_4

    .line 252
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 256
    .end local v1    # "recommendationHeader":Lcom/navdy/client/app/framework/models/Suggestion;
    .end local v4    # "successCount":I
    :cond_4
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedImportantTips:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_5

    .line 257
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedUnimportantTips:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_a

    .line 258
    const v6, 0x7f0804ad

    invoke-static {v6, v5}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addThisHeader(ILjava/util/ArrayList;)V

    .line 259
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedUnimportantTips:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    :cond_5
    :goto_1
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedHistory:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_8

    .line 266
    const v6, 0x7f0801f8

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildHeaderFor(I)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v0

    .line 267
    .local v0, "historyHeader":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    const/4 v4, 0x0

    .line 269
    .restart local v4    # "successCount":I
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->cachedHistory:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 270
    .restart local v2    # "s":Lcom/navdy/client/app/framework/models/Suggestion;
    const/16 v8, 0x14

    if-lt v4, v8, :cond_b

    .line 279
    .end local v2    # "s":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_7
    if-gtz v4, :cond_8

    .line 280
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 284
    .end local v0    # "historyHeader":Lcom/navdy/client/app/framework/models/Suggestion;
    .end local v4    # "successCount":I
    :cond_8
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_9

    .line 285
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getEmptySuggestionState()Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 287
    .end local v5    # "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    :cond_9
    monitor-exit v7

    return-object v5

    .line 260
    .restart local v5    # "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    :cond_a
    :try_start_1
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;

    if-eqz v6, :cond_5

    .line 261
    sget-object v6, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 230
    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    .line 274
    .restart local v0    # "historyHeader":Lcom/navdy/client/app/framework/models/Suggestion;
    .restart local v2    # "s":Lcom/navdy/client/app/framework/models/Suggestion;
    .restart local v4    # "successCount":I
    :cond_b
    :try_start_2
    invoke-static {v2, v5}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addToSuggestionsOnlyCheckForDuplicates(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 275
    .restart local v3    # "success":Z
    if-eqz v3, :cond_6

    .line 276
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public static getTrip()Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    return-object v0
.end method

.method private static getTripSuggestion()Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 4

    .prologue
    .line 386
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    .line 388
    .local v0, "navdyRouteHandler":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    const/4 v1, 0x0

    .line 390
    .local v1, "trip":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->isInOneOfTheActiveTripStates()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 391
    new-instance v1, Lcom/navdy/client/app/framework/models/Suggestion;

    .end local v1    # "trip":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCurrentDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 397
    .restart local v1    # "trip":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_0
    :goto_0
    return-object v1

    .line 393
    :cond_1
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->isInOneOfThePendingTripStates()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 394
    new-instance v1, Lcom/navdy/client/app/framework/models/Suggestion;

    .end local v1    # "trip":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCurrentDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .restart local v1    # "trip":Lcom/navdy/client/app/framework/models/Suggestion;
    goto :goto_0
.end method

.method private static declared-synchronized insertOtaTip()V
    .locals 5

    .prologue
    .line 179
    const-class v3, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    .line 190
    .local v0, "context":Landroid/content/Context;
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    :goto_0
    monitor-exit v3

    return-void

    .line 185
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_0
    :try_start_1
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 186
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 187
    .restart local v0    # "context":Landroid/content/Context;
    const v2, 0x7f080494

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 188
    const v2, 0x7f08049b

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 189
    new-instance v2, Lcom/navdy/client/app/framework/models/Suggestion;

    sget-object v4, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->OTA:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v2, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    sput-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    .end local v0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static isCloseToLastRecommendation(Landroid/location/Location;)Z
    .locals 4
    .param p0, "currentPhoneLocation"    # Landroid/location/Location;

    .prologue
    const/4 v2, 0x0

    .line 734
    if-eqz p0, :cond_0

    sget-object v3, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastUserLocation:Landroid/location/Location;

    if-nez v3, :cond_1

    .line 739
    :cond_0
    :goto_0
    return v2

    .line 737
    :cond_1
    const/high16 v1, 0x42c80000    # 100.0f

    .line 738
    .local v1, "significantDistance":F
    sget-object v3, Lcom/navdy/client/app/framework/util/SuggestionManager;->lastUserLocation:Landroid/location/Location;

    invoke-static {p0, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Landroid/location/Location;Landroid/location/Location;)F

    move-result v0

    .line 739
    .local v0, "distance":F
    cmpg-float v3, v0, v1

    if-gez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static isDuplicate(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z
    .locals 4
    .param p0, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1054
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 1055
    .local v0, "s":Lcom/navdy/client/app/framework/models/Suggestion;
    iget-object v2, v0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/models/Destination;->equals(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1056
    const/4 v1, 0x1

    .line 1059
    .end local v0    # "s":Lcom/navdy/client/app/framework/models/Suggestion;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isTooCloseToCurrentLocation(Lcom/navdy/client/app/framework/models/Destination;)Z
    .locals 10
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 1012
    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v6, v7, v8, v9}, Lcom/navdy/client/app/framework/map/MapUtils;->buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 1013
    .local v0, "destinationCoord":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static {v0}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1014
    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v6, v7, v8, v9}, Lcom/navdy/client/app/framework/map/MapUtils;->buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 1016
    :cond_0
    invoke-static {v0}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1017
    sget-object v5, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "The destination has no coordinates so assuming we are too close"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1029
    :cond_1
    :goto_0
    return v4

    .line 1021
    :cond_2
    sget-object v5, Lcom/navdy/client/app/framework/util/SuggestionManager;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    .line 1022
    .local v1, "smartStart":Lcom/navdy/service/library/events/location/Coordinate;
    if-nez v1, :cond_3

    .line 1023
    sget-object v5, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "No smart start coordinates so assuming we are too close"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 1027
    :cond_3
    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v2

    .line 1028
    .local v2, "distanceBetween":D
    sget-object v5, Lcom/navdy/client/app/framework/util/SuggestionManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "There is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "m. between the user and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->toShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1029
    const-wide/high16 v6, 0x4069000000000000L    # 200.0

    cmpg-double v5, v2, v6

    if-ltz v5, :cond_1

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static makeSuggestion(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 1
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "suggestionType"    # Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1070
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    return-object v0
.end method

.method private static declared-synchronized notifyListenersIfAny(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const-class v3, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->listeners:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 120
    :cond_0
    monitor-exit v3

    return-void

    .line 114
    :cond_1
    :try_start_1
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 115
    .local v0, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;>;"
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    .line 116
    .local v1, "suggestionBuildListener":Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;
    if-eqz v1, :cond_2

    .line 117
    invoke-interface {v1, p0}, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 110
    .end local v0    # "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;>;"
    .end local v1    # "suggestionBuildListener":Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static onCalendarChanged(Z)V
    .locals 1
    .param p0, "sendToHud"    # Z

    .prologue
    .line 1075
    new-instance v0, Lcom/navdy/client/app/framework/util/SuggestionManager$7;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/util/SuggestionManager$7;-><init>(Z)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildCalendarSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    .line 1089
    return-void
.end method

.method public static rebuildSuggestionListAndSendToHudAsync()V
    .locals 2

    .prologue
    .line 314
    new-instance v0, Lcom/navdy/client/app/framework/util/SuggestionManager$2;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager$2;-><init>()V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 321
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/SuggestionManager$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 322
    return-void
.end method

.method public static declared-synchronized removeListener(Ljava/lang/ref/WeakReference;)V
    .locals 2
    .param p0    # Ljava/lang/ref/WeakReference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;>;"
    const-class v1, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit v1

    return-void

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized removeOtaTip()V
    .locals 2

    .prologue
    .line 196
    const-class v0, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    monitor-exit v0

    return-void

    .line 196
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized setRecommendations(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p0, "recommendations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const-class v0, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    monitor-exit v0

    return-void

    .line 159
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized setTrip(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 2
    .param p0, "newTrip"    # Lcom/navdy/client/app/framework/models/Suggestion;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 146
    const-class v1, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    if-eqz v0, :cond_0

    if-eqz p0, :cond_2

    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    if-nez v0, :cond_1

    if-nez p0, :cond_2

    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 148
    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/models/Suggestion;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 149
    :cond_2
    sput-object p0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 150
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getSuggestions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->notifyListenersIfAny(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :cond_3
    monitor-exit v1

    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized setTripAsync(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 4
    .param p0, "newTrip"    # Lcom/navdy/client/app/framework/models/Suggestion;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 131
    const-class v1, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v2, Lcom/navdy/client/app/framework/util/SuggestionManager$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/util/SuggestionManager$1;-><init>(Lcom/navdy/client/app/framework/models/Suggestion;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    monitor-exit v1

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static suggestionListIsEmpty()Z
    .locals 1

    .prologue
    .line 942
    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->trip:Lcom/navdy/client/app/framework/models/Suggestion;

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->importantTips:Ljava/util/ArrayList;

    .line 944
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->recommendations:Ljava/util/ArrayList;

    .line 945
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->unimportantTips:Ljava/util/ArrayList;

    .line 946
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/util/SuggestionManager;->history:Ljava/util/ArrayList;

    .line 947
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized updateOtaStatus(Lcom/navdy/client/ota/OTAUpdateService$State;)V
    .locals 4
    .param p0, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 204
    const-class v3, Lcom/navdy/client/app/framework/util/SuggestionManager;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 206
    const/4 v1, 0x0

    .line 207
    .local v1, "somethingChanged":Z
    sget-object v2, Lcom/navdy/client/app/framework/util/SuggestionManager;->ota:Lcom/navdy/client/app/framework/models/Suggestion;

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 208
    .local v0, "otaWasHere":Z
    :goto_0
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-ne p0, v2, :cond_3

    .line 209
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->insertOtaTip()V

    .line 210
    const/4 v1, 0x1

    .line 216
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 217
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->getSuggestions()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/SuggestionManager;->notifyListenersIfAny(Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 219
    :cond_1
    monitor-exit v3

    return-void

    .line 207
    .end local v0    # "otaWasHere":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 211
    .restart local v0    # "otaWasHere":Z
    :cond_3
    if-eqz v0, :cond_0

    .line 212
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->removeOtaTip()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    const/4 v1, 0x1

    goto :goto_1

    .line 204
    .end local v0    # "otaWasHere":Z
    .end local v1    # "somethingChanged":Z
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method
