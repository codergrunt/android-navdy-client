.class public Lcom/navdy/client/app/framework/util/TimeStampUtils;
.super Ljava/lang/Object;
.source "TimeStampUtils.java"


# static fields
.field private static final MILE_PER_METER:D = 6.21371E-4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getArrivalTimeString(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 10
    .param p0, "eta"    # Ljava/lang/Integer;

    .prologue
    .line 84
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-gez v7, :cond_1

    .line 85
    :cond_0
    const-string v7, ""

    .line 96
    :goto_0
    return-object v7

    .line 88
    :cond_1
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 90
    .local v4, "millis":J
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 91
    .local v3, "now":Ljava/util/Date;
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 92
    .local v1, "arrivalTimeLong":Ljava/lang/Long;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 94
    .local v0, "arrivalDate":Ljava/util/Date;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v2, "HH:mm"

    .line 95
    .local v2, "formatString":Ljava/lang/String;
    :goto_1
    new-instance v6, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v6, v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 96
    .local v6, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 94
    .end local v2    # "formatString":Ljava/lang/String;
    .end local v6    # "simpleDateFormat":Ljava/text/SimpleDateFormat;
    :cond_2
    const-string v2, "h:mm aa"

    goto :goto_1
.end method

.method public static getDistanceStringInMiles(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 8
    .param p0, "distance"    # Ljava/lang/Integer;

    .prologue
    .line 65
    const-string v1, ""

    .line 67
    .local v1, "estimatedDistance":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-gez v4, :cond_1

    :cond_0
    move-object v2, v1

    .line 75
    .end local v1    # "estimatedDistance":Ljava/lang/String;
    .local v2, "estimatedDistance":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 71
    .end local v2    # "estimatedDistance":Ljava/lang/String;
    .restart local v1    # "estimatedDistance":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3f445c700fd4d6a9L    # 6.21371E-4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 72
    .local v3, "milesDouble":Ljava/lang/Double;
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v4, "#.#"

    invoke-direct {v0, v4}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "decimalFormat":Ljava/text/DecimalFormat;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mi"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 75
    .end local v1    # "estimatedDistance":Ljava/lang/String;
    .restart local v2    # "estimatedDistance":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getDurationStringInHoursAndMinutes(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 13
    .param p0, "eta"    # Ljava/lang/Integer;

    .prologue
    .line 28
    const-string v1, ""

    .line 30
    .local v1, "estimatedETA":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-gez v5, :cond_1

    :cond_0
    move-object v4, v1

    .line 56
    .end local v1    # "estimatedETA":Ljava/lang/String;
    .local v4, "estimatedETA":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 34
    .end local v4    # "estimatedETA":Ljava/lang/String;
    .restart local v1    # "estimatedETA":Ljava/lang/String;
    :cond_1
    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v5, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    .line 35
    .local v8, "minutes":J
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v8, v9}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v6

    .line 36
    .local v6, "hours":J
    sget-object v5, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v2

    .line 38
    .local v2, "days":J
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 39
    .local v0, "context":Landroid/content/Context;
    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-lez v5, :cond_3

    .line 40
    sget-object v5, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v10

    sub-long/2addr v6, v10

    .line 41
    const-wide/16 v10, 0x0

    cmp-long v5, v6, v10

    if-lez v5, :cond_2

    .line 42
    const v5, 0x7f08011e

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v5, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v4, v1

    .line 56
    .end local v1    # "estimatedETA":Ljava/lang/String;
    .restart local v4    # "estimatedETA":Ljava/lang/String;
    goto :goto_0

    .line 44
    .end local v4    # "estimatedETA":Ljava/lang/String;
    .restart local v1    # "estimatedETA":Ljava/lang/String;
    :cond_2
    const v5, 0x7f08011f

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v5, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 46
    :cond_3
    const-wide/16 v10, 0x0

    cmp-long v5, v6, v10

    if-lez v5, :cond_5

    .line 47
    sget-object v5, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 48
    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-lez v5, :cond_4

    .line 49
    const v5, 0x7f080242

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v5, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 51
    :cond_4
    const v5, 0x7f080243

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v5, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 54
    :cond_5
    const v5, 0x7f0802ca

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v5, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
