.class public Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;
.super Ljava/lang/Object;
.source "DeviceConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/DeviceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceInfoEvent"
.end annotation


# instance fields
.field public deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 0
    .param p1, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput-object p1, p0, Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    .line 168
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceInfoEvent: info=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;->deviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
