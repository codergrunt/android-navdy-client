.class public Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;
.super Lcom/navdy/service/library/util/Listenable;
.source "ClientConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/service/ClientConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceChangeBroadcaster"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;,
        Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$EventDispatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/service/library/util/Listenable",
        "<",
        "Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/service/library/util/Listenable;-><init>()V

    return-void
.end method


# virtual methods
.method dispatchDeviceChangedEvent(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "newDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 56
    new-instance v0, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;-><init>(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 62
    return-void
.end method
