.class public Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;
.super Landroid/app/job/JobService;
.source "UnmeteredNetworkConnectivityJobService.java"


# static fields
.field public static final UNMETERED_NETWORK_SERVICE:I = 0x65

.field public static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method public static scheduleNetworkServiceForNougatOrAbove()V
    .locals 7

    .prologue
    const/16 v6, 0x65

    .line 64
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v3, v4, :cond_0

    .line 65
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 66
    .local v0, "context":Landroid/content/Context;
    const-class v3, Landroid/app/job/JobScheduler;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/job/JobScheduler;

    .line 67
    .local v2, "jobScheduler":Landroid/app/job/JobScheduler;
    if-eqz v2, :cond_1

    invoke-virtual {v2, v6}, Landroid/app/job/JobScheduler;->getPendingJob(I)Landroid/app/job/JobInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 68
    sget-object v3, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "NetworkConnectivityJobService already pending"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    sget-object v3, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Schedule network connectivity job service"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 72
    new-instance v3, Landroid/app/job/JobInfo$Builder;

    new-instance v4, Landroid/content/ComponentName;

    const-class v5, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;

    invoke-direct {v4, v0, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v3, v6, v4}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    const/4 v4, 0x2

    .line 75
    invoke-virtual {v3, v4}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v3

    .line 76
    invoke-virtual {v3}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v1

    .line 77
    .local v1, "job":Landroid/app/job/JobInfo;
    if-eqz v2, :cond_0

    .line 78
    invoke-virtual {v2, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I

    goto :goto_0
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 3
    .param p1, "jobParameters"    # Landroid/app/job/JobParameters;

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onStartJob"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 33
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService$1;-><init>(Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;)V

    const/4 v2, 0x1

    .line 34
    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 5
    .param p1, "jobParameters"    # Landroid/app/job/JobParameters;

    .prologue
    .line 48
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService$2;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService$2;-><init>(Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;)V

    const/4 v4, 0x1

    .line 49
    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 57
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 58
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "pending_sensor_data_exist"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 59
    .local v0, "pendingSensorDataExist":Z
    sget-object v2, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStopJob - JobService will reschedule: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 60
    return v0
.end method
