.class public Lcom/navdy/client/app/framework/i18n/AddressUtils;
.super Ljava/lang/Object;
.source "AddressUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    }
.end annotation


# static fields
.field public static final ADDRESS_SEPARATOR:Ljava/lang/String; = ","

.field private static final CITY:Ljava/lang/String; = "city"

.field private static final COUNTRIES:Ljava/lang/String; = "countries"

.field private static final COUNTRY:Ljava/lang/String; = "country"

.field public static final DEFAULT_COUNTRY_CODE:Ljava/lang/String; = "US"

.field private static final FORMAT:Ljava/lang/String; = "format"

.field private static final FORMATS:Ljava/lang/String; = "formats"

.field private static final ISO_3166_1_ALPHA_3:Ljava/lang/String; = "ISO 3166-1-a3"

.field private static final ISO_3166_2:Ljava/lang/String; = "ISO_3166-2"

.field private static final NUMBER:Ljava/lang/String; = "number"

.field private static final STATE:Ljava/lang/String; = "state"

.field private static final STREET:Ljava/lang/String; = "street"

.field private static final ZIP:Ljava/lang/String; = "zip"

.field private static final formats:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;",
            ">;"
        }
    .end annotation
.end field

.field private static final iso3ToIso2:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final isoToFormatString:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/i18n/AddressUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/AddressUtils;->logger:Lcom/navdy/service/library/log/Logger;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/AddressUtils;->formats:Ljava/util/HashMap;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/AddressUtils;->iso3ToIso2:Ljava/util/Map;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isoToFormatString:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanUpDebugPrefix(Landroid/util/Pair;)Landroid/util/Pair;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    .local p0, "addressPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    return-object p0
.end method

.method private static cleanUpDebugPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "line"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 289
    return-object p0
.end method

.method public static convertIso2ToIso3(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "iso2"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 320
    sget-object v3, Lcom/navdy/client/app/framework/i18n/AddressUtils;->iso3ToIso2:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 322
    .local v2, "iso3s":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 323
    .local v1, "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 324
    .local v0, "currentIso2":Ljava/lang/String;
    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 325
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 329
    .end local v0    # "currentIso2":Ljava/lang/String;
    .end local v1    # "entries":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-object v3

    :cond_1
    move-object v3, p0

    goto :goto_0
.end method

.method public static convertIso3ToIso2(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "iso3"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 311
    sget-object v0, Lcom/navdy/client/app/framework/i18n/AddressUtils;->iso3ToIso2:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getAddressFormatFor(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    .locals 3
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "format":Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    invoke-static {p0}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->getFormatStringFor(Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "formatString":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 223
    sget-object v2, Lcom/navdy/client/app/framework/i18n/AddressUtils;->formats:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;

    invoke-static {v2}, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->getClone(Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;)Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    .line 229
    :cond_0
    return-object v0
.end method

.method private static getFormatStringFor(Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;
    .locals 3
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 211
    .local v0, "formatString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasDetailedAddress()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    sget-object v1, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isoToFormatString:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "formatString":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 214
    .restart local v0    # "formatString":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method static initAddressFormats()V
    .locals 9

    .prologue
    .line 75
    const/4 v4, 0x0

    .line 77
    .local v4, "reader":Landroid/util/JsonReader;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 78
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f070000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 79
    .local v2, "is":Ljava/io/InputStream;
    new-instance v5, Landroid/util/JsonReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    .end local v4    # "reader":Landroid/util/JsonReader;
    .local v5, "reader":Landroid/util/JsonReader;
    :try_start_1
    invoke-virtual {v5}, Landroid/util/JsonReader;->beginObject()V

    .line 82
    :goto_0
    invoke-virtual {v5}, Landroid/util/JsonReader;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 83
    invoke-virtual {v5}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "name":Ljava/lang/String;
    const/4 v6, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v6, :pswitch_data_0

    .line 92
    invoke-virtual {v5}, Landroid/util/JsonReader;->skipValue()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 97
    .end local v3    # "name":Ljava/lang/String;
    :catch_0
    move-exception v1

    move-object v4, v5

    .line 98
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v5    # "reader":Landroid/util/JsonReader;
    .local v1, "e":Ljava/lang/Exception;
    .restart local v4    # "reader":Landroid/util/JsonReader;
    :goto_2
    :try_start_2
    sget-object v6, Lcom/navdy/client/app/framework/i18n/AddressUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception found: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 99
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 101
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v6

    .line 84
    .end local v4    # "reader":Landroid/util/JsonReader;
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v5    # "reader":Landroid/util/JsonReader;
    :sswitch_0
    :try_start_3
    const-string v7, "countries"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v6, 0x0

    goto :goto_1

    :sswitch_1
    const-string v7, "formats"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    .line 86
    :pswitch_0
    invoke-static {v5}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->readCountries(Landroid/util/JsonReader;)V

    goto :goto_0

    .line 101
    .end local v3    # "name":Ljava/lang/String;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "reader":Landroid/util/JsonReader;
    .restart local v4    # "reader":Landroid/util/JsonReader;
    goto :goto_3

    .line 89
    .end local v4    # "reader":Landroid/util/JsonReader;
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v5    # "reader":Landroid/util/JsonReader;
    :pswitch_1
    invoke-static {v5}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->readFormats(Landroid/util/JsonReader;)V

    goto :goto_0

    .line 96
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Landroid/util/JsonReader;->endObject()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 101
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 103
    return-void

    .line 97
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v5    # "reader":Landroid/util/JsonReader;
    .restart local v4    # "reader":Landroid/util/JsonReader;
    :catch_1
    move-exception v1

    goto :goto_2

    .line 84
    :sswitch_data_0
    .sparse-switch
        -0x2860f8a4 -> :sswitch_1
        0x509f9ab4 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static insertAddressPartsInFormat(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;
    .locals 2
    .param p0, "line"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "d"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 235
    const-string v0, "number"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getStreetNumber()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceOrErase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 236
    const-string v0, "street"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getStreetName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceOrErase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 237
    const-string v0, "zip"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getZipCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceOrErase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 238
    const-string v0, "city"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceOrErase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 239
    const-string v0, "state"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceOrErase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 240
    const-string v0, "country"

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceOrErase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 241
    return-object p0
.end method

.method public static isTitleIsInTheAddress(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "destinationTitle"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "address"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 294
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 296
    :cond_0
    const/4 v0, 0x0

    .line 306
    :goto_0
    return v0

    .line 304
    :cond_1
    invoke-virtual {p1, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 306
    .local v0, "titleIsInFirstLine":Z
    goto :goto_0
.end method

.method public static joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;
    .locals 4
    .param p1, "startAt"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 247
    .local p0, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, p1, :cond_0

    .line 248
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p0, p1, v2}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 249
    .local v1, "subList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, ", "

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 251
    .local v0, "line":Ljava/lang/String;
    const-string v2, ", *, *"

    const-string v3, ", "

    invoke-static {v0, v2, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->replaceAllSafely(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 256
    .end local v0    # "line":Ljava/lang/String;
    .end local v1    # "subList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v2

    :cond_0
    const-string v2, ""

    goto :goto_0
.end method

.method private static readCountries(Landroid/util/JsonReader;)V
    .locals 6
    .param p0, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginArray()V

    .line 108
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 110
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 111
    const/4 v1, 0x0

    .line 112
    .local v1, "iso2":Ljava/lang/String;
    const/4 v2, 0x0

    .line 113
    .local v2, "iso3":Ljava/lang/String;
    const/4 v0, 0x0

    .line 114
    .local v0, "format":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 115
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "name":Ljava/lang/String;
    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 128
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_1

    .line 116
    :sswitch_0
    const-string v5, "ISO_3166-2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "ISO 3166-1-a3"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "format"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x2

    goto :goto_2

    .line 118
    :pswitch_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    .line 119
    goto :goto_1

    .line 121
    :pswitch_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    .line 122
    goto :goto_1

    .line 124
    :pswitch_2
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    .line 125
    goto :goto_1

    .line 132
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 133
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 134
    sget-object v4, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isoToFormatString:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    :cond_2
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 137
    sget-object v4, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isoToFormatString:Ljava/util/HashMap;

    invoke-virtual {v4, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_3
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 140
    sget-object v4, Lcom/navdy/client/app/framework/i18n/AddressUtils;->iso3ToIso2:Ljava/util/Map;

    invoke-interface {v4, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    :cond_4
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    goto/16 :goto_0

    .line 145
    .end local v0    # "format":Ljava/lang/String;
    .end local v1    # "iso2":Ljava/lang/String;
    .end local v2    # "iso3":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Landroid/util/JsonReader;->endArray()V

    .line 146
    return-void

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4ba00809 -> :sswitch_2
        -0x3b1ee723 -> :sswitch_0
        -0xf46381e -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static readFormats(Landroid/util/JsonReader;)V
    .locals 7
    .param p0, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginArray()V

    .line 151
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 153
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 154
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 155
    .local v2, "formatName":Ljava/lang/String;
    new-instance v1, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;-><init>()V

    .line 158
    .local v1, "format":Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 159
    :goto_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 160
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "lineName":Ljava/lang/String;
    const/4 v5, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_1
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 190
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_1

    .line 161
    :sswitch_0
    const-string v6, "local_lines"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "foreign_lines"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    .line 164
    :pswitch_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginArray()V

    .line 165
    :cond_2
    :goto_3
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 167
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    .line 168
    .local v4, "localLine":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 169
    iget-object v5, v1, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->localLines:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 173
    .end local v4    # "localLine":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Landroid/util/JsonReader;->endArray()V

    goto :goto_1

    .line 177
    :pswitch_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginArray()V

    .line 178
    :cond_4
    :goto_4
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 180
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "foreignLine":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 182
    iget-object v5, v1, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->foreignLines:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 186
    .end local v0    # "foreignLine":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Landroid/util/JsonReader;->endArray()V

    goto :goto_1

    .line 195
    .end local v3    # "lineName":Ljava/lang/String;
    :cond_6
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    .line 197
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V

    .line 199
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 200
    sget-object v5, Lcom/navdy/client/app/framework/i18n/AddressUtils;->formats:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 204
    .end local v1    # "format":Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    .end local v2    # "formatName":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Landroid/util/JsonReader;->endArray()V

    .line 205
    return-void

    .line 161
    :sswitch_data_0
    .sparse-switch
        0xbe443d4 -> :sswitch_1
        0x687d152b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "address"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .prologue
    .line 261
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-object p0

    .line 264
    :cond_1
    const-string v0, "\n"

    const-string v1, ", "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 265
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 266
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const-string v2, ","

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
