.class public Lcom/navdy/client/app/framework/callcontrol/TelephonyInterfaceFactory;
.super Ljava/lang/Object;
.source "TelephonyInterfaceFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTelephonyInterface(Landroid/content/Context;)Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x19

    if-le v0, v1, :cond_0

    .line 12
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;-><init>(Landroid/content/Context;)V

    .line 45
    :goto_0
    return-object v0

    .line 13
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_9

    .line 14
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "nexus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 18
    :cond_1
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "h1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v1, "p1"

    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 21
    :cond_2
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 23
    :cond_3
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 26
    :cond_4
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 27
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-G920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-G935"

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-G930"

    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-N900"

    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-N910"

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-G900"

    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 33
    :cond_5
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 34
    :cond_6
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SM-G950"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 36
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G950;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G950;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 39
    :cond_7
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "htc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC6545LVW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 40
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21HTC10;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21HTC10;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 42
    :cond_8
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 45
    :cond_9
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
