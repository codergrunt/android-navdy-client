.class Lcom/navdy/client/app/framework/map/HereMapsManager$3;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

.field final synthetic val$listener:Lcom/here/android/mpa/common/OnEngineInitListener;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/HereMapsManager;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$3;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$3;->val$listener:Lcom/here/android/mpa/common/OnEngineInitListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 117
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$300()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$3;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$400(Lcom/navdy/client/app/framework/map/HereMapsManager;)Ljava/util/Queue;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$3;->val$listener:Lcom/here/android/mpa/common/OnEngineInitListener;

    invoke-interface {v0, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 119
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$3;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$200(Lcom/navdy/client/app/framework/map/HereMapsManager;)Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->IDLE:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    if-ne v0, v1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$3;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$600(Lcom/navdy/client/app/framework/map/HereMapsManager;)V

    .line 124
    :cond_0
    return-void

    .line 119
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
