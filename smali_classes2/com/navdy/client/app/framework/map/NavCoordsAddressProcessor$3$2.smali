.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;->onError(Lcom/here/android/mpa/search/ErrorCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;

.field final synthetic val$error:Lcom/here/android/mpa/search/ErrorCode;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 278
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tryHerePlacesSearchNativeApiWithAddress returned HERE error["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;->val$error:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v2}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "];"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " trying with Google now"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$800(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    .line 281
    return-void
.end method
