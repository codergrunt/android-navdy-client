.class final Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callWebHerePlacesSearchApi(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$HERE_SEARCH_BASE_URL:Ljava/lang/String;

.field final synthetic val$address:Ljava/lang/String;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$address:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$HERE_SEARCH_BASE_URL:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;
    .param p1, "x1"    # Ljava/lang/Double;
    .param p2, "x2"    # Ljava/lang/Double;
    .param p3, "x3"    # Ljava/lang/Double;
    .param p4, "x4"    # Ljava/lang/Double;
    .param p5, "x5"    # Lcom/here/android/mpa/search/Address;
    .param p6, "x6"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 519
    invoke-direct/range {p0 .. p6}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Lorg/json/JSONObject;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;
    .param p1, "x1"    # Lorg/json/JSONObject;
    .param p2, "x2"    # Ljava/lang/Double;
    .param p3, "x3"    # Ljava/lang/Double;
    .param p4, "x4"    # Lcom/here/android/mpa/search/Address;
    .param p5, "x5"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 519
    invoke-direct/range {p0 .. p5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->parseMetaDataResponse(Lorg/json/JSONObject;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    return-void
.end method

.method private fetchMetaData(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 11
    .param p1, "metaDataUrl"    # Ljava/lang/String;
    .param p2, "navLat"    # Ljava/lang/Double;
    .param p3, "navLong"    # Ljava/lang/Double;
    .param p4, "hereAddress"    # Lcom/here/android/mpa/search/Address;
    .param p5, "callback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 605
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    .line 632
    :goto_0
    return-void

    .line 610
    :cond_0
    new-instance v8, Lcom/android/volley/toolbox/JsonObjectRequest;

    const/4 v7, 0x0

    const/4 v10, 0x0

    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$1;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    move-object v2, v8

    move v3, v7

    move-object v4, p1

    move-object v5, v10

    move-object v6, v0

    move-object v7, v1

    invoke-direct/range {v2 .. v7}, Lcom/android/volley/toolbox/JsonObjectRequest;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 629
    .local v8, "jsonRequest":Lcom/android/volley/toolbox/JsonObjectRequest;
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/volley/toolbox/Volley;->newRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v9

    .line 630
    .local v9, "requestQueue":Lcom/android/volley/RequestQueue;
    invoke-virtual {v9, v8}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto :goto_0
.end method

.method private finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 6
    .param p1, "dispLat"    # Ljava/lang/Double;
    .param p2, "dispLong"    # Ljava/lang/Double;
    .param p3, "navLat"    # Ljava/lang/Double;
    .param p4, "navLong"    # Ljava/lang/Double;
    .param p5, "hereAddress"    # Lcom/here/android/mpa/search/Address;
    .param p6, "callback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    const-wide/16 v2, 0x0

    .line 664
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 665
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 666
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 665
    invoke-interface {p6, v0, v1, p5, v2}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onComplete(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    .line 672
    :goto_0
    return-void

    .line 670
    :cond_0
    sget-object v0, Lcom/here/android/mpa/search/ErrorCode;->BAD_LOCATION:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {p6, v0}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0
.end method

.method private parseAddressJson(Lorg/json/JSONObject;)Lcom/here/android/mpa/search/Address;
    .locals 3
    .param p1, "location"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 588
    new-instance v1, Lcom/here/android/mpa/search/Address;

    invoke-direct {v1}, Lcom/here/android/mpa/search/Address;-><init>()V

    .line 589
    .local v1, "hereAddress":Lcom/here/android/mpa/search/Address;
    const-string v2, "address"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 590
    .local v0, "address":Lorg/json/JSONObject;
    const-string v2, "text"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setText(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 591
    const-string v2, "house"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setHouseNumber(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 592
    const-string v2, "street"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setStreet(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 593
    const-string v2, "city"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setCity(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 594
    const-string v2, "stateCode"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setState(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 595
    const-string v2, "postalCode"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setPostalCode(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 596
    const-string v2, "country"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/search/Address;->setCounty(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 597
    return-object v1
.end method

.method private parseLocationJson(Lorg/json/JSONObject;)Landroid/util/Pair;
    .locals 6
    .param p1, "item"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 579
    const-string v5, "position"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 580
    .local v4, "position":Lorg/json/JSONArray;
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 581
    .local v1, "navLatStr":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 582
    .local v2, "navLonStr":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    .line 583
    .local v0, "navLat":Ljava/lang/Double;
    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    .line 584
    .local v3, "navLong":Ljava/lang/Double;
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v0, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v5
.end method

.method private parseMetaDataResponse(Lorg/json/JSONObject;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 14
    .param p1, "response"    # Lorg/json/JSONObject;
    .param p2, "navLat"    # Ljava/lang/Double;
    .param p3, "navLong"    # Ljava/lang/Double;
    .param p4, "hereAddress"    # Lcom/here/android/mpa/search/Address;
    .param p5, "callback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 640
    :try_start_0
    const-string v2, "location"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 642
    .local v10, "location":Lorg/json/JSONObject;
    invoke-direct {p0, v10}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->parseLocationJson(Lorg/json/JSONObject;)Landroid/util/Pair;

    move-result-object v11

    .line 643
    .local v11, "navCoords":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Double;>;"
    iget-object v2, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, Ljava/lang/Double;

    move-object/from16 p2, v0

    .line 644
    iget-object v2, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, Ljava/lang/Double;

    move-object/from16 p3, v0

    .line 646
    invoke-direct {p0, v10}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->parseAddressJson(Lorg/json/JSONObject;)Lcom/here/android/mpa/search/Address;

    move-result-object p4

    .line 648
    const-string v2, "view"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 649
    .local v12, "view":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Here returned the result with URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 654
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object v2, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    .line 656
    .end local v10    # "location":Lorg/json/JSONObject;
    .end local v11    # "navCoords":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Double;>;"
    .end local v12    # "view":Ljava/lang/String;
    :goto_0
    return-void

    .line 651
    :catch_0
    move-exception v9

    .line 652
    .local v9, "e":Lorg/json/JSONException;
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Json Error occured while trying to retrieve more info about a HERE places search result. "

    invoke-virtual {v2, v3, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 654
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object v2, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    goto :goto_0

    .end local v9    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v2

    move-object v13, v2

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    move-object v2, p0

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    throw v13
.end method


# virtual methods
.method public run()V
    .locals 29

    .prologue
    .line 522
    const/16 v16, 0x0

    .line 524
    .local v16, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v14

    .line 525
    .local v14, "currentLocation":Lcom/navdy/service/library/events/location/Coordinate;
    if-nez v14, :cond_0

    .line 526
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    sget-object v7, Lcom/here/android/mpa/search/ErrorCode;->BAD_LOCATION:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {v2, v7}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 576
    .end local v14    # "currentLocation":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_0
    return-void

    .line 530
    .restart local v14    # "currentLocation":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v14, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ","

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, v14, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "&q="

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$address:Ljava/lang/String;

    const-string v8, "UTF-8"

    .line 531
    invoke-static {v7, v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 532
    .local v20, "params":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$HERE_SEARCH_BASE_URL:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 534
    .local v27, "urlStr":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Calling HERE with URL: https://places.cit.api.here.com/places/v1/discover/search?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&at="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 536
    new-instance v25, Ljava/net/URL;

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 537
    .local v25, "url":Ljava/net/URL;
    invoke-virtual/range {v25 .. v25}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v26

    check-cast v26, Ljavax/net/ssl/HttpsURLConnection;

    .line 538
    .local v26, "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    invoke-virtual/range {v26 .. v26}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v15

    .line 540
    .local v15, "httpResponse":I
    new-instance v6, Lcom/here/android/mpa/search/Address;

    invoke-direct {v6}, Lcom/here/android/mpa/search/Address;-><init>()V

    .line 542
    .local v6, "hereAddress":Lcom/here/android/mpa/search/Address;
    const/16 v2, 0xc8

    if-lt v15, v2, :cond_2

    const/16 v2, 0x12c

    if-ge v15, v2, :cond_2

    .line 543
    invoke-virtual/range {v26 .. v26}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v16

    .line 544
    const-string v2, "UTF-8"

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 546
    .local v18, "jsonStr":Ljava/lang/String;
    new-instance v22, Lorg/json/JSONObject;

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 547
    .local v22, "root":Lorg/json/JSONObject;
    const-string v2, "results"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v21

    .line 548
    .local v21, "results":Lorg/json/JSONObject;
    const-string v2, "items"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    const/4 v7, 0x0

    .line 549
    invoke-virtual {v2, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v17

    .line 551
    .local v17, "item":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->parseLocationJson(Lorg/json/JSONObject;)Landroid/util/Pair;

    move-result-object v19

    .line 552
    .local v19, "navCoords":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Double;>;"
    move-object/from16 v0, v19

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Double;

    .line 553
    .local v4, "navLat":Ljava/lang/Double;
    move-object/from16 v0, v19

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    .line 555
    .local v5, "navLong":Ljava/lang/Double;
    const-string v2, "title"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 556
    .local v24, "title":Ljava/lang/String;
    const-string v2, "vicinity"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 558
    .local v28, "vicinity":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/here/android/mpa/search/Address;->setText(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 559
    const-string v2, "href"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 561
    .local v3, "metaDataUrl":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 562
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->fetchMetaData(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574
    .end local v3    # "metaDataUrl":Ljava/lang/String;
    .end local v4    # "navLat":Ljava/lang/Double;
    .end local v5    # "navLong":Ljava/lang/Double;
    .end local v17    # "item":Lorg/json/JSONObject;
    .end local v18    # "jsonStr":Ljava/lang/String;
    .end local v19    # "navCoords":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Double;>;"
    .end local v21    # "results":Lorg/json/JSONObject;
    .end local v22    # "root":Lorg/json/JSONObject;
    .end local v24    # "title":Ljava/lang/String;
    .end local v28    # "vicinity":Ljava/lang/String;
    :goto_1
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 564
    .restart local v3    # "metaDataUrl":Ljava/lang/String;
    .restart local v4    # "navLat":Ljava/lang/Double;
    .restart local v5    # "navLong":Ljava/lang/Double;
    .restart local v17    # "item":Lorg/json/JSONObject;
    .restart local v18    # "jsonStr":Ljava/lang/String;
    .restart local v19    # "navCoords":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Double;>;"
    .restart local v21    # "results":Lorg/json/JSONObject;
    .restart local v22    # "root":Lorg/json/JSONObject;
    .restart local v24    # "title":Ljava/lang/String;
    .restart local v28    # "vicinity":Ljava/lang/String;
    :cond_1
    const-wide/16 v8, 0x0

    :try_start_2
    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object/from16 v7, p0

    move-object v10, v4

    move-object v11, v5

    move-object v12, v6

    invoke-direct/range {v7 .. v13}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->finalizeWebPlaceSearch(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 570
    .end local v3    # "metaDataUrl":Ljava/lang/String;
    .end local v4    # "navLat":Ljava/lang/Double;
    .end local v5    # "navLong":Ljava/lang/Double;
    .end local v6    # "hereAddress":Lcom/here/android/mpa/search/Address;
    .end local v14    # "currentLocation":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v15    # "httpResponse":I
    .end local v17    # "item":Lorg/json/JSONObject;
    .end local v18    # "jsonStr":Ljava/lang/String;
    .end local v19    # "navCoords":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Double;Ljava/lang/Double;>;"
    .end local v20    # "params":Ljava/lang/String;
    .end local v21    # "results":Lorg/json/JSONObject;
    .end local v22    # "root":Lorg/json/JSONObject;
    .end local v24    # "title":Ljava/lang/String;
    .end local v25    # "url":Ljava/net/URL;
    .end local v26    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v27    # "urlStr":Ljava/lang/String;
    .end local v28    # "vicinity":Ljava/lang/String;
    :catch_0
    move-exception v23

    .line 571
    .local v23, "t":Ljava/lang/Throwable;
    :try_start_3
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v7, "getNavigationCoordinate"

    move-object/from16 v0, v23

    invoke-virtual {v2, v7, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 572
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    sget-object v7, Lcom/here/android/mpa/search/ErrorCode;->SERVER_INTERNAL:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {v2, v7}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 574
    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 567
    .end local v23    # "t":Ljava/lang/Throwable;
    .restart local v6    # "hereAddress":Lcom/here/android/mpa/search/Address;
    .restart local v14    # "currentLocation":Lcom/navdy/service/library/events/location/Coordinate;
    .restart local v15    # "httpResponse":I
    .restart local v20    # "params":Ljava/lang/String;
    .restart local v25    # "url":Ljava/net/URL;
    .restart local v26    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v27    # "urlStr":Ljava/lang/String;
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "callWebHereGeocoderApi: response code:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 568
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    sget-object v7, Lcom/here/android/mpa/search/ErrorCode;->BAD_REQUEST:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface {v2, v7}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 574
    .end local v6    # "hereAddress":Lcom/here/android/mpa/search/Address;
    .end local v14    # "currentLocation":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v15    # "httpResponse":I
    .end local v20    # "params":Ljava/lang/String;
    .end local v25    # "url":Ljava/net/URL;
    .end local v26    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v27    # "urlStr":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-static/range {v16 .. v16}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method
