.class Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;
.super Ljava/lang/Object;
.source "HereGeocoder.java"

# interfaces
.implements Lcom/here/android/mpa/search/ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/map/HereGeocoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TimeoutWrappedResultListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/here/android/mpa/search/ResultListener",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final GEOCODE_REQUEST_TIMEOUT_MILLIS:J = 0x7530L


# instance fields
.field cancelled:Z

.field public complete:Z

.field private final listener:Lcom/here/android/mpa/search/ResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/here/android/mpa/search/ResultListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final timeout:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/here/android/mpa/search/ResultListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/search/ResultListener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p0, "this":Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;, "Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener<TT;>;"
    .local p1, "listener":Lcom/here/android/mpa/search/ResultListener;, "Lcom/here/android/mpa/search/ResultListener<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->listener:Lcom/here/android/mpa/search/ResultListener;

    .line 211
    new-instance v0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener$1;-><init>(Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->timeout:Ljava/lang/Runnable;

    .line 227
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$400()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->timeout:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 228
    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;)Lcom/here/android/mpa/search/ResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->listener:Lcom/here/android/mpa/search/ResultListener;

    return-object v0
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 3
    .param p2, "errorCode"    # Lcom/here/android/mpa/search/ErrorCode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/here/android/mpa/search/ErrorCode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 232
    .local p0, "this":Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;, "Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener<TT;>;"
    .local p1, "locations":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    .line 233
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$400()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->timeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->complete:Z

    .line 235
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->cancelled:Z

    if-eqz v0, :cond_0

    .line 236
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Geocode call completed after timeout, error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 237
    const-string v0, "Debug_Here_Geocode_Completed_After_Timeout"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 242
    :goto_0
    monitor-exit p0

    .line 243
    return-void

    .line 239
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereGeocoder;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Geocode call completed successfully"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereGeocoder$TimeoutWrappedResultListener;->listener:Lcom/here/android/mpa/search/ResultListener;

    invoke-interface {v0, p1, p2}, Lcom/here/android/mpa/search/ResultListener;->onCompleted(Ljava/lang/Object;Lcom/here/android/mpa/search/ErrorCode;)V

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
