.class Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;
.super Ljava/lang/Object;
.source "SettingsServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->onDriverProfileRequest(Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

.field final synthetic val$driverProfilePreferencesRequest:Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;->val$driverProfilePreferencesRequest:Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 50
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 51
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v4, "profile_serial_number"

    sget-object v5, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    .line 52
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 51
    invoke-interface {v0, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 53
    .local v2, "serial":J
    iget-object v4, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;->val$driverProfilePreferencesRequest:Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesRequest;->serial_number:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    .line 54
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Driver profile version up to date"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 55
    iget-object v4, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    new-instance v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/squareup/wire/Message;)V

    .line 63
    .end local v0    # "customerPrefs":Landroid/content/SharedPreferences;
    .end local v2    # "serial":J
    :goto_0
    return-void

    .line 57
    .restart local v0    # "customerPrefs":Landroid/content/SharedPreferences;
    .restart local v2    # "serial":J
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    .end local v0    # "customerPrefs":Landroid/content/SharedPreferences;
    .end local v2    # "serial":J
    :catch_0
    move-exception v1

    .line 60
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 61
    new-instance v4, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v4, v5, v10, v6, v10}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)V

    invoke-static {v4}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0
.end method
