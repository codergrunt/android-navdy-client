.class Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;
.super Ljava/lang/Object;
.source "ContactServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->onFavoriteContactsRequest(Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

.field final synthetic val$responseBuilder:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->val$request:Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->val$responseBuilder:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 77
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->getInstance()Lcom/navdy/client/app/framework/util/ContactsManager;

    move-result-object v7

    .line 78
    invoke-virtual {v7}, Lcom/navdy/client/app/framework/util/ContactsManager;->getFavoriteContactsWithPhone()Ljava/util/List;

    move-result-object v2

    .line 80
    .local v2, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    invoke-static {v7, v2}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 81
    .local v6, "sanitizedContactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 82
    .local v1, "contactIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v4, "outboundList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->val$request:Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

    iget-object v8, v8, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;->maxContacts:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 85
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 87
    .local v0, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    iget-boolean v7, v0, Lcom/navdy/client/app/framework/models/ContactModel;->favorite:Z

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    if-eqz v7, :cond_0

    .line 88
    iget-object v7, v0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    .line 89
    .local v5, "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    iget-object v8, v5, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 90
    new-instance v3, Lcom/navdy/service/library/events/contacts/Contact;

    iget-object v8, v0, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iget-object v9, v5, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    iget-object v10, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    iget v11, v5, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    .line 93
    invoke-static {v10, v11}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;I)Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v3, v8, v9, v10, v11}, Lcom/navdy/service/library/events/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/contacts/PhoneNumberType;Ljava/lang/String;)V

    .line 96
    .local v3, "outboundContact":Lcom/navdy/service/library/events/contacts/Contact;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    .end local v0    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v3    # "outboundContact":Lcom/navdy/service/library/events/contacts/Contact;
    .end local v5    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :cond_2
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->val$responseBuilder:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    sget-object v8, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 102
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    move-result-object v7

    .line 103
    invoke-virtual {v7, v4}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->contacts(Ljava/util/List;)Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    .line 104
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$1;->val$responseBuilder:Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;

    invoke-virtual {v7}, Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/FavoriteContactsResponse;

    move-result-object v7

    invoke-static {v7}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 105
    return-void
.end method
