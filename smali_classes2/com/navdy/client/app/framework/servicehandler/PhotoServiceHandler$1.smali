.class Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;
.super Ljava/lang/Object;
.source "PhotoServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->onPhotoRequest(Lcom/navdy/service/library/events/photo/PhotoRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;Lcom/navdy/service/library/events/photo/PhotoRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 58
    const/4 v7, 0x0

    .line 60
    .local v7, "photo":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/photo/PhotoType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    invoke-static {v3}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;)I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 67
    :cond_0
    :goto_0
    if-nez v7, :cond_3

    .line 68
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhotoServiceHandler:Cannot find requested photo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 73
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    invoke-static {v7, v0}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$300(Landroid/graphics/Bitmap;Lcom/navdy/service/library/events/photo/PhotoRequest;)Lcom/navdy/service/library/events/photo/PhotoResponse;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 83
    :goto_2
    return-void

    .line 62
    :cond_1
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/photo/PhotoType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v0, v0, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/MusicUtils;->getMusicPhoto(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    goto :goto_0

    .line 64
    :cond_2
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_DRIVER_PROFILE:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v2, v2, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/photo/PhotoType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;)I

    move-result v0

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->getScaledProfilePhoto(I)Landroid/graphics/Bitmap;

    move-result-object v7

    goto :goto_0

    .line 71
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhotoServiceHandler:found photo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 74
    :catch_0
    move-exception v8

    .line 75
    .local v8, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PhotoServiceHandler:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 76
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoResponse;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v4, v3, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler$1;->val$request:Lcom/navdy/service/library/events/photo/PhotoRequest;

    iget-object v6, v3, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    move-object v3, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/photo/PhotoResponse;-><init>(Lokio/ByteString;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_2
.end method
