.class Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;
.super Ljava/lang/Object;
.source "DestinationSuggestionService.java"

# interfaces
.implements Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->uploadFileToS3(Ljava/io/File;Lcom/squareup/otto/Bus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

.field final synthetic val$bus:Lcom/squareup/otto/Bus;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Ljava/io/File;Lcom/squareup/otto/Bus;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    iput-object p2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$file:Ljava/io/File;

    iput-object p3, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 604
    invoke-static {}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uploading File "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", onError : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 605
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 608
    :cond_0
    return-void
.end method

.method public onProgressChanged(IJJ)V
    .locals 4
    .param p1, "id"    # I
    .param p2, "bytesCurrent"    # J
    .param p4, "bytesTotal"    # J

    .prologue
    .line 599
    invoke-static {}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uploading File "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", onProgressChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Size :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 600
    return-void
.end method

.method public onStateChanged(ILcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "state"    # Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    .prologue
    .line 585
    invoke-static {}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$500()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uploading File "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", onStatChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 586
    sget-object v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->COMPLETED:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-ne p2, v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->SUCCESS:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    sget-object v0, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->FAILED:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-ne p2, v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$4;->val$bus:Lcom/squareup/otto/Bus;

    sget-object v1, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;->FAILURE:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$UploadResult;

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
