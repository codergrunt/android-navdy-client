.class public Lcom/navdy/client/app/tracking/TrackerConstants$Screen$FirstLaunch;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FirstLaunch"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/tracking/TrackerConstants$Screen$FirstLaunch$Install;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Screen$FirstLaunch$Marketing;
    }
.end annotation


# static fields
.field public static final CAR_INFO:Ljava/lang/String; = "First_Launch_Car_Info"

.field public static final EDIT_CAR_INFO:Ljava/lang/String; = "First_Launch_Edit_Car_Info"

.field public static final FIRST_LAUNCH:Ljava/lang/String; = "First_Launch"

.field public static final INSTALL_VIDEO:Ljava/lang/String; = "First_Launch_Install_Video"

.field public static final SCREEN_LIGHT:Ljava/lang/String; = "First_Launch_Screen_Light"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
