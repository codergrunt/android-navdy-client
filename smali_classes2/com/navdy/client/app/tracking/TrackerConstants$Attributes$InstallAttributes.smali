.class public Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$InstallAttributes;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Attributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InstallAttributes"
.end annotation


# static fields
.field public static final MEDIUM_MOUNT:Ljava/lang/String; = "Medium_Mount"

.field public static final MOUNT_PICKER_MOUNT_TYPE:Ljava/lang/String; = "Mount_Type"

.field public static final MOUNT_PICKER_MOUNT_TYPE_MED_TALL:Ljava/lang/String; = "Medium_Tall"

.field public static final MOUNT_PICKER_MOUNT_TYPE_SHORT:Ljava/lang/String; = "Short"

.field public static final MOUNT_PICKER_USE_CASE:Ljava/lang/String; = "Use_Case"

.field public static final NB_CONFIGURATIONS:Ljava/lang/String; = "Nb_Configurations"

.field public static final NEW_BOX:Ljava/lang/String; = "New_Box"

.field public static final NEW_BOX_PLUS_MOUNTS:Ljava/lang/String; = "New_Box_Plus_Mounts"

.field public static final OLD_BOX:Ljava/lang/String; = "Old_Box"

.field public static final SELECTED_BOX:Ljava/lang/String; = "Selected_Box"

.field public static final SELECTED_MOUNT:Ljava/lang/String; = "Selected_Mount"

.field public static final SHORT_MOUNT:Ljava/lang/String; = "Short_Mount"

.field public static final TALL_MOUNT:Ljava/lang/String; = "Tall_Mount"

.field public static final VIDEO_URL:Ljava/lang/String; = "Video_URL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
