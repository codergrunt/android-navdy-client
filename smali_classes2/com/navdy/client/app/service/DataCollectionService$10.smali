.class Lcom/navdy/client/app/service/DataCollectionService$10;
.super Ljava/lang/Object;
.source "DataCollectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/service/DataCollectionService;->handleLowMemory()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/service/DataCollectionService;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 1209
    iput-object p1, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1212
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$100(Lcom/navdy/client/app/service/DataCollectionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1213
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v2, "We are about to run out of memory"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1214
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$1600(Lcom/navdy/client/app/service/DataCollectionService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1216
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v2, v2, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-wide v2, v2, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    invoke-static {v0, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->access$800(Lcom/navdy/client/app/service/DataCollectionService;J)V

    .line 1223
    :goto_0
    monitor-exit v1

    .line 1224
    return-void

    .line 1219
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$1500(Lcom/navdy/client/app/service/DataCollectionService;)V

    .line 1221
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$10;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$1700(Lcom/navdy/client/app/service/DataCollectionService;)V

    goto :goto_0

    .line 1223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
