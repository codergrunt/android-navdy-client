.class public Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;
.super Landroid/os/Binder;
.source "OTAUpdateManagerImpl.java"

# interfaces
.implements Lcom/navdy/client/ota/OTAUpdateManager;


# static fields
.field public static final S3_CONNECTION_TIMEOUT:I = 0x3a98

.field public static final S3_DOWNLOAD_TIMEOUT:I = 0xea60

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

.field private mContext:Landroid/content/Context;

.field protected mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

.field private mRemoteFileTransferManager:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

.field protected mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

.field private mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

.field protected mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/client/ota/OTAUpdateListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/navdy/client/ota/OTAUpdateListener;

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 59
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    iput-object p1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mContext:Landroid/content/Context;

    .line 64
    iput-object p2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    .line 65
    if-nez p1, :cond_0

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method public static createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;
    .locals 6

    .prologue
    .line 72
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080551

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "AWS_ACCOUNT_ID":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080552

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "AWS_SECRET":Ljava/lang/String;
    new-instance v3, Lcom/amazonaws/auth/BasicAWSCredentials;

    invoke-direct {v3, v0, v1}, Lcom/amazonaws/auth/BasicAWSCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .local v3, "credentials":Lcom/amazonaws/auth/BasicAWSCredentials;
    new-instance v2, Lcom/amazonaws/ClientConfiguration;

    invoke-direct {v2}, Lcom/amazonaws/ClientConfiguration;-><init>()V

    .line 79
    .local v2, "config":Lcom/amazonaws/ClientConfiguration;
    const/16 v4, 0x3a98

    invoke-virtual {v2, v4}, Lcom/amazonaws/ClientConfiguration;->setConnectionTimeout(I)V

    .line 80
    const v4, 0xea60

    invoke-virtual {v2, v4}, Lcom/amazonaws/ClientConfiguration;->setSocketTimeout(I)V

    .line 81
    new-instance v4, Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-direct {v4, v3, v2}, Lcom/amazonaws/services/s3/AmazonS3Client;-><init>(Lcom/amazonaws/auth/AWSCredentials;Lcom/amazonaws/ClientConfiguration;)V

    return-object v4
.end method

.method private fetchMetaData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "bucket"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 466
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-virtual {v2, p1, p2}, Lcom/amazonaws/services/s3/AmazonS3Client;->getObject(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/s3/model/S3Object;

    move-result-object v0

    .line 467
    .local v0, "jsonObject":Lcom/amazonaws/services/s3/model/S3Object;
    invoke-virtual {v0}, Lcom/amazonaws/services/s3/model/S3Object;->getObjectContent()Lcom/amazonaws/services/s3/model/S3ObjectInputStream;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 471
    .end local v0    # "jsonObject":Lcom/amazonaws/services/s3/model/S3Object;
    :goto_0
    return-object v2

    .line 468
    :catch_0
    move-exception v1

    .line 469
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Error while reading the meta data "

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 471
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private fetchUpdateInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/client/ota/model/UpdateInfo;
    .locals 6
    .param p1, "bucket"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 455
    iget-object v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-virtual {v3, p1, p2}, Lcom/amazonaws/services/s3/AmazonS3Client;->getObject(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/s3/model/S3Object;

    move-result-object v1

    .line 457
    .local v1, "jsonObject":Lcom/amazonaws/services/s3/model/S3Object;
    invoke-virtual {v1}, Lcom/amazonaws/services/s3/model/S3Object;->getObjectContent()Lcom/amazonaws/services/s3/model/S3ObjectInputStream;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 458
    .local v2, "jsonString":Ljava/lang/String;
    sget-object v3, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fetched update info: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 460
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 461
    .local v0, "gson":Lcom/google/gson/Gson;
    const-class v3, Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/ota/model/UpdateInfo;

    return-object v3
.end method

.method private getOtaTopLevelFolderName()Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->STABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    .line 201
    .local v0, "buildSource":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 202
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 203
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildSource()Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v0

    .line 205
    :cond_0
    return-object v0
.end method

.method private static parseS3Url(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p0, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 442
    if-eqz p0, :cond_0

    const-string v4, "s3://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 443
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Invalid url"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 445
    :cond_1
    const-string v4, "s3://"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 446
    .local v3, "shortForm":Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "bucketName":Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 448
    .local v1, "key":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    .line 449
    .local v2, "result":[Ljava/lang/String;
    aput-object v0, v2, v5

    .line 450
    const/4 v4, 0x1

    aput-object v1, v2, v4

    .line 451
    return-object v2
.end method


# virtual methods
.method public abortDownload()V
    .locals 4

    .prologue
    .line 416
    sget-object v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Download aborted"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 417
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    if-eqz v1, :cond_0

    .line 420
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    iget-object v2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->cancel(I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 421
    :catch_0
    move-exception v0

    .line 422
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error aborting the download"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public abortUpload()V
    .locals 3

    .prologue
    .line 430
    sget-object v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "abortUpload: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mRemoteFileTransferManager:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mRemoteFileTransferManager:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-virtual {v0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->cancelFileUpload()V

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    sget-object v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Cannot abort upload as it is already aborted"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public checkForUpdate(IZ)V
    .locals 14
    .param p1, "currentIncrementalVersion"    # I
    .param p2, "forceFullUpdate"    # Z

    .prologue
    .line 92
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Checking for OTA update , current version :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 93
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 94
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "No connectivity for checking OTA update"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 96
    :try_start_0
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    if-eqz v9, :cond_0

    .line 97
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v10, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/navdy/client/ota/OTAUpdateListener;->onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v8

    .line 100
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Bad listener"

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 104
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v4, 0x0

    .line 105
    .local v4, "incrementalUpdateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    invoke-direct {p0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->getOtaTopLevelFolderName()Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v0

    .line 106
    .local v0, "buildSource":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    if-nez p2, :cond_2

    const/4 v9, -0x1

    if-eq p1, v9, :cond_2

    .line 108
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->getPrefix()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lookup"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "latest.json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "fileNameForIncrementalVersion":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->getBucket()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, v3}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->fetchUpdateInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/client/ota/model/UpdateInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v4

    .line 128
    .end local v3    # "fileNameForIncrementalVersion":Ljava/lang/String;
    :cond_2
    :goto_1
    const/4 v5, 0x0

    .line 129
    .local v5, "latestUpdateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->getPrefix()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "lookup"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "latest.json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "fileName":Ljava/lang/String;
    :try_start_2
    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->getBucket()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, v2}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->fetchUpdateInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/navdy/client/ota/model/UpdateInfo;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v5

    .line 144
    :cond_3
    :goto_2
    const/4 v6, 0x0

    .line 145
    .local v6, "metaData":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 146
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->getPrefix()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "release_notes_%s.json"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, v5, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    .line 149
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    .line 147
    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 150
    .local v7, "metaDataFileName":Ljava/lang/String;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Fetching the meta data from :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->getBucket()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9, v7}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->fetchMetaData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 154
    .end local v7    # "metaDataFileName":Ljava/lang/String;
    :cond_4
    if-eqz v5, :cond_5

    iget v9, v5, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    if-lt p1, v9, :cond_a

    .line 155
    :cond_5
    if-nez v5, :cond_6

    .line 156
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Couldn\'t retrieve the latest info data (latestUpdateInfo == null)"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 158
    :cond_6
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    if-eqz v9, :cond_0

    .line 159
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "S/w up to date"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 161
    :try_start_3
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 162
    iput-object v6, v5, Lcom/navdy/client/ota/model/UpdateInfo;->metaData:Ljava/lang/String;

    .line 164
    :cond_7
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v10, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->UPTODATE:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    invoke-interface {v9, v10, v5}, Lcom/navdy/client/ota/OTAUpdateListener;->onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 165
    :catch_1
    move-exception v8

    .line 166
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Bad listener "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 111
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v5    # "latestUpdateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    .end local v6    # "metaData":Ljava/lang/String;
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v3    # "fileNameForIncrementalVersion":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    instance-of v9, v1, Lcom/amazonaws/services/s3/model/AmazonS3Exception;

    if-eqz v9, :cond_8

    move-object v9, v1

    check-cast v9, Lcom/amazonaws/services/s3/model/AmazonS3Exception;

    invoke-virtual {v9}, Lcom/amazonaws/services/s3/model/AmazonS3Exception;->getStatusCode()I

    move-result v9

    const/16 v10, 0x194

    if-eq v9, v10, :cond_9

    .line 113
    :cond_8
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Server error while checking for OTA update, current version :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 115
    :try_start_4
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    if-eqz v9, :cond_0

    .line 116
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v10, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/navdy/client/ota/OTAUpdateListener;->onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 118
    :catch_3
    move-exception v8

    .line 119
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Bad listener "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 123
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_9
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "File does not exist in Amazon S3: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 132
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "fileNameForIncrementalVersion":Ljava/lang/String;
    .restart local v2    # "fileName":Ljava/lang/String;
    .restart local v5    # "latestUpdateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    :catch_4
    move-exception v1

    .line 134
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_5
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Server error while checking for OTA update, current version :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 135
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    if-eqz v9, :cond_3

    .line 136
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v10, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/navdy/client/ota/OTAUpdateListener;->onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_2

    .line 138
    :catch_5
    move-exception v8

    .line 139
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Bad listener "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 172
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v6    # "metaData":Ljava/lang/String;
    :cond_a
    if-eqz v4, :cond_c

    iget v9, v4, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    iget v10, v5, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    if-ne v9, v10, :cond_c

    .line 173
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "An incremental update is available, from :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v4, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 175
    const/4 v9, 0x1

    :try_start_6
    iput-boolean v9, v4, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    .line 176
    iput p1, v4, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    .line 177
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 178
    iput-object v6, v4, Lcom/navdy/client/ota/model/UpdateInfo;->metaData:Ljava/lang/String;

    .line 180
    :cond_b
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v10, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->AVAILABLE:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    invoke-interface {v9, v10, v4}, Lcom/navdy/client/ota/OTAUpdateListener;->onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 181
    :catch_6
    move-exception v8

    .line 182
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Bad listener "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 186
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_c
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "A full update is available : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v5, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 187
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_d

    .line 188
    iput-object v6, v5, Lcom/navdy/client/ota/model/UpdateInfo;->metaData:Ljava/lang/String;

    .line 191
    :cond_d
    :try_start_7
    iget-object v9, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v10, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->AVAILABLE:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    invoke-interface {v9, v10, v5}, Lcom/navdy/client/ota/OTAUpdateListener;->onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_7

    goto/16 :goto_0

    .line 192
    :catch_7
    move-exception v8

    .line 193
    .restart local v8    # "t":Ljava/lang/Throwable;
    sget-object v9, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Bad listener "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public download(Lcom/navdy/client/ota/model/UpdateInfo;Ljava/io/File;I)V
    .locals 12
    .param p1, "info"    # Lcom/navdy/client/ota/model/UpdateInfo;
    .param p2, "outputFile"    # Ljava/io/File;
    .param p3, "downloadId"    # I

    .prologue
    .line 210
    sget-object v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Downloading the update.."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 212
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 213
    if-eqz p1, :cond_0

    iget v1, p1, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    if-lez v1, :cond_0

    iget-object v1, p1, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 214
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Update information is not valid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :catch_0
    move-exception v11

    .line 332
    .local v11, "t":Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    if-eqz v1, :cond_2

    .line 333
    const-wide/16 v6, 0x0

    .line 334
    .local v6, "size":J
    if-eqz p1, :cond_1

    .line 335
    iget-wide v6, p1, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-wide/16 v4, 0x0

    move v3, p3

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V

    .line 340
    .end local v6    # "size":J
    .end local v11    # "t":Ljava/lang/Throwable;
    :cond_2
    :goto_0
    return-void

    .line 217
    :cond_3
    :try_start_1
    new-instance v1, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    iget-object v2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    iget-object v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;-><init>(Lcom/amazonaws/services/s3/AmazonS3;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    .line 218
    const/4 v10, 0x0

    .line 219
    .local v10, "recordFound":Z
    const/4 v1, -0x1

    if-eq p3, v1, :cond_5

    .line 220
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-virtual {v1, p3}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->getTransferById(I)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    .line 221
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    if-eqz v1, :cond_5

    .line 222
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getState()Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    move-result-object v1

    sget-object v2, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->COMPLETED:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-ne v1, v2, :cond_4

    .line 223
    sget-object v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Download completed successfully"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    move v3, p3

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V

    goto :goto_0

    .line 227
    :cond_4
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getState()Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    move-result-object v1

    sget-object v2, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;->PAUSED:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferState;

    if-ne v1, v2, :cond_5

    .line 228
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-virtual {v1, p3}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->resume(I)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    .line 229
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    if-eqz v1, :cond_5

    .line 230
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getId()I

    move-result p3

    .line 231
    const/4 v10, 0x1

    .line 236
    :cond_5
    if-nez v10, :cond_8

    .line 237
    iget-object v1, p1, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->parseS3Url(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 238
    .local v9, "parsedResult":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v0, v9, v1

    .line 239
    .local v0, "bucket":Ljava/lang/String;
    const/4 v1, 0x1

    aget-object v8, v9, v1

    .line 240
    .local v8, "key":Ljava/lang/String;
    if-eqz v0, :cond_6

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    if-eqz v8, :cond_6

    const-string v1, ""

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 241
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Not a valid url"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 243
    :cond_7
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mTransferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-virtual {v1, v0, v8, p2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->download(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    .line 244
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getId()I

    move-result p3

    .line 246
    .end local v0    # "bucket":Ljava/lang/String;
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "parsedResult":[Ljava/lang/String;
    :cond_8
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mDownloadObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    new-instance v2, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;-><init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;Lcom/navdy/client/ota/model/UpdateInfo;)V

    invoke-virtual {v1, v2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->setTransferListener(Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public uploadToHUD(Ljava/io/File;JLjava/lang/String;)V
    .locals 10
    .param p1, "updateFile"    # Ljava/io/File;
    .param p2, "offset"    # J
    .param p4, "file"    # Ljava/lang/String;

    .prologue
    .line 344
    sget-object v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Uploading the update file to HUD"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 346
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 347
    .local v8, "total":J
    new-instance v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    new-instance v5, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;

    invoke-direct {v5, p0, v8, v9}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;-><init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;J)V

    move-object v2, p1

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;-><init>(Landroid/content/Context;Ljava/io/File;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    iput-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mRemoteFileTransferManager:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .line 405
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUpdateListener:Lcom/navdy/client/ota/OTAUpdateListener;

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const/4 v6, 0x0

    move-wide v2, p2

    move-wide v4, v8

    invoke-interface/range {v0 .. v6}, Lcom/navdy/client/ota/OTAUpdateListener;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V

    .line 408
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mRemoteFileTransferManager:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-virtual {v0, p2, p3}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendFile(J)Z

    .line 412
    :goto_0
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mRemoteFileTransferManager:Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    invoke-virtual {v0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->sendFile()Z

    goto :goto_0
.end method
