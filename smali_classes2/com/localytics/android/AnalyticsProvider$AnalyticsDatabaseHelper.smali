.class Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;
.super Lcom/localytics/android/BaseProvider$LocalyticsDatabaseHelper;
.source "AnalyticsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/localytics/android/AnalyticsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AnalyticsDatabaseHelper"
.end annotation


# instance fields
.field private mScreensFromV2:Lorg/json/JSONArray;


# direct methods
.method constructor <init>(Ljava/lang/String;ILcom/localytics/android/LocalyticsDao;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "version"    # I
    .param p3, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;

    .prologue
    .line 362
    invoke-direct {p0, p1, p2, p3}, Lcom/localytics/android/BaseProvider$LocalyticsDatabaseHelper;-><init>(Ljava/lang/String;ILcom/localytics/android/LocalyticsDao;)V

    .line 363
    return-void
.end method


# virtual methods
.method protected addFirstOpenEventToInfoTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 787
    const-string v0, "ALTER TABLE %s ADD COLUMN %s TEXT;"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "info"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "first_open_event_blob"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 788
    return-void
.end method

.method protected addScreens(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 792
    const-string v4, "CREATE TABLE %s (%s TEXT NOT NULL)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "screens"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "name"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 795
    iget-object v4, p0, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->mScreensFromV2:Lorg/json/JSONArray;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->mScreensFromV2:Lorg/json/JSONArray;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 797
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->mScreensFromV2:Lorg/json/JSONArray;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 799
    const/4 v2, 0x0

    .line 802
    .local v2, "screen":Ljava/lang/String;
    :try_start_0
    iget-object v4, p0, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->mScreensFromV2:Lorg/json/JSONArray;

    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 809
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 811
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 812
    .local v3, "screenValues":Landroid/content/ContentValues;
    const-string v4, "name"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v4, "screens"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 797
    .end local v3    # "screenValues":Landroid/content/ContentValues;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 803
    :catch_0
    move-exception v0

    .line 805
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "Bad data in v2 db. Non-string type in screen flow"

    invoke-static {v4}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 817
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "i":I
    .end local v2    # "screen":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected migrateV2ToV3(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 50
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 390
    const-string v4, "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL, %s INTEGER NOT NULL);"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "events"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "blob"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "upload_format"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 400
    const-string v4, "CREATE TABLE %s (%s TEXT PRIMARY KEY, %s TEXT NOT NULL);"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "identifiers"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "key"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "value"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 408
    const-string v4, "CREATE TABLE %s (%s TEXT PRIMARY KEY, %s TEXT NOT NULL);"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "custom_dimensions"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "custom_dimension_key"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "custom_dimension_value"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 445
    const-string v4, "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT UNIQUE NOT NULL, %s TEXT UNIQUE NOT NULL, %s INTEGER NOT NULL CHECK (%s >= 0), %s INTEGER NOT NULL CHECK(%s IN (%s, %s)), %s INTEGER NOT NULL CHECK(%s IN (%s, %s)), %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER CHECK (%s >= 0), %s INTEGER CHECK (%s >= 0), %s INTEGER NOT NULL CHECK (%s >= 0), %s INTEGER NOT NULL CHECK (%s >= 0), %s TEXT, %s INTEGER);"

    const/16 v5, 0x24

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "info"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "api_key"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "uuid"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "created_time"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "created_time"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "opt_out"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "opt_out"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "0"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "1"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "push_disabled"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    const-string v7, "push_disabled"

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "0"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    const-string v7, "1"

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "sender_id"

    aput-object v7, v5, v6

    const/16 v6, 0xf

    const-string v7, "registration_id"

    aput-object v7, v5, v6

    const/16 v6, 0x10

    const-string v7, "registration_version"

    aput-object v7, v5, v6

    const/16 v6, 0x11

    const-string v7, "customer_id"

    aput-object v7, v5, v6

    const/16 v6, 0x12

    const-string v7, "user_type"

    aput-object v7, v5, v6

    const/16 v6, 0x13

    const-string v7, "fb_attribution"

    aput-object v7, v5, v6

    const/16 v6, 0x14

    const-string v7, "play_attribution"

    aput-object v7, v5, v6

    const/16 v6, 0x15

    const-string v7, "first_android_id"

    aput-object v7, v5, v6

    const/16 v6, 0x16

    const-string v7, "first_advertising_id"

    aput-object v7, v5, v6

    const/16 v6, 0x17

    const-string v7, "package_name"

    aput-object v7, v5, v6

    const/16 v6, 0x18

    const-string v7, "app_version"

    aput-object v7, v5, v6

    const/16 v6, 0x19

    const-string v7, "current_session_uuid"

    aput-object v7, v5, v6

    const/16 v6, 0x1a

    const-string v7, "last_session_open_time"

    aput-object v7, v5, v6

    const/16 v6, 0x1b

    const-string v7, "last_session_open_time"

    aput-object v7, v5, v6

    const/16 v6, 0x1c

    const-string v7, "last_session_close_time"

    aput-object v7, v5, v6

    const/16 v6, 0x1d

    const-string v7, "last_session_close_time"

    aput-object v7, v5, v6

    const/16 v6, 0x1e

    const-string v7, "next_session_number"

    aput-object v7, v5, v6

    const/16 v6, 0x1f

    const-string v7, "next_session_number"

    aput-object v7, v5, v6

    const/16 v6, 0x20

    const-string v7, "next_header_number"

    aput-object v7, v5, v6

    const/16 v6, 0x21

    const-string v7, "next_header_number"

    aput-object v7, v5, v6

    const/16 v6, 0x22

    const-string v7, "queued_close_session_blob"

    aput-object v7, v5, v6

    const/16 v6, 0x23

    const-string v7, "queued_close_session_blob_upload_format"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 496
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v4, :cond_f

    .line 498
    sget-object v49, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    monitor-enter v49

    .line 500
    const/16 v25, 0x0

    .line 501
    .local v25, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v48, Landroid/content/ContentValues;

    invoke-direct/range {v48 .. v48}, Landroid/content/ContentValues;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 503
    .local v48, "values":Landroid/content/ContentValues;
    const/4 v12, 0x0

    .line 504
    .local v12, "apiKey":Ljava/lang/String;
    const/16 v46, 0x0

    .line 505
    .local v46, "uuid":Ljava/lang/String;
    const-wide/16 v20, 0x0

    .line 506
    .local v20, "createdTime":J
    const/16 v42, 0x0

    .line 507
    .local v42, "optedOut":Z
    const-wide/16 v30, 0x1

    .line 508
    .local v30, "headerNumber":J
    const-wide/16 v44, 0x1

    .line 509
    .local v44, "sessionNumber":J
    const/16 v37, 0x0

    .line 510
    .local v37, "newestCloseSessionTag":Ljava/lang/String;
    const-wide/16 v40, 0x0

    .line 511
    .local v40, "newestCloseSessionTime":J
    const/16 v24, 0x0

    .line 512
    .local v24, "currentSessionUUID":Ljava/lang/String;
    const/16 v26, 0x0

    .line 513
    .local v26, "customerID":Ljava/lang/String;
    const/16 v43, 0x0

    .line 517
    .local v43, "screenFlow":Lorg/json/JSONArray;
    :try_start_1
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "api_keys"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 518
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 520
    const-string v4, "api_key"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 521
    const-string v4, "uuid"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v46

    .line 522
    const-string v4, "created_time"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 523
    const-string v4, "opt_out"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const-string v5, "1"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    if-ne v4, v5, :cond_4

    const/16 v42, 0x1

    .line 528
    :cond_0
    :goto_0
    if-eqz v25, :cond_1

    .line 530
    :try_start_2
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 531
    const/16 v25, 0x0

    .line 537
    :cond_1
    :try_start_3
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "identifiers"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 538
    :goto_1
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 540
    const-string v4, "key"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 541
    .local v36, "key":Ljava/lang/String;
    const-string v4, "value"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v47

    .line 542
    .local v47, "value":Ljava/lang/String;
    const-string v4, "customer_id"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 543
    move-object/from16 v26, v47

    .line 545
    :cond_2
    const-string v4, "key"

    move-object/from16 v0, v48

    move-object/from16 v1, v36

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    const-string v4, "value"

    move-object/from16 v0, v48

    move-object/from16 v1, v47

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const-string v4, "identifiers"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 548
    invoke-virtual/range {v48 .. v48}, Landroid/content/ContentValues;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 553
    .end local v36    # "key":Ljava/lang/String;
    .end local v47    # "value":Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-eqz v25, :cond_3

    .line 555
    :try_start_4
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 556
    const/16 v25, 0x0

    :cond_3
    throw v4

    .line 744
    .end local v12    # "apiKey":Ljava/lang/String;
    .end local v20    # "createdTime":J
    .end local v24    # "currentSessionUUID":Ljava/lang/String;
    .end local v26    # "customerID":Ljava/lang/String;
    .end local v30    # "headerNumber":J
    .end local v37    # "newestCloseSessionTag":Ljava/lang/String;
    .end local v40    # "newestCloseSessionTime":J
    .end local v42    # "optedOut":Z
    .end local v43    # "screenFlow":Lorg/json/JSONArray;
    .end local v44    # "sessionNumber":J
    .end local v46    # "uuid":Ljava/lang/String;
    .end local v48    # "values":Landroid/content/ContentValues;
    :catchall_1
    move-exception v4

    monitor-exit v49
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    .line 523
    .restart local v12    # "apiKey":Ljava/lang/String;
    .restart local v20    # "createdTime":J
    .restart local v24    # "currentSessionUUID":Ljava/lang/String;
    .restart local v26    # "customerID":Ljava/lang/String;
    .restart local v30    # "headerNumber":J
    .restart local v37    # "newestCloseSessionTag":Ljava/lang/String;
    .restart local v40    # "newestCloseSessionTime":J
    .restart local v42    # "optedOut":Z
    .restart local v43    # "screenFlow":Lorg/json/JSONArray;
    .restart local v44    # "sessionNumber":J
    .restart local v46    # "uuid":Ljava/lang/String;
    .restart local v48    # "values":Landroid/content/ContentValues;
    :cond_4
    const/16 v42, 0x0

    goto :goto_0

    .line 528
    :catchall_2
    move-exception v4

    if-eqz v25, :cond_5

    .line 530
    :try_start_5
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 531
    const/16 v25, 0x0

    :cond_5
    throw v4

    .line 553
    :cond_6
    if-eqz v25, :cond_7

    .line 555
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 556
    const/16 v25, 0x0

    .line 562
    :cond_7
    :try_start_6
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "custom_dimensions"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 563
    :goto_2
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 565
    const-string v4, "custom_dimension_key"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 566
    .restart local v36    # "key":Ljava/lang/String;
    const-string v4, "custom_dimension_key"

    const-string v5, "com.localytics.android:"

    const-string v6, ""

    move-object/from16 v0, v36

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v4, "custom_dimension_value"

    const-string v5, "custom_dimension_value"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const-string v4, "custom_dimensions"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 569
    invoke-virtual/range {v48 .. v48}, Landroid/content/ContentValues;->clear()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_2

    .line 574
    .end local v36    # "key":Ljava/lang/String;
    :catchall_3
    move-exception v4

    if-eqz v25, :cond_8

    .line 576
    :try_start_7
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 577
    const/16 v25, 0x0

    :cond_8
    throw v4

    .line 574
    :cond_9
    if-eqz v25, :cond_a

    .line 576
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 577
    const/16 v25, 0x0

    .line 581
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v13

    .line 582
    .local v13, "appContext":Landroid/content/Context;
    if-eqz v12, :cond_b

    .line 584
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v4}, Lcom/localytics/android/MigrationDatabaseHelper;->preUploadBuildBlobs(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 586
    const/16 v28, 0x0

    .line 589
    .local v28, "headerBlob":Lorg/json/JSONObject;
    :goto_3
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v13, v4, v12}, Lcom/localytics/android/MigrationDatabaseHelper;->convertDatabaseToJson(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v27

    .line 590
    .local v27, "events":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->isEmpty()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v4

    if-eqz v4, :cond_10

    .line 685
    .end local v27    # "events":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    .end local v28    # "headerBlob":Lorg/json/JSONObject;
    :cond_b
    :try_start_8
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "info"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 686
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 688
    const-string v4, "api_key"

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const-string v4, "uuid"

    move-object/from16 v0, v48

    move-object/from16 v1, v46

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    const-string v4, "created_time"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 691
    const-string v4, "opt_out"

    invoke-static/range {v42 .. v42}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 692
    const-string v4, "push_disabled"

    const-string v5, "push_disabled"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 693
    const-string v4, "sender_id"

    const-string v5, "sender_id"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const-string v4, "registration_id"

    const-string v5, "registration_id"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    const-string v4, "registration_version"

    const-string v5, "registration_version"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    if-eqz v26, :cond_18

    .line 698
    const-string v4, "customer_id"

    move-object/from16 v0, v48

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    const-string v4, "user_type"

    const-string v5, "known"

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :goto_4
    const-string v4, "fb_attribution"

    const-string v5, "fb_attribution"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const-string v4, "play_attribution"

    const-string v5, "play_attribution"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    const-string v4, "first_android_id"

    const-string v5, "first_android_id"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    const-string v4, "first_advertising_id"

    const-string v5, "first_advertising_id"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const-string v4, "app_version"

    invoke-static {v13}, Lcom/localytics/android/DatapointHelper;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    const-string v4, "package_name"

    const-string v5, "package_name"

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    const-string v4, "current_session_uuid"

    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    const-string v4, "last_session_open_time"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v38

    .line 714
    .local v38, "lastSessionOpenTime":J
    const-string v4, "last_session_open_time"

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 715
    const-string v4, "last_session_close_time"

    invoke-static/range {v40 .. v41}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 716
    const-string v4, "next_header_number"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 717
    const-string v4, "next_session_number"

    const-wide/16 v6, 0x1

    add-long v6, v6, v44

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 718
    cmp-long v4, v40, v38

    if-lez v4, :cond_1a

    .line 720
    const-string v4, "queued_close_session_blob"

    move-object/from16 v0, v48

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    const-string v4, "queued_close_session_blob_upload_format"

    sget-object v5, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v5}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 722
    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->mScreensFromV2:Lorg/json/JSONArray;

    .line 731
    :cond_c
    :goto_5
    const-string v4, "info"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 732
    invoke-virtual/range {v48 .. v48}, Landroid/content/ContentValues;->clear()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 737
    .end local v38    # "lastSessionOpenTime":J
    :cond_d
    if-eqz v25, :cond_e

    .line 739
    :try_start_9
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 743
    :cond_e
    invoke-static {}, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->cleanUpOldDB()V

    .line 744
    monitor-exit v49

    .line 746
    .end local v12    # "apiKey":Ljava/lang/String;
    .end local v13    # "appContext":Landroid/content/Context;
    .end local v20    # "createdTime":J
    .end local v24    # "currentSessionUUID":Ljava/lang/String;
    .end local v25    # "cursor":Landroid/database/Cursor;
    .end local v26    # "customerID":Ljava/lang/String;
    .end local v30    # "headerNumber":J
    .end local v37    # "newestCloseSessionTag":Ljava/lang/String;
    .end local v40    # "newestCloseSessionTime":J
    .end local v42    # "optedOut":Z
    .end local v43    # "screenFlow":Lorg/json/JSONArray;
    .end local v44    # "sessionNumber":J
    .end local v46    # "uuid":Ljava/lang/String;
    .end local v48    # "values":Landroid/content/ContentValues;
    :cond_f
    return-void

    .line 594
    .restart local v12    # "apiKey":Ljava/lang/String;
    .restart local v13    # "appContext":Landroid/content/Context;
    .restart local v20    # "createdTime":J
    .restart local v24    # "currentSessionUUID":Ljava/lang/String;
    .restart local v25    # "cursor":Landroid/database/Cursor;
    .restart local v26    # "customerID":Ljava/lang/String;
    .restart local v27    # "events":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    .restart local v28    # "headerBlob":Lorg/json/JSONObject;
    .restart local v30    # "headerNumber":J
    .restart local v37    # "newestCloseSessionTag":Ljava/lang/String;
    .restart local v40    # "newestCloseSessionTime":J
    .restart local v42    # "optedOut":Z
    .restart local v43    # "screenFlow":Lorg/json/JSONArray;
    .restart local v44    # "sessionNumber":J
    .restart local v46    # "uuid":Ljava/lang/String;
    .restart local v48    # "values":Landroid/content/ContentValues;
    :cond_10
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 595
    .local v15, "builder":Ljava/lang/StringBuilder;
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    .local v29, "i$":Ljava/util/Iterator;
    move-wide/from16 v32, v30

    .end local v30    # "headerNumber":J
    .local v32, "headerNumber":J
    :goto_6
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_17

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lorg/json/JSONObject;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 599
    .local v35, "json":Lorg/json/JSONObject;
    :try_start_a
    const-string v4, "dt"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "h"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 601
    if-nez v28, :cond_1b

    .line 603
    const-string v4, "seq"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v30

    .line 605
    .end local v32    # "headerNumber":J
    .restart local v30    # "headerNumber":J
    :goto_7
    move-object/from16 v28, v35

    :goto_8
    move-wide/from16 v32, v30

    .line 679
    .end local v30    # "headerNumber":J
    .restart local v32    # "headerNumber":J
    goto :goto_6

    .line 609
    :cond_11
    const-string v4, "seq"
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    const-wide/16 v6, 0x1

    add-long v30, v32, v6

    .end local v32    # "headerNumber":J
    .restart local v30    # "headerNumber":J
    :try_start_b
    move-object/from16 v0, v28

    move-wide/from16 v1, v32

    invoke-virtual {v0, v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 610
    const-string v4, "u"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 612
    const-string v4, "attrs"

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 613
    .local v14, "attributes":Lorg/json/JSONObject;
    const-string v4, "lpg"

    invoke-static {v13}, Lcom/localytics/android/DatapointHelper;->isLocationPermissionGranted(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v14, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 615
    invoke-virtual/range {v28 .. v28}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v35 .. v35}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    const-string v4, "dt"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "c"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 618
    const/16 v34, 0x0

    .line 619
    .local v34, "isMostRecentClose":Z
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 620
    .local v19, "currentCloseSessionTag":Ljava/lang/String;
    const-string v4, "u"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 621
    .local v18, "closeUUID":Ljava/lang/String;
    sget-object v4, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->oldDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/localytics/android/MigrationDatabaseHelper;->wallTimeForEvent(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v22

    .line 625
    .local v22, "currentCloseSessionWallTimeMillis":J
    if-eqz v37, :cond_14

    .line 629
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 630
    .local v17, "closeSessionContentValues":Landroid/content/ContentValues;
    const-string v4, "upload_format"

    sget-object v5, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v5}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 633
    cmp-long v4, v22, v40

    if-lez v4, :cond_13

    .line 635
    const-string v4, "blob"

    move-object/from16 v0, v17

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 639
    move-object/from16 v37, v19

    .line 640
    move-wide/from16 v40, v22

    .line 641
    const/16 v34, 0x1

    .line 656
    .end local v17    # "closeSessionContentValues":Landroid/content/ContentValues;
    :goto_9
    if-eqz v34, :cond_12

    const-string v4, "fl"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 658
    const-string v4, "fl"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v43

    .line 672
    .end local v18    # "closeUUID":Ljava/lang/String;
    .end local v19    # "currentCloseSessionTag":Ljava/lang/String;
    .end local v22    # "currentCloseSessionWallTimeMillis":J
    .end local v34    # "isMostRecentClose":Z
    :cond_12
    :goto_a
    invoke-virtual/range {v48 .. v48}, Landroid/content/ContentValues;->clear()V

    .line 673
    const/4 v4, 0x0

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 676
    .end local v14    # "attributes":Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    goto/16 :goto_8

    .line 644
    .restart local v14    # "attributes":Lorg/json/JSONObject;
    .restart local v17    # "closeSessionContentValues":Landroid/content/ContentValues;
    .restart local v18    # "closeUUID":Ljava/lang/String;
    .restart local v19    # "currentCloseSessionTag":Ljava/lang/String;
    .restart local v22    # "currentCloseSessionWallTimeMillis":J
    .restart local v34    # "isMostRecentClose":Z
    :cond_13
    const-string v4, "blob"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_9

    .line 650
    .end local v17    # "closeSessionContentValues":Landroid/content/ContentValues;
    :cond_14
    move-object/from16 v37, v19

    .line 651
    move-wide/from16 v40, v22

    .line 652
    const/16 v34, 0x1

    goto :goto_9

    .line 663
    .end local v18    # "closeUUID":Ljava/lang/String;
    .end local v19    # "currentCloseSessionTag":Ljava/lang/String;
    .end local v22    # "currentCloseSessionWallTimeMillis":J
    .end local v34    # "isMostRecentClose":Z
    :cond_15
    const-string v4, "dt"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 665
    const-string v4, "nth"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v44

    .line 666
    const-string v4, "u"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 668
    :cond_16
    const-string v4, "blob"

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    const-string v4, "upload_format"

    sget-object v5, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v5}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 670
    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_a

    .end local v14    # "attributes":Lorg/json/JSONObject;
    .end local v30    # "headerNumber":J
    .end local v35    # "json":Lorg/json/JSONObject;
    .restart local v32    # "headerNumber":J
    :cond_17
    move-wide/from16 v30, v32

    .line 680
    .end local v32    # "headerNumber":J
    .restart local v30    # "headerNumber":J
    goto/16 :goto_3

    .line 703
    .end local v15    # "builder":Ljava/lang/StringBuilder;
    .end local v27    # "events":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    .end local v28    # "headerBlob":Lorg/json/JSONObject;
    .end local v29    # "i$":Ljava/util/Iterator;
    :cond_18
    :try_start_c
    const-string v4, "customer_id"

    move-object/from16 v0, v48

    move-object/from16 v1, v46

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const-string v4, "user_type"

    const-string v5, "anonymous"

    move-object/from16 v0, v48

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    goto/16 :goto_4

    .line 737
    :catchall_4
    move-exception v4

    if-eqz v25, :cond_19

    .line 739
    :try_start_d
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_19
    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 724
    .restart local v38    # "lastSessionOpenTime":J
    :cond_1a
    if-eqz v37, :cond_c

    .line 726
    :try_start_e
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 727
    .local v16, "closeEventTagValues":Landroid/content/ContentValues;
    const-string v4, "blob"

    move-object/from16 v0, v16

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    const-string v4, "upload_format"

    sget-object v5, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->V2:Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;

    invoke-virtual {v5}, Lcom/localytics/android/AnalyticsProvider$EventsV3Columns$UploadFormat;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 729
    const-string v4, "events"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v5, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto/16 :goto_5

    .line 676
    .end local v16    # "closeEventTagValues":Landroid/content/ContentValues;
    .end local v30    # "headerNumber":J
    .end local v38    # "lastSessionOpenTime":J
    .restart local v15    # "builder":Ljava/lang/StringBuilder;
    .restart local v27    # "events":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    .restart local v28    # "headerBlob":Lorg/json/JSONObject;
    .restart local v29    # "i$":Ljava/util/Iterator;
    .restart local v32    # "headerNumber":J
    .restart local v35    # "json":Lorg/json/JSONObject;
    :catch_1
    move-exception v4

    move-wide/from16 v30, v32

    .end local v32    # "headerNumber":J
    .restart local v30    # "headerNumber":J
    goto/16 :goto_8

    .end local v30    # "headerNumber":J
    .restart local v32    # "headerNumber":J
    :cond_1b
    move-wide/from16 v30, v32

    .end local v32    # "headerNumber":J
    .restart local v30    # "headerNumber":J
    goto/16 :goto_7
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 377
    if-nez p1, :cond_0

    .line 379
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "db cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_0
    const-string v0, "PRAGMA auto_vacuum = INCREMENTAL;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 383
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, p1, v0, v1}, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 384
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 770
    invoke-super {p0, p1}, Lcom/localytics/android/BaseProvider$LocalyticsDatabaseHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 772
    const-string v0, "SQLite library version is: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "select sqlite_version()"

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 774
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 779
    const-string v0, "PRAGMA foreign_keys = ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 781
    :cond_0
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 751
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 753
    invoke-virtual {p0, p1}, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->migrateV2ToV3(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 756
    :cond_0
    const/4 v0, 0x2

    if-ge p2, v0, :cond_1

    .line 758
    invoke-virtual {p0, p1}, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->addFirstOpenEventToInfoTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 761
    :cond_1
    const/4 v0, 0x3

    if-ge p2, v0, :cond_2

    .line 763
    invoke-virtual {p0, p1}, Lcom/localytics/android/AnalyticsProvider$AnalyticsDatabaseHelper;->addScreens(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 765
    :cond_2
    return-void
.end method
