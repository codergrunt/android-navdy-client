.class public Lcom/vividsolutions/jts/geom/CoordinateSequences;
.super Ljava/lang/Object;
.source "CoordinateSequences.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copy(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;II)V
    .locals 3
    .param p0, "src"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p1, "srcPos"    # I
    .param p2, "dest"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p3, "destPos"    # I
    .param p4, "length"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 86
    add-int v1, p1, v0

    add-int v2, p3, v0

    invoke-static {p0, v1, p2, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->copyCoord(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;I)V

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_0
    return-void
.end method

.method public static copyCoord(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 4
    .param p0, "src"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p1, "srcPos"    # I
    .param p2, "dest"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p3, "destPos"    # I

    .prologue
    .line 101
    const/4 v0, 0x0

    .local v0, "dim":I
    :goto_0
    invoke-interface {p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getDimension()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 102
    invoke-interface {p0, p1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v2

    invoke-interface {p2, p3, v0, v2, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_0
    return-void
.end method

.method private static createClosedRing(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 5
    .param p0, "fact"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 158
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getDimension()I

    move-result v3

    invoke-interface {p0, p2, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create(II)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    .line 159
    .local v2, "newseq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    .line 160
    .local v1, "n":I
    invoke-static {p1, v4, v2, v4, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->copy(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;II)V

    .line 162
    move v0, v1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 163
    const/4 v3, 0x1

    invoke-static {p1, v4, v2, v0, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->copy(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;II)V

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164
    :cond_0
    return-object v2
.end method

.method public static ensureValidRing(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 8
    .param p0, "fact"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 142
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    .line 144
    .local v1, "n":I
    if-nez v1, :cond_1

    .line 153
    .end local p1    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_0
    :goto_0
    return-object p1

    .line 146
    .restart local p1    # "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    :cond_1
    const/4 v3, 0x3

    if-gt v1, v3, :cond_2

    .line 147
    const/4 v2, 0x4

    invoke-static {p0, p1, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->createClosedRing(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object p1

    goto :goto_0

    .line 149
    :cond_2
    invoke-interface {p1, v2, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-int/lit8 v3, v1, -0x1

    invoke-interface {p1, v3, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    invoke-interface {p1, v2, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-int/lit8 v3, v1, -0x1

    invoke-interface {p1, v3, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    .line 151
    .local v0, "isClosed":Z
    :goto_1
    if-nez v0, :cond_0

    .line 153
    add-int/lit8 v2, v1, 0x1

    invoke-static {p0, p1, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->createClosedRing(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object p1

    goto :goto_0

    .end local v0    # "isClosed":Z
    :cond_3
    move v0, v2

    .line 149
    goto :goto_1
.end method

.method public static extend(Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 5
    .param p0, "fact"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 169
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getDimension()I

    move-result v3

    invoke-interface {p0, p2, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create(II)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    .line 170
    .local v2, "newseq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    .line 171
    .local v1, "n":I
    invoke-static {p1, v4, v2, v4, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->copy(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;II)V

    .line 173
    if-lez v1, :cond_0

    .line 174
    move v0, v1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 175
    add-int/lit8 v3, v1, -0x1

    const/4 v4, 0x1

    invoke-static {p1, v3, v2, v0, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->copy(Lcom/vividsolutions/jts/geom/CoordinateSequence;ILcom/vividsolutions/jts/geom/CoordinateSequence;II)V

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    .end local v0    # "i":I
    :cond_0
    return-object v2
.end method

.method public static isRing(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Z
    .locals 8
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    invoke-interface {p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v0

    .line 119
    .local v0, "n":I
    if-nez v0, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    const/4 v3, 0x3

    if-gt v0, v3, :cond_2

    move v1, v2

    .line 122
    goto :goto_0

    .line 124
    :cond_2
    invoke-interface {p0, v2, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-int/lit8 v3, v0, -0x1

    invoke-interface {p0, v3, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-nez v3, :cond_3

    invoke-interface {p0, v2, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-int/lit8 v3, v0, -0x1

    invoke-interface {p0, v3, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v6

    cmpl-double v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public static reverse(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V
    .locals 4
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 49
    invoke-interface {p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    .line 50
    .local v1, "last":I
    div-int/lit8 v2, v1, 0x2

    .line 51
    .local v2, "mid":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-gt v0, v2, :cond_0

    .line 52
    sub-int v3, v1, v0

    invoke-static {p0, v0, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->swap(Lcom/vividsolutions/jts/geom/CoordinateSequence;II)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public static swap(Lcom/vividsolutions/jts/geom/CoordinateSequence;II)V
    .locals 6
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 65
    if-ne p1, p2, :cond_1

    .line 71
    :cond_0
    return-void

    .line 66
    :cond_1
    const/4 v0, 0x0

    .local v0, "dim":I
    :goto_0
    invoke-interface {p0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getDimension()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 67
    invoke-interface {p0, p1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v2

    .line 68
    .local v2, "tmp":D
    invoke-interface {p0, p2, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    invoke-interface {p0, p1, v0, v4, v5}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 69
    invoke-interface {p0, p2, v0, v2, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
