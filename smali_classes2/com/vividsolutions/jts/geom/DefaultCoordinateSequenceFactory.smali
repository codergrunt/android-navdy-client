.class public Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;
.super Ljava/lang/Object;
.source "DefaultCoordinateSequenceFactory.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;
.implements Ljava/io/Serializable;


# static fields
.field private static final instanceObject:Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;

.field private static final serialVersionUID:J = -0x38e49fa6cf6f2ea9L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;->instanceObject:Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method public static instance()Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;->instanceObject:Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;->instance()Lcom/vividsolutions/jts/geom/DefaultCoordinateSequenceFactory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public create(II)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1
    .param p1, "size"    # I
    .param p2, "dimension"    # I

    .prologue
    .line 91
    new-instance v0, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequence;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequence;-><init>(I)V

    return-object v0
.end method

.method public create(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1
    .param p1, "coordSeq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 84
    new-instance v0, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequence;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequence;-><init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V

    return-object v0
.end method

.method public create([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 77
    new-instance v0, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequence;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/DefaultCoordinateSequence;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v0
.end method
