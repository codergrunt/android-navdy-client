.class public interface abstract Lcom/vividsolutions/jts/geom/prep/PreparedGeometry;
.super Ljava/lang/Object;
.source "PreparedGeometry.java"


# virtual methods
.method public abstract contains(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract containsProperly(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract coveredBy(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract covers(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract crosses(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract disjoint(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
.end method

.method public abstract intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract overlaps(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract touches(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method

.method public abstract within(Lcom/vividsolutions/jts/geom/Geometry;)Z
.end method
