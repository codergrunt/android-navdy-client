.class public Lcom/vividsolutions/jts/geom/prep/PreparedPoint;
.super Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;
.source "PreparedPoint.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Puntal;)V
    .locals 0
    .param p1, "point"    # Lcom/vividsolutions/jts/geom/Puntal;

    .prologue
    .line 50
    check-cast p1, Lcom/vividsolutions/jts/geom/Geometry;

    .end local p1    # "point":Lcom/vividsolutions/jts/geom/Puntal;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 51
    return-void
.end method


# virtual methods
.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPoint;->envelopesIntersect(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPoint;->isAnyTargetComponentInTest(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method
