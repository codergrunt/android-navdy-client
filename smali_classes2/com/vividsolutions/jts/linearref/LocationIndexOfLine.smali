.class Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;
.super Ljava/lang/Object;
.source "LocationIndexOfLine.java"


# instance fields
.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 64
    return-void
.end method

.method public static indicesOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 2
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "subLine"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 56
    new-instance v0, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 57
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;->indicesOf(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public indicesOf(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 12
    .param p1, "subLine"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 68
    invoke-virtual {p1, v10}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v5, v10}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 69
    .local v3, "startPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p1, v5}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 70
    .local v1, "lastLine":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v5}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 72
    .local v0, "endPt":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;

    iget-object v5, p0, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v2, v5}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 73
    .local v2, "locPt":Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;
    const/4 v5, 0x2

    new-array v4, v5, [Lcom/vividsolutions/jts/linearref/LinearLocation;

    .line 74
    .local v4, "subLineLoc":[Lcom/vividsolutions/jts/linearref/LinearLocation;
    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v5

    aput-object v5, v4, v10

    .line 77
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmpl-double v5, v6, v8

    if-nez v5, :cond_0

    .line 78
    aget-object v5, v4, v10

    invoke-virtual {v5}, Lcom/vividsolutions/jts/linearref/LinearLocation;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/linearref/LinearLocation;

    aput-object v5, v4, v11

    .line 83
    :goto_0
    return-object v4

    .line 81
    :cond_0
    aget-object v5, v4, v10

    invoke-virtual {v2, v0, v5}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v5

    aput-object v5, v4, v11

    goto :goto_0
.end method
