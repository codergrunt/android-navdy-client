.class public Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;
.super Ljava/lang/Object;
.source "LinearGeometryBuilder.java"


# instance fields
.field private coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

.field private fixInvalidLines:Z

.field private geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private ignoreInvalidLines:Z

.field private lastPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private lines:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 3
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->lines:Ljava/util/List;

    .line 49
    iput-object v2, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    .line 51
    iput-boolean v1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->ignoreInvalidLines:Z

    .line 52
    iput-boolean v1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->fixInvalidLines:Z

    .line 54
    iput-object v2, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->lastPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 57
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 58
    return-void
.end method

.method private validCoordinateSequence([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 141
    array-length v1, p1

    if-lt v1, v3, :cond_0

    .line 143
    .end local p1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object p1

    .line 142
    .restart local p1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    new-array v0, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aget-object v2, p1, v2

    aput-object v2, v0, v1

    .local v0, "validPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object p1, v0

    .line 143
    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 90
    return-void
.end method

.method public add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "allowRepeatedPoints"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 102
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->lastPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 103
    return-void
.end method

.method public endLine()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 112
    iget-object v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    if-nez v4, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-boolean v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->ignoreInvalidLines:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_2

    .line 116
    iput-object v6, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    goto :goto_0

    .line 119
    :cond_2
    iget-object v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 120
    .local v3, "rawPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object v2, v3

    .line 121
    .local v2, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-boolean v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->fixInvalidLines:Z

    if-eqz v4, :cond_3

    .line 122
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->validCoordinateSequence([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 124
    :cond_3
    iput-object v6, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    .line 125
    const/4 v1, 0x0

    .line 127
    .local v1, "line":Lcom/vividsolutions/jts/geom/LineString;
    :try_start_0
    iget-object v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 136
    :cond_4
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->lines:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 132
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    iget-boolean v4, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->ignoreInvalidLines:Z

    if-nez v4, :cond_4

    .line 133
    throw v0
.end method

.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->endLine()V

    .line 150
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->lines:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public getLastCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->lastPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public setFixInvalidLines(Z)V
    .locals 0
    .param p1, "fixInvalidLines"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->fixInvalidLines:Z

    .line 80
    return-void
.end method

.method public setIgnoreInvalidLines(Z)V
    .locals 0
    .param p1, "ignoreInvalidLines"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->ignoreInvalidLines:Z

    .line 69
    return-void
.end method
