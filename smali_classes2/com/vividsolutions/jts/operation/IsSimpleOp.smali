.class public Lcom/vividsolutions/jts/operation/IsSimpleOp;
.super Ljava/lang/Object;
.source "IsSimpleOp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
    }
.end annotation


# instance fields
.field private inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private isClosedEndpointsInInterior:Z

.field private nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isClosedEndpointsInInterior:Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 97
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isClosedEndpointsInInterior:Z

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 105
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 106
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    const/4 v0, 0x1

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isClosedEndpointsInInterior:Z

    .line 89
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 116
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 117
    const/4 v1, 0x2

    invoke-interface {p2, v1}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->isInBoundary(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isClosedEndpointsInInterior:Z

    .line 118
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private addEndpoint(Ljava/util/Map;Lcom/vividsolutions/jts/geom/Coordinate;Z)V
    .locals 1
    .param p1, "endPoints"    # Ljava/util/Map;
    .param p2, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "isClosed"    # Z

    .prologue
    .line 339
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;

    .line 340
    .local v0, "eiInfo":Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
    if-nez v0, :cond_0

    .line 341
    new-instance v0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;

    .end local v0    # "eiInfo":Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
    invoke-direct {v0, p2}, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 342
    .restart local v0    # "eiInfo":Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
    invoke-interface {p1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    :cond_0
    invoke-virtual {v0, p3}, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->addEndpoint(Z)V

    .line 345
    return-void
.end method

.method private computeSimple(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x1

    .line 133
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 134
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return v0

    .line 135
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleLinearGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 136
    :cond_2
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v1, :cond_3

    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleLinearGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 137
    :cond_3
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleMultiPoint(Lcom/vividsolutions/jts/geom/MultiPoint;)Z

    move-result v0

    goto :goto_0

    .line 138
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v1, :cond_5

    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimplePolygonal(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 139
    :cond_5
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method

.method private hasClosedEndpointIntersection(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Z
    .locals 11
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    const/4 v8, 0x0

    .line 313
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 314
    .local v2, "endPoints":Ljava/util/Map;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 315
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 316
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getMaximumSegmentIndex()I

    move-result v5

    .line 317
    .local v5, "maxSegmentIndex":I
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->isClosed()Z

    move-result v4

    .line 318
    .local v4, "isClosed":Z
    invoke-virtual {v0, v8}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    .line 319
    .local v6, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v2, v6, v4}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->addEndpoint(Ljava/util/Map;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 320
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getNumPoints()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v0, v9}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 321
    .local v7, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v2, v7, v4}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->addEndpoint(Ljava/util/Map;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    goto :goto_0

    .line 324
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v4    # "isClosed":Z
    .end local v5    # "maxSegmentIndex":I
    .end local v6    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v7    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 325
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;

    .line 326
    .local v1, "eiInfo":Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
    iget-boolean v9, v1, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->isClosed:Z

    if-eqz v9, :cond_1

    iget v9, v1, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->degree:I

    const/4 v10, 0x2

    if-eq v9, v10, :cond_1

    .line 327
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    iput-object v8, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 328
    const/4 v8, 0x1

    .line 331
    .end local v1    # "eiInfo":Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
    :cond_2
    return v8
.end method

.method private hasNonEndpointIntersection(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Z
    .locals 6
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 268
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 269
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 270
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getMaximumSegmentIndex()I

    move-result v4

    .line 271
    .local v4, "maxSegmentIndex":I
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "eiIt":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 272
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 273
    .local v1, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    invoke-virtual {v1, v4}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->isEndPoint(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 274
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 275
    const/4 v5, 0x1

    .line 279
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .end local v2    # "eiIt":Ljava/util/Iterator;
    .end local v4    # "maxSegmentIndex":I
    :goto_0
    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private isSimpleGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 235
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 236
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 237
    .local v0, "comp":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->computeSimple(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 238
    const/4 v2, 0x0

    .line 240
    .end local v0    # "comp":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_1
    return v2

    .line 235
    .restart local v0    # "comp":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    .end local v0    # "comp":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private isSimpleLinearGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 6
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 245
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 259
    :cond_0
    :goto_0
    return v3

    .line 246
    :cond_1
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-direct {v0, v4, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;)V

    .line 247
    .local v0, "graph":Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    new-instance v1, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v1}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    .line 248
    .local v1, "li":Lcom/vividsolutions/jts/algorithm/LineIntersector;
    invoke-virtual {v0, v1, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    move-result-object v2

    .line 250
    .local v2, "si":Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasIntersection()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 251
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperIntersection()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 252
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->getProperIntersectionPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    iput-object v3, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    move v3, v4

    .line 253
    goto :goto_0

    .line 255
    :cond_2
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->hasNonEndpointIntersection(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    goto :goto_0

    .line 256
    :cond_3
    iget-boolean v5, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isClosedEndpointsInInterior:Z

    if-eqz v5, :cond_0

    .line 257
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->hasClosedEndpointIntersection(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private isSimpleMultiPoint(Lcom/vividsolutions/jts/geom/MultiPoint;)Z
    .locals 6
    .param p1, "mp"    # Lcom/vividsolutions/jts/geom/MultiPoint;

    .prologue
    const/4 v4, 0x1

    .line 193
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPoint;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v4

    .line 194
    :cond_1
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 195
    .local v2, "points":Ljava/util/Set;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPoint;->getNumGeometries()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 196
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/MultiPoint;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Point;

    .line 197
    .local v3, "pt":Lcom/vividsolutions/jts/geom/Point;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 198
    .local v1, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 199
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 200
    const/4 v4, 0x0

    goto :goto_0

    .line 202
    :cond_2
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private isSimplePolygonal(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 217
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 218
    .local v2, "rings":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 219
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 220
    .local v1, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleLinearGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 221
    const/4 v3, 0x0

    .line 223
    .end local v1    # "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getNonSimpleLocation()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public isSimple()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->nonSimpleLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 128
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->computeSimple(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public isSimple(Lcom/vividsolutions/jts/geom/LineString;)Z
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleLinearGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public isSimple(Lcom/vividsolutions/jts/geom/MultiLineString;)Z
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/MultiLineString;

    .prologue
    .line 179
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleLinearGeometry(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public isSimple(Lcom/vividsolutions/jts/geom/MultiPoint;)Z
    .locals 1
    .param p1, "mp"    # Lcom/vividsolutions/jts/geom/MultiPoint;

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimpleMultiPoint(Lcom/vividsolutions/jts/geom/MultiPoint;)Z

    move-result v0

    return v0
.end method
