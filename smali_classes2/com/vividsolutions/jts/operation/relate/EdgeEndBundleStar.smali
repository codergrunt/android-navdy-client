.class public Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;
.super Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;
.source "EdgeEndBundleStar.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method public insert(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V
    .locals 2
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .prologue
    .line 70
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;->edgeMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;

    .line 71
    .local v0, "eb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;

    .end local v0    # "eb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;-><init>(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    .line 73
    .restart local v0    # "eb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;->insertEdgeEnd(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;Ljava/lang/Object;)V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->insert(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    goto :goto_0
.end method

.method updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 3
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;

    .line 87
    .local v0, "esb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    goto :goto_0

    .line 89
    .end local v0    # "esb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    :cond_0
    return-void
.end method
