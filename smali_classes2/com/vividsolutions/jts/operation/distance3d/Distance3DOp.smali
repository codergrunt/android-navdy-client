.class public Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;
.super Ljava/lang/Object;
.source "Distance3DOp.java"


# instance fields
.field private geom:[Lcom/vividsolutions/jts/geom/Geometry;

.field private isDone:Z

.field private minDistance:D

.field private minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

.field private terminateDistance:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 127
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 128
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 3
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "terminateDistance"    # D

    .prologue
    const/4 v2, 0x0

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->terminateDistance:D

    .line 114
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    .line 115
    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    .line 142
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    .line 143
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aput-object p1, v0, v2

    .line 144
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 145
    iput-wide p3, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->terminateDistance:D

    .line 146
    return-void
.end method

.method private computeMinDistance()V
    .locals 5

    .prologue
    .line 204
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    if-eqz v2, :cond_0

    .line 211
    :goto_0
    return-void

    .line 206
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .line 208
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->mostPolygonalIndex()I

    move-result v1

    .line 209
    .local v1, "geomIndex":I
    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 210
    .local v0, "flip":Z
    :goto_1
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    rsub-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    invoke-direct {p0, v2, v3, v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceMultiMulti(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    goto :goto_0

    .line 209
    .end local v0    # "flip":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private computeMinDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V
    .locals 3
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "flip"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 308
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v2, :cond_5

    .line 309
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v2, :cond_1

    .line 310
    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    check-cast p2, Lcom/vividsolutions/jts/geom/Point;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePointPoint(Lcom/vividsolutions/jts/geom/Point;Lcom/vividsolutions/jts/geom/Point;Z)V

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 313
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_3

    .line 314
    check-cast p2, Lcom/vividsolutions/jts/geom/LineString;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    if-nez p3, :cond_2

    :goto_1
    invoke-direct {p0, p2, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLinePoint(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 317
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v2, :cond_5

    .line 318
    invoke-static {p2}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    move-result-object v2

    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    if-nez p3, :cond_4

    :goto_2
    invoke-direct {p0, v2, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonPoint(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Point;Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2

    .line 322
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_9

    .line 323
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v2, :cond_6

    .line 324
    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    check-cast p2, Lcom/vividsolutions/jts/geom/Point;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLinePoint(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;Z)V

    goto :goto_0

    .line 327
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_6
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_7

    .line 328
    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    check-cast p2, Lcom/vividsolutions/jts/geom/LineString;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLineLine(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/LineString;Z)V

    goto :goto_0

    .line 331
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_7
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v2, :cond_9

    .line 332
    invoke-static {p2}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    move-result-object v2

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    if-nez p3, :cond_8

    :goto_3
    invoke-direct {p0, v2, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonLine(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;Z)V

    goto :goto_0

    :cond_8
    move v0, v1

    goto :goto_3

    .line 336
    .restart local p1    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_9
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_0

    .line 337
    instance-of v0, p2, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_a

    .line 338
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    move-result-object v0

    check-cast p2, Lcom/vividsolutions/jts/geom/Point;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonPoint(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Point;Z)V

    goto :goto_0

    .line 341
    .restart local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_a
    instance-of v0, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_b

    .line 342
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    move-result-object v0

    check-cast p2, Lcom/vividsolutions/jts/geom/LineString;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonLine(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;Z)V

    goto :goto_0

    .line 345
    .restart local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_b
    instance-of v0, p2, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_0

    .line 346
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    move-result-object v0

    check-cast p2, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p2    # "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonPolygon(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Polygon;Z)V

    goto :goto_0
.end method

.method private computeMinDistanceLineLine(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/LineString;Z)V
    .locals 13
    .param p1, "line0"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "line1"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p3, "flip"    # Z

    .prologue
    .line 497
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 498
    .local v7, "coord0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    .line 500
    .local v8, "coord1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    if-ge v9, v1, :cond_1

    .line 501
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    array-length v1, v8

    add-int/lit8 v1, v1, -0x1

    if-ge v10, v1, :cond_3

    .line 502
    aget-object v1, v7, v9

    add-int/lit8 v4, v9, 0x1

    aget-object v4, v7, v4

    aget-object v5, v8, v10

    add-int/lit8 v6, v10, 0x1

    aget-object v6, v8, v6

    invoke-static {v1, v4, v5, v6}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distanceSegmentSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 504
    .local v2, "dist":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 505
    iput-wide v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    .line 507
    new-instance v11, Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v1, v7, v9

    add-int/lit8 v4, v9, 0x1

    aget-object v4, v7, v4

    invoke-direct {v11, v1, v4}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 508
    .local v11, "seg0":Lcom/vividsolutions/jts/geom/LineSegment;
    new-instance v12, Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v1, v8, v10

    add-int/lit8 v4, v10, 0x1

    aget-object v4, v8, v4

    invoke-direct {v12, v1, v4}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 509
    .local v12, "seg1":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual {v11, v12}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoints(Lcom/vividsolutions/jts/geom/LineSegment;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 510
    .local v0, "closestPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v4, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-direct {v4, p1, v9, v1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    new-instance v5, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-direct {v5, p2, v10, v1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    move-object v1, p0

    move/from16 v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->updateDistance(DLcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 516
    .end local v0    # "closestPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v11    # "seg0":Lcom/vividsolutions/jts/geom/LineSegment;
    .end local v12    # "seg1":Lcom/vividsolutions/jts/geom/LineSegment;
    :cond_0
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v1, :cond_2

    .line 519
    .end local v2    # "dist":D
    .end local v10    # "j":I
    :cond_1
    return-void

    .line 501
    .restart local v2    # "dist":D
    .restart local v10    # "j":I
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 500
    .end local v2    # "dist":D
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0
.end method

.method private computeMinDistanceLinePoint(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;Z)V
    .locals 11
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "point"    # Lcom/vividsolutions/jts/geom/Point;
    .param p3, "flip"    # Z

    .prologue
    .line 523
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    .line 524
    .local v8, "lineCoord":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 526
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v1, v8

    add-int/lit8 v1, v1, -0x1

    if-ge v7, v1, :cond_1

    .line 527
    aget-object v1, v8, v7

    add-int/lit8 v4, v7, 0x1

    aget-object v4, v8, v4

    invoke-static {v0, v1, v4}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 529
    .local v2, "dist":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    .line 530
    new-instance v9, Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v1, v8, v7

    add-int/lit8 v4, v7, 0x1

    aget-object v4, v8, v4

    invoke-direct {v9, v1, v4}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 531
    .local v9, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual {v9, v0}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v10

    .line 532
    .local v10, "segClosestPoint":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v4, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-direct {v4, p1, v7, v10}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    new-instance v5, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v1, 0x0

    invoke-direct {v5, p2, v1, v0}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    move-object v1, p0

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->updateDistance(DLcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 537
    .end local v9    # "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    .end local v10    # "segClosestPoint":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v1, :cond_2

    .line 539
    .end local v2    # "dist":D
    :cond_1
    return-void

    .line 526
    .restart local v2    # "dist":D
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method private computeMinDistanceMultiMulti(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V
    .locals 4
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "flip"    # Z

    .prologue
    .line 236
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v3, :cond_2

    .line 237
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    .line 238
    .local v2, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 239
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 240
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceMultiMulti(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    .line 241
    iget-boolean v3, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v3, :cond_1

    .line 256
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "i":I
    .end local v2    # "n":I
    :cond_0
    :goto_1
    return-void

    .line 238
    .restart local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local v1    # "i":I
    .restart local v2    # "n":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 246
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "i":I
    .end local v2    # "n":I
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 250
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_3

    .line 251
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    move-result-object v3

    invoke-direct {p0, v3, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceOneMulti(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    goto :goto_1

    .line 254
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceOneMulti(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    goto :goto_1
.end method

.method private computeMinDistanceOneMulti(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V
    .locals 4
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "flip"    # Z

    .prologue
    .line 259
    instance-of v3, p2, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v3, :cond_2

    .line 260
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    .line 261
    .local v2, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 262
    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 263
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, v0, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceOneMulti(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    .line 264
    iget-boolean v3, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v3, :cond_1

    .line 270
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "i":I
    .end local v2    # "n":I
    :cond_0
    :goto_1
    return-void

    .line 261
    .restart local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local v1    # "i":I
    .restart local v2    # "n":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 268
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "i":I
    .end local v2    # "n":I
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    goto :goto_1
.end method

.method private computeMinDistanceOneMulti(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Geometry;Z)V
    .locals 4
    .param p1, "poly"    # Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "flip"    # Z

    .prologue
    .line 273
    instance-of v3, p2, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v3, :cond_2

    .line 274
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    .line 275
    .local v2, "n":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 276
    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 277
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, v0, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceOneMulti(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Geometry;Z)V

    .line 278
    iget-boolean v3, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v3, :cond_1

    .line 295
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "i":I
    .end local v2    # "n":I
    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_1
    return-void

    .line 275
    .restart local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local v1    # "i":I
    .restart local v2    # "n":I
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 282
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "i":I
    .end local v2    # "n":I
    :cond_2
    instance-of v3, p2, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v3, :cond_3

    .line 283
    check-cast p2, Lcom/vividsolutions/jts/geom/Point;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonPoint(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Point;Z)V

    goto :goto_1

    .line 286
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v3, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v3, :cond_4

    .line 287
    check-cast p2, Lcom/vividsolutions/jts/geom/LineString;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonLine(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;Z)V

    goto :goto_1

    .line 290
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v3, p2, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_0

    .line 291
    check-cast p2, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonPolygon(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Polygon;Z)V

    goto :goto_1
.end method

.method private computeMinDistancePointPoint(Lcom/vividsolutions/jts/geom/Point;Lcom/vividsolutions/jts/geom/Point;Z)V
    .locals 7
    .param p1, "point0"    # Lcom/vividsolutions/jts/geom/Point;
    .param p2, "point1"    # Lcom/vividsolutions/jts/geom/Point;
    .param p3, "flip"    # Z

    .prologue
    const/4 v6, 0x0

    .line 542
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 545
    .local v2, "dist":D
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    cmpg-double v0, v2, v0

    if-gez v0, :cond_0

    .line 546
    new-instance v4, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-direct {v4, p1, v6, v0}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    new-instance v5, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-direct {v5, p2, v6, v0}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    move-object v1, p0

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->updateDistance(DLcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 551
    :cond_0
    return-void
.end method

.method private computeMinDistancePolygonLine(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;Z)V
    .locals 9
    .param p1, "poly"    # Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .param p2, "line"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p3, "flip"    # Z

    .prologue
    const/4 v6, 0x0

    .line 403
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->intersection(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 404
    .local v7, "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v7, :cond_1

    .line 405
    const-wide/16 v2, 0x0

    new-instance v4, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-direct {v4, v1, v6, v7}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    new-instance v5, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-direct {v5, p2, v6, v7}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    move-object v1, p0

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->updateDistance(DLcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 421
    :cond_0
    return-void

    .line 414
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLineLine(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/LineString;Z)V

    .line 415
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-nez v1, :cond_0

    .line 416
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v8

    .line 417
    .local v8, "nHole":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_0

    .line 418
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLineLine(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/LineString;Z)V

    .line 419
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-nez v1, :cond_0

    .line 417
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private computeMinDistancePolygonPoint(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Point;Z)V
    .locals 11
    .param p1, "polyPlane"    # Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .param p2, "point"    # Lcom/vividsolutions/jts/geom/Point;
    .param p3, "flip"    # Z

    .prologue
    const/4 v6, 0x0

    .line 468
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v9

    .line 470
    .local v9, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v10

    .line 471
    .local v10, "shell":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {p1, v9, v10}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 474
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v8

    .line 475
    .local v8, "nHole":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v8, :cond_1

    .line 476
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    .line 477
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {p1, v9, v0}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 478
    invoke-direct {p0, v0, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLinePoint(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;Z)V

    .line 493
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LineString;
    .end local v7    # "i":I
    .end local v8    # "nHole":I
    :goto_1
    return-void

    .line 475
    .restart local v0    # "hole":Lcom/vividsolutions/jts/geom/LineString;
    .restart local v7    # "i":I
    .restart local v8    # "nHole":I
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 484
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LineString;
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPlane()Lcom/vividsolutions/jts/math/Plane3D;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/vividsolutions/jts/math/Plane3D;->orientedDistance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 485
    .local v2, "dist":D
    new-instance v4, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-direct {v4, v1, v6, v9}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    new-instance v5, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-direct {v5, p2, v6, v9}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    move-object v1, p0

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->updateDistance(DLcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 492
    .end local v2    # "dist":D
    .end local v7    # "i":I
    .end local v8    # "nHole":I
    :cond_2
    invoke-direct {p0, v10, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistanceLinePoint(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;Z)V

    goto :goto_1
.end method

.method private computeMinDistancePolygonPolygon(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Polygon;Z)V
    .locals 2
    .param p1, "poly0"    # Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .param p2, "poly1"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p3, "flip"    # Z

    .prologue
    .line 373
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonRings(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Polygon;Z)V

    .line 374
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v1, :cond_0

    .line 377
    :goto_0
    return-void

    .line 375
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    invoke-direct {v0, p2}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;-><init>(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 376
    .local v0, "polyPlane1":Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonRings(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Polygon;Z)V

    goto :goto_0
.end method

.method private computeMinDistancePolygonRings(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/Polygon;Z)V
    .locals 3
    .param p1, "poly"    # Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .param p2, "ringPoly"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p3, "flip"    # Z

    .prologue
    .line 389
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-direct {p0, p1, v2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonLine(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;Z)V

    .line 390
    iget-boolean v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-eqz v2, :cond_1

    .line 397
    :cond_0
    return-void

    .line 392
    :cond_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    .line 393
    .local v1, "nHole":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 394
    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-direct {p0, p1, v2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistancePolygonLine(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;Z)V

    .line 395
    iget-boolean v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    if-nez v2, :cond_0

    .line 393
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 73
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 74
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->distance()D

    move-result-wide v2

    return-wide v2
.end method

.method private intersection(Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 14
    .param p1, "poly"    # Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .param p2, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 424
    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v8

    .line 425
    .local v8, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-interface {v8}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v9

    if-nez v9, :cond_1

    .line 426
    const/4 v7, 0x0

    .line 463
    :cond_0
    :goto_0
    return-object v7

    .line 429
    :cond_1
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 430
    .local v0, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v9, 0x0

    invoke-interface {v8, v9, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 431
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPlane()Lcom/vividsolutions/jts/math/Plane3D;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/vividsolutions/jts/math/Plane3D;->orientedDistance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 434
    .local v2, "d0":D
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 435
    .local v1, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-interface {v8}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v6, v9, :cond_3

    .line 436
    invoke-interface {v8, v6, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 437
    add-int/lit8 v9, v6, 0x1

    invoke-interface {v8, v9, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 438
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->getPlane()Lcom/vividsolutions/jts/math/Plane3D;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/vividsolutions/jts/math/Plane3D;->orientedDistance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 444
    .local v4, "d1":D
    mul-double v10, v2, v4

    const-wide/16 v12, 0x0

    cmpl-double v9, v10, v12

    if-lez v9, :cond_2

    .line 435
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 454
    :cond_2
    invoke-static/range {v0 .. v5}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->segmentPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 456
    .local v7, "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1, v7}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 461
    move-wide v2, v4

    goto :goto_2

    .line 463
    .end local v4    # "d1":D
    .end local v7    # "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_3
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public static isWithinDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "distance"    # D

    .prologue
    .line 90
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 91
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->distance()D

    move-result-wide v2

    cmpg-double v1, v2, p2

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private mostPolygonalIndex()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 221
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v0

    .line 222
    .local v0, "dim0":I
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    .line 223
    .local v1, "dim1":I
    if-lt v0, v5, :cond_2

    if-lt v1, v5, :cond_2

    .line 224
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getNumPoints()I

    move-result v4

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getNumPoints()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 232
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    .line 226
    goto :goto_0

    .line 229
    :cond_2
    if-ge v0, v5, :cond_0

    .line 230
    if-lt v1, v5, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method public static nearestPoints(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 105
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 106
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method

.method private static polyPlane(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
    .locals 1
    .param p0, "poly"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 304
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;

    check-cast p0, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p0    # "poly":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;-><init>(Lcom/vividsolutions/jts/geom/Polygon;)V

    return-object v0
.end method

.method private static segmentPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;DD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 14
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "d0"    # D
    .param p4, "d1"    # D

    .prologue
    .line 570
    const-wide/16 v0, 0x0

    cmpg-double v0, p2, v0

    if-gtz v0, :cond_0

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 577
    :goto_0
    return-object v1

    .line 571
    :cond_0
    const-wide/16 v0, 0x0

    cmpg-double v0, p4, v0

    if-gtz v0, :cond_1

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1, p1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0

    .line 573
    :cond_1
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    add-double/2addr v10, v12

    div-double v8, v0, v10

    .line 574
    .local v8, "f":D
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v12, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v10, v12

    mul-double/2addr v10, v8

    add-double v2, v0, v10

    .line 575
    .local v2, "intx":D
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v12, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v10, v12

    mul-double/2addr v10, v8

    add-double v4, v0, v10

    .line 576
    .local v4, "inty":D
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v12, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    sub-double/2addr v10, v12

    mul-double/2addr v10, v8

    add-double v6, v0, v10

    .line 577
    .local v6, "intz":D
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    goto :goto_0
.end method

.method private updateDistance(DLcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V
    .locals 7
    .param p1, "dist"    # D
    .param p3, "loc0"    # Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .param p4, "loc1"    # Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .param p5, "flip"    # Z

    .prologue
    const/4 v1, 0x1

    .line 194
    iput-wide p1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    .line 195
    if-eqz p5, :cond_1

    move v0, v1

    .line 196
    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aput-object p3, v2, v0

    .line 197
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    rsub-int/lit8 v3, v0, 0x1

    aput-object p4, v2, v3

    .line 198
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->terminateDistance:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 199
    iput-boolean v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->isDone:Z

    .line 200
    :cond_0
    return-void

    .line 195
    .end local v0    # "index":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public distance()D
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 156
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v2

    if-nez v0, :cond_1

    .line 157
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null geometries are not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    :cond_2
    const-wide/16 v0, 0x0

    .line 163
    :goto_0
    return-wide v0

    .line 162
    :cond_3
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistance()V

    .line 163
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistance:D

    goto :goto_0
.end method

.method public nearestLocations()[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistance()V

    .line 188
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    return-object v0
.end method

.method public nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 173
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->computeMinDistance()V

    .line 174
    const/4 v1, 0x2

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance3d/Distance3DOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v3

    .line 177
    .local v0, "nearestPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    return-object v0
.end method
