.class public Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;
.super Ljava/lang/Object;
.source "GeometrySnapper.java"


# static fields
.field private static final SNAP_PRECISION_FACTOR:D = 1.0E-9


# instance fields
.field private srcGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "srcGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->srcGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 143
    return-void
.end method

.method private computeMinimumSegmentLength([Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 7
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 212
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 213
    .local v2, "minSegLen":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 214
    aget-object v1, p1, v0

    add-int/lit8 v6, v0, 0x1

    aget-object v6, p1, v6

    invoke-virtual {v1, v6}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 215
    .local v4, "segLen":D
    cmpg-double v1, v4, v2

    if-gez v1, :cond_0

    .line 216
    move-wide v2, v4

    .line 213
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    .end local v4    # "segLen":D
    :cond_1
    return-wide v2
.end method

.method public static computeOverlaySnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 10
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 69
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->computeSizeBasedSnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v4

    .line 80
    .local v4, "snapTolerance":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v2

    .line 81
    .local v2, "pm":Lcom/vividsolutions/jts/geom/PrecisionModel;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/PrecisionModel;->getType()Lcom/vividsolutions/jts/geom/PrecisionModel$Type;

    move-result-object v3

    sget-object v6, Lcom/vividsolutions/jts/geom/PrecisionModel;->FIXED:Lcom/vividsolutions/jts/geom/PrecisionModel$Type;

    if-ne v3, v6, :cond_0

    .line 82
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/PrecisionModel;->getScale()D

    move-result-wide v8

    div-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    const-wide v8, 0x3ff6a3d70a3d70a4L    # 1.415

    div-double v0, v6, v8

    .line 83
    .local v0, "fixedSnapTol":D
    cmpl-double v3, v0, v4

    if-lez v3, :cond_0

    .line 84
    move-wide v4, v0

    .line 86
    .end local v0    # "fixedSnapTol":D
    :cond_0
    return-wide v4
.end method

.method public static computeOverlaySnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 99
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->computeOverlaySnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v0

    invoke-static {p1}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->computeOverlaySnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static computeSizeBasedSnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 10
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 92
    .local v0, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 93
    .local v2, "minDimension":D
    const-wide v6, 0x3e112e0be826d695L    # 1.0E-9

    mul-double v4, v2, v6

    .line 94
    .local v4, "snapTol":D
    return-wide v4
.end method

.method private computeSnapTolerance([Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 6
    .param p1, "ringPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->computeMinimumSegmentLength([Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 206
    .local v0, "minSegLen":D
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    div-double v2, v0, v4

    .line 207
    .local v2, "snapTol":D
    return-wide v2
.end method

.method public static snap(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)[Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "snapTolerance"    # D

    .prologue
    const/4 v4, 0x0

    .line 112
    const/4 v3, 0x2

    new-array v0, v3, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 113
    .local v0, "snapGeom":[Lcom/vividsolutions/jts/geom/Geometry;
    new-instance v1, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 114
    .local v1, "snapper0":Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;
    invoke-virtual {v1, p1, p2, p3}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->snapTo(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    aput-object v3, v0, v4

    .line 120
    new-instance v2, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;

    invoke-direct {v2, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 121
    .local v2, "snapper1":Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;
    const/4 v3, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v2, v4, p2, p3}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->snapTo(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    aput-object v4, v0, v3

    .line 125
    return-object v0
.end method

.method public static snapToSelf(Lcom/vividsolutions/jts/geom/Geometry;DZ)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "snapTolerance"    # D
    .param p3, "cleanResult"    # Z

    .prologue
    .line 129
    new-instance v0, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 130
    .local v0, "snapper0":Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;
    invoke-virtual {v0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->snapToSelf(DZ)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public extractTargetCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 188
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 189
    .local v1, "ptSet":Ljava/util/Set;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 190
    .local v2, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 191
    aget-object v3, v2, v0

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    const/4 v3, 0x0

    new-array v3, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v1, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v3
.end method

.method public snapTo(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "snapGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "snapTolerance"    # D

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->extractTargetCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 158
    .local v0, "snapPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;

    invoke-direct {v1, p2, p3, v0}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;-><init>(D[Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 159
    .local v1, "snapTrans":Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->srcGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->transform(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    return-object v2
.end method

.method public snapToSelf(DZ)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p1, "snapTolerance"    # D
    .param p3, "cleanResult"    # Z

    .prologue
    .line 173
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->srcGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p0, v4}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->extractTargetCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 175
    .local v1, "snapPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;

    const/4 v4, 0x1

    invoke-direct {v2, p1, p2, v1, v4}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;-><init>(D[Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 176
    .local v2, "snapTrans":Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->srcGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2, v4}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->transform(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 177
    .local v3, "snappedGeom":Lcom/vividsolutions/jts/geom/Geometry;
    move-object v0, v3

    .line 178
    .local v0, "result":Lcom/vividsolutions/jts/geom/Geometry;
    if-eqz p3, :cond_0

    instance-of v4, v0, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v4, :cond_0

    .line 180
    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 182
    :cond_0
    return-object v0
.end method
