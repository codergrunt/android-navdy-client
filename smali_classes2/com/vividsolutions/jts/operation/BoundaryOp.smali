.class public Lcom/vividsolutions/jts/operation/BoundaryOp;
.super Ljava/lang/Object;
.source "BoundaryOp.java"


# instance fields
.field private bnRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field private endpointMap:Ljava/util/Map;

.field private geom:Lcom/vividsolutions/jts/geom/Geometry;

.field private geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 60
    sget-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->MOD2_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/BoundaryOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "bnRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 66
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 67
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->bnRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 68
    return-void
.end method

.method private addEndpoint(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 136
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->endpointMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/Counter;

    .line 137
    .local v0, "counter":Lcom/vividsolutions/jts/operation/Counter;
    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/vividsolutions/jts/operation/Counter;

    .end local v0    # "counter":Lcom/vividsolutions/jts/operation/Counter;
    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/Counter;-><init>()V

    .line 139
    .restart local v0    # "counter":Lcom/vividsolutions/jts/operation/Counter;
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->endpointMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    :cond_0
    iget v1, v0, Lcom/vividsolutions/jts/operation/Counter;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/vividsolutions/jts/operation/Counter;->count:I

    .line 142
    return-void
.end method

.method private boundaryLineString(Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    const/4 v2, 0x2

    .line 146
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/BoundaryOp;->getEmptyMultiPoint()Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v1

    .line 160
    :goto_0
    return-object v1

    .line 150
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 152
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->bnRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    invoke-interface {v1, v2}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->isInBoundary(I)Z

    move-result v0

    .line 153
    .local v0, "closedEndpointOnBoundary":Z
    if-eqz v0, :cond_1

    .line 154
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getStartPoint()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    goto :goto_0

    .line 157
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v1, 0x0

    check-cast v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v1

    goto :goto_0

    .line 160
    .end local v0    # "closedEndpointOnBoundary":Z
    :cond_2
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/Point;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getStartPoint()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getEndPoint()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Point;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v1

    goto :goto_0
.end method

.method private boundaryMultiLineString(Lcom/vividsolutions/jts/geom/MultiLineString;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "mLine"    # Lcom/vividsolutions/jts/geom/MultiLineString;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/BoundaryOp;->getEmptyMultiPoint()Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v1

    .line 95
    :goto_0
    return-object v1

    .line 88
    :cond_0
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/BoundaryOp;->computeBoundaryCoordinates(Lcom/vividsolutions/jts/geom/MultiLineString;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 91
    .local v0, "bdyPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 92
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    goto :goto_0

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v1

    goto :goto_0
.end method

.method private computeBoundaryCoordinates(Lcom/vividsolutions/jts/geom/MultiLineString;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "mLine"    # Lcom/vividsolutions/jts/geom/MultiLineString;

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v0, "bdyPts":Ljava/util/List;
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    iput-object v7, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->endpointMap:Ljava/util/Map;

    .line 114
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiLineString;->getNumGeometries()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 115
    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/geom/MultiLineString;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/LineString;

    .line 116
    .local v5, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v7

    if-nez v7, :cond_0

    .line 114
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 118
    :cond_0
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/operation/BoundaryOp;->addEndpoint(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 119
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v7}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/operation/BoundaryOp;->addEndpoint(Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_1

    .line 122
    .end local v5    # "line":Lcom/vividsolutions/jts/geom/LineString;
    :cond_1
    iget-object v7, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->endpointMap:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "it":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 123
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 124
    .local v2, "entry":Ljava/util/Map$Entry;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/Counter;

    .line 125
    .local v1, "counter":Lcom/vividsolutions/jts/operation/Counter;
    iget v6, v1, Lcom/vividsolutions/jts/operation/Counter;->count:I

    .line 126
    .local v6, "valence":I
    iget-object v7, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->bnRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    invoke-interface {v7, v6}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->isInBoundary(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 127
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 131
    .end local v1    # "counter":Lcom/vividsolutions/jts/operation/Counter;
    .end local v2    # "entry":Ljava/util/Map$Entry;
    .end local v6    # "valence":I
    :cond_3
    invoke-static {v0}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->toCoordinateArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    return-object v7
.end method

.method private getEmptyMultiPoint()Lcom/vividsolutions/jts/geom/MultiPoint;
    .locals 2

    .prologue
    .line 79
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v0, 0x0

    check-cast v0, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/BoundaryOp;->boundaryLineString(Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    check-cast v0, Lcom/vividsolutions/jts/geom/MultiLineString;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/BoundaryOp;->boundaryMultiLineString(Lcom/vividsolutions/jts/geom/MultiLineString;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/BoundaryOp;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getBoundary()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method
