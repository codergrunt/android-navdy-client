.class public Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;
.super Ljava/lang/Object;
.source "Polygonizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$1;,
        Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;
    }
.end annotation


# instance fields
.field protected cutEdges:Ljava/util/List;

.field protected dangles:Ljava/util/Collection;

.field protected graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

.field protected holeList:Ljava/util/List;

.field protected invalidRingLines:Ljava/util/List;

.field private lineStringAdder:Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;

.field protected polyList:Ljava/util/List;

.field protected shellList:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;-><init>(Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$1;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->lineStringAdder:Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->dangles:Ljava/util/Collection;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->cutEdges:Ljava/util/List;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->invalidRingLines:Ljava/util/List;

    .line 83
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->holeList:Ljava/util/List;

    .line 84
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->shellList:Ljava/util/List;

    .line 85
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polyList:Ljava/util/List;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 0
    .param p0, "x0"    # Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;
    .param p1, "x1"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->add(Lcom/vividsolutions/jts/geom/LineString;)V

    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 2
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->addEdge(Lcom/vividsolutions/jts/geom/LineString;)V

    .line 135
    return-void
.end method

.method private static assignHoleToShell(Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;Ljava/util/List;)V
    .locals 2
    .param p0, "holeER"    # Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    .param p1, "shellList"    # Ljava/util/List;

    .prologue
    .line 242
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->findEdgeRingContaining(Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;Ljava/util/List;)Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    move-result-object v0

    .line 243
    .local v0, "shell":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getRing()Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->addHole(Lcom/vividsolutions/jts/geom/LinearRing;)V

    .line 245
    :cond_0
    return-void
.end method

.method private static assignHolesToShells(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p0, "holeList"    # Ljava/util/List;
    .param p1, "shellList"    # Ljava/util/List;

    .prologue
    .line 234
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 236
    .local v0, "holeER":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    invoke-static {v0, p1}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->assignHoleToShell(Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;Ljava/util/List;)V

    goto :goto_0

    .line 238
    .end local v0    # "holeER":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    :cond_0
    return-void
.end method

.method private findShellsAndHoles(Ljava/util/List;)V
    .locals 3
    .param p1, "edgeRingList"    # Ljava/util/List;

    .prologue
    .line 220
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->holeList:Ljava/util/List;

    .line 221
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->shellList:Ljava/util/List;

    .line 222
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 223
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 224
    .local v0, "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->isHole()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->holeList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->shellList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    .end local v0    # "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    :cond_1
    return-void
.end method

.method private findValidRings(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p1, "edgeRingList"    # Ljava/util/List;
    .param p2, "validEdgeRingList"    # Ljava/util/List;
    .param p3, "invalidRingList"    # Ljava/util/List;

    .prologue
    .line 209
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 211
    .local v0, "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 212
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getLineString()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    .end local v0    # "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    :cond_1
    return-void
.end method

.method private polygonize()V
    .locals 6

    .prologue
    .line 183
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polyList:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 205
    :cond_0
    return-void

    .line 184
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polyList:Ljava/util/List;

    .line 187
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    if-eqz v4, :cond_0

    .line 189
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->deleteDangles()Ljava/util/Collection;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->dangles:Ljava/util/Collection;

    .line 190
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->deleteCutEdges()Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->cutEdges:Ljava/util/List;

    .line 191
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->graph:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->getEdgeRings()Ljava/util/List;

    move-result-object v0

    .line 193
    .local v0, "edgeRingList":Ljava/util/List;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 194
    .local v3, "validEdgeRingList":Ljava/util/List;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->invalidRingLines:Ljava/util/List;

    .line 195
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->invalidRingLines:Ljava/util/List;

    invoke-direct {p0, v0, v3, v4}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->findValidRings(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 197
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->findShellsAndHoles(Ljava/util/List;)V

    .line 198
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->holeList:Ljava/util/List;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->shellList:Ljava/util/List;

    invoke-static {v4, v5}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->assignHolesToShells(Ljava/util/List;Ljava/util/List;)V

    .line 200
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polyList:Ljava/util/List;

    .line 201
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->shellList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 202
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 203
    .local v1, "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polyList:Ljava/util/List;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->getPolygon()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->lineStringAdder:Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 122
    return-void
.end method

.method public add(Ljava/util/Collection;)V
    .locals 3
    .param p1, "geomList"    # Ljava/util/Collection;

    .prologue
    .line 105
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 107
    .local v0, "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0

    .line 109
    .end local v0    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method public getCutEdges()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polygonize()V

    .line 164
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->cutEdges:Ljava/util/List;

    return-object v0
.end method

.method public getDangles()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polygonize()V

    .line 154
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->dangles:Ljava/util/Collection;

    return-object v0
.end method

.method public getInvalidRingLines()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polygonize()V

    .line 174
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->invalidRingLines:Ljava/util/List;

    return-object v0
.end method

.method public getPolygons()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polygonize()V

    .line 144
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->polyList:Ljava/util/List;

    return-object v0
.end method
