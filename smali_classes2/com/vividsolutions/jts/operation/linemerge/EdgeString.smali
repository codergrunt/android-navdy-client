.class public Lcom/vividsolutions/jts/operation/linemerge/EdgeString;
.super Ljava/lang/Object;
.source "EdgeString.java"


# instance fields
.field private coordinates:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private directedEdges:Ljava/util/List;

.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->directedEdges:Ljava/util/List;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->coordinates:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 62
    return-void
.end method

.method private getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8

    .prologue
    .line 72
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->coordinates:[Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v5, :cond_2

    .line 73
    const/4 v2, 0x0

    .line 74
    .local v2, "forwardDirectedEdges":I
    const/4 v4, 0x0

    .line 75
    .local v4, "reverseDirectedEdges":I
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 76
    .local v0, "coordinateList":Lcom/vividsolutions/jts/geom/CoordinateList;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->directedEdges:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    .line 78
    .local v1, "directedEdge":Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getEdgeDirection()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 79
    add-int/lit8 v2, v2, 0x1

    .line 84
    :goto_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getEdgeDirection()Z

    move-result v7

    invoke-virtual {v0, v5, v6, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->add([Lcom/vividsolutions/jts/geom/Coordinate;ZZ)Z

    goto :goto_0

    .line 82
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 88
    .end local v1    # "directedEdge":Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
    :cond_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->coordinates:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 89
    if-le v4, v2, :cond_2

    .line 90
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->coordinates:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v5}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->reverse([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 94
    .end local v0    # "coordinateList":Lcom/vividsolutions/jts/geom/CoordinateList;
    .end local v2    # "forwardDirectedEdges":I
    .end local v3    # "i":Ljava/util/Iterator;
    .end local v4    # "reverseDirectedEdges":I
    :cond_2
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->coordinates:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v5
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;)V
    .locals 1
    .param p1, "directedEdge"    # Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->directedEdges:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public toLineString()Lcom/vividsolutions/jts/geom/LineString;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method
