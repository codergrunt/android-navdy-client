.class public Lcom/vividsolutions/jts/noding/NodingValidator;
.super Ljava/lang/Object;
.source "NodingValidator.java"


# instance fields
.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private segStrings:Ljava/util/Collection;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 55
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    .line 56
    return-void
.end method

.method private checkCollapse(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 3
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 87
    invoke-virtual {p1, p3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "found non-noded collapse at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2, p3}, Lcom/vividsolutions/jts/util/Debug;->toLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    return-void
.end method

.method private checkCollapses()V
    .locals 3

    .prologue
    .line 71
    iget-object v2, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 73
    .local v1, "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkCollapses(Lcom/vividsolutions/jts/noding/SegmentString;)V

    goto :goto_0

    .line 75
    .end local v1    # "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    :cond_0
    return-void
.end method

.method private checkCollapses(Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 5
    .param p1, "ss"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 79
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 80
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    add-int/lit8 v2, v2, -0x2

    if-ge v0, v2, :cond_0

    .line 81
    aget-object v2, v1, v0

    add-int/lit8 v3, v0, 0x1

    aget-object v3, v1, v3

    add-int/lit8 v4, v0, 0x2

    aget-object v4, v1, v4

    invoke-direct {p0, v2, v3, v4}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkCollapse(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method private checkEndPtVertexIntersections()V
    .locals 5

    .prologue
    .line 159
    iget-object v3, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 160
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 161
    .local v2, "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-interface {v2}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 162
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    invoke-direct {p0, v3, v4}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkEndPtVertexIntersections(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/Collection;)V

    .line 163
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v1, v3

    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    invoke-direct {p0, v3, v4}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkEndPtVertexIntersections(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/Collection;)V

    goto :goto_0

    .line 165
    .end local v1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v2    # "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    :cond_0
    return-void
.end method

.method private checkEndPtVertexIntersections(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/Collection;)V
    .locals 7
    .param p1, "testPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 169
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 170
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 171
    .local v3, "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-interface {v3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 172
    .local v2, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_0
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 173
    aget-object v4, v2, v1

    invoke-virtual {v4, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 174
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found endpt/interior pt intersection at index "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " :pt "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 172
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v1    # "j":I
    .end local v2    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    :cond_2
    return-void
.end method

.method private checkInteriorIntersections()V
    .locals 5

    .prologue
    .line 97
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 98
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 99
    .local v2, "ss0":Lcom/vividsolutions/jts/noding/SegmentString;
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->segStrings:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "j":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 102
    .local v3, "ss1":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-direct {p0, v2, v3}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkInteriorIntersections(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;)V

    goto :goto_0

    .line 105
    .end local v1    # "j":Ljava/util/Iterator;
    .end local v2    # "ss0":Lcom/vividsolutions/jts/noding/SegmentString;
    .end local v3    # "ss1":Lcom/vividsolutions/jts/noding/SegmentString;
    :cond_1
    return-void
.end method

.method private checkInteriorIntersections(Lcom/vividsolutions/jts/noding/SegmentString;ILcom/vividsolutions/jts/noding/SegmentString;I)V
    .locals 7
    .param p1, "e0"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "segIndex0"    # I
    .param p3, "e1"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p4, "segIndex1"    # I

    .prologue
    .line 120
    if-ne p1, p3, :cond_1

    if-ne p2, p4, :cond_1

    .line 139
    :cond_0
    return-void

    .line 122
    :cond_1
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aget-object v0, v4, p2

    .line 123
    .local v0, "p00":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    add-int/lit8 v5, p2, 0x1

    aget-object v1, v4, v5

    .line 124
    .local v1, "p01":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aget-object v2, v4, p4

    .line 125
    .local v2, "p10":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    add-int/lit8 v5, p4, 0x1

    aget-object v3, v4, v5

    .line 127
    .local v3, "p11":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 128
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 130
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isProper()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {p0, v4, v0, v1}, Lcom/vividsolutions/jts/noding/NodingValidator;->hasInteriorIntersection(Lcom/vividsolutions/jts/algorithm/LineIntersector;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/vividsolutions/jts/noding/NodingValidator;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {p0, v4, v2, v3}, Lcom/vividsolutions/jts/noding/NodingValidator;->hasInteriorIntersection(Lcom/vividsolutions/jts/algorithm/LineIntersector;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 133
    :cond_2
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found non-noded intersection at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private checkInteriorIntersections(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 5
    .param p1, "ss0"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "ss1"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 109
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 110
    .local v2, "pts0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p2}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 111
    .local v3, "pts1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i0":I
    :goto_0
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 112
    const/4 v1, 0x0

    .local v1, "i1":I
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 113
    invoke-direct {p0, p1, v0, p2, v1}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkInteriorIntersections(Lcom/vividsolutions/jts/noding/SegmentString;ILcom/vividsolutions/jts/noding/SegmentString;I)V

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 111
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    .end local v1    # "i1":I
    :cond_1
    return-void
.end method

.method private hasInteriorIntersection(Lcom/vividsolutions/jts/algorithm/LineIntersector;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 3
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 145
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersectionNum()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 146
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersection(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 147
    .local v1, "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, p3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 148
    const/4 v2, 0x1

    .line 150
    .end local v1    # "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return v2

    .line 145
    .restart local v1    # "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    .end local v1    # "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public checkValid()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkEndPtVertexIntersections()V

    .line 62
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkInteriorIntersections()V

    .line 63
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkCollapses()V

    .line 64
    return-void
.end method
