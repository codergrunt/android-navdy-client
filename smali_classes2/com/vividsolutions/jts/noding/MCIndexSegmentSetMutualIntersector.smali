.class public Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;
.super Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;
.source "MCIndexSegmentSetMutualIntersector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector$SegmentOverlapAction;
    }
.end annotation


# instance fields
.field private index:Lcom/vividsolutions/jts/index/SpatialIndex;

.field private indexCounter:I

.field private nOverlaps:I

.field private processCounter:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;-><init>()V

    .line 58
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/STRtree;

    invoke-direct {v0}, Lcom/vividsolutions/jts/index/strtree/STRtree;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->index:Lcom/vividsolutions/jts/index/SpatialIndex;

    .line 59
    iput v1, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->indexCounter:I

    .line 60
    iput v1, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->processCounter:I

    .line 62
    iput v1, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->nOverlaps:I

    .line 66
    return-void
.end method

.method private addToIndex(Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 5
    .param p1, "segStr"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 80
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/vividsolutions/jts/index/chain/MonotoneChainBuilder;->getChains([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 81
    .local v2, "segChains":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/index/chain/MonotoneChain;

    .line 83
    .local v1, "mc":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    iget v3, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->indexCounter:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->indexCounter:I

    invoke-virtual {v1, v3}, Lcom/vividsolutions/jts/index/chain/MonotoneChain;->setId(I)V

    .line 84
    iget-object v3, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->index:Lcom/vividsolutions/jts/index/SpatialIndex;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/index/chain/MonotoneChain;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Lcom/vividsolutions/jts/index/SpatialIndex;->insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    goto :goto_0

    .line 86
    .end local v1    # "mc":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    :cond_0
    return-void
.end method

.method private addToMonoChains(Lcom/vividsolutions/jts/noding/SegmentString;Ljava/util/List;)V
    .locals 5
    .param p1, "segStr"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "monoChains"    # Ljava/util/List;

    .prologue
    .line 119
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/vividsolutions/jts/index/chain/MonotoneChainBuilder;->getChains([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 120
    .local v2, "segChains":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/index/chain/MonotoneChain;

    .line 122
    .local v1, "mc":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    iget v3, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->processCounter:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->processCounter:I

    invoke-virtual {v1, v3}, Lcom/vividsolutions/jts/index/chain/MonotoneChain;->setId(I)V

    .line 123
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    .end local v1    # "mc":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    :cond_0
    return-void
.end method

.method private intersectChains(Ljava/util/List;)V
    .locals 8
    .param p1, "monoChains"    # Ljava/util/List;

    .prologue
    .line 103
    new-instance v2, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector$SegmentOverlapAction;

    iget-object v6, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    invoke-direct {v2, p0, v6}, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector$SegmentOverlapAction;-><init>(Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;Lcom/vividsolutions/jts/noding/SegmentIntersector;)V

    .line 105
    .local v2, "overlapAction":Lcom/vividsolutions/jts/index/chain/MonotoneChainOverlapAction;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 106
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/index/chain/MonotoneChain;

    .line 107
    .local v4, "queryChain":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->index:Lcom/vividsolutions/jts/index/SpatialIndex;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/index/chain/MonotoneChain;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/vividsolutions/jts/index/SpatialIndex;->query(Lcom/vividsolutions/jts/geom/Envelope;)Ljava/util/List;

    move-result-object v3

    .line 108
    .local v3, "overlapChains":Ljava/util/List;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "j":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 109
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/index/chain/MonotoneChain;

    .line 110
    .local v5, "testChain":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    invoke-virtual {v4, v5, v2}, Lcom/vividsolutions/jts/index/chain/MonotoneChain;->computeOverlaps(Lcom/vividsolutions/jts/index/chain/MonotoneChain;Lcom/vividsolutions/jts/index/chain/MonotoneChainOverlapAction;)V

    .line 111
    iget v6, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->nOverlaps:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->nOverlaps:I

    .line 112
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    invoke-interface {v6}, Lcom/vividsolutions/jts/noding/SegmentIntersector;->isDone()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 115
    .end local v1    # "j":Ljava/util/Iterator;
    .end local v3    # "overlapChains":Ljava/util/List;
    .end local v4    # "queryChain":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    .end local v5    # "testChain":Lcom/vividsolutions/jts/index/chain/MonotoneChain;
    :cond_2
    return-void
.end method


# virtual methods
.method public getIndex()Lcom/vividsolutions/jts/index/SpatialIndex;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->index:Lcom/vividsolutions/jts/index/SpatialIndex;

    return-object v0
.end method

.method public process(Ljava/util/Collection;)V
    .locals 3
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 90
    iget v2, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->indexCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->processCounter:I

    .line 91
    const/4 v2, 0x0

    iput v2, p0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->nOverlaps:I

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v1, "monoChains":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/noding/SegmentString;

    invoke-direct {p0, v2, v1}, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->addToMonoChains(Lcom/vividsolutions/jts/noding/SegmentString;Ljava/util/List;)V

    goto :goto_0

    .line 96
    :cond_0
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->intersectChains(Ljava/util/List;)V

    .line 99
    return-void
.end method

.method public setBaseSegments(Ljava/util/Collection;)V
    .locals 2
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 73
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/noding/SegmentString;

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;->addToIndex(Lcom/vividsolutions/jts/noding/SegmentString;)V

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method
