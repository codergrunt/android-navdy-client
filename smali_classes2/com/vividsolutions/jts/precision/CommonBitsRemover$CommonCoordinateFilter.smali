.class Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;
.super Ljava/lang/Object;
.source "CommonBitsRemover.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/precision/CommonBitsRemover;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CommonCoordinateFilter"
.end annotation


# instance fields
.field private commonBitsX:Lcom/vividsolutions/jts/precision/CommonBits;

.field private commonBitsY:Lcom/vividsolutions/jts/precision/CommonBits;

.field final synthetic this$0:Lcom/vividsolutions/jts/precision/CommonBitsRemover;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/precision/CommonBitsRemover;)V
    .locals 1

    .prologue
    .line 128
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->this$0:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBits;

    invoke-direct {v0}, Lcom/vividsolutions/jts/precision/CommonBits;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->commonBitsX:Lcom/vividsolutions/jts/precision/CommonBits;

    .line 132
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBits;

    invoke-direct {v0}, Lcom/vividsolutions/jts/precision/CommonBits;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->commonBitsY:Lcom/vividsolutions/jts/precision/CommonBits;

    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->commonBitsX:Lcom/vividsolutions/jts/precision/CommonBits;

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/precision/CommonBits;->add(D)V

    .line 137
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->commonBitsY:Lcom/vividsolutions/jts/precision/CommonBits;

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/precision/CommonBits;->add(D)V

    .line 138
    return-void
.end method

.method public getCommonCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6

    .prologue
    .line 142
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->commonBitsX:Lcom/vividsolutions/jts/precision/CommonBits;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/precision/CommonBits;->getCommon()D

    move-result-wide v2

    iget-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->commonBitsY:Lcom/vividsolutions/jts/precision/CommonBits;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/precision/CommonBits;->getCommon()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v0
.end method
