.class Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;
.super Ljava/lang/Object;
.source "CommonBitsRemover.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/precision/CommonBitsRemover;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Translater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

.field trans:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/precision/CommonBitsRemover;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p2, "trans"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;->this$0:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;->trans:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 155
    iput-object p2, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;->trans:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 156
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 159
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v2, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;->trans:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 160
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v2, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;->trans:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 161
    return-void
.end method
