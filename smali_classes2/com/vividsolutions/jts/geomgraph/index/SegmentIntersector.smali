.class public Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
.super Ljava/lang/Object;
.source "SegmentIntersector.java"


# instance fields
.field private bdyNodes:[Ljava/util/Collection;

.field private hasIntersection:Z

.field private hasProper:Z

.field private hasProperInterior:Z

.field private includeProper:Z

.field private isSelfIntersection:Z

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private numIntersections:I

.field public numTests:I

.field private properIntersectionPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private recordIsolated:Z


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;ZZ)V
    .locals 2
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "includeProper"    # Z
    .param p3, "recordIsolated"    # Z

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-boolean v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasIntersection:Z

    .line 64
    iput-boolean v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProper:Z

    .line 65
    iput-boolean v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperInterior:Z

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->properIntersectionPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 74
    iput v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->numIntersections:I

    .line 77
    iput v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->numTests:I

    .line 87
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 88
    iput-boolean p2, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->includeProper:Z

    .line 89
    iput-boolean p3, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->recordIsolated:Z

    .line 90
    return-void
.end method

.method public static isAdjacentSegments(II)Z
    .locals 2
    .param p0, "i1"    # I
    .param p1, "i2"    # I

    .prologue
    const/4 v0, 0x1

    .line 56
    sub-int v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isBoundaryPoint(Lcom/vividsolutions/jts/algorithm/LineIntersector;Ljava/util/Collection;)Z
    .locals 4
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "bdyNodes"    # Ljava/util/Collection;

    .prologue
    .line 208
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 209
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 210
    .local v1, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 211
    .local v2, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isIntersection(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    .line 213
    .end local v1    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    .end local v2    # "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private isBoundaryPoint(Lcom/vividsolutions/jts/algorithm/LineIntersector;[Ljava/util/Collection;)Z
    .locals 3
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "bdyNodes"    # [Ljava/util/Collection;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 200
    if-nez p2, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    aget-object v2, p2, v0

    invoke-direct {p0, p1, v2}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->isBoundaryPoint(Lcom/vividsolutions/jts/algorithm/LineIntersector;Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 202
    :cond_2
    aget-object v2, p2, v1

    invoke-direct {p0, p1, v2}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->isBoundaryPoint(Lcom/vividsolutions/jts/algorithm/LineIntersector;Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private isTrivialIntersection(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geomgraph/Edge;I)Z
    .locals 3
    .param p1, "e0"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "segIndex0"    # I
    .param p3, "e1"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p4, "segIndex1"    # I

    .prologue
    const/4 v1, 0x1

    .line 129
    if-ne p1, p3, :cond_3

    .line 130
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersectionNum()I

    move-result v2

    if-ne v2, v1, :cond_3

    .line 131
    invoke-static {p2, p4}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->isAdjacentSegments(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v1

    .line 133
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 134
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getNumPoints()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 135
    .local v0, "maxSegIndex":I
    if-nez p2, :cond_2

    if-eq p4, v0, :cond_0

    :cond_2
    if-nez p4, :cond_3

    if-eq p2, v0, :cond_0

    .line 142
    .end local v0    # "maxSegIndex":I
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addIntersections(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geomgraph/Edge;I)V
    .locals 8
    .param p1, "e0"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "segIndex0"    # I
    .param p3, "e1"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p4, "segIndex1"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 156
    if-ne p1, p3, :cond_1

    if-ne p2, p4, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->numTests:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->numTests:I

    .line 158
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aget-object v0, v4, p2

    .line 159
    .local v0, "p00":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    add-int/lit8 v5, p2, 0x1

    aget-object v1, v4, v5

    .line 160
    .local v1, "p01":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p3}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    aget-object v2, v4, p4

    .line 161
    .local v2, "p10":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p3}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    add-int/lit8 v5, p4, 0x1

    aget-object v3, v4, v5

    .line 163
    .local v3, "p11":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 169
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 170
    iget-boolean v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->recordIsolated:Z

    if-eqz v4, :cond_2

    .line 171
    invoke-virtual {p1, v6}, Lcom/vividsolutions/jts/geomgraph/Edge;->setIsolated(Z)V

    .line 172
    invoke-virtual {p3, v6}, Lcom/vividsolutions/jts/geomgraph/Edge;->setIsolated(Z)V

    .line 175
    :cond_2
    iget v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->numIntersections:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->numIntersections:I

    .line 179
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->isTrivialIntersection(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geomgraph/Edge;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 180
    iput-boolean v7, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasIntersection:Z

    .line 181
    iget-boolean v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->includeProper:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isProper()Z

    move-result v4

    if-nez v4, :cond_4

    .line 183
    :cond_3
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {p1, v4, p2, v6}, Lcom/vividsolutions/jts/geomgraph/Edge;->addIntersections(Lcom/vividsolutions/jts/algorithm/LineIntersector;II)V

    .line 184
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {p3, v4, p4, v7}, Lcom/vividsolutions/jts/geomgraph/Edge;->addIntersections(Lcom/vividsolutions/jts/algorithm/LineIntersector;II)V

    .line 186
    :cond_4
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isProper()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 187
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v4, v6}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersection(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Coordinate;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->properIntersectionPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 188
    iput-boolean v7, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProper:Z

    .line 189
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    iget-object v5, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->bdyNodes:[Ljava/util/Collection;

    invoke-direct {p0, v4, v5}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->isBoundaryPoint(Lcom/vividsolutions/jts/algorithm/LineIntersector;[Ljava/util/Collection;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 190
    iput-boolean v7, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperInterior:Z

    goto/16 :goto_0
.end method

.method public getProperIntersectionPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->properIntersectionPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public hasIntersection()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasIntersection:Z

    return v0
.end method

.method public hasProperInteriorIntersection()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperInterior:Z

    return v0
.end method

.method public hasProperIntersection()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProper:Z

    return v0
.end method

.method public setBoundaryNodes(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 2
    .param p1, "bdyNodes0"    # Ljava/util/Collection;
    .param p2, "bdyNodes1"    # Ljava/util/Collection;

    .prologue
    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Collection;

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->bdyNodes:[Ljava/util/Collection;

    .line 96
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->bdyNodes:[Ljava/util/Collection;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 97
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->bdyNodes:[Ljava/util/Collection;

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 98
    return-void
.end method
