.class public Lcom/vividsolutions/jts/geomgraph/NodeMap;
.super Ljava/lang/Object;
.source "NodeMap.java"


# instance fields
.field nodeFact:Lcom/vividsolutions/jts/geomgraph/NodeFactory;

.field nodeMap:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V
    .locals 1
    .param p1, "nodeFact"    # Lcom/vividsolutions/jts/geomgraph/NodeFactory;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    .line 57
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeFact:Lcom/vividsolutions/jts/geomgraph/NodeFactory;

    .line 58
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V
    .locals 2
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .prologue
    .line 100
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 101
    .local v1, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v0

    .line 102
    .local v0, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Node;->add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    .line 103
    return-void
.end method

.method public addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 2
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 74
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 75
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    if-nez v0, :cond_0

    .line 76
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeFact:Lcom/vividsolutions/jts/geomgraph/NodeFactory;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geomgraph/NodeFactory;->createNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    :cond_0
    return-object v0
.end method

.method public addNode(Lcom/vividsolutions/jts/geomgraph/Node;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 3
    .param p1, "n"    # Lcom/vividsolutions/jts/geomgraph/Node;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 85
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    if-nez v0, :cond_0

    .line 86
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .end local p1    # "n":Lcom/vividsolutions/jts/geomgraph/Node;
    :goto_0
    return-object p1

    .line 89
    .restart local p1    # "n":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Node;->mergeLabel(Lcom/vividsolutions/jts/geomgraph/Node;)V

    move-object p1, v0

    .line 90
    goto :goto_0
.end method

.method public find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    return-object v0
.end method

.method public getBoundaryNodes(I)Ljava/util/Collection;
    .locals 5
    .param p1, "geomIndex"    # I

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v0, "bdyNodes":Ljava/util/Collection;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 122
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 123
    .local v2, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 124
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    .end local v2    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_1
    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public print(Ljava/io/PrintStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 134
    .local v1, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geomgraph/Node;->print(Ljava/io/PrintStream;)V

    goto :goto_0

    .line 136
    .end local v1    # "n":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method public values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/NodeMap;->nodeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
