.class public abstract Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
.super Ljava/lang/Object;
.source "IntervalRTreeNode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode$NodeComparator;
    }
.end annotation


# instance fields
.field protected max:D

.field protected min:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->min:D

    .line 44
    const-wide/high16 v0, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->max:D

    .line 64
    return-void
.end method


# virtual methods
.method public getMax()D
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->max:D

    return-wide v0
.end method

.method public getMin()D
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->min:D

    return-wide v0
.end method

.method protected intersects(DD)Z
    .locals 3
    .param p1, "queryMin"    # D
    .param p3, "queryMax"    # D

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->min:D

    cmpl-double v0, v0, p3

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->max:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 55
    :cond_0
    const/4 v0, 0x0

    .line 56
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 61
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->min:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->max:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
