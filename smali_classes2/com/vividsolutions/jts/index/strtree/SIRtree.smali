.class public Lcom/vividsolutions/jts/index/strtree/SIRtree;
.super Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree;
.source "SIRtree.java"


# instance fields
.field private comparator:Ljava/util/Comparator;

.field private intersectsOp:Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree$IntersectsOp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/index/strtree/SIRtree;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "nodeCapacity"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree;-><init>(I)V

    .line 51
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/SIRtree$1;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/index/strtree/SIRtree$1;-><init>(Lcom/vividsolutions/jts/index/strtree/SIRtree;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/strtree/SIRtree;->comparator:Ljava/util/Comparator;

    .line 59
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/SIRtree$2;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/index/strtree/SIRtree$2;-><init>(Lcom/vividsolutions/jts/index/strtree/SIRtree;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/strtree/SIRtree;->intersectsOp:Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree$IntersectsOp;

    .line 76
    return-void
.end method


# virtual methods
.method protected createNode(I)Lcom/vividsolutions/jts/index/strtree/AbstractNode;
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 79
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/SIRtree$3;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/index/strtree/SIRtree$3;-><init>(Lcom/vividsolutions/jts/index/strtree/SIRtree;I)V

    return-object v0
.end method

.method protected getComparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/SIRtree;->comparator:Ljava/util/Comparator;

    return-object v0
.end method

.method protected getIntersectsOp()Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree$IntersectsOp;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/SIRtree;->intersectsOp:Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree$IntersectsOp;

    return-object v0
.end method

.method public insert(DDLjava/lang/Object;)V
    .locals 7
    .param p1, "x1"    # D
    .param p3, "x2"    # D
    .param p5, "item"    # Ljava/lang/Object;

    .prologue
    .line 100
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/Interval;

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/index/strtree/Interval;-><init>(DD)V

    invoke-super {p0, v0, p5}, Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree;->insert(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    return-void
.end method

.method public query(D)Ljava/util/List;
    .locals 1
    .param p1, "x"    # D

    .prologue
    .line 107
    invoke-virtual {p0, p1, p2, p1, p2}, Lcom/vividsolutions/jts/index/strtree/SIRtree;->query(DD)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public query(DD)Ljava/util/List;
    .locals 7
    .param p1, "x1"    # D
    .param p3, "x2"    # D

    .prologue
    .line 115
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/Interval;

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/index/strtree/Interval;-><init>(DD)V

    invoke-super {p0, v0}, Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree;->query(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
