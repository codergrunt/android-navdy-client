.class public Lcom/vividsolutions/jts/algorithm/Angle;
.super Ljava/lang/Object;
.source "Angle.java"


# static fields
.field public static final CLOCKWISE:I = -0x1

.field public static final COUNTERCLOCKWISE:I = 0x1

.field public static final NONE:I = 0x0

.field public static final PI_OVER_2:D = 1.5707963267948966

.field public static final PI_OVER_4:D = 0.7853981633974483

.field public static final PI_TIMES_2:D = 6.283185307179586


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static angle(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 4
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 8
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 85
    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v0, v4, v6

    .line 86
    .local v0, "dx":D
    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v2, v4, v6

    .line 87
    .local v2, "dy":D
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    return-wide v4
.end method

.method public static angleBetween(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 6
    .param p0, "tip1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "tail"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "tip2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 155
    invoke-static {p1, p0}, Lcom/vividsolutions/jts/algorithm/Angle;->angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 156
    .local v0, "a1":D
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/algorithm/Angle;->angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 158
    .local v2, "a2":D
    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/Angle;->diff(DD)D

    move-result-wide v4

    return-wide v4
.end method

.method public static angleBetweenOriented(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 10
    .param p0, "tip1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "tail"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "tip2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide v8, 0x401921fb54442d18L    # 6.283185307179586

    .line 178
    invoke-static {p1, p0}, Lcom/vividsolutions/jts/algorithm/Angle;->angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 179
    .local v0, "a1":D
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/algorithm/Angle;->angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 180
    .local v2, "a2":D
    sub-double v4, v2, v0

    .line 183
    .local v4, "angDel":D
    const-wide v6, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v6, v4, v6

    if-gtz v6, :cond_1

    .line 184
    add-double/2addr v4, v8

    .line 187
    .end local v4    # "angDel":D
    :cond_0
    :goto_0
    return-wide v4

    .line 185
    .restart local v4    # "angDel":D
    :cond_1
    const-wide v6, 0x400921fb54442d18L    # Math.PI

    cmpl-double v6, v4, v6

    if-lez v6, :cond_0

    .line 186
    sub-double/2addr v4, v8

    goto :goto_0
.end method

.method public static diff(DD)D
    .locals 4
    .param p0, "ang1"    # D
    .param p2, "ang2"    # D

    .prologue
    .line 297
    cmpg-double v2, p0, p2

    if-gez v2, :cond_1

    .line 298
    sub-double v0, p2, p0

    .line 303
    .local v0, "delAngle":D
    :goto_0
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    cmpl-double v2, v0, v2

    if-lez v2, :cond_0

    .line 304
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v0, v2, v0

    .line 307
    :cond_0
    return-wide v0

    .line 300
    .end local v0    # "delAngle":D
    :cond_1
    sub-double v0, p0, p2

    .restart local v0    # "delAngle":D
    goto :goto_0
.end method

.method public static getTurn(DD)I
    .locals 6
    .param p0, "ang1"    # D
    .param p2, "ang2"    # D

    .prologue
    const-wide/16 v4, 0x0

    .line 220
    sub-double v2, p2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    .line 222
    .local v0, "crossproduct":D
    cmpl-double v2, v0, v4

    if-lez v2, :cond_0

    .line 223
    const/4 v2, 0x1

    .line 228
    :goto_0
    return v2

    .line 225
    :cond_0
    cmpg-double v2, v0, v4

    if-gez v2, :cond_1

    .line 226
    const/4 v2, -0x1

    goto :goto_0

    .line 228
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static interiorAngle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 6
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 205
    invoke-static {p1, p0}, Lcom/vividsolutions/jts/algorithm/Angle;->angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 206
    .local v2, "anglePrev":D
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/algorithm/Angle;->angle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 207
    .local v0, "angleNext":D
    sub-double v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    return-wide v4
.end method

.method public static isAcute(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 16
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 115
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v4, v12, v14

    .line 116
    .local v4, "dx0":D
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v8, v12, v14

    .line 117
    .local v8, "dy0":D
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v6, v12, v14

    .line 118
    .local v6, "dx1":D
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v10, v12, v14

    .line 119
    .local v10, "dy1":D
    mul-double v12, v4, v6

    mul-double v14, v8, v10

    add-double v2, v12, v14

    .line 120
    .local v2, "dotprod":D
    const-wide/16 v12, 0x0

    cmpl-double v12, v2, v12

    if-lez v12, :cond_0

    const/4 v12, 0x1

    :goto_0
    return v12

    :cond_0
    const/4 v12, 0x0

    goto :goto_0
.end method

.method public static isObtuse(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 16
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 136
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v4, v12, v14

    .line 137
    .local v4, "dx0":D
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v8, v12, v14

    .line 138
    .local v8, "dy0":D
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v6, v12, v14

    .line 139
    .local v6, "dx1":D
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v10, v12, v14

    .line 140
    .local v10, "dy1":D
    mul-double v12, v4, v6

    mul-double v14, v8, v10

    add-double v2, v12, v14

    .line 141
    .local v2, "dotprod":D
    const-wide/16 v12, 0x0

    cmpg-double v12, v2, v12

    if-gez v12, :cond_0

    const/4 v12, 0x1

    :goto_0
    return v12

    :cond_0
    const/4 v12, 0x0

    goto :goto_0
.end method

.method public static normalize(D)D
    .locals 4
    .param p0, "angle"    # D

    .prologue
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    .line 240
    :goto_0
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    cmpl-double v0, p0, v0

    if-lez v0, :cond_0

    .line 241
    sub-double/2addr p0, v2

    goto :goto_0

    .line 242
    :cond_0
    :goto_1
    const-wide v0, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_1

    .line 243
    add-double/2addr p0, v2

    goto :goto_1

    .line 244
    :cond_1
    return-wide p0
.end method

.method public static normalizePositive(D)D
    .locals 6
    .param p0, "angle"    # D

    .prologue
    const-wide/16 v4, 0x0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    .line 268
    cmpg-double v0, p0, v4

    if-gez v0, :cond_2

    .line 269
    :goto_0
    cmpg-double v0, p0, v4

    if-gez v0, :cond_0

    .line 270
    add-double/2addr p0, v2

    goto :goto_0

    .line 272
    :cond_0
    cmpl-double v0, p0, v2

    if-ltz v0, :cond_1

    .line 273
    const-wide/16 p0, 0x0

    .line 282
    :cond_1
    :goto_1
    return-wide p0

    .line 276
    :cond_2
    :goto_2
    cmpl-double v0, p0, v2

    if-ltz v0, :cond_3

    .line 277
    sub-double/2addr p0, v2

    goto :goto_2

    .line 279
    :cond_3
    cmpg-double v0, p0, v4

    if-gez v0, :cond_1

    .line 280
    const-wide/16 p0, 0x0

    goto :goto_1
.end method

.method public static toDegrees(D)D
    .locals 4
    .param p0, "radians"    # D

    .prologue
    .line 63
    const-wide v0, 0x4066800000000000L    # 180.0

    mul-double/2addr v0, p0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static toRadians(D)D
    .locals 4
    .param p0, "angleDegrees"    # D

    .prologue
    .line 73
    const-wide v0, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, p0

    const-wide v2, 0x4066800000000000L    # 180.0

    div-double/2addr v0, v2

    return-wide v0
.end method
