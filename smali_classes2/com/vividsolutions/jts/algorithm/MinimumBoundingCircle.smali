.class public Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;
.super Ljava/lang/Object;
.source "MinimumBoundingCircle.java"


# instance fields
.field private centre:Lcom/vividsolutions/jts/geom/Coordinate;

.field private extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private input:Lcom/vividsolutions/jts/geom/Geometry;

.field private radius:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 80
    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 81
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->radius:D

    .line 91
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    .line 92
    return-void
.end method

.method private compute()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->computeCirclePoints()V

    .line 182
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->computeCentre()V

    .line 183
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->radius:D

    goto :goto_0
.end method

.method private computeCentre()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 158
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    packed-switch v0, :pswitch_data_0

    .line 175
    :goto_0
    return-void

    .line 160
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 163
    :pswitch_1
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v6

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 166
    :pswitch_2
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v6

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v7

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v2, v4

    div-double/2addr v2, v8

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v6

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v7

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v4, v6

    div-double/2addr v4, v8

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 172
    :pswitch_3
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v7

    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/geom/Triangle;->circumcentre(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private computeCirclePoints()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 190
    iget-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 191
    new-array v7, v10, [Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 259
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/Geometry;->getNumPoints()I

    move-result v7

    if-ne v7, v9, :cond_1

    .line 195
    iget-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    .line 196
    .local v6, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-array v7, v9, [Lcom/vividsolutions/jts/geom/Coordinate;

    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v9, v6, v10

    invoke-direct {v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, v7, v10

    iput-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 204
    .end local v6    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    iget-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/Geometry;->convexHull()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 206
    .local v3, "convexHull":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 209
    .local v4, "hullPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object v6, v4

    .line 210
    .restart local v6    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v7, v4, v10

    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    aget-object v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 211
    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    new-array v6, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 212
    array-length v7, v4

    add-int/lit8 v7, v7, -0x1

    invoke-static {v4, v10, v6, v10, v7}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->copyDeep([Lcom/vividsolutions/jts/geom/Coordinate;I[Lcom/vividsolutions/jts/geom/Coordinate;II)V

    .line 218
    :cond_2
    array-length v7, v6

    if-gt v7, v11, :cond_3

    .line 219
    invoke-static {v6}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->copyDeep([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    iput-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 224
    :cond_3
    invoke-static {v6}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->lowestPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 227
    .local v0, "P":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v6, v0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->pointWitMinAngleWithX([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 236
    .local v1, "Q":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v7, v6

    if-ge v5, v7, :cond_7

    .line 237
    invoke-static {v6, v0, v1}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->pointWithMinAngleWithSegment([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 240
    .local v2, "R":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v0, v2, v1}, Lcom/vividsolutions/jts/algorithm/Angle;->isObtuse(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 241
    new-array v7, v11, [Lcom/vividsolutions/jts/geom/Coordinate;

    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v8, v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, v7, v10

    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v8, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, v7, v9

    iput-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 245
    :cond_4
    invoke-static {v2, v0, v1}, Lcom/vividsolutions/jts/algorithm/Angle;->isObtuse(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 246
    move-object v0, v2

    .line 236
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 250
    :cond_5
    invoke-static {v2, v1, v0}, Lcom/vividsolutions/jts/algorithm/Angle;->isObtuse(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 251
    move-object v1, v2

    .line 252
    goto :goto_2

    .line 255
    :cond_6
    const/4 v7, 0x3

    new-array v7, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v8, v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, v7, v10

    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v8, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, v7, v9

    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v8, v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, v7, v11

    iput-object v7, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    goto/16 :goto_0

    .line 258
    .end local v2    # "R":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_7
    const-string v7, "Logic failure in Minimum Bounding Circle algorithm!"

    invoke-static {v7}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static lowestPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 263
    const/4 v2, 0x0

    aget-object v1, p0, v2

    .line 264
    .local v1, "min":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 265
    aget-object v2, p0, v0

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 266
    aget-object v1, p0, v0

    .line 264
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    :cond_1
    return-object v1
.end method

.method private static pointWitMinAngleWithX([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 20
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "P"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 273
    const-wide v10, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 274
    .local v10, "minSin":D
    const/4 v7, 0x0

    .line 275
    .local v7, "minAngPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move-object/from16 v0, p0

    array-length v13, v0

    if-ge v6, v13, :cond_3

    .line 277
    aget-object v12, p0, v6

    .line 278
    .local v12, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p1

    if-ne v12, v0, :cond_1

    .line 275
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 283
    :cond_1
    iget-wide v0, v12, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    sub-double v2, v16, v18

    .line 284
    .local v2, "dx":D
    iget-wide v0, v12, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    sub-double v4, v16, v18

    .line 285
    .local v4, "dy":D
    const-wide/16 v16, 0x0

    cmpg-double v13, v4, v16

    if-gez v13, :cond_2

    neg-double v4, v4

    .line 286
    :cond_2
    mul-double v16, v2, v2

    mul-double v18, v4, v4

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 287
    .local v8, "len":D
    div-double v14, v4, v8

    .line 289
    .local v14, "sin":D
    cmpg-double v13, v14, v10

    if-gez v13, :cond_0

    .line 290
    move-wide v10, v14

    .line 291
    move-object v7, v12

    goto :goto_1

    .line 294
    .end local v2    # "dx":D
    .end local v4    # "dy":D
    .end local v8    # "len":D
    .end local v12    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v14    # "sin":D
    :cond_3
    return-object v7
.end method

.method private static pointWithMinAngleWithSegment([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "P"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "Q"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 299
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 300
    .local v4, "minAng":D
    const/4 v3, 0x0

    .line 301
    .local v3, "minAngPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, p0

    if-ge v2, v7, :cond_2

    .line 303
    aget-object v6, p0, v2

    .line 304
    .local v6, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    if-ne v6, p1, :cond_1

    .line 301
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 305
    :cond_1
    if-eq v6, p2, :cond_0

    .line 307
    invoke-static {p1, v6, p2}, Lcom/vividsolutions/jts/algorithm/Angle;->angleBetween(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 308
    .local v0, "ang":D
    cmpg-double v7, v0, v4

    if-gez v7, :cond_0

    .line 309
    move-wide v4, v0

    .line 310
    move-object v3, v6

    goto :goto_1

    .line 313
    .end local v0    # "ang":D
    .end local v6    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    return-object v3
.end method


# virtual methods
.method public getCentre()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->compute()V

    .line 142
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getCircle()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->compute()V

    .line 111
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v1, :cond_1

    .line 112
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    .line 116
    :cond_0
    :goto_0
    return-object v0

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->centre:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    .line 114
    .local v0, "centrePoint":Lcom/vividsolutions/jts/geom/Point;
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->radius:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 116
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->radius:D

    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/geom/Point;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method public getExtremalPoints()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->compute()V

    .line 130
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->extremalPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getRadius()D
    .locals 2

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->compute()V

    .line 153
    iget-wide v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumBoundingCircle;->radius:D

    return-wide v0
.end method
