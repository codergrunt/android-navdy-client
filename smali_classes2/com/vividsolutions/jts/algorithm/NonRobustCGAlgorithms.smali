.class public Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;
.super Lcom/vividsolutions/jts/algorithm/CGAlgorithms;
.source "NonRobustCGAlgorithms.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;-><init>()V

    .line 48
    return-void
.end method

.method public static computeOrientation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 1
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "q"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 181
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    return v0
.end method

.method public static distanceLineLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 24
    .param p0, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "C"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "D"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 229
    invoke-virtual/range {p0 .. p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 230
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v16

    .line 279
    :goto_0
    return-wide v16

    .line 231
    :cond_0
    invoke-virtual/range {p2 .. p3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 232
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v16

    goto :goto_0

    .line 250
    :cond_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    mul-double v16, v16, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v18, v18, v20

    sub-double v8, v16, v18

    .line 251
    .local v8, "r_top":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    mul-double v16, v16, v18

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v18, v18, v20

    sub-double v6, v16, v18

    .line 253
    .local v6, "r_bot":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    mul-double v16, v16, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v18, v18, v20

    sub-double v14, v16, v18

    .line 254
    .local v14, "s_top":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    mul-double v16, v16, v18

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v18, v18, v20

    sub-double v12, v16, v18

    .line 256
    .local v12, "s_bot":D
    const-wide/16 v16, 0x0

    cmpl-double v16, v6, v16

    if-eqz v16, :cond_2

    const-wide/16 v16, 0x0

    cmpl-double v16, v12, v16

    if-nez v16, :cond_3

    .line 257
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v16

    invoke-static/range {p1 .. p3}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v18

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v20

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->min(DD)D

    move-result-wide v20

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->min(DD)D

    move-result-wide v18

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->min(DD)D

    move-result-wide v16

    goto/16 :goto_0

    .line 266
    :cond_3
    div-double v10, v14, v12

    .line 267
    .local v10, "s":D
    div-double v4, v8, v6

    .line 269
    .local v4, "r":D
    const-wide/16 v16, 0x0

    cmpg-double v16, v4, v16

    if-ltz v16, :cond_4

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v16, v4, v16

    if-gtz v16, :cond_4

    const-wide/16 v16, 0x0

    cmpg-double v16, v10, v16

    if-ltz v16, :cond_4

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpl-double v16, v10, v16

    if-lez v16, :cond_5

    .line 271
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v16

    invoke-static/range {p1 .. p3}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v18

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v20

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/NonRobustCGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v22

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->min(DD)D

    move-result-wide v20

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->min(DD)D

    move-result-wide v18

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->min(DD)D

    move-result-wide v16

    goto/16 :goto_0

    .line 279
    :cond_5
    const-wide/16 v16, 0x0

    goto/16 :goto_0
.end method

.method public static isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 26
    .param p0, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 108
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v9, v17, -0x1

    .line 111
    .local v9, "nPts":I
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    const/16 v17, 0x0

    .line 167
    :goto_0
    return v17

    .line 115
    :cond_0
    const/16 v17, 0x0

    aget-object v5, p0, v17

    .line 116
    .local v5, "hip":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v4, 0x0

    .line 117
    .local v4, "hii":I
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_1
    if-gt v6, v9, :cond_2

    .line 118
    aget-object v11, p0, v6

    .line 119
    .local v11, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v0, v11, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    iget-wide v0, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    cmpl-double v17, v22, v24

    if-lez v17, :cond_1

    .line 120
    move-object v5, v11

    .line 121
    move v4, v6

    .line 117
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 126
    .end local v11    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    move v8, v4

    .line 128
    .local v8, "iPrev":I
    :cond_3
    add-int/lit8 v17, v8, -0x1

    rem-int v8, v17, v9

    .line 129
    aget-object v17, p0, v8

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    if-ne v8, v4, :cond_3

    .line 132
    :cond_4
    move v7, v4

    .line 134
    .local v7, "iNext":I
    :cond_5
    add-int/lit8 v17, v7, 0x1

    rem-int v7, v17, v9

    .line 135
    aget-object v17, p0, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    if-ne v7, v4, :cond_5

    .line 137
    :cond_6
    aget-object v16, p0, v8

    .line 138
    .local v16, "prev":Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v10, p0, v7

    .line 140
    .local v10, "next":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    invoke-virtual {v10, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 141
    :cond_7
    new-instance v17, Ljava/lang/IllegalArgumentException;

    const-string v22, "degenerate ring (does not contain 3 different points)"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 147
    :cond_8
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    iget-wide v0, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    sub-double v18, v22, v24

    .line 148
    .local v18, "prev2x":D
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    iget-wide v0, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    sub-double v20, v22, v24

    .line 149
    .local v20, "prev2y":D
    iget-wide v0, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    iget-wide v0, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    sub-double v12, v22, v24

    .line 150
    .local v12, "next2x":D
    iget-wide v0, v10, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    iget-wide v0, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    sub-double v14, v22, v24

    .line 153
    .local v14, "next2y":D
    mul-double v22, v12, v20

    mul-double v24, v14, v18

    sub-double v2, v22, v24

    .line 161
    .local v2, "disc":D
    const-wide/16 v22, 0x0

    cmpl-double v17, v2, v22

    if-nez v17, :cond_a

    .line 163
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    iget-wide v0, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    cmpl-double v17, v22, v24

    if-lez v17, :cond_9

    const/16 v17, 0x1

    goto/16 :goto_0

    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 167
    :cond_a
    const-wide/16 v22, 0x0

    cmpl-double v17, v2, v22

    if-lez v17, :cond_b

    const/16 v17, 0x1

    goto/16 :goto_0

    :cond_b
    const/16 v17, 0x0

    goto/16 :goto_0
.end method

.method public static isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 22
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 64
    const/4 v2, 0x0

    .line 66
    .local v2, "crossings":I
    move-object/from16 v0, p1

    array-length v5, v0

    .line 69
    .local v5, "nPts":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_3

    .line 70
    add-int/lit8 v4, v3, -0x1

    .line 71
    .local v4, "i1":I
    aget-object v6, p1, v3

    .line 72
    .local v6, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v7, p1, v4

    .line 73
    .local v7, "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v0, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v8, v18, v20

    .line 74
    .local v8, "x1":D
    iget-wide v0, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    sub-double v14, v18, v20

    .line 75
    .local v14, "y1":D
    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v10, v18, v20

    .line 76
    .local v10, "x2":D
    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    sub-double v16, v18, v20

    .line 78
    .local v16, "y2":D
    const-wide/16 v18, 0x0

    cmpl-double v18, v14, v18

    if-lez v18, :cond_0

    const-wide/16 v18, 0x0

    cmpg-double v18, v16, v18

    if-lez v18, :cond_1

    :cond_0
    const-wide/16 v18, 0x0

    cmpl-double v18, v16, v18

    if-lez v18, :cond_2

    const-wide/16 v18, 0x0

    cmpg-double v18, v14, v18

    if-gtz v18, :cond_2

    .line 81
    :cond_1
    mul-double v18, v8, v16

    mul-double v20, v10, v14

    sub-double v18, v18, v20

    sub-double v20, v16, v14

    div-double v12, v18, v20

    .line 84
    .local v12, "xInt":D
    const-wide/16 v18, 0x0

    cmpg-double v18, v18, v12

    if-gez v18, :cond_2

    .line 85
    add-int/lit8 v2, v2, 0x1

    .line 69
    .end local v12    # "xInt":D
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 89
    .end local v4    # "i1":I
    .end local v6    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v7    # "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v8    # "x1":D
    .end local v10    # "x2":D
    .end local v14    # "y1":D
    .end local v16    # "y2":D
    :cond_3
    rem-int/lit8 v18, v2, 0x2

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 90
    const/16 v18, 0x1

    .line 92
    :goto_1
    return v18

    :cond_4
    const/16 v18, 0x0

    goto :goto_1
.end method

.method public static orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 16
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "q"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 201
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v4, v12, v14

    .line 202
    .local v4, "dx1":D
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v8, v12, v14

    .line 203
    .local v8, "dy1":D
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v6, v12, v14

    .line 204
    .local v6, "dx2":D
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v10, v12, v14

    .line 205
    .local v10, "dy2":D
    mul-double v12, v4, v10

    mul-double v14, v6, v8

    sub-double v2, v12, v14

    .line 206
    .local v2, "det":D
    const-wide/16 v12, 0x0

    cmpl-double v12, v2, v12

    if-lez v12, :cond_0

    const/4 v12, 0x1

    .line 208
    :goto_0
    return v12

    .line 207
    :cond_0
    const-wide/16 v12, 0x0

    cmpg-double v12, v2, v12

    if-gez v12, :cond_1

    const/4 v12, -0x1

    goto :goto_0

    .line 208
    :cond_1
    const/4 v12, 0x0

    goto :goto_0
.end method
