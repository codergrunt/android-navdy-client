.class public Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;
.super Ljava/lang/Object;
.source "ConformingDelaunayTriangulator.java"


# static fields
.field private static final MAX_SPLIT_ITER:I = 0x63


# instance fields
.field private computeAreaEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private convexHull:Lcom/vividsolutions/jts/geom/Geometry;

.field private incDel:Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;

.field private initialVertices:Ljava/util/List;

.field private kdt:Lcom/vividsolutions/jts/index/kdtree/KdTree;

.field private segVertices:Ljava/util/List;

.field private segments:Ljava/util/List;

.field private splitFinder:Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;

.field private splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

.field private tolerance:D

.field private vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;


# direct methods
.method public constructor <init>(Ljava/util/Collection;D)V
    .locals 2
    .param p1, "initialVertices"    # Ljava/util/Collection;
    .param p2, "tolerance"    # D

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segments:Ljava/util/List;

    .line 105
    iput-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 108
    new-instance v0, Lcom/vividsolutions/jts/triangulate/NonEncroachingSplitPointFinder;

    invoke-direct {v0}, Lcom/vividsolutions/jts/triangulate/NonEncroachingSplitPointFinder;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitFinder:Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;

    .line 109
    iput-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->kdt:Lcom/vividsolutions/jts/index/kdtree/KdTree;

    .line 110
    iput-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    .line 115
    iput-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->initialVertices:Ljava/util/List;

    .line 132
    iput-wide p2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->tolerance:D

    .line 133
    new-instance v0, Lcom/vividsolutions/jts/index/kdtree/KdTree;

    invoke-direct {v0, p2, p3}, Lcom/vividsolutions/jts/index/kdtree/KdTree;-><init>(D)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->kdt:Lcom/vividsolutions/jts/index/kdtree/KdTree;

    .line 134
    return-void
.end method

.method private addConstraintVertices()V
    .locals 1

    .prologue
    .line 427
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeConvexHull()V

    .line 429
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segVertices:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->insertSites(Ljava/util/Collection;)V

    .line 430
    return-void
.end method

.method private computeBoundingBox()V
    .locals 14

    .prologue
    const-wide v12, 0x3fc999999999999aL    # 0.2

    .line 243
    iget-object v9, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->initialVertices:Ljava/util/List;

    invoke-static {v9}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeVertexEnvelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v8

    .line 244
    .local v8, "vertexEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v9, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segVertices:Ljava/util/List;

    invoke-static {v9}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeVertexEnvelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    .line 246
    .local v1, "segEnv":Lcom/vividsolutions/jts/geom/Envelope;
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0, v8}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 247
    .local v0, "allPointsEnv":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 249
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v10

    mul-double v4, v10, v12

    .line 250
    .local v4, "deltaX":D
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v10

    mul-double v6, v10, v12

    .line 252
    .local v6, "deltaY":D
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 254
    .local v2, "delta":D
    new-instance v9, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v9, v0}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    iput-object v9, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeAreaEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 255
    iget-object v9, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeAreaEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v9, v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->expandBy(D)V

    .line 256
    return-void
.end method

.method private computeConvexHull()V
    .locals 4

    .prologue
    .line 259
    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    .line 260
    .local v1, "fact":Lcom/vividsolutions/jts/geom/GeometryFactory;
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->getPointArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 261
    .local v0, "coords":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/algorithm/ConvexHull;

    invoke-direct {v2, v0, v1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 262
    .local v2, "hull":Lcom/vividsolutions/jts/algorithm/ConvexHull;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->getConvexHull()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    iput-object v3, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->convexHull:Lcom/vividsolutions/jts/geom/Geometry;

    .line 263
    return-void
.end method

.method private static computeVertexEnvelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;
    .locals 4
    .param p0, "vertices"    # Ljava/util/Collection;

    .prologue
    .line 91
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    .line 92
    .local v0, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 94
    .local v2, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0

    .line 96
    .end local v2    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_0
    return-object v0
.end method

.method private createVertex(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    .locals 3
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 315
    const/4 v0, 0x0

    .line 316
    .local v0, "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;->createVertex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    .line 319
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .end local v0    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .restart local v0    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    goto :goto_0
.end method

.method private createVertex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "seg"    # Lcom/vividsolutions/jts/triangulate/Segment;

    .prologue
    .line 331
    const/4 v0, 0x0

    .line 332
    .local v0, "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    invoke-interface {v1, p1, p2}, Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;->createVertex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    move-result-object v0

    .line 336
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->setOnConstraint(Z)V

    .line 337
    return-object v0

    .line 335
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .end local v0    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .restart local v0    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    goto :goto_0
.end method

.method private enforceGabriel(Ljava/util/Collection;)I
    .locals 26
    .param p1, "segsToInsert"    # Ljava/util/Collection;

    .prologue
    .line 440
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 441
    .local v21, "newSegments":Ljava/util/List;
    const/16 v25, 0x0

    .line 442
    .local v25, "splits":I
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 450
    .local v23, "segsToRemove":Ljava/util/List;
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 451
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/vividsolutions/jts/triangulate/Segment;

    .line 454
    .local v22, "seg":Lcom/vividsolutions/jts/triangulate/Segment;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->findNonGabrielPoint(Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 456
    .local v2, "encroachPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v2, :cond_0

    .line 460
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitFinder:Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;

    move-object/from16 v0, v22

    invoke-interface {v4, v0, v2}, Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;->findSplitPoint(Lcom/vividsolutions/jts/triangulate/Segment;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v4, v1}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->createVertex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    move-result-object v24

    .line 481
    .local v24, "splitVertex":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->insertSite(Lcom/vividsolutions/jts/triangulate/ConstraintVertex;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    move-result-object v20

    .line 482
    .local v20, "insertedVertex":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    invoke-virtual/range {v20 .. v20}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4, v6}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 483
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Split pt snapped to: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/vividsolutions/jts/util/Debug;->println(Ljava/lang/Object;)V

    .line 491
    :cond_1
    new-instance v3, Lcom/vividsolutions/jts/triangulate/Segment;

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getStartX()D

    move-result-wide v4

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getStartY()D

    move-result-wide v6

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getStartZ()D

    move-result-wide v8

    invoke-virtual/range {v24 .. v24}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getX()D

    move-result-wide v10

    invoke-virtual/range {v24 .. v24}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getY()D

    move-result-wide v12

    invoke-virtual/range {v24 .. v24}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getZ()D

    move-result-wide v14

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getData()Ljava/lang/Object;

    move-result-object v16

    invoke-direct/range {v3 .. v16}, Lcom/vividsolutions/jts/triangulate/Segment;-><init>(DDDDDDLjava/lang/Object;)V

    .line 494
    .local v3, "s1":Lcom/vividsolutions/jts/triangulate/Segment;
    new-instance v5, Lcom/vividsolutions/jts/triangulate/Segment;

    invoke-virtual/range {v24 .. v24}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getX()D

    move-result-wide v6

    invoke-virtual/range {v24 .. v24}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getY()D

    move-result-wide v8

    invoke-virtual/range {v24 .. v24}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getZ()D

    move-result-wide v10

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getEndX()D

    move-result-wide v12

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getEndY()D

    move-result-wide v14

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getEndZ()D

    move-result-wide v16

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/triangulate/Segment;->getData()Ljava/lang/Object;

    move-result-object v18

    invoke-direct/range {v5 .. v18}, Lcom/vividsolutions/jts/triangulate/Segment;-><init>(DDDDDDLjava/lang/Object;)V

    .line 497
    .local v5, "s2":Lcom/vividsolutions/jts/triangulate/Segment;
    move-object/from16 v0, v21

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501
    add-int/lit8 v25, v25, 0x1

    .line 502
    goto/16 :goto_0

    .line 503
    .end local v2    # "encroachPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "s1":Lcom/vividsolutions/jts/triangulate/Segment;
    .end local v5    # "s2":Lcom/vividsolutions/jts/triangulate/Segment;
    .end local v20    # "insertedVertex":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    .end local v22    # "seg":Lcom/vividsolutions/jts/triangulate/Segment;
    .end local v24    # "splitVertex":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    .line 504
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 506
    return v25
.end method

.method private findNonGabrielPoint(Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 28
    .param p1, "seg"    # Lcom/vividsolutions/jts/triangulate/Segment;

    .prologue
    .line 527
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    .line 528
    .local v11, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getEnd()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v12

    .line 530
    .local v12, "q":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v7, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v11, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    iget-wide v0, v12, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    add-double v22, v22, v24

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v22, v22, v24

    iget-wide v0, v11, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    iget-wide v0, v12, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v26, v0

    add-double v24, v24, v26

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    div-double v24, v24, v26

    move-wide/from16 v0, v22

    move-wide/from16 v2, v24

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 531
    .local v7, "midPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v11, v7}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v14

    .line 534
    .local v14, "segRadius":D
    new-instance v5, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v5, v7}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 535
    .local v5, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v5, v14, v15}, Lcom/vividsolutions/jts/geom/Envelope;->expandBy(D)V

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->kdt:Lcom/vividsolutions/jts/index/kdtree/KdTree;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->query(Lcom/vividsolutions/jts/geom/Envelope;)Ljava/util/List;

    move-result-object v13

    .line 541
    .local v13, "result":Ljava/util/List;
    const/4 v4, 0x0

    .line 542
    .local v4, "closestNonGabriel":Lcom/vividsolutions/jts/geom/Coordinate;
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 543
    .local v8, "minDist":D
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 544
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 545
    .local v10, "nextNode":Lcom/vividsolutions/jts/index/kdtree/KdNode;
    invoke-virtual {v10}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v18

    .line 547
    .local v18, "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v19

    if-nez v19, :cond_0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 550
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v20

    .line 551
    .local v20, "testRadius":D
    cmpg-double v19, v20, v14

    if-gez v19, :cond_0

    .line 553
    move-wide/from16 v16, v20

    .line 554
    .local v16, "testDist":D
    if-eqz v4, :cond_1

    cmpg-double v19, v16, v8

    if-gez v19, :cond_0

    .line 555
    :cond_1
    move-object/from16 v4, v18

    .line 556
    move-wide/from16 v8, v16

    goto :goto_0

    .line 560
    .end local v10    # "nextNode":Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .end local v16    # "testDist":D
    .end local v18    # "testPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v20    # "testRadius":D
    :cond_2
    return-object v4
.end method

.method private getPointArray()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8

    .prologue
    .line 300
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->initialVertices:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segVertices:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    new-array v4, v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 302
    .local v4, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .line 303
    .local v2, "index":I
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->initialVertices:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 304
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 305
    .local v5, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "index":I
    .local v3, "index":I
    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    aput-object v6, v4, v2

    move v2, v3

    .line 306
    .end local v3    # "index":I
    .restart local v2    # "index":I
    goto :goto_0

    .line 307
    .end local v5    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_0
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segVertices:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i2":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 308
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 309
    .restart local v5    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "index":I
    .restart local v3    # "index":I
    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    aput-object v6, v4, v2

    move v2, v3

    .line 310
    .end local v3    # "index":I
    .restart local v2    # "index":I
    goto :goto_1

    .line 311
    .end local v5    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_1
    return-object v4
.end method

.method private insertSite(Lcom/vividsolutions/jts/triangulate/ConstraintVertex;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    .locals 4
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .prologue
    .line 354
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->kdt:Lcom/vividsolutions/jts/index/kdtree/KdTree;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->insert(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)Lcom/vividsolutions/jts/index/kdtree/KdNode;

    move-result-object v0

    .line 355
    .local v0, "kdnode":Lcom/vividsolutions/jts/index/kdtree/KdNode;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->isRepeated()Z

    move-result v2

    if-nez v2, :cond_0

    .line 356
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->incDel:Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;

    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->insertSite(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 366
    .end local p1    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    :goto_0
    return-object p1

    .line 358
    .restart local p1    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    :cond_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .line 359
    .local v1, "snappedV":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->merge(Lcom/vividsolutions/jts/triangulate/ConstraintVertex;)V

    move-object p1, v1

    .line 360
    goto :goto_0
.end method

.method private insertSites(Ljava/util/Collection;)V
    .locals 4
    .param p1, "vertices"    # Ljava/util/Collection;

    .prologue
    .line 346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding sites: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vividsolutions/jts/util/Debug;->println(Ljava/lang/Object;)V

    .line 347
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 348
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .line 349
    .local v1, "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->insertSite(Lcom/vividsolutions/jts/triangulate/ConstraintVertex;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    goto :goto_0

    .line 351
    .end local v1    # "v":Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
    :cond_0
    return-void
.end method


# virtual methods
.method public enforceConstraints()V
    .locals 5

    .prologue
    const/16 v4, 0x63

    .line 405
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->addConstraintVertices()V

    .line 408
    const/4 v0, 0x0

    .line 409
    .local v0, "count":I
    const/4 v1, 0x0

    .line 411
    .local v1, "splits":I
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segments:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->enforceGabriel(Ljava/util/Collection;)I

    move-result v1

    .line 413
    add-int/lit8 v0, v0, 0x1

    .line 414
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Iter: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   Splits: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   Current # segments = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/vividsolutions/jts/util/Debug;->println(Ljava/lang/Object;)V

    .line 416
    if-lez v1, :cond_1

    if-lt v0, v4, :cond_0

    .line 417
    :cond_1
    if-ne v0, v4, :cond_2

    .line 418
    const-string v2, "ABORTED! Too many iterations while enforcing constraints"

    invoke-static {v2}, Lcom/vividsolutions/jts/util/Debug;->println(Ljava/lang/Object;)V

    .line 419
    invoke-static {}, Lcom/vividsolutions/jts/util/Debug;->isDebugging()Z

    move-result v2

    if-nez v2, :cond_2

    .line 420
    new-instance v2, Lcom/vividsolutions/jts/triangulate/ConstraintEnforcementException;

    const-string v3, "Too many splitting iterations while enforcing constraints.  Last split point was at: "

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, v3, v4}, Lcom/vividsolutions/jts/triangulate/ConstraintEnforcementException;-><init>(Ljava/lang/String;Lcom/vividsolutions/jts/geom/Coordinate;)V

    throw v2

    .line 424
    :cond_2
    return-void
.end method

.method public formInitialDelaunay()V
    .locals 4

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeBoundingBox()V

    .line 388
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->computeAreaEnv:Lcom/vividsolutions/jts/geom/Envelope;

    iget-wide v2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->tolerance:D

    invoke-direct {v0, v1, v2, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;-><init>(Lcom/vividsolutions/jts/geom/Envelope;D)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 389
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    new-instance v1, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->setLocator(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;)V

    .line 390
    new-instance v0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->incDel:Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;

    .line 391
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->initialVertices:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->insertSites(Ljava/util/Collection;)V

    .line 392
    return-void
.end method

.method public getConstraintSegments()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segments:Ljava/util/List;

    return-object v0
.end method

.method public getConvexHull()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->convexHull:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public getInitialVertices()Ljava/util/List;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->initialVertices:Ljava/util/List;

    return-object v0
.end method

.method public getKDT()Lcom/vividsolutions/jts/index/kdtree/KdTree;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->kdt:Lcom/vividsolutions/jts/index/kdtree/KdTree;

    return-object v0
.end method

.method public getSubdivision()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    return-object v0
.end method

.method public getTolerance()D
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->tolerance:D

    return-wide v0
.end method

.method public getVertexFactory()Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    return-object v0
.end method

.method public insertSite(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->createVertex(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->insertSite(Lcom/vividsolutions/jts/triangulate/ConstraintVertex;)Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .line 379
    return-void
.end method

.method public setConstraints(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "segments"    # Ljava/util/List;
    .param p2, "segVertices"    # Ljava/util/List;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segments:Ljava/util/List;

    .line 149
    iput-object p2, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->segVertices:Ljava/util/List;

    .line 150
    return-void
.end method

.method public setSplitPointFinder(Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;)V
    .locals 0
    .param p1, "splitFinder"    # Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->splitFinder:Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;

    .line 162
    return-void
.end method

.method public setVertexFactory(Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;)V
    .locals 0
    .param p1, "vertexFactory"    # Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/ConformingDelaunayTriangulator;->vertexFactory:Lcom/vividsolutions/jts/triangulate/ConstraintVertexFactory;

    .line 191
    return-void
.end method
