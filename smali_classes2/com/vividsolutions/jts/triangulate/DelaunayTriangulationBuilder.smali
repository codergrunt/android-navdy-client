.class public Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;
.super Ljava/lang/Object;
.source "DelaunayTriangulationBuilder.java"


# instance fields
.field private siteCoords:Ljava/util/Collection;

.field private subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

.field private tolerance:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->tolerance:D

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 113
    return-void
.end method

.method private create()V
    .locals 6

    .prologue
    .line 153
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    if-eqz v3, :cond_0

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    invoke-static {v3}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->envelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 156
    .local v0, "siteEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    invoke-static {v3}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->toVertices(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 157
    .local v2, "vertices":Ljava/util/List;
    new-instance v3, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->tolerance:D

    invoke-direct {v3, v0, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;-><init>(Lcom/vividsolutions/jts/geom/Envelope;D)V

    iput-object v3, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 158
    new-instance v1, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;

    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-direct {v1, v3}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V

    .line 159
    .local v1, "triangulator":Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->insertSites(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static envelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;
    .locals 4
    .param p0, "coords"    # Ljava/util/Collection;

    .prologue
    .line 95
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    .line 96
    .local v1, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 98
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0

    .line 100
    .end local v0    # "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-object v1
.end method

.method public static extractUniqueCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateList;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 57
    if-nez p0, :cond_0

    .line 58
    new-instance v1, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 61
    :goto_0
    return-object v1

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 61
    .local v0, "coords":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v0}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->unique([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateList;

    move-result-object v1

    goto :goto_0
.end method

.method public static toVertices(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .param p0, "coords"    # Ljava/util/Collection;

    .prologue
    .line 79
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v2, "verts":Ljava/util/List;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 82
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v3, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-direct {v3, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    .end local v0    # "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-object v2
.end method

.method public static unique([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateList;
    .locals 3
    .param p0, "coords"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 66
    invoke-static {p0}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->copyDeep([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 67
    .local v1, "coordsCopy":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 68
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 69
    .local v0, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    return-object v0
.end method


# virtual methods
.method public getEdges(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->create()V

    .line 182
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getEdges(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public getSubdivision()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;
    .locals 1

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->create()V

    .line 170
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    return-object v0
.end method

.method public getTriangles(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->create()V

    .line 195
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getTriangles(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public setSites(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 124
    invoke-static {p1}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->extractUniqueCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateList;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    .line 125
    return-void
.end method

.method public setSites(Ljava/util/Collection;)V
    .locals 1
    .param p1, "coords"    # Ljava/util/Collection;

    .prologue
    .line 136
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->toCoordinateArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-static {v0}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->unique([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateList;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->siteCoords:Ljava/util/Collection;

    .line 137
    return-void
.end method

.method public setTolerance(D)V
    .locals 1
    .param p1, "tolerance"    # D

    .prologue
    .line 148
    iput-wide p1, p0, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->tolerance:D

    .line 149
    return-void
.end method
