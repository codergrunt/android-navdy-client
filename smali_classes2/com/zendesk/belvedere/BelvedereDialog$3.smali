.class Lcom/zendesk/belvedere/BelvedereDialog$3;
.super Ljava/lang/Object;
.source "BelvedereDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/belvedere/BelvedereDialog;->fillListView(Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/belvedere/BelvedereDialog;

.field final synthetic val$activity:Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/belvedere/BelvedereDialog;Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/belvedere/BelvedereDialog;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereDialog$3;->this$0:Lcom/zendesk/belvedere/BelvedereDialog;

    iput-object p2, p0, Lcom/zendesk/belvedere/BelvedereDialog$3;->val$activity:Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p1    # Landroid/widget/AdapterView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "view"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/zendesk/belvedere/BelvedereIntent;

    if-eqz v0, :cond_0

    .line 120
    iget-object v1, p0, Lcom/zendesk/belvedere/BelvedereDialog$3;->val$activity:Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/belvedere/BelvedereIntent;

    invoke-interface {v1, v0}, Lcom/zendesk/belvedere/BelvedereDialog$StartActivity;->startActivity(Lcom/zendesk/belvedere/BelvedereIntent;)V

    .line 121
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereDialog$3;->this$0:Lcom/zendesk/belvedere/BelvedereDialog;

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereDialog;->dismiss()V

    .line 123
    :cond_0
    return-void
.end method
