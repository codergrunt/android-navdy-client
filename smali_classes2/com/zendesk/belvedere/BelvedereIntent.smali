.class public Lcom/zendesk/belvedere/BelvedereIntent;
.super Ljava/lang/Object;
.source "BelvedereIntent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/zendesk/belvedere/BelvedereIntent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final intent:Landroid/content/Intent;

.field private final requestCode:I

.field private final source:Lcom/zendesk/belvedere/BelvedereSource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/zendesk/belvedere/BelvedereIntent$1;

    invoke-direct {v0}, Lcom/zendesk/belvedere/BelvedereIntent$1;-><init>()V

    sput-object v0, Lcom/zendesk/belvedere/BelvedereIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;ILcom/zendesk/belvedere/BelvedereSource;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "requestCode"    # I
    .param p3, "source"    # Lcom/zendesk/belvedere/BelvedereSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereIntent;->intent:Landroid/content/Intent;

    .line 35
    iput p2, p0, Lcom/zendesk/belvedere/BelvedereIntent;->requestCode:I

    .line 36
    iput-object p3, p0, Lcom/zendesk/belvedere/BelvedereIntent;->source:Lcom/zendesk/belvedere/BelvedereSource;

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->requestCode:I

    .line 116
    const-class v0, Lcom/zendesk/belvedere/BelvedereIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->intent:Landroid/content/Intent;

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/zendesk/belvedere/BelvedereSource;

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->source:Lcom/zendesk/belvedere/BelvedereSource;

    .line 118
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/zendesk/belvedere/BelvedereIntent$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/zendesk/belvedere/BelvedereIntent$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/zendesk/belvedere/BelvedereIntent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public getRequestCode()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->requestCode:I

    return v0
.end method

.method public getSource()Lcom/zendesk/belvedere/BelvedereSource;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->source:Lcom/zendesk/belvedere/BelvedereSource;

    return-object v0
.end method

.method public open(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 76
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->intent:Landroid/content/Intent;

    iget v1, p0, Lcom/zendesk/belvedere/BelvedereIntent;->requestCode:I

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 77
    return-void
.end method

.method public open(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 87
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->intent:Landroid/content/Intent;

    iget v1, p0, Lcom/zendesk/belvedere/BelvedereIntent;->requestCode:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 88
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "flags"    # I

    .prologue
    .line 97
    iget v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->requestCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->intent:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 99
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereIntent;->source:Lcom/zendesk/belvedere/BelvedereSource;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 100
    return-void
.end method
