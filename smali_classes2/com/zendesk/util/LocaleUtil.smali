.class public Lcom/zendesk/util/LocaleUtil;
.super Ljava/lang/Object;
.source "LocaleUtil.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final NEW_ISO_CODES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    const-class v0, Lcom/zendesk/util/LocaleUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "he"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "yi"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "id"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/zendesk/util/LocaleUtil;->NEW_ISO_CODES:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method private static createIso639Alpha3LocaleAndroid(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 12
    .param p0, "language"    # Ljava/lang/String;
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 182
    :try_start_0
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xe

    if-lt v7, v8, :cond_0

    .line 183
    const-class v7, Ljava/util/Locale;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    sget-object v10, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-class v10, Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 184
    .local v2, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Ljava/util/Locale;>;"
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 185
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p0, v7, v8

    const/4 v8, 0x2

    aput-object p1, v7, v8

    invoke-virtual {v2, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Locale;

    .line 209
    .end local v2    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Ljava/util/Locale;>;"
    :goto_0
    return-object v7

    .line 188
    :cond_0
    const-class v7, Ljava/util/Locale;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v7, v8}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 189
    .local v1, "constructor":Ljava/lang/reflect/Constructor;
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 190
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Locale;

    .line 191
    .local v6, "locale":Ljava/util/Locale;
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 193
    .local v0, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/util/Locale;>;"
    const-string v7, "languageCode"

    invoke-virtual {v0, v7}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 194
    .local v5, "languageCode":Ljava/lang/reflect/Field;
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 195
    invoke-virtual {v5, v6, p0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 197
    const-string v7, "countryCode"

    invoke-virtual {v0, v7}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 198
    .local v3, "countryCode":Ljava/lang/reflect/Field;
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 199
    invoke-virtual {v3, v6, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v6

    .line 201
    goto :goto_0

    .line 204
    .end local v0    # "aClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/util/Locale;>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;
    .end local v3    # "countryCode":Ljava/lang/reflect/Field;
    .end local v5    # "languageCode":Ljava/lang/reflect/Field;
    .end local v6    # "locale":Ljava/util/Locale;
    :catch_0
    move-exception v4

    .line 208
    .local v4, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Unable to create ISO-6390-Alpha3 per reflection"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v7, v8, v4, v9}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    .line 209
    const/4 v7, 0x0

    goto :goto_0
.end method

.method private static createIso639Alpha3LocaleJdk(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;
    .locals 9
    .param p0, "language"    # Ljava/lang/String;
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 139
    :try_start_0
    const-class v2, Ljava/util/Locale;

    const-string v4, "createConstant"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 140
    .local v1, "getInstance":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 141
    const/4 v2, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    .end local v1    # "getInstance":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 142
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Unable to create ISO-6390-Alpha3 per reflection"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2, v4, v0, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    move-object v2, v3

    .line 147
    goto :goto_0
.end method

.method public static forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;
    .locals 13
    .param p0, "localeString"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 61
    sget-object v10, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    const-string v11, "Assuming Locale.getDefault()"

    new-array v12, v9, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    .line 64
    .local v5, "locale":Ljava/util/Locale;
    invoke-static {p0}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 65
    new-instance v7, Ljava/util/StringTokenizer;

    const-string v10, "-"

    invoke-direct {v7, p0, v10}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .local v7, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v6

    .line 69
    .local v6, "numberOfTokens":I
    if-eq v6, v8, :cond_0

    if-ne v6, v1, :cond_2

    :cond_0
    move v2, v8

    .line 71
    .local v2, "hasExpectedTokenCount":Z
    :goto_0
    if-eqz v2, :cond_9

    .line 79
    if-ne v6, v8, :cond_3

    .line 83
    .local v1, "expectedStringLength":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-eq v1, v8, :cond_4

    .line 84
    sget-object v8, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    const-string v10, "number of tokens is correct but the length of the locale string does not match the expected length"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v10, v9}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    .end local v1    # "expectedStringLength":I
    .end local v2    # "hasExpectedTokenCount":Z
    .end local v6    # "numberOfTokens":I
    .end local v7    # "st":Ljava/util/StringTokenizer;
    :cond_1
    :goto_2
    return-object v5

    .restart local v6    # "numberOfTokens":I
    .restart local v7    # "st":Ljava/util/StringTokenizer;
    :cond_2
    move v2, v9

    .line 69
    goto :goto_0

    .line 79
    .restart local v2    # "hasExpectedTokenCount":Z
    :cond_3
    const/4 v1, 0x5

    goto :goto_1

    .line 86
    .restart local v1    # "expectedStringLength":I
    :cond_4
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "language":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    :goto_3
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v10}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "country":Ljava/lang/String;
    sget-object v8, Lcom/zendesk/util/LocaleUtil;->NEW_ISO_CODES:Ljava/util/List;

    invoke-interface {v8, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 90
    sget-object v8, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    const-string v10, "New ISO-6390-Alpha3 locale detected trying to create new locale per reflection"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v10, v9}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    invoke-static {v4, v0}, Lcom/zendesk/util/LocaleUtil;->createIso639Alpha3LocaleJdk(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 93
    .local v3, "iso639Alpha3Locale":Ljava/util/Locale;
    if-nez v3, :cond_5

    .line 94
    invoke-static {v4, v0}, Lcom/zendesk/util/LocaleUtil;->createIso639Alpha3LocaleAndroid(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 97
    :cond_5
    if-nez v3, :cond_7

    .line 98
    new-instance v5, Ljava/util/Locale;

    .end local v5    # "locale":Ljava/util/Locale;
    invoke-direct {v5, v4, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v5    # "locale":Ljava/util/Locale;
    goto :goto_2

    .line 87
    .end local v0    # "country":Ljava/lang/String;
    .end local v3    # "iso639Alpha3Locale":Ljava/util/Locale;
    :cond_6
    const-string v8, ""

    goto :goto_3

    .line 100
    .restart local v0    # "country":Ljava/lang/String;
    .restart local v3    # "iso639Alpha3Locale":Ljava/util/Locale;
    :cond_7
    move-object v5, v3

    goto :goto_2

    .line 104
    .end local v3    # "iso639Alpha3Locale":Ljava/util/Locale;
    :cond_8
    new-instance v5, Ljava/util/Locale;

    .end local v5    # "locale":Ljava/util/Locale;
    invoke-direct {v5, v4, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v5    # "locale":Ljava/util/Locale;
    goto :goto_2

    .line 109
    .end local v0    # "country":Ljava/lang/String;
    .end local v1    # "expectedStringLength":I
    .end local v4    # "language":Ljava/lang/String;
    :cond_9
    sget-object v8, Lcom/zendesk/util/LocaleUtil;->LOG_TAG:Ljava/lang/String;

    const-string v10, "Unexpected number of tokens, must be at least one and at most two"

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v10, v9}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;
    .locals 3
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 38
    if-eqz p0, :cond_1

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    invoke-virtual {p0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 48
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
