.class public abstract Lcom/zendesk/sdk/deeplinking/actions/Action;
.super Ljava/lang/Object;
.source "Action.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Handler::",
        "Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;",
        "Data::",
        "Lcom/zendesk/sdk/deeplinking/actions/ActionData;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mActionData:Lcom/zendesk/sdk/deeplinking/actions/ActionData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TData;"
        }
    .end annotation
.end field

.field private mActionType:Lcom/zendesk/sdk/deeplinking/actions/ActionType;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/deeplinking/actions/ActionType;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V
    .locals 0
    .param p1, "actionType"    # Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/deeplinking/actions/ActionType;",
            "TData;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/actions/Action;, "Lcom/zendesk/sdk/deeplinking/actions/Action<THandler;TData;>;"
    .local p2, "actionData":Lcom/zendesk/sdk/deeplinking/actions/ActionData;, "TData;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/zendesk/sdk/deeplinking/actions/Action;->mActionType:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    .line 19
    iput-object p2, p0, Lcom/zendesk/sdk/deeplinking/actions/Action;->mActionData:Lcom/zendesk/sdk/deeplinking/actions/ActionData;

    .line 20
    return-void
.end method


# virtual methods
.method public abstract canHandleData(Lcom/zendesk/sdk/deeplinking/actions/ActionData;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TData;)Z"
        }
    .end annotation
.end method

.method public abstract execute(Lcom/zendesk/sdk/deeplinking/actions/ActionHandler;Lcom/zendesk/sdk/deeplinking/actions/ActionData;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(THandler;TData;)V"
        }
    .end annotation
.end method

.method public getActionData()Lcom/zendesk/sdk/deeplinking/actions/ActionData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TData;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/actions/Action;, "Lcom/zendesk/sdk/deeplinking/actions/Action<THandler;TData;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/actions/Action;->mActionData:Lcom/zendesk/sdk/deeplinking/actions/ActionData;

    return-object v0
.end method

.method public getActionType()Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lcom/zendesk/sdk/deeplinking/actions/Action;, "Lcom/zendesk/sdk/deeplinking/actions/Action<THandler;TData;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/actions/Action;->mActionType:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    return-object v0
.end method
