.class public Lcom/zendesk/sdk/model/settings/HelpCenterSettings;
.super Ljava/lang/Object;
.source "HelpCenterSettings.java"


# instance fields
.field private enabled:Z

.field private locale:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLocale()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/HelpCenterSettings;->locale:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/settings/HelpCenterSettings;->enabled:Z

    return v0
.end method
