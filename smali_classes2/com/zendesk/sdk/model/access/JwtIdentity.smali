.class public Lcom/zendesk/sdk/model/access/JwtIdentity;
.super Ljava/lang/Object;
.source "JwtIdentity.java"

# interfaces
.implements Lcom/zendesk/sdk/model/access/Identity;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private token:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/zendesk/sdk/model/access/JwtIdentity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/model/access/JwtIdentity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "jwtUserIdentifier"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/zendesk/sdk/model/access/JwtIdentity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "A null or empty JWT was specified. This will not work. Please check your initialisation of JwtIdentity!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    :cond_0
    iput-object p1, p0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    if-ne p0, p1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/zendesk/sdk/model/access/JwtIdentity;

    .line 52
    .local v0, "that":Lcom/zendesk/sdk/model/access/JwtIdentity;
    iget-object v3, p0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    iget-object v4, v0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public getJwtUserIdentifier()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/model/access/JwtIdentity;->token:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
