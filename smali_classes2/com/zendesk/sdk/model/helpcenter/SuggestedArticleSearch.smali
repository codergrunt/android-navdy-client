.class public Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
.super Ljava/lang/Object;
.source "SuggestedArticleSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
    }
.end annotation


# instance fields
.field private mCategoryId:Ljava/lang/Long;

.field private mLabelNames:Ljava/lang/String;

.field private mLocale:Ljava/util/Locale;

.field private mQuery:Ljava/lang/String;

.field private mSectionId:Ljava/lang/Long;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mQuery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    .param p1, "x1"    # Ljava/util/Locale;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mLocale:Ljava/util/Locale;

    return-object p1
.end method

.method static synthetic access$302(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mLabelNames:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mCategoryId:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic access$502(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    .param p1, "x1"    # Ljava/lang/Long;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mSectionId:Ljava/lang/Long;

    return-object p1
.end method


# virtual methods
.method public getCategoryId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mCategoryId:Ljava/lang/Long;

    return-object v0
.end method

.method public getLabelNames()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mLabelNames:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->mSectionId:Ljava/lang/Long;

    return-object v0
.end method
