.class public Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;
.super Ljava/lang/Object;
.source "HelpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULT_ARTICLES_PER_SECTION:I = 0x5

.field private static final INCLUDE_ALL:Ljava/lang/String; = "categories,sections"

.field private static final INCLUDE_CATEGORIES:Ljava/lang/String; = "categories"

.field private static final INCLUDE_SECTIONS:Ljava/lang/String; = "sections"

.field private static final LOG_TAG:Ljava/lang/String; = "HelpRequest"


# instance fields
.field private articlesPerPageLimit:I

.field private categoryIds:Ljava/lang/String;

.field private includes:Ljava/lang/String;

.field private labelNames:[Ljava/lang/String;

.field private sectionIds:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->access$000(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->categoryIds:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->access$100(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->sectionIds:Ljava/lang/String;

    .line 35
    invoke-static {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->access$200(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->includes:Ljava/lang/String;

    .line 36
    invoke-static {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->access$300(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)I

    move-result v0

    iput v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->articlesPerPageLimit:I

    .line 37
    invoke-static {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->access$400(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->labelNames:[Ljava/lang/String;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .param p2, "x1"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;-><init>(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public getArticlesPerPageLimit()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->articlesPerPageLimit:I

    return v0
.end method

.method public getCategoryIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->categoryIds:Ljava/lang/String;

    return-object v0
.end method

.method public getIncludes()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->includes:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->labelNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getSectionIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->sectionIds:Ljava/lang/String;

    return-object v0
.end method
