.class public final enum Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;
.super Ljava/lang/Enum;
.source "ZendeskPicassoTransformationFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;,
        Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;,
        Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

.field public static final enum INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    sget-object v1, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    aput-object v1, v0, v2

    sput-object v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->$VALUES:[Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->$VALUES:[Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    return-object v0
.end method


# virtual methods
.method public getResizeTransformationHeight(I)Lcom/squareup/picasso/Transformation;
    .locals 1
    .param p1, "maxHeight"    # I

    .prologue
    .line 25
    new-instance v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;

    invoke-direct {v0, p0, p1}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformHeight;-><init>(Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;I)V

    return-object v0
.end method

.method public getResizeTransformationWidth(I)Lcom/squareup/picasso/Transformation;
    .locals 1
    .param p1, "maxWidth"    # I

    .prologue
    .line 21
    new-instance v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;

    invoke-direct {v0, p0, p1}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;-><init>(Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;I)V

    return-object v0
.end method

.method public getRoundedTransformation(II)Lcom/squareup/picasso/Transformation;
    .locals 1
    .param p1, "radius"    # I
    .param p2, "margin"    # I

    .prologue
    .line 29
    new-instance v0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;

    invoke-direct {v0, p0, p1, p2}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;-><init>(Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;II)V

    return-object v0
.end method
