.class Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;
.super Lcom/zendesk/sdk/ui/TextWatcherAdapter;
.source "EmailAddressAutoCompleteTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->initialise()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;->this$0:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-direct {p0}, Lcom/zendesk/sdk/ui/TextWatcherAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;->this$0:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-static {v0, p1}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->access$000(Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;->this$0:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;->this$0:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    iget-object v1, p0, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;->this$0:Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$string;->contact_fragment_email_validation_error:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
