.class public Lcom/zendesk/sdk/util/BaseInjector;
.super Ljava/lang/Object;
.source "BaseInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static injectAppId(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static injectApplicationContext(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Landroid/content/Context;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static injectConnectionSpec(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/util/List;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/network/impl/ApplicationScope;",
            ")",
            "Ljava/util/List",
            "<",
            "Lokhttp3/ConnectionSpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getSdkOptions()Lcom/zendesk/sdk/network/SdkOptions;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/SdkOptions;->getServiceOptions()Lcom/zendesk/sdk/network/SdkOptions$ServiceOptions;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/SdkOptions$ServiceOptions;->getConnectionSpecs()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static injectLocale(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/util/Locale;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public static injectOAuthToken(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getOAuthToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static injectUrl(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static injectUserAgentHeader(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getUserAgentHeader()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
