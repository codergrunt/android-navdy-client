.class public final Lcom/zendesk/sdk/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0f00ce

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0f00cf

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0f00d0

.field public static final abc_color_highlight_material:I = 0x7f0f00d2

.field public static final abc_input_method_navigation_guard:I = 0x7f0f0003

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0f00d5

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0f00d6

.field public static final abc_primary_text_material_dark:I = 0x7f0f00d7

.field public static final abc_primary_text_material_light:I = 0x7f0f00d8

.field public static final abc_search_url_text:I = 0x7f0f00d9

.field public static final abc_search_url_text_normal:I = 0x7f0f0004

.field public static final abc_search_url_text_pressed:I = 0x7f0f0005

.field public static final abc_search_url_text_selected:I = 0x7f0f0006

.field public static final abc_secondary_text_material_dark:I = 0x7f0f00da

.field public static final abc_secondary_text_material_light:I = 0x7f0f00db

.field public static final abc_tint_btn_checkable:I = 0x7f0f00dc

.field public static final abc_tint_default:I = 0x7f0f00dd

.field public static final abc_tint_edittext:I = 0x7f0f00de

.field public static final abc_tint_seek_thumb:I = 0x7f0f00df

.field public static final abc_tint_spinner:I = 0x7f0f00e0

.field public static final abc_tint_switch_thumb:I = 0x7f0f00e1

.field public static final abc_tint_switch_track:I = 0x7f0f00e2

.field public static final accent_material_dark:I = 0x7f0f0007

.field public static final accent_material_light:I = 0x7f0f0008

.field public static final article_attachment_list_divider_color:I = 0x7f0f000a

.field public static final article_attachment_row_background_color:I = 0x7f0f000b

.field public static final background_floating_material_dark:I = 0x7f0f000c

.field public static final background_floating_material_light:I = 0x7f0f000d

.field public static final background_material_dark:I = 0x7f0f000e

.field public static final background_material_light:I = 0x7f0f000f

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0f0017

.field public static final bright_foreground_disabled_material_light:I = 0x7f0f0018

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0f0019

.field public static final bright_foreground_inverse_material_light:I = 0x7f0f001a

.field public static final bright_foreground_material_dark:I = 0x7f0f001b

.field public static final bright_foreground_material_light:I = 0x7f0f001c

.field public static final button_material_dark:I = 0x7f0f001d

.field public static final button_material_light:I = 0x7f0f001e

.field public static final contact_fragment_seperator_body:I = 0x7f0f0034

.field public static final contact_fragment_seperator_border_color:I = 0x7f0f0035

.field public static final design_fab_shadow_end_color:I = 0x7f0f003a

.field public static final design_fab_shadow_mid_color:I = 0x7f0f003b

.field public static final design_fab_shadow_start_color:I = 0x7f0f003c

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0f003d

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0f003e

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0f003f

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0f0040

.field public static final design_snackbar_background_color:I = 0x7f0f0041

.field public static final design_textinput_error_color_dark:I = 0x7f0f0042

.field public static final design_textinput_error_color_light:I = 0x7f0f0043

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0f0044

.field public static final dim_foreground_disabled_material_light:I = 0x7f0f0045

.field public static final dim_foreground_material_dark:I = 0x7f0f0046

.field public static final dim_foreground_material_light:I = 0x7f0f0047

.field public static final fallback_text_color:I = 0x7f0f004f

.field public static final feedback_container:I = 0x7f0f0050

.field public static final feedback_divider:I = 0x7f0f0051

.field public static final foreground_material_dark:I = 0x7f0f0052

.field public static final foreground_material_light:I = 0x7f0f0053

.field public static final help_divider_color:I = 0x7f0f0069

.field public static final highlighted_text_material_dark:I = 0x7f0f006a

.field public static final highlighted_text_material_light:I = 0x7f0f006b

.field public static final material_blue_grey_800:I = 0x7f0f0086

.field public static final material_blue_grey_900:I = 0x7f0f0087

.field public static final material_blue_grey_950:I = 0x7f0f0088

.field public static final material_deep_teal_200:I = 0x7f0f0089

.field public static final material_deep_teal_500:I = 0x7f0f008a

.field public static final material_grey_100:I = 0x7f0f008b

.field public static final material_grey_300:I = 0x7f0f008c

.field public static final material_grey_50:I = 0x7f0f008d

.field public static final material_grey_600:I = 0x7f0f008e

.field public static final material_grey_800:I = 0x7f0f008f

.field public static final material_grey_850:I = 0x7f0f0090

.field public static final material_grey_900:I = 0x7f0f0091

.field public static final no_network_view_container_background_color:I = 0x7f0f0093

.field public static final no_network_view_separator_color:I = 0x7f0f0094

.field public static final no_network_view_text_color:I = 0x7f0f0095

.field public static final primary_dark_material_dark:I = 0x7f0f00a2

.field public static final primary_dark_material_light:I = 0x7f0f00a3

.field public static final primary_material_dark:I = 0x7f0f00a4

.field public static final primary_material_light:I = 0x7f0f00a5

.field public static final primary_text_default_material_dark:I = 0x7f0f00a6

.field public static final primary_text_default_material_light:I = 0x7f0f00a7

.field public static final primary_text_disabled_material_dark:I = 0x7f0f00a8

.field public static final primary_text_disabled_material_light:I = 0x7f0f00a9

.field public static final request_error_message:I = 0x7f0f00b4

.field public static final request_list_divider_color:I = 0x7f0f00b5

.field public static final request_list_retry_button_text_color:I = 0x7f0f00b6

.field public static final request_list_unread_indicator_color:I = 0x7f0f00b7

.field public static final request_view_comment_list_divider:I = 0x7f0f00b8

.field public static final retry_view_container_background_color:I = 0x7f0f00b9

.field public static final retry_view_separator_color:I = 0x7f0f00ba

.field public static final retry_view_text_color:I = 0x7f0f00bb

.field public static final ripple_material_dark:I = 0x7f0f00bc

.field public static final ripple_material_light:I = 0x7f0f00bd

.field public static final secondary_text_default_material_dark:I = 0x7f0f00c3

.field public static final secondary_text_default_material_light:I = 0x7f0f00c4

.field public static final secondary_text_disabled_material_dark:I = 0x7f0f00c5

.field public static final secondary_text_disabled_material_light:I = 0x7f0f00c6

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0f00c7

.field public static final switch_thumb_disabled_material_light:I = 0x7f0f00c8

.field public static final switch_thumb_material_dark:I = 0x7f0f00eb

.field public static final switch_thumb_material_light:I = 0x7f0f00ec

.field public static final switch_thumb_normal_material_dark:I = 0x7f0f00c9

.field public static final switch_thumb_normal_material_light:I = 0x7f0f00ca


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
