.class final Lcom/zendesk/sdk/support/SupportUiConfig$1;
.super Ljava/lang/Object;
.source "SupportUiConfig.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportUiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/zendesk/sdk/support/SupportUiConfig;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/zendesk/sdk/support/SupportUiConfig;
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 141
    new-instance v0, Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-direct {v0, p1}, Lcom/zendesk/sdk/support/SupportUiConfig;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/SupportUiConfig$1;->createFromParcel(Landroid/os/Parcel;)Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/zendesk/sdk/support/SupportUiConfig;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 146
    new-array v0, p1, [Lcom/zendesk/sdk/support/SupportUiConfig;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/SupportUiConfig$1;->newArray(I)[Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v0

    return-object v0
.end method
