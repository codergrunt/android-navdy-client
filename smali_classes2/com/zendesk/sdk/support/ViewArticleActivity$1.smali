.class Lcom/zendesk/sdk/support/ViewArticleActivity$1;
.super Landroid/webkit/WebViewClient;
.source "ViewArticleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/ViewArticleActivity;->setupRequestInterceptor()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

.field final synthetic val$deepLinkingParser:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;

.field final synthetic val$httpClient:Lokhttp3/OkHttpClient;

.field final synthetic val$interceptRequests:Z


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/ViewArticleActivity;ZLokhttp3/OkHttpClient;Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    iput-boolean p2, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->val$interceptRequests:Z

    iput-object p3, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->val$httpClient:Lokhttp3/OkHttpClient;

    iput-object p4, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->val$deepLinkingParser:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 16
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 220
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->val$interceptRequests:Z

    if-nez v11, :cond_0

    .line 221
    invoke-static {}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$000()Ljava/lang/String;

    move-result-object v11

    const-string v12, "Cannot add request interceptor because sdk options prohibit it. See ZendeskConfig.INSTANCE.getSdkOptions()"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v13}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    invoke-super/range {p0 .. p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v11

    .line 273
    :goto_0
    return-object v11

    .line 225
    :cond_0
    sget-object v11, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v11}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getZendeskUrl()Ljava/lang/String;

    move-result-object v10

    .line 227
    .local v10, "zendeskUrl":Ljava/lang/String;
    invoke-static {v10}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 228
    :cond_1
    invoke-static {}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$000()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Will not intercept request because the url is not hosted by Zendesk"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v13}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-super/range {p0 .. p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v11

    goto :goto_0

    .line 232
    :cond_2
    sget-object v11, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v11}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v11

    invoke-interface {v11}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v11

    invoke-interface {v11}, Lcom/zendesk/sdk/storage/IdentityStorage;->getStoredAccessTokenAsBearerToken()Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "bearerToken":Ljava/lang/String;
    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 235
    invoke-static {}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$000()Ljava/lang/String;

    move-result-object v11

    const-string v12, "We will not intercept the request there is no authentication present"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v13}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    invoke-super/range {p0 .. p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v11

    goto :goto_0

    .line 239
    :cond_3
    const/4 v3, 0x0

    .line 240
    .local v3, "content":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 241
    .local v5, "contentType":Ljava/lang/String;
    const/4 v4, 0x0

    .line 245
    .local v4, "contentEncoding":Ljava/lang/String;
    :try_start_0
    new-instance v11, Lokhttp3/Request$Builder;

    invoke-direct {v11}, Lokhttp3/Request$Builder;-><init>()V

    .line 246
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v11

    .line 247
    invoke-virtual {v11}, Lokhttp3/Request$Builder;->get()Lokhttp3/Request$Builder;

    move-result-object v11

    const-string v12, "Authorization"

    .line 248
    invoke-virtual {v11, v12, v1}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v11

    .line 249
    invoke-virtual {v11}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v8

    .line 250
    .local v8, "request":Lokhttp3/Request;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->val$httpClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v11, v8}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v11

    invoke-interface {v11}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v9

    .line 252
    .local v9, "response":Lokhttp3/Response;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Lokhttp3/Response;->isSuccessful()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v9}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v11

    if-eqz v11, :cond_5

    .line 253
    invoke-virtual {v9}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v11

    invoke-virtual {v11}, Lokhttp3/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object v3

    .line 255
    invoke-virtual {v9}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v11

    invoke-virtual {v11}, Lokhttp3/ResponseBody;->contentType()Lokhttp3/MediaType;

    move-result-object v7

    .line 256
    .local v7, "mediaType":Lokhttp3/MediaType;
    if-eqz v7, :cond_5

    .line 258
    invoke-virtual {v7}, Lokhttp3/MediaType;->type()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v7}, Lokhttp3/MediaType;->subtype()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 259
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "%s/%s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v7}, Lokhttp3/MediaType;->type()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-virtual {v7}, Lokhttp3/MediaType;->subtype()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 262
    :cond_4
    invoke-virtual {v7}, Lokhttp3/MediaType;->charset()Ljava/nio/charset/Charset;

    move-result-object v2

    .line 263
    .local v2, "charset":Ljava/nio/charset/Charset;
    if-eqz v2, :cond_5

    .line 264
    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 273
    .end local v2    # "charset":Ljava/nio/charset/Charset;
    .end local v7    # "mediaType":Lokhttp3/MediaType;
    .end local v8    # "request":Lokhttp3/Request;
    .end local v9    # "response":Lokhttp3/Response;
    :cond_5
    :goto_1
    new-instance v11, Landroid/webkit/WebResourceResponse;

    invoke-direct {v11, v5, v4, v3}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto/16 :goto_0

    .line 269
    :catch_0
    move-exception v6

    .line 270
    .local v6, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$000()Ljava/lang/String;

    move-result-object v11

    const-string v12, "Exception encountered when trying to intercept request"

    const/4 v13, 0x0

    new-array v13, v13, [Ljava/lang/Object;

    invoke-static {v11, v12, v6, v13}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 281
    iget-object v5, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->val$deepLinkingParser:Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;

    iget-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-virtual {v5, p2, v6}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->parse(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 282
    .local v2, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$1;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    .line 284
    .local v0, "context":Landroid/content/Context;
    if-eqz v2, :cond_0

    .line 286
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :goto_0
    return v3

    .line 288
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-static {}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Could not open Intent: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v5, v6, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    move v3, v4

    .line 293
    goto :goto_0
.end method
