.class Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;
.super Lcom/zendesk/service/ZendeskCallback;
.source "HelpAdapterPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->loadMoreArticles(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

.field final synthetic val$loadMoreRetryAction:Lcom/zendesk/sdk/network/RetryAction;

.field final synthetic val$section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

.field final synthetic val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;Lcom/zendesk/sdk/network/RetryAction;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    iput-object p3, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    iput-object p4, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$loadMoreRetryAction:Lcom/zendesk/sdk/network/RetryAction;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 4
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 323
    const-string v0, "HelpAdapterPresenter"

    const-string v1, "Failed to load more articles"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 325
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$800(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    move-result-object v0

    sget-object v1, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->ARTICLES_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$loadMoreRetryAction:Lcom/zendesk/sdk/network/RetryAction;

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->onErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V

    .line 326
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 285
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 289
    .local p1, "itemsForSection":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 290
    .local v2, "positionInFullList":I
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 292
    .local v1, "positionInFilteredList":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .line 293
    .local v0, "item":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v6}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 295
    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v6}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v6

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "positionInFullList":I
    .local v3, "positionInFullList":I
    invoke-interface {v6, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 297
    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    invoke-virtual {v6, v0}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->addChild(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V

    .line 299
    const/4 v6, -0x1

    if-eq v1, v6, :cond_2

    .line 301
    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v6}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 303
    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v6}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$700(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/help/HelpMvp$View;

    move-result-object v6

    invoke-interface {v6, v1, v0}, Lcom/zendesk/sdk/support/help/HelpMvp$View;->addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V

    .line 304
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    .end local v3    # "positionInFullList":I
    .restart local v2    # "positionInFullList":I
    goto :goto_0

    .line 310
    .end local v0    # "item":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    :cond_1
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v5, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 311
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v5, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 313
    .local v4, "removedItemPosition":I
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v5, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 315
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$section:Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->val$seeAllItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-virtual {v5, v6}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->removeChild(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V

    .line 317
    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$700(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/help/HelpMvp$View;

    move-result-object v5

    invoke-interface {v5, v4}, Lcom/zendesk/sdk/support/help/HelpMvp$View;->removeItem(I)V

    .line 318
    return-void

    .end local v2    # "positionInFullList":I
    .end local v4    # "removedItemPosition":I
    .restart local v0    # "item":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .restart local v3    # "positionInFullList":I
    :cond_2
    move v2, v3

    .end local v3    # "positionInFullList":I
    .restart local v2    # "positionInFullList":I
    goto :goto_0
.end method
