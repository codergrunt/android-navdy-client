.class Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "HelpSearchRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$PaddingViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$NoResultsViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "HelpSearchRecyclerViewAdapter"

.field private static final TYPE_ARTICLE:I = 0x213

.field private static final TYPE_NO_RESULTS:I = 0x1b9

.field private static final TYPE_PADDING:I = 0x1a7


# instance fields
.field private addEndPadding:Z

.field private query:Ljava/lang/String;

.field private resultsCleared:Z

.field private searchArticles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 1
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "addEndPadding"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .local p1, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 39
    iput-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->resultsCleared:Z

    .line 41
    iput-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->addEndPadding:Z

    .line 50
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    .line 51
    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->query:Ljava/lang/String;

    .line 52
    iput-boolean p3, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->addEndPadding:Z

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->query:Ljava/lang/String;

    return-object v0
.end method

.method private getPaddingExtraItem()I
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->addEndPadding:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method clearResults()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->resultsCleared:Z

    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->query:Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 76
    return-void
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->resultsCleared:Z

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->getPaddingExtraItem()I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 122
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const/16 v0, 0x1b9

    .line 127
    :goto_0
    return v0

    .line 124
    :cond_0
    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 125
    const/16 v0, 0x1a7

    goto :goto_0

    .line 127
    :cond_1
    const/16 v0, 0x213

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 101
    const/16 v0, 0x213

    invoke-virtual {p0, p2}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->getItemViewType(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 102
    check-cast p1, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;

    .end local p1    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;

    invoke-virtual {p1, v0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->bindTo(Lcom/zendesk/sdk/model/helpcenter/SearchArticle;)V

    .line 104
    :cond_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v3, 0x0

    .line 81
    sparse-switch p2, :sswitch_data_0

    .line 96
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 83
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$layout;->row_search_article:I

    .line 84
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;Landroid/view/View;Landroid/content/Context;)V

    goto :goto_0

    .line 87
    .end local v0    # "view":Landroid/view/View;
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$layout;->row_no_articles_found:I

    .line 88
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 89
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$NoResultsViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$NoResultsViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 91
    .end local v0    # "view":Landroid/view/View;
    :sswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$layout;->row_padding:I

    .line 92
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 93
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$PaddingViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$PaddingViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x1a7 -> :sswitch_2
        0x1b9 -> :sswitch_1
        0x213 -> :sswitch_0
    .end sparse-switch
.end method

.method update(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->resultsCleared:Z

    .line 63
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->searchArticles:Ljava/util/List;

    .line 64
    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->query:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 66
    return-void
.end method
