.class Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
.super Ljava/lang/Object;
.source "HelpAdapterPresenter.java"

# interfaces
.implements Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "HelpAdapterPresenter"

.field private static final RETRY_ACTION_ID:Ljava/lang/Integer;

.field private static final TAG:Ljava/lang/String; = "HelpAdapterPresenter"


# instance fields
.field private callback:Lcom/zendesk/service/ZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private contentPresenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

.field private filteredItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation
.end field

.field private hasError:Z

.field private helpItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/zendesk/sdk/support/help/HelpMvp$Model;

.field private networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

.field private noResults:Z

.field private retryAction:Lcom/zendesk/sdk/network/RetryAction;

.field private supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

.field private view:Lcom/zendesk/sdk/support/help/HelpMvp$View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/zendesk/sdk/support/help/HelpMvp$View;Lcom/zendesk/sdk/support/help/HelpMvp$Model;Lcom/zendesk/sdk/network/NetworkInfoProvider;Lcom/zendesk/sdk/support/SupportUiConfig;)V
    .locals 1
    .param p1, "view"    # Lcom/zendesk/sdk/support/help/HelpMvp$View;
    .param p2, "model"    # Lcom/zendesk/sdk/support/help/HelpMvp$Model;
    .param p3, "networkInfoProvider"    # Lcom/zendesk/sdk/network/NetworkInfoProvider;
    .param p4, "supportUiConfig"    # Lcom/zendesk/sdk/support/SupportUiConfig;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->helpItems:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    .line 171
    new-instance v0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;-><init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)V

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->callback:Lcom/zendesk/service/ZendeskCallback;

    .line 62
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->view:Lcom/zendesk/sdk/support/help/HelpMvp$View;

    .line 63
    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->model:Lcom/zendesk/sdk/support/help/HelpMvp$Model;

    .line 64
    iput-object p3, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    .line 65
    iput-object p4, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->requestHelpContent()V

    return-void
.end method

.method static synthetic access$102(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->hasError:Z

    return p1
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->helpItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->helpItems:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportUiConfig;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$402(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->getCollapsedCategories(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$602(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->noResults:Z

    return p1
.end method

.method static synthetic access$700(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/help/HelpMvp$View;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->view:Lcom/zendesk/sdk/support/help/HelpMvp$View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->contentPresenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->loadMoreArticles(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V

    return-void
.end method

.method private addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .prologue
    .line 263
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 264
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->view:Lcom/zendesk/sdk/support/help/HelpMvp$View;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/support/help/HelpMvp$View;->addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V

    .line 265
    return-void
.end method

.method private collapseItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt p1, v1, :cond_1

    .line 260
    :cond_0
    return-void

    .line 254
    :cond_1
    add-int/lit8 v0, p1, 0x1

    .line 256
    .local v0, "positionToRemoveFrom":I
    :goto_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    .line 257
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    invoke-interface {v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getViewType()I

    move-result v1

    if-eq v2, v1, :cond_0

    .line 258
    invoke-direct {p0, v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->removeItem(I)V

    goto :goto_0
.end method

.method private expandItem(Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)V
    .locals 10
    .param p1, "categoryItem"    # Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
    .param p2, "position"    # I

    .prologue
    .line 233
    add-int/lit8 v3, p2, 0x1

    .line 234
    .local v3, "positionToAddAt":I
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->getChildren()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .line 235
    .local v1, "categoryChildItem":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    invoke-direct {p0, v3, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V

    .line 236
    add-int/lit8 v3, v3, 0x1

    .line 238
    :try_start_0
    move-object v0, v1

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    move-object v5, v0

    .line 239
    .local v5, "sectionItem":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getChildren()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .line 240
    .local v4, "sectionChildItem":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    invoke-direct {p0, v3, v4}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    add-int/lit8 v3, v3, 0x1

    .line 242
    goto :goto_1

    .line 243
    .end local v4    # "sectionChildItem":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .end local v5    # "sectionItem":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    :catch_0
    move-exception v2

    .line 244
    .local v2, "ex":Ljava/lang/ClassCastException;
    const-string v7, "HelpAdapterPresenter"

    const-string v8, "Error expanding item"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v2, v9}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0

    .line 247
    .end local v1    # "categoryChildItem":Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .end local v2    # "ex":Ljava/lang/ClassCastException;
    :cond_1
    return-void
.end method

.method private getCollapsedCategories(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v0, "collapsedCategories":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 229
    :cond_0
    return-object v0

    .line 223
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .local v1, "count":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 224
    const/4 v4, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    invoke-interface {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getViewType()I

    move-result v3

    if-ne v4, v3, :cond_2

    .line 225
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->setExpanded(Z)Z

    .line 223
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getPaddingItemCount()I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportUiConfig;->isAddListPaddingBottom()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadMoreArticles(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V
    .locals 5
    .param p1, "seeAllItem"    # Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    .prologue
    .line 274
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->getSection()Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    move-result-object v1

    .line 276
    .local v1, "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    new-instance v0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$3;

    invoke-direct {v0, p0, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$3;-><init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V

    .line 283
    .local v0, "loadMoreRetryAction":Lcom/zendesk/sdk/network/RetryAction;
    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v2}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->isNetworkAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->model:Lcom/zendesk/sdk/support/help/HelpMvp$Model;

    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v3}, Lcom/zendesk/sdk/support/SupportUiConfig;->getLabelNames()[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;

    invoke-direct {v4, p0, p1, v1, v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$4;-><init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;Lcom/zendesk/sdk/network/RetryAction;)V

    invoke-interface {v2, v1, v3, v4}, Lcom/zendesk/sdk/support/help/HelpMvp$Model;->getArticlesForSection(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 332
    :goto_0
    return-void

    .line 329
    :cond_0
    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->retryAction:Lcom/zendesk/sdk/network/RetryAction;

    .line 330
    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v3, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->retryAction:Lcom/zendesk/sdk/network/RetryAction;

    invoke-interface {v2, v3, v4}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->addRetryAction(Ljava/lang/Integer;Lcom/zendesk/sdk/network/RetryAction;)V

    goto :goto_0
.end method

.method private removeItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 268
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 269
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->view:Lcom/zendesk/sdk/support/help/HelpMvp$View;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/support/help/HelpMvp$View;->removeItem(I)V

    .line 270
    return-void
.end method

.method private requestHelpContent()V
    .locals 5

    .prologue
    .line 157
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->model:Lcom/zendesk/sdk/support/help/HelpMvp$Model;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/support/SupportUiConfig;->getCategoryIds()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/support/SupportUiConfig;->getSectionIds()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->supportUiConfig:Lcom/zendesk/sdk/support/SupportUiConfig;

    .line 159
    invoke-virtual {v3}, Lcom/zendesk/sdk/support/SupportUiConfig;->getLabelNames()[Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->callback:Lcom/zendesk/service/ZendeskCallback;

    .line 158
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/support/help/HelpMvp$Model;->getArticles(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 169
    :goto_0
    return-void

    .line 161
    :cond_0
    new-instance v0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$1;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$1;-><init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)V

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->retryAction:Lcom/zendesk/sdk/network/RetryAction;

    .line 167
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v1, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->retryAction:Lcom/zendesk/sdk/network/RetryAction;

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->addRetryAction(Ljava/lang/Integer;Lcom/zendesk/sdk/network/RetryAction;)V

    goto :goto_0
.end method


# virtual methods
.method public getItem(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    return-object v0
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->hasError:Z

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 124
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->getPaddingItemCount()I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getItemForBinding(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .locals 2
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 148
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    .line 149
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    :cond_0
    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->noResults:Z

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x7

    .line 139
    :goto_0
    return v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    .line 139
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    invoke-interface {v0}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getViewType()I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public onAttached()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->register()V

    .line 77
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->helpItems:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->requestHelpContent()V

    .line 80
    :cond_0
    return-void
.end method

.method public onCategoryClick(Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)Z
    .locals 3
    .param p1, "categoryItem"    # Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
    .param p2, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 90
    if-nez p1, :cond_0

    .line 102
    :goto_0
    return v1

    .line 94
    :cond_0
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->isExpanded()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {p1, v1}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->setExpanded(Z)Z

    move-result v0

    .line 96
    .local v0, "expanded":Z
    if-eqz v0, :cond_2

    .line 97
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->expandItem(Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)V

    :goto_1
    move v1, v0

    .line 102
    goto :goto_0

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->filteredItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->collapseItem(I)V

    goto :goto_1
.end method

.method public onDetached()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v1, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->removeRetryAction(Ljava/lang/Integer;)V

    .line 85
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->unregister()V

    .line 86
    return-void
.end method

.method public onSeeAllClick(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V
    .locals 0
    .param p1, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->loadMoreArticles(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V

    .line 108
    return-void
.end method

.method public setContentPresenter(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V
    .locals 0
    .param p1, "presenter"    # Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->contentPresenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    .line 71
    return-void
.end method
