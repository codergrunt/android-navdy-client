.class public interface abstract Lcom/zendesk/sdk/network/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACCEPT_HEADER:Ljava/lang/String; = "Accept"

.field public static final APPLICATION_JSON:Ljava/lang/String; = "application/json"

.field public static final AUTHORIZATION_HEADER:Ljava/lang/String; = "Authorization"

.field public static final CLIENT_IDENTIFIER_HEADER:Ljava/lang/String; = "Client-Identifier"

.field public static final ENVIRONMENT_DEBUG:Ljava/lang/String; = "Development"

.field public static final ENVIRONMENT_PRODUCTION:Ljava/lang/String; = "Production"

.field public static final HEADER_SUFFIX_FORMAT:Ljava/lang/String; = " %s/%s"

.field public static final HEADER_SUFFIX_MAX_LENGTH:I = 0x400

.field public static final SDK_GUID_HEADER:Ljava/lang/String; = "Mobile-Sdk-Identity"

.field public static final USER_AGENT_HEADER:Ljava/lang/String; = "User-Agent"

.field public static final ZENDESK_SDK_FOR_ANDROID:Ljava/lang/String; = "Zendesk-SDK/%s Android/%d Env/%s"
