.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->searchArticles(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 12
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 249
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getInclude()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "categories"

    aput-object v1, v0, v6

    const-string v1, "sections"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string v2, "users"

    aput-object v2, v0, v1

    .line 251
    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 254
    .local v4, "include":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getLabelNames()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x0

    .line 258
    .local v5, "labelNames":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getLocale()Ljava/util/Locale;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .line 259
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->getBestLocale(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Ljava/util/Locale;

    move-result-object v3

    .line 262
    .local v3, "locale":Ljava/util/Locale;
    :goto_2
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v0

    .line 263
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getQuery()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    .line 264
    invoke-virtual {v6}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getCategoryIds()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getSectionIds()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-virtual {v8}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getPage()Ljava/lang/Integer;

    move-result-object v8

    iget-object v9, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    .line 265
    invoke-virtual {v9}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getPerPage()Ljava/lang/Integer;

    move-result-object v9

    new-instance v10, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;

    iget-object v11, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v10, p0, v11}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;Lcom/zendesk/service/ZendeskCallback;)V

    .line 262
    invoke-virtual/range {v0 .. v10}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->searchArticles(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/zendesk/service/ZendeskCallback;)V

    .line 288
    .end local v3    # "locale":Ljava/util/Locale;
    .end local v4    # "include":Ljava/lang/String;
    .end local v5    # "labelNames":Ljava/lang/String;
    :cond_0
    return-void

    .line 251
    :cond_1
    new-array v0, v7, [Ljava/lang/String;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    .line 252
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getInclude()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 254
    .restart local v4    # "include":Ljava/lang/String;
    :cond_2
    new-array v0, v7, [Ljava/lang/String;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    .line 256
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getLabelNames()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 259
    .restart local v5    # "labelNames":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->val$search:Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    .line 260
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->getLocale()Ljava/util/Locale;

    move-result-object v3

    goto :goto_2
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 246
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
