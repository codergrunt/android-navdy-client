.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$6;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getCategoryById(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;",
        "Lcom/zendesk/sdk/model/helpcenter/Category;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$6;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;)Lcom/zendesk/sdk/model/helpcenter/Category;
    .locals 1
    .param p1, "data"    # Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;

    .prologue
    .line 260
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;->getCategory()Lcom/zendesk/sdk/model/helpcenter/Category;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 257
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$6;->extract(Lcom/zendesk/sdk/model/helpcenter/CategoryResponse;)Lcom/zendesk/sdk/model/helpcenter/Category;

    move-result-object v0

    return-object v0
.end method
