.class public final enum Lcom/zendesk/sdk/network/impl/ZendeskConfig;
.super Ljava/lang/Enum;
.source "ZendeskConfig.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/network/impl/ZendeskConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/network/impl/ZendeskConfig;

.field public static final HEADER_SUFFIX_MAX_LENGTH:I = 0x400

.field public static final enum INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskConfiguration"

.field private static final SLASH:Ljava/lang/String; = "/"


# instance fields
.field private applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

.field private initialised:Z

.field private uaHeaderSuffixes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    aput-object v1, v0, v2

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->$VALUES:[Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initWithStubs()V

    .line 58
    return-void
.end method

.method private buildUserAgentHeader()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 496
    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 497
    invoke-virtual {v3}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isDevelopment()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "Development"

    .line 501
    .local v0, "environment":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Zendesk-SDK/%s Android/%d Env/%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "1.7.2.1"

    aput-object v6, v5, v8

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 504
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    aput-object v0, v5, v10

    .line 501
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 507
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    invoke-static {v3}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 508
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 509
    .local v2, "suffix":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, " %s/%s"

    new-array v6, v10, [Ljava/lang/Object;

    iget-object v7, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v7, v6, v8

    iget-object v7, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 497
    .end local v0    # "environment":Ljava/lang/String;
    .end local v1    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v2    # "suffix":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    const-string v0, "Production"

    goto :goto_0

    .line 513
    .restart local v0    # "environment":Ljava/lang/String;
    .restart local v1    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V
    .locals 2
    .param p1, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 547
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .line 548
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigInjector;->injectZendeskConfigHelper(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    .line 550
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v0

    .line 551
    .local v0, "sdkStorage":Lcom/zendesk/sdk/storage/SdkStorage;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/storage/SdkStorage;->registerUserStorage(Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;)V

    .line 552
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/storage/SdkStorage;->registerUserStorage(Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;)V

    .line 553
    return-void
.end method

.method private initWithStubs()V
    .locals 5

    .prologue
    .line 556
    new-instance v0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    const/4 v1, 0x0

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .line 559
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfigInjector;->injectStubZendeskConfigHelper(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    .line 560
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    .line 561
    return-void
.end method

.method private suffixContainsInvalidCharacter(Landroid/util/Pair;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 461
    .local p1, "suffix":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/zendesk/util/StringUtils;->LINE_SEPARATOR:Ljava/lang/String;

    .line 462
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/zendesk/util/StringUtils;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private suffixWouldMakeHeaderTooLong(Landroid/util/Pair;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "suffix":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v7, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 466
    const-string v4, " %s/%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v6, v5, v2

    iget-object v6, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 469
    .local v1, "formattedSuffix":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getUserAgentHeader()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    move v0, v2

    .line 471
    .local v0, "currentHeaderLength":I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v7, :cond_0

    .line 472
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v0

    if-lt v4, v7, :cond_1

    :cond_0
    move v2, v3

    :cond_1
    return v2

    .line 469
    .end local v0    # "currentHeaderLength":I
    :cond_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getUserAgentHeader()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ZendeskConfig;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/network/impl/ZendeskConfig;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->$VALUES:[Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/network/impl/ZendeskConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    return-object v0
.end method


# virtual methods
.method addUserAgentHeaderSuffix(Landroid/util/Pair;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "suffix":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 435
    if-eqz p1, :cond_0

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v3, :cond_1

    .line 436
    :cond_0
    const-string v2, "ZendeskConfiguration"

    const-string v3, "User agent header suffix must be a valid non-null name-value pair. Suffix not added."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :goto_0
    return v1

    .line 439
    :cond_1
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->suffixContainsInvalidCharacter(Landroid/util/Pair;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 440
    const-string v2, "ZendeskConfiguration"

    const-string v3, "Neither element of the suffix Pair can contain a slash or a new line character. Suffix not added."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 444
    :cond_2
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->suffixWouldMakeHeaderTooLong(Landroid/util/Pair;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 445
    const-string v2, "ZendeskConfiguration"

    const-string v3, "Header must be less than 1024 characters. Suffix not added."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 449
    :cond_3
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    if-nez v1, :cond_4

    .line 450
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    .line 453
    :cond_4
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->buildUserAgentHeader()Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, "userAgentHeader":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    move v1, v2

    .line 457
    goto :goto_0
.end method

.method clearUserAgentHeaderSuffixes()V
    .locals 2

    .prologue
    .line 479
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 480
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 483
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->buildUserAgentHeader()Ljava/lang/String;

    move-result-object v0

    .line 484
    .local v0, "userAgentHeader":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 485
    return-void
.end method

.method public disablePush(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 389
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-boolean v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    if-eqz v1, :cond_1

    .line 390
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getProviderStore()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/impl/ProviderStore;->pushRegistrationProvider()Lcom/zendesk/sdk/network/PushRegistrationProvider;

    move-result-object v0

    .line 391
    .local v0, "pushRegistrationProvider":Lcom/zendesk/sdk/network/PushRegistrationProvider;
    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/network/PushRegistrationProvider;->unregisterDevice(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 397
    .end local v0    # "pushRegistrationProvider":Lcom/zendesk/sdk/network/PushRegistrationProvider;
    :cond_0
    :goto_0
    return-void

    .line 393
    :cond_1
    if-eqz p2, :cond_0

    .line 394
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "Zendesk not initialized, skipping disablePush()"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method public enablePushWithIdentifier(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 355
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/push/PushRegistrationResponse;>;"
    iget-boolean v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    if-eqz v1, :cond_1

    .line 356
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getProviderStore()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/impl/ProviderStore;->pushRegistrationProvider()Lcom/zendesk/sdk/network/PushRegistrationProvider;

    move-result-object v0

    .line 357
    .local v0, "pushProvider":Lcom/zendesk/sdk/network/PushRegistrationProvider;
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getDeviceLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2}, Lcom/zendesk/sdk/network/PushRegistrationProvider;->registerDeviceWithIdentifier(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V

    .line 363
    .end local v0    # "pushProvider":Lcom/zendesk/sdk/network/PushRegistrationProvider;
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    if-eqz p2, :cond_0

    .line 360
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "Zendesk not initialized, skipping enablePush()"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method public enablePushWithUAChannelId(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 372
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/push/PushRegistrationResponse;>;"
    iget-boolean v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    if-eqz v1, :cond_1

    .line 373
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getProviderStore()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/impl/ProviderStore;->pushRegistrationProvider()Lcom/zendesk/sdk/network/PushRegistrationProvider;

    move-result-object v0

    .line 374
    .local v0, "pushProvider":Lcom/zendesk/sdk/network/PushRegistrationProvider;
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getDeviceLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-interface {v0, p1, v1, p2}, Lcom/zendesk/sdk/network/PushRegistrationProvider;->registerDeviceWithUAChannelId(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V

    .line 380
    .end local v0    # "pushProvider":Lcom/zendesk/sdk/network/PushRegistrationProvider;
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    if-eqz p2, :cond_0

    .line 377
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "Zendesk not initialized, skipping enablePush()"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method public getApplicationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getAppId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCustomFields()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getCustomFields()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getDeviceLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .locals 2

    .prologue
    .line 161
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->sdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->getStoredSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    .line 162
    .local v0, "mobileSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    if-nez v0, :cond_0

    new-instance v0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .end local v0    # "mobileSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    new-instance v1, Lcom/zendesk/sdk/model/settings/MobileSettings;

    invoke-direct {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;-><init>()V

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;-><init>(Lcom/zendesk/sdk/model/settings/MobileSettings;)V

    :cond_0
    return-object v0
.end method

.method public getOauthClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getOAuthToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSdkOptions()Lcom/zendesk/sdk/network/SdkOptions;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getSdkOptions()Lcom/zendesk/sdk/network/SdkOptions;

    move-result-object v0

    return-object v0
.end method

.method public getTicketFormId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 282
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getTicketFormId()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method getUserAgentHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getUserAgentHeader()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getZendeskUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public init(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "zendeskUrl"    # Ljava/lang/String;
    .param p3, "applicationId"    # Ljava/lang/String;
    .param p4, "oauthClientId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    .line 89
    iget-boolean v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    if-eqz v1, :cond_0

    .line 90
    const-string v0, "ZendeskConfig is already initialised, skipping reinitialisation."

    .line 91
    .local v0, "message":Ljava/lang/String;
    const-string v1, "ZendeskConfiguration"

    const-string v2, "ZendeskConfig is already initialised, skipping reinitialisation."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->buildUserAgentHeader()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 118
    .end local v0    # "message":Ljava/lang/String;
    :goto_0
    return-void

    .line 97
    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    invoke-static {p3}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    invoke-static {p4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    new-instance v1, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .line 102
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, p2, p3, p4}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->buildUserAgentHeader()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v1

    .line 101
    invoke-direct {p0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 108
    iput-boolean v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    goto :goto_0

    .line 111
    :cond_1
    const-string v4, "ZendeskConfiguration"

    const-string v5, "Invalid configuration provided | Context: %b | Url: %b | Application Id: %b | OauthClientId: %b"

    new-instance v6, Lcom/zendesk/sdk/network/impl/ConfigurationRuntimeException;

    const-string v1, "Could not validate minimal configuration"

    invoke-direct {v6, v1}, Lcom/zendesk/sdk/network/impl/ConfigurationRuntimeException;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x4

    new-array v7, v1, [Ljava/lang/Object;

    if-eqz p1, :cond_2

    move v1, v2

    .line 115
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v7, v3

    invoke-static {p2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v7, v2

    const/4 v1, 0x2

    invoke-static {p3}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v1

    const/4 v1, 0x3

    invoke-static {p4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v1

    .line 111
    invoke-static {v4, v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public isAuthenticationAvailable()Z
    .locals 2

    .prologue
    .line 177
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v0

    .line 178
    .local v0, "identity":Lcom/zendesk/sdk/model/access/Identity;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCoppaEnabled()Z
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->isCoppaEnabled()Z

    move-result v0

    return v0
.end method

.method isDevelopment()Z
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->isDevelopmentMode()Z

    move-result v0

    return v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    return v0
.end method

.method public provider()Lcom/zendesk/sdk/network/impl/ProviderStore;
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getProviderStore()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    return-object v0
.end method

.method reset(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 575
    const-string v0, "ZendeskConfiguration"

    const-string v1, "Tearing down ZendeskConfig"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->uaHeaderSuffixes:Ljava/util/List;

    .line 578
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkStorage;->clearUserData()V

    .line 579
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initWithStubs()V

    .line 580
    return-void
.end method

.method public setCoppaEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 333
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/IdentityStorage;->anonymiseIdentity()Lcom/zendesk/sdk/model/access/Identity;

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->coppa(Z)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 337
    return-void
.end method

.method public setCustomFields(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 250
    .local p1, "customFields":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/CustomField;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->customFields(Ljava/util/List;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 251
    return-void
.end method

.method public setDeviceLocale(Ljava/util/Locale;)V
    .locals 4
    .param p1, "deviceLocale"    # Ljava/util/Locale;

    .prologue
    const/4 v3, 0x0

    .line 298
    iget-boolean v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initialised:Z

    if-eqz v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->locale(Ljava/util/Locale;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 300
    const-string v1, "ZendeskConfiguration"

    const-string v2, "Locale has been overridden, requesting new settings."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getProviderStore()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/impl/ProviderStore;->sdkSettingsProvider()Lcom/zendesk/sdk/network/SdkSettingsProvider;

    move-result-object v0

    .line 302
    .local v0, "provider":Lcom/zendesk/sdk/network/SdkSettingsProvider;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/SdkSettingsProvider;->getSettings(Lcom/zendesk/service/ZendeskCallback;)V

    .line 307
    .end local v0    # "provider":Lcom/zendesk/sdk/network/SdkSettingsProvider;
    :goto_0
    return-void

    .line 304
    :cond_0
    const-string v1, "ZendeskConfiguration"

    const-string v2, "Locale cannot be set before SDK has been initialised. init() must be called before setDeviceLocale()."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setIdentity(Lcom/zendesk/sdk/model/access/Identity;)V
    .locals 11
    .param p1, "identity"    # Lcom/zendesk/sdk/model/access/Identity;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 200
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v6

    invoke-interface {v6}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v3

    .line 201
    .local v3, "identityStorage":Lcom/zendesk/sdk/storage/IdentityStorage;
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v6

    invoke-interface {v6}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v6

    invoke-interface {v6}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v0

    .line 203
    .local v0, "currentIdentity":Lcom/zendesk/sdk/model/access/Identity;
    if-nez v0, :cond_1

    .line 204
    const-string v4, "ZendeskConfiguration"

    const-string v6, "No previous identity set, storing identity"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v6, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    invoke-interface {v3, p1}, Lcom/zendesk/sdk/storage/IdentityStorage;->storeIdentity(Lcom/zendesk/sdk/model/access/Identity;)V

    .line 206
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    if-nez p1, :cond_4

    move v2, v4

    .line 211
    .local v2, "endUserWasCleared":Z
    :goto_1
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    move v1, v4

    .line 213
    .local v1, "endUserHasChanged":Z
    :goto_2
    if-nez v2, :cond_2

    if-eqz v1, :cond_0

    .line 215
    :cond_2
    const-string v6, "ZendeskConfiguration"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Clearing user data. End user cleared: %s, end user changed: %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    .line 220
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v4

    .line 217
    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v7, v5, [Ljava/lang/Object;

    .line 215
    invoke-static {v6, v4, v7}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/storage/StorageStore;->sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/storage/SdkStorage;->clearUserData()V

    .line 225
    if-eqz p1, :cond_0

    .line 226
    const-string v4, "ZendeskConfiguration"

    const-string v6, "Identity has changed, storing new identity"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v6, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 228
    instance-of v4, p1, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v4, :cond_3

    move-object v4, p1

    .line 231
    check-cast v4, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->reloadGuid()V

    .line 234
    :cond_3
    invoke-interface {v3, p1}, Lcom/zendesk/sdk/storage/IdentityStorage;->storeIdentity(Lcom/zendesk/sdk/model/access/Identity;)V

    .line 235
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    goto :goto_0

    .end local v1    # "endUserHasChanged":Z
    .end local v2    # "endUserWasCleared":Z
    :cond_4
    move v2, v5

    .line 210
    goto :goto_1

    .restart local v2    # "endUserWasCleared":Z
    :cond_5
    move v1, v5

    .line 211
    goto :goto_2
.end method

.method public setSdkOptions(Lcom/zendesk/sdk/network/SdkOptions;)V
    .locals 1
    .param p1, "sdkOptions"    # Lcom/zendesk/sdk/network/SdkOptions;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->sdkOptions(Lcom/zendesk/sdk/network/SdkOptions;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 419
    return-void
.end method

.method public setTicketFormId(Ljava/lang/Long;)V
    .locals 1
    .param p1, "ticketFormId"    # Ljava/lang/Long;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->newBuilder()Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->ticketFormId(Ljava/lang/Long;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->build()Lcom/zendesk/sdk/network/impl/ApplicationScope;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->initApplicationScope(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    .line 273
    return-void
.end method

.method public storage()Lcom/zendesk/sdk/storage/StorageStore;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->zendeskConfigHelper:Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    return-object v0
.end method
