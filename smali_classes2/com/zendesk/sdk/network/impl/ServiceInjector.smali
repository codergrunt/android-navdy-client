.class Lcom/zendesk/sdk/network/impl/ServiceInjector;
.super Ljava/lang/Object;
.source "ServiceInjector.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static injectAccessService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/AccessService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 26
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/AccessService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/AccessService;

    return-object v0
.end method

.method private static injectHelpCenterService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/HelpCenterService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 37
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/HelpCenterService;

    return-object v0
.end method

.method private static injectPushRegistrationService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/PushRegistrationService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 46
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/PushRegistrationService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/PushRegistrationService;

    return-object v0
.end method

.method private static injectRequestService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/RequestService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 55
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/RequestService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/RequestService;

    return-object v0
.end method

.method private static injectSdkSettingsService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SdkSettingsService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 64
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/SdkSettingsService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/SdkSettingsService;

    return-object v0
.end method

.method private static injectUploadService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UploadService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 73
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/UploadService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/UploadService;

    return-object v0
.end method

.method private static injectUserService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UserService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 82
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;

    move-result-object v0

    const-class v1, Lcom/zendesk/sdk/network/UserService;

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/UserService;

    return-object v0
.end method

.method static injectZendeskAccessService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskAccessService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 30
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;

    .line 31
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectAccessService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/AccessService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;-><init>(Lcom/zendesk/sdk/network/AccessService;)V

    return-object v0
.end method

.method static injectZendeskHelpCenterService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 41
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectHelpCenterService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/HelpCenterService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;-><init>(Lcom/zendesk/sdk/network/HelpCenterService;)V

    return-object v0
.end method

.method static injectZendeskPushRegistrationService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 50
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectPushRegistrationService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/PushRegistrationService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;-><init>(Lcom/zendesk/sdk/network/PushRegistrationService;)V

    return-object v0
.end method

.method static injectZendeskRequestService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskRequestService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 59
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectRequestService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/RequestService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;-><init>(Lcom/zendesk/sdk/network/RequestService;)V

    return-object v0
.end method

.method static injectZendeskSdkSettingsService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 68
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectSdkSettingsService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/SdkSettingsService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsService;-><init>(Lcom/zendesk/sdk/network/SdkSettingsService;)V

    return-object v0
.end method

.method static injectZendeskUploadService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskUploadService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 77
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectUploadService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UploadService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;-><init>(Lcom/zendesk/sdk/network/UploadService;)V

    return-object v0
.end method

.method static injectZendeskUserService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/ZendeskUserService;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 86
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/ServiceInjector;->injectUserService(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/UserService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskUserService;-><init>(Lcom/zendesk/sdk/network/UserService;)V

    return-object v0
.end method
