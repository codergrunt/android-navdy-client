.class Lcom/zendesk/sdk/network/impl/ConfigurationRuntimeException;
.super Ljava/lang/RuntimeException;
.source "ConfigurationRuntimeException.java"


# static fields
.field private static final MESSAGE:Ljava/lang/String; = "Zendesk configuration error occurred"


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "Zendesk configuration error occurred"

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method
