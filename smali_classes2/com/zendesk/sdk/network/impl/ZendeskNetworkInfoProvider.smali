.class Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;
.super Ljava/lang/Object;
.source "ZendeskNetworkInfoProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/NetworkInfoProvider;
.implements Lcom/zendesk/sdk/network/NetworkAware;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskNetworkInfoProvider"


# instance fields
.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private context:Landroid/content/Context;

.field private listeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/zendesk/sdk/network/NetworkAware;",
            ">;>;"
        }
    .end annotation
.end field

.field private networkAvailable:Z

.field private networkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private retryActions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/zendesk/sdk/network/RetryAction;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->listeners:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->retryActions:Ljava/util/Map;

    .line 64
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    .line 65
    invoke-static {p1}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkAvailable:Z

    .line 66
    return-void
.end method

.method private registerForNetworkCallbacks()V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 166
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_0

    .line 168
    const-string v2, "ZendeskNetworkInfoProvider"

    const-string v3, "Adding pre-Lollipop network callbacks..."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$NetworkAvailabilityBroadcastReceiver;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;)V

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 171
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 206
    :goto_0
    return-void

    .line 174
    :cond_0
    const-string v2, "ZendeskNetworkInfoProvider"

    const-string v3, "Adding Lollipop network callbacks..."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    const-string v3, "connectivity"

    .line 177
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 179
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 181
    .local v1, "handler":Landroid/os/Handler;
    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$1;

    invoke-direct {v2, p0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 203
    new-instance v2, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v2}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 204
    invoke-virtual {v2}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 203
    invoke-virtual {v0, v2, v3}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    goto :goto_0
.end method

.method private unregisterForNetworkCallbacks()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 210
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 214
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v1, :cond_1

    .line 215
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 216
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 218
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 220
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    :cond_1
    return-void
.end method


# virtual methods
.method public addNetworkAwareListener(Ljava/lang/Integer;Lcom/zendesk/sdk/network/NetworkAware;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/zendesk/sdk/network/NetworkAware;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 96
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 97
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->listeners:Ljava/util/Map;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    :cond_0
    return-void
.end method

.method public addRetryAction(Ljava/lang/Integer;Lcom/zendesk/sdk/network/RetryAction;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "retryAction"    # Lcom/zendesk/sdk/network/RetryAction;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 114
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 115
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->retryActions:Ljava/util/Map;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    :cond_0
    return-void
.end method

.method public clearNetworkAwareListeners()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->listeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 109
    return-void
.end method

.method public clearRetryActions()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->retryActions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 127
    return-void
.end method

.method public isNetworkAvailable()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkAvailable:Z

    return v0
.end method

.method public onNetworkAvailable()V
    .locals 6

    .prologue
    .line 131
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->listeners:Ljava/util/Map;

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 132
    .local v1, "listenerReferences":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/NetworkAware;>;>;"
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->retryActions:Ljava/util/Map;

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 134
    .local v3, "retryActionReferences":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/RetryAction;>;>;"
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkAvailable:Z

    .line 136
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 137
    .local v0, "listenerReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/NetworkAware;>;"
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 138
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/zendesk/sdk/network/NetworkAware;

    invoke-interface {v4}, Lcom/zendesk/sdk/network/NetworkAware;->onNetworkAvailable()V

    goto :goto_0

    .line 142
    .end local v0    # "listenerReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/NetworkAware;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 143
    .local v2, "retryActionReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/RetryAction;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 144
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/zendesk/sdk/network/RetryAction;

    invoke-interface {v4}, Lcom/zendesk/sdk/network/RetryAction;->onRetry()V

    goto :goto_1

    .line 147
    .end local v2    # "retryActionReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/RetryAction;>;"
    :cond_3
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->retryActions:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 148
    return-void
.end method

.method public onNetworkUnavailable()V
    .locals 4

    .prologue
    .line 152
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->listeners:Ljava/util/Map;

    invoke-static {v2}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 154
    .local v1, "listenerReferences":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/NetworkAware;>;>;"
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->networkAvailable:Z

    .line 156
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 157
    .local v0, "listenerReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/NetworkAware;>;"
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 158
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/zendesk/sdk/network/NetworkAware;

    invoke-interface {v2}, Lcom/zendesk/sdk/network/NetworkAware;->onNetworkUnavailable()V

    goto :goto_0

    .line 161
    .end local v0    # "listenerReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/zendesk/sdk/network/NetworkAware;>;"
    :cond_1
    return-void
.end method

.method public register()V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 71
    const-string v0, "ZendeskNetworkInfoProvider"

    const-string v1, "Cannot register NetworkInformer, supplied Context is null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->registerForNetworkCallbacks()V

    goto :goto_0
.end method

.method public removeNetworkAwareListener(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 103
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->listeners:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method

.method public removeRetryAction(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 121
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->retryActions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    return-void
.end method

.method public unregister()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 81
    const-string v0, "ZendeskNetworkInfoProvider"

    const-string v1, "Cannot unregister NetworkInformer, supplied Context is null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/network/impl/ZendeskNetworkInfoProvider;->unregisterForNetworkCallbacks()V

    goto :goto_0
.end method
