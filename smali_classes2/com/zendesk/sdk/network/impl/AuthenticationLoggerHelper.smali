.class Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;
.super Ljava/lang/Object;
.source "AuthenticationLoggerHelper.java"


# instance fields
.field private final logMessage:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/sdk/model/access/Identity;)V
    .locals 2
    .param p1, "expectedAuthenticationType"    # Lcom/zendesk/sdk/model/access/AuthenticationType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "storedIdentity"    # Lcom/zendesk/sdk/model/access/Identity;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xa0

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 31
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "The expected type of authentication is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    if-nez p1, :cond_3

    .line 34
    const-string v1, "null. Check that settings have been downloaded."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    :cond_0
    :goto_0
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 43
    const-string v1, "The local identity is"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    if-nez p2, :cond_1

    .line 46
    const-string v1, " not"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    :cond_1
    const-string v1, " present.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    if-eqz p2, :cond_2

    .line 52
    const-string v1, "The local identity is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    instance-of v1, p2, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v1, :cond_5

    .line 55
    const-string v1, "anonymous."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;->logMessage:Ljava/lang/String;

    .line 64
    return-void

    .line 35
    :cond_3
    sget-object v1, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne p1, v1, :cond_4

    .line 36
    const-string v1, "anonymous."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 37
    :cond_4
    sget-object v1, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne p1, v1, :cond_0

    .line 38
    const-string v1, "jwt."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 56
    :cond_5
    instance-of v1, p2, Lcom/zendesk/sdk/model/access/JwtIdentity;

    if-eqz v1, :cond_6

    .line 57
    const-string v1, "jwt."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 59
    :cond_6
    const-string v1, "unknown."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public getLogMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;->logMessage:Ljava/lang/String;

    return-object v0
.end method
