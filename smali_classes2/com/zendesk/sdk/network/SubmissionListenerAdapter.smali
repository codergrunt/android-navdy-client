.class public Lcom/zendesk/sdk/network/SubmissionListenerAdapter;
.super Ljava/lang/Object;
.source "SubmissionListenerAdapter.java"

# interfaces
.implements Lcom/zendesk/sdk/network/SubmissionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSubmissionCancel()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public onSubmissionCompleted()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public onSubmissionError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 0
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 29
    return-void
.end method

.method public onSubmissionStarted()V
    .locals 0

    .prologue
    .line 14
    return-void
.end method
