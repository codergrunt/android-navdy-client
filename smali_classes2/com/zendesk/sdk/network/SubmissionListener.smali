.class public interface abstract Lcom/zendesk/sdk/network/SubmissionListener;
.super Ljava/lang/Object;
.source "SubmissionListener.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract onSubmissionCancel()V
.end method

.method public abstract onSubmissionCompleted()V
.end method

.method public abstract onSubmissionError(Lcom/zendesk/service/ErrorResponse;)V
.end method

.method public abstract onSubmissionStarted()V
.end method
