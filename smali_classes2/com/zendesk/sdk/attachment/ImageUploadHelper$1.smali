.class Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ImageUploadHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/attachment/ImageUploadHelper;->newZendeskCallback(Lcom/zendesk/belvedere/BelvedereResult;)Lcom/zendesk/service/ZendeskCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/UploadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

.field final synthetic val$file:Lcom/zendesk/belvedere/BelvedereResult;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/belvedere/BelvedereResult;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    iput-object p2, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 7
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    const/4 v6, 0x0

    .line 252
    invoke-static {}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Error; Reason: %s, Status: %s, isNetworkError: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->isNetworkError()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$100(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 256
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$700(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 258
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$300(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$300(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-interface {v0, p1, v1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;->imageUploadError(Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/belvedere/BelvedereResult;)V

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$600(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)V

    .line 263
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/UploadResponse;)V
    .locals 6
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/UploadResponse;

    .prologue
    const/4 v5, 0x0

    .line 221
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getAttachment()Lcom/zendesk/sdk/model/request/Attachment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    invoke-static {}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Image successfully uploaded: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 227
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getAttachment()Lcom/zendesk/sdk/model/request/Attachment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/request/Attachment;->getContentUrl()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    .line 224
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    .line 222
    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$100(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 232
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$200(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$300(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$300(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-interface {v0, p1, v1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;->imageUploaded(Lcom/zendesk/sdk/model/request/UploadResponse;Lcom/zendesk/belvedere/BelvedereResult;)V

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$400(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$500(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/network/UploadProvider;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getToken()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/network/UploadProvider;->deleteAttachment(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->this$0:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->access$600(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)V

    .line 248
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 217
    check-cast p1, Lcom/zendesk/sdk/model/request/UploadResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;->onSuccess(Lcom/zendesk/sdk/model/request/UploadResponse;)V

    return-void
.end method
