.class Lcom/zendesk/sdk/storage/StubSdkStorage;
.super Ljava/lang/Object;
.source "StubSdkStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/SdkStorage;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "StubSdkStorage"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearUserData()V
    .locals 3

    .prologue
    .line 23
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 24
    return-void
.end method

.method public getUserStorage()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    return-object v0
.end method

.method public registerUserStorage(Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;)V
    .locals 3
    .param p1, "userStorage"    # Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;

    .prologue
    .line 18
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 19
    return-void
.end method
