.class Lcom/zendesk/sdk/storage/StubStorageStore;
.super Ljava/lang/Object;
.source "StubStorageStore.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/StorageStore;


# instance fields
.field private final helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

.field private final identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

.field private final requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

.field private final sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

.field private final sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/zendesk/sdk/storage/StubSdkStorage;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubSdkStorage;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    .line 17
    new-instance v0, Lcom/zendesk/sdk/storage/StubIdentityStorage;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubIdentityStorage;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    .line 18
    new-instance v0, Lcom/zendesk/sdk/storage/StubRequestStorage;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubRequestStorage;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    .line 19
    new-instance v0, Lcom/zendesk/sdk/storage/StubSdkSettingsStorage;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubSdkSettingsStorage;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    .line 20
    new-instance v0, Lcom/zendesk/sdk/storage/StubHelpCenterSessionCache;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubHelpCenterSessionCache;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .line 21
    return-void
.end method


# virtual methods
.method public helpCenterSessionCache()Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    return-object v0
.end method

.method public identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    return-object v0
.end method

.method public requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    return-object v0
.end method

.method public sdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    return-object v0
.end method

.method public sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/zendesk/sdk/storage/StubStorageStore;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    return-object v0
.end method
