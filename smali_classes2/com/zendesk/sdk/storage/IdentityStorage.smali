.class public interface abstract Lcom/zendesk/sdk/storage/IdentityStorage;
.super Ljava/lang/Object;
.source "IdentityStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;


# virtual methods
.method public abstract anonymiseIdentity()Lcom/zendesk/sdk/model/access/Identity;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getIdentity()Lcom/zendesk/sdk/model/access/Identity;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getStoredAccessToken()Lcom/zendesk/sdk/model/access/AccessToken;
.end method

.method public abstract getStoredAccessTokenAsBearerToken()Ljava/lang/String;
.end method

.method public abstract getUUID()Ljava/lang/String;
.end method

.method public abstract storeAccessToken(Lcom/zendesk/sdk/model/access/AccessToken;)V
.end method

.method public abstract storeIdentity(Lcom/zendesk/sdk/model/access/Identity;)V
.end method
