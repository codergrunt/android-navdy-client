.class public interface abstract Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
.super Ljava/lang/Object;
.source "ZendeskFeedbackConfiguration.java"

# interfaces
.implements Ljava/io/Serializable;


# virtual methods
.method public abstract getAdditionalInfo()Ljava/lang/String;
.end method

.method public abstract getRequestSubject()Ljava/lang/String;
.end method

.method public abstract getTags()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
