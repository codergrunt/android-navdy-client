.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;
.super Lcom/zendesk/belvedere/BelvedereCallback;
.source "ContactZendeskFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setUpCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/belvedere/BelvedereCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/belvedere/BelvedereResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {p0}, Lcom/zendesk/belvedere/BelvedereCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic success(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 488
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;->success(Ljava/util/List;)V

    return-void
.end method

.method public success(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 491
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    .line 492
    .local v0, "storedSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    if-eqz v0, :cond_0

    .line 493
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$100(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v3}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$200(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    move-result-object v3

    invoke-static {p1, v1, v2, v3, v0}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->processAndUploadSelectedFiles(Ljava/util/List;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Landroid/content/Context;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$4;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    .line 496
    return-void
.end method
