.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$DefaultContactConfiguration;
.super Lcom/zendesk/sdk/feedback/BaseZendeskFeedbackConfiguration;
.source "ContactZendeskActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DefaultContactConfiguration"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$DefaultContactConfiguration;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/BaseZendeskFeedbackConfiguration;-><init>()V

    return-void
.end method


# virtual methods
.method public getRequestSubject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$DefaultContactConfiguration;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    sget v1, Lcom/zendesk/sdk/R$string;->contact_fragment_request_subject:I

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
