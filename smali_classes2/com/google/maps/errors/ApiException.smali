.class public Lcom/google/maps/errors/ApiException;
.super Ljava/lang/Exception;
.source "ApiException.java"


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public static from(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/errors/ApiException;
    .locals 7
    .param p0, "status"    # Ljava/lang/String;
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 37
    const-string v0, "OK"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 68
    :goto_0
    return-object v0

    .line 39
    :cond_0
    const-string v0, "INVALID_REQUEST"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    new-instance v0, Lcom/google/maps/errors/InvalidRequestException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/InvalidRequestException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_1
    const-string v0, "MAX_ELEMENTS_EXCEEDED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    new-instance v0, Lcom/google/maps/errors/MaxElementsExceededException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/MaxElementsExceededException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_2
    const-string v0, "NOT_FOUND"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44
    new-instance v0, Lcom/google/maps/errors/NotFoundException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/NotFoundException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_3
    const-string v0, "OVER_QUERY_LIMIT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 46
    new-instance v0, Lcom/google/maps/errors/OverQueryLimitException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/OverQueryLimitException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :cond_4
    const-string v0, "REQUEST_DENIED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 48
    new-instance v0, Lcom/google/maps/errors/RequestDeniedException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/RequestDeniedException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_5
    const-string v0, "UNKNOWN_ERROR"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 50
    new-instance v0, Lcom/google/maps/errors/UnknownErrorException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/UnknownErrorException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_6
    const-string v0, "ZERO_RESULTS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 52
    new-instance v0, Lcom/google/maps/errors/ZeroResultsException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/ZeroResultsException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_7
    const-string v0, "ACCESS_NOT_CONFIGURED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 57
    new-instance v0, Lcom/google/maps/errors/AccessNotConfiguredException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/AccessNotConfiguredException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_8
    const-string v0, "INVALID_ARGUMENT"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 59
    new-instance v0, Lcom/google/maps/errors/InvalidRequestException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/InvalidRequestException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_9
    const-string v0, "RESOURCE_EXHAUSTED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 61
    new-instance v0, Lcom/google/maps/errors/OverQueryLimitException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/OverQueryLimitException;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 62
    :cond_a
    const-string v0, "PERMISSION_DENIED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 63
    new-instance v0, Lcom/google/maps/errors/RequestDeniedException;

    invoke-direct {v0, p1}, Lcom/google/maps/errors/RequestDeniedException;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 68
    :cond_b
    new-instance v0, Lcom/google/maps/errors/UnknownErrorException;

    const-string v1, "An unexpected error occurred. Status: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/maps/errors/UnknownErrorException;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
