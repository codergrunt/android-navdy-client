.class public Lcom/google/maps/internal/ExceptionResult;
.super Ljava/lang/Object;
.source "ExceptionResult.java"

# interfaces
.implements Lcom/google/maps/PendingResult;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/maps/PendingResult",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final exception:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 26
    .local p0, "this":Lcom/google/maps/internal/ExceptionResult;, "Lcom/google/maps/internal/ExceptionResult<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/maps/internal/ExceptionResult;->exception:Ljava/lang/Exception;

    .line 28
    return-void
.end method


# virtual methods
.method public await()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/google/maps/internal/ExceptionResult;, "Lcom/google/maps/internal/ExceptionResult<TT;>;"
    iget-object v0, p0, Lcom/google/maps/internal/ExceptionResult;->exception:Ljava/lang/Exception;

    throw v0
.end method

.method public awaitIgnoreError()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/google/maps/internal/ExceptionResult;, "Lcom/google/maps/internal/ExceptionResult<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public cancel()V
    .locals 0

    .prologue
    .line 47
    .local p0, "this":Lcom/google/maps/internal/ExceptionResult;, "Lcom/google/maps/internal/ExceptionResult<TT;>;"
    return-void
.end method

.method public setCallback(Lcom/google/maps/PendingResult$Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/PendingResult$Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/google/maps/internal/ExceptionResult;, "Lcom/google/maps/internal/ExceptionResult<TT;>;"
    .local p1, "callback":Lcom/google/maps/PendingResult$Callback;, "Lcom/google/maps/PendingResult$Callback<TT;>;"
    iget-object v0, p0, Lcom/google/maps/internal/ExceptionResult;->exception:Ljava/lang/Exception;

    invoke-interface {p1, v0}, Lcom/google/maps/PendingResult$Callback;->onFailure(Ljava/lang/Throwable;)V

    .line 33
    return-void
.end method
