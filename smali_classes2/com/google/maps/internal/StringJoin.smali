.class public Lcom/google/maps/internal/StringJoin;
.super Ljava/lang/Object;
.source "StringJoin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/maps/internal/StringJoin$UrlValue;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static varargs join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;
    .locals 8
    .param p0, "delim"    # C
    .param p1, "parts"    # [Lcom/google/maps/internal/StringJoin$UrlValue;

    .prologue
    .line 49
    array-length v7, p1

    new-array v6, v7, [Ljava/lang/String;

    .line 50
    .local v6, "strings":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 51
    .local v1, "i":I
    move-object v0, p1

    .local v0, "arr$":[Lcom/google/maps/internal/StringJoin$UrlValue;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 52
    .local v5, "part":Lcom/google/maps/internal/StringJoin$UrlValue;
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 51
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 55
    .end local v5    # "part":Lcom/google/maps/internal/StringJoin$UrlValue;
    :cond_0
    invoke-static {p0, v6}, Lcom/google/maps/internal/StringJoin;->join(C[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static varargs join(C[Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "delim"    # C
    .param p1, "parts"    # [Ljava/lang/String;

    .prologue
    .line 38
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .local v1, "result":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 40
    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 43
    :cond_0
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
