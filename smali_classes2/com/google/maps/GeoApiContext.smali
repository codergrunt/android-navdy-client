.class public Lcom/google/maps/GeoApiContext;
.super Ljava/lang/Object;
.source "GeoApiContext.java"


# static fields
.field private static final DEFAULT_BACKOFF_TIMEOUT_MILLIS:I = 0xea60

.field private static final USER_AGENT:Ljava/lang/String; = "GoogleGeoApiClientJava/0.1.6"

.field private static final VERSION:Ljava/lang/String; = "0.1.6"

.field private static log:Ljava/util/logging/Logger;


# instance fields
.field private apiKey:Ljava/lang/String;

.field private baseUrlOverride:Ljava/lang/String;

.field private final client:Lcom/squareup/okhttp/OkHttpClient;

.field private clientId:Ljava/lang/String;

.field private errorTimeout:J

.field private final rateLimitExecutorService:Lcom/google/maps/internal/RateLimitExecutorService;

.field private urlSigner:Lcom/google/maps/internal/UrlSigner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/google/maps/GeoApiContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/maps/GeoApiContext;->log:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/squareup/okhttp/OkHttpClient;

    invoke-direct {v0}, Lcom/squareup/okhttp/OkHttpClient;-><init>()V

    iput-object v0, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    .line 54
    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Lcom/google/maps/GeoApiContext;->errorTimeout:J

    .line 57
    new-instance v0, Lcom/google/maps/internal/RateLimitExecutorService;

    invoke-direct {v0}, Lcom/google/maps/internal/RateLimitExecutorService;-><init>()V

    iput-object v0, p0, Lcom/google/maps/GeoApiContext;->rateLimitExecutorService:Lcom/google/maps/internal/RateLimitExecutorService;

    .line 58
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    new-instance v1, Lcom/squareup/okhttp/Dispatcher;

    iget-object v2, p0, Lcom/google/maps/GeoApiContext;->rateLimitExecutorService:Lcom/google/maps/internal/RateLimitExecutorService;

    invoke-direct {v1, v2}, Lcom/squareup/okhttp/Dispatcher;-><init>(Ljava/util/concurrent/ExecutorService;)V

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/OkHttpClient;->setDispatcher(Lcom/squareup/okhttp/Dispatcher;)Lcom/squareup/okhttp/OkHttpClient;

    .line 59
    return-void
.end method

.method private checkContext(Z)V
    .locals 2
    .param p1, "canUseClientId"    # Z

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->urlSigner:Lcom/google/maps/internal/UrlSigner;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->apiKey:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must provide either API key or Maps for Work credentials."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->apiKey:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 146
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "API does not support client ID & secret - you must provide a key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->urlSigner:Lcom/google/maps/internal/UrlSigner;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->apiKey:Ljava/lang/String;

    const-string v1, "AIza"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 150
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid API key."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_2
    return-void
.end method

.method private getWithPath(Ljava/lang/Class;Lcom/google/gson/FieldNamingPolicy;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/maps/PendingResult;
    .locals 14
    .param p2, "fieldNamingPolicy"    # Lcom/google/gson/FieldNamingPolicy;
    .param p3, "hostName"    # Ljava/lang/String;
    .param p4, "path"    # Ljava/lang/String;
    .param p5, "canUseClientId"    # Z
    .param p6, "encodedPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R::",
            "Lcom/google/maps/internal/ApiResponse",
            "<TT;>;>(",
            "Ljava/lang/Class",
            "<TR;>;",
            "Lcom/google/gson/FieldNamingPolicy;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TR;>;"
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/google/maps/GeoApiContext;->checkContext(Z)V

    .line 106
    const-string v3, "&"

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 107
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v5, "encodedPath must start with &"

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 110
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    move-object/from16 v0, p4

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 111
    .local v11, "url":Ljava/lang/StringBuilder;
    if-eqz p5, :cond_3

    iget-object v3, p0, Lcom/google/maps/GeoApiContext;->clientId:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 112
    const-string v3, "?client="

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/maps/GeoApiContext;->clientId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :goto_0
    move-object/from16 v0, p6

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    if-eqz p5, :cond_1

    iget-object v3, p0, Lcom/google/maps/GeoApiContext;->clientId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 120
    :try_start_0
    iget-object v3, p0, Lcom/google/maps/GeoApiContext;->urlSigner:Lcom/google/maps/internal/UrlSigner;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/maps/internal/UrlSigner;->getSignature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 121
    .local v10, "signature":Ljava/lang/String;
    const-string v3, "&signature="

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v10    # "signature":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/maps/GeoApiContext;->baseUrlOverride:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 128
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->baseUrlOverride:Ljava/lang/String;

    move-object/from16 p3, v0

    .line 131
    :cond_2
    new-instance v3, Lcom/squareup/okhttp/Request$Builder;

    invoke-direct {v3}, Lcom/squareup/okhttp/Request$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/squareup/okhttp/Request$Builder;->get()Lcom/squareup/okhttp/Request$Builder;

    move-result-object v3

    const-string v5, "User-Agent"

    const-string v6, "GoogleGeoApiClientJava/0.1.6"

    invoke-virtual {v3, v5, v6}, Lcom/squareup/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    move-result-object v3

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/squareup/okhttp/Request$Builder;->url(Ljava/lang/String;)Lcom/squareup/okhttp/Request$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/okhttp/Request$Builder;->build()Lcom/squareup/okhttp/Request;

    move-result-object v4

    .line 136
    .local v4, "req":Lcom/squareup/okhttp/Request;
    sget-object v3, Lcom/google/maps/GeoApiContext;->log:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v6, "Request: {0}"

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v9, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v5, v6, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    .line 138
    new-instance v3, Lcom/google/maps/internal/OkHttpPendingResult;

    iget-object v5, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    iget-wide v8, p0, Lcom/google/maps/GeoApiContext;->errorTimeout:J

    move-object v6, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v9}, Lcom/google/maps/internal/OkHttpPendingResult;-><init>(Lcom/squareup/okhttp/Request;Lcom/squareup/okhttp/OkHttpClient;Ljava/lang/Class;Lcom/google/gson/FieldNamingPolicy;J)V

    .end local v4    # "req":Lcom/squareup/okhttp/Request;
    :goto_1
    return-object v3

    .line 114
    :cond_3
    const-string v3, "?key="

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/google/maps/GeoApiContext;->apiKey:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 122
    :catch_0
    move-exception v2

    .line 123
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/google/maps/internal/ExceptionResult;

    invoke-direct {v3, v2}, Lcom/google/maps/internal/ExceptionResult;-><init>(Ljava/lang/Exception;)V

    goto :goto_1
.end method


# virtual methods
.method get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;Ljava/util/Map;)Lcom/google/maps/PendingResult;
    .locals 11
    .param p1, "config"    # Lcom/google/maps/internal/ApiConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R::",
            "Lcom/google/maps/internal/ApiResponse",
            "<TT;>;>(",
            "Lcom/google/maps/internal/ApiConfig;",
            "Ljava/lang/Class",
            "<+TR;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/maps/PendingResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TR;>;"
    .local p3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v10, "query":Ljava/lang/StringBuilder;
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 66
    .local v9, "param":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v0, 0x26

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    :try_start_0
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v7

    .line 70
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v0, Lcom/google/maps/internal/ExceptionResult;

    invoke-direct {v0, v7}, Lcom/google/maps/internal/ExceptionResult;-><init>(Ljava/lang/Exception;)V

    .line 74
    .end local v7    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v9    # "param":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_1
    return-object v0

    :cond_0
    iget-object v2, p1, Lcom/google/maps/internal/ApiConfig;->fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

    iget-object v3, p1, Lcom/google/maps/internal/ApiConfig;->hostName:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/maps/internal/ApiConfig;->path:Ljava/lang/String;

    iget-boolean v5, p1, Lcom/google/maps/internal/ApiConfig;->supportsClientId:Z

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/maps/GeoApiContext;->getWithPath(Ljava/lang/Class;Lcom/google/gson/FieldNamingPolicy;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    goto :goto_1
.end method

.method varargs get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;
    .locals 10
    .param p1, "config"    # Lcom/google/maps/internal/ApiConfig;
    .param p3, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R::",
            "Lcom/google/maps/internal/ApiResponse",
            "<TT;>;>(",
            "Lcom/google/maps/internal/ApiConfig;",
            "Ljava/lang/Class",
            "<+TR;>;[",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 80
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TR;>;"
    array-length v0, p3

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Params must be matching key/value pairs."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v9, "query":Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, p3

    if-ge v8, v0, :cond_1

    .line 87
    const/16 v0, 0x26

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p3, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    add-int/lit8 v8, v8, 0x1

    .line 92
    :try_start_0
    aget-object v0, p3, v8

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 93
    :catch_0
    move-exception v7

    .line 94
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v0, Lcom/google/maps/internal/ExceptionResult;

    invoke-direct {v0, v7}, Lcom/google/maps/internal/ExceptionResult;-><init>(Ljava/lang/Exception;)V

    .line 98
    .end local v7    # "e":Ljava/io/UnsupportedEncodingException;
    :goto_1
    return-object v0

    :cond_1
    iget-object v2, p1, Lcom/google/maps/internal/ApiConfig;->fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

    iget-object v3, p1, Lcom/google/maps/internal/ApiConfig;->hostName:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/maps/internal/ApiConfig;->path:Ljava/lang/String;

    iget-boolean v5, p1, Lcom/google/maps/internal/ApiConfig;->supportsClientId:Z

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/maps/GeoApiContext;->getWithPath(Ljava/lang/Class;Lcom/google/gson/FieldNamingPolicy;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    goto :goto_1
.end method

.method public setApiKey(Ljava/lang/String;)Lcom/google/maps/GeoApiContext;
    .locals 0
    .param p1, "apiKey"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/maps/GeoApiContext;->apiKey:Ljava/lang/String;

    .line 165
    return-object p0
.end method

.method setBaseUrlForTesting(Ljava/lang/String;)Lcom/google/maps/GeoApiContext;
    .locals 0
    .param p1, "baseUrl"    # Ljava/lang/String;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/maps/GeoApiContext;->baseUrlOverride:Ljava/lang/String;

    .line 160
    return-object p0
.end method

.method public setConnectTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/okhttp/OkHttpClient;->setConnectTimeout(JLjava/util/concurrent/TimeUnit;)V

    .line 181
    return-object p0
.end method

.method public setEnterpriseCredentials(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "clientId"    # Ljava/lang/String;
    .param p2, "cryptographicSecret"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/maps/GeoApiContext;->clientId:Ljava/lang/String;

    .line 170
    new-instance v0, Lcom/google/maps/internal/UrlSigner;

    invoke-direct {v0, p2}, Lcom/google/maps/internal/UrlSigner;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/maps/GeoApiContext;->urlSigner:Lcom/google/maps/internal/UrlSigner;

    .line 171
    return-object p0
.end method

.method public setProxy(Ljava/net/Proxy;)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "proxy"    # Ljava/net/Proxy;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    if-nez p1, :cond_0

    sget-object p1, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .end local p1    # "proxy":Ljava/net/Proxy;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/OkHttpClient;->setProxy(Ljava/net/Proxy;)Lcom/squareup/okhttp/OkHttpClient;

    .line 241
    return-object p0
.end method

.method public setQueryRateLimit(I)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "maxQps"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->rateLimitExecutorService:Lcom/google/maps/internal/RateLimitExecutorService;

    invoke-virtual {v0, p1}, Lcom/google/maps/internal/RateLimitExecutorService;->setQueriesPerSecond(I)V

    .line 218
    return-object p0
.end method

.method public setQueryRateLimit(II)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "maxQps"    # I
    .param p2, "minimumInterval"    # I

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->rateLimitExecutorService:Lcom/google/maps/internal/RateLimitExecutorService;

    invoke-virtual {v0, p1, p2}, Lcom/google/maps/internal/RateLimitExecutorService;->setQueriesPerSecond(II)V

    .line 231
    return-object p0
.end method

.method public setReadTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/okhttp/OkHttpClient;->setReadTimeout(JLjava/util/concurrent/TimeUnit;)V

    .line 191
    return-object p0
.end method

.method public setRetryTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/google/maps/GeoApiContext;
    .locals 3
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 207
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/maps/GeoApiContext;->errorTimeout:J

    .line 208
    return-object p0
.end method

.method public setWriteTimeout(JLjava/util/concurrent/TimeUnit;)Lcom/google/maps/GeoApiContext;
    .locals 1
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/maps/GeoApiContext;->client:Lcom/squareup/okhttp/OkHttpClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/okhttp/OkHttpClient;->setWriteTimeout(JLjava/util/concurrent/TimeUnit;)V

    .line 199
    return-object p0
.end method
