.class Lcom/nimbusds/jose/crypto/LegacyConcatKDF;
.super Ljava/lang/Object;
.source "LegacyConcatKDF.java"


# static fields
.field private static final ENCRYPTION_BYTES:[B

.field private static final INTEGRITY_BYTES:[B

.field private static final ONE_BYTES:[B

.field private static final ZERO_BYTES:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 36
    new-array v0, v3, [B

    const/4 v1, 0x3

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    sput-object v0, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ONE_BYTES:[B

    .line 42
    new-array v0, v3, [B

    sput-object v0, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ZERO_BYTES:[B

    .line 48
    const/16 v0, 0xa

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ENCRYPTION_BYTES:[B

    .line 57
    const/16 v0, 0x9

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->INTEGRITY_BYTES:[B

    .line 60
    return-void

    .line 48
    :array_0
    .array-data 1
        0x45t
        0x6et
        0x63t
        0x72t
        0x79t
        0x70t
        0x74t
        0x69t
        0x6ft
        0x6et
    .end array-data

    .line 57
    nop

    :array_1
    .array-data 1
        0x49t
        0x6et
        0x74t
        0x65t
        0x67t
        0x72t
        0x69t
        0x74t
        0x79t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    return-void
.end method

.method public static generateCEK(Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/EncryptionMethod;[B[B)Ljavax/crypto/SecretKey;
    .locals 16
    .param p0, "key"    # Ljavax/crypto/SecretKey;
    .param p1, "enc"    # Lcom/nimbusds/jose/EncryptionMethod;
    .param p2, "epu"    # [B
    .param p3, "epv"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 90
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v13, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ONE_BYTES:[B

    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 93
    invoke-interface/range {p0 .. p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v6

    .line 94
    .local v6, "cmkBytes":[B
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 97
    array-length v13, v6

    mul-int/lit8 v5, v13, 0x8

    .line 98
    .local v5, "cmkBitLength":I
    move v9, v5

    .line 99
    .local v9, "hashBitLength":I
    div-int/lit8 v2, v5, 0x2

    .line 100
    .local v2, "cekBitLength":I
    invoke-static {v2}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v3

    .line 101
    .local v3, "cekBitLengthBytes":[B
    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 104
    invoke-virtual/range {p1 .. p1}, Lcom/nimbusds/jose/EncryptionMethod;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    .line 105
    .local v8, "encBytes":[B
    invoke-virtual {v1, v8}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 108
    if-eqz p2, :cond_0

    .line 110
    move-object/from16 v0, p2

    array-length v13, v0

    invoke-static {v13}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 111
    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 118
    :goto_0
    if-eqz p3, :cond_1

    .line 120
    move-object/from16 v0, p3

    array-length v13, v0

    invoke-static {v13}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 121
    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 128
    :goto_1
    sget-object v13, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ENCRYPTION_BYTES:[B

    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 142
    .local v10, "hashInput":[B
    :try_start_1
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "SHA-"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    .line 149
    .local v12, "md":Ljava/security/MessageDigest;
    invoke-virtual {v12, v10}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v11

    .line 151
    .local v11, "hashOutput":[B
    array-length v13, v11

    div-int/lit8 v13, v13, 0x2

    new-array v4, v13, [B

    .line 152
    .local v4, "cekBytes":[B
    const/4 v13, 0x0

    const/4 v14, 0x0

    array-length v15, v4

    invoke-static {v11, v13, v4, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    new-instance v13, Ljavax/crypto/spec/SecretKeySpec;

    const-string v14, "AES"

    invoke-direct {v13, v4, v14}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v13

    .line 114
    .end local v4    # "cekBytes":[B
    .end local v10    # "hashInput":[B
    .end local v11    # "hashOutput":[B
    .end local v12    # "md":Ljava/security/MessageDigest;
    :cond_0
    :try_start_2
    sget-object v13, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ZERO_BYTES:[B

    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 130
    .end local v2    # "cekBitLength":I
    .end local v3    # "cekBitLengthBytes":[B
    .end local v5    # "cmkBitLength":I
    .end local v6    # "cmkBytes":[B
    .end local v8    # "encBytes":[B
    .end local v9    # "hashBitLength":I
    :catch_0
    move-exception v7

    .line 132
    .local v7, "e":Ljava/io/IOException;
    new-instance v13, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14, v7}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v13

    .line 124
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v2    # "cekBitLength":I
    .restart local v3    # "cekBitLengthBytes":[B
    .restart local v5    # "cmkBitLength":I
    .restart local v6    # "cmkBytes":[B
    .restart local v8    # "encBytes":[B
    .restart local v9    # "hashBitLength":I
    :cond_1
    :try_start_3
    sget-object v13, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ZERO_BYTES:[B

    invoke-virtual {v1, v13}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 144
    .restart local v10    # "hashInput":[B
    :catch_1
    move-exception v7

    .line 146
    .local v7, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v13, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v7}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14, v7}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v13
.end method

.method public static generateCIK(Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/EncryptionMethod;[B[B)Ljavax/crypto/SecretKey;
    .locals 15
    .param p0, "key"    # Ljavax/crypto/SecretKey;
    .param p1, "enc"    # Lcom/nimbusds/jose/EncryptionMethod;
    .param p2, "epu"    # [B
    .param p3, "epv"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 179
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 186
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v11, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ONE_BYTES:[B

    invoke-virtual {v1, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 189
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v5

    .line 190
    .local v5, "cmkBytes":[B
    invoke-virtual {v1, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 193
    array-length v11, v5

    mul-int/lit8 v4, v11, 0x8

    .line 194
    .local v4, "cmkBitLength":I
    move v8, v4

    .line 195
    .local v8, "hashBitLength":I
    move v2, v4

    .line 196
    .local v2, "cikBitLength":I
    invoke-static {v2}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v3

    .line 197
    .local v3, "cikBitLengthBytes":[B
    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 200
    invoke-virtual/range {p1 .. p1}, Lcom/nimbusds/jose/EncryptionMethod;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 201
    .local v7, "encBytes":[B
    invoke-virtual {v1, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 204
    if-eqz p2, :cond_0

    .line 206
    move-object/from16 v0, p2

    array-length v11, v0

    invoke-static {v11}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 207
    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 214
    :goto_0
    if-eqz p3, :cond_1

    .line 216
    move-object/from16 v0, p3

    array-length v11, v0

    invoke-static {v11}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 217
    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 224
    :goto_1
    sget-object v11, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->INTEGRITY_BYTES:[B

    invoke-virtual {v1, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    .line 238
    .local v9, "hashInput":[B
    :try_start_1
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "SHA-"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v10

    .line 246
    .local v10, "md":Ljava/security/MessageDigest;
    new-instance v11, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v10, v9}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "HMACSHA"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v11

    .line 210
    .end local v9    # "hashInput":[B
    .end local v10    # "md":Ljava/security/MessageDigest;
    :cond_0
    :try_start_2
    sget-object v11, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ZERO_BYTES:[B

    invoke-virtual {v1, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 226
    .end local v2    # "cikBitLength":I
    .end local v3    # "cikBitLengthBytes":[B
    .end local v4    # "cmkBitLength":I
    .end local v5    # "cmkBytes":[B
    .end local v7    # "encBytes":[B
    .end local v8    # "hashBitLength":I
    :catch_0
    move-exception v6

    .line 228
    .local v6, "e":Ljava/io/IOException;
    new-instance v11, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v6}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11

    .line 220
    .end local v6    # "e":Ljava/io/IOException;
    .restart local v2    # "cikBitLength":I
    .restart local v3    # "cikBitLengthBytes":[B
    .restart local v4    # "cmkBitLength":I
    .restart local v5    # "cmkBytes":[B
    .restart local v7    # "encBytes":[B
    .restart local v8    # "hashBitLength":I
    :cond_1
    :try_start_3
    sget-object v11, Lcom/nimbusds/jose/crypto/LegacyConcatKDF;->ZERO_BYTES:[B

    invoke-virtual {v1, v11}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 240
    .restart local v9    # "hashInput":[B
    :catch_1
    move-exception v6

    .line 242
    .local v6, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v11, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v6}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v6}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11
.end method
