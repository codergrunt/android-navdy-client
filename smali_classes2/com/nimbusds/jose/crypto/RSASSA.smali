.class Lcom/nimbusds/jose/crypto/RSASSA;
.super Ljava/lang/Object;
.source "RSASSA.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method

.method protected static getSignerAndVerifier(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/security/Provider;)Ljava/security/Signature;
    .locals 9
    .param p0, "alg"    # Lcom/nimbusds/jose/JWSAlgorithm;
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 43
    const/4 v0, 0x0

    .line 45
    .local v0, "pssSpec":Ljava/security/spec/PSSParameterSpec;
    sget-object v1, Lcom/nimbusds/jose/JWSAlgorithm;->RS256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v1}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    const-string v7, "SHA256withRSA"

    .line 69
    .local v7, "jcaAlg":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_7

    .line 70
    :try_start_0
    invoke-static {v7, p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 79
    .local v8, "signature":Ljava/security/Signature;
    :goto_1
    if-eqz v0, :cond_0

    .line 81
    :try_start_1
    invoke-virtual {v8, v0}, Ljava/security/Signature;->setParameter(Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_1

    .line 87
    :cond_0
    return-object v8

    .line 47
    .end local v7    # "jcaAlg":Ljava/lang/String;
    .end local v8    # "signature":Ljava/security/Signature;
    :cond_1
    sget-object v1, Lcom/nimbusds/jose/JWSAlgorithm;->RS384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v1}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 48
    const-string v7, "SHA384withRSA"

    .line 49
    .restart local v7    # "jcaAlg":Ljava/lang/String;
    goto :goto_0

    .end local v7    # "jcaAlg":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/nimbusds/jose/JWSAlgorithm;->RS512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v1}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 50
    const-string v7, "SHA512withRSA"

    .line 51
    .restart local v7    # "jcaAlg":Ljava/lang/String;
    goto :goto_0

    .end local v7    # "jcaAlg":Ljava/lang/String;
    :cond_3
    sget-object v1, Lcom/nimbusds/jose/JWSAlgorithm;->PS256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v1}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 52
    const-string v7, "SHA256withRSAandMGF1"

    .line 54
    .restart local v7    # "jcaAlg":Ljava/lang/String;
    new-instance v0, Ljava/security/spec/PSSParameterSpec;

    .end local v0    # "pssSpec":Ljava/security/spec/PSSParameterSpec;
    const-string v1, "SHA256"

    const-string v2, "MGF1"

    sget-object v3, Ljava/security/spec/MGF1ParameterSpec;->SHA256:Ljava/security/spec/MGF1ParameterSpec;

    const/16 v4, 0x20

    invoke-direct/range {v0 .. v5}, Ljava/security/spec/PSSParameterSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;II)V

    .line 55
    .restart local v0    # "pssSpec":Ljava/security/spec/PSSParameterSpec;
    goto :goto_0

    .end local v7    # "jcaAlg":Ljava/lang/String;
    :cond_4
    sget-object v1, Lcom/nimbusds/jose/JWSAlgorithm;->PS384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v1}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 56
    const-string v7, "SHA384withRSAandMGF1"

    .line 58
    .restart local v7    # "jcaAlg":Ljava/lang/String;
    new-instance v0, Ljava/security/spec/PSSParameterSpec;

    .end local v0    # "pssSpec":Ljava/security/spec/PSSParameterSpec;
    const-string v1, "SHA384"

    const-string v2, "MGF1"

    sget-object v3, Ljava/security/spec/MGF1ParameterSpec;->SHA384:Ljava/security/spec/MGF1ParameterSpec;

    const/16 v4, 0x30

    invoke-direct/range {v0 .. v5}, Ljava/security/spec/PSSParameterSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;II)V

    .line 59
    .restart local v0    # "pssSpec":Ljava/security/spec/PSSParameterSpec;
    goto :goto_0

    .end local v7    # "jcaAlg":Ljava/lang/String;
    :cond_5
    sget-object v1, Lcom/nimbusds/jose/JWSAlgorithm;->PS512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v1}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 60
    const-string v7, "SHA512withRSAandMGF1"

    .line 62
    .restart local v7    # "jcaAlg":Ljava/lang/String;
    new-instance v0, Ljava/security/spec/PSSParameterSpec;

    .end local v0    # "pssSpec":Ljava/security/spec/PSSParameterSpec;
    const-string v1, "SHA512"

    const-string v2, "MGF1"

    sget-object v3, Ljava/security/spec/MGF1ParameterSpec;->SHA512:Ljava/security/spec/MGF1ParameterSpec;

    const/16 v4, 0x40

    invoke-direct/range {v0 .. v5}, Ljava/security/spec/PSSParameterSpec;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/spec/AlgorithmParameterSpec;II)V

    .line 63
    .restart local v0    # "pssSpec":Ljava/security/spec/PSSParameterSpec;
    goto :goto_0

    .line 64
    .end local v7    # "jcaAlg":Ljava/lang/String;
    :cond_6
    new-instance v1, Lcom/nimbusds/jose/JOSEException;

    sget-object v2, Lcom/nimbusds/jose/crypto/RSASSAProvider;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-static {p0, v2}, Lcom/nimbusds/jose/crypto/AlgorithmSupportMessage;->unsupportedJWSAlgorithm(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 72
    .restart local v7    # "jcaAlg":Ljava/lang/String;
    :cond_7
    :try_start_2
    invoke-static {v7}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v8

    .restart local v8    # "signature":Ljava/security/Signature;
    goto :goto_1

    .line 74
    .end local v8    # "signature":Ljava/security/Signature;
    :catch_0
    move-exception v6

    .line 75
    .local v6, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v1, Lcom/nimbusds/jose/JOSEException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported RSASSA algorithm: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 82
    .end local v6    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v8    # "signature":Ljava/security/Signature;
    :catch_1
    move-exception v6

    .line 83
    .local v6, "e":Ljava/security/InvalidAlgorithmParameterException;
    new-instance v1, Lcom/nimbusds/jose/JOSEException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid RSASSA-PSS salt length parameter: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/security/InvalidAlgorithmParameterException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
