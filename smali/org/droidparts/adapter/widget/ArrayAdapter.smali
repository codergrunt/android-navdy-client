.class public Lorg/droidparts/adapter/widget/ArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ArrayAdapter.java"

# interfaces
.implements Lorg/droidparts/contract/AlterableContent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/ArrayAdapter",
        "<TT;>;",
        "Lorg/droidparts/contract/AlterableContent",
        "<",
        "Ljava/util/Collection",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private layoutInflater:Landroid/view/LayoutInflater;
    .annotation runtime Lorg/droidparts/annotation/inject/InjectSystemService;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 36
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, Lorg/droidparts/adapter/widget/ArrayAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILjava/util/List;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "rowResId"    # I
    .param p3, "textViewResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 50
    invoke-static {p1, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "rowResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    .local p3, "objects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const v0, 0x1020014

    invoke-direct {p0, p1, p2, v0, p3}, Lorg/droidparts/adapter/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    .local p2, "objects":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const v0, 0x1090003

    invoke-direct {p0, p1, v0, p2}, Lorg/droidparts/adapter/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected getLayoutInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 64
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    iget-object v0, p0, Lorg/droidparts/adapter/widget/ArrayAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public bridge synthetic setContent(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 29
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    check-cast p1, Ljava/util/Collection;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/adapter/widget/ArrayAdapter;->setContent(Ljava/util/Collection;)V

    return-void
.end method

.method public setContent(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lorg/droidparts/adapter/widget/ArrayAdapter;, "Lorg/droidparts/adapter/widget/ArrayAdapter<TT;>;"
    .local p1, "coll":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lorg/droidparts/adapter/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 56
    invoke-virtual {p0}, Lorg/droidparts/adapter/widget/ArrayAdapter;->clear()V

    .line 57
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 58
    .local v1, "item":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v1}, Lorg/droidparts/adapter/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    .end local v1    # "item":Ljava/lang/Object;, "TT;"
    :cond_0
    invoke-virtual {p0}, Lorg/droidparts/adapter/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 61
    return-void
.end method
