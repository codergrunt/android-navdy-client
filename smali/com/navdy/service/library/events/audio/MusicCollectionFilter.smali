.class public final Lcom/navdy/service/library/events/audio/MusicCollectionFilter;
.super Lcom/squareup/wire/Message;
.source "MusicCollectionFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FIELD:Ljava/lang/String; = ""

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final field:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;

    .prologue
    .line 34
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->field:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionFilter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicCollectionFilter$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionFilter$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->field:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->value:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    if-ne p1, p0, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 42
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;

    .line 43
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicCollectionFilter;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->field:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->field:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->value:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->value:Ljava/lang/String;

    .line 44
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->hashCode:I

    .line 50
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 51
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->field:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 52
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->value:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 53
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionFilter;->hashCode:I

    .line 55
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 51
    goto :goto_0
.end method
