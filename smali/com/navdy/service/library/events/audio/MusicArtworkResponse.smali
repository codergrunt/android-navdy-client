.class public final Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
.super Lcom/squareup/wire/Message;
.source "MusicArtworkResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALBUM:Ljava/lang/String; = ""

.field public static final DEFAULT_AUTHOR:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONID:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTO:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final album:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final author:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photo:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BYTES:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 20
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->DEFAULT_PHOTO:Lokio/ByteString;

    .line 21
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;

    .prologue
    .line 62
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->album:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->author:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->photo:Lokio/ByteString;

    iget-object v6, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v7, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionId:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;Lcom/navdy/service/library/events/audio/MusicArtworkResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;)V
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "album"    # Ljava/lang/String;
    .param p4, "author"    # Ljava/lang/String;
    .param p5, "photo"    # Lokio/ByteString;
    .param p6, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .param p7, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 53
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    .line 57
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 58
    iput-object p7, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    if-ne p1, p0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 70
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .line 71
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    .line 72
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    .line 77
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 82
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->hashCode:I

    .line 83
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 84
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v0

    .line 85
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 86
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 87
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 88
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    invoke-virtual {v2}, Lokio/ByteString;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 89
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 91
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->hashCode:I

    .line 93
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 84
    goto :goto_0

    :cond_3
    move v2, v1

    .line 85
    goto :goto_1

    :cond_4
    move v2, v1

    .line 86
    goto :goto_2

    :cond_5
    move v2, v1

    .line 87
    goto :goto_3

    :cond_6
    move v2, v1

    .line 88
    goto :goto_4

    :cond_7
    move v2, v1

    .line 89
    goto :goto_5
.end method
