.class public final Lcom/navdy/service/library/events/audio/MusicCapability;
.super Lcom/squareup/wire/Message;
.source "MusicCapability.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;,
        Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_AUTHORIZATIONSTATUS:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_COLLECTIONTYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_SUPPORTEDREPEATMODES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SUPPORTEDSHUFFLEMODES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionTypes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        enumType = Lcom/navdy/service/library/events/audio/MusicCollectionType;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;"
        }
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final supportedRepeatModes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        enumType = Lcom/navdy/service/library/events/audio/MusicRepeatMode;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
            ">;"
        }
    .end annotation
.end field

.field public final supportedShuffleModes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        enumType = Lcom/navdy/service/library/events/audio/MusicShuffleMode;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability;->DEFAULT_COLLECTIONTYPES:Ljava/util/List;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_NOT_DETERMINED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability;->DEFAULT_AUTHORIZATIONSTATUS:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability;->DEFAULT_SUPPORTEDSHUFFLEMODES:Ljava/util/List;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability;->DEFAULT_SUPPORTEDREPEATMODES:Ljava/util/List;

    .line 23
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicCapability$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicCapability$Builder;

    .prologue
    .line 83
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionTypes:Ljava/util/List;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->supportedShuffleModes:Ljava/util/List;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->supportedRepeatModes:Ljava/util/List;

    iget-object v6, p1, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->serial_number:Ljava/lang/Long;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/audio/MusicCapability;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/util/List;Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;)V

    .line 84
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCapability;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicCapability$Builder;Lcom/navdy/service/library/events/audio/MusicCapability$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicCapability$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicCapability$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCapability;-><init>(Lcom/navdy/service/library/events/audio/MusicCapability$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/util/List;Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;)V
    .locals 1
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p3, "authorizationStatus"    # Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    .param p6, "serial_number"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/MusicCollectionSource;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
            ">;",
            "Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicShuffleMode;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicRepeatMode;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    .local p4, "supportedShuffleModes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicShuffleMode;>;"
    .local p5, "supportedRepeatModes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicRepeatMode;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 75
    invoke-static {p2}, Lcom/navdy/service/library/events/audio/MusicCapability;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    .line 76
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 77
    invoke-static {p4}, Lcom/navdy/service/library/events/audio/MusicCapability;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedShuffleModes:Ljava/util/List;

    .line 78
    invoke-static {p5}, Lcom/navdy/service/library/events/audio/MusicCapability;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedRepeatModes:Ljava/util/List;

    .line 79
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    .line 80
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCapability;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCapability;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/MusicCapability;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    if-ne p1, p0, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v1

    .line 90
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicCapability;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 91
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCapability;

    .line 92
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicCapability;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    .line 93
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 94
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedShuffleModes:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedShuffleModes:Ljava/util/List;

    .line 95
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedRepeatModes:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedRepeatModes:Ljava/util/List;

    .line 96
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    .line 97
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 102
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->hashCode:I

    .line 103
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 104
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v0

    .line 105
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->collectionTypes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 106
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->authorizationStatus:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 107
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedShuffleModes:Ljava/util/List;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedShuffleModes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 108
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedRepeatModes:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->supportedRepeatModes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_0
    add-int v0, v2, v3

    .line 109
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    .line 110
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability;->hashCode:I

    .line 112
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 104
    goto :goto_0

    :cond_4
    move v2, v3

    .line 105
    goto :goto_1

    :cond_5
    move v2, v1

    .line 106
    goto :goto_2

    :cond_6
    move v2, v3

    .line 107
    goto :goto_3
.end method
