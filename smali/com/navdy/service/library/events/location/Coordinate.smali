.class public final Lcom/navdy/service/library/events/location/Coordinate;
.super Lcom/squareup/wire/Message;
.source "Coordinate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/location/Coordinate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACCURACY:Ljava/lang/Float;

.field public static final DEFAULT_ALTITUDE:Ljava/lang/Double;

.field public static final DEFAULT_BEARING:Ljava/lang/Float;

.field public static final DEFAULT_LATITUDE:Ljava/lang/Double;

.field public static final DEFAULT_LONGITUDE:Ljava/lang/Double;

.field public static final DEFAULT_PROVIDER:Ljava/lang/String; = ""

.field public static final DEFAULT_SPEED:Ljava/lang/Float;

.field public static final DEFAULT_TIMESTAMP:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final accuracy:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final altitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final bearing:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final latitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final longitude:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final provider:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final speed:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 17
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_LATITUDE:Ljava/lang/Double;

    .line 18
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_LONGITUDE:Ljava/lang/Double;

    .line 19
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_ACCURACY:Ljava/lang/Float;

    .line 20
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_ALTITUDE:Ljava/lang/Double;

    .line 21
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_BEARING:Ljava/lang/Float;

    .line 22
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_SPEED:Ljava/lang/Float;

    .line 23
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/Coordinate;->DEFAULT_TIMESTAMP:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/location/Coordinate$Builder;)V
    .locals 9
    .param p1, "builder"    # Lcom/navdy/service/library/events/location/Coordinate$Builder;

    .prologue
    .line 86
    iget-object v1, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->latitude:Ljava/lang/Double;

    iget-object v2, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->longitude:Ljava/lang/Double;

    iget-object v3, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->accuracy:Ljava/lang/Float;

    iget-object v4, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->altitude:Ljava/lang/Double;

    iget-object v5, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->bearing:Ljava/lang/Float;

    iget-object v6, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->speed:Ljava/lang/Float;

    iget-object v7, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->timestamp:Ljava/lang/Long;

    iget-object v8, p1, Lcom/navdy/service/library/events/location/Coordinate$Builder;->provider:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/location/Coordinate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/location/Coordinate$Builder;Lcom/navdy/service/library/events/location/Coordinate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/location/Coordinate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/location/Coordinate$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Lcom/navdy/service/library/events/location/Coordinate$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0
    .param p1, "latitude"    # Ljava/lang/Double;
    .param p2, "longitude"    # Ljava/lang/Double;
    .param p3, "accuracy"    # Ljava/lang/Float;
    .param p4, "altitude"    # Ljava/lang/Double;
    .param p5, "bearing"    # Ljava/lang/Float;
    .param p6, "speed"    # Ljava/lang/Float;
    .param p7, "timestamp"    # Ljava/lang/Long;
    .param p8, "provider"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    .line 76
    iput-object p2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    .line 77
    iput-object p3, p0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    .line 78
    iput-object p4, p0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    .line 79
    iput-object p5, p0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    .line 80
    iput-object p6, p0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    .line 81
    iput-object p7, p0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    .line 82
    iput-object p8, p0, Lcom/navdy/service/library/events/location/Coordinate;->provider:Ljava/lang/String;

    .line 83
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 92
    if-ne p1, p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/location/Coordinate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 94
    check-cast v0, Lcom/navdy/service/library/events/location/Coordinate;

    .line 95
    .local v0, "o":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    .line 96
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    .line 97
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    .line 98
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    .line 99
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    .line 101
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->provider:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->provider:Ljava/lang/String;

    .line 102
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/Coordinate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 107
    iget v0, p0, Lcom/navdy/service/library/events/location/Coordinate;->hashCode:I

    .line 108
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 109
    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v0

    .line 110
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 111
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 112
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 113
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 114
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 115
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 116
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/location/Coordinate;->provider:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/location/Coordinate;->provider:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 117
    iput v0, p0, Lcom/navdy/service/library/events/location/Coordinate;->hashCode:I

    .line 119
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 109
    goto :goto_0

    :cond_3
    move v2, v1

    .line 110
    goto :goto_1

    :cond_4
    move v2, v1

    .line 111
    goto :goto_2

    :cond_5
    move v2, v1

    .line 112
    goto :goto_3

    :cond_6
    move v2, v1

    .line 113
    goto :goto_4

    :cond_7
    move v2, v1

    .line 114
    goto :goto_5

    :cond_8
    move v2, v1

    .line 115
    goto :goto_6
.end method
