.class public final Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;
.super Lcom/squareup/wire/Message;
.source "PhotoUpdateQuery.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALBUM:Ljava/lang/String; = ""

.field public static final DEFAULT_AUTHOR:Ljava/lang/String; = ""

.field public static final DEFAULT_CHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

.field public static final DEFAULT_SIZE:Ljava/lang/Long;

.field public static final DEFAULT_TRACK:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final album:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final author:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final checksum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photoType:Lcom/navdy/service/library/events/photo/PhotoType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final size:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final track:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 22
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->DEFAULT_SIZE:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;

    .prologue
    .line 63
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->identifier:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->size:Ljava/lang/Long;

    iget-object v4, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->checksum:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->album:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->track:Ljava/lang/String;

    iget-object v7, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;->author:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQuery$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;
    .param p3, "size"    # Ljava/lang/Long;
    .param p4, "checksum"    # Ljava/lang/String;
    .param p5, "album"    # Ljava/lang/String;
    .param p6, "track"    # Ljava/lang/String;
    .param p7, "author"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->identifier:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 55
    iput-object p3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->size:Ljava/lang/Long;

    .line 56
    iput-object p4, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->checksum:Ljava/lang/String;

    .line 57
    iput-object p5, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->album:Ljava/lang/String;

    .line 58
    iput-object p6, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->track:Ljava/lang/String;

    .line 59
    iput-object p7, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->author:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p1, p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 70
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 71
    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;

    .line 72
    .local v0, "o":Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;
    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->identifier:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->size:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->size:Ljava/lang/Long;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->checksum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->checksum:Ljava/lang/String;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->album:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->album:Ljava/lang/String;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->track:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->track:Ljava/lang/String;

    .line 77
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->author:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->author:Ljava/lang/String;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 83
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->hashCode:I

    .line 84
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 85
    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->identifier:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 86
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/photo/PhotoType;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 87
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->size:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->size:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 88
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->checksum:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->checksum:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 89
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->album:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->album:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->track:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->track:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->author:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->author:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 92
    iput v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQuery;->hashCode:I

    .line 94
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 85
    goto :goto_0

    :cond_3
    move v2, v1

    .line 86
    goto :goto_1

    :cond_4
    move v2, v1

    .line 87
    goto :goto_2

    :cond_5
    move v2, v1

    .line 88
    goto :goto_3

    :cond_6
    move v2, v1

    .line 89
    goto :goto_4

    :cond_7
    move v2, v1

    .line 90
    goto :goto_5
.end method
