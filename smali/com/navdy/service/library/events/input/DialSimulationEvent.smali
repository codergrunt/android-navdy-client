.class public final Lcom/navdy/service/library/events/input/DialSimulationEvent;
.super Lcom/squareup/wire/Message;
.source "DialSimulationEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;,
        Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DIALACTION:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

.field private static final serialVersionUID:J


# instance fields
.field public final dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    sput-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->DEFAULT_DIALACTION:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;

    .prologue
    .line 25
    iget-object v0, p1, Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/input/DialSimulationEvent;-><init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;)V

    .line 26
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/input/DialSimulationEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;Lcom/navdy/service/library/events/input/DialSimulationEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/input/DialSimulationEvent$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/input/DialSimulationEvent;-><init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;)V
    .locals 0
    .param p1, "dialAction"    # Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 31
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 33
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 32
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/input/DialSimulationEvent;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    check-cast p1, Lcom/navdy/service/library/events/input/DialSimulationEvent;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/input/DialSimulationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 38
    iget v0, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->hashCode:I

    .line 39
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->dialAction:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
