.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
.super Lcom/squareup/wire/Message;
.source "NavigationSessionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_NEWSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final DEFAULT_ORIGINDISPLAY:Ljava/lang/Boolean;

.field public static final DEFAULT_ROUTEID:Ljava/lang/String; = ""

.field public static final DEFAULT_SIMULATIONSPEED:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final originDisplay:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final simulationSpeed:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->DEFAULT_NEWSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 20
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->DEFAULT_SIMULATIONSPEED:Ljava/lang/Integer;

    .line 21
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->DEFAULT_ORIGINDISPLAY:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;

    .prologue
    .line 62
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->label:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->routeId:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->simulationSpeed:Ljava/lang/Integer;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 63
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "routeId"    # Ljava/lang/String;
    .param p4, "simulationSpeed"    # Ljava/lang/Integer;
    .param p5, "originDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 55
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    .line 58
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->originDisplay:Ljava/lang/Boolean;

    .line 59
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 68
    if-ne p1, p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 70
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .line 71
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    .line 72
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->originDisplay:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->originDisplay:Ljava/lang/Boolean;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 80
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->hashCode:I

    .line 81
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 82
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->hashCode()I

    move-result v0

    .line 83
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 84
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 85
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 86
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->originDisplay:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->originDisplay:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 87
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->hashCode:I

    .line 89
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 82
    goto :goto_0

    :cond_3
    move v2, v1

    .line 83
    goto :goto_1

    :cond_4
    move v2, v1

    .line 84
    goto :goto_2

    :cond_5
    move v2, v1

    .line 85
    goto :goto_3
.end method
