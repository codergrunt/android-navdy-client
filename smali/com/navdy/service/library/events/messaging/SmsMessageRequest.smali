.class public final Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
.super Lcom/squareup/wire/Message;
.source "SmsMessageRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;

    .prologue
    .line 42
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->number:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->message:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->id:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;Lcom/navdy/service/library/events/messaging/SmsMessageRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/messaging/SmsMessageRequest$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;-><init>(Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "id"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    .line 51
    .local v0, "o":Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    .line 52
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    .line 53
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    .line 54
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    iget v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->hashCode:I

    .line 60
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 61
    iget-object v2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 62
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 63
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 64
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 65
    iput v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->hashCode:I

    .line 67
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 61
    goto :goto_0

    :cond_3
    move v2, v1

    .line 62
    goto :goto_1

    :cond_4
    move v2, v1

    .line 63
    goto :goto_2
.end method
