.class public final Lcom/navdy/service/library/task/TaskManager;
.super Ljava/lang/Object;
.source "TaskManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;,
        Lcom/navdy/service/library/task/TaskManager$TaskManagerExecutor;,
        Lcom/navdy/service/library/task/TaskManager$PriorityTaskComparator;,
        Lcom/navdy/service/library/task/TaskManager$PriorityTask;,
        Lcom/navdy/service/library/task/TaskManager$TaskPriority;
    }
.end annotation


# static fields
.field private static final INITIAL_CAPACITY:I = 0xa

.field private static final VERBOSE:Z

.field private static orderCounter:Ljava/util/concurrent/atomic/AtomicLong;

.field private static sLogger:Lcom/navdy/service/library/log/Logger;

.field private static final sPriorityComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final sSingleton:Lcom/navdy/service/library/task/TaskManager;

.field private static final threadNameCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final executors:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private initialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/task/TaskManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 112
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager;->orderCounter:Ljava/util/concurrent/atomic/AtomicLong;

    .line 142
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager;->threadNameCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 144
    new-instance v0, Lcom/navdy/service/library/task/TaskManager$PriorityTaskComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/navdy/service/library/task/TaskManager$PriorityTaskComparator;-><init>(Lcom/navdy/service/library/task/TaskManager$1;)V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager;->sPriorityComparator:Ljava/util/Comparator;

    .line 147
    new-instance v0, Lcom/navdy/service/library/task/TaskManager;

    invoke-direct {v0}, Lcom/navdy/service/library/task/TaskManager;-><init>()V

    sput-object v0, Lcom/navdy/service/library/task/TaskManager;->sSingleton:Lcom/navdy/service/library/task/TaskManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/task/TaskManager;->executors:Landroid/util/SparseArray;

    .line 159
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/service/library/task/TaskManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/service/library/task/TaskManager;->threadNameCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private createExecutor(I)Ljava/util/concurrent/ExecutorService;
    .locals 9
    .param p1, "poolSize"    # I

    .prologue
    .line 183
    new-instance v1, Lcom/navdy/service/library/task/TaskManager$TaskManagerExecutor;

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v0, 0xa

    sget-object v2, Lcom/navdy/service/library/task/TaskManager;->sPriorityComparator:Ljava/util/Comparator;

    invoke-direct {v7, v0, v2}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    new-instance v8, Lcom/navdy/service/library/task/TaskManager$1;

    invoke-direct {v8, p0}, Lcom/navdy/service/library/task/TaskManager$1;-><init>(Lcom/navdy/service/library/task/TaskManager;)V

    move v2, p1

    move v3, p1

    invoke-direct/range {v1 .. v8}, Lcom/navdy/service/library/task/TaskManager$TaskManagerExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v1
.end method

.method public static getInstance()Lcom/navdy/service/library/task/TaskManager;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/navdy/service/library/task/TaskManager;->sSingleton:Lcom/navdy/service/library/task/TaskManager;

    return-object v0
.end method


# virtual methods
.method public addTaskQueue(II)V
    .locals 2
    .param p1, "queueId"    # I
    .param p2, "poolSize"    # I

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/navdy/service/library/task/TaskManager;->initialized:Z

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/task/TaskManager;->executors:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "already exists"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/task/TaskManager;->executors:Landroid/util/SparseArray;

    invoke-direct {p0, p2}, Lcom/navdy/service/library/task/TaskManager;->createExecutor(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 176
    return-void
.end method

.method public execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "queueId"    # I

    .prologue
    .line 214
    sget-object v0, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->NORMAL:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;
    .locals 5
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "queueId"    # I
    .param p3, "taskPriority"    # Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    .prologue
    .line 198
    iget-boolean v2, p0, Lcom/navdy/service/library/task/TaskManager;->initialized:Z

    if-nez v2, :cond_0

    .line 199
    sget-object v2, Lcom/navdy/service/library/task/TaskManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Task manager not initialized"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 200
    const/4 v2, 0x0

    .line 210
    :goto_0
    return-object v2

    .line 205
    :cond_0
    if-nez p1, :cond_1

    .line 206
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 208
    :cond_1
    iget-object v2, p0, Lcom/navdy/service/library/task/TaskManager;->executors:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    .line 209
    .local v0, "executorService":Ljava/util/concurrent/ExecutorService;
    new-instance v1, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    sget-object v2, Lcom/navdy/service/library/task/TaskManager;->orderCounter:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-direct {v1, p1, p3, v2, v3}, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;-><init>(Ljava/lang/Runnable;Lcom/navdy/service/library/task/TaskManager$TaskPriority;J)V

    .line 210
    .local v1, "priorityRunnable":Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;
    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    goto :goto_0
.end method

.method public getExecutor(I)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p1, "queueId"    # I

    .prologue
    .line 179
    iget-object v0, p0, Lcom/navdy/service/library/task/TaskManager;->executors:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/navdy/service/library/task/TaskManager;->initialized:Z

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/task/TaskManager;->initialized:Z

    .line 166
    return-void
.end method
