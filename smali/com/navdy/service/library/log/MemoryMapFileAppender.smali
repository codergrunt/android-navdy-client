.class public Lcom/navdy/service/library/log/MemoryMapFileAppender;
.super Ljava/lang/Object;
.source "MemoryMapFileAppender.java"

# interfaces
.implements Lcom/navdy/service/library/log/LogAppender;


# static fields
.field private static final COLON:Ljava/lang/String; = ":"

.field private static final CURRENT_POINTER_PREF:Ljava/lang/String; = "pointer"

.field private static final CURRENT_POINTER_PREF_FILE_SUFFIX:Ljava/lang/String; = "_current_log_pointer"

.field private static final MIN_FILE_SIZE:J = 0x4000L

.field private static final NEWLINE:Ljava/lang/String; = "\r\n"

.field private static final ROLLOVER_MARKER:[B

.field private static final ROLLOVER_META_LEN:I

.field private static final SLASH:Ljava/lang/String; = "/"

.field private static final SPACE:Ljava/lang/String; = " "

.field private static final TAG:Ljava/lang/String; = "MemoryMapFileAppender"


# instance fields
.field private builder:Ljava/lang/StringBuilder;

.field private context:Landroid/content/Context;

.field private currentPointerPrefEditor:Landroid/content/SharedPreferences$Editor;

.field private currentPointerPrefFileName:Ljava/lang/String;

.field private dateFormat:Ljava/text/DateFormat;

.field private fileName:Ljava/lang/String;

.field private fileSize:J

.field private maxFiles:I

.field private memoryMap:Ljava/nio/MappedByteBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "\r\n<<<<rolling>>>>\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->ROLLOVER_MARKER:[B

    .line 50
    sget-object v0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->ROLLOVER_MARKER:[B

    array-length v0, v0

    sput v0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->ROLLOVER_META_LEN:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "fileSize"    # J
    .param p6, "maxFiles"    # I

    .prologue
    .line 81
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/log/MemoryMapFileAppender;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JIZ)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JIZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "fileSize"    # J
    .param p6, "maxFiles"    # I
    .param p7, "useProcessName"    # Z

    .prologue
    const/4 v4, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x5000

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    .line 57
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM-dd HH:mm:ss.SSS"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->dateFormat:Ljava/text/DateFormat;

    .line 104
    if-eqz p1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 105
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    .line 107
    :cond_1
    iput p6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    .line 108
    const-wide/16 v4, 0x4000

    cmp-long v4, p4, v4

    if-gez v4, :cond_2

    .line 109
    const-wide/16 p4, 0x4000

    .line 111
    :cond_2
    const-string v2, ""

    .line 112
    .local v2, "processName":Ljava/lang/String;
    if-eqz p7, :cond_3

    .line 113
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    invoke-static {p1, v5}, Lcom/navdy/service/library/util/SystemUtils;->getProcessName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 115
    :cond_3
    iput-object p1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->context:Landroid/content/Context;

    .line 116
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    .line 117
    iput-wide p4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileSize:J

    .line 118
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_current_log_pointer"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->currentPointerPrefFileName:Ljava/lang/String;

    .line 121
    :try_start_0
    const-string v4, "MemoryMapFileAppender"

    const-string v5, "MemoryMapFileAppender::ctor::start"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-static {p2}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/lang/String;)Z

    .line 123
    iget-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->currentPointerPrefFileName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 126
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v4, "pointer"

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 127
    .local v0, "currentPointer":I
    if-lez v0, :cond_4

    int-to-long v4, v0

    cmp-long v4, v4, p4

    if-gez v4, :cond_4

    .line 128
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "pointer"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 129
    const-string v4, "MemoryMapFileAppender"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MemoryMapFileAppender, pos set = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v0, p4, p5}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->setMemoryMap(Ljava/lang/String;IJ)V

    .line 134
    const-string v4, "MemoryMapFileAppender"

    const-string v5, "MemoryMapFileAppender::ctor::end"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    .end local v0    # "currentPointer":I
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v3

    .line 136
    .local v3, "t":Ljava/lang/Throwable;
    const-string v4, "MemoryMapFileAppender"

    const-string v5, "MemoryMapFileAppender.ctor()::"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private getFormatedNumber(I)Ljava/lang/String;
    .locals 6
    .param p1, "i"    # I

    .prologue
    .line 241
    iget v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    int-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    double-to-int v0, v2

    .line 242
    .local v0, "nbDigits":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "d"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 5
    .param p0, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 361
    if-nez p0, :cond_0

    .line 362
    const-string v3, ""

    .line 377
    :goto_0
    return-object v3

    .line 366
    :cond_0
    move-object v2, p0

    .line 367
    .local v2, "t":Ljava/lang/Throwable;
    :goto_1
    if-eqz v2, :cond_2

    .line 368
    instance-of v3, v2, Ljava/net/UnknownHostException;

    if-eqz v3, :cond_1

    .line 369
    const-string v3, ""

    goto :goto_0

    .line 371
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_1

    .line 373
    :cond_2
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 374
    .local v1, "sw":Ljava/io/StringWriter;
    new-instance v0, Lcom/navdy/service/library/log/FastPrintWriter;

    const/4 v3, 0x0

    const/16 v4, 0x100

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/log/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    .line 375
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 376
    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    .line 377
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private rollFiles()V
    .locals 7

    .prologue
    .line 218
    iget v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    if-lez v4, :cond_0

    .line 219
    iget-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->context:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->getFormatedNumber(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".txt"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 223
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    if-ge v1, v4, :cond_3

    .line 225
    iget v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    .line 226
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 230
    .local v2, "src":Ljava/io/File;
    :goto_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 231
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->getFormatedNumber(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "dst":Ljava/io/File;
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    .line 233
    .local v3, "worked":Z
    if-nez v3, :cond_1

    .line 234
    const-string v4, "MemoryMapFileAppender"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to rename "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".txt"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    .end local v0    # "dst":Ljava/io/File;
    .end local v3    # "worked":Z
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 228
    .end local v2    # "src":Ljava/io/File;
    :cond_2
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-direct {p0, v5}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->getFormatedNumber(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v2    # "src":Ljava/io/File;
    goto/16 :goto_1

    .line 238
    .end local v2    # "src":Ljava/io/File;
    :cond_3
    return-void
.end method

.method private declared-synchronized rollOver()V
    .locals 4

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 249
    iget-object v0, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    sget-object v1, Lcom/navdy/service/library/log/MemoryMapFileAppender;->ROLLOVER_MARKER:[B

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    :goto_0
    monitor-exit p0

    return-void

    .line 252
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v0}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 253
    invoke-direct {p0}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->rollFiles()V

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileSize:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->setMemoryMap(Ljava/lang/String;IJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setMemoryMap(Ljava/lang/String;IJ)V
    .locals 11
    .param p1, "outputPath"    # Ljava/lang/String;
    .param p2, "position"    # I
    .param p3, "size"    # J

    .prologue
    .line 190
    const/4 v6, 0x0

    .line 191
    .local v6, "dataFileHandle":Ljava/io/RandomAccessFile;
    const/4 v0, 0x0

    .line 194
    .local v0, "dataFileChannel":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 195
    .local v8, "file":Ljava/io/File;
    if-nez p2, :cond_0

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    invoke-direct {p0}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->rollFiles()V

    .line 198
    :cond_0
    new-instance v7, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v7, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    .end local v6    # "dataFileHandle":Ljava/io/RandomAccessFile;
    .local v7, "dataFileHandle":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 200
    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    add-long/2addr v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    .line 201
    if-lez p2, :cond_1

    .line 202
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v1, p2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 204
    :cond_1
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 205
    const/4 v6, 0x0

    .line 206
    .end local v7    # "dataFileHandle":Ljava/io/RandomAccessFile;
    .restart local v6    # "dataFileHandle":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 207
    const/4 v0, 0x0

    .line 214
    .end local v8    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 208
    :catch_0
    move-exception v9

    .line 209
    .local v9, "t":Ljava/lang/Throwable;
    :goto_1
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 210
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 211
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    .line 212
    const-string v1, "MemoryMapFileAppender"

    const-string v2, "setMemoryMap::"

    invoke-static {v1, v2, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 208
    .end local v6    # "dataFileHandle":Ljava/io/RandomAccessFile;
    .end local v9    # "t":Ljava/lang/Throwable;
    .restart local v7    # "dataFileHandle":Ljava/io/RandomAccessFile;
    .restart local v8    # "file":Ljava/io/File;
    :catch_1
    move-exception v9

    move-object v6, v7

    .end local v7    # "dataFileHandle":Ljava/io/RandomAccessFile;
    .restart local v6    # "dataFileHandle":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method

.method private truncateFileAtCurrentPosition(Ljava/lang/String;)V
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 321
    iget-object v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v5}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v5

    int-to-long v2, v5

    .line 322
    .local v2, "position":J
    const/4 v1, 0x0

    .line 324
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v5, "rws"

    invoke-direct {v4, p1, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .local v4, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v4, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 331
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v4

    .line 333
    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 331
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 328
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 329
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 331
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v5

    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local v4    # "raf":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v5

    move-object v1, v4

    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 328
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local v4    # "raf":Ljava/io/RandomAccessFile;
    :catch_2
    move-exception v0

    move-object v1, v4

    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 326
    .end local v1    # "raf":Ljava/io/RandomAccessFile;
    .restart local v4    # "raf":Ljava/io/RandomAccessFile;
    :catch_3
    move-exception v0

    move-object v1, v4

    .end local v4    # "raf":Ljava/io/RandomAccessFile;
    .restart local v1    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method


# virtual methods
.method protected append(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 168
    :try_start_0
    iget-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    if-nez v4, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    if-eqz p1, :cond_0

    .line 172
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 173
    .local v1, "data":[B
    array-length v2, v1

    .line 174
    .local v2, "data_len":I
    sget v4, Lcom/navdy/service/library/log/MemoryMapFileAppender;->ROLLOVER_META_LEN:I

    add-int/2addr v4, v2

    int-to-long v4, v4

    iget-wide v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileSize:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 178
    iget-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v0

    .line 179
    .local v0, "cur_pos":I
    add-int v4, v0, v2

    int-to-long v4, v4

    iget-wide v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileSize:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 180
    invoke-direct {p0}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->rollOver()V

    .line 182
    :cond_2
    iget-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 184
    .end local v0    # "cur_pos":I
    .end local v1    # "data":[B
    .end local v2    # "data_len":I
    :catch_0
    move-exception v3

    .line 185
    .local v3, "t":Ljava/lang/Throwable;
    const-string v4, "MemoryMapFileAppender"

    const-string v5, "MemoryMapFileAppender.append()::"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;
    .param p4, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 142
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :try_start_1
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 144
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->dateFormat:Ljava/text/DateFormat;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    if-eqz p3, :cond_0

    .line 152
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_0
    if-eqz p4, :cond_1

    .line 155
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    invoke-static {p4}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    iget-object v1, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;)V

    .line 160
    monitor-exit p0

    .line 164
    :goto_0
    return-void

    .line 160
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 313
    monitor-enter p0

    .line 314
    :try_start_0
    const-string v0, "MemoryMapFileAppender"

    const-string v1, "MemoryMapFileAppender:closing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-virtual {p0}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->flush()V

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    .line 317
    monitor-exit p0

    .line 318
    return-void

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 272
    const-string v0, "D"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 278
    const-string v0, "D"

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 279
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 303
    const-string v0, "W"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 304
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 308
    const-string v0, "W"

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 309
    return-void
.end method

.method public flush()V
    .locals 6

    .prologue
    .line 336
    monitor-enter p0

    .line 338
    :try_start_0
    iget-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    if-eqz v3, :cond_1

    .line 339
    iget-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v0

    .line 340
    .local v0, "curPos":I
    iget-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->memoryMap:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 341
    if-lez v0, :cond_1

    .line 343
    iget-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->currentPointerPrefEditor:Landroid/content/SharedPreferences$Editor;

    if-nez v3, :cond_0

    .line 344
    iget-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->currentPointerPrefFileName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 347
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->currentPointerPrefEditor:Landroid/content/SharedPreferences$Editor;

    .line 350
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v3, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->currentPointerPrefEditor:Landroid/content/SharedPreferences$Editor;

    const-string v4, "pointer"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 351
    const-string v3, "MemoryMapFileAppender"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MemoryMapFileAppender:stored pref pos:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    .end local v0    # "curPos":I
    :cond_1
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 358
    return-void

    .line 354
    :catch_0
    move-exception v2

    .line 355
    .local v2, "t":Ljava/lang/Throwable;
    const-string v3, "MemoryMapFileAppender"

    const-string v4, "MemoryMapFileAppender:flush"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 357
    .end local v2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public getLogFiles()Ljava/util/ArrayList;
    .locals 10
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    new-instance v3, Ljava/util/ArrayList;

    iget v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 383
    .local v3, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    if-ge v4, v5, :cond_2

    .line 384
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v4}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->getFormatedNumber(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".txt"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 385
    .local v0, "currentFile":Ljava/lang/String;
    iget v5, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->maxFiles:I

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_0

    .line 387
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/navdy/service/library/log/MemoryMapFileAppender;->fileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".txt"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/navdy/service/library/util/IOUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 388
    invoke-direct {p0, v0}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->truncateFileAtCurrentPosition(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    :cond_0
    :goto_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 394
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 395
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 389
    .end local v2    # "file":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 390
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 398
    .end local v0    # "currentFile":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    return-object v3
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 283
    const-string v0, "I"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 284
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 288
    const-string v0, "I"

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 289
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 261
    const-string v0, "V"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 263
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 267
    const-string v0, "V"

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 268
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 293
    const-string v0, "W"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 294
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 298
    const-string v0, "W"

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/navdy/service/library/log/MemoryMapFileAppender;->append(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 299
    return-void
.end method
