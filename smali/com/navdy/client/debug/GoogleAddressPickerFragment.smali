.class public Lcom/navdy/client/debug/GoogleAddressPickerFragment;
.super Lcom/navdy/client/debug/common/BaseDebugFragment;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;,
        Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteListener;,
        Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;
    }
.end annotation


# static fields
.field private static final HERE_GEOCODER_ENDPOINT:Ljava/lang/String; = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id="

.field private static final HERE_GEOCODER_ENDPOINT2:Ljava/lang/String; = "&app_code="

.field private static final HERE_GEOCODER_ENDPOINT3:Ljava/lang/String; = "&gen=9&searchtext="

.field private static HERE_GEO_APP_ID:Ljava/lang/String; = null

.field private static final HERE_GEO_APP_ID_METADATA:Ljava/lang/String; = "HERE_GEO_APP_ID"

.field private static HERE_GEO_APP_TOKEN:Ljava/lang/String; = null

.field private static final HERE_GEO_APP_TOKEN_METADATA:Ljava/lang/String; = "HERE_GEO_APP_TOKEN"

.field private static final HERE_LATITUDE_STR:Ljava/lang/String; = "Latitude"

.field private static final HERE_LOCATION_STR:Ljava/lang/String; = "Location"

.field private static final HERE_LONGITUDE_STR:Ljava/lang/String; = "Longitude"

.field private static final HERE_NAVIGATION_STR:Ljava/lang/String; = "NavigationPosition"

.field private static final HERE_RESPONSE_STR:Ljava/lang/String; = "Response"

.field private static final HERE_RESULT_STR:Ljava/lang/String; = "Result"

.field private static final HERE_VIEW_STR:Ljava/lang/String; = "View"

.field private static final ROUTE_REQUEST_TIMEOUT:I = 0x3a98

.field private static final SEARCH_RADIUS_METERS:I = 0xc350

.field private static final SI_IS_ROUTE_REQUEST_PENDING:Ljava/lang/String; = "1"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private chousedRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field private currentSelectedRouteIndex:I

.field private event:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

.field private geoApiContext:Lcom/google/maps/GeoApiContext;

.field private handler:Landroid/os/Handler;

.field private lastLocation:Landroid/location/Location;

.field listView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10022d
    .end annotation
.end field

.field private mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

.field private mAutocompleteClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field mAutocompleteView:Landroid/widget/AutoCompleteTextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10024f
    .end annotation
.end field

.field protected mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field protected mLastSelectedPlace:Lcom/google/android/gms/location/places/Place;

.field private mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

.field private mMap:Lcom/google/android/gms/maps/GoogleMap;

.field protected mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

.field protected mSearchLocation:Lcom/google/android/gms/maps/model/LatLng;

.field private mUpdatePlaceDetailsCallback:Lcom/google/android/gms/common/api/ResultCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/ResultCallback",
            "<",
            "Lcom/google/android/gms/location/places/PlaceBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private progressDialog:Landroid/app/ProgressDialog;

.field private requestedLocationUpdates:Z

.field private routeDescriptionAdapter:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

.field private routeRequestSendToHud:Z

.field private routeRequestTimeout:Ljava/lang/Runnable;

.field private routes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/model/Polyline;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 96
    new-instance v3, Lcom/navdy/service/library/log/Logger;

    const-class v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-direct {v3, v4}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v3, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 133
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 134
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 135
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 137
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_0

    .line 138
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "HERE_GEO_APP_ID"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->HERE_GEO_APP_ID:Ljava/lang/String;

    .line 139
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "HERE_GEO_APP_TOKEN"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->HERE_GEO_APP_TOKEN:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_0
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v2

    .line 142
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/client/debug/common/BaseDebugFragment;-><init>()V

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    .line 172
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->currentSelectedRouteIndex:I

    .line 177
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->handler:Landroid/os/Handler;

    .line 182
    new-instance v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$1;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestTimeout:Ljava/lang/Runnable;

    .line 360
    new-instance v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAutocompleteClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 402
    new-instance v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mUpdatePlaceDetailsCallback:Lcom/google/android/gms/common/api/ResultCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/google/android/gms/common/api/ResultCallback;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mUpdatePlaceDetailsCallback:Lcom/google/android/gms/common/api/ResultCallback;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->startProgress(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/location/places/Place;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # Lcom/google/android/gms/location/places/Place;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->updateMap(Lcom/google/android/gms/location/places/Place;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/maps/model/LatLng;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # Lcom/google/android/gms/location/places/Place;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->searchRoutesOnHud(Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/maps/model/LatLng;)V

    return-void
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->HERE_GEO_APP_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->HERE_GEO_APP_TOKEN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/maps/model/LatLng;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getSelectedRoute(Lcom/google/android/gms/maps/model/LatLng;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->currentSelectedRouteIndex:I

    return v0
.end method

.method static synthetic access$1702(Lcom/navdy/client/debug/GoogleAddressPickerFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->currentSelectedRouteIndex:I

    return p1
.end method

.method static synthetic access$1704(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->currentSelectedRouteIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->currentSelectedRouteIndex:I

    return v0
.end method

.method static synthetic access$1800(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestTimeout:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->stopProgress()V

    return-void
.end method

.method static synthetic access$2000(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->chousedRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->event:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    return-object v0
.end method

.method static synthetic access$302(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/maps/GoogleMap;)Lcom/google/android/gms/maps/GoogleMap;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;
    .param p1, "x1"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->initMap()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->resetRouteColor()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/client/debug/PlaceAutocompleteAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    return-object v0
.end method

.method private addPolyline(Ljava/util/List;)Lcom/google/android/gms/maps/model/Polyline;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/model/LatLng;",
            ">;)",
            "Lcom/google/android/gms/maps/model/Polyline;"
        }
    .end annotation

    .prologue
    .line 645
    .local p1, "points":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    new-instance v1, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    .line 646
    .local v1, "opt":Lcom/google/android/gms/maps/model/PolylineOptions;
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->addAll(Ljava/lang/Iterable;)Lcom/google/android/gms/maps/model/PolylineOptions;

    .line 647
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v3

    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0012

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v0

    .line 648
    .local v0, "line":Lcom/google/android/gms/maps/model/Polyline;
    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ADD TO MAP:/....... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 649
    return-object v0
.end method

.method private findFirstDifference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "label1"    # Ljava/lang/String;
    .param p2, "label2"    # Ljava/lang/String;

    .prologue
    .line 832
    const-string v4, ","

    invoke-static {p1, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 833
    .local v0, "firstLabelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, ","

    invoke-static {p2, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 834
    .local v3, "secondLabelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 835
    .local v1, "i":I
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 836
    .local v2, "l":Ljava/lang/String;
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 841
    .end local v2    # "l":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 839
    .restart local v2    # "l":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 840
    goto :goto_0

    .line 841
    .end local v2    # "l":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v2, v4

    goto :goto_1
.end method

.method private getGoogleNavigationCoordinate(Lcom/google/android/gms/location/places/Place;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V
    .locals 10
    .param p1, "place"    # Lcom/google/android/gms/location/places/Place;
    .param p2, "callback"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    .prologue
    .line 484
    :try_start_0
    sget-object v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "using direction api to get nav position"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 485
    invoke-interface {p1}, Lcom/google/android/gms/location/places/Place;->getLatLng()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 486
    .local v1, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v2, Lcom/google/maps/model/LatLng;

    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    .line 487
    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    .line 488
    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/google/maps/model/LatLng;-><init>(DD)V

    .line 489
    .local v2, "origin":Lcom/google/maps/model/LatLng;
    new-instance v0, Lcom/google/maps/model/LatLng;

    iget-wide v6, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v8, v1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/google/maps/model/LatLng;-><init>(DD)V

    .line 492
    .local v0, "destination":Lcom/google/maps/model/LatLng;
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->geoApiContext:Lcom/google/maps/GeoApiContext;

    invoke-static {v5}, Lcom/google/maps/DirectionsApi;->newRequest(Lcom/google/maps/GeoApiContext;)Lcom/google/maps/DirectionsApiRequest;

    move-result-object v5

    .line 493
    invoke-virtual {v5, v0}, Lcom/google/maps/DirectionsApiRequest;->destination(Lcom/google/maps/model/LatLng;)Lcom/google/maps/DirectionsApiRequest;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/maps/DirectionsApiRequest;->origin(Lcom/google/maps/model/LatLng;)Lcom/google/maps/DirectionsApiRequest;

    move-result-object v3

    .line 494
    .local v3, "request":Lcom/google/maps/DirectionsApiRequest;
    new-instance v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;

    invoke-direct {v5, p0, p2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$6;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V

    invoke-virtual {v3, v5}, Lcom/google/maps/DirectionsApiRequest;->setCallback(Lcom/google/maps/PendingResult$Callback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 528
    .end local v0    # "destination":Lcom/google/maps/model/LatLng;
    .end local v1    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .end local v2    # "origin":Lcom/google/maps/model/LatLng;
    .end local v3    # "request":Lcom/google/maps/DirectionsApiRequest;
    :goto_0
    return-void

    .line 524
    :catch_0
    move-exception v4

    .line 525
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "getNavigationCoordinate"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 526
    invoke-interface {p2, v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getHereNavigationCoordinate(Ljava/lang/String;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V
    .locals 5
    .param p1, "streetAddress"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    .prologue
    .line 533
    :try_start_0
    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->HERE_GEO_APP_ID:Ljava/lang/String;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->HERE_GEO_APP_TOKEN:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 534
    :cond_0
    const-string v0, "no here token in manifest"

    .line 535
    .local v0, "str":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 536
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onFailure(Ljava/lang/Throwable;)V

    .line 588
    .end local v0    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 539
    :cond_1
    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "using here geocoder to get nav position"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 540
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;

    invoke-direct {v3, p0, p1, p2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Ljava/lang/String;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 584
    :catch_0
    move-exception v1

    .line 585
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "getNavigationCoordinate"

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 586
    invoke-interface {p2, v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getRouteDifference(Ljava/util/List;I)Ljava/lang/String;
    .locals 3
    .param p2, "routeIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "routeResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    const/4 v1, 0x0

    .line 820
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 821
    :cond_0
    const-string v0, ""

    .line 826
    :goto_0
    return-object v0

    .line 823
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    .line 824
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_2

    move v0, v1

    :goto_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->findFirstDifference(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    add-int/lit8 p2, p2, 0x1

    move v0, p2

    goto :goto_1

    .line 826
    :cond_3
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    const-string v2, ","

    invoke-static {v0, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private getSelectedRoute(Lcom/google/android/gms/maps/model/LatLng;)Ljava/util/List;
    .locals 8
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/maps/model/LatLng;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/model/Polyline;",
            ">;"
        }
    .end annotation

    .prologue
    .line 323
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/Polyline;>;"
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/Polyline;

    .line 325
    .local v0, "polyline":Lcom/google/android/gms/maps/model/Polyline;
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->m4bToGms(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 326
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/Polyline;->getPoints()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->m4bToGms(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 325
    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/maps/android/PolyUtil;->isLocationOnPath(Lcom/google/android/gms/maps/model/LatLng;Ljava/util/List;ZD)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 328
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331
    .end local v0    # "polyline":Lcom/google/android/gms/maps/model/Polyline;
    :cond_1
    return-object v1
.end method

.method private handleRouteResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 11
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 787
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->isAlive()Z

    move-result v5

    if-nez v5, :cond_0

    .line 816
    :goto_0
    return-void

    .line 790
    :cond_0
    sget-object v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 791
    invoke-direct {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->stopProgress()V

    .line 792
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->event:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .line 793
    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-eq v5, v6, :cond_1

    .line 794
    sget-object v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to get routes."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 795
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0800de

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    aput-object v8, v7, v10

    invoke-virtual {p0, v6, v7}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 798
    :cond_1
    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    .line 799
    .local v4, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_3

    .line 800
    :cond_2
    sget-object v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "No results."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 801
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f080306

    invoke-static {v5, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 804
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 805
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/adapter/RouteDescriptionData;>;"
    const/4 v1, 0x0

    .line 806
    .local v1, "i":I
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 807
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 808
    .local v3, "res":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    iget-object v6, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    invoke-static {v3}, Lcom/navdy/client/app/framework/map/MapUtils;->getGmsLatLongsFromRouteResult(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)Ljava/util/List;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->addPolyline(Ljava/util/List;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 809
    new-instance v6, Lcom/navdy/client/debug/adapter/RouteDescriptionData;

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-direct {p0, v4, v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getRouteDifference(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iget-object v9, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {v6, v7, v8, v9, v3}, Lcom/navdy/client/debug/adapter/RouteDescriptionData;-><init>(Ljava/lang/String;IILcom/navdy/service/library/events/navigation/NavigationRouteResult;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 811
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 812
    .end local v3    # "res":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    :cond_4
    new-instance v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, p0, v0, v6}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Ljava/util/List;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeDescriptionAdapter:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    .line 813
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->listView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeDescriptionAdapter:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 814
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->listView:Landroid/widget/ListView;

    invoke-virtual {v5, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 815
    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iput-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->chousedRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    goto/16 :goto_0
.end method

.method private initMap()V
    .locals 3

    .prologue
    .line 858
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mSearchLocation:Lcom/google/android/gms/maps/model/LatLng;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-static {v1, v2}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 859
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 862
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 864
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 877
    return-void
.end method

.method private resetRouteColor()V
    .locals 4

    .prologue
    .line 315
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/Polyline;

    .line 316
    .local v0, "polyline":Lcom/google/android/gms/maps/model/Polyline;
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/Polyline;->setColor(I)V

    .line 317
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/Polyline;->setZIndex(F)V

    .line 318
    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/Polyline;->setWidth(F)V

    goto :goto_0

    .line 320
    .end local v0    # "polyline":Lcom/google/android/gms/maps/model/Polyline;
    :cond_0
    return-void
.end method

.method private searchAreaFor(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 6
    .param p1, "location"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    const-wide v4, 0x40e86a0000000000L    # 50000.0

    .line 845
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 846
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->m4bToGms(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v1, v4, v5, v2, v3}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    .line 848
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->m4bToGms(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    const-wide v2, 0x4056800000000000L    # 90.0

    invoke-static {v1, v4, v5, v2, v3}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    .line 850
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->m4bToGms(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    const-wide v2, 0x4066800000000000L    # 180.0

    invoke-static {v1, v4, v5, v2, v3}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    .line 852
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->m4bToGms(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    const-wide v2, 0x4070e00000000000L    # 270.0

    invoke-static {v1, v4, v5, v2, v3}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    .line 854
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    return-object v0
.end method

.method private searchRoutesOnHud(Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/maps/model/LatLng;)V
    .locals 15
    .param p1, "place"    # Lcom/google/android/gms/location/places/Place;
    .param p2, "isStreetAddress"    # Z
    .param p3, "navigationCoordinate"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 595
    const v2, 0x7f0800dd

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->startProgress(Ljava/lang/String;)V

    .line 596
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/location/places/Place;->getName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 597
    .local v11, "label":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/location/places/Place;->getAddress()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    .line 599
    .local v14, "streetAddress":Ljava/lang/String;
    const/4 v0, 0x0

    .line 600
    .local v0, "display":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz p3, :cond_1

    .line 601
    move-object/from16 v12, p3

    .line 602
    .local v12, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/location/places/Place;->getLatLng()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v10

    .line 603
    .local v10, "displayPos":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate;

    .end local v0    # "display":Lcom/navdy/service/library/events/location/Coordinate;
    iget-wide v2, v10, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-wide v2, v10, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 604
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v3, 0x0

    .line 605
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    const-wide/16 v4, 0x0

    .line 606
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const/4 v5, 0x0

    .line 607
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const/4 v6, 0x0

    .line 608
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    const-wide/16 v8, 0x0

    .line 609
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const-string v8, "Google"

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 614
    .end local v10    # "displayPos":Lcom/google/android/gms/maps/model/LatLng;
    .restart local v0    # "display":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_0
    new-instance v1, Lcom/navdy/service/library/events/location/Coordinate;

    iget-wide v2, v12, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iget-wide v4, v12, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 615
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    const/4 v4, 0x0

    .line 616
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    const-wide/16 v6, 0x0

    .line 617
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const/4 v6, 0x0

    .line 618
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    const/4 v7, 0x0

    .line 619
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const-wide/16 v8, 0x0

    .line 620
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const-string v9, "Google"

    invoke-direct/range {v1 .. v9}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 623
    .local v1, "destination":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;

    if-eqz v2, :cond_0

    .line 624
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    check-cast v13, Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;

    .line 625
    .local v13, "listener":Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;
    invoke-interface {v13, v11, v1}, Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;->setDestination(Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 628
    .end local v13    # "listener":Lcom/navdy/client/debug/DestinationAddressPickerFragment$DestinationListener;
    :cond_0
    sget-object v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sending route to HUD:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "dest:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " streetAddress:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 630
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    const/4 v5, 0x0

    move-object v3, v1

    move-object v4, v11

    move-object v6, v14

    move-object v7, v0

    invoke-virtual/range {v2 .. v7}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;)Z

    .line 631
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestTimeout:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 632
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    .line 633
    return-void

    .line 612
    .end local v1    # "destination":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v12    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/location/places/Place;->getLatLng()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->gmsToM4b(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v12

    .restart local v12    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    goto/16 :goto_0
.end method

.method private startProgress(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 880
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 881
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 882
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 883
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/navdy/client/debug/GoogleAddressPickerFragment$9;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$9;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 895
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 896
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 897
    return-void
.end method

.method private stopProgress()V
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 901
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    .line 903
    :cond_0
    return-void
.end method

.method private updateMap(Lcom/google/android/gms/location/places/Place;)V
    .locals 4
    .param p1, "place"    # Lcom/google/android/gms/location/places/Place;

    .prologue
    .line 636
    invoke-interface {p1}, Lcom/google/android/gms/location/places/Place;->getLatLng()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/map/MapsForWorkUtils;->gmsToM4b(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 637
    .local v0, "location":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-static {v0, v2}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 638
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 639
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 640
    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    .line 641
    invoke-interface {p1}, Lcom/google/android/gms/location/places/Place;->getName()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 639
    invoke-virtual {v2, v1}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    .line 642
    return-void
.end method


# virtual methods
.method public createLocationRequest()V
    .locals 4

    .prologue
    .line 720
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "creating LocationRequest Object"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 721
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->create()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/16 v1, 0x64

    .line 722
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->setPriority(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x2710

    .line 723
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->setInterval(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    .line 724
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->setFastestInterval(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

    .line 725
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 690
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->setGoogleApiClient(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 691
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "GoogleApiClient connected."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 692
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 695
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1}, Lcom/google/android/gms/location/FusedLocationProviderApi;->getLastLocation(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    .line 698
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    if-nez v0, :cond_1

    .line 702
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "lastLocation was null. Now requesting location update."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 704
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->requestLocationUpdate()V

    .line 709
    :goto_0
    return-void

    .line 706
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->setLocationBoundsForMap()V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 679
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConnectionFailed: ConnectionResult.getErrorCode() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 680
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 679
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->setGoogleApiClient(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 684
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 714
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->setGoogleApiClient(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 715
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "GoogleApiClient connection suspended."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 716
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/navdy/client/debug/common/BaseDebugFragment;->onCreate(Landroid/os/Bundle;)V

    .line 204
    const-string v1, ""

    .line 206
    .local v1, "apiKey":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 207
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 208
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    .line 209
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x80

    .line 208
    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 211
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 212
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v4, "com.google.android.geo.API_KEY"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 218
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "bundle":Landroid/os/Bundle;
    :goto_0
    new-instance v4, Lcom/google/maps/GeoApiContext;

    invoke-direct {v4}, Lcom/google/maps/GeoApiContext;-><init>()V

    invoke-virtual {v4, v1}, Lcom/google/maps/GeoApiContext;->setApiKey(Ljava/lang/String;)Lcom/google/maps/GeoApiContext;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->geoApiContext:Lcom/google/maps/GeoApiContext;

    .line 220
    if-eqz p1, :cond_0

    .line 221
    const-string v4, "1"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    .line 224
    :cond_0
    new-instance v4, Lcom/navdy/client/debug/navigation/HUDNavigationManager;

    invoke-direct {v4}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;-><init>()V

    iput-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    .line 225
    return-void

    .line 213
    :catch_0
    move-exception v3

    .line 214
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to load meta-data, NameNotFound: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 215
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v3

    .line 216
    .local v3, "e":Ljava/lang/NullPointerException;
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to load meta-data, NullPointer: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 243
    const v5, 0x7f03008c

    const/4 v6, 0x0

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 244
    .local v3, "rootView":Landroid/view/View;
    invoke-static {p0, v3}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 245
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 246
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar;->hide()V

    .line 248
    :cond_0
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 250
    .local v0, "context":Landroid/content/Context;
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAutocompleteView:Landroid/widget/AutoCompleteTextView;

    iget-object v6, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAutocompleteClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 252
    new-instance v5, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    const v6, 0x1090003

    invoke-direct {v5, v0, v6, v7, v7}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;-><init>(Landroid/content/Context;ILcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V

    iput-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    .line 258
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v5, :cond_1

    .line 259
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 260
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 263
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->rebuildGoogleApiClient()V

    .line 264
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    iget-object v6, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v5, v6}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->setGoogleApiClient(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    .line 265
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAutocompleteView:Landroid/widget/AutoCompleteTextView;

    iget-object v6, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 267
    invoke-static {}, Lcom/google/android/gms/maps/MapFragment;->newInstance()Lcom/google/android/gms/maps/MapFragment;

    move-result-object v2

    .line 268
    .local v2, "mMapFragment":Lcom/google/android/gms/maps/MapFragment;
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 269
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 270
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    const v5, 0x7f100250

    invoke-virtual {v4, v5, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 271
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 273
    new-instance v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$2;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/MapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 283
    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->listView:Landroid/widget/ListView;

    new-instance v6, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;

    invoke-direct {v6, p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 297
    return-object v3
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->stopProgress()V

    .line 311
    invoke-super {p0}, Lcom/navdy/client/debug/common/BaseDebugFragment;->onDestroy()V

    .line 312
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 750
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 751
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Location changed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 752
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    .line 753
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->setLocationBoundsForMap()V

    .line 754
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->stopLocationUpdates()V

    .line 756
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->stopLocationUpdates()V

    .line 761
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 762
    invoke-super {p0}, Lcom/navdy/client/debug/common/BaseDebugFragment;->onPause()V

    .line 763
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 767
    invoke-super {p0}, Lcom/navdy/client/debug/common/BaseDebugFragment;->onResume()V

    .line 768
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->requestLocationUpdate()V

    .line 771
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 772
    return-void
.end method

.method public onRoutingResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    .line 233
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->handleRouteResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V

    .line 238
    :goto_0
    return-void

    .line 235
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no current route request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dest:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeRequestSendToHud:Z

    if-eqz v0, :cond_0

    .line 303
    const-string v0, "1"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 305
    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/client/debug/common/BaseDebugFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 306
    return-void
.end method

.method protected declared-synchronized rebuildGoogleApiClient()V
    .locals 2

    .prologue
    .line 662
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    .line 663
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/Places;->GEO_DATA_API:Lcom/google/android/gms/common/api/Api;

    .line 664
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/LocationServices;->API:Lcom/google/android/gms/common/api/Api;

    .line 665
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 666
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 667
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668
    monitor-exit p0

    return-void

    .line 662
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public requestLocationUpdate()V
    .locals 3

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->createLocationRequest()V

    .line 730
    sget-object v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "calling requestLocationUpdates from FusedLocationApi"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 731
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 734
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/gms/location/FusedLocationProviderApi;->requestLocationUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/LocationListener;)Lcom/google/android/gms/common/api/PendingResult;

    .line 736
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->requestedLocationUpdates:Z

    .line 737
    return-void
.end method

.method public setLocationBoundsForMap()V
    .locals 6

    .prologue
    .line 775
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mSearchLocation:Lcom/google/android/gms/maps/model/LatLng;

    .line 776
    sget-object v1, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting search locus to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    .line 777
    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->lastLocation:Landroid/location/Location;

    .line 778
    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 776
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 779
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mSearchLocation:Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->searchAreaFor(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    .line 780
    .local v0, "searchArea":Lcom/google/android/gms/maps/model/LatLngBounds;
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAdapter:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-virtual {v1, v0}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->setBounds(Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 781
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v1, :cond_0

    .line 782
    invoke-direct {p0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->initMap()V

    .line 784
    :cond_0
    return-void
.end method

.method protected showRouteDetails(I)V
    .locals 7
    .param p1, "selectedRouteIndex"    # I

    .prologue
    const/4 v6, 0x0

    .line 335
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->event:Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    iget-object v4, v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iput-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->chousedRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 336
    const/4 v1, 0x0

    .line 337
    .local v1, "i":I
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/Polyline;

    .line 338
    .local v2, "polyline":Lcom/google/android/gms/maps/model/Polyline;
    if-eq v1, p1, :cond_0

    .line 339
    invoke-virtual {v2, v6}, Lcom/google/android/gms/maps/model/Polyline;->setVisible(Z)V

    .line 341
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 342
    goto :goto_0

    .line 343
    .end local v2    # "polyline":Lcom/google/android/gms/maps/model/Polyline;
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 344
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeDescriptionAdapter:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->getData()Ljava/util/List;

    move-result-object v0

    .line 345
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/adapter/RouteDescriptionData;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/debug/adapter/RouteDescriptionData;

    .line 346
    .local v3, "routeDescriptionData":Lcom/navdy/client/debug/adapter/RouteDescriptionData;
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/adapter/RouteDescriptionData;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 347
    .restart local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/debug/adapter/RouteDescriptionData;>;"
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->routeDescriptionAdapter:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    invoke-virtual {v4, v0}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->setData(Ljava/util/List;)V

    .line 349
    return-void
.end method

.method protected stopLocationUpdates()V
    .locals 2

    .prologue
    .line 740
    iget-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->requestedLocationUpdates:Z

    if-eqz v0, :cond_0

    .line 741
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/location/FusedLocationProviderApi;->removeLocationUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationListener;)Lcom/google/android/gms/common/api/PendingResult;

    .line 742
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->requestedLocationUpdates:Z

    .line 744
    :cond_0
    return-void
.end method
