.class public Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;
.super Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;
.source "AppSetupActivity.java"


# instance fields
.field private bottomCard:Landroid/support/v4/view/ViewPager;

.field private hasNeverShownBtSuccess:Z

.field private hasOpenedBluetoothPairingDialog:Z

.field private hiddenButtonClickCount:I

.field private hud:Landroid/widget/ImageView;

.field private setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

.field private showingFail:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;-><init>()V

    .line 59
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasNeverShownBtSuccess:Z

    .line 61
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasOpenedBluetoothPairingDialog:Z

    .line 598
    iput v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hiddenButtonClickCount:I

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->userPhoto:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->customerPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showFailureForCurrentScreen()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->customerPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->email:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showProfileStuff()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hideProfileStuff()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasNeverShownBtSuccess:Z

    return v0
.end method

.method static synthetic access$902(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasNeverShownBtSuccess:Z

    return p1
.end method

.method private changeProfileStuffVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 477
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setVisibility(I)V

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->photoHint:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 481
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->photoHint:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 483
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hud:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 484
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hud:Landroid/widget/ImageView;

    const v1, 0x7f020209

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 486
    :cond_2
    return-void
.end method

.method public static goToAppSetup(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 66
    return-void
.end method

.method private goToHomeScreen()V
    .locals 13

    .prologue
    .line 547
    const-string v11, "First_Launch_Completed"

    invoke-static {v11}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 549
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 550
    .local v6, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v11, "vehicle-year"

    const-string v12, ""

    invoke-interface {v6, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 551
    .local v4, "carYear":Ljava/lang/String;
    const-string v11, "vehicle-make"

    const-string v12, ""

    invoke-interface {v6, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552
    .local v2, "carMake":Ljava/lang/String;
    const-string v11, "vehicle-model"

    const-string v12, ""

    invoke-interface {v6, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 554
    .local v3, "carModel":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v10

    .line 555
    .local v10, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v11, "box"

    const-string v12, "Old_Box"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 558
    .local v1, "box":Ljava/lang/String;
    const-string v11, "nb_config"

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 562
    .local v9, "nbConfig":I
    invoke-static {v10}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType(Landroid/content/SharedPreferences;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v5

    .line 563
    .local v5, "currentMountType":Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 565
    .local v8, "mount":Ljava/lang/String;
    new-instance v0, Ljava/util/HashMap;

    const/4 v11, 0x5

    invoke-direct {v0, v11}, Ljava/util/HashMap;-><init>(I)V

    .line 566
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v11, "Car_Year"

    invoke-virtual {v0, v11, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    const-string v11, "Car_Make"

    invoke-virtual {v0, v11, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    const-string v11, "Car_Model"

    invoke-virtual {v0, v11, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    const-string v11, "Selected_Mount"

    invoke-virtual {v0, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    const-string v11, "Selected_Box"

    invoke-virtual {v0, v11, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    const-string v11, "Nb_Configurations"

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    const-string v11, "Configuration_At_Completion"

    invoke-static {v11, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 575
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->forceSendFavoriteContactsToHud()V

    .line 577
    new-instance v7, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-class v12, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {v7, v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 578
    .local v7, "i":Landroid/content/Intent;
    const v11, 0x10008000

    invoke-virtual {v7, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 579
    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->startActivity(Landroid/content/Intent;)V

    .line 580
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->finish()V

    .line 581
    return-void
.end method

.method private handleConnectionChange(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    .line 279
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v1, :cond_0

    .line 280
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 282
    :cond_0
    if-nez p1, :cond_2

    .line 283
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 291
    :cond_1
    :goto_0
    return-void

    .line 285
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    .line 286
    .local v0, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v1, v2, :cond_1

    .line 288
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0
.end method

.method private hideProfileStuff()V
    .locals 1

    .prologue
    .line 473
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->changeProfileStuffVisibility(I)V

    .line 474
    return-void
.end method

.method private moveToNextScreen()V
    .locals 5

    .prologue
    .line 535
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 536
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 537
    .local v0, "currentIndex":I
    add-int/lit8 v2, v0, 0x1

    .line 538
    .local v2, "nextIndex":I
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v1

    .line 539
    .local v1, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v1, :cond_1

    iget-object v3, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v4, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v3, v4, :cond_1

    .line 540
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->goToHomeScreen()V

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 542
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method private setUpViewPager()V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 160
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/navdy/client/app/framework/util/ImageCache;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    .line 162
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 203
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 204
    return-void
.end method

.method private showFailureForCurrentScreen()V
    .locals 4

    .prologue
    .line 516
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "showFailureForCurrentScreen"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 517
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_0

    .line 518
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 519
    .local v0, "currentPosition":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    .line 520
    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    .line 521
    .local v1, "fragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    .line 522
    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->showFail()V

    .line 523
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->updateIllustration(I)V

    .line 525
    .end local v0    # "currentPosition":I
    .end local v1    # "fragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;
    :cond_0
    return-void
.end method

.method private showProfileStuff()V
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->changeProfileStuffVisibility(I)V

    .line 470
    return-void
.end method

.method public static userHasFinishedAppSetup()Z
    .locals 3

    .prologue
    .line 594
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 595
    .local v0, "settingsPrefs":Landroid/content/SharedPreferences;
    const-string v1, "finished_app_setup"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public appInstanceDeviceConnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 1
    .param p1, "deviceConnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 270
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->handleConnectionChange(Z)V

    .line 271
    return-void
.end method

.method public appInstanceDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 1
    .param p1, "deviceDisconnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 275
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->handleConnectionChange(Z)V

    .line 276
    return-void
.end method

.method public getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 529
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 530
    .local v0, "currentIndex":I
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v1, :cond_1

    .line 531
    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getScreen(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v1

    :goto_1
    return-object v1

    .line 529
    .end local v0    # "currentIndex":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 531
    .restart local v0    # "currentIndex":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isDeviceConnected(Landroid/content/Context;)Z
    .locals 2
    .param p1, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 294
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 295
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v1

    return v1
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 585
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 586
    .local v0, "nextIndex":I
    if-gez v0, :cond_0

    .line 587
    invoke-super {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onBackPressed()V

    .line 591
    :goto_0
    return-void

    .line 589
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public onButtonClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    .line 337
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v1

    .line 339
    .local v1, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$3;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$3;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    .line 349
    .local v3, "doThingsThatRequirePermission":Ljava/lang/Runnable;
    new-instance v4, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$4;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$4;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    .line 355
    .local v4, "handlePermissionDenial":Ljava/lang/Runnable;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 356
    .local v0, "applicationContext":Landroid/content/Context;
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$8;->$SwitchMap$com$navdy$client$app$ui$firstlaunch$AppSetupScreen$ScreenType:[I

    iget-object v7, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    invoke-virtual {v7}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 456
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0

    .line 358
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v2

    .line 359
    .local v2, "deviceIsConnected":Z
    if-eqz v2, :cond_2

    .line 360
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0

    .line 362
    :cond_2
    iput-boolean v8, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasNeverShownBtSuccess:Z

    .line 363
    iput-boolean v8, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasOpenedBluetoothPairingDialog:Z

    .line 364
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->openBtConnectionDialog()V

    goto :goto_0

    .line 368
    .end local v2    # "deviceIsConnected":Z
    :pswitch_1
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveNotificationPermission()Z

    move-result v6

    if-nez v6, :cond_3

    .line 369
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 371
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0

    .line 375
    :pswitch_2
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_4

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_5

    .line 376
    :cond_4
    new-instance v6, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$5;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$5;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    invoke-virtual {p0, v6, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 387
    :cond_5
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0

    .line 391
    :pswitch_3
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_6

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_7

    .line 392
    :cond_6
    new-instance v6, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$6;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$6;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    invoke-virtual {p0, v6, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestMicrophonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 399
    :cond_7
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0

    .line 403
    :pswitch_4
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_8

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_9

    .line 404
    :cond_8
    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestContactsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 406
    :cond_9
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto :goto_0

    .line 410
    :pswitch_5
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_a

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_b

    .line 411
    :cond_a
    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestSmsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 413
    :cond_b
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto/16 :goto_0

    .line 417
    :pswitch_6
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_c

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_d

    .line 418
    :cond_c
    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestPhonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 420
    :cond_d
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto/16 :goto_0

    .line 424
    :pswitch_7
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_e

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_f

    .line 425
    :cond_e
    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestCalendarPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 427
    :cond_f
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto/16 :goto_0

    .line 431
    :pswitch_8
    iget-boolean v6, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v6, :cond_10

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-nez v6, :cond_11

    .line 432
    :cond_10
    new-instance v6, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$7;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$7;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    invoke-virtual {p0, v6, v4}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 447
    :cond_11
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto/16 :goto_0

    .line 451
    :pswitch_9
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 452
    .local v5, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "finished_app_setup"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 453
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    goto/16 :goto_0

    .line 356
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public onContactSupportClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 311
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 312
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->startActivity(Landroid/content/Intent;)V

    .line 313
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const v0, 0x7f030064

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setContentView(I)V

    .line 75
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f0800be

    .line 76
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 80
    const v0, 0x7f100168

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hud:Landroid/widget/ImageView;

    .line 81
    const v0, 0x7f10016b

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    .line 83
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hud:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->bottomCard:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Ui element missing !"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 156
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getUserProfilePhoto()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->userPhoto:Landroid/graphics/Bitmap;

    .line 90
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->customerPrefs:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fname"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->name:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v1, "email"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->email:Ljava/lang/String;

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidName(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->email:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidEmail(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 100
    :cond_3
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->requestContactsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 155
    :cond_4
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setUpViewPager()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 255
    const/4 v0, 0x1

    return v0
.end method

.method public onDescriptionClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 316
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    if-eqz v1, :cond_0

    .line 317
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    .line 318
    .local v0, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v1, v2, :cond_1

    .line 320
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->onHelpCenterClick(Landroid/view/View;)V

    .line 327
    .end local v0    # "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    :cond_0
    :goto_0
    return-void

    .line 321
    .restart local v0    # "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-eq v1, v2, :cond_0

    .line 324
    :cond_2
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->goToSystemSettingsAppInfoForOurApp(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->clearCache()V

    .line 250
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onDestroy()V

    .line 251
    return-void
.end method

.method public onHelpCenterClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 330
    invoke-static {}, Lcom/navdy/client/app/framework/util/ZendeskJWT;->getZendeskUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->openBrowserFor(Landroid/net/Uri;)V

    .line 331
    return-void
.end method

.method public onHiddenButtonClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    .line 601
    .local v0, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v1, v2, :cond_0

    .line 603
    iget v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hiddenButtonClickCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hiddenButtonClickCount:I

    .line 604
    iget v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hiddenButtonClickCount:I

    const/16 v2, 0xa

    if-lt v1, v2, :cond_0

    .line 605
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hiddenButtonClickCount:I

    .line 606
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->pretendBtConnected()V

    .line 607
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    .line 610
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 208
    invoke-super {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onPause()V

    .line 209
    return-void
.end method

.method public onPrivacyPolicyClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 305
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    .local v0, "browserIntent":Landroid/content/Intent;
    const-string v1, "type"

    const-string v2, "Privacy"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 307
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->startActivity(Landroid/content/Intent;)V

    .line 308
    return-void
.end method

.method public onProfileInfoSet()V
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->onButtonClick(Landroid/view/View;)V

    .line 466
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "grantResults"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 260
    invoke-super {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 261
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 264
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 213
    invoke-super {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onResume()V

    .line 215
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->weHaveCarInfo()Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->startActivity(Landroid/content/Intent;)V

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 222
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getCurrentScreen()Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    .line 223
    .local v0, "currentScreen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v0, :cond_2

    .line 224
    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v1, v2, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    .line 233
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v1, v2, :cond_2

    .line 234
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveNotificationPermission()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 235
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->moveToNextScreen()V

    .line 242
    :cond_2
    :goto_1
    const-string v1, "First_Launch"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 243
    return-void

    .line 228
    :cond_3
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hasOpenedBluetoothPairingDialog:Z

    if-eqz v1, :cond_1

    .line 229
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showFailureForCurrentScreen()V

    goto :goto_0

    .line 237
    :cond_4
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showFailureForCurrentScreen()V

    goto :goto_1
.end method

.method public updateIllustration(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 510
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->hud:Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 512
    .local v0, "hudRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/widget/ImageView;>;"
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->showingFail:Z

    invoke-virtual {v1, v0, p1, v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->updateIllustration(Ljava/lang/ref/WeakReference;IZ)V

    .line 513
    return-void
.end method

.method protected validateAndSaveEmail(Z)Z
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 499
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateAndSaveEmail(Z)Z

    move-result p1

    .line 500
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 503
    :cond_0
    return p1
.end method

.method protected validateAndSaveName(Z)Z
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 490
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateAndSaveName(Z)Z

    move-result p1

    .line 491
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->setupPagerAdapter:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 494
    :cond_0
    return p1
.end method
