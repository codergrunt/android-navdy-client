.class public Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "HomescreenActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;


# static fields
.field public static final EASTER_EGG_HIDE_ALL_TIPS:I = 0xf

.field public static final EASTER_EGG_SHOW_ALL_TIPS:I = 0x14

.field public static final EASTER_EGG_SHOW_TRIP_TAB:I = 0xa

.field public static final EXTRA_DESTINATION:Ljava/lang/String; = "extra_destination"

.field private static final EXTRA_HOMESCREEN_EXPLICITLY_ROUTING:Ljava/lang/String; = "extra_homescreen_explicitly_routing"

.field private static final GOOGLE_MAPS_HOST:Ljava/lang/String; = "maps.google.com"

.field private static final GOOGLE_NAVIGATION_SCHEME:Ljava/lang/String; = "google.navigation"

.field private static final SUCCESS_BANNER_DISMISS_DELAY:I = 0x7d0

.field private static final VERSION_CHECK_INTERVAL:J = 0x7530L


# instance fields
.field private addFavoriteLabel:Landroid/widget/TextView;

.field private addHome:Landroid/support/design/widget/FloatingActionButton;

.field private addHomeLabel:Landroid/widget/TextView;

.field private addWork:Landroid/support/design/widget/FloatingActionButton;

.field private addWorkLabel:Landroid/widget/TextView;

.field private fab:Landroid/support/design/widget/FloatingActionButton;

.field private film:Landroid/view/View;

.field private highlightGlanceSwitchOnResume:Z

.field private isFabShowing:Z

.field private lastCheck:J

.field private lastExternalIntentHandled:Landroid/content/Intent;

.field private mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field private mode:Landroid/support/v7/view/ActionMode;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field private navigationView:Landroid/support/design/widget/NavigationView;

.field pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

.field private reloadProfile:Z

.field private tabLayout:Landroid/support/design/widget/TabLayout;

.field private toolbar:Landroid/support/v7/widget/Toolbar;

.field private trackerEvent:Ljava/lang/String;

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 138
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastExternalIntentHandled:Landroid/content/Intent;

    .line 152
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFabShowing:Z

    .line 162
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->reloadProfile:Z

    .line 166
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->highlightGlanceSwitchOnResume:Z

    .line 167
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastCheck:J

    .line 169
    const-string v0, "Navigate_Using_Search_Results"

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->trackerEvent:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/client/app/framework/AppInstance;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showConnectivityBanner()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/design/widget/TabLayout;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/design/widget/NavigationView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/support/design/widget/NavigationView;)Landroid/support/design/widget/NavigationView;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .param p1, "x1"    # Landroid/support/design/widget/NavigationView;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateMenuVisibility(ZI)V

    return-void
.end method

.method static synthetic access$1300()V
    .locals 0

    .prologue
    .line 126
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showAllTips()V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideOtherFabs()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToDestination(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pickFabAction()V

    return-void
.end method

.method static synthetic access$2000(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->closeDrawer()V

    return-void
.end method

.method static synthetic access$402(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->reloadProfile:Z

    return p1
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;)V
    .locals 1
    .param p1, "searchType"    # Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .prologue
    .line 1074
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;Z)V

    .line 1075
    return-void
.end method

.method private callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;Z)V
    .locals 0
    .param p1, "searchType"    # Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;
    .param p2, "searchMic"    # Z

    .prologue
    .line 1078
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideOtherFabs()V

    .line 1079
    invoke-static {p1, p2, p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->startSearchActivityFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;ZLandroid/app/Activity;)V

    .line 1080
    return-void
.end method

.method private checkForUpdates(Lnet/hockeyapp/android/UpdateManagerListener;)V
    .locals 10
    .param p1, "listener"    # Lnet/hockeyapp/android/UpdateManagerListener;

    .prologue
    .line 1583
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 1584
    .local v4, "now":J
    iget-wide v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastCheck:J

    sub-long v6, v4, v6

    const-wide/16 v8, 0x7530

    cmp-long v3, v6, v8

    if-lez v3, :cond_0

    .line 1585
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Checking Hockey app for updates."

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1586
    iput-wide v4, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastCheck:J

    .line 1588
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1589
    .local v1, "context":Landroid/content/Context;
    const v3, 0x7f08055a

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1590
    .local v2, "credentials":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1591
    .local v0, "HOCKEY_APP_ID":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {p0, v0, p1, v3}, Lnet/hockeyapp/android/UpdateManager;->register(Landroid/app/Activity;Ljava/lang/String;Lnet/hockeyapp/android/UpdateManagerListener;Z)V

    .line 1598
    .end local v0    # "HOCKEY_APP_ID":Ljava/lang/String;
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "credentials":Ljava/lang/String;
    :goto_0
    return-void

    .line 1593
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Hasn\'t been long enough to be checking Hockey app for updates. now = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " lastCheck = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastCheck:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " now - lastCheck = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastCheck:J

    sub-long v8, v4, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private closeDrawer()V
    .locals 2

    .prologue
    .line 493
    const v1, 0x7f1002b2

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 494
    .local v0, "drawer":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v0, :cond_0

    .line 495
    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 497
    :cond_0
    return-void
.end method

.method private favoritesFragmentFabAction()V
    .locals 2

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "favoritesFragmentFabAction"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1034
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFabShowing:Z

    if-nez v0, :cond_0

    .line 1035
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isHomeSet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1036
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isWorkSet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1037
    :cond_0
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->FAVORITE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;)V

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showOtherFabs()V

    goto :goto_0
.end method

.method public static getAppVersionString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 500
    const/4 v2, 0x0

    .line 501
    .local v2, "pInfo":Landroid/content/pm/PackageInfo;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 503
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 507
    :goto_0
    if-eqz v2, :cond_0

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 508
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 509
    .local v3, "versionNameParts":[Ljava/lang/String;
    aget-object v4, v3, v7

    .line 511
    .end local v3    # "versionNameParts":[Ljava/lang/String;
    :goto_1
    return-object v4

    .line 504
    :catch_0
    move-exception v1

    .line 505
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 511
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const v4, 0x7f0804da

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method private static getDisplaySerialString(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 536
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 537
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    .line 538
    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 539
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    .line 540
    iget-object v2, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    .line 543
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :goto_0
    return-object v2

    :cond_0
    const v2, 0x7f0804da

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static getDisplayVersionString(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 515
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v3

    .line 516
    .local v3, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v3, :cond_1

    .line 517
    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v1

    .line 519
    .local v1, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v1, :cond_1

    iget-object v4, v1, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 520
    iget-object v4, v1, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 522
    .local v0, "clientVersionParts":[Ljava/lang/String;
    array-length v4, v0

    if-lez v4, :cond_1

    .line 523
    const/4 v4, 0x0

    aget-object v2, v0, v4

    .line 532
    .end local v0    # "clientVersionParts":[Ljava/lang/String;
    .end local v1    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 527
    :cond_1
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getLastDeviceVersionText()Ljava/lang/String;

    move-result-object v2

    .line 529
    .local v2, "lastDeviceVersionText":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 532
    const v4, 0x7f0804da

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private handleIntent(Landroid/content/Intent;)Z
    .locals 11
    .param p1, "callingIntent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1168
    :try_start_0
    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1170
    .local v2, "isActionView":Z
    if-eqz v2, :cond_0

    const-string v8, "extra_destination"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1172
    new-instance v8, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;

    invoke-direct {v8, p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/content/Intent;)V

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Void;

    .line 1185
    invoke-virtual {v8, v9}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$16;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1227
    .end local v2    # "isActionView":Z
    :goto_0
    return v6

    .line 1189
    .restart local v2    # "isActionView":Z
    :cond_0
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "handleIntent: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1190
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-static {p1, v8}, Lcom/navdy/client/app/framework/util/SystemUtils;->logIntentExtras(Landroid/content/Intent;Lcom/navdy/service/library/log/Logger;)V

    .line 1192
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v3

    .line 1193
    .local v3, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    const-string v8, "Third_Party_Intent"

    invoke-virtual {v3, v8}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setSourceValue(Ljava/lang/String;)V

    .line 1194
    const-string v8, "Intent"

    invoke-virtual {v3, v8}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 1196
    if-eqz v2, :cond_4

    .line 1197
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "ACTION_VIEW Intent found"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1199
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 1200
    .local v5, "uri":Landroid/net/Uri;
    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 1201
    .local v1, "host":Ljava/lang/String;
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "uri: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1202
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "uri host: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1203
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "uri scheme: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1204
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "uri schemeSpecificPart: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1205
    const-string v8, "maps.google.com"

    invoke-static {v1, v8}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1206
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "google.navigation"

    invoke-static {v8, v9}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1207
    :cond_1
    invoke-direct {p0, v5}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToUri(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1224
    .end local v1    # "host":Ljava/lang/String;
    .end local v2    # "isActionView":Z
    .end local v3    # "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    .end local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 1225
    .local v0, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    move v6, v7

    .line 1227
    goto/16 :goto_0

    .line 1209
    .restart local v1    # "host":Ljava/lang/String;
    .restart local v2    # "isActionView":Z
    .restart local v3    # "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_3
    :try_start_1
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string v9, "geo"

    invoke-static {v8, v9}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1210
    invoke-direct {p0, v5}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToGeoUri(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1213
    .end local v1    # "host":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_4
    const-string v8, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v8

    const-string v9, "text/plain"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1214
    const-string v8, "android.intent.extra.TEXT"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1215
    .local v4, "sharedText":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1216
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "ACTION_SEND Intent had bad text"

    invoke-virtual {v6, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    move v6, v7

    .line 1217
    goto/16 :goto_0

    .line 1219
    :cond_5
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_SEND Intent found: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1221
    invoke-direct {p0, v4, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->searchOrRouteToSharedText(Ljava/lang/String;Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public static hideAllTips()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 868
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 869
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 870
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_watched_the_demo"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 871
    const-string v2, "user_enabled_glances_once_before"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 872
    const-string v2, "user_tried_gestures_once_before"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 873
    const-string v2, "user_already_saw_microphone_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 874
    const-string v2, "user_already_saw_google_now_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 875
    const-string v2, "user_already_saw_gesture_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 876
    const-string v2, "user_already_saw_add_home_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 877
    const-string v2, "user_already_saw_add_work_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 878
    const-string v2, "user_already_saw_music_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 879
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 880
    return-void
.end method

.method private hideOtherFabs()V
    .locals 1

    .prologue
    .line 1088
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFabShowing:Z

    .line 1089
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setOtherFabsVisibility(I)V

    .line 1090
    return-void
.end method

.method private openStartTab()V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 1650
    const v1, 0x7f1002b2

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 1652
    .local v0, "drawer":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1653
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 1656
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1657
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1658
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1659
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideOtherFabs()V

    .line 1661
    :cond_1
    return-void
.end method

.method private parseLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1539
    const-string v3, "(-?[0-9]+\\.?[0-9]*), *(-?[0-9]+\\.?[0-9]*)"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1542
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1543
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 1545
    .local v1, "latLngStr":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 1547
    move-object v0, v1

    .line 1559
    .end local v1    # "latLngStr":Ljava/lang/String;
    .local v0, "label":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1551
    .end local v0    # "label":Ljava/lang/String;
    .restart local v1    # "latLngStr":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "("

    const-string v5, ""

    .line 1552
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ")"

    const-string v5, ""

    .line 1553
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 1554
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "label":Ljava/lang/String;
    goto :goto_0

    .line 1557
    .end local v0    # "label":Ljava/lang/String;
    .end local v1    # "latLngStr":Ljava/lang/String;
    :cond_1
    move-object v0, p1

    .restart local v0    # "label":Ljava/lang/String;
    goto :goto_0
.end method

.method private parseParamValue(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "param"    # Ljava/lang/String;

    .prologue
    .line 1564
    new-instance v2, Landroid/net/UrlQuerySanitizer;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/net/UrlQuerySanitizer;-><init>(Ljava/lang/String;)V

    .line 1565
    .local v2, "querySanitizer":Landroid/net/UrlQuerySanitizer;
    invoke-static {}, Landroid/net/UrlQuerySanitizer;->getAllButNulAndAngleBracketsLegal()Landroid/net/UrlQuerySanitizer$ValueSanitizer;

    move-result-object v4

    invoke-virtual {v2, p2, v4}, Landroid/net/UrlQuerySanitizer;->registerParameter(Ljava/lang/String;Landroid/net/UrlQuerySanitizer$ValueSanitizer;)V

    .line 1566
    const/4 v1, 0x0

    .line 1568
    .local v1, "q":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1569
    .local v3, "value":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 1570
    const-string v4, "_"

    const-string v5, " "

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1571
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1576
    .end local v3    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 1573
    :catch_0
    move-exception v0

    .line 1574
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t decode parameter \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' in uri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private pickFabAction()V
    .locals 4

    .prologue
    .line 1020
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1021
    .local v0, "position":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pickFabAction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1022
    packed-switch v0, :pswitch_data_0

    .line 1027
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported viewPager position for fab: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1030
    :goto_0
    return-void

    .line 1024
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->favoritesFragmentFabAction()V

    goto :goto_0

    .line 1022
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private routeToAddressOrCoords(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "address"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1466
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "routeToAddressOrCoords, address: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "lat/lng: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1468
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1469
    const v1, 0x7f080116

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 1526
    :goto_0
    return-void

    .line 1473
    :cond_0
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 1475
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1476
    iput-object p1, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 1479
    :cond_1
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1480
    iput-object p2, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 1483
    :cond_2
    invoke-static {p3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1484
    iget-wide v2, p3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iput-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 1485
    iget-wide v2, p3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iput-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 1489
    :cond_3
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p3}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1491
    invoke-virtual {v0, p3}, Lcom/navdy/client/app/framework/models/Destination;->setCoordsToSame(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 1492
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$19;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$19;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-static {p3, v0, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->doReverseGeocodingFor(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1507
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showProgressDialog()V

    .line 1509
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_0
.end method

.method private routeToDestination(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1529
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$21;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$21;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showRequestNewRouteDialog(Ljava/lang/Runnable;)V

    .line 1535
    return-void
.end method

.method private routeToGeoUri(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1352
    sget-object v5, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "routeToGeoUri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1354
    if-nez p1, :cond_1

    .line 1355
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to route to a null URI"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1383
    :cond_0
    :goto_0
    return-void

    .line 1359
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1360
    .local v3, "partsString":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1364
    const-string v5, "\\?"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1365
    .local v2, "parts":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 1366
    .local v0, "address":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1367
    .local v1, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    array-length v5, v2

    if-lez v5, :cond_2

    .line 1368
    const/4 v5, 0x0

    aget-object v5, v2, v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/map/MapUtils;->parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 1371
    :cond_2
    const-string v5, "q"

    invoke-direct {p0, p1, v5}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->parseParamValue(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1373
    .local v4, "q":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1374
    if-nez v1, :cond_3

    .line 1375
    invoke-static {v4}, Lcom/navdy/client/app/framework/map/MapUtils;->parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 1377
    :cond_3
    invoke-direct {p0, v4}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->parseLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1378
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1379
    move-object v0, v4

    .line 1382
    :cond_4
    const/4 v5, 0x0

    invoke-direct {p0, v5, v0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToAddressOrCoords(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method

.method private routeToUri(Landroid/net/Uri;)V
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1252
    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "routeToUri: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1259
    const/4 v0, 0x0

    .line 1260
    .local v0, "address":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1262
    .local v4, "latLngStr":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1263
    .local v1, "daddr":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1264
    .local v8, "query":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->isHierarchical()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1266
    const-string v10, "daddr"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1267
    const-string v10, "q"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1270
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v9

    .line 1272
    .local v9, "schemeSpecificPart":Ljava/lang/String;
    if-eqz v1, :cond_6

    .line 1273
    const-string v10, " +?@"

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1275
    .local v7, "parts":[Ljava/lang/String;
    array-length v10, v7

    const/4 v11, 0x1

    if-le v10, v11, :cond_4

    .line 1276
    const/4 v10, 0x0

    aget-object v0, v7, v10

    .line 1277
    const/4 v10, 0x1

    aget-object v4, v7, v10

    .line 1326
    .end local v7    # "parts":[Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/net/Uri;->isHierarchical()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1327
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1329
    const-string v10, "q"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1331
    :cond_2
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1332
    const-string v10, "ll"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1335
    :cond_3
    const/4 v10, 0x0

    invoke-static {v4}, Lcom/navdy/client/app/framework/map/MapUtils;->parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v11

    invoke-direct {p0, v10, v0, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToAddressOrCoords(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 1336
    :goto_1
    return-void

    .line 1278
    .restart local v7    # "parts":[Ljava/lang/String;
    :cond_4
    const/4 v10, 0x0

    aget-object v10, v7, v10

    const-string v11, "(-?[0-9]+\\.?[0-9]*), *(-?[0-9]+\\.?[0-9]*)"

    invoke-virtual {v10, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1279
    const/4 v10, 0x0

    aget-object v4, v7, v10

    goto :goto_0

    .line 1281
    :cond_5
    const/4 v10, 0x0

    aget-object v0, v7, v10

    goto :goto_0

    .line 1283
    .end local v7    # "parts":[Ljava/lang/String;
    :cond_6
    if-eqz v9, :cond_9

    const-string v10, "q="

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1288
    const/4 v10, 0x2

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 1289
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1291
    :try_start_0
    const-string v10, "UTF-8"

    invoke-static {v8, v10}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 1297
    :goto_2
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1298
    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToFirstResultFor(Ljava/lang/String;)V

    goto :goto_1

    .line 1292
    :catch_0
    move-exception v2

    .line 1293
    .local v2, "e":Ljava/io/UnsupportedEncodingException;
    const-string v10, "%2C"

    const-string v11, ","

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1294
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_2

    .line 1301
    .end local v2    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_7
    const/4 v0, 0x0

    .line 1302
    invoke-static {v8}, Lcom/navdy/client/app/framework/map/MapUtils;->parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 1303
    .local v3, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    if-nez v3, :cond_8

    .line 1304
    move-object v0, v8

    .line 1306
    :cond_8
    const/4 v10, 0x0

    invoke-direct {p0, v10, v0, v3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToAddressOrCoords(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_1

    .line 1309
    .end local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_9
    if-eqz v8, :cond_1

    .line 1310
    const-string v10, "(-?[0-9]+\\.?[0-9]*), *(-?[0-9]+\\.?[0-9]*)"

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 1312
    .local v5, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1314
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v4

    .line 1316
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->end()I

    move-result v10

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const-string v11, "("

    const-string v12, ""

    .line 1317
    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, ")"

    const-string v12, ""

    .line 1318
    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 1319
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1321
    .local v6, "name":Ljava/lang/String;
    const/4 v10, 0x0

    invoke-static {v4}, Lcom/navdy/client/app/framework/map/MapUtils;->parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v11

    invoke-direct {p0, v6, v10, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToAddressOrCoords(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V

    goto/16 :goto_1
.end method

.method private saveDestinationToFavorites(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 685
    const/4 v3, -0x1

    if-eq p2, v3, :cond_0

    .line 751
    :goto_0
    return-void

    .line 689
    :cond_0
    const-string v3, "search_result"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 690
    .local v0, "d":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveDestinationToFavorites: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 692
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showProgressDialog()V

    .line 694
    iget-object v3, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/navdy/client/app/framework/models/Destination;->setFavoriteLabel(Ljava/lang/String;)V

    .line 696
    packed-switch p1, :pswitch_data_0

    .line 711
    iget-object v3, v0, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-ne v3, v4, :cond_1

    .line 715
    const/4 v2, -0x4

    .line 723
    .local v2, "specialType":I
    :goto_1
    move v1, v2

    .line 725
    .local v1, "finalSpecialType":I
    new-instance v3, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;

    invoke-direct {v3, p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$7;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;I)V

    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_0

    .line 698
    .end local v1    # "finalSpecialType":I
    .end local v2    # "specialType":I
    :pswitch_0
    const/4 v2, -0x3

    .line 699
    .restart local v2    # "specialType":I
    const v3, 0x7f08023f

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/client/app/framework/models/Destination;->setFavoriteLabel(Ljava/lang/String;)V

    .line 700
    const-string v3, "Set_Home"

    invoke-static {v3}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    goto :goto_1

    .line 704
    .end local v2    # "specialType":I
    :pswitch_1
    const/4 v2, -0x2

    .line 705
    .restart local v2    # "specialType":I
    const v3, 0x7f080503

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/navdy/client/app/framework/models/Destination;->setFavoriteLabel(Ljava/lang/String;)V

    .line 706
    const-string v3, "Set_Work"

    invoke-static {v3}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    goto :goto_1

    .line 717
    .end local v2    # "specialType":I
    :cond_1
    const/4 v2, -0x1

    .restart local v2    # "specialType":I
    goto :goto_1

    .line 696
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private searchOrRouteToSharedText(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3
    .param p1, "sharedText"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1391
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Ljava/lang/String;Landroid/content/Intent;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1450
    return-void
.end method

.method private sendDestinationToNavdy(ILandroid/content/Intent;)V
    .locals 5
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 661
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 663
    const-string v2, "search_result"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 665
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "destination address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 667
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$6;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$6;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 676
    .local v1, "requestRouteRunnable":Ljava/lang/Runnable;
    const-string v2, "extra_is_voice_search"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 677
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 682
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v1    # "requestRouteRunnable":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    return-void

    .line 679
    .restart local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v1    # "requestRouteRunnable":Ljava/lang/Runnable;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showRequestNewRouteDialog(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setConnectionIndicator()V
    .locals 2

    .prologue
    .line 885
    const v1, 0x7f1002b5

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 887
    .local v0, "indicator":Landroid/widget/ImageView;
    if-nez v0, :cond_0

    .line 897
    :goto_0
    return-void

    .line 891
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 892
    const v1, 0x7f020150

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 896
    :goto_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showConnectivityBanner()V

    goto :goto_0

    .line 894
    :cond_1
    const v1, 0x7f020151

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method private setOtherFabsVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addFavoriteLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1094
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isHomeSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1095
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addHome:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 1096
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addHomeLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1098
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isWorkSet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1099
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addWork:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 1100
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addWorkLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1102
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->film:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1103
    return-void
.end method

.method private setUpFloatingActionMenu()V
    .locals 2

    .prologue
    .line 1044
    const v0, 0x7f1000e7

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addFavoriteLabel:Landroid/widget/TextView;

    .line 1045
    const v0, 0x7f1000e6

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addHome:Landroid/support/design/widget/FloatingActionButton;

    .line 1046
    const v0, 0x7f1000e8

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addHomeLabel:Landroid/widget/TextView;

    .line 1047
    const v0, 0x7f1000e5

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/FloatingActionButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addWork:Landroid/support/design/widget/FloatingActionButton;

    .line 1048
    const v0, 0x7f1000e9

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addWorkLabel:Landroid/widget/TextView;

    .line 1049
    const v0, 0x7f1000e3

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->film:Landroid/view/View;

    .line 1051
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addHome:Landroid/support/design/widget/FloatingActionButton;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$12;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$12;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1058
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->addWork:Landroid/support/design/widget/FloatingActionButton;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$13;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$13;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1065
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->film:Landroid/view/View;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$14;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$14;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071
    return-void
.end method

.method private setUpLogoEasterEgg()V
    .locals 2

    .prologue
    .line 788
    const v1, 0x7f1001ef

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 789
    .local v0, "logo":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 790
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 840
    :cond_0
    return-void
.end method

.method private setUsernameAndAddress()V
    .locals 7

    .prologue
    .line 547
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    if-nez v5, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 551
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/design/widget/NavigationView;->getHeaderView(I)Landroid/view/View;

    move-result-object v4

    .line 552
    .local v4, "headerView":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 553
    new-instance v5, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$4;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$4;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 563
    invoke-static {}, Lcom/navdy/client/app/framework/models/UserAccountInfo;->getUserAccountInfo()Lcom/navdy/client/app/framework/models/UserAccountInfo;

    move-result-object v1

    .line 564
    .local v1, "accountInfo":Lcom/navdy/client/app/framework/models/UserAccountInfo;
    const v5, 0x7f100100

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    .line 565
    .local v3, "accountPhoto":Lcom/navdy/client/app/ui/customviews/DestinationImageView;
    if-eqz v3, :cond_2

    .line 566
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_4

    .line 567
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v5}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(Landroid/graphics/Bitmap;)V

    .line 572
    :cond_2
    :goto_1
    const v5, 0x7f100102

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 573
    .local v2, "accountName":Landroid/widget/TextView;
    if-eqz v2, :cond_3

    .line 574
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 575
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 580
    :cond_3
    :goto_2
    const v5, 0x7f100101

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 581
    .local v0, "accountEmail":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 582
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 583
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 569
    .end local v0    # "accountEmail":Landroid/widget/TextView;
    .end local v2    # "accountName":Landroid/widget/TextView;
    :cond_4
    const v5, 0x7f0201b4

    iget-object v6, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(ILjava/lang/String;)V

    goto :goto_1

    .line 577
    .restart local v2    # "accountName":Landroid/widget/TextView;
    :cond_5
    const v5, 0x7f0801e5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 584
    .restart local v0    # "accountEmail":Landroid/widget/TextView;
    :cond_6
    iget-object v5, v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 585
    const v5, 0x7f0800bc

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 587
    :cond_7
    const v5, 0x7f0801e4

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private setupBurgerMenu()V
    .locals 19

    .prologue
    .line 364
    const v2, 0x7f1002b2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/widget/DrawerLayout;

    .line 365
    .local v3, "drawer":Landroid/support/v4/widget/DrawerLayout;
    if-nez v3, :cond_1

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    new-instance v1, Landroid/support/v7/app/ActionBarDrawerToggle;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v5, 0x7f0802b8

    const v6, 0x7f0802b7

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Landroid/support/v7/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    .line 370
    .local v1, "toggle":Landroid/support/v7/app/ActionBarDrawerToggle;
    invoke-virtual {v3, v1}, Landroid/support/v4/widget/DrawerLayout;->addDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 371
    invoke-virtual {v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    .line 373
    const v2, 0x7f1002b3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/NavigationView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    .line 374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    if-eqz v2, :cond_0

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/support/design/widget/NavigationView;->setItemIconTintList(Landroid/content/res/ColorStateList;)V

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    .line 379
    .local v12, "applicationContext":Landroid/content/Context;
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setUsernameAndAddress()V

    .line 382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    invoke-virtual {v2}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v14

    .line 383
    .local v14, "menu":Landroid/view/Menu;
    if-eqz v14, :cond_6

    .line 384
    const v2, 0x7f100413

    invoke-interface {v14, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 385
    .local v13, "debugMenuItem":Landroid/view/MenuItem;
    if-eqz v13, :cond_2

    .line 389
    :cond_2
    const v2, 0x7f10040e

    invoke-interface {v14, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    .line 390
    .local v15, "messagingMenuItem":Landroid/view/MenuItem;
    if-eqz v15, :cond_3

    .line 391
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getPermissionsSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v16

    .line 392
    .local v16, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v2, "hud_canned_response_capable"

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v17

    .line 395
    .local v17, "supportsCannedResponses":Z
    move/from16 v0, v17

    invoke-interface {v15, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 398
    .end local v16    # "sharedPrefs":Landroid/content/SharedPreferences;
    .end local v17    # "supportsCannedResponses":Z
    :cond_3
    const v2, 0x7f1003f3

    invoke-interface {v14, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v18

    .line 399
    .local v18, "versionItem":Landroid/view/MenuItem;
    if-eqz v18, :cond_6

    .line 400
    invoke-interface/range {v18 .. v18}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v7

    .line 402
    .local v7, "actionView":Landroid/view/View;
    const v2, 0x7f1003f1

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 403
    .local v8, "appVersion":Landroid/widget/TextView;
    if-eqz v8, :cond_5

    .line 404
    const v2, 0x7f0802b0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 405
    .local v10, "appVersionRes":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getAppVersionString()Ljava/lang/String;

    move-result-object v11

    .line 406
    .local v11, "appVersionString":Ljava/lang/String;
    const v2, 0x7f0804da

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 407
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 409
    :cond_4
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v11, v2, v4

    invoke-static {v10, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 410
    .local v9, "appVersionFormattedString":Ljava/lang/String;
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    .end local v9    # "appVersionFormattedString":Ljava/lang/String;
    .end local v10    # "appVersionRes":Ljava/lang/String;
    .end local v11    # "appVersionString":Ljava/lang/String;
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateHudVersionString()V

    .line 418
    .end local v7    # "actionView":Landroid/view/View;
    .end local v8    # "appVersion":Landroid/widget/TextView;
    .end local v13    # "debugMenuItem":Landroid/view/MenuItem;
    .end local v15    # "messagingMenuItem":Landroid/view/MenuItem;
    .end local v18    # "versionItem":Landroid/view/MenuItem;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    new-instance v4, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/content/Context;)V

    invoke-virtual {v2, v4}, Landroid/support/design/widget/NavigationView;->setNavigationItemSelectedListener(Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;)V

    goto/16 :goto_0
.end method

.method private setupViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 781
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    .line 782
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 783
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 784
    invoke-virtual {p1, p0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 785
    return-void
.end method

.method private static showAllTips()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 853
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 854
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 855
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "user_watched_the_demo"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 856
    const-string v2, "user_enabled_glances_once_before"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 857
    const-string v2, "user_tried_gestures_once_before"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 858
    const-string v2, "user_already_saw_microphone_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 859
    const-string v2, "user_already_saw_google_now_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 860
    const-string v2, "user_already_saw_gesture_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 861
    const-string v2, "user_already_saw_add_home_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 862
    const-string v2, "user_already_saw_add_work_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 863
    const-string v2, "user_already_saw_music_tip"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 864
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 865
    return-void
.end method

.method private showConnectivityBanner()V
    .locals 10

    .prologue
    const/16 v7, 0x8

    const/4 v8, 0x0

    .line 915
    const v6, 0x7f1000ea

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 916
    .local v4, "successBanner":Landroid/widget/RelativeLayout;
    const v6, 0x7f1000f0

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 917
    .local v2, "failureBanner":Landroid/widget/RelativeLayout;
    if-eqz v4, :cond_0

    if-nez v2, :cond_1

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 920
    :cond_1
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 921
    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 922
    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 924
    const v6, 0x7f1000ee

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 925
    .local v5, "text":Landroid/widget/TextView;
    if-eqz v5, :cond_3

    .line 926
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v3

    .line 927
    .local v3, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    const/4 v1, 0x0

    .line 928
    .local v1, "deviceName":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 929
    invoke-virtual {v3}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 930
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_2

    .line 931
    iget-object v1, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceName:Ljava/lang/String;

    .line 934
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_2
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 935
    const v6, 0x7f08010c

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 940
    .end local v1    # "deviceName":Ljava/lang/String;
    .end local v3    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_3
    :goto_1
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->handler:Landroid/os/Handler;

    new-instance v7, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$10;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$10;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    const-wide/16 v8, 0x7d0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 937
    .restart local v1    # "deviceName":Ljava/lang/String;
    .restart local v3    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_4
    const v6, 0x7f0802db

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 947
    .end local v1    # "deviceName":Ljava/lang/String;
    .end local v3    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_5
    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 948
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private showOtherFabs()V
    .locals 1

    .prologue
    .line 1083
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFabShowing:Z

    .line 1084
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setOtherFabsVisibility(I)V

    .line 1085
    return-void
.end method

.method private stopSelectionActionMode()V
    .locals 1

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 1614
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 1616
    :cond_0
    return-void
.end method

.method private updateHudVersionString()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 466
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 467
    .local v1, "applicationContext":Landroid/content/Context;
    iget-object v11, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    invoke-virtual {v11}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v9

    .line 468
    .local v9, "menu":Landroid/view/Menu;
    if-eqz v9, :cond_2

    .line 469
    const v11, 0x7f100415

    invoke-interface {v9, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    .line 470
    .local v10, "versionItem":Landroid/view/MenuItem;
    if-eqz v10, :cond_2

    .line 471
    invoke-interface {v10}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 472
    .local v0, "actionView":Landroid/view/View;
    const v11, 0x7f1003f1

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 473
    .local v6, "hudVersion":Landroid/widget/TextView;
    if-eqz v6, :cond_1

    .line 474
    const v11, 0x7f0802b6

    invoke-virtual {p0, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 475
    .local v8, "hudVersionRes":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getDisplayVersionString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 476
    .local v2, "displayVersionString":Ljava/lang/String;
    const v11, 0x7f0804da

    invoke-virtual {p0, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 477
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "v."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 479
    :cond_0
    new-array v11, v14, [Ljava/lang/Object;

    aput-object v2, v11, v13

    invoke-static {v8, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 480
    .local v7, "hudVersionFormattedString":Ljava/lang/String;
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 482
    .end local v2    # "displayVersionString":Ljava/lang/String;
    .end local v7    # "hudVersionFormattedString":Ljava/lang/String;
    .end local v8    # "hudVersionRes":Ljava/lang/String;
    :cond_1
    const v11, 0x7f1003f2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 483
    .local v3, "hudSerial":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    .line 484
    const v11, 0x7f080550

    invoke-virtual {p0, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 485
    .local v5, "hudSerialString":Ljava/lang/String;
    new-array v11, v14, [Ljava/lang/Object;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getDisplaySerialString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v13

    invoke-static {v5, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 486
    .local v4, "hudSerialFormattedString":Ljava/lang/String;
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    .end local v0    # "actionView":Landroid/view/View;
    .end local v3    # "hudSerial":Landroid/widget/TextView;
    .end local v4    # "hudSerialFormattedString":Ljava/lang/String;
    .end local v5    # "hudSerialString":Ljava/lang/String;
    .end local v6    # "hudVersion":Landroid/widget/TextView;
    .end local v10    # "versionItem":Landroid/view/MenuItem;
    :cond_2
    return-void
.end method

.method private updateMenuVisibility(ZI)V
    .locals 3
    .param p1, "visible"    # Z
    .param p2, "menuItemId"    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 843
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    invoke-virtual {v2}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 844
    .local v0, "menu":Landroid/view/Menu;
    if-eqz v0, :cond_0

    .line 845
    invoke-interface {v0, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 846
    .local v1, "messagingMenuItem":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 847
    invoke-interface {v1, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 850
    .end local v1    # "messagingMenuItem":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method private updateMessagingVisibility()V
    .locals 4

    .prologue
    .line 1009
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1010
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "messaging_has_seen_capable_hud"

    const/4 v3, 0x0

    .line 1011
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1013
    .local v0, "messagingHasSeenCapableHud":Z
    const v2, 0x7f10040e

    invoke-direct {p0, v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateMenuVisibility(ZI)V

    .line 1014
    return-void
.end method


# virtual methods
.method goToFavoritesTab()V
    .locals 2

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1677
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1679
    :cond_0
    return-void
.end method

.method goToGesture()V
    .locals 2

    .prologue
    .line 1689
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1690
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 1691
    return-void
.end method

.method goToGlancesTab()V
    .locals 2

    .prologue
    .line 1682
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1683
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1684
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->highlightGlanceSwitchOnResume:Z

    .line 1686
    :cond_0
    return-void
.end method

.method public hideConnectionBanner(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    .line 904
    const v1, 0x7f1000ea

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 905
    .local v0, "banner":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 906
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 908
    :cond_0
    const v1, 0x7f1000f0

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "banner":Landroid/widget/RelativeLayout;
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 909
    .restart local v0    # "banner":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_1

    .line 910
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 912
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 621
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult: requestCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 622
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 657
    :cond_0
    :goto_0
    return-void

    .line 626
    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 628
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->trackerEvent:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 630
    const-string v1, "Navigate_Using_Search_Results"

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->trackerEvent:Ljava/lang/String;

    .line 631
    invoke-direct {p0, p2, p3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->sendDestinationToNavdy(ILandroid/content/Intent;)V

    goto :goto_0

    .line 636
    :pswitch_2
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->saveDestinationToFavorites(IILandroid/content/Intent;)V

    goto :goto_0

    .line 639
    :pswitch_3
    const-string v1, "updated_destination"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 640
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Details Code reached"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 641
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCurrentDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 642
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    .line 643
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "destination contents: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 644
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$5;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$5;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;->reloadSelfFromDatabaseAsync(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 626
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 597
    const v1, 0x7f1002b2

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 599
    .local v0, "drawer":Landroid/support/v4/widget/DrawerLayout;
    invoke-static {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Activity already ending so ignoring onBackPressed."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 613
    :goto_0
    return-void

    .line 604
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 605
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    goto :goto_0

    .line 606
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eqz v1, :cond_2

    .line 607
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 608
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 609
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideOtherFabs()V

    goto :goto_0

    .line 611
    :cond_2
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 173
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HomescreenActivity::onCreate"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 174
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 175
    const v6, 0x7f0300a9

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setContentView(I)V

    .line 178
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 181
    const v6, 0x7f1002b4

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/Toolbar;

    iput-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 182
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 183
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setConnectionIndicator()V

    .line 184
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setUpLogoEasterEgg()V

    .line 187
    const v6, 0x7f1002b5

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 188
    .local v5, "navdyDisplay":Landroid/view/View;
    if-eqz v5, :cond_0

    .line 189
    new-instance v6, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setupBurgerMenu()V

    .line 217
    const v6, 0x7f1002b8

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v4/view/ViewPager;

    iput-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 218
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setupViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 220
    const v6, 0x7f1002b7

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/design/widget/TabLayout;

    iput-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    .line 221
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    if-eqz v6, :cond_1

    .line 222
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v7, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6, v7}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 226
    :cond_1
    const v6, 0x7f1000e4

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/design/widget/FloatingActionButton;

    iput-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    .line 227
    const v6, 0x7f040013

    invoke-static {p0, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 228
    .local v1, "animation":Landroid/view/animation/Animation;
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v6, v1}, Landroid/support/design/widget/FloatingActionButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 229
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    new-instance v7, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$2;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$2;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v6, v7}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setUpFloatingActionMenu()V

    .line 238
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 239
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "action":Ljava/lang/String;
    const-string v6, "com.google.android.gms.actions.SEARCH_ACTION"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "com.navdy.client.actions.SEARCH_ACTION"

    .line 242
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    :cond_2
    const/4 v4, 0x1

    .line 244
    .local v4, "isComingFromGoogleNow":Z
    :goto_0
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Search is coming from Google Now = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 245
    if-eqz v4, :cond_3

    const-string v6, "query"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 246
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    const-string v6, "query"

    const-string v7, "query"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    sget-object v6, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v6}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v6

    invoke-virtual {p0, v3, v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 250
    const-string v6, "Navigate_Using_Google_Now"

    iput-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->trackerEvent:Ljava/lang/String;

    .line 252
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_3
    return-void

    .line 242
    .end local v4    # "isComingFromGoogleNow":Z
    :cond_4
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 355
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onDestroy()V

    .line 357
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HomescreenActivity::onDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public onDeviceConnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 3
    .param p1, "deviceConnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 989
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeviceConnectedEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 990
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setConnectionIndicator()V

    .line 992
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateMessagingVisibility()V

    .line 993
    return-void
.end method

.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 3
    .param p1, "deviceDisconnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 997
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeviceDisconnectedEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 998
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setConnectionIndicator()V

    .line 999
    return-void
.end method

.method public onDeviceInfoEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;)V
    .locals 3
    .param p1, "deviceInfoEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeviceInfoEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1004
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateHudVersionString()V

    .line 1005
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateMessagingVisibility()V

    .line 1006
    return-void
.end method

.method public onFailureBannerClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 953
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 954
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->openBtConnectionDialog()V

    .line 956
    :cond_0
    return-void
.end method

.method public onFavoriteListChanged()V
    .locals 1

    .prologue
    .line 754
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 769
    return-void
.end method

.method public onHelpConnectClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 984
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/SupportActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 985
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 1620
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 1621
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setIntent(Landroid/content/Intent;)V

    .line 1623
    const-string v0, "extra_homescreen_explicitly_routing"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1624
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1626
    :cond_0
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 1125
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 1131
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageSelected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1136
    packed-switch p1, :pswitch_data_0

    .line 1149
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0}, Landroid/support/design/widget/FloatingActionButton;->hide()V

    .line 1153
    :goto_0
    if-eqz p1, :cond_0

    .line 1154
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->stopSelectionActionMode()V

    .line 1157
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideConnectionBanner(Landroid/view/View;)V

    .line 1159
    invoke-static {p1}, Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Home;->tag(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 1160
    return-void

    .line 1138
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 1139
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$15;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$15;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->hide(Landroid/support/design/widget/FloatingActionButton$OnVisibilityChangedListener;)V

    goto :goto_0

    .line 1136
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 346
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->stopSelectionActionMode()V

    .line 350
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onPause()V

    .line 351
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1632
    return-void
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1629
    return-void
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1694
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 1695
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 1667
    return-void
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const/16 v9, 0x19

    const/4 v12, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v11, 0x0

    .line 256
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 262
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 263
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v8, "permissions_version"

    invoke-interface {v4, v8, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-le v8, v9, :cond_6

    move v5, v6

    .line 266
    .local v5, "usesOreoPermissions":Z
    :goto_0
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v8, v9, :cond_4

    if-nez v5, :cond_4

    .line 267
    const-string v8, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveThisPermission(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 268
    invoke-virtual {p0, v11, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 270
    :cond_0
    const-string v8, "android.permission.RECEIVE_SMS"

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveThisPermission(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 271
    invoke-virtual {p0, v11, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->requestSmsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 273
    :cond_1
    const-string v8, "android.permission.CALL_PHONE"

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveThisPermission(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 274
    invoke-virtual {p0, v11, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->requestPhonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 276
    :cond_2
    const-string v8, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->weHaveThisPermission(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 277
    invoke-virtual {p0, v11, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 281
    :cond_3
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "permissions_version"

    const/16 v10, 0x1a

    .line 282
    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    .line 283
    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 286
    :cond_4
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getScreensCount()I

    move-result v8

    if-ge v2, v8, :cond_7

    .line 287
    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getScreen(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v3

    .line 288
    .local v3, "screen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v3, :cond_5

    iget-boolean v8, v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-eqz v8, :cond_5

    .line 289
    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->weHavePermissionForThisScreen(Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 290
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Screen "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") is mandatory and we seem to be missing this permission."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 292
    invoke-static {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->goToAppSetup(Landroid/app/Activity;)V

    .line 293
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->finish()V

    .line 286
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v2    # "i":I
    .end local v3    # "screen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    .end local v5    # "usesOreoPermissions":Z
    :cond_6
    move v5, v7

    .line 263
    goto/16 :goto_0

    .line 300
    .restart local v2    # "i":I
    .restart local v5    # "usesOreoPermissions":Z
    :cond_7
    iget-boolean v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->reloadProfile:Z

    if-eqz v8, :cond_8

    .line 301
    iput-boolean v7, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->reloadProfile:Z

    .line 302
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setUsernameAndAddress()V

    .line 305
    :cond_8
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateHudVersionString()V

    .line 307
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->updateMessagingVisibility()V

    .line 310
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 311
    .local v0, "callingIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastExternalIntentHandled:Landroid/content/Intent;

    if-eq v0, v8, :cond_9

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->handleIntent(Landroid/content/Intent;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 312
    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->lastExternalIntentHandled:Landroid/content/Intent;

    .line 315
    :cond_9
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v8}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v8

    if-eq v8, v6, :cond_a

    .line 316
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->fab:Landroid/support/design/widget/FloatingActionButton;

    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 320
    :cond_a
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->isDeveloperBuild()Z

    move-result v6

    if-nez v6, :cond_b

    .line 321
    invoke-direct {p0, v11}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->checkForUpdates(Lnet/hockeyapp/android/UpdateManagerListener;)V

    .line 324
    :cond_b
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v6, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 326
    iget-boolean v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->highlightGlanceSwitchOnResume:Z

    if-eqz v6, :cond_c

    .line 327
    iput-boolean v7, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->highlightGlanceSwitchOnResume:Z

    .line 328
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v6, :cond_c

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v6

    if-nez v6, :cond_c

    .line 329
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v6, v12}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 330
    iget-object v6, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    invoke-virtual {v6, v12}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 331
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v6, v1, Lcom/navdy/client/app/ui/glances/GlancesFragment;

    if-eqz v6, :cond_c

    .line 332
    check-cast v1, Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v1}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->highlightGlanceSwitch()V

    .line 337
    :cond_c
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showObdDialogIfCarHasBeenAddedToBlacklist()V

    .line 339
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setConnectionIndicator()V

    .line 340
    return-void
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1670
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1641
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->openStartTab()V

    .line 1642
    return-void
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1636
    invoke-static {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->startRoutingActivity(Landroid/app/Activity;)V

    .line 1637
    return-void
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1646
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->openStartTab()V

    .line 1647
    return-void
.end method

.method public onSearchClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1601
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;)V

    .line 1602
    return-void
.end method

.method public onSearchMicClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1605
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->callSearchFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;Z)V

    .line 1606
    return-void
.end method

.method public onStopRoute()V
    .locals 0

    .prologue
    .line 1673
    return-void
.end method

.method public onSuccessBannerClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 960
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 962
    const v1, 0x7f08012f

    const v2, 0x7f080130

    const v3, 0x7f080505

    const v4, 0x7f0800e6

    new-instance v5, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$11;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$11;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->showQuestionDialog(IIIILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 981
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1664
    return-void
.end method

.method public rebuildSuggestions()V
    .locals 3

    .prologue
    .line 772
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    if-eqz v1, :cond_0

    .line 773
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .line 774
    .local v0, "suggestionsFragment":Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v1, :cond_0

    .line 775
    iget-object v1, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 778
    .end local v0    # "suggestionsFragment":Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    :cond_0
    return-void
.end method

.method public routeToFirstResultFor(Ljava/lang/String;)V
    .locals 3
    .param p1, "searchTerm"    # Ljava/lang/String;

    .prologue
    .line 1453
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1454
    .local v0, "searchIntent":Landroid/content/Intent;
    const-string v1, "ACTION_SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1455
    const-string v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1457
    new-instance v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$18;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$18;-><init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/content/Intent;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1463
    return-void
.end method

.method startSelectionActionMode(Landroid/support/v7/view/ActionMode$Callback;)V
    .locals 1
    .param p1, "actionModeCallback"    # Landroid/support/v7/view/ActionMode$Callback;

    .prologue
    .line 1609
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startSupportActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->mode:Landroid/support/v7/view/ActionMode;

    .line 1610
    return-void
.end method
