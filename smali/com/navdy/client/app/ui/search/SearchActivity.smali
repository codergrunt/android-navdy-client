.class public Lcom/navdy/client/app/ui/search/SearchActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "SearchActivity.java"

# interfaces
.implements Lcom/google/android/gms/location/LocationListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;
    }
.end annotation


# static fields
.field private static final AUTO_COMPLETE_MARGIN_BOTTOM:I

.field private static final AUTO_COMPLETE_MARGIN_LEFT:I

.field private static final AUTO_COMPLETE_MARGIN_RIGHT:I

.field private static final AUTO_COMPLETE_MARGIN_TOP:I

.field private static final EXTRA_LOCATION:Ljava/lang/String; = "location"

.field private static final EXTRA_SPEECH_RECOGNITION:Ljava/lang/String; = "speech_recognition"

.field private static final KEY_LISTENER_DELAY:I = 0x1f4

.field private static final VOICE_RECOGNITION_REQUEST_CODE:I = 0x2a

.field private static hasDisplayedOfflineMessage:Z


# instance fields
.field private card:Landroid/widget/RelativeLayout;

.field private centerMap:Landroid/widget/ImageButton;

.field private editText:Landroid/widget/EditText;

.field private fab:Landroid/widget/ImageButton;

.field private fromActionSendIntent:Z

.field private googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field private googlePlacesSearch:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

.field private ignoreAutoCompleteDelay:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private ignoreNextKeystrokeKey:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isComingFromGoogleNow:Z

.field private isShowingMap:Z

.field private lastLocation:Landroid/location/Location;

.field mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private offlineBanner:Landroid/widget/RelativeLayout;

.field private previousSelectedMarker:Lcom/google/android/gms/maps/model/Marker;

.field private requestedLocationUpdates:Z

.field private requestedPlaceDetailsDestinationBackup:Lcom/navdy/client/app/framework/models/Destination;

.field private rightButtonIsMic:Z

.field private searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

.field private searchRecycler:Landroid/support/v7/widget/RecyclerView;

.field private searchType:I

.field private showServices:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x41280000    # 10.5f

    .line 115
    const/high16 v0, -0x3ea00000    # -14.0f

    invoke-static {v0}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_TOP:I

    .line 116
    invoke-static {v1}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_LEFT:I

    .line 117
    invoke-static {v1}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_RIGHT:I

    .line 118
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v0}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_BOTTOM:I

    .line 123
    const/4 v0, 0x0

    sput-boolean v0, Lcom/navdy/client/app/ui/search/SearchActivity;->hasDisplayedOfflineMessage:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 120
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreNextKeystrokeKey:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 121
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreAutoCompleteDelay:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 126
    iput-boolean v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->showServices:Z

    .line 127
    iput-boolean v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->rightButtonIsMic:Z

    .line 138
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    .line 139
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fromActionSendIntent:Z

    .line 147
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isShowingMap:Z

    .line 152
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->buildAndConnectGoogleApiClient()V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/search/SearchActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->showServices:Z

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/search/SearchActivity;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->clearAndApplyListOrDropDownLook()V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showAutoComplete()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateRightButton()V

    return-void
.end method

.method static synthetic access$2200(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showList()V

    return-void
.end method

.method static synthetic access$2300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/navdy/client/app/ui/search/SearchActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreNextKeystrokeKey:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/navdy/client/app/ui/search/SearchActivity;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreAutoCompleteDelay:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runAutoCompleteSearch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runOfflineAutocompleteSearch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runTextSearchOrAskHud(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->getLatLngBounds()V

    return-void
.end method

.method static synthetic access$3000(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->putSearchResultExtraAndFinishIfNoUiRequired(Lcom/navdy/client/app/framework/search/SearchResults;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->addDistancesToDestinationsIfNeeded(Lcom/navdy/client/app/framework/search/SearchResults;)V

    return-void
.end method

.method static synthetic access$3200(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/search/SearchResults;
    .param p2, "x2"    # Z

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateRecyclerViewWithSearchResults(Lcom/navdy/client/app/framework/search/SearchResults;Z)V

    return-void
.end method

.method static synthetic access$3300(Lcom/navdy/client/app/ui/search/SearchActivity;[Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # [Ljava/util/List;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->showNoResultsFoundDialogIfAllAreEmpty([Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/navdy/client/app/ui/search/SearchActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->showList(Z)V

    return-void
.end method

.method static synthetic access$3500(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->addDistancesToDestinations(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$3600(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showNoResultsFoundDialog()V

    return-void
.end method

.method static synthetic access$3702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 104
    sput-boolean p0, Lcom/navdy/client/app/ui/search/SearchActivity;->hasDisplayedOfflineMessage:Z

    return p0
.end method

.method static synthetic access$3800(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->putSearchResultExtraAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->requestContactsPermission()V

    return-void
.end method

.method static synthetic access$4000(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/navdy/client/app/ui/search/SearchActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isShowingMap:Z

    return v0
.end method

.method static synthetic access$4300(Lcom/navdy/client/app/ui/search/SearchActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    return v0
.end method

.method static synthetic access$4400(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/google/android/gms/maps/model/Marker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->previousSelectedMarker:Lcom/google/android/gms/maps/model/Marker;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->unselectCurrentMarker()V

    return-void
.end method

.method static synthetic access$4600(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/google/android/gms/maps/model/Marker;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->selectThisMarker(Lcom/google/android/gms/maps/model/Marker;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/search/SearchActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->rightButtonIsMic:Z

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/client/app/ui/search/SearchActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->rightButtonIsMic:Z

    return p1
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->startVoiceRecognitionActivity()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->resetEditText()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    return-object v0
.end method

.method private addDistancesToDestinations(Ljava/util/List;)V
    .locals 10
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 965
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 967
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "addDistancesToDestinations"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 968
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-gtz v7, :cond_1

    .line 988
    :cond_0
    :goto_0
    return-void

    .line 971
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 973
    .local v0, "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :try_start_0
    iget-object v8, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 974
    .local v2, "destinationLat":D
    iget-object v8, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 975
    .local v4, "destinationLng":D
    new-instance v1, Landroid/location/Location;

    const-string v8, ""

    invoke-direct {v1, v8}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 976
    .local v1, "destinationLocation":Landroid/location/Location;
    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 977
    invoke-virtual {v1, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 978
    iget-object v8, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    if-eqz v8, :cond_2

    .line 980
    iget-object v8, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    invoke-virtual {v8, v1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v8

    float-to-double v8, v8

    iput-wide v8, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->distance:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 982
    .end local v1    # "destinationLocation":Landroid/location/Location;
    .end local v2    # "destinationLat":D
    .end local v4    # "destinationLng":D
    :catch_0
    move-exception v6

    .line 983
    .local v6, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 984
    throw v6

    .line 987
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_3
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "status of destinations: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private addDistancesToDestinationsIfNeeded(Lcom/navdy/client/app/framework/search/SearchResults;)V
    .locals 6
    .param p1, "searchResults"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    const/4 v2, 0x1

    .line 830
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getGoogleResults()Ljava/util/List;

    move-result-object v0

    .line 831
    .local v0, "googleResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    move v1, v2

    .line 833
    .local v1, "weHaveGoogleResults":Z
    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    if-nez v3, :cond_0

    .line 834
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/search/SearchActivity$14;

    invoke-direct {v4, p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity$14;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/util/List;)V

    sget-object v5, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {v3, v4, v2, v5}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;

    .line 843
    :cond_0
    return-void

    .line 831
    .end local v1    # "weHaveGoogleResults":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private applyListOrDropDownLook()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, -0x1

    const/4 v8, 0x0

    .line 489
    const/4 v1, -0x1

    .line 490
    .local v1, "height":I
    const/4 v7, 0x0

    .line 491
    .local v7, "marginTop":I
    const/4 v5, 0x0

    .line 492
    .local v5, "marginLeft":I
    const/4 v6, 0x0

    .line 493
    .local v6, "marginRight":I
    const/4 v4, 0x0

    .line 495
    .local v4, "marginBottom":I
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iget-object v9, v9, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v10, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v9, v10, :cond_3

    const/4 v2, 0x1

    .line 497
    .local v2, "isAutoComplete":Z
    :goto_0
    if-eqz v2, :cond_0

    .line 498
    const/4 v1, -0x2

    .line 499
    sget v7, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_TOP:I

    .line 500
    sget v5, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_LEFT:I

    .line 501
    sget v6, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_RIGHT:I

    .line 502
    sget v4, Lcom/navdy/client/app/ui/search/SearchActivity;->AUTO_COMPLETE_MARGIN_BOTTOM:I

    .line 505
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 507
    .local v0, "canReachInternet":Z
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v9, :cond_1

    .line 508
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v11, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 511
    .local v3, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v0, :cond_4

    .line 512
    invoke-virtual {v3, v5, v7, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 516
    :goto_1
    const v9, 0x7f1000aa

    invoke-virtual {v3, v12, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 518
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v9, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 519
    if-eqz v2, :cond_5

    .line 520
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    const v10, 0x7f02005a

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/RecyclerView;->setBackgroundResource(I)V

    .line 524
    :goto_2
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v9, v8}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    .line 527
    .end local v3    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    if-eqz v9, :cond_2

    .line 528
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v9, -0x2

    invoke-direct {v3, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 531
    .restart local v3    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v5, v7, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 532
    const v8, 0x7f100358

    invoke-virtual {v3, v12, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 533
    iget-object v8, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 536
    .end local v3    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updatePoweredByGoogleHeaderVisibility(Z)V

    .line 537
    return-void

    .end local v0    # "canReachInternet":Z
    .end local v2    # "isAutoComplete":Z
    :cond_3
    move v2, v8

    .line 495
    goto :goto_0

    .line 514
    .restart local v0    # "canReachInternet":Z
    .restart local v2    # "isAutoComplete":Z
    .restart local v3    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    invoke-virtual {v3, v5, v8, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    .line 522
    :cond_5
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    const v10, 0x7f0f00cc

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/RecyclerView;->setBackgroundResource(I)V

    goto :goto_2
.end method

.method private declared-synchronized buildAndConnectGoogleApiClient()V
    .locals 2

    .prologue
    .line 427
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    .line 428
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 429
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/Places;->GEO_DATA_API:Lcom/google/android/gms/common/api/Api;

    .line 430
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/Places;->PLACE_DETECTION_API:Lcom/google/android/gms/common/api/Api;

    .line 431
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/LocationServices;->API:Lcom/google/android/gms/common/api/Api;

    .line 432
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 434
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 435
    monitor-exit p0

    return-void

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private checkServices()V
    .locals 3

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 362
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 363
    const-string v1, "search_type"

    sget-object v2, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    .line 364
    iget v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    sget-object v2, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->showServices:Z

    .line 370
    :cond_0
    return-void

    .line 364
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private clear()V
    .locals 1

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v0, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->clear()V

    .line 1382
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    .line 1383
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$RecycledViewPool;->clear()V

    .line 1385
    :cond_1
    return-void
.end method

.method private clearAndApplyListOrDropDownLook()V
    .locals 0

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->clear()V

    .line 479
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->applyListOrDropDownLook()V

    .line 480
    return-void
.end method

.method private createLocationRequest()V
    .locals 4

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "creating LocationRequest Object"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1006
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->create()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/16 v1, 0x64

    .line 1007
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->setPriority(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x2710

    .line 1008
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->setInterval(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    .line 1009
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->setFastestInterval(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

    .line 1010
    return-void
.end method

.method private createOfflineDialogForOnClick(Landroid/view/View;)Landroid/app/Dialog;
    .locals 5
    .param p1, "searchRow"    # Landroid/view/View;

    .prologue
    .line 1226
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1227
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1228
    const v3, 0x7f08031a

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1229
    .local v2, "title":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1230
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1231
    :cond_0
    const v3, 0x7f08031b

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1232
    .local v1, "message":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1233
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1235
    :cond_1
    const v3, 0x7f080402

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/search/SearchActivity$20;

    invoke-direct {v4, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$20;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Landroid/view/View;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1242
    const v3, 0x7f0800e5

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1243
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method private getLatLngBounds()V
    .locals 14

    .prologue
    const-wide v10, 0x4099240000000000L    # 1609.0

    .line 440
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "getLatLngBounds"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 441
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    if-nez v6, :cond_0

    .line 442
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 444
    .local v0, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v0, :cond_0

    .line 445
    new-instance v6, Landroid/location/Location;

    const-string v7, ""

    invoke-direct {v6, v7}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    .line 446
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    iget-object v7, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 447
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    iget-object v7, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/location/Location;->setLongitude(D)V

    .line 451
    .end local v0    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    if-eqz v6, :cond_1

    .line 452
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    .line 453
    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    iget-object v8, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    .line 454
    invoke-virtual {v8}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-direct {v1, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 455
    .local v1, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    const-wide/16 v6, 0x0

    .line 456
    invoke-static {v1, v10, v11, v6, v7}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    .line 457
    .local v2, "north":Lcom/google/android/gms/maps/model/LatLng;
    const-wide v6, 0x4056800000000000L    # 90.0

    .line 458
    invoke-static {v2, v10, v11, v6, v7}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    .line 459
    .local v3, "northEast":Lcom/google/android/gms/maps/model/LatLng;
    const-wide v6, 0x4066800000000000L    # 180.0

    .line 460
    invoke-static {v1, v10, v11, v6, v7}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    .line 461
    .local v4, "south":Lcom/google/android/gms/maps/model/LatLng;
    const-wide v6, 0x4070e00000000000L    # 270.0

    .line 462
    invoke-static {v4, v10, v11, v6, v7}, Lcom/google/maps/android/SphericalUtil;->computeOffset(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v5

    .line 463
    .local v5, "southWest":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v6, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v5, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v10, v5, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v8, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v10, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v12, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v8, v10, v11, v12, v13}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 475
    .end local v1    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .end local v2    # "north":Lcom/google/android/gms/maps/model/LatLng;
    .end local v3    # "northEast":Lcom/google/android/gms/maps/model/LatLng;
    .end local v4    # "south":Lcom/google/android/gms/maps/model/LatLng;
    .end local v5    # "southWest":Lcom/google/android/gms/maps/model/LatLng;
    :cond_1
    return-void
.end method

.method private hideCard()V
    .locals 2

    .prologue
    .line 1553
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1554
    return-void
.end method

.method static isFavoriteSearch(I)Z
    .locals 2
    .param p0, "searchType"    # I

    .prologue
    const/4 v0, 0x1

    .line 1206
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private putSearchResultExtraAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1287
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 1289
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$23;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$23;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 1327
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity$23;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1328
    return-void
.end method

.method private putSearchResultExtraAndFinishIfNoUiRequired(Lcom/navdy/client/app/framework/search/SearchResults;)V
    .locals 3
    .param p1, "searchResults"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    .line 781
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fromActionSendIntent:Z

    if-eqz v1, :cond_1

    .line 783
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getFirstDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 784
    .local v0, "firstDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_2

    .line 785
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->putSearchResultExtraAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 790
    .end local v0    # "firstDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    :goto_0
    return-void

    .line 787
    .restart local v0    # "firstDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Unable to get search results for Google now or hud voice search query!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestContactsPermission()V
    .locals 2

    .prologue
    .line 284
    const/4 v0, 0x0

    new-instance v1, Lcom/navdy/client/app/ui/search/SearchActivity$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$4;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->requestContactsPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 290
    return-void
.end method

.method private requestLocationUpdate()V
    .locals 4

    .prologue
    .line 992
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->createLocationRequest()V

    .line 993
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "calling requestLocationUpdates from FusedLocationApi"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 994
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 995
    .local v0, "context":Landroid/content/Context;
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 996
    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 999
    sget-object v1, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-interface {v1, v2, v3, p0}, Lcom/google/android/gms/location/FusedLocationProviderApi;->requestLocationUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/LocationListener;)Lcom/google/android/gms/common/api/PendingResult;

    .line 1001
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->requestedLocationUpdates:Z

    .line 1002
    return-void
.end method

.method private resetEditText()V
    .locals 2

    .prologue
    .line 321
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->EMPTY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 322
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 323
    return-void
.end method

.method private runAutoCompleteSearch(Ljava/lang/String;)V
    .locals 3
    .param p1, "constraint"    # Ljava/lang/String;

    .prologue
    .line 541
    if-nez p1, :cond_0

    .line 609
    :goto_0
    return-void

    .line 545
    :cond_0
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$7;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$7;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V

    .line 608
    .local v0, "runGoogleQueryAutoComplete":Ljava/lang/Runnable;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private runOfflineAutocompleteSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "constraint"    # Ljava/lang/String;

    .prologue
    .line 618
    if-nez p1, :cond_0

    .line 660
    :goto_0
    return-void

    .line 622
    :cond_0
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$8;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$8;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 659
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity$8;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V
    .locals 3
    .param p1, "serviceType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .prologue
    .line 846
    if-nez p1, :cond_0

    .line 871
    :goto_0
    return-void

    .line 850
    :cond_0
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 852
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 853
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->rightButtonIsMic:Z

    .line 854
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateRightButton()V

    .line 855
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googlePlacesSearch:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    goto :goto_0

    .line 856
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 857
    new-instance v0, Lcom/navdy/client/app/framework/search/NavdySearch;

    new-instance v1, Lcom/navdy/client/app/ui/search/SearchActivity$15;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$15;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/search/NavdySearch;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;Z)V

    .line 867
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->getHudServiceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->runSearch(Ljava/lang/String;)V

    goto :goto_0

    .line 869
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    goto :goto_0
.end method

.method private runTextSearchOrAskHud(Ljava/lang/String;)Z
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 748
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 749
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 777
    :goto_0
    return v0

    .line 752
    :cond_0
    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-direct {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 753
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 755
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchActivity$11;

    invoke-direct {v2, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$11;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V

    new-array v3, v1, [Ljava/lang/Void;

    .line 761
    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/search/SearchActivity$11;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 763
    new-instance v2, Lcom/navdy/client/app/framework/search/NavdySearch;

    new-instance v3, Lcom/navdy/client/app/ui/search/SearchActivity$12;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$12;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-direct {v2, v3, v0}, Lcom/navdy/client/app/framework/search/NavdySearch;-><init>(Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;Z)V

    .line 775
    invoke-virtual {v2, p1}, Lcom/navdy/client/app/framework/search/NavdySearch;->runSearch(Ljava/lang/String;)V

    move v0, v1

    .line 777
    goto :goto_0
.end method

.method private selectThisMarker(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 2
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1483
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->updatePin(Lcom/google/android/gms/maps/model/Marker;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 1484
    .local v0, "destinationIndex":Ljava/lang/Integer;
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->previousSelectedMarker:Lcom/google/android/gms/maps/model/Marker;

    .line 1485
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showCardFor(Ljava/lang/Integer;)V

    .line 1486
    return-void
.end method

.method private setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V
    .locals 3
    .param p1, "newMode"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .prologue
    .line 874
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v0, :cond_0

    .line 875
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting current mode to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 876
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iput-object p1, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .line 880
    :goto_0
    return-void

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to current mode to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because the searchAdapter is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setEditTextListener()V
    .locals 3

    .prologue
    .line 664
    const v1, 0x7f10036f

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    .line 666
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    new-instance v2, Lcom/navdy/client/app/ui/search/SearchActivity$9;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$9;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 731
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    new-instance v2, Lcom/navdy/client/app/ui/search/SearchActivity$10;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$10;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    :goto_0
    return-void

    .line 742
    :catch_0
    move-exception v0

    .line 743
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private setEditTextWithoutTriggeringAutocomplete(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1406
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1407
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1409
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1410
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->rightButtonIsMic:Z

    .line 1411
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateRightButton()V

    .line 1412
    return-void
.end method

.method private setHintText()V
    .locals 2

    .prologue
    .line 407
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 408
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v1, 0x7f0803e9

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->WORK:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 410
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v1, 0x7f0803ec

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0

    .line 411
    :cond_2
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->HOME:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 412
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v1, 0x7f0803eb

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0

    .line 413
    :cond_3
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->FAVORITE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 414
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v1, 0x7f0803ea

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0
.end method

.method private setRecyclerView()V
    .locals 5

    .prologue
    .line 391
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 392
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 393
    new-instance v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iget v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    invoke-direct {v1, v2, v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;-><init>(ILcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Landroid/widget/RelativeLayout;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .line 394
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    .line 395
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 397
    :cond_0
    new-instance v1, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/ui/search/SearchActivity$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/search/SearchActivity$ServiceAndHistoryAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 398
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setClickListener(Landroid/view/View$OnClickListener;)V

    .line 400
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_1

    .line 401
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 403
    :cond_1
    return-void
.end method

.method private setResultForAutocompleteAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 3
    .param p1, "d"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1213
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 1214
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->requestedPlaceDetailsDestinationBackup:Lcom/navdy/client/app/framework/models/Destination;

    .line 1215
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Getting place details for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1217
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googlePlacesSearch:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v1, p1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runDetailsSearchWebApi(Ljava/lang/String;)V

    .line 1218
    return-void
.end method

.method private setResultForContactAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 3
    .param p1, "d"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1248
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 1251
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$21;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$21;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 1259
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity$21;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1261
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/search/SearchActivity$22;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$22;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1283
    return-void
.end method

.method private setupToolbarButtons()V
    .locals 3

    .prologue
    .line 294
    const v2, 0x7f100165

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 295
    .local v0, "backButton":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 296
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchActivity$5;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$5;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 303
    const v2, 0x7f10036e

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 304
    .local v1, "rightButton":Landroid/widget/ImageButton;
    if-eqz v1, :cond_0

    .line 305
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchActivity$6;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$6;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 318
    .end local v1    # "rightButton":Landroid/widget/ImageButton;
    :cond_0
    return-void
.end method

.method private showAutoComplete()V
    .locals 2

    .prologue
    .line 1515
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    .line 1516
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showList()V

    .line 1517
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1518
    return-void
.end method

.method private showCardFor(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "destinationIndex"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1544
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_1

    .line 1550
    :cond_0
    :goto_0
    return-void

    .line 1547
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1548
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;

    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;-><init>(Landroid/view/View;)V

    .line 1549
    .local v0, "svh":Lcom/navdy/client/app/ui/search/SearchViewHolder;
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setCardData(Lcom/navdy/client/app/ui/search/SearchViewHolder;I)V

    goto :goto_0
.end method

.method private showInstructionalCard()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    .line 1557
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f100361

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    .line 1558
    .local v2, "icon":Lcom/navdy/client/app/ui/customviews/DestinationImageView;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f100360

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1559
    .local v5, "row":Landroid/view/View;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f1002f5

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1560
    .local v3, "navBtn":Landroid/view/View;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f100363

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1561
    .local v6, "title":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f100364

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1562
    .local v0, "details":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f100366

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1563
    .local v4, "price":Landroid/view/View;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const v8, 0x7f100365

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1565
    .local v1, "distance":Landroid/view/View;
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1566
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1567
    const v7, 0x7f020132

    invoke-virtual {v2, v7}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImageResource(I)V

    .line 1568
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1569
    const v7, 0x7f08033d

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1570
    const v7, 0x7f080497

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(I)V

    .line 1571
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1572
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1573
    return-void
.end method

.method private showList()V
    .locals 1

    .prologue
    .line 1521
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showList(Z)V

    .line 1522
    return-void
.end method

.method private showList(Z)V
    .locals 4
    .param p1, "showMapFab"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 1525
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isShowingMap:Z

    .line 1526
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1527
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    const v3, 0x7f020153

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1528
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1529
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1531
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v0, p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->hideThisFragment(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V

    .line 1533
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->centerMap:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1534
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideCard()V

    .line 1535
    return-void

    :cond_0
    move v0, v1

    .line 1528
    goto :goto_0
.end method

.method private showMap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1435
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isShowingMap:Z

    .line 1436
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->unselectCurrentMarker()V

    .line 1437
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 1438
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->previousSelectedMarker:Lcom/google/android/gms/maps/model/Marker;

    .line 1440
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v0, p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->showThisFragment(Landroid/app/Fragment;Landroid/support/v4/app/FragmentActivity;)V

    .line 1442
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->centerMap:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1443
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1444
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    const v1, 0x7f020152

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1445
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 1446
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->centerMapOnMarkers(Z)V

    .line 1448
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v0, :cond_0

    .line 1449
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/search/SearchActivity$25;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$25;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 1473
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showInstructionalCard()V

    .line 1474
    return-void
.end method

.method private showNoResultsFoundDialog()V
    .locals 5

    .prologue
    .line 1346
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->isActivityDestroyed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1376
    :goto_0
    return-void

    .line 1350
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v2, :cond_1

    .line 1351
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->clear()V

    .line 1352
    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-direct {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 1353
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addSearchPoweredByGoogleHeader()V

    .line 1354
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItem()V

    .line 1357
    :cond_1
    const v2, 0x7f080304

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1359
    .local v1, "title":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    if-eqz v2, :cond_2

    .line 1360
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1363
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1364
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1365
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1366
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1367
    :cond_3
    const v2, 0x7f08031e

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/ui/search/SearchActivity$24;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$24;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1375
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private varargs showNoResultsFoundDialogIfAllAreEmpty([Ljava/util/List;)V
    .locals 4
    .param p1, "lists"    # [Ljava/util/List;

    .prologue
    .line 1331
    if-eqz p1, :cond_0

    array-length v1, p1

    if-gtz v1, :cond_2

    .line 1332
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showNoResultsFoundDialog()V

    .line 1343
    :cond_1
    :goto_0
    return-void

    .line 1336
    :cond_2
    array-length v2, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v0, p1, v1

    .line 1337
    .local v0, "list":Ljava/util/List;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1336
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1342
    .end local v0    # "list":Ljava/util/List;
    :cond_4
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showNoResultsFoundDialog()V

    goto :goto_0
.end method

.method public static startSearchActivityFor(Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;ZLandroid/app/Activity;)V
    .locals 3
    .param p0, "searchType"    # Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;
    .param p1, "searchMic"    # Z
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1417
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1418
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "search_type"

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1419
    const-string v1, "speech_recognition"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1420
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->getCode()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1421
    return-void
.end method

.method private startVoiceRecognitionActivity()V
    .locals 4

    .prologue
    .line 336
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 339
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "calling_package"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    const-string v2, "android.speech.extra.PROMPT"

    const v3, 0x7f0803ee

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v2, "android.speech.extra.LANGUAGE_MODEL"

    const-string v3, "free_form"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const-string v2, "android.speech.extra.MAX_RESULTS"

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 353
    const/16 v2, 0x2a

    :try_start_0
    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    :goto_0
    return-void

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "com.google.android.googlequicksearchbox"

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->openMarketAppFor(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stopLocationUpdates()V
    .locals 2

    .prologue
    .line 1013
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->requestedLocationUpdates:Z

    if-eqz v0, :cond_0

    .line 1014
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/location/FusedLocationProviderApi;->removeLocationUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationListener;)Lcom/google/android/gms/common/api/PendingResult;

    .line 1015
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->requestedLocationUpdates:Z

    .line 1017
    :cond_0
    return-void
.end method

.method private unselectCurrentMarker()V
    .locals 2

    .prologue
    .line 1477
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->previousSelectedMarker:Lcom/google/android/gms/maps/model/Marker;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->updatePin(Lcom/google/android/gms/maps/model/Marker;Z)Ljava/lang/Integer;

    .line 1478
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->previousSelectedMarker:Lcom/google/android/gms/maps/model/Marker;

    .line 1479
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showInstructionalCard()V

    .line 1480
    return-void
.end method

.method private updatePin(Lcom/google/android/gms/maps/model/Marker;Z)Ljava/lang/Integer;
    .locals 6
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "isSelected"    # Z
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1490
    if-nez p1, :cond_0

    move-object v1, v3

    .line 1511
    :goto_0
    return-object v1

    .line 1493
    :cond_0
    const/4 v1, 0x0

    .line 1494
    .local v1, "destinationIndex":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v4, :cond_2

    .line 1495
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getDestinationIndexForMarker(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 1496
    if-nez v1, :cond_1

    move-object v1, v3

    .line 1497
    goto :goto_0

    .line 1499
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getDestinationAt(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 1500
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_2

    .line 1502
    if-eqz p2, :cond_3

    .line 1503
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset()I

    move-result v2

    .line 1507
    .local v2, "pinAsset":I
    :goto_1
    invoke-static {p1, v2}, Lcom/navdy/client/app/framework/map/MapUtils;->setMarkerImageResource(Lcom/google/android/gms/maps/model/Marker;I)V

    .line 1510
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "pinAsset":I
    :cond_2
    if-eqz p2, :cond_4

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {p1, v3}, Lcom/google/android/gms/maps/model/Marker;->setZIndex(F)V

    goto :goto_0

    .line 1505
    .restart local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_3
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getUnselectedPinAsset()I

    move-result v2

    .restart local v2    # "pinAsset":I
    goto :goto_1

    .line 1510
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "pinAsset":I
    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method private updateRecyclerViewForServiceSearch(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 947
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$17;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$17;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 961
    return-void
.end method

.method private updateRecyclerViewWithSearchResults(Lcom/navdy/client/app/framework/search/SearchResults;Z)V
    .locals 1
    .param p1, "searchResults"    # Lcom/navdy/client/app/framework/search/SearchResults;
    .param p2, "showNoResultsFoundDialog"    # Z

    .prologue
    .line 793
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$13;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/ui/search/SearchActivity$13;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;Z)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 823
    return-void
.end method

.method private updateRightButton()V
    .locals 2

    .prologue
    .line 326
    const v1, 0x7f10036e

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 327
    .local v0, "rightButton":Landroid/widget/ImageButton;
    if-eqz v0, :cond_0

    .line 328
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->rightButtonIsMic:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0201b8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 330
    :cond_0
    return-void

    .line 328
    :cond_1
    const v1, 0x7f0201b7

    goto :goto_0
.end method


# virtual methods
.method public centerOnDriver(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->centerMapOnMarkers(Z)V

    .line 1539
    return-void
.end method

.method public handleReachabilityStateChange(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;)V
    .locals 3
    .param p1, "reachabilityEvent"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1579
    if-eqz p1, :cond_1

    .line 1580
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reachabilityEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;->isReachable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1581
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1582
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    .line 1584
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateOfflineBannerVisibility()V

    .line 1585
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->applyListOrDropDownLook()V

    .line 1587
    :cond_1
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 1390
    const/4 v2, 0x1

    if-ne p2, v2, :cond_1

    .line 1391
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->finish()V

    .line 1403
    :cond_0
    :goto_0
    return-void

    .line 1392
    :cond_1
    const/16 v2, 0x2a

    if-ne p1, v2, :cond_0

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 1394
    const-string v2, "android.speech.extra.RESULTS"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1395
    .local v0, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1396
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1397
    .local v1, "query":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->setEditTextWithoutTriggeringAutocomplete(Ljava/lang/String;)V

    .line 1398
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runTextSearchOrAskHud(Ljava/lang/String;)Z

    goto :goto_0

    .line 1400
    .end local v1    # "query":Ljava/lang/String;
    :cond_2
    const v2, 0x7f0804d6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 275
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onBackPressed()V

    .line 279
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->resetEditText()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v11, 0x7f1002f5

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1066
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v4

    .line 1067
    .local v4, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1068
    .local v2, "position":I
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "item clicked at position: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1070
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .line 1071
    invoke-virtual {v6, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getDestinationAt(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 1073
    .local v0, "d":Lcom/navdy/client/app/framework/models/Destination;
    :goto_0
    if-nez v0, :cond_2

    .line 1074
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to retrieve a destination from position: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1170
    :cond_0
    :goto_1
    return-void

    .line 1071
    .end local v0    # "d":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1077
    .restart local v0    # "d":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 1082
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_HISTORY:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-ne v6, v7, :cond_4

    .line 1083
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    if-ne v6, v11, :cond_3

    .line 1084
    new-instance v6, Lcom/navdy/client/app/ui/search/SearchActivity$18;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$18;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    new-array v7, v10, [Ljava/lang/Integer;

    iget v8, v0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 1111
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/ui/search/SearchActivity$18;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 1113
    :cond_3
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreAutoCompleteDelay:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1114
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1115
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 1117
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->requestFocus()Z

    .line 1120
    new-instance v6, Lcom/navdy/client/app/ui/search/SearchActivity$19;

    invoke-direct {v6, p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity$19;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    new-array v7, v9, [Ljava/lang/Void;

    .line 1126
    invoke-virtual {v6, v7}, Lcom/navdy/client/app/ui/search/SearchActivity$19;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 1128
    :cond_4
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-ne v6, v7, :cond_5

    iget-object v6, v0, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 1129
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1130
    sget-object v6, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-direct {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 1131
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreNextKeystrokeKey:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1132
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1133
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    iget-object v7, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 1134
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 1135
    iget-object v6, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->runTextSearchOrAskHud(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 1136
    :cond_5
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->MORE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-ne v6, v7, :cond_6

    .line 1137
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1138
    .local v3, "query":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->runTextSearchOrAskHud(Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 1139
    .end local v3    # "query":Ljava/lang/String;
    :cond_6
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT_ADDRESS_SELECTION:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-ne v6, v7, :cond_7

    .line 1140
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v6, :cond_0

    .line 1141
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v6, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getItemAt(I)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    move-result-object v5

    .line 1142
    .local v5, "si":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    if-eqz v5, :cond_0

    iget-object v6, v5, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->contact:Lcom/navdy/client/app/framework/models/ContactModel;

    if-eqz v6, :cond_0

    .line 1145
    sget-object v6, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-direct {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->setCurrentMode(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;)V

    .line 1146
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->clearAndApplyListOrDropDownLook()V

    .line 1147
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iget-object v7, v5, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->contact:Lcom/navdy/client/app/framework/models/ContactModel;

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithContact(Lcom/navdy/client/app/framework/models/ContactModel;)V

    goto/16 :goto_1

    .line 1149
    .end local v5    # "si":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    if-eq v6, v11, :cond_8

    iget v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->isFavoriteSearch(I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1150
    :cond_8
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v6

    if-nez v6, :cond_9

    sget-boolean v6, Lcom/navdy/client/app/ui/search/SearchActivity;->hasDisplayedOfflineMessage:Z

    if-nez v6, :cond_9

    iget v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchType:I

    .line 1151
    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->isFavoriteSearch(I)Z

    move-result v6

    if-nez v6, :cond_9

    .line 1152
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->createOfflineDialogForOnClick(Landroid/view/View;)Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->show()V

    goto/16 :goto_1

    .line 1153
    :cond_9
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-ne v6, v7, :cond_a

    .line 1155
    const-string v6, "Search_Autocomplete"

    invoke-virtual {v4, v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 1156
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setResultForAutocompleteAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V

    goto/16 :goto_1

    .line 1157
    :cond_a
    iget v6, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v7, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v7

    if-ne v6, v7, :cond_b

    .line 1158
    const-string v6, "Search_Contact"

    invoke-virtual {v4, v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 1159
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setResultForContactAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V

    goto/16 :goto_1

    .line 1161
    :cond_b
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->putSearchResultExtraAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V

    goto/16 :goto_1

    .line 1165
    :cond_c
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1166
    .local v1, "i":Landroid/content/Intent;
    const-string v6, "search_result"

    invoke-virtual {v1, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1167
    const-string v6, "location"

    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1168
    invoke-virtual {p0, v1, v9}, Lcom/navdy/client/app/ui/search/SearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1033
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "GoogleApiClient connected."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1034
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1035
    .local v0, "context":Landroid/content/Context;
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 1037
    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 1039
    sget-object v1, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2}, Lcom/google/android/gms/location/FusedLocationProviderApi;->getLastLocation(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/location/Location;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    .line 1042
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    if-nez v1, :cond_1

    .line 1046
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "lastLocation was null. Now requesting location update."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1047
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->requestLocationUpdate()V

    .line 1049
    :cond_1
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConnectionFailed: ConnectionResult.getErrorCode() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1054
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1053
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1055
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "GoogleApiClient connection suspended."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1061
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 159
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 160
    const v6, 0x7f0300e4

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->setContentView(I)V

    .line 161
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 164
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->checkServices()V

    .line 166
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setupToolbarButtons()V

    .line 168
    const v6, 0x7f100359

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v7/widget/RecyclerView;

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchRecycler:Landroid/support/v7/widget/RecyclerView;

    .line 169
    const v6, 0x7f100146

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->card:Landroid/widget/RelativeLayout;

    .line 170
    const v6, 0x7f1000aa

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    .line 171
    const v6, 0x7f1000e4

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fab:Landroid/widget/ImageButton;

    .line 172
    const v6, 0x7f1000ae

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->centerMap:Landroid/widget/ImageButton;

    .line 173
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const v9, 0x7f100147

    .line 174
    invoke-virtual {v6, v9}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 176
    new-instance v6, Lcom/navdy/client/app/ui/search/SearchActivity$1;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$1;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    new-instance v9, Lcom/navdy/client/app/ui/search/SearchActivity$2;

    invoke-direct {v9, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$2;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {p0, v6, v9}, Lcom/navdy/client/app/ui/search/SearchActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 198
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setRecyclerView()V

    .line 201
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setEditTextListener()V

    .line 202
    new-instance v6, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->handler:Landroid/os/Handler;

    invoke-direct {v6, p0, v9}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    iput-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->googlePlacesSearch:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .line 205
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->setHintText()V

    .line 207
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 208
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "speech_recognition"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 209
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->startVoiceRecognitionActivity()V

    .line 239
    :cond_0
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v6

    const-string v7, "Search"

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setSourceValue(Ljava/lang/String;)V

    .line 241
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showAutoComplete()V

    .line 251
    const v6, 0x7f10035b

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 252
    .local v5, "v":Landroid/view/View;
    new-instance v6, Lcom/navdy/client/app/ui/search/SearchActivity$3;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/search/SearchActivity$3;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 262
    return-void

    .line 211
    .end local v5    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "action":Ljava/lang/String;
    const-string v6, "com.google.android.gms.actions.SEARCH_ACTION"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 214
    .local v3, "isGoogleNowSearch":Z
    const-string v6, "ACTION_SEND"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fromActionSendIntent:Z

    .line 215
    const-string v6, "com.navdy.client.actions.SEARCH_ACTION"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 216
    .local v2, "isDestinationFinderSearch":Z
    if-nez v3, :cond_2

    if-eqz v2, :cond_7

    :cond_2
    move v6, v8

    :goto_1
    iput-boolean v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    .line 218
    if-eqz v3, :cond_3

    .line 219
    const-string v6, "Search_Using_Google_Now"

    invoke-static {v6}, Lcom/navdy/client/app/framework/LocalyticsManager;->tagEvent(Ljava/lang/String;)V

    .line 221
    :cond_3
    if-eqz v2, :cond_4

    .line 222
    const-string v6, "Search_Using_Destination_Finder"

    invoke-static {v6}, Lcom/navdy/client/app/framework/LocalyticsManager;->tagEvent(Ljava/lang/String;)V

    .line 225
    :cond_4
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Search is coming from Google Now = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v9, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 226
    iget-boolean v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    if-nez v6, :cond_5

    iget-boolean v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->fromActionSendIntent:Z

    if-eqz v6, :cond_0

    :cond_5
    const-string v6, "query"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 227
    const-string v6, "query"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 228
    .local v4, "query":Ljava/lang/String;
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 229
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 230
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    if-eqz v6, :cond_6

    .line 231
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreNextKeystrokeKey:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 232
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v6, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 234
    :cond_6
    invoke-direct {p0, v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->runTextSearchOrAskHud(Ljava/lang/String;)Z

    goto/16 :goto_0

    .end local v4    # "query":Ljava/lang/String;
    :cond_7
    move v6, v7

    .line 216
    goto :goto_1
.end method

.method public onFabClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1427
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isShowingMap:Z

    if-nez v0, :cond_0

    .line 1428
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showMap()V

    .line 1432
    :goto_0
    return-void

    .line 1430
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showList()V

    goto :goto_0
.end method

.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    const/4 v7, 0x0

    .line 885
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onGoogleSearchResult: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 887
    if-nez p1, :cond_0

    .line 888
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-direct {p1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 891
    .restart local p1    # "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v3

    .line 893
    .local v3, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    sget-object v4, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    if-ne p2, v4, :cond_1

    .line 894
    const-string v4, "Search_Result"

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 897
    :cond_1
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchActivity$26;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    invoke-virtual {p2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 938
    :cond_2
    :goto_0
    return-void

    .line 901
    :pswitch_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->isComingFromGoogleNow:Z

    if-nez v4, :cond_3

    .line 902
    move-object v1, p1

    .line 903
    .local v1, "finalDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v4

    new-instance v5, Lcom/navdy/client/app/ui/search/SearchActivity$16;

    invoke-direct {v5, p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity$16;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/util/List;)V

    const/4 v6, 0x1

    sget-object v7, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {v4, v5, v6, v7}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;

    .line 913
    .end local v1    # "finalDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :cond_3
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateRecyclerViewForServiceSearch(Ljava/util/List;)V

    .line 914
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateNearbySearchResultsWithKnownDestinationData()V

    .line 915
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    goto :goto_0

    .line 918
    :pswitch_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "successfully received detail callback from web service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 919
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 920
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 921
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 922
    .local v2, "result":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    if-eqz v2, :cond_4

    .line 923
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2, v4, v0}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 925
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->persistPlaceDetailInfo()I

    .line 926
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->updateDestinationListsAsync()V

    .line 931
    .end local v2    # "result":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_4
    :goto_1
    if-eqz v0, :cond_2

    .line 932
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->putSearchResultExtraAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0

    .line 929
    :cond_5
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->requestedPlaceDetailsDestinationBackup:Lcom/navdy/client/app/framework/models/Destination;

    goto :goto_1

    .line 897
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 1023
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Location changed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1024
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->lastLocation:Landroid/location/Location;

    .line 1025
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->stopLocationUpdates()V

    .line 1027
    :cond_0
    return-void
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1607
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 1608
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 268
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updateOfflineBannerVisibility()V

    .line 269
    const-string v0, "Search"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 270
    return-void
.end method

.method public onServiceClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1173
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "User clicked on a service search."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1174
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 1175
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->ignoreNextKeystrokeKey:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1176
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v0

    .line 1177
    .local v0, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    const-string v1, "Search_Quick"

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 1179
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1197
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown service view in onServiceClick: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1201
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 1202
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 1203
    return-void

    .line 1181
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v2, 0x7f0801d5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(I)V

    .line 1182
    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->GAS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    goto :goto_0

    .line 1185
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v2, 0x7f08032f

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(I)V

    .line 1186
    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->PARKING:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    goto :goto_0

    .line 1189
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v2, 0x7f0801d2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(I)V

    .line 1190
    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->FOOD:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    goto :goto_0

    .line 1193
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->editText:Landroid/widget/EditText;

    const v2, 0x7f0800c5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(I)V

    .line 1194
    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ATM:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->runServiceSearch(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    goto :goto_0

    .line 1179
    nop

    :pswitch_data_0
    .packed-switch 0x7f100368
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public updateOfflineBannerVisibility()V
    .locals 3

    .prologue
    .line 1590
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 1591
    .local v0, "canReachInternet":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->offlineBanner:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1593
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->updatePoweredByGoogleHeaderVisibility(Z)V

    .line 1594
    return-void

    .line 1591
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updatePoweredByGoogleHeaderVisibility(Z)V
    .locals 1
    .param p1, "canReachInternet"    # Z

    .prologue
    .line 1597
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    if-eqz v0, :cond_0

    .line 1598
    if-eqz p1, :cond_1

    .line 1599
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addSearchPoweredByGoogleHeader()V

    .line 1604
    :cond_0
    :goto_0
    return-void

    .line 1601
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity;->searchAdapter:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->removeSearchPoweredByGoogleHeader()V

    goto :goto_0
.end method
