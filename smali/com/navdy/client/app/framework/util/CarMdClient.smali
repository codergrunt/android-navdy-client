.class public Lcom/navdy/client/app/framework/util/CarMdClient;
.super Ljava/lang/Object;
.source "CarMdClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final ACCESS_IMAGE_URL:Ljava/lang/String; = "accessImageURL"

.field public static final ACCESS_NOTES:Ljava/lang/String; = "accessNotes"

.field private static final CAR_MD_API_URL:Lokhttp3/HttpUrl;

.field private static final CAR_MD_OBD_API_URL:Lokhttp3/HttpUrl;

.field private static final CONNECTION_TIMEOUT_IN_SECONDS:I = 0x1e

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final LOCATION_IMAGE_URL:Ljava/lang/String; = "locationImageURL"

.field public static final NOTES:Ljava/lang/String; = "notes"

.field private static instance:Lcom/navdy/client/app/framework/util/CarMdClient;

.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final CAR_MD_AUTH:Ljava/lang/String;

.field private final CAR_MD_TOKEN:Ljava/lang/String;

.field private httpClient:Lokhttp3/OkHttpClient;

.field mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-string v0, "https://api2.carmd.com/v2.0/decode"

    invoke-static {v0}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_API_URL:Lokhttp3/HttpUrl;

    .line 39
    const-string v0, "http://api2.carmd.com/v2.0/articles/dlclocationbyymm"

    invoke-static {v0}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_OBD_API_URL:Lokhttp3/HttpUrl;

    .line 51
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/CarMdClient;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->instance:Lcom/navdy/client/app/framework/util/CarMdClient;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1e

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    .line 56
    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->httpClient:Lokhttp3/OkHttpClient;

    .line 59
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 60
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    invoke-interface {v0}, Lcom/navdy/service/library/network/http/IHttpManager;->getClientCopy()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 61
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->retryOnConnectionFailure(Z)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 62
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 63
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->httpClient:Lokhttp3/OkHttpClient;

    .line 65
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080553

    .line 66
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_AUTH:Ljava/lang/String;

    .line 67
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080554

    .line 68
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_TOKEN:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/util/CarMdClient;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->instance:Lcom/navdy/client/app/framework/util/CarMdClient;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/navdy/client/app/framework/util/CarMdClient;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/CarMdClient;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->instance:Lcom/navdy/client/app/framework/util/CarMdClient;

    .line 75
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/util/CarMdClient;->instance:Lcom/navdy/client/app/framework/util/CarMdClient;

    return-object v0
.end method


# virtual methods
.method public buildCarMdRequest(Lokhttp3/HttpUrl;Ljava/util/ArrayList;)Lokhttp3/Request;
    .locals 6
    .param p1, "requestUrl"    # Lokhttp3/HttpUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/HttpUrl;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lokhttp3/Request;"
        }
    .end annotation

    .prologue
    .line 216
    .local p2, "queryParams":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p2, :cond_2

    .line 217
    invoke-virtual {p1}, Lokhttp3/HttpUrl;->newBuilder()Lokhttp3/HttpUrl$Builder;

    move-result-object v0

    .line 218
    .local v0, "builder":Lokhttp3/HttpUrl$Builder;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 219
    .local v1, "param":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/CharSequence;

    .line 220
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/CharSequence;

    .line 221
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 222
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lokhttp3/HttpUrl$Builder;->addQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    goto :goto_0

    .line 225
    .end local v1    # "param":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v0}, Lokhttp3/HttpUrl$Builder;->build()Lokhttp3/HttpUrl;

    move-result-object p1

    .line 228
    .end local v0    # "builder":Lokhttp3/HttpUrl$Builder;
    :cond_2
    new-instance v3, Lokhttp3/Request$Builder;

    invoke-direct {v3}, Lokhttp3/Request$Builder;-><init>()V

    invoke-virtual {v3, p1}, Lokhttp3/Request$Builder;->url(Lokhttp3/HttpUrl;)Lokhttp3/Request$Builder;

    move-result-object v2

    .line 229
    .local v2, "requestBuilder":Lokhttp3/Request$Builder;
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_AUTH:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 230
    const-string v3, "authorization"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Basic "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_AUTH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 234
    :goto_1
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_TOKEN:Ljava/lang/String;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 235
    const-string v3, "partner-token"

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_TOKEN:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 239
    :goto_2
    invoke-virtual {v2}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v3

    return-object v3

    .line 232
    :cond_3
    sget-object v3, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Missing car md auth credential !"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 237
    :cond_4
    sget-object v3, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Missing car md token credential !"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getListFromJsonResponse(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "jsonString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 121
    const/4 v5, 0x0

    .line 123
    .local v5, "results":Lorg/json/JSONArray;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 124
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v6, "data"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 129
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_0
    if-eqz v5, :cond_1

    .line 130
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 131
    .local v4, "makeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 132
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v3

    .line 133
    .local v3, "make":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 134
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    .end local v1    # "i":I
    .end local v3    # "make":Ljava/lang/String;
    .end local v4    # "makeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to parse car info json response: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 140
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v5    # "results":Lorg/json/JSONArray;
    :cond_1
    const/4 v4, 0x0

    :cond_2
    return-object v4
.end method

.method public getMakes(Lcom/navdy/client/app/framework/util/CarMdCallBack;)V
    .locals 3
    .param p1, "callback"    # Lcom/navdy/client/app/framework/util/CarMdCallBack;

    .prologue
    .line 86
    sget-object v1, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_API_URL:Lokhttp3/HttpUrl;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/framework/util/CarMdClient;->buildCarMdRequest(Lokhttp3/HttpUrl;Ljava/util/ArrayList;)Lokhttp3/Request;

    move-result-object v0

    .line 87
    .local v0, "request":Lokhttp3/Request;
    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/framework/util/CarMdClient;->sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V

    .line 88
    return-void
.end method

.method public getModels(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/util/CarMdCallBack;)V
    .locals 4
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "year"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/navdy/client/app/framework/util/CarMdCallBack;

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    .local v0, "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v2, Landroid/util/Pair;

    const-string v3, "make"

    invoke-direct {v2, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v2, Landroid/util/Pair;

    const-string v3, "year"

    invoke-direct {v2, v3, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    sget-object v2, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_API_URL:Lokhttp3/HttpUrl;

    invoke-virtual {p0, v2, v0}, Lcom/navdy/client/app/framework/util/CarMdClient;->buildCarMdRequest(Lokhttp3/HttpUrl;Ljava/util/ArrayList;)Lokhttp3/Request;

    move-result-object v1

    .line 116
    .local v1, "request":Lokhttp3/Request;
    invoke-virtual {p0, p3, v1}, Lcom/navdy/client/app/framework/util/CarMdClient;->sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V

    .line 117
    return-void
.end method

.method public getObdLocation(Lcom/navdy/client/app/framework/util/CarMdCallBack;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "callback"    # Lcom/navdy/client/app/framework/util/CarMdCallBack;
    .param p2, "make"    # Ljava/lang/String;
    .param p3, "year"    # Ljava/lang/String;
    .param p4, "model"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    .line 151
    if-eqz p1, :cond_0

    .line 152
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    invoke-static {p3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 154
    invoke-static {p4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 155
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "getObdLocation: One of the parameters is null! callback = %s, make = %s year = %s model = %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x2

    aput-object p3, v4, v5

    aput-object p4, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 159
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 160
    .local v0, "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v2, Landroid/util/Pair;

    const-string v3, "make"

    invoke-direct {v2, v3, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v2, Landroid/util/Pair;

    const-string v3, "year"

    invoke-direct {v2, v3, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    new-instance v2, Landroid/util/Pair;

    const-string v3, "model"

    invoke-direct {v2, v3, p4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v2, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_OBD_API_URL:Lokhttp3/HttpUrl;

    invoke-virtual {p0, v2, v0}, Lcom/navdy/client/app/framework/util/CarMdClient;->buildCarMdRequest(Lokhttp3/HttpUrl;Ljava/util/ArrayList;)Lokhttp3/Request;

    move-result-object v1

    .line 164
    .local v1, "request":Lokhttp3/Request;
    invoke-virtual {p0, p1, v1}, Lcom/navdy/client/app/framework/util/CarMdClient;->sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V

    goto :goto_0
.end method

.method public getYears(Ljava/lang/String;Lcom/navdy/client/app/framework/util/CarMdCallBack;)V
    .locals 4
    .param p1, "make"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/navdy/client/app/framework/util/CarMdCallBack;

    .prologue
    .line 98
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 99
    .local v0, "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v2, Landroid/util/Pair;

    const-string v3, "make"

    invoke-direct {v2, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v2, Lcom/navdy/client/app/framework/util/CarMdClient;->CAR_MD_API_URL:Lokhttp3/HttpUrl;

    invoke-virtual {p0, v2, v0}, Lcom/navdy/client/app/framework/util/CarMdClient;->buildCarMdRequest(Lokhttp3/HttpUrl;Ljava/util/ArrayList;)Lokhttp3/Request;

    move-result-object v1

    .line 101
    .local v1, "request":Lokhttp3/Request;
    invoke-virtual {p0, p2, v1}, Lcom/navdy/client/app/framework/util/CarMdClient;->sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V

    .line 102
    return-void
.end method

.method public parseCarMdObdLocationResponse(Lokhttp3/ResponseBody;)Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    .locals 11
    .param p1, "responseBody"    # Lokhttp3/ResponseBody;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v10, 0x8

    .line 251
    new-instance v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;

    invoke-direct {v6}, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;-><init>()V

    .line 252
    .local v6, "response":Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v3

    .line 253
    .local v3, "jsonString":Ljava/lang/String;
    sget-object v7, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Car MD: response: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 255
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 256
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v7, "data"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 257
    .local v1, "data":Lorg/json/JSONObject;
    const-string v7, "accessImageURL"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->accessImageURL:Ljava/lang/String;

    .line 258
    const-string v7, "locationImageURL"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 259
    .local v4, "locationImageURL":Ljava/lang/String;
    const-string v7, "notes"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 260
    .local v5, "notes":Ljava/lang/String;
    const-string v7, "accessNotes"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "accessNotes":Ljava/lang/String;
    iput-object v5, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->note:Ljava/lang/String;

    .line 263
    iput-object v0, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->accessNote:Ljava/lang/String;

    .line 264
    const/4 v7, -0x1

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    .line 265
    const-string v7, "dlc/position/1.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 266
    const/4 v7, 0x0

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    .line 285
    :cond_0
    :goto_0
    iget v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    if-ltz v7, :cond_1

    iget v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    if-le v7, v10, :cond_a

    .line 286
    :cond_1
    new-instance v7, Ljava/lang/Exception;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Missing Obd Location. locationImageURL = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v7

    .line 267
    :cond_2
    const-string v7, "dlc/position/2.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 268
    const/4 v7, 0x1

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 269
    :cond_3
    const-string v7, "dlc/position/3.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 270
    const/4 v7, 0x2

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 271
    :cond_4
    const-string v7, "dlc/position/4.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 272
    const/4 v7, 0x3

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 273
    :cond_5
    const-string v7, "dlc/position/5.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 274
    const/4 v7, 0x4

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 275
    :cond_6
    const-string v7, "dlc/position/6.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 276
    const/4 v7, 0x5

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 277
    :cond_7
    const-string v7, "dlc/position/7.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 278
    const/4 v7, 0x6

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 279
    :cond_8
    const-string v7, "dlc/position/8.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 280
    const/4 v7, 0x7

    iput v7, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 281
    :cond_9
    const-string v7, "dlc/position/9.jpg"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 282
    iput v10, v6, Lcom/navdy/client/app/framework/util/CarMdClient$CarMdObdLocationResponse;->locationNumber:I

    goto :goto_0

    .line 289
    :cond_a
    return-object v6
.end method

.method public sendCarMdRequest(Lcom/navdy/client/app/framework/util/CarMdCallBack;Lokhttp3/Request;)V
    .locals 4
    .param p1, "callback"    # Lcom/navdy/client/app/framework/util/CarMdCallBack;
    .param p2, "request"    # Lokhttp3/Request;

    .prologue
    .line 173
    if-nez p1, :cond_0

    .line 207
    :goto_0
    return-void

    .line 177
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/util/CarMdClient;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calling car MD: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/CarMdClient;->httpClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v1, p2}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/framework/util/CarMdClient$1;

    invoke-direct {v2, p0, p1}, Lcom/navdy/client/app/framework/util/CarMdClient$1;-><init>(Lcom/navdy/client/app/framework/util/CarMdClient;Lcom/navdy/client/app/framework/util/CarMdCallBack;)V

    invoke-interface {v1, v2}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p1, v0}, Lcom/navdy/client/app/framework/util/CarMdCallBack;->processFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
