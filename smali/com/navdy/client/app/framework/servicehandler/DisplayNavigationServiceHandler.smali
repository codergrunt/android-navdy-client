.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$HUDReadyEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$ArrivedTripEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;,
        Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final singleton:Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation
.end field


# instance fields
.field private final bus:Lcom/squareup/otto/Bus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 155
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    .line 159
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 181
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationSessionRouteChange(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationRouteStatus(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationRouteResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationSessionDeferRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->handleNavigationSessionResponse(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V

    return-void
.end method

.method static synthetic access$800()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;)Lcom/squareup/otto/Bus;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->bus:Lcom/squareup/otto/Bus;

    return-object v0
.end method

.method private getDestination(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 3
    .param p1, "navigationRouteResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p2, "destinationId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 444
    const/4 v0, 0x0

    .line 446
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz p2, :cond_0

    .line 447
    invoke-static {p2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 452
    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 453
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-direct {v0, p1}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V

    .line 454
    .restart local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 455
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    .line 456
    .local v1, "destinationFromNavRouteResult":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v1, :cond_1

    .line 457
    move-object v0, v1

    .line 462
    .end local v1    # "destinationFromNavRouteResult":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    .locals 2

    .prologue
    .line 169
    const-class v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private handleNavigationRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V
    .locals 3
    .param p1, "navigationRouteCancelRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;

    .prologue
    const/4 v2, 0x0

    .line 430
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;->handle:Ljava/lang/String;

    invoke-direct {v0, v2, v2, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    .line 431
    return-void
.end method

.method private handleNavigationRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 7
    .param p1, "navigationRouteRequest"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 272
    if-eqz p1, :cond_2

    .line 273
    const/4 v0, 0x0

    .line 274
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    .line 276
    .local v1, "destinationId":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 278
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 279
    .local v3, "id":I
    invoke-static {v3}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 285
    .end local v3    # "id":I
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v4, :cond_1

    .line 286
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-direct {v0, v4}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Lcom/navdy/service/library/events/destination/Destination;)V

    .line 289
    .restart local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    if-nez v0, :cond_3

    .line 290
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "handleNavigationRouteRequest, destination is null, no-op"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 299
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v1    # "destinationId":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 280
    .restart local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .restart local v1    # "destinationId":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 281
    .local v2, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to look up the destination for id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 294
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    new-instance v4, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-direct {v4, v5, v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;-><init>(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-direct {p0, v4}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private handleNavigationRouteResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 4
    .param p1, "navigationRouteResponse"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .prologue
    const/4 v3, 0x0

    .line 389
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$10;->$SwitchMap$com$navdy$service$library$events$RequestStatus:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 412
    :goto_0
    return-void

    .line 391
    :pswitch_0
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;-><init>(Ljava/util/List;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 399
    :pswitch_1
    const/4 v0, 0x0

    .line 401
    .local v0, "cancelHandle":Ljava/lang/String;
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_0

    .line 402
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    .line 404
    :cond_0
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v1, v2, v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 407
    .end local v0    # "cancelHandle":Ljava/lang/String;
    :pswitch_2
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleNavigationRouteStatus(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;)V
    .locals 4
    .param p1, "navigationRouteStatus"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    .prologue
    .line 382
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    .line 383
    return-void
.end method

.method private handleNavigationSessionDeferRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;)V
    .locals 6
    .param p1, "navigationSessionDeferRequest"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;

    .prologue
    .line 418
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->requestId:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->routeId:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->delay:Ljava/lang/Integer;

    .line 421
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;->expireTimestamp:Ljava/lang/Long;

    .line 422
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DeferStartActiveTripEvent;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 418
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    .line 424
    return-void
.end method

.method private handleNavigationSessionResponse(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V
    .locals 4
    .param p1, "navigationSessionResponse"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    .prologue
    const/4 v3, 0x0

    .line 434
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "navigationSessionResponse:\n\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 435
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-ne v0, v1, :cond_0

    .line 436
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    invoke-direct {v0, v3, v1, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    .line 438
    :cond_0
    return-void
.end method

.method private handleNavigationSessionRouteChange(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V
    .locals 4
    .param p1, "navigationSessionRouteChange"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    .prologue
    .line 306
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    if-nez v1, :cond_1

    .line 307
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "handleNavigationSessionRouteChange called with a null navigationSessionRouteChange or the newRoute is null."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 318
    :goto_0
    return-void

    .line 311
    :cond_1
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->getDestination(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 313
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v3, v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 4
    .param p1, "navigationSessionStatusEvent"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    .prologue
    const/4 v3, 0x0

    .line 326
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$10;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 363
    :goto_0
    return-void

    .line 328
    :pswitch_0
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->getDestination(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 333
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 340
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :pswitch_1
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->getDestination(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 345
    .restart local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 352
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :pswitch_2
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$ArrivedTripEvent;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$ArrivedTripEvent;-><init>()V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 355
    :pswitch_3
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    invoke-direct {v1, v3, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 358
    :pswitch_4
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$HUDReadyEvent;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$HUDReadyEvent;-><init>()V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->postWhenReady(Ljava/lang/Object;)V

    goto :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private postWhenReady(Ljava/lang/Object;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/Object;

    .prologue
    .line 366
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$9;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 376
    return-void
.end method


# virtual methods
.method public onNavigationRouteCancelRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 247
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$7;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteCancelRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 253
    return-void
.end method

.method public onNavigationRouteRequest(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 187
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 193
    return-void
.end method

.method public onNavigationRouteResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 227
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$5;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 233
    return-void
.end method

.method public onNavigationRouteStatus(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 217
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$4;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 223
    return-void
.end method

.method public onNavigationSessionDeferRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 237
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$6;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionDeferRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 243
    return-void
.end method

.method public onNavigationSessionResponse(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 257
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$8;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$8;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 263
    return-void
.end method

.method public onNavigationSessionRouteChange(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 197
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 203
    return-void
.end method

.method public onNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 207
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$3;-><init>(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 213
    return-void
.end method
