.class public Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
.super Ljava/lang/Object;
.source "NetworkStatusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    }
.end annotation


# static fields
.field public static final ENABLE_REACHABILITY_CHECK:Z = false

.field private static final ENDPOINT_CONNECTION_TIMEOUT:I = 0x7530

.field private static final FAIL_PING_COUNT:I = 0x2

.field private static final FAIL_PING_TIMEOUT:I = 0x7530

.field private static final FAST_FAIL_PING_TIMEOUT:I = 0x7d0

.field public static final NETWORK_CHECK_RETRY_INTERVAL:I = 0x3a98

.field private static final PING_URL:Ljava/lang/String; = "http://www.google.com"

.field public static final REACHABILITY_CHECK_INITIAL_DELAY:I = 0x1388

.field private static final SUCCESS_PING_TIMEOUT:I = 0x1d4c0

.field private static final VERBOSE:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private context:Landroid/content/Context;

.field private handler:Landroid/os/Handler;

.field private isNetworkReachable:Z

.field private lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

.field mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private makeNetworkCall:Ljava/lang/Runnable;

.field private networkCheck:Ljava/lang/Runnable;

.field private reachableCheck:Ljava/lang/Runnable;

.field private receiver:Landroid/content/BroadcastReceiver;

.field private registered:Z

.field private remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

.field private retryCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    .line 75
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->reachableCheck:Ljava/lang/Runnable;

    .line 87
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->networkCheck:Ljava/lang/Runnable;

    .line 95
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;-><init>(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->makeNetworkCall:Ljava/lang/Runnable;

    .line 180
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$4;-><init>(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->receiver:Landroid/content/BroadcastReceiver;

    .line 176
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    .line 177
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 178
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->makeNetworkCall:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sendEvent()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->retryCount:I

    return v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->retryCount:I

    return p1
.end method

.method static synthetic access$404(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->retryCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isNetworkReachable:Z

    return v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isNetworkReachable:Z

    return p1
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->reachableCheck:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private cleanState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 336
    iput v2, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->retryCount:I

    .line 337
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "stop reachable check"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->reachableCheck:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 339
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isNetworkReachable:Z

    .line 340
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 341
    return-void
.end method

.method private getNetworkStateChange()Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 301
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    .line 302
    .local v1, "connected":Z
    if-eqz v1, :cond_1

    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToCellNetwork(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v3

    .line 303
    .local v0, "cell":Z
    :goto_0
    if-eqz v1, :cond_2

    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 304
    .local v3, "wifi":Z
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->canReachInternet()Z

    move-result v2

    .line 306
    .local v2, "isReachable":Z
    if-eqz v1, :cond_0

    if-nez v2, :cond_0

    .line 307
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "connected but not reachable"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 308
    const/4 v1, 0x0

    .line 311
    :cond_0
    new-instance v4, Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 312
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 313
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 314
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 315
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/navdy/service/library/events/settings/NetworkStateChange;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-object v4

    .end local v0    # "cell":Z
    .end local v2    # "isReachable":Z
    .end local v3    # "wifi":Z
    :cond_1
    move v0, v4

    .line 302
    goto :goto_0

    .restart local v0    # "cell":Z
    :cond_2
    move v3, v4

    .line 303
    goto :goto_1
.end method

.method private isBooleanEquals(Ljava/lang/Boolean;Ljava/lang/Boolean;)Z
    .locals 2
    .param p1, "left"    # Ljava/lang/Boolean;
    .param p2, "right"    # Ljava/lang/Boolean;

    .prologue
    .line 328
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNetworkStateEquals(Lcom/navdy/service/library/events/settings/NetworkStateChange;Lcom/navdy/service/library/events/settings/NetworkStateChange;)Z
    .locals 2
    .param p1, "left"    # Lcom/navdy/service/library/events/settings/NetworkStateChange;
    .param p2, "right"    # Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .prologue
    .line 319
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    iget-object v1, p2, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    .line 321
    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isBooleanEquals(Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    iget-object v1, p2, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    .line 322
    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isBooleanEquals(Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    iget-object v1, p2, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    .line 323
    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isBooleanEquals(Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    iget-object v1, p2, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    .line 324
    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isBooleanEquals(Ljava/lang/Boolean;Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendEvent()V
    .locals 6

    .prologue
    .line 275
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 276
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-nez v0, :cond_0

    .line 277
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[NetworkStateChange] hud not connected"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    .end local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :goto_0
    return-void

    .line 281
    .restart local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->getNetworkStateChange()Lcom/navdy/service/library/events/settings/NetworkStateChange;

    move-result-object v1

    .line 283
    .local v1, "status":Lcom/navdy/service/library/events/settings/NetworkStateChange;
    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    invoke-direct {p0, v1, v3}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isNetworkStateEquals(Lcom/navdy/service/library/events/settings/NetworkStateChange;Lcom/navdy/service/library/events/settings/NetworkStateChange;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 284
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[NetworkStateChange] already sent lastState["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    iget-object v5, v5, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 295
    .end local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    .end local v1    # "status":Lcom/navdy/service/library/events/settings/NetworkStateChange;
    :catch_0
    move-exception v2

    .line 296
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[NetworkStateChange]"

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 287
    .end local v2    # "t":Ljava/lang/Throwable;
    .restart local v0    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    .restart local v1    # "status":Lcom/navdy/service/library/events/settings/NetworkStateChange;
    :cond_1
    :try_start_1
    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 288
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NetworkStateChange available["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->networkAvailable:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] cell["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->cellNetwork:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] wifi["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->wifiNetwork:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] reach["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/settings/NetworkStateChange;->reachability:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 293
    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 294
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "[NetworkStateChange] status sent"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public canReachInternet()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public checkForNetwork()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 201
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "checkForNetwork: Connectivity changed, no n/w, stop reachable"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 203
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->cleanState()V

    .line 204
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sendEvent()V

    .line 205
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;

    invoke-direct {v1, v4}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->networkCheck:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 207
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->networkCheck:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 222
    :goto_0
    return v4

    .line 219
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "checkForNetwork: Connectivity changed, n/w available, Connected to internet"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 220
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sendEvent()V

    .line 221
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onRemoteDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 264
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 265
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sendEvent()V

    .line 266
    return-void
.end method

.method public onRemoteDeviceDisconnected()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 269
    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 270
    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->lastState:Lcom/navdy/service/library/events/settings/NetworkStateChange;

    .line 271
    return-void
.end method

.method public declared-synchronized register()V
    .locals 6

    .prologue
    .line 228
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->cleanState()V

    .line 230
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->registered:Z

    if-nez v1, :cond_0

    .line 231
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "filter":Landroid/content/IntentFilter;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->registered:Z

    .line 233
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "stop reachable check"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->reachableCheck:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 235
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "launch reachable check"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 237
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->isNetworkReachable:Z

    .line 248
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    monitor-exit p0

    return-void

    .line 245
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Not connected to the n/w, Check after some time"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->networkCheck:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 228
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unregister()V
    .locals 2

    .prologue
    .line 253
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->cleanState()V

    .line 255
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->registered:Z

    if-eqz v0, :cond_0

    .line 256
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->registered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    :cond_0
    monitor-exit p0

    return-void

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
