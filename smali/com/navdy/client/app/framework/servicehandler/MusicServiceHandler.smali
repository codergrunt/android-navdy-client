.class public Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
.super Ljava/lang/Object;
.source "MusicServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;,
        Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;
    }
.end annotation


# static fields
.field public static final EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

.field public static final INDEX_PLAY_LIST_RETRY_INTERVAL:I = 0x2710

.field private static final MAX_RETRIES_TO_INDEX_PLAY_LISTS:I = 0x5

.field public static final PLAY_LIST_INDEXING_DELAY_MILLIS:I = 0x1388

.field private static final THROTTLING_LOGGER_WAIT:I = 0x1388

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field private static singleton:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

.field private static final throttlingLogger:Lcom/navdy/client/app/framework/util/ThrottlingLogger;


# instance fields
.field private currentArtworkHash:Ljava/lang/String;

.field private currentMediaArtwork:Landroid/graphics/Bitmap;

.field private currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private volatile indexingRetries:I

.field private musicObserverHandler:Landroid/os/Handler;

.field private musicObserverLooper:Landroid/os/Looper;

.field private musicSeekHelper:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;

.field private postPhotoUpdates:Z

.field private retryIndexingPlayListsRunnable:Ljava/lang/Runnable;

.field private shouldPerformFullPlaylistIndex:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 80
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 83
    new-instance v0, Lcom/navdy/client/app/framework/util/ThrottlingLogger;

    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-wide/16 v2, 0x1388

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;-><init>(Lcom/navdy/service/library/log/Logger;J)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->throttlingLogger:Lcom/navdy/client/app/framework/util/ThrottlingLogger;

    .line 85
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 86
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 87
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->playbackState(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 94
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput v4, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexingRetries:I

    .line 96
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 98
    iput-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaArtwork:Landroid/graphics/Bitmap;

    .line 99
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->postPhotoUpdates:Z

    .line 101
    iput-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicSeekHelper:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;

    .line 110
    iput-boolean v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->shouldPerformFullPlaylistIndex:Z

    .line 123
    new-instance v3, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)V

    iput-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->retryIndexingPlayListsRunnable:Ljava/lang/Runnable;

    .line 133
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "MusicServiceHandler ctor"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 136
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "MusicObserver"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 137
    .local v2, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 138
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverLooper:Landroid/os/Looper;

    .line 139
    new-instance v3, Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverLooper:Landroid/os/Looper;

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverHandler:Landroid/os/Handler;

    .line 140
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)V

    .line 141
    .local v0, "gpmPlaylistObserver":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistsUri()Landroid/net/Uri;

    move-result-object v1

    .line 142
    .local v1, "gpmPlaylistsUri":Landroid/net/Uri;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    .line 143
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 144
    invoke-virtual {v3, v1, v5, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 145
    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverHandler:Landroid/os/Handler;

    new-instance v4, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$2;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)V

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 151
    return-void
.end method

.method static synthetic access$008(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    .prologue
    .line 79
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexingRetries:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexingRetries:I

    return v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexAllPlaylists(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Landroid/database/Cursor;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->createCharacterMap(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    .param p1, "x1"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexPlaylist(I)V

    return-void
.end method

.method private createCharacterMap(Landroid/database/Cursor;)Ljava/util/List;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    const/4 v1, 0x0

    .line 462
    .local v1, "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    if-eqz p1, :cond_2

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 463
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    .end local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .local v2, "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    const/4 v3, 0x0

    .line 465
    .local v3, "characterOffset":I
    const/4 v5, 0x0

    .line 466
    .local v5, "indexedNumbers":Z
    const/4 v6, 0x0

    .line 468
    .local v6, "indexedSymbols":Z
    :cond_0
    const/4 v7, 0x0

    .line 469
    .local v7, "shouldAdd":Z
    :try_start_1
    const-string v9, "letter"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, "c":Ljava/lang/String;
    const-string v9, "count"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 471
    .local v4, "count":I
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_3

    .line 472
    :cond_1
    sget-object v9, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bad value in character map cursor: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 497
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v9

    if-nez v9, :cond_0

    move-object v1, v2

    .line 502
    .end local v0    # "c":Ljava/lang/String;
    .end local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .end local v3    # "characterOffset":I
    .end local v4    # "count":I
    .end local v5    # "indexedNumbers":Z
    .end local v6    # "indexedSymbols":Z
    .end local v7    # "shouldAdd":Z
    .restart local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    :cond_2
    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 504
    :goto_1
    return-object v1

    .line 475
    .end local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v0    # "c":Ljava/lang/String;
    .restart local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v3    # "characterOffset":I
    .restart local v4    # "count":I
    .restart local v5    # "indexedNumbers":Z
    .restart local v6    # "indexedSymbols":Z
    .restart local v7    # "shouldAdd":Z
    :cond_3
    :try_start_2
    const-string v9, "[A-Z]"

    invoke-virtual {v0, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 476
    const/4 v7, 0x1

    .line 490
    :cond_4
    :goto_2
    if-eqz v7, :cond_5

    .line 491
    new-instance v9, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;

    invoke-direct {v9}, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;-><init>()V

    .line 492
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->character(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;

    move-result-object v9

    .line 493
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->offset(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;

    move-result-object v9

    .line 494
    invoke-virtual {v9}, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    move-result-object v9

    .line 491
    invoke-interface {v2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 496
    :cond_5
    add-int/2addr v3, v4

    goto :goto_0

    .line 477
    :cond_6
    const-string v9, "\\d"

    invoke-virtual {v0, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 478
    if-nez v5, :cond_4

    .line 479
    const-string v0, "#"

    .line 480
    const/4 v5, 0x1

    .line 481
    const/4 v7, 0x1

    goto :goto_2

    .line 484
    :cond_7
    if-nez v6, :cond_4

    .line 485
    const-string v0, "%"
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 486
    const/4 v6, 0x1

    .line 487
    const/4 v7, 0x1

    goto :goto_2

    .line 499
    .end local v0    # "c":Ljava/lang/String;
    .end local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .end local v3    # "characterOffset":I
    .end local v4    # "count":I
    .end local v5    # "indexedNumbers":Z
    .end local v6    # "indexedSymbols":Z
    .end local v7    # "shouldAdd":Z
    .restart local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    :catch_0
    move-exception v8

    .line 500
    .local v8, "t":Ljava/lang/Throwable;
    :goto_3
    :try_start_3
    sget-object v9, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t generate character map: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 502
    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v8    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v9

    :goto_4
    invoke-static {p1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v9

    .end local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v3    # "characterOffset":I
    .restart local v5    # "indexedNumbers":Z
    .restart local v6    # "indexedSymbols":Z
    .restart local v7    # "shouldAdd":Z
    :catchall_1
    move-exception v9

    move-object v1, v2

    .end local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    goto :goto_4

    .line 499
    .end local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    :catch_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    .restart local v1    # "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    goto :goto_3
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    .locals 2

    .prologue
    .line 168
    const-class v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    .line 171
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getMusicCapabilities()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;
    .locals 5

    .prologue
    .line 515
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveStoragePermission()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 516
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_AUTHORIZED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 521
    .local v0, "authorizationStatus":Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 522
    .local v2, "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 523
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524
    sget-object v3, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 527
    .local v1, "capabilitiesList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCapability;>;"
    new-instance v3, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;-><init>()V

    sget-object v4, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_ANDROID_LOCAL:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 528
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;

    move-result-object v3

    .line 529
    invoke-virtual {v3, v2}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->collectionTypes(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;

    move-result-object v3

    .line 530
    invoke-virtual {v3, v0}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->authorizationStatus(Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;)Lcom/navdy/service/library/events/audio/MusicCapability$Builder;

    move-result-object v3

    .line 531
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/MusicCapability$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCapability;

    move-result-object v3

    .line 527
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 532
    new-instance v3, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;

    invoke-direct {v3}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;-><init>()V

    .line 533
    invoke-virtual {v3, v1}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;->capabilities(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;

    move-result-object v3

    .line 534
    invoke-virtual {v3}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    move-result-object v3

    return-object v3

    .line 518
    .end local v0    # "authorizationStatus":Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    .end local v1    # "capabilitiesList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCapability;>;"
    .end local v2    # "collectionTypes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionType;>;"
    :cond_0
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_DENIED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .restart local v0    # "authorizationStatus":Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    goto :goto_0
.end method

.method private indexAllPlaylists(Z)V
    .locals 12
    .param p1, "initializing"    # Z
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 795
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "indexAllPlaylists ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 796
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 797
    const/4 v5, 0x0

    .line 799
    .local v5, "playlistsCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->deleteAllPlaylists()V

    .line 801
    const/4 v0, 0x0

    .line 802
    .local v0, "analyticsPlaylistCount":I
    const/4 v1, 0x0

    .line 804
    .local v1, "analyticsTrackCount":I
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistsCursor()Landroid/database/Cursor;

    move-result-object v5

    .line 805
    if-eqz v5, :cond_2

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 806
    const/4 v7, 0x0

    iput v7, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexingRetries:I

    .line 809
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->shouldPerformFullPlaylistIndex:Z

    .line 811
    :cond_0
    const-string v7, "_id"

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 812
    .local v3, "playlistId":I
    const-string v7, "playlist_name"

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 814
    .local v4, "playlistName":Ljava/lang/String;
    invoke-direct {p0, v3, v4}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexPlaylistMembers(ILjava/lang/String;)I

    move-result v7

    add-int/2addr v1, v7

    .line 815
    add-int/lit8 v0, v0, 0x1

    .line 816
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 818
    new-instance v2, Ljava/util/HashMap;

    const/4 v7, 0x2

    invoke-direct {v2, v7}, Ljava/util/HashMap;-><init>(I)V

    .line 819
    .local v2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "Num_Gpm_Playlists_Indexed"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    const-string v7, "Num_Gpm_Tracks_Indexed"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    const-string v7, "Music_Playlist_Complete_Reindex"

    invoke-static {v7, v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 835
    .end local v2    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "playlistId":I
    .end local v4    # "playlistName":Ljava/lang/String;
    :cond_1
    :goto_0
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 837
    .end local v0    # "analyticsPlaylistCount":I
    .end local v1    # "analyticsTrackCount":I
    :goto_1
    return-void

    .line 822
    .restart local v0    # "analyticsPlaylistCount":I
    .restart local v1    # "analyticsTrackCount":I
    :cond_2
    if-nez v5, :cond_1

    .line 823
    :try_start_1
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cursor returned is null, we will retry, Retries : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexingRetries:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 824
    if-eqz p1, :cond_3

    iget v7, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexingRetries:I

    const/4 v8, 0x5

    if-gt v7, v8, :cond_3

    .line 826
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->retryIndexingPlayListsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 827
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->retryIndexingPlayListsRunnable:Ljava/lang/Runnable;

    const-wide/16 v10, 0x2710

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 832
    .end local v0    # "analyticsPlaylistCount":I
    .end local v1    # "analyticsTrackCount":I
    :catch_0
    move-exception v6

    .line 833
    .local v6, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to index playlists: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 835
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .line 829
    .end local v6    # "t":Ljava/lang/Throwable;
    .restart local v0    # "analyticsPlaylistCount":I
    .restart local v1    # "analyticsTrackCount":I
    :cond_3
    :try_start_3
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Not retrying as the maximum retries has reached"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 835
    .end local v0    # "analyticsPlaylistCount":I
    .end local v1    # "analyticsTrackCount":I
    :catchall_0
    move-exception v7

    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7
.end method

.method private indexPlaylist(I)V
    .locals 6
    .param p1, "playlistId"    # I
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 841
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "indexPlaylist: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 842
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 843
    const/4 v0, 0x0

    .line 847
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->deletePlaylist(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 852
    :try_start_1
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistCursor(I)Landroid/database/Cursor;

    move-result-object v0

    .line 853
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 854
    const-string v3, "playlist_name"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 855
    .local v2, "playlistName":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 862
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 864
    .end local v2    # "playlistName":Ljava/lang/String;
    :goto_0
    return-void

    .line 848
    :catch_0
    move-exception v1

    .line 849
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while deleting playlist "

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 862
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 859
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "playlistName":Ljava/lang/String;
    :cond_0
    :try_start_3
    invoke-direct {p0, p1, v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexPlaylistMembers(ILjava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 862
    .end local v2    # "playlistName":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v3
.end method

.method private indexPlaylistMembers(ILjava/lang/String;)I
    .locals 11
    .param p1, "playlistId"    # I
    .param p2, "playlistName"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 869
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 870
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistMembersCursor(I)Landroid/database/Cursor;

    move-result-object v4

    .line 872
    .local v4, "membersCursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 875
    .local v1, "analyticsTrackCount":I
    if-eqz v4, :cond_3

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 876
    const/4 v3, 0x0

    .line 878
    .local v3, "count":I
    :cond_0
    const-string v8, "SourceId"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 880
    .local v5, "sourceId":Ljava/lang/String;
    const-string v8, "\\d+"

    invoke-virtual {v5, v8}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 881
    if-nez v3, :cond_1

    .line 882
    sget-object v8, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Playlist ID: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Name: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 883
    invoke-static {p1, p2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->addPlaylistToDb(ILjava/lang/String;)V

    .line 885
    :cond_1
    const-string v8, "artist"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 886
    .local v2, "artist":Ljava/lang/String;
    const-string v8, "album"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 887
    .local v0, "album":Ljava/lang/String;
    const-string v8, "title"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 888
    .local v7, "title":Ljava/lang/String;
    sget-object v8, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "- Artist: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Album: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Title: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", ID: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 890
    invoke-static {p1, v5, v2, v0, v7}, Lcom/navdy/client/app/providers/NavdyContentProvider;->addPlaylistMemberToDb(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    add-int/lit8 v3, v3, 0x1

    .line 894
    .end local v0    # "album":Ljava/lang/String;
    .end local v2    # "artist":Ljava/lang/String;
    .end local v7    # "title":Ljava/lang/String;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 895
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-nez v8, :cond_0

    .line 900
    .end local v3    # "count":I
    .end local v5    # "sourceId":Ljava/lang/String;
    :cond_3
    if-eqz v4, :cond_4

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 902
    :cond_4
    :goto_0
    return v1

    .line 897
    :catch_0
    move-exception v6

    .line 898
    .local v6, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v8, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to index playlist "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " members: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 900
    if-eqz v4, :cond_4

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v6    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    if-eqz v4, :cond_5

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v8
.end method

.method private isTheSameAsCurrentSong(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z
    .locals 2
    .param p1, "theOtherSong"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 690
    .line 691
    invoke-static {p1}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v0

    .line 692
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v1

    .line 690
    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private logInternalPlaylistTables()V
    .locals 3

    .prologue
    .line 989
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$6;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$6;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1028
    return-void
.end method

.method private sendCurrentMediaArtworkAsUpdate()V
    .locals 5

    .prologue
    .line 735
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendCurrentMediaArtworkAsUpdate, postPhotoUpdates: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->postPhotoUpdates:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 736
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->postPhotoUpdates:Z

    if-eqz v2, :cond_2

    .line 737
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Sending music artwork photo update"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 738
    new-instance v1, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;-><init>()V

    .line 739
    .local v1, "builder":Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    sget-object v2, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    .line 741
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/service/library/util/MusicDataUtils;->photoIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v2

    .line 740
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    .line 743
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaArtwork()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 744
    .local v0, "art":Landroid/graphics/Bitmap;
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "State of currentMediaArtwork: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 745
    if-eqz v0, :cond_0

    .line 746
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->bitmap2ByteBuffer(Landroid/graphics/Bitmap;)[B

    move-result-object v2

    invoke-static {v2}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photo(Lokio/ByteString;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    move-result-object v1

    .line 749
    :cond_0
    iget-object v2, v1, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photo:Lokio/ByteString;

    if-nez v2, :cond_1

    .line 750
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Photo within the photo update is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 753
    :cond_1
    invoke-virtual {v1}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdate;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V

    .line 755
    .end local v0    # "art":Landroid/graphics/Bitmap;
    .end local v1    # "builder":Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    :cond_2
    return-void
.end method

.method private sendCurrentTrackInfo()V
    .locals 1

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V

    .line 650
    return-void
.end method

.method private sendRemoteMessage(Lcom/squareup/wire/Message;)V
    .locals 3
    .param p1, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 640
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending message to the HUD: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 641
    invoke-static {p1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 642
    return-void
.end method

.method private declared-synchronized setCurrentMediaArtwork(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2
    .param p1, "artwork"    # Landroid/graphics/Bitmap;
    .param p2, "hash"    # Ljava/lang/String;

    .prologue
    .line 717
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentArtworkHash:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaArtwork:Landroid/graphics/Bitmap;

    .line 719
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentArtworkHash:Ljava/lang/String;

    .line 720
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendCurrentMediaArtworkAsUpdate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 724
    :goto_0
    monitor-exit p0

    return-void

    .line 722
    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Album art is identical to current, ignoring"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public checkAndSetCurrentMediaArtwork(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p2, "artwork"    # Landroid/graphics/Bitmap;

    .prologue
    .line 696
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->isTheSameAsCurrentSong(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 697
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Tried to set artwork from the wrong song - ignored"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 714
    :goto_0
    return-void

    .line 701
    :cond_0
    if-nez p2, :cond_1

    .line 702
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "null set as current media artwork (artwork reset)"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 705
    :cond_1
    invoke-static {p2}, Lcom/navdy/service/library/util/IOUtils;->hashForBitmap(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    .line 706
    .local v0, "artworkHash":Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got album artwork with hash "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 707
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/MusicUtils;->isDefaultArtwork(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 709
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "default album art found - ignored"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 713
    :cond_2
    invoke-direct {p0, p2, v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setCurrentMediaArtwork(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 155
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 157
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 158
    return-void
.end method

.method public declared-synchronized getCurrentMediaArtwork()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 727
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaArtwork:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 654
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLastMusicApp()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 773
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 774
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "last_media_app"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 776
    .local v0, "packageName":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLastMusicApp "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 777
    return-object v0
.end method

.method public getMusicSeekHelper()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicSeekHelper:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;

    return-object v0
.end method

.method public indexPlaylists()V
    .locals 2

    .prologue
    .line 785
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicObserverHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$5;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$5;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 791
    return-void
.end method

.method public isMusicPlayerActive()Z
    .locals 2

    .prologue
    .line 781
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onHUDConnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 188
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUD is connected - doing some initial stuff for music"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->postPhotoUpdates:Z

    .line 190
    return-void
.end method

.method public onHUDDisconnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 180
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUD is disconnected - reset internal state"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->postPhotoUpdates:Z

    .line 182
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->executeLongPressedKeyUp()V

    .line 183
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicUtils;->stopInternalMusicPlayer()V

    .line 184
    return-void
.end method

.method public onMusicArtworkRequest(Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 539
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMusicArtworkRequest "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 540
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$4;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Lcom/navdy/service/library/events/audio/MusicArtworkRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 601
    return-void
.end method

.method public onMusicCapabilitiesRequest(Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;)V
    .locals 2
    .param p1, "musicCapabilitiesRequest"    # Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 509
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onMusicCapabilitiesRequest"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 510
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getMusicCapabilities()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 511
    return-void
.end method

.method public onMusicCollectionRequest(Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 194
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$3;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 457
    return-void
.end method

.method public onMusicEvent(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/service/library/events/audio/MusicEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 611
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Request for performing some action to the music received."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 613
    :try_start_0
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/MusicUtils;->executeMusicAction(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_2

    .line 621
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v0

    .line 615
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot load music"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 616
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 617
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot start music player"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 618
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_2
    move-exception v0

    .line 619
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Cannot execute action"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onMusicTrackInfoRequest(Lcom/navdy/service/library/events/audio/MusicTrackInfoRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/navdy/service/library/events/audio/MusicTrackInfoRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 605
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Request for track info received."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 606
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendCurrentTrackInfo()V

    .line 607
    return-void
.end method

.method public onPhotoUpdatesRequest(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 625
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request for changing photo updating status received with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    .line 626
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for imageResourceIds of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 627
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 625
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 628
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/photo/PhotoType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->postPhotoUpdates:Z

    .line 630
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendCurrentMediaArtworkAsUpdate()V

    .line 632
    :cond_0
    return-void
.end method

.method public declared-synchronized setCurrentMediaTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
    .locals 3
    .param p1, "currentMediaTrackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 665
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCurrentMediaTrackInfo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    if-ne p1, v0, :cond_0

    .line 668
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Got same track info object - disregarded"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    :goto_0
    monitor-exit p0

    return-void

    .line 672
    :cond_0
    if-nez p1, :cond_1

    .line 673
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Got null track info object - cleaning"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 674
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->EMPTY_TRACK_INFO:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 675
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setCurrentMediaArtwork(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 676
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 665
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 680
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-virtual {p1, v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 681
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->throttlingLogger:Lcom/navdy/client/app/framework/util/ThrottlingLogger;

    const/4 v1, 0x1

    const-string v2, "Got no new data in the update - disregarded"

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/util/ThrottlingLogger;->i(ILjava/lang/String;)V

    goto :goto_0

    .line 685
    :cond_2
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 686
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->currentMediaTrackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public setLastMusicApp(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 763
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLastMusicApp "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 764
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 765
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_media_app"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 766
    return-void
.end method

.method public setMusicSeekHelper(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;)V
    .locals 0
    .param p1, "musicSeekHelper"    # Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->musicSeekHelper:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;

    .line 122
    return-void
.end method

.method public shouldPerformFullPlaylistIndex()Z
    .locals 1

    .prologue
    .line 1031
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->shouldPerformFullPlaylistIndex:Z

    return v0
.end method
