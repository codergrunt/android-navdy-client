.class public final Lcom/navdy/client/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final NO:I = 0x7f08009e

.field public static final Securing_mount_desc:I = 0x7f08009f

.field public static final Securing_mount_title:I = 0x7f0800a0

.field public static final YES:I = 0x7f0800a1

.field public static final abc_action_bar_home_description:I = 0x7f080000

.field public static final abc_action_bar_home_description_format:I = 0x7f080001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f080002

.field public static final abc_action_bar_up_description:I = 0x7f080003

.field public static final abc_action_menu_overflow_description:I = 0x7f080004

.field public static final abc_action_mode_done:I = 0x7f080005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f080006

.field public static final abc_activitychooserview_choose_application:I = 0x7f080007

.field public static final abc_capital_off:I = 0x7f080008

.field public static final abc_capital_on:I = 0x7f080009

.field public static final abc_font_family_body_1_material:I = 0x7f08051b

.field public static final abc_font_family_body_2_material:I = 0x7f08051c

.field public static final abc_font_family_button_material:I = 0x7f08051d

.field public static final abc_font_family_caption_material:I = 0x7f08051e

.field public static final abc_font_family_display_1_material:I = 0x7f08051f

.field public static final abc_font_family_display_2_material:I = 0x7f080520

.field public static final abc_font_family_display_3_material:I = 0x7f080521

.field public static final abc_font_family_display_4_material:I = 0x7f080522

.field public static final abc_font_family_headline_material:I = 0x7f080523

.field public static final abc_font_family_menu_material:I = 0x7f080524

.field public static final abc_font_family_subhead_material:I = 0x7f080525

.field public static final abc_font_family_title_material:I = 0x7f080526

.field public static final abc_search_hint:I = 0x7f08000a

.field public static final abc_searchview_description_clear:I = 0x7f08000b

.field public static final abc_searchview_description_query:I = 0x7f08000c

.field public static final abc_searchview_description_search:I = 0x7f08000d

.field public static final abc_searchview_description_submit:I = 0x7f08000e

.field public static final abc_searchview_description_voice:I = 0x7f08000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f080010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f080011

.field public static final abc_toolbar_collapse_description:I = 0x7f080012

.field public static final about:I = 0x7f0800a2

.field public static final about_check_for_update:I = 0x7f0800a3

.field public static final about_copyright:I = 0x7f080527

.field public static final about_display_info:I = 0x7f0800a4

.field public static final about_serial:I = 0x7f080528

.field public static final about_version:I = 0x7f0800a5

.field public static final about_vin:I = 0x7f0800a6

.field public static final accept:I = 0x7f080013

.field public static final acknowledgments:I = 0x7f0800a7

.field public static final action_settings:I = 0x7f0800a8

.field public static final actions:I = 0x7f0800a9

.field public static final active_trip_prefix:I = 0x7f0800aa

.field public static final active_trip_title:I = 0x7f0800ab

.field public static final add_favorite:I = 0x7f0800ac

.field public static final add_home:I = 0x7f0800ad

.field public static final add_message:I = 0x7f0800ae

.field public static final add_photo:I = 0x7f0800af

.field public static final add_to_favorites:I = 0x7f0800b0

.field public static final add_work:I = 0x7f0800b1

.field public static final address_format_subtitle:I = 0x7f0800b2

.field public static final address_format_title:I = 0x7f0800b3

.field public static final air_plane_mode:I = 0x7f0800b4

.field public static final align_lens:I = 0x7f0800b5

.field public static final allow_gestures_description:I = 0x7f0800b6

.field public static final allow_glances:I = 0x7f0800b7

.field public static final allow_glances_description:I = 0x7f0800b8

.field public static final already_have_account:I = 0x7f0800b9

.field public static final already_installed_skip_to_app_setup:I = 0x7f0800ba

.field public static final always_send_logs:I = 0x7f0800bb

.field public static final and_email:I = 0x7f0800bc

.field public static final android_calendar:I = 0x7f0800bd

.field public static final app_name:I = 0x7f080529

.field public static final app_setup:I = 0x7f0800be

.field public static final app_setup_description:I = 0x7f0800bf

.field public static final app_setup_video:I = 0x7f0800c0

.field public static final app_version_title:I = 0x7f0800c1

.field public static final appbar_scrolling_view_behavior:I = 0x7f08052a

.field public static final are_you_sure:I = 0x7f0800c2

.field public static final arrive_title:I = 0x7f0800c3

.field public static final articles_list_fragment_error_message:I = 0x7f080060

.field public static final articles_list_fragment_no_articles_found:I = 0x7f080061

.field public static final articles_search_results_list_fragment_contact_us:I = 0x7f080062

.field public static final articles_search_results_list_fragment_error_message:I = 0x7f080063

.field public static final articles_search_results_list_fragment_no_articles_found:I = 0x7f080064

.field public static final assemble_mount:I = 0x7f0800c4

.field public static final atm:I = 0x7f0800c5

.field public static final attach_another_photo:I = 0x7f0800c6

.field public static final attach_image:I = 0x7f0800c7

.field public static final attach_image_title:I = 0x7f0800c8

.field public static final attach_logs:I = 0x7f0800c9

.field public static final attach_logs_desc:I = 0x7f0800ca

.field public static final attaching_dial:I = 0x7f0800cb

.field public static final attachment_add_menu:I = 0x7f080065

.field public static final attachment_select_source_choose_existing:I = 0x7f080066

.field public static final attachment_select_source_new_photo:I = 0x7f080067

.field public static final attachment_upload_error_cancel:I = 0x7f080068

.field public static final attachment_upload_error_file_already_added:I = 0x7f080069

.field public static final attachment_upload_error_file_not_found:I = 0x7f08006a

.field public static final attachment_upload_error_file_too_big:I = 0x7f08006b

.field public static final attachment_upload_error_try_again:I = 0x7f08006c

.field public static final attachment_upload_error_upload_failed:I = 0x7f08006d

.field public static final audio_recording_error:I = 0x7f0800cc

.field public static final auth_google_play_services_client_facebook_display_name:I = 0x7f08052b

.field public static final auth_google_play_services_client_google_display_name:I = 0x7f08052c

.field public static final auto_download:I = 0x7f0800cd

.field public static final auto_download_description:I = 0x7f0800ce

.field public static final autocomplete_hint:I = 0x7f0800cf

.field public static final aws_access_key_id:I = 0x7f08052d

.field public static final aws_secret_access_key:I = 0x7f08052e

.field public static final back:I = 0x7f0800d0

.field public static final be_there_in_ETA:I = 0x7f0800d1

.field public static final belvedere_dialog_camera:I = 0x7f08052f

.field public static final belvedere_dialog_gallery:I = 0x7f080530

.field public static final belvedere_dialog_unknown:I = 0x7f080531

.field public static final belvedere_sample_camera:I = 0x7f080532

.field public static final belvedere_sample_gallery:I = 0x7f080533

.field public static final belvedere_sdk_fpa_suffix:I = 0x7f080534

.field public static final beta_disclaimer:I = 0x7f0800d2

.field public static final bluetooth_explanation:I = 0x7f0800d3

.field public static final bluetooth_failed_bullet_list:I = 0x7f0800d4

.field public static final bluetooth_failed_explanation:I = 0x7f0800d5

.field public static final bluetooth_is_disabled:I = 0x7f0800d6

.field public static final bluetooth_not_compatible:I = 0x7f0800d7

.field public static final bluetooth_not_found_advice:I = 0x7f0800d8

.field public static final bottom_sheet_behavior:I = 0x7f080535

.field public static final brightness:I = 0x7f0800d9

.field public static final brightness_auto:I = 0x7f0800da

.field public static final build_source_beta:I = 0x7f080536

.field public static final build_source_nightly:I = 0x7f080537

.field public static final build_source_release:I = 0x7f080538

.field public static final build_source_stable:I = 0x7f080539

.field public static final build_type_description:I = 0x7f0800db

.field public static final buy_navdy_at_navdycom:I = 0x7f0800dc

.field public static final calculate_route:I = 0x7f0800dd

.field public static final calculate_route_failure:I = 0x7f0800de

.field public static final calculating:I = 0x7f0800df

.field public static final calculating_distance:I = 0x7f0800e0

.field public static final calculating_dot_dot_dot:I = 0x7f0800e1

.field public static final calculating_route:I = 0x7f0800e2

.field public static final calendar:I = 0x7f0800e3

.field public static final camera_permission_not_granted:I = 0x7f0800e4

.field public static final cancel:I = 0x7f0800e5

.field public static final cancel_button:I = 0x7f0800e6

.field public static final cancel_download:I = 0x7f0800e7

.field public static final cancel_request:I = 0x7f0800e8

.field public static final cant_calculate_distances_without_location_permission:I = 0x7f0800e9

.field public static final cant_save_destination_without_storage_permission:I = 0x7f0800ea

.field public static final cant_search_contacts_without_location_permission:I = 0x7f0800eb

.field public static final cant_search_without_location_permission:I = 0x7f0800ec

.field public static final cant_suggest_destinations_without_location_permission:I = 0x7f0800ed

.field public static final car_info:I = 0x7f0800ee

.field public static final car_info_tell_us_what_you_have:I = 0x7f0800ef

.field public static final car_location:I = 0x7f0800f0

.field public static final car_md_auth:I = 0x7f08053a

.field public static final car_md_token:I = 0x7f08053b

.field public static final car_selector:I = 0x7f0800f1

.field public static final categories_list_fragment_error_message:I = 0x7f08006e

.field public static final categories_list_fragment_no_categories_found:I = 0x7f08006f

.field public static final center_compartment__next_to_hand_brake:I = 0x7f0800f2

.field public static final center_compartment__under_armrest:I = 0x7f0800f3

.field public static final center_console____below_radio__ac_controls:I = 0x7f0800f4

.field public static final center_console__above_climate_control:I = 0x7f0800f5

.field public static final center_console__behind_ashtray:I = 0x7f0800f6

.field public static final center_console__behind_coin_tray:I = 0x7f0800f7

.field public static final center_console__behind_fuse_box_cover:I = 0x7f0800f8

.field public static final center_console__right_side_of_console:I = 0x7f0800f9

.field public static final center_console__right_side_of_radio:I = 0x7f0800fa

.field public static final center_map:I = 0x7f0800fb

.field public static final center_on_my_location:I = 0x7f0800fc

.field public static final character_counter_pattern:I = 0x7f08053c

.field public static final check_for_update:I = 0x7f0800fd

.field public static final check_that_feet_arent_obstructed:I = 0x7f0800fe

.field public static final checking_for_update:I = 0x7f0800ff

.field public static final checking_for_updates:I = 0x7f080100

.field public static final choose_stem:I = 0x7f080101

.field public static final cla_location:I = 0x7f080102

.field public static final cla_power_cable:I = 0x7f080103

.field public static final cla_power_cable_description:I = 0x7f080104

.field public static final clean_your_dash:I = 0x7f080105

.field public static final clear_text:I = 0x7f080106

.field public static final collect_logs:I = 0x7f080107

.field public static final collecting_device_logs:I = 0x7f080108

.field public static final collecting_logs:I = 0x7f080109

.field public static final com_crashlytics_android_build_id:I = 0x7f08053d

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f080014

.field public static final common_android_wear_update_text:I = 0x7f080015

.field public static final common_android_wear_update_title:I = 0x7f080016

.field public static final common_google_play_services_api_unavailable_text:I = 0x7f080017

.field public static final common_google_play_services_enable_button:I = 0x7f080018

.field public static final common_google_play_services_enable_text:I = 0x7f080019

.field public static final common_google_play_services_enable_title:I = 0x7f08001a

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f08001b

.field public static final common_google_play_services_install_button:I = 0x7f08001c

.field public static final common_google_play_services_install_text:I = 0x7f08001d

.field public static final common_google_play_services_install_text_phone:I = 0x7f08001e

.field public static final common_google_play_services_install_text_tablet:I = 0x7f08001f

.field public static final common_google_play_services_install_title:I = 0x7f080020

.field public static final common_google_play_services_invalid_account_text:I = 0x7f080021

.field public static final common_google_play_services_invalid_account_title:I = 0x7f080022

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f080023

.field public static final common_google_play_services_network_error_text:I = 0x7f080024

.field public static final common_google_play_services_network_error_title:I = 0x7f080025

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f080026

.field public static final common_google_play_services_notification_ticker:I = 0x7f080027

.field public static final common_google_play_services_sign_in_failed_text:I = 0x7f080028

.field public static final common_google_play_services_sign_in_failed_title:I = 0x7f080029

.field public static final common_google_play_services_unknown_issue:I = 0x7f08002a

.field public static final common_google_play_services_unsupported_text:I = 0x7f08002b

.field public static final common_google_play_services_unsupported_title:I = 0x7f08002c

.field public static final common_google_play_services_update_button:I = 0x7f08002d

.field public static final common_google_play_services_update_text:I = 0x7f08002e

.field public static final common_google_play_services_update_title:I = 0x7f08002f

.field public static final common_google_play_services_updating_text:I = 0x7f080030

.field public static final common_google_play_services_updating_title:I = 0x7f080031

.field public static final common_google_play_services_wear_update_text:I = 0x7f080032

.field public static final common_open_on_phone:I = 0x7f080033

.field public static final common_signin_button_text:I = 0x7f080034

.field public static final common_signin_button_text_long:I = 0x7f080035

.field public static final comp_current_image:I = 0x7f08010a

.field public static final comp_refresh:I = 0x7f08010b

.field public static final connected_to:I = 0x7f08010c

.field public static final contact_fragment_description_hint:I = 0x7f080070

.field public static final contact_fragment_email_hint:I = 0x7f080071

.field public static final contact_fragment_email_validation_error:I = 0x7f080072

.field public static final contact_fragment_request_subject:I = 0x7f080073

.field public static final contact_fragment_send_button_label:I = 0x7f080074

.field public static final contact_fragment_title:I = 0x7f080075

.field public static final contact_support:I = 0x7f08010d

.field public static final contact_us:I = 0x7f08010e

.field public static final contact_us_desc:I = 0x7f08010f

.field public static final contacting_navdy_servers:I = 0x7f080110

.field public static final contacts:I = 0x7f080111

.field public static final context_menu:I = 0x7f080112

.field public static final continu:I = 0x7f080113

.field public static final continue_anyway:I = 0x7f080114

.field public static final contour_the_mount:I = 0x7f080115

.field public static final could_not_route_to_intent:I = 0x7f080116

.field public static final covered:I = 0x7f080117

.field public static final create_account:I = 0x7f080118

.field public static final create_account_lowercase:I = 0x7f080119

.field public static final create_calendar_message:I = 0x7f080036

.field public static final create_calendar_title:I = 0x7f080037

.field public static final crop__cancel:I = 0x7f080056

.field public static final crop__done:I = 0x7f080057

.field public static final crop__pick_error:I = 0x7f080058

.field public static final crop__saving:I = 0x7f080059

.field public static final crop__wait:I = 0x7f08005a

.field public static final current_hud_version:I = 0x7f08011a

.field public static final current_hud_version_is:I = 0x7f08011b

.field public static final current_hud_version_unknown:I = 0x7f08011c

.field public static final dash:I = 0x7f08011d

.field public static final days_and_hours_format:I = 0x7f08011e

.field public static final days_format:I = 0x7f08011f

.field public static final debug_menu_ad_information:I = 0x7f080038

.field public static final debug_menu_creative_preview:I = 0x7f080039

.field public static final debug_menu_title:I = 0x7f08003a

.field public static final debug_menu_troubleshooting:I = 0x7f08003b

.field public static final decline:I = 0x7f08003c

.field public static final default_navdy_name:I = 0x7f080120

.field public static final delete_btn:I = 0x7f080121

.field public static final demo_video_duration:I = 0x7f080122

.field public static final demo_video_url:I = 0x7f080123

.field public static final depart_within:I = 0x7f080124

.field public static final describe_the_problem_bulletpoint:I = 0x7f080125

.field public static final description:I = 0x7f080126

.field public static final destination_search_hint:I = 0x7f080127

.field public static final device_pairing_failed:I = 0x7f080128

.field public static final dial_bond:I = 0x7f080129

.field public static final dial_clear_bond:I = 0x7f08012a

.field public static final dial_get_status:I = 0x7f08012b

.field public static final dial_long_press_desc:I = 0x7f08012c

.field public static final dial_rebond:I = 0x7f08012d

.field public static final did_you_mean:I = 0x7f08012e

.field public static final disconnect_from_display:I = 0x7f08012f

.field public static final disconnect_from_display_desc:I = 0x7f080130

.field public static final dismiss:I = 0x7f080131

.field public static final dismiss_notification:I = 0x7f080132

.field public static final distance:I = 0x7f080133

.field public static final do_not_suggest_desc:I = 0x7f080134

.field public static final do_transfer:I = 0x7f080135

.field public static final do_you_recognize_your_device:I = 0x7f080136

.field public static final do_you_want_to_end_trip:I = 0x7f080137

.field public static final done:I = 0x7f080138

.field public static final dot:I = 0x7f080139

.field public static final download_beta:I = 0x7f08013a

.field public static final download_failed:I = 0x7f08013b

.field public static final download_unstable:I = 0x7f08013c

.field public static final download_update:I = 0x7f08013d

.field public static final downloading:I = 0x7f08013e

.field public static final downloading_update:I = 0x7f08013f

.field public static final drag_to_select_place:I = 0x7f080140

.field public static final driver_side__kick_panel_behind_fuse_box_cover:I = 0x7f080141

.field public static final driver_side__left_of_steering_wheel_above_hood_release:I = 0x7f080142

.field public static final driver_side__left_side_of_center_console:I = 0x7f080143

.field public static final driver_side__right_side_of_steering_wheel:I = 0x7f080144

.field public static final driver_side__under_lower_left_side_of_dashboard:I = 0x7f080145

.field public static final driver_side__under_lower_right_side_of_dashboard:I = 0x7f080146

.field public static final driver_side__under_steering_wheel_column:I = 0x7f080147

.field public static final dropped_pin:I = 0x7f080148

.field public static final dude_you_need_google_play_services:I = 0x7f080149

.field public static final edit:I = 0x7f08014a

.field public static final edit_favorite:I = 0x7f08014b

.field public static final edit_message:I = 0x7f08014c

.field public static final edit_photo:I = 0x7f08014d

.field public static final edit_suggestion_title:I = 0x7f08014e

.field public static final email:I = 0x7f08014f

.field public static final email_address:I = 0x7f080150

.field public static final email_address_example:I = 0x7f080151

.field public static final enable_button:I = 0x7f080152

.field public static final enable_gestures:I = 0x7f080153

.field public static final enable_gestures_description:I = 0x7f080154

.field public static final enable_glances:I = 0x7f080155

.field public static final enable_glances_description:I = 0x7f080156

.field public static final enable_location_permissions:I = 0x7f080157

.field public static final enable_map_zoom:I = 0x7f080158

.field public static final enable_microphone:I = 0x7f080159

.field public static final enable_microphone_description:I = 0x7f08015a

.field public static final end:I = 0x7f08015b

.field public static final end_trip:I = 0x7f08015c

.field public static final enter_an_accurate_email_bulletpoint:I = 0x7f08015d

.field public static final enter_your_name:I = 0x7f08015e

.field public static final environment_too_loud:I = 0x7f08015f

.field public static final error:I = 0x7f080160

.field public static final error_creating_ticket:I = 0x7f080161

.field public static final error_loading_google_maps:I = 0x7f080162

.field public static final error_please_retry_route:I = 0x7f080163

.field public static final eta_days:I = 0x7f080164

.field public static final eta_hours:I = 0x7f080165

.field public static final eta_hr:I = 0x7f080166

.field public static final eta_min:I = 0x7f080167

.field public static final eta_minutes:I = 0x7f080168

.field public static final eta_title:I = 0x7f080169

.field public static final eta_with_heavy_traffic:I = 0x7f08016a

.field public static final eta_with_normal_traffic:I = 0x7f08016b

.field public static final event_times:I = 0x7f08016c

.field public static final external_storage_not_mounted:I = 0x7f08016d

.field public static final facebook:I = 0x7f08016e

.field public static final facebook_messenger:I = 0x7f08016f

.field public static final fav_edit_address_label:I = 0x7f080170

.field public static final fav_edit_label_label:I = 0x7f080171

.field public static final fav_label_hint:I = 0x7f080172

.field public static final favorites_fragment:I = 0x7f080173

.field public static final favorites_fragment_description:I = 0x7f080174

.field public static final favorites_fragment_title:I = 0x7f080175

.field public static final favs_fragment:I = 0x7f080176

.field public static final feature_not_implemented_yet:I = 0x7f080177

.field public static final feature_updating_navdy:I = 0x7f080178

.field public static final feature_videos:I = 0x7f080179

.field public static final features_answering_calls:I = 0x7f08017a

.field public static final features_brightness:I = 0x7f08017b

.field public static final features_connection:I = 0x7f08017c

.field public static final features_controlling_music:I = 0x7f08017d

.field public static final features_dash_mode:I = 0x7f08017e

.field public static final features_dial:I = 0x7f08017f

.field public static final features_favorites:I = 0x7f080180

.field public static final features_favorites_navigation:I = 0x7f080181

.field public static final features_gestures:I = 0x7f080182

.field public static final features_making_calls:I = 0x7f080183

.field public static final features_map_mode:I = 0x7f080184

.field public static final features_mobile_overview:I = 0x7f080185

.field public static final features_overview:I = 0x7f080186

.field public static final features_power:I = 0x7f080187

.field public static final features_reading_glances:I = 0x7f080188

.field public static final features_search:I = 0x7f080189

.field public static final features_suggested_places:I = 0x7f08018a

.field public static final features_using_google_now:I = 0x7f08018b

.field public static final features_video_activity:I = 0x7f08018c

.field public static final feet_abbrev:I = 0x7f08018d

.field public static final finish_later:I = 0x7f08018e

.field public static final flashlight:I = 0x7f08018f

.field public static final fle_app_setup_calendar_button:I = 0x7f080190

.field public static final fle_app_setup_calendar_button_fail:I = 0x7f080191

.field public static final fle_app_setup_calendar_desc:I = 0x7f080192

.field public static final fle_app_setup_calendar_desc_fail:I = 0x7f080193

.field public static final fle_app_setup_calendar_title:I = 0x7f080194

.field public static final fle_app_setup_calendar_title_fail:I = 0x7f080195

.field public static final fle_app_setup_contacts_button:I = 0x7f080196

.field public static final fle_app_setup_contacts_button_fail:I = 0x7f080197

.field public static final fle_app_setup_contacts_desc:I = 0x7f080198

.field public static final fle_app_setup_contacts_desc_fail:I = 0x7f080199

.field public static final fle_app_setup_contacts_title:I = 0x7f08019a

.field public static final fle_app_setup_contacts_title_fail:I = 0x7f08019b

.field public static final fle_app_setup_default_button_fail:I = 0x7f08019c

.field public static final fle_app_setup_default_desc_fail:I = 0x7f08019d

.field public static final fle_app_setup_default_title_fail:I = 0x7f08019e

.field public static final fle_app_setup_end_button:I = 0x7f08019f

.field public static final fle_app_setup_end_desc:I = 0x7f0801a0

.field public static final fle_app_setup_end_title:I = 0x7f0801a1

.field public static final fle_app_setup_glances_button:I = 0x7f0801a2

.field public static final fle_app_setup_glances_button_fail:I = 0x7f0801a3

.field public static final fle_app_setup_glances_desc:I = 0x7f0801a4

.field public static final fle_app_setup_glances_desc_fail:I = 0x7f0801a5

.field public static final fle_app_setup_glances_title:I = 0x7f0801a6

.field public static final fle_app_setup_glances_title_fail:I = 0x7f0801a7

.field public static final fle_app_setup_location_button:I = 0x7f0801a8

.field public static final fle_app_setup_location_button_fail:I = 0x7f0801a9

.field public static final fle_app_setup_location_desc:I = 0x7f0801aa

.field public static final fle_app_setup_location_desc_fail:I = 0x7f0801ab

.field public static final fle_app_setup_location_title:I = 0x7f0801ac

.field public static final fle_app_setup_location_title_fail:I = 0x7f0801ad

.field public static final fle_app_setup_make_calls_button:I = 0x7f0801ae

.field public static final fle_app_setup_make_calls_button_fail:I = 0x7f0801af

.field public static final fle_app_setup_make_calls_desc:I = 0x7f0801b0

.field public static final fle_app_setup_make_calls_desc_fail:I = 0x7f0801b1

.field public static final fle_app_setup_make_calls_title:I = 0x7f0801b2

.field public static final fle_app_setup_make_calls_title_fail:I = 0x7f0801b3

.field public static final fle_app_setup_messaging_button:I = 0x7f0801b4

.field public static final fle_app_setup_messaging_button_fail:I = 0x7f0801b5

.field public static final fle_app_setup_messaging_desc:I = 0x7f0801b6

.field public static final fle_app_setup_messaging_desc_fail:I = 0x7f0801b7

.field public static final fle_app_setup_messaging_title:I = 0x7f0801b8

.field public static final fle_app_setup_messaging_title_fail:I = 0x7f0801b9

.field public static final fle_app_setup_microphone_button:I = 0x7f0801ba

.field public static final fle_app_setup_microphone_button_fail:I = 0x7f0801bb

.field public static final fle_app_setup_microphone_desc:I = 0x7f0801bc

.field public static final fle_app_setup_microphone_desc_fail:I = 0x7f0801bd

.field public static final fle_app_setup_microphone_title:I = 0x7f0801be

.field public static final fle_app_setup_microphone_title_fail:I = 0x7f0801bf

.field public static final fle_app_setup_pair_with_display_button:I = 0x7f0801c0

.field public static final fle_app_setup_pair_with_display_button_fail:I = 0x7f0801c1

.field public static final fle_app_setup_pair_with_display_button_success:I = 0x7f0801c2

.field public static final fle_app_setup_pair_with_display_desc:I = 0x7f0801c3

.field public static final fle_app_setup_pair_with_display_desc_fail:I = 0x7f0801c4

.field public static final fle_app_setup_pair_with_display_desc_success:I = 0x7f0801c5

.field public static final fle_app_setup_pair_with_display_title:I = 0x7f0801c6

.field public static final fle_app_setup_pair_with_display_title_fail:I = 0x7f0801c7

.field public static final fle_app_setup_pair_with_display_title_success:I = 0x7f0801c8

.field public static final fle_app_setup_profile_button:I = 0x7f0801c9

.field public static final fle_app_setup_profile_desc:I = 0x7f0801ca

.field public static final fle_app_setup_profile_title:I = 0x7f0801cb

.field public static final fle_app_setup_storage_button:I = 0x7f0801cc

.field public static final fle_app_setup_storage_button_fail:I = 0x7f0801cd

.field public static final fle_app_setup_storage_desc:I = 0x7f0801ce

.field public static final fle_app_setup_storage_desc_fail:I = 0x7f0801cf

.field public static final fle_app_setup_storage_title:I = 0x7f0801d0

.field public static final fle_app_setup_storage_title_fail:I = 0x7f0801d1

.field public static final food:I = 0x7f0801d2

.field public static final force_full_update:I = 0x7f0801d3

.field public static final fuel:I = 0x7f0801d4

.field public static final gas:I = 0x7f0801d5

.field public static final gestures:I = 0x7f0801d6

.field public static final gestures_video_duration:I = 0x7f0801d7

.field public static final gestures_video_url:I = 0x7f0801d8

.field public static final get_a_mount_kit:I = 0x7f0801d9

.field public static final glance_dialog_description:I = 0x7f0801da

.field public static final glance_failure_explanation:I = 0x7f0801db

.field public static final glance_test_message_one:I = 0x7f0801dc

.field public static final glances_fragment:I = 0x7f0801dd

.field public static final glances_fragment_description:I = 0x7f0801de

.field public static final glances_notifications_description:I = 0x7f0801df

.field public static final glances_progress_label:I = 0x7f0801e0

.field public static final glances_warning:I = 0x7f0801e1

.field public static final go_to_bluetooth_settings:I = 0x7f0801e2

.field public static final go_to_permission_settings:I = 0x7f0801e3

.field public static final go_to_profile_to_set_email:I = 0x7f0801e4

.field public static final go_to_profile_to_set_name:I = 0x7f0801e5

.field public static final google_app_id:I = 0x7f08053e

.field public static final google_backup_service_key:I = 0x7f08053f

.field public static final google_calendar:I = 0x7f0801e6

.field public static final google_cloud_sender_id:I = 0x7f080540

.field public static final google_geo_api_key:I = 0x7f080541

.field public static final google_hangouts:I = 0x7f0801e7

.field public static final google_mail:I = 0x7f0801e8

.field public static final google_music:I = 0x7f0801e9

.field public static final google_now_dialog_description:I = 0x7f0801ea

.field public static final google_now_tip:I = 0x7f0801eb

.field public static final google_now_tip_subtitle:I = 0x7f0801ec

.field public static final google_web_service_key:I = 0x7f080542

.field public static final got_it:I = 0x7f0801ed

.field public static final guide_search_subtitle_format:I = 0x7f080543

.field public static final guided_tour:I = 0x7f0801ee

.field public static final guided_tour_desc:I = 0x7f0801ef

.field public static final heavy_traffic:I = 0x7f0801f0

.field public static final hello_there:I = 0x7f0801f1

.field public static final help:I = 0x7f0801f2

.field public static final help_center:I = 0x7f0801f3

.field public static final help_center_desc:I = 0x7f0801f4

.field public static final help_search_no_results_label:I = 0x7f080076

.field public static final help_see_all_articles_label:I = 0x7f080077

.field public static final help_see_all_n_articles_label:I = 0x7f080078

.field public static final here_geo_appid:I = 0x7f080544

.field public static final here_geo_apptoken:I = 0x7f080545

.field public static final here_maps_appid:I = 0x7f080546

.field public static final here_maps_apptoken:I = 0x7f080547

.field public static final here_maps_license_key:I = 0x7f080548

.field public static final here_you_go:I = 0x7f080515

.field public static final hide_all_tips:I = 0x7f0801f5

.field public static final high_accuracy_request_detail:I = 0x7f0801f6

.field public static final high_accuracy_request_title:I = 0x7f0801f7

.field public static final history:I = 0x7f0801f8

.field public static final hockey_appid:I = 0x7f080549

.field public static final hockeyapp_crash_dialog_app_name_fallback:I = 0x7f0801f9

.field public static final hockeyapp_crash_dialog_message:I = 0x7f0801fa

.field public static final hockeyapp_crash_dialog_negative_button:I = 0x7f0801fb

.field public static final hockeyapp_crash_dialog_neutral_button:I = 0x7f0801fc

.field public static final hockeyapp_crash_dialog_positive_button:I = 0x7f0801fd

.field public static final hockeyapp_crash_dialog_title:I = 0x7f0801fe

.field public static final hockeyapp_dialog_error_message:I = 0x7f0801ff

.field public static final hockeyapp_dialog_error_title:I = 0x7f080200

.field public static final hockeyapp_dialog_negative_button:I = 0x7f080201

.field public static final hockeyapp_dialog_positive_button:I = 0x7f080202

.field public static final hockeyapp_download_failed_dialog_message:I = 0x7f080203

.field public static final hockeyapp_download_failed_dialog_negative_button:I = 0x7f080204

.field public static final hockeyapp_download_failed_dialog_positive_button:I = 0x7f080205

.field public static final hockeyapp_download_failed_dialog_title:I = 0x7f080206

.field public static final hockeyapp_error_no_network_message:I = 0x7f080207

.field public static final hockeyapp_expiry_info_text:I = 0x7f080208

.field public static final hockeyapp_expiry_info_title:I = 0x7f080209

.field public static final hockeyapp_feedback_attach_file:I = 0x7f08020a

.field public static final hockeyapp_feedback_attach_picture:I = 0x7f08020b

.field public static final hockeyapp_feedback_attachment_button_text:I = 0x7f08020c

.field public static final hockeyapp_feedback_attachment_error:I = 0x7f08020d

.field public static final hockeyapp_feedback_attachment_loading:I = 0x7f08020e

.field public static final hockeyapp_feedback_email_hint:I = 0x7f08020f

.field public static final hockeyapp_feedback_failed_text:I = 0x7f080210

.field public static final hockeyapp_feedback_failed_title:I = 0x7f080211

.field public static final hockeyapp_feedback_fetching_feedback_text:I = 0x7f080212

.field public static final hockeyapp_feedback_generic_error:I = 0x7f080213

.field public static final hockeyapp_feedback_last_updated_text:I = 0x7f080214

.field public static final hockeyapp_feedback_max_attachments_allowed:I = 0x7f080215

.field public static final hockeyapp_feedback_message_hint:I = 0x7f080216

.field public static final hockeyapp_feedback_name_hint:I = 0x7f080217

.field public static final hockeyapp_feedback_refresh_button_text:I = 0x7f080218

.field public static final hockeyapp_feedback_response_button_text:I = 0x7f080219

.field public static final hockeyapp_feedback_select_file:I = 0x7f08021a

.field public static final hockeyapp_feedback_select_picture:I = 0x7f08021b

.field public static final hockeyapp_feedback_send_button_text:I = 0x7f08021c

.field public static final hockeyapp_feedback_send_generic_error:I = 0x7f08021d

.field public static final hockeyapp_feedback_send_network_error:I = 0x7f08021e

.field public static final hockeyapp_feedback_sending_feedback_text:I = 0x7f08021f

.field public static final hockeyapp_feedback_subject_hint:I = 0x7f080220

.field public static final hockeyapp_feedback_title:I = 0x7f080221

.field public static final hockeyapp_feedback_validate_email_empty:I = 0x7f080222

.field public static final hockeyapp_feedback_validate_email_error:I = 0x7f080223

.field public static final hockeyapp_feedback_validate_name_error:I = 0x7f080224

.field public static final hockeyapp_feedback_validate_subject_error:I = 0x7f080225

.field public static final hockeyapp_feedback_validate_text_error:I = 0x7f080226

.field public static final hockeyapp_login_email_hint:I = 0x7f080227

.field public static final hockeyapp_login_headline_text:I = 0x7f080228

.field public static final hockeyapp_login_headline_text_email_only:I = 0x7f080229

.field public static final hockeyapp_login_login_button_text:I = 0x7f08022a

.field public static final hockeyapp_login_missing_credentials_toast:I = 0x7f08022b

.field public static final hockeyapp_login_password_hint:I = 0x7f08022c

.field public static final hockeyapp_paint_dialog_message:I = 0x7f08022d

.field public static final hockeyapp_paint_dialog_negative_button:I = 0x7f08022e

.field public static final hockeyapp_paint_dialog_neutral_button:I = 0x7f08022f

.field public static final hockeyapp_paint_dialog_positive_button:I = 0x7f080230

.field public static final hockeyapp_paint_indicator_toast:I = 0x7f080231

.field public static final hockeyapp_paint_menu_clear:I = 0x7f080232

.field public static final hockeyapp_paint_menu_save:I = 0x7f080233

.field public static final hockeyapp_paint_menu_undo:I = 0x7f080234

.field public static final hockeyapp_permission_dialog_negative_button:I = 0x7f080235

.field public static final hockeyapp_permission_dialog_positive_button:I = 0x7f080236

.field public static final hockeyapp_permission_update_message:I = 0x7f080237

.field public static final hockeyapp_permission_update_title:I = 0x7f080238

.field public static final hockeyapp_update_button:I = 0x7f080239

.field public static final hockeyapp_update_dialog_message:I = 0x7f08023a

.field public static final hockeyapp_update_dialog_negative_button:I = 0x7f08023b

.field public static final hockeyapp_update_dialog_positive_button:I = 0x7f08023c

.field public static final hockeyapp_update_dialog_title:I = 0x7f08023d

.field public static final hockeyapp_update_mandatory_toast:I = 0x7f08023e

.field public static final hockeyapp_update_version_details_label:I = 0x7f08054a

.field public static final home:I = 0x7f08023f

.field public static final home_address_type:I = 0x7f080240

.field public static final home_fragment:I = 0x7f080241

.field public static final hours_and_minutes_format:I = 0x7f080242

.field public static final hours_format:I = 0x7f080243

.field public static final how_audio_works:I = 0x7f080244

.field public static final how_audio_works_desc:I = 0x7f080245

.field public static final how_to_enable_storage:I = 0x7f080246

.field public static final hud_local_music_browser_dialog_description:I = 0x7f080247

.field public static final hud_local_music_browser_tip:I = 0x7f080248

.field public static final hud_local_music_browser_tip_subtitle:I = 0x7f080249

.field public static final hud_settings:I = 0x7f08024a

.field public static final hud_sw_update:I = 0x7f08024b

.field public static final hud_version_is:I = 0x7f08024c

.field public static final hud_voice_search:I = 0x7f08024d

.field public static final hybrid_map:I = 0x7f08024e

.field public static final i_am_navdy:I = 0x7f080516

.field public static final i_can_do_this:I = 0x7f08024f

.field public static final i_don_t_know_your_home:I = 0x7f080250

.field public static final i_don_t_know_your_work:I = 0x7f080251

.field public static final i_have_mout_kit:I = 0x7f080252

.field public static final i_ll_zip_it_now:I = 0x7f080517

.field public static final i_love_you_too:I = 0x7f080518

.field public static final if_medium_try_tall_if_tall_contact_us:I = 0x7f080253

.field public static final if_tall_try_medium_if_medium_try_short:I = 0x7f080254

.field public static final illustration:I = 0x7f080255

.field public static final in_app_notification_connected_detail:I = 0x7f080256

.field public static final in_app_notification_connected_title:I = 0x7f080257

.field public static final info:I = 0x7f080258

.field public static final information:I = 0x7f080259

.field public static final insert_battery:I = 0x7f08025a

.field public static final insert_power_puck:I = 0x7f08025b

.field public static final install_complete:I = 0x7f08025c

.field public static final install_complete_line1:I = 0x7f08025d

.field public static final install_complete_line2:I = 0x7f08025e

.field public static final install_complete_line3:I = 0x7f08025f

.field public static final install_complete_line4:I = 0x7f080260

.field public static final install_complete_title:I = 0x7f080261

.field public static final install_display:I = 0x7f080262

.field public static final install_intro_car:I = 0x7f080263

.field public static final install_intro_navdy:I = 0x7f080264

.field public static final install_intro_phone:I = 0x7f080265

.field public static final install_intro_title:I = 0x7f080266

.field public static final install_now:I = 0x7f080267

.field public static final install_video:I = 0x7f080268

.field public static final installation:I = 0x7f080269

.field public static final installation_video:I = 0x7f08026a

.field public static final installation_video_desc:I = 0x7f08026b

.field public static final installing_dial_desc:I = 0x7f08026c

.field public static final installing_dial_title:I = 0x7f08026d

.field public static final invalid_address:I = 0x7f08026e

.field public static final is_the_lens_too_high:I = 0x7f08026f

.field public static final is_the_lens_too_low:I = 0x7f080270

.field public static final jira_credentials:I = 0x7f08054b

.field public static final key:I = 0x7f080271

.field public static final kilometers_abbrev:I = 0x7f080272

.field public static final large_file_over_network:I = 0x7f080273

.field public static final large_file_over_network_desc:I = 0x7f080274

.field public static final last_arrival_marker:I = 0x7f080275

.field public static final last_routed_on:I = 0x7f080276

.field public static final lat_long:I = 0x7f080277

.field public static final later:I = 0x7f080278

.field public static final latitude_is:I = 0x7f080279

.field public static final learn_how:I = 0x7f08027a

.field public static final learn_more:I = 0x7f08027b

.field public static final learn_more_caps:I = 0x7f08027c

.field public static final learning_gestures:I = 0x7f08027d

.field public static final learning_gestures_description:I = 0x7f08027e

.field public static final learning_gestures_subtitle:I = 0x7f08027f

.field public static final led_brightness:I = 0x7f080280

.field public static final lens_check_desc:I = 0x7f080281

.field public static final lens_check_title:I = 0x7f080282

.field public static final lens_is_too_high:I = 0x7f080283

.field public static final lens_is_too_low:I = 0x7f080284

.field public static final lens_ok:I = 0x7f080285

.field public static final lens_too_high:I = 0x7f080286

.field public static final lens_too_low:I = 0x7f080287

.field public static final less_info:I = 0x7f080288

.field public static final lets_get_going:I = 0x7f080289

.field public static final lets_try_short:I = 0x7f08028a

.field public static final loading_please_wait:I = 0x7f08028b

.field public static final localytics_app_key:I = 0x7f08054c

.field public static final locating_obd_port:I = 0x7f08028c

.field public static final location_explanation:I = 0x7f08028d

.field public static final longitude_is:I = 0x7f08028e

.field public static final looking_for_navdy:I = 0x7f08028f

.field public static final lowers_the_display:I = 0x7f080290

.field public static final main_debug:I = 0x7f080291

.field public static final main_gesture:I = 0x7f080292

.field public static final main_music:I = 0x7f080293

.field public static final main_navdy_dial:I = 0x7f080294

.field public static final main_recents:I = 0x7f080295

.field public static final main_record_drive:I = 0x7f080296

.field public static final main_search:I = 0x7f080297

.field public static final main_settings:I = 0x7f080298

.field public static final main_stop_record_drive:I = 0x7f080299

.field public static final main_submit_ticket:I = 0x7f08029a

.field public static final manual_entry:I = 0x7f08029b

.field public static final manual_entry_dialog_description:I = 0x7f08029c

.field public static final map_service_process_name:I = 0x7f08054d

.field public static final map_tilt:I = 0x7f08029d

.field public static final map_zoom_level:I = 0x7f08029e

.field public static final maps_API_MY_LOCATION_ACCURACY:I = 0x7f080043

.field public static final maps_API_OUTDATED_WARNING:I = 0x7f080044

.field public static final maps_BACK_TO_LIST:I = 0x7f080045

.field public static final maps_CLOSE_SOFTKEY:I = 0x7f080046

.field public static final maps_COMPASS_ALT_TEXT:I = 0x7f080047

.field public static final maps_DIRECTIONS_ALT_TEXT:I = 0x7f08005b

.field public static final maps_GOOGLE_MAP:I = 0x7f08005c

.field public static final maps_LEVEL_ALT_TEXT:I = 0x7f08005d

.field public static final maps_MY_LOCATION_ALT_TEXT:I = 0x7f080048

.field public static final maps_NO_GMM:I = 0x7f08054e

.field public static final maps_OPEN_GMM_ALT_TEXT:I = 0x7f08005e

.field public static final maps_STAR_ALT_TEXT:I = 0x7f08054f

.field public static final maps_YOUR_LOCATION:I = 0x7f08005f

.field public static final maps_ZOOM_IN_ALT_TEXT:I = 0x7f080049

.field public static final maps_ZOOM_OUT_ALT_TEXT:I = 0x7f08004a

.field public static final maps_dav_map_copyrights_full:I = 0x7f08004b

.field public static final maps_dav_map_copyrights_google_only:I = 0x7f08004c

.field public static final maps_dav_map_copyrights_imagery_only:I = 0x7f08004d

.field public static final maps_dav_map_copyrights_map_data_only:I = 0x7f08004e

.field public static final maps_invalid_panorama_data:I = 0x7f08004f

.field public static final maps_network_unavailable:I = 0x7f080050

.field public static final maps_no_panorama_data:I = 0x7f080051

.field public static final maps_panorama_disabled:I = 0x7f080052

.field public static final maps_service_unavailable:I = 0x7f080053

.field public static final maps_street_range_name_format:I = 0x7f080054

.field public static final maps_waiting_for_network:I = 0x7f080055

.field public static final marketing_effortless_control:I = 0x7f08029f

.field public static final marketing_effortless_control_desc:I = 0x7f0802a0

.field public static final marketing_get_started:I = 0x7f0802a1

.field public static final marketing_get_started_desc:I = 0x7f0802a2

.field public static final marketing_meet_navdy:I = 0x7f0802a3

.field public static final marketing_meet_navdy_desc:I = 0x7f0802a4

.field public static final marketing_never_miss_turn:I = 0x7f0802a5

.field public static final marketing_never_miss_turn_desc:I = 0x7f0802a6

.field public static final marketing_stay_connected:I = 0x7f0802a7

.field public static final marketing_stay_connected_desc:I = 0x7f0802a8

.field public static final max_photos_exceeded:I = 0x7f0802a9

.field public static final max_photos_exceeded_explanation:I = 0x7f0802aa

.field public static final medium_mount:I = 0x7f0802ab

.field public static final medium_or_tall_mount_desc:I = 0x7f0802ac

.field public static final medium_or_tall_mount_title:I = 0x7f0802ad

.field public static final medium_tall_mount:I = 0x7f0802ae

.field public static final menu:I = 0x7f0802af

.field public static final menu_app_version:I = 0x7f0802b0

.field public static final menu_audio:I = 0x7f0802b1

.field public static final menu_calendar:I = 0x7f0802b2

.field public static final menu_car:I = 0x7f0802b3

.field public static final menu_debug:I = 0x7f0802b4

.field public static final menu_delete:I = 0x7f0802b5

.field public static final menu_display_serial:I = 0x7f080550

.field public static final menu_display_version:I = 0x7f0802b6

.field public static final menu_drawer_close:I = 0x7f0802b7

.field public static final menu_drawer_open:I = 0x7f0802b8

.field public static final menu_edit:I = 0x7f0802b9

.field public static final menu_general:I = 0x7f0802ba

.field public static final menu_legal:I = 0x7f0802bb

.field public static final menu_messaging:I = 0x7f0802bc

.field public static final menu_navdy_display:I = 0x7f0802bd

.field public static final menu_navigation:I = 0x7f0802be

.field public static final menu_pending:I = 0x7f0802bf

.field public static final menu_profile:I = 0x7f0802c0

.field public static final menu_save:I = 0x7f0802c1

.field public static final menu_stop:I = 0x7f0802c2

.field public static final menu_support:I = 0x7f0802c3

.field public static final message_already_exists:I = 0x7f0802c4

.field public static final metadata_aws_account_id:I = 0x7f080551

.field public static final metadata_aws_secret:I = 0x7f080552

.field public static final metadata_car_md_auth:I = 0x7f080553

.field public static final metadata_car_md_token:I = 0x7f080554

.field public static final metadata_google_app_id:I = 0x7f080555

.field public static final metadata_google_backup_service_key:I = 0x7f080556

.field public static final metadata_google_web_service:I = 0x7f080557

.field public static final metadata_here_geo_app_id:I = 0x7f080558

.field public static final metadata_here_geo_app_token:I = 0x7f080559

.field public static final metadata_hockey_app_credentials:I = 0x7f08055a

.field public static final metadata_jira_credentials:I = 0x7f08055b

.field public static final metadata_zendesk_appid:I = 0x7f08055c

.field public static final metadata_zendesk_oauth:I = 0x7f08055d

.field public static final meters_abbrev:I = 0x7f0802c5

.field public static final mic_busy:I = 0x7f0802c6

.field public static final miles:I = 0x7f0802c7

.field public static final miles_abbrev:I = 0x7f0802c8

.field public static final min:I = 0x7f0802c9

.field public static final minutes_format:I = 0x7f0802ca

.field public static final mobile:I = 0x7f0802cb

.field public static final more_details_required:I = 0x7f0802cc

.field public static final more_info:I = 0x7f0802cd

.field public static final more_routes:I = 0x7f0802ce

.field public static final more_routes_capitalized:I = 0x7f0802cf

.field public static final mount_picker_description:I = 0x7f0802d0

.field public static final mount_picker_title:I = 0x7f0802d1

.field public static final mounts_explained:I = 0x7f0802d2

.field public static final music_control:I = 0x7f0802d3

.field public static final music_notification_access_label:I = 0x7f0802d4

.field public static final name:I = 0x7f0802d5

.field public static final name_plus_address:I = 0x7f0802d6

.field public static final nav_send_event:I = 0x7f0802d7

.field public static final nav_title:I = 0x7f0802d8

.field public static final navdy:I = 0x7f0802d9

.field public static final navdy_apps:I = 0x7f0802da

.field public static final navdy_connected:I = 0x7f0802db

.field public static final navdy_connecting:I = 0x7f0802dc

.field public static final navdy_connection_failure:I = 0x7f0802dd

.field public static final navdy_display:I = 0x7f0802de

.field public static final navdy_display_disconnected:I = 0x7f0802df

.field public static final navdy_display_up_to_date:I = 0x7f0802e0

.field public static final navdy_glances:I = 0x7f0802e1

.field public static final navdy_lost_connection:I = 0x7f0802e2

.field public static final navdy_not_connected:I = 0x7f0802e3

.field public static final navdy_software_update:I = 0x7f0802e4

.field public static final navigate_to_x:I = 0x7f0802e5

.field public static final navigation_control_label_pause:I = 0x7f0802e6

.field public static final navigation_control_label_resume:I = 0x7f0802e7

.field public static final navigation_control_label_simulate:I = 0x7f0802e8

.field public static final navigation_control_label_start:I = 0x7f0802e9

.field public static final navigation_control_label_stop:I = 0x7f0802ea

.field public static final need_help_contact_support:I = 0x7f0802eb

.field public static final need_internet_for_this_feature:I = 0x7f0802ec

.field public static final need_mic_permissions:I = 0x7f0802ed

.field public static final need_permissions:I = 0x7f0802ee

.field public static final need_to_review:I = 0x7f0802ef

.field public static final network_activity_no_connectivity:I = 0x7f080079

.field public static final network_issues:I = 0x7f0802f0

.field public static final new_box_description:I = 0x7f0802f1

.field public static final new_box_long_description:I = 0x7f0802f2

.field public static final new_box_plus_mounts_description:I = 0x7f0802f3

.field public static final new_box_plus_mounts_long_description:I = 0x7f0802f4

.field public static final new_box_plus_mounts_title:I = 0x7f0802f5

.field public static final new_box_title:I = 0x7f0802f6

.field public static final next:I = 0x7f0802f7

.field public static final next_right_arrow:I = 0x7f0802f8

.field public static final next_step:I = 0x7f0802f9

.field public static final next_trip:I = 0x7f0802fa

.field public static final next_trip_title:I = 0x7f0802fb

.field public static final no_browser:I = 0x7f0802fc

.field public static final no_camera_found:I = 0x7f0802fd

.field public static final no_connectivity:I = 0x7f0802fe

.field public static final no_devices_found:I = 0x7f0802ff

.field public static final no_google_play_services:I = 0x7f080300

.field public static final no_location:I = 0x7f080301

.field public static final no_mount_will_work_desc:I = 0x7f080302

.field public static final no_mount_will_work_title:I = 0x7f080303

.field public static final no_results_found:I = 0x7f080304

.field public static final no_results_found_for:I = 0x7f080305

.field public static final no_route:I = 0x7f080306

.field public static final no_thanks:I = 0x7f080307

.field public static final no_update_info:I = 0x7f080308

.field public static final no_voice_match:I = 0x7f080309

.field public static final north_up:I = 0x7f08030a

.field public static final not_connected_to_bluetooth:I = 0x7f08030b

.field public static final not_connected_to_bluetooth_summary:I = 0x7f08030c

.field public static final not_connected_to_device:I = 0x7f08030d

.field public static final not_enough_space:I = 0x7f08030e

.field public static final not_now:I = 0x7f08030f

.field public static final not_recommended:I = 0x7f080310

.field public static final not_sure_i_understand:I = 0x7f080311

.field public static final not_this_time:I = 0x7f080312

.field public static final notification_access_label:I = 0x7f080313

.field public static final notification_glances:I = 0x7f080314

.field public static final obd2_cable:I = 0x7f080315

.field public static final obd2_cable_description:I = 0x7f080316

.field public static final obd2_port_locator:I = 0x7f080317

.field public static final obd_location_unknown:I = 0x7f080318

.field public static final offline:I = 0x7f080319

.field public static final offline_mode:I = 0x7f08031a

.field public static final offline_mode_explanation:I = 0x7f08031b

.field public static final offline_mode_explanation_for_homescreen:I = 0x7f08031c

.field public static final offline_search:I = 0x7f08031d

.field public static final ok:I = 0x7f08031e

.field public static final ok_ending_trip:I = 0x7f080519

.field public static final ok_heres_what_i_found_for:I = 0x7f08031f

.field public static final ok_navigating_to_x:I = 0x7f080320

.field public static final old_box_description:I = 0x7f080321

.field public static final old_box_long_description:I = 0x7f080322

.field public static final old_box_title:I = 0x7f080323

.field public static final open_hour_titles:I = 0x7f080324

.field public static final open_now:I = 0x7f080325

.field public static final other:I = 0x7f080326

.field public static final other_address_type:I = 0x7f080327

.field public static final outdated_gms_message:I = 0x7f080328

.field public static final outdated_gms_title:I = 0x7f080329

.field public static final overview_title:I = 0x7f08032a

.field public static final pair_bluetooth:I = 0x7f08032b

.field public static final pairing_dial:I = 0x7f08032c

.field public static final pairing_with_navdy:I = 0x7f08032d

.field public static final pandora:I = 0x7f08032e

.field public static final parking:I = 0x7f08032f

.field public static final passenger_side__under_lower_left_side_of_glove_compartment:I = 0x7f080330

.field public static final password:I = 0x7f080331

.field public static final password_toggle_content_description:I = 0x7f08055e

.field public static final path_password_eye:I = 0x7f08055f

.field public static final path_password_eye_mask_strike_through:I = 0x7f080560

.field public static final path_password_eye_mask_visible:I = 0x7f080561

.field public static final path_password_strike_through:I = 0x7f080562

.field public static final pause:I = 0x7f080332

.field public static final pending_trip:I = 0x7f080333

.field public static final pending_trip_list_item:I = 0x7f080334

.field public static final pending_trip_prefix:I = 0x7f080335

.field public static final personalize_your_hud:I = 0x7f080336

.field public static final phone_calls:I = 0x7f080337

.field public static final phone_is_muted:I = 0x7f080338

.field public static final phone_number_with_type:I = 0x7f080563

.field public static final phone_speaker:I = 0x7f080339

.field public static final photo_upload_failed:I = 0x7f08033a

.field public static final photo_upload_succeeded:I = 0x7f08033b

.field public static final pick_a_place:I = 0x7f08033c

.field public static final pick_a_result:I = 0x7f08033d

.field public static final pick_a_route:I = 0x7f08033e

.field public static final place:I = 0x7f08033f

.field public static final place_autocomplete_clear_button:I = 0x7f08003d

.field public static final place_autocomplete_search_hint:I = 0x7f08003e

.field public static final place_details_contact_first_line:I = 0x7f080340

.field public static final place_details_contact_second_line:I = 0x7f080341

.field public static final place_display:I = 0x7f080342

.field public static final place_type_accounting:I = 0x7f080343

.field public static final place_type_airport:I = 0x7f080344

.field public static final place_type_amusement_park:I = 0x7f080345

.field public static final place_type_aquarium:I = 0x7f080346

.field public static final place_type_art_gallery:I = 0x7f080347

.field public static final place_type_atm:I = 0x7f080348

.field public static final place_type_bakery:I = 0x7f080349

.field public static final place_type_bank:I = 0x7f08034a

.field public static final place_type_bar:I = 0x7f08034b

.field public static final place_type_beauty_salon:I = 0x7f08034c

.field public static final place_type_bicycle_store:I = 0x7f08034d

.field public static final place_type_book_store:I = 0x7f08034e

.field public static final place_type_bowling_alley:I = 0x7f08034f

.field public static final place_type_bus_station:I = 0x7f080350

.field public static final place_type_cafe:I = 0x7f080351

.field public static final place_type_campground:I = 0x7f080352

.field public static final place_type_car_dealer:I = 0x7f080353

.field public static final place_type_car_rental:I = 0x7f080354

.field public static final place_type_car_repair:I = 0x7f080355

.field public static final place_type_car_wash:I = 0x7f080356

.field public static final place_type_casino:I = 0x7f080357

.field public static final place_type_cemetery:I = 0x7f080358

.field public static final place_type_church:I = 0x7f080359

.field public static final place_type_city_hall:I = 0x7f08035a

.field public static final place_type_clothing_store:I = 0x7f08035b

.field public static final place_type_convenience_store:I = 0x7f08035c

.field public static final place_type_courthouse:I = 0x7f08035d

.field public static final place_type_dentist:I = 0x7f08035e

.field public static final place_type_department_store:I = 0x7f08035f

.field public static final place_type_doctor:I = 0x7f080360

.field public static final place_type_electrician:I = 0x7f080361

.field public static final place_type_electronics_store:I = 0x7f080362

.field public static final place_type_embassy:I = 0x7f080363

.field public static final place_type_fire_station:I = 0x7f080364

.field public static final place_type_florist:I = 0x7f080365

.field public static final place_type_funeral_home:I = 0x7f080366

.field public static final place_type_furniture_store:I = 0x7f080367

.field public static final place_type_gas_station:I = 0x7f080368

.field public static final place_type_grocery_or_supermarket:I = 0x7f080369

.field public static final place_type_gym:I = 0x7f08036a

.field public static final place_type_hair_care:I = 0x7f08036b

.field public static final place_type_hardware_store:I = 0x7f08036c

.field public static final place_type_hindu_temple:I = 0x7f08036d

.field public static final place_type_home_goods_store:I = 0x7f08036e

.field public static final place_type_hospital:I = 0x7f08036f

.field public static final place_type_insurance_agency:I = 0x7f080370

.field public static final place_type_jewelry_store:I = 0x7f080371

.field public static final place_type_laundry:I = 0x7f080372

.field public static final place_type_lawyer:I = 0x7f080373

.field public static final place_type_library:I = 0x7f080374

.field public static final place_type_liquor_store:I = 0x7f080375

.field public static final place_type_local_government_office:I = 0x7f080376

.field public static final place_type_locksmith:I = 0x7f080377

.field public static final place_type_lodging:I = 0x7f080378

.field public static final place_type_meal_delivery:I = 0x7f080379

.field public static final place_type_meal_takeaway:I = 0x7f08037a

.field public static final place_type_mosque:I = 0x7f08037b

.field public static final place_type_movie_rental:I = 0x7f08037c

.field public static final place_type_movie_theater:I = 0x7f08037d

.field public static final place_type_moving_company:I = 0x7f08037e

.field public static final place_type_museum:I = 0x7f08037f

.field public static final place_type_night_club:I = 0x7f080380

.field public static final place_type_painter:I = 0x7f080381

.field public static final place_type_park:I = 0x7f080382

.field public static final place_type_parking:I = 0x7f080383

.field public static final place_type_pet_store:I = 0x7f080384

.field public static final place_type_pharmacy:I = 0x7f080385

.field public static final place_type_physiotherapist:I = 0x7f080386

.field public static final place_type_plumber:I = 0x7f080387

.field public static final place_type_police:I = 0x7f080388

.field public static final place_type_post_office:I = 0x7f080389

.field public static final place_type_real_estate_agency:I = 0x7f08038a

.field public static final place_type_restaurant:I = 0x7f08038b

.field public static final place_type_roofing_contractor:I = 0x7f08038c

.field public static final place_type_rv_park:I = 0x7f08038d

.field public static final place_type_school:I = 0x7f08038e

.field public static final place_type_shoe_store:I = 0x7f08038f

.field public static final place_type_shopping_mall:I = 0x7f080390

.field public static final place_type_spa:I = 0x7f080391

.field public static final place_type_stadium:I = 0x7f080392

.field public static final place_type_storage:I = 0x7f080393

.field public static final place_type_store:I = 0x7f080394

.field public static final place_type_subway_station:I = 0x7f080395

.field public static final place_type_synagogue:I = 0x7f080396

.field public static final place_type_taxi_stand:I = 0x7f080397

.field public static final place_type_train_station:I = 0x7f080398

.field public static final place_type_travel_agency:I = 0x7f080399

.field public static final place_type_university:I = 0x7f08039a

.field public static final place_type_veterinary_care:I = 0x7f08039b

.field public static final place_type_zoo:I = 0x7f08039c

.field public static final play:I = 0x7f08039d

.field public static final play_sequence:I = 0x7f08039e

.field public static final playing_sequence:I = 0x7f08039f

.field public static final please_connect_to_your_hud:I = 0x7f0803a0

.field public static final please_enable_hud_location:I = 0x7f0803a1

.field public static final please_enable_location:I = 0x7f0803a2

.field public static final please_enter_car_info:I = 0x7f0803a3

.field public static final please_enter_password:I = 0x7f0803a4

.field public static final please_enter_title:I = 0x7f0803a5

.field public static final please_fill_out_description:I = 0x7f0803a6

.field public static final please_look_at_the_hud:I = 0x7f0803a7

.field public static final please_make_sure_you_have_internet:I = 0x7f0803a8

.field public static final please_wait:I = 0x7f0803a9

.field public static final plug_cable_desc:I = 0x7f0803aa

.field public static final plug_cable_title:I = 0x7f0803ab

.field public static final plug_in_cla:I = 0x7f0803ac

.field public static final plugging_in_navdy:I = 0x7f0803ad

.field public static final powered_by_google:I = 0x7f080564

.field public static final pref_map_search_provider_default:I = 0x7f080565

.field public static final pref_map_search_provider_title:I = 0x7f0803ae

.field public static final pref_maps_search_provider:I = 0x7f080566

.field public static final prev:I = 0x7f0803af

.field public static final privacy_policy:I = 0x7f0803b0

.field public static final problem_sending_route:I = 0x7f0803b1

.field public static final progress:I = 0x7f0803b2

.field public static final progress_activity:I = 0x7f0803b3

.field public static final project:I = 0x7f0803b4

.field public static final purchase_link:I = 0x7f0803b5

.field public static final raises_the_display:I = 0x7f0803b6

.field public static final rate_my_app_dialog_dismiss_action_label:I = 0x7f08007a

.field public static final rate_my_app_dialog_feedback_description_label:I = 0x7f08007b

.field public static final rate_my_app_dialog_feedback_device_api_version:I = 0x7f08007c

.field public static final rate_my_app_dialog_feedback_device_memory:I = 0x7f08007d

.field public static final rate_my_app_dialog_feedback_device_model:I = 0x7f08007e

.field public static final rate_my_app_dialog_feedback_device_name:I = 0x7f08007f

.field public static final rate_my_app_dialog_feedback_device_os_version:I = 0x7f080080

.field public static final rate_my_app_dialog_feedback_request_subject:I = 0x7f080081

.field public static final rate_my_app_dialog_feedback_send_error_no_connectivity_toast:I = 0x7f080082

.field public static final rate_my_app_dialog_feedback_send_error_toast:I = 0x7f080083

.field public static final rate_my_app_dialog_feedback_send_success_toast:I = 0x7f080084

.field public static final rate_my_app_dialog_feedback_title_label:I = 0x7f080085

.field public static final rate_my_app_dialog_negative_action_label:I = 0x7f080086

.field public static final rate_my_app_dialog_positive_action_label:I = 0x7f080087

.field public static final rate_my_app_dialog_title_label:I = 0x7f080088

.field public static final rate_my_app_dialogue_feedback_cancel_button_label:I = 0x7f080089

.field public static final rate_my_app_dialogue_feedback_issue_hint:I = 0x7f08008a

.field public static final rate_my_app_dialogue_feedback_send_button_label:I = 0x7f08008b

.field public static final read_aloud:I = 0x7f0803b7

.field public static final read_aloud_and_show_content:I = 0x7f0803b8

.field public static final ready_to_install:I = 0x7f0803b9

.field public static final ready_to_install_desc:I = 0x7f0803ba

.field public static final ready_to_transfer_update:I = 0x7f0803bb

.field public static final ready_to_transfer_update_desc:I = 0x7f0803bc

.field public static final rear_center_console___left_of_ashtray:I = 0x7f0803bd

.field public static final recap_Securing_mount_desc:I = 0x7f0803be

.field public static final recap_Securing_mount_title:I = 0x7f0803bf

.field public static final recap_installing_dial_desc:I = 0x7f0803c0

.field public static final recap_installing_dial_title:I = 0x7f0803c1

.field public static final recap_lens_check_desc:I = 0x7f0803c2

.field public static final recap_lens_check_title:I = 0x7f0803c3

.field public static final recap_medium_or_tall_mount_desc:I = 0x7f0803c4

.field public static final recap_medium_or_tall_mount_title:I = 0x7f0803c5

.field public static final recap_overview_desc:I = 0x7f0803c6

.field public static final recap_overview_title:I = 0x7f0803c7

.field public static final recap_plug_cable_desc:I = 0x7f0803c8

.field public static final recap_plug_cable_title:I = 0x7f0803c9

.field public static final recap_short_mount_desc:I = 0x7f0803ca

.field public static final recap_short_mount_title:I = 0x7f0803cb

.field public static final recap_tidying_up_desc:I = 0x7f0803cc

.field public static final recap_tidying_up_title:I = 0x7f0803cd

.field public static final recap_turn_on_desc:I = 0x7f0803ce

.field public static final recap_turn_on_title:I = 0x7f0803cf

.field public static final recent_places_matching:I = 0x7f0803d0

.field public static final recommended_for_your_car:I = 0x7f0803d1

.field public static final refresh:I = 0x7f0803d2

.field public static final refresh_list:I = 0x7f0803d3

.field public static final refreshing:I = 0x7f080567

.field public static final release_notes:I = 0x7f0803d4

.field public static final remove_protective_sleeve:I = 0x7f0803d5

.field public static final request_list_activity_title:I = 0x7f08008c

.field public static final request_list_fragment_error_message:I = 0x7f08008d

.field public static final request_list_fragment_no_requests:I = 0x7f08008e

.field public static final request_new_route_dialog_message:I = 0x7f0803d6

.field public static final request_new_route_dialog_title:I = 0x7f0803d7

.field public static final request_route:I = 0x7f0803d8

.field public static final request_sent:I = 0x7f0803d9

.field public static final request_sent_to_navdy:I = 0x7f0803da

.field public static final request_timeout:I = 0x7f0803db

.field public static final request_view_comment_entry_hint:I = 0x7f08008f

.field public static final retry_button:I = 0x7f0803dc

.field public static final retry_view_button_label:I = 0x7f080090

.field public static final route:I = 0x7f0803dd

.field public static final routing:I = 0x7f0803de

.field public static final row_request_unread_indicator_alt:I = 0x7f080091

.field public static final samsung_calendar:I = 0x7f0803df

.field public static final save_car:I = 0x7f0803e0

.field public static final save_info:I = 0x7f0803e1

.field public static final save_pending_trip:I = 0x7f0803e2

.field public static final saved_preferences:I = 0x7f0803e3

.field public static final screen_config_reset:I = 0x7f0803e4

.field public static final screen_config_vertical_position:I = 0x7f0803e5

.field public static final search_activity:I = 0x7f0803e6

.field public static final search_failed:I = 0x7f0803e7

.field public static final search_for_route:I = 0x7f0803e8

.field public static final search_hint:I = 0x7f0803e9

.field public static final search_hint_favorite:I = 0x7f0803ea

.field public static final search_hint_home:I = 0x7f0803eb

.field public static final search_hint_work:I = 0x7f0803ec

.field public static final search_history_header:I = 0x7f0803ed

.field public static final search_menu_title:I = 0x7f08003f

.field public static final search_or_say_address:I = 0x7f0803ee

.field public static final search_powered_by:I = 0x7f080568

.field public static final search_results:I = 0x7f0803ef

.field public static final sections_list_fragment_error_message:I = 0x7f080092

.field public static final sections_list_fragment_no_sections_found:I = 0x7f080093

.field public static final secure_the_anchor:I = 0x7f0803f0

.field public static final secure_the_mount:I = 0x7f0803f1

.field public static final see_a_glance:I = 0x7f0803f2

.field public static final see_my_port_location:I = 0x7f0803f3

.field public static final select_a_mount_to_continue:I = 0x7f0803f4

.field public static final select_a_topic:I = 0x7f0803f5

.field public static final select_a_topic_bulletpoint:I = 0x7f0803f6

.field public static final select_box:I = 0x7f0803f7

.field public static final select_navdy:I = 0x7f0803f8

.field public static final select_problem_type:I = 0x7f0803f9

.field public static final select_voice:I = 0x7f0803fa

.field public static final select_your_box:I = 0x7f0803fb

.field public static final select_your_box_desc:I = 0x7f0803fc

.field public static final select_your_car:I = 0x7f0803fd

.field public static final select_your_cord:I = 0x7f0803fe

.field public static final selected_place:I = 0x7f0803ff

.field public static final send_logs:I = 0x7f080400

.field public static final send_once:I = 0x7f080401

.field public static final send_to_navdy:I = 0x7f080402

.field public static final sending_to_navdy:I = 0x7f080403

.field public static final server_error:I = 0x7f080404

.field public static final set_as_home_btn:I = 0x7f080405

.field public static final set_as_work_btn:I = 0x7f080406

.field public static final settings:I = 0x7f080407

.field public static final settings_adjust_screen:I = 0x7f080408

.field public static final settings_advanced_section:I = 0x7f080409

.field public static final settings_all_updated_failure:I = 0x7f08040a

.field public static final settings_all_updated_successfully:I = 0x7f08040b

.field public static final settings_audio_automatic:I = 0x7f08040c

.field public static final settings_audio_automatic_description:I = 0x7f08040d

.field public static final settings_audio_bluetooth:I = 0x7f08040e

.field public static final settings_audio_camera_warnings:I = 0x7f08040f

.field public static final settings_audio_failed:I = 0x7f080410

.field public static final settings_audio_loud_volume:I = 0x7f080411

.field public static final settings_audio_low_volume:I = 0x7f080412

.field public static final settings_audio_normal_volume:I = 0x7f080413

.field public static final settings_audio_output:I = 0x7f080414

.field public static final settings_audio_phone:I = 0x7f080415

.field public static final settings_audio_play_as_phone_call:I = 0x7f080416

.field public static final settings_audio_play_as_phone_call_description:I = 0x7f080417

.field public static final settings_audio_speaker:I = 0x7f080418

.field public static final settings_audio_speech_delay:I = 0x7f080419

.field public static final settings_audio_speech_settings:I = 0x7f08041a

.field public static final settings_audio_speed_warnings:I = 0x7f08041b

.field public static final settings_audio_status_playing_audio_test:I = 0x7f08041c

.field public static final settings_audio_succeeded:I = 0x7f08041d

.field public static final settings_audio_test_audio:I = 0x7f08041e

.field public static final settings_audio_title:I = 0x7f08041f

.field public static final settings_audio_turn_by_turn_navigation:I = 0x7f080420

.field public static final settings_audio_voice:I = 0x7f080421

.field public static final settings_audio_volume_illustration:I = 0x7f080422

.field public static final settings_audio_welcome_message:I = 0x7f080423

.field public static final settings_audio_what_to_play:I = 0x7f080424

.field public static final settings_car_auto_on:I = 0x7f080425

.field public static final settings_car_auto_on_desc:I = 0x7f080426

.field public static final settings_car_compact:I = 0x7f080427

.field public static final settings_car_display_ui_scaling:I = 0x7f080428

.field public static final settings_car_display_ui_scaling_desc:I = 0x7f080429

.field public static final settings_car_edit_failed:I = 0x7f08042a

.field public static final settings_car_edit_succeeded:I = 0x7f08042b

.field public static final settings_car_normal:I = 0x7f08042c

.field public static final settings_car_obd_description:I = 0x7f08042d

.field public static final settings_car_obd_title:I = 0x7f08042e

.field public static final settings_connect:I = 0x7f08042f

.field public static final settings_dial_long_press:I = 0x7f080430

.field public static final settings_general_dial_long_press_google_now:I = 0x7f080431

.field public static final settings_general_dial_long_press_place_search:I = 0x7f080432

.field public static final settings_general_limit_cell_data_desc:I = 0x7f080433

.field public static final settings_general_limit_cell_data_title:I = 0x7f080434

.field public static final settings_general_smart_suggestions_desc:I = 0x7f080435

.field public static final settings_general_smart_suggestions_title:I = 0x7f080436

.field public static final settings_general_unit_system:I = 0x7f080437

.field public static final settings_general_unit_system_imperial:I = 0x7f080438

.field public static final settings_general_unit_system_metric:I = 0x7f080439

.field public static final settings_gesture_engine:I = 0x7f080569

.field public static final settings_gesture_preview:I = 0x7f08056a

.field public static final settings_hud_buy_a_hud:I = 0x7f08043a

.field public static final settings_messaging_description:I = 0x7f08043b

.field public static final settings_messaging_failed:I = 0x7f08043c

.field public static final settings_messaging_succeeded:I = 0x7f08043d

.field public static final settings_messaging_title:I = 0x7f08043e

.field public static final settings_navigation_auto_recalc:I = 0x7f08043f

.field public static final settings_navigation_auto_recalc_desc:I = 0x7f080440

.field public static final settings_navigation_auto_trains:I = 0x7f080441

.field public static final settings_navigation_current_region:I = 0x7f080442

.field public static final settings_navigation_failed:I = 0x7f080443

.field public static final settings_navigation_ferries:I = 0x7f080444

.field public static final settings_navigation_highways:I = 0x7f080445

.field public static final settings_navigation_maps:I = 0x7f080446

.field public static final settings_navigation_offline_maps:I = 0x7f080447

.field public static final settings_navigation_route_calculation:I = 0x7f080448

.field public static final settings_navigation_route_calculation_fastest:I = 0x7f080449

.field public static final settings_navigation_route_calculation_shortest:I = 0x7f08044a

.field public static final settings_navigation_route_preferences:I = 0x7f08044b

.field public static final settings_navigation_routing:I = 0x7f08044c

.field public static final settings_navigation_show_traffic:I = 0x7f08044d

.field public static final settings_navigation_smart_traffic:I = 0x7f08044e

.field public static final settings_navigation_succeeded:I = 0x7f08044f

.field public static final settings_navigation_toll_roads:I = 0x7f080450

.field public static final settings_navigation_traffic:I = 0x7f080451

.field public static final settings_navigation_tunnels:I = 0x7f080452

.field public static final settings_navigation_units:I = 0x7f080453

.field public static final settings_navigation_unpaved_roads:I = 0x7f080454

.field public static final settings_need_to_be_connected_to_hud:I = 0x7f080455

.field public static final settings_obd_warning_description:I = 0x7f080456

.field public static final settings_obd_warning_title:I = 0x7f080457

.field public static final settings_ota_connecting_to_server:I = 0x7f080458

.field public static final settings_ota_failed:I = 0x7f080459

.field public static final settings_ota_options:I = 0x7f08045a

.field public static final settings_ota_succeeded:I = 0x7f08045b

.field public static final settings_profile_about_you:I = 0x7f08045c

.field public static final settings_profile_change_photo_btn:I = 0x7f08045d

.field public static final settings_profile_delete_account_btn:I = 0x7f08045e

.field public static final settings_profile_delete_succeeded:I = 0x7f08045f

.field public static final settings_profile_edit_car_info:I = 0x7f080460

.field public static final settings_profile_edit_failed:I = 0x7f080461

.field public static final settings_profile_edit_invalid_email:I = 0x7f080462

.field public static final settings_profile_edit_invalid_name:I = 0x7f080463

.field public static final settings_profile_edit_succeeded:I = 0x7f080464

.field public static final settings_profile_email_address_label:I = 0x7f080465

.field public static final settings_profile_image_too_large:I = 0x7f080466

.field public static final settings_profile_make_label:I = 0x7f080467

.field public static final settings_profile_model_label:I = 0x7f080468

.field public static final settings_profile_name_label:I = 0x7f080469

.field public static final settings_profile_public:I = 0x7f08046a

.field public static final settings_profile_vehicle_info:I = 0x7f08046b

.field public static final settings_profile_year_label:I = 0x7f08046c

.field public static final settings_report_a_problem_for:I = 0x7f08046d

.field public static final settings_report_a_problem_go_to_navdy:I = 0x7f08046e

.field public static final settings_report_a_problem_header:I = 0x7f08046f

.field public static final settings_report_a_problem_hint:I = 0x7f080470

.field public static final settings_report_a_problem_log_attachment_hint:I = 0x7f080471

.field public static final settings_report_a_problem_more_details:I = 0x7f080472

.field public static final settings_report_a_problem_submit:I = 0x7f080473

.field public static final settings_report_a_problem_title:I = 0x7f080474

.field public static final settings_video_loop:I = 0x7f08056b

.field public static final shared_by:I = 0x7f080475

.field public static final short_mount:I = 0x7f080476

.field public static final short_mount_desc:I = 0x7f080477

.field public static final short_mount_title:I = 0x7f080478

.field public static final show_all_calendars:I = 0x7f080479

.field public static final show_all_tips:I = 0x7f08047a

.field public static final show_content:I = 0x7f08047b

.field public static final show_on_map:I = 0x7f08047c

.field public static final sign_in:I = 0x7f08047d

.field public static final size:I = 0x7f08047e

.field public static final slack:I = 0x7f08047f

.field public static final small_cable_clips:I = 0x7f080480

.field public static final something_went_wrong:I = 0x7f080481

.field public static final sorry_to_hear_that:I = 0x7f08051a

.field public static final space:I = 0x7f08056c

.field public static final speech_delay:I = 0x7f080482

.field public static final speech_delay_message:I = 0x7f080483

.field public static final speech_prompt:I = 0x7f080484

.field public static final spotify:I = 0x7f080485

.field public static final start_install:I = 0x7f080486

.field public static final start_installation:I = 0x7f080487

.field public static final start_navigation:I = 0x7f080488

.field public static final start_sim:I = 0x7f080489

.field public static final status_bar_notification_info_overflow:I = 0x7f080040

.field public static final stop_navigation:I = 0x7f08048a

.field public static final storage_permission_not_granted:I = 0x7f08048b

.field public static final store_picture_message:I = 0x7f080041

.field public static final store_picture_title:I = 0x7f080042

.field public static final stream_volume_muted:I = 0x7f08048c

.field public static final submit:I = 0x7f08048d

.field public static final submit_jira_ticket:I = 0x7f08048e

.field public static final success:I = 0x7f08048f

.field public static final suggested_places:I = 0x7f080490

.field public static final suggestion_footer:I = 0x7f080491

.field public static final suggestion_footer_title:I = 0x7f080492

.field public static final summary:I = 0x7f080493

.field public static final support_activity_title:I = 0x7f080094

.field public static final support_activity_unable_to_contact_support:I = 0x7f080095

.field public static final support_contact_menu:I = 0x7f080096

.field public static final support_conversations_menu:I = 0x7f080097

.field public static final support_list_search_hint:I = 0x7f080098

.field public static final sw_update_available:I = 0x7f080494

.field public static final take_a_photo:I = 0x7f080495

.field public static final tall_mount:I = 0x7f080496

.field public static final tap_a_pin:I = 0x7f080497

.field public static final tap_anywhere_to_stop:I = 0x7f080498

.field public static final tap_to_add_new_favorite:I = 0x7f080499

.field public static final tap_to_disconnect:I = 0x7f08049a

.field public static final tap_to_download:I = 0x7f08049b

.field public static final tap_to_reconnect:I = 0x7f08049c

.field public static final tap_to_set_car_info:I = 0x7f08049d

.field public static final tap_to_set_home_address:I = 0x7f08049e

.field public static final tap_to_set_work_address:I = 0x7f08049f

.field public static final terms_of_service:I = 0x7f0804a0

.field public static final text_sms:I = 0x7f0804a1

.field public static final text_with_parentheses:I = 0x7f0804a2

.field public static final the_medium_and_tall_mounts_are_not_recommended:I = 0x7f0804a3

.field public static final the_medium_and_tall_mounts_are_not_recommended_description:I = 0x7f0804a4

.field public static final the_short_mount_is_not_recommended:I = 0x7f0804a5

.field public static final the_short_mount_is_not_recommended_description:I = 0x7f0804a6

.field public static final this_and_that:I = 0x7f0804a7

.field public static final ticket_description_boilerplate:I = 0x7f0804a8

.field public static final ticket_submission_success_title:I = 0x7f0804a9

.field public static final tidying_up_desc:I = 0x7f0804aa

.field public static final tidying_up_title:I = 0x7f0804ab

.field public static final timed_out:I = 0x7f0804ac

.field public static final tip:I = 0x7f0804ad

.field public static final title:I = 0x7f0804ae

.field public static final title_activity_bluetooth:I = 0x7f08056d

.field public static final title_activity_details:I = 0x7f0804af

.field public static final title_activity_google_address_picker:I = 0x7f08056e

.field public static final title_activity_homescreen:I = 0x7f0804b0

.field public static final title_activity_main:I = 0x7f0804b1

.field public static final title_activity_rate_my_app_dialogue_test:I = 0x7f08056f

.field public static final title_activity_search:I = 0x7f0804b2

.field public static final title_activity_settings_bluetooth:I = 0x7f0804b3

.field public static final title_activity_signup:I = 0x7f080570

.field public static final title_address_search:I = 0x7f0804b4

.field public static final title_device_picker:I = 0x7f0804b5

.field public static final title_mocked_position_search:I = 0x7f0804b6

.field public static final title_more_routes_activity:I = 0x7f0804b7

.field public static final title_navigate:I = 0x7f0804b8

.field public static final title_send_to_navdy_activity:I = 0x7f0804b9

.field public static final title_settings:I = 0x7f0804ba

.field public static final toast_favorite_delete_error:I = 0x7f0804bb

.field public static final toast_favorite_edit_error:I = 0x7f0804bc

.field public static final toast_favorite_edit_saved:I = 0x7f0804bd

.field public static final today:I = 0x7f0804be

.field public static final trace_file_path:I = 0x7f0804bf

.field public static final traffic:I = 0x7f0804c0

.field public static final transfer_failed:I = 0x7f0804c1

.field public static final transfer_progress:I = 0x7f0804c2

.field public static final transferring_update:I = 0x7f0804c3

.field public static final transferring_update_desc:I = 0x7f0804c4

.field public static final trips_fragment:I = 0x7f0804c5

.field public static final try_again:I = 0x7f0804c6

.field public static final try_gestures:I = 0x7f0804c7

.field public static final try_medium_and_tall_mount:I = 0x7f0804c8

.field public static final try_raising_seat:I = 0x7f0804c9

.field public static final turn_on_check1:I = 0x7f0804ca

.field public static final turn_on_check2:I = 0x7f0804cb

.field public static final turn_on_check3:I = 0x7f0804cc

.field public static final turn_on_check4:I = 0x7f0804cd

.field public static final turn_on_desc:I = 0x7f0804ce

.field public static final turn_on_title:I = 0x7f0804cf

.field public static final turn_volume_up:I = 0x7f0804d0

.field public static final turned_up_the_volume:I = 0x7f0804d1

.field public static final twitter:I = 0x7f0804d2

.field public static final typical_setup:I = 0x7f0804d3

.field public static final unable_to_find_a_route:I = 0x7f0804d4

.field public static final unable_to_find_a_route_desc:I = 0x7f0804d5

.field public static final unable_to_hear_any_search:I = 0x7f0804d6

.field public static final unable_to_load_webview:I = 0x7f0804d7

.field public static final unable_to_remove_search_history:I = 0x7f0804d8

.field public static final uncovered:I = 0x7f0804d9

.field public static final unknown:I = 0x7f0804da

.field public static final unknown_status:I = 0x7f0804db

.field public static final unset_destination_label:I = 0x7f0804dc

.field public static final unstable_disclaimer:I = 0x7f0804dd

.field public static final unstable_version_title:I = 0x7f0804de

.field public static final up_to_date:I = 0x7f0804df

.field public static final update_available:I = 0x7f0804e0

.field public static final update_info:I = 0x7f0804e1

.field public static final update_now:I = 0x7f0804e2

.field public static final update_version_text:I = 0x7f0804e3

.field public static final updating_location:I = 0x7f0804e4

.field public static final upload_a_photo:I = 0x7f0804e5

.field public static final upload_to_device_failed:I = 0x7f0804e6

.field public static final upload_trip_data:I = 0x7f0804e7

.field public static final use_special_hud_voice_commands:I = 0x7f0804e8

.field public static final use_the_velcro_strap:I = 0x7f0804e9

.field public static final username:I = 0x7f0804ea

.field public static final via_route:I = 0x7f0804eb

.field public static final view_article_attachments_error:I = 0x7f080099

.field public static final view_article_seperator:I = 0x7f08009a

.field public static final view_photo:I = 0x7f0804ec

.field public static final view_request_agent_avatar_imageview_alt:I = 0x7f08009b

.field public static final view_request_load_comments_error:I = 0x7f08009c

.field public static final view_request_send_comment_error:I = 0x7f08009d

.field public static final voice_search:I = 0x7f0804ed

.field public static final voice_search_description:I = 0x7f0804ee

.field public static final voice_search_description_long:I = 0x7f0804ef

.field public static final voice_search_dialog_description:I = 0x7f0804f0

.field public static final waiting_for_navdy:I = 0x7f0804f1

.field public static final warning:I = 0x7f0804f2

.field public static final watch_feature_video:I = 0x7f0804f3

.field public static final watch_obd_video:I = 0x7f0804f4

.field public static final watch_this_in_depth_demo:I = 0x7f0804f5

.field public static final watch_video:I = 0x7f0804f6

.field public static final watched:I = 0x7f0804f7

.field public static final we_are_downloading_update:I = 0x7f0804f8

.field public static final we_need_a_bit_more:I = 0x7f0804f9

.field public static final we_need_storage_permission_for_tickets:I = 0x7f0804fa

.field public static final website:I = 0x7f0804fb

.field public static final what_ended_up_working:I = 0x7f0804fc

.field public static final what_ended_up_working_desc:I = 0x7f0804fd

.field public static final whats_in_the_box:I = 0x7f0804fe

.field public static final whats_in_the_mount_kit_box:I = 0x7f0804ff

.field public static final whatsapp:I = 0x7f080500

.field public static final with_heavy_traffic:I = 0x7f080501

.field public static final with_normal_traffic:I = 0x7f080502

.field public static final work:I = 0x7f080503

.field public static final work_address_type:I = 0x7f080504

.field public static final yes_button:I = 0x7f080505

.field public static final you_will_need_the_mount_kit_description:I = 0x7f080506

.field public static final you_will_need_the_mount_kit_title:I = 0x7f080507

.field public static final youll_arrive_late:I = 0x7f080508

.field public static final youll_arrive_right_when_it_starts:I = 0x7f080509

.field public static final zendesk_appid:I = 0x7f080571

.field public static final zendesk_currently_resending_toast:I = 0x7f08050a

.field public static final zendesk_jwt_secret:I = 0x7f080572

.field public static final zendesk_notification_failure_message:I = 0x7f08050b

.field public static final zendesk_notification_failure_title:I = 0x7f08050c

.field public static final zendesk_oauth:I = 0x7f080573

.field public static final zendesk_retry_negative_message:I = 0x7f08050d

.field public static final zendesk_retry_positive_message:I = 0x7f08050e

.field public static final zendesk_submit_succeeded:I = 0x7f08050f

.field public static final zendesk_ticket_readme_message:I = 0x7f080510

.field public static final zendesk_ticket_upload_in_progress:I = 0x7f080511

.field public static final zendesk_unable_to_submit_x_tickets:I = 0x7f080512

.field public static final zendesk_upload_support_request:I = 0x7f080513

.field public static final zendesk_url:I = 0x7f080514


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
