.class public final Lcom/google/android/gms/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final adjust_height:I = 0x7f10005c

.field public static final adjust_width:I = 0x7f10005d

.field public static final auto:I = 0x7f100032

.field public static final button:I = 0x7f100132

.field public static final center:I = 0x7f100034

.field public static final contact:I = 0x7f100057

.field public static final dark:I = 0x7f100072

.field public static final date:I = 0x7f100064

.field public static final demote_common_words:I = 0x7f100069

.field public static final demote_rfc822_hostnames:I = 0x7f10006a

.field public static final email:I = 0x7f100058

.field public static final html:I = 0x7f100065

.field public static final hybrid:I = 0x7f10005e

.field public static final icon_only:I = 0x7f10006f

.field public static final icon_uri:I = 0x7f100049

.field public static final index_entity_types:I = 0x7f10006b

.field public static final instant_message:I = 0x7f100059

.field public static final intent_action:I = 0x7f10004a

.field public static final intent_activity:I = 0x7f10004b

.field public static final intent_data:I = 0x7f10004c

.field public static final intent_data_id:I = 0x7f10004d

.field public static final intent_extra_data:I = 0x7f10004e

.field public static final large_icon_uri:I = 0x7f10004f

.field public static final light:I = 0x7f100073

.field public static final match_global_nicknames:I = 0x7f10006c

.field public static final none:I = 0x7f10001c

.field public static final normal:I = 0x7f10001e

.field public static final omnibox_title_section:I = 0x7f10006d

.field public static final omnibox_url_section:I = 0x7f10006e

.field public static final place_autocomplete_clear_button:I = 0x7f10032f

.field public static final place_autocomplete_powered_by_google:I = 0x7f100331

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f100333

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f100334

.field public static final place_autocomplete_progress:I = 0x7f100332

.field public static final place_autocomplete_search_button:I = 0x7f10032d

.field public static final place_autocomplete_search_input:I = 0x7f10032e

.field public static final place_autocomplete_separator:I = 0x7f100330

.field public static final plain:I = 0x7f100066

.field public static final radio:I = 0x7f100092

.field public static final rfc822:I = 0x7f100067

.field public static final satellite:I = 0x7f10005f

.field public static final standard:I = 0x7f100070

.field public static final terrain:I = 0x7f100060

.field public static final text:I = 0x7f10000d

.field public static final text1:I = 0x7f100050

.field public static final text2:I = 0x7f100051

.field public static final thing_proto:I = 0x7f100052

.field public static final url:I = 0x7f100068

.field public static final wide:I = 0x7f100071

.field public static final wrap_content:I = 0x7f100031


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
