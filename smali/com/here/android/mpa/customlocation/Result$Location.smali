.class public final Lcom/here/android/mpa/customlocation/Result$Location;
.super Ljava/lang/Object;
.source "Result.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/customlocation/Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Location"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Lcom/here/android/mpa/common/GeoCoordinate;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/customlocation/CustomAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:F

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/here/android/mpa/common/GeoCoordinate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/here/android/mpa/customlocation/Result$Location;F)F
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->d:F

    return p1
.end method

.method static synthetic a(Lcom/here/android/mpa/customlocation/Result$Location;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->a:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object p1
.end method

.method static synthetic a(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/here/android/mpa/customlocation/Result$Location;)Ljava/util/List;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/here/android/mpa/customlocation/Result$Location;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->s:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object p1
.end method

.method static synthetic b(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic e(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->r:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic f(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic g(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic h(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic i(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic j(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic k(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->i:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic l(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->j:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic m(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic n(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic o(Lcom/here/android/mpa/customlocation/Result$Location;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/here/android/mpa/customlocation/Result$Location;->l:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getCity()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getCounty()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomAttributes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/customlocation/CustomAttribute;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->b:Ljava/util/List;

    return-object v0
.end method

.method public getCustomerLocationId()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 134
    iget v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->d:F

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getFaxNumber()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->a:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method public getHouseNumber()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getName1()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getName2()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getName3()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 239
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getRouteGeoCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->s:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getStreetName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/here/android/mpa/customlocation/Result$Location;->l:Ljava/lang/String;

    return-object v0
.end method
