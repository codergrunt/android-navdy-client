.class public Lcom/nimbusds/jose/crypto/ECDHEncrypter;
.super Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;
.source "ECDHEncrypter.java"

# interfaces
.implements Lcom/nimbusds/jose/JWEEncrypter;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# instance fields
.field private final publicKey:Ljava/security/interfaces/ECPublicKey;


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey;)V
    .locals 1
    .param p1, "ecJWK"    # Lcom/nimbusds/jose/jwk/ECKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/ECKey;->getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;)V

    .line 93
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/ECKey;->toECPublicKey()Ljava/security/interfaces/ECPublicKey;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->publicKey:Ljava/security/interfaces/ECPublicKey;

    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/ECPublicKey;)V
    .locals 1
    .param p1, "publicKey"    # Ljava/security/interfaces/ECPublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->forECParameterSpec(Ljava/security/spec/ECParameterSpec;)Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;)V

    .line 77
    iput-object p1, p0, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->publicKey:Ljava/security/interfaces/ECPublicKey;

    .line 78
    return-void
.end method

.method private generateEphemeralKeyPair(Ljava/security/spec/ECParameterSpec;)Ljava/security/KeyPair;
    .locals 6
    .param p1, "ecParameterSpec"    # Ljava/security/spec/ECParameterSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v2

    .line 170
    .local v2, "keProvider":Ljava/security/Provider;
    if-eqz v2, :cond_0

    .line 171
    :try_start_0
    const-string v3, "EC"

    invoke-static {v3, v2}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    .line 176
    .local v1, "generator":Ljava/security/KeyPairGenerator;
    :goto_0
    invoke-virtual {v1, p1}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 177
    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v3

    return-object v3

    .line 173
    .end local v1    # "generator":Ljava/security/KeyPairGenerator;
    :cond_0
    const-string v3, "EC"

    invoke-static {v3}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .restart local v1    # "generator":Ljava/security/KeyPairGenerator;
    goto :goto_0

    .line 178
    .end local v1    # "generator":Ljava/security/KeyPairGenerator;
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Ljava/security/GeneralSecurityException;
    :goto_1
    new-instance v3, Lcom/nimbusds/jose/JOSEException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t generate ephemeral EC key pair: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 178
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public encrypt(Lcom/nimbusds/jose/JWEHeader;[B)Lcom/nimbusds/jose/JWECryptoParts;
    .locals 15
    .param p1, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p2, "clearText"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-virtual/range {p1 .. p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v2

    .line 113
    .local v2, "alg":Lcom/nimbusds/jose/JWEAlgorithm;
    invoke-static {v2}, Lcom/nimbusds/jose/crypto/ECDH;->resolveAlgorithmMode(Lcom/nimbusds/jose/JWEAlgorithm;)Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;

    move-result-object v3

    .line 114
    .local v3, "algMode":Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;
    invoke-virtual/range {p1 .. p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v5

    .line 117
    .local v5, "enc":Lcom/nimbusds/jose/EncryptionMethod;
    iget-object v12, p0, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->publicKey:Ljava/security/interfaces/ECPublicKey;

    invoke-interface {v12}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->generateEphemeralKeyPair(Ljava/security/spec/ECParameterSpec;)Ljava/security/KeyPair;

    move-result-object v7

    .line 118
    .local v7, "ephemeralKeyPair":Ljava/security/KeyPair;
    invoke-virtual {v7}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v9

    check-cast v9, Ljava/security/interfaces/ECPublicKey;

    .line 119
    .local v9, "ephemeralPublicKey":Ljava/security/interfaces/ECPublicKey;
    invoke-virtual {v7}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v8

    check-cast v8, Ljava/security/interfaces/ECPrivateKey;

    .line 123
    .local v8, "ephemeralPrivateKey":Ljava/security/interfaces/ECPrivateKey;
    iget-object v12, p0, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->publicKey:Ljava/security/interfaces/ECPublicKey;

    .line 125
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v13

    invoke-virtual {v13}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v13

    .line 122
    invoke-static {v12, v8, v13}, Lcom/nimbusds/jose/crypto/ECDH;->deriveSharedSecret(Ljava/security/interfaces/ECPublicKey;Ljava/security/interfaces/ECPrivateKey;Ljava/security/Provider;)Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 128
    .local v1, "Z":Ljavax/crypto/SecretKey;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getConcatKDF()Lcom/nimbusds/jose/crypto/ConcatKDF;

    move-result-object v12

    invoke-virtual {v12}, Lcom/nimbusds/jose/crypto/ConcatKDF;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v12

    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v13

    invoke-virtual {v13}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getMACProvider()Ljava/security/Provider;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/nimbusds/jose/jca/JCAContext;->setProvider(Ljava/security/Provider;)V

    .line 129
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getConcatKDF()Lcom/nimbusds/jose/crypto/ConcatKDF;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v1, v12}, Lcom/nimbusds/jose/crypto/ECDH;->deriveSharedKey(Lcom/nimbusds/jose/JWEHeader;Ljavax/crypto/SecretKey;Lcom/nimbusds/jose/crypto/ConcatKDF;)Ljavax/crypto/SecretKey;

    move-result-object v10

    .line 134
    .local v10, "sharedKey":Ljavax/crypto/SecretKey;
    sget-object v12, Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;->DIRECT:Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;

    invoke-virtual {v3, v12}, Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 135
    move-object v4, v10

    .line 136
    .local v4, "cek":Ljavax/crypto/SecretKey;
    const/4 v6, 0x0

    .line 145
    .local v6, "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    :goto_0
    new-instance v12, Lcom/nimbusds/jose/JWEHeader$Builder;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/nimbusds/jose/JWEHeader$Builder;-><init>(Lcom/nimbusds/jose/JWEHeader;)V

    .line 146
    new-instance v13, Lcom/nimbusds/jose/jwk/ECKey$Builder;

    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v14

    invoke-direct {v13, v14, v9}, Lcom/nimbusds/jose/jwk/ECKey$Builder;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Ljava/security/interfaces/ECPublicKey;)V

    invoke-virtual {v13}, Lcom/nimbusds/jose/jwk/ECKey$Builder;->build()Lcom/nimbusds/jose/jwk/ECKey;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/nimbusds/jose/JWEHeader$Builder;->ephemeralPublicKey(Lcom/nimbusds/jose/jwk/ECKey;)Lcom/nimbusds/jose/JWEHeader$Builder;

    move-result-object v12

    .line 147
    invoke-virtual {v12}, Lcom/nimbusds/jose/JWEHeader$Builder;->build()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v11

    .line 149
    .local v11, "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-static {v11, v0, v4, v6, v12}, Lcom/nimbusds/jose/crypto/ContentCryptoProvider;->encrypt(Lcom/nimbusds/jose/JWEHeader;[BLjavax/crypto/SecretKey;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jca/JWEJCAContext;)Lcom/nimbusds/jose/JWECryptoParts;

    move-result-object v12

    return-object v12

    .line 137
    .end local v4    # "cek":Ljavax/crypto/SecretKey;
    .end local v6    # "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    .end local v11    # "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    :cond_0
    sget-object v12, Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;->KW:Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;

    invoke-virtual {v3, v12}, Lcom/nimbusds/jose/crypto/ECDH$AlgorithmMode;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v12

    invoke-virtual {v12}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v12

    invoke-static {v5, v12}, Lcom/nimbusds/jose/crypto/ContentCryptoProvider;->generateCEK(Lcom/nimbusds/jose/EncryptionMethod;Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v4

    .line 139
    .restart local v4    # "cek":Ljavax/crypto/SecretKey;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v12

    invoke-virtual {v12}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v12

    invoke-static {v4, v10, v12}, Lcom/nimbusds/jose/crypto/AESKW;->wrapCEK(Ljavax/crypto/SecretKey;Ljavax/crypto/SecretKey;Ljava/security/Provider;)[B

    move-result-object v12

    invoke-static {v12}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    .line 140
    .restart local v6    # "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    goto :goto_0

    .line 141
    .end local v4    # "cek":Ljavax/crypto/SecretKey;
    .end local v6    # "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    :cond_1
    new-instance v12, Lcom/nimbusds/jose/JOSEException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Unexpected JWE ECDH algorithm mode: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v12
.end method

.method public bridge synthetic getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;->getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v0

    return-object v0
.end method

.method public getPublicKey()Ljava/security/interfaces/ECPublicKey;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/ECDHEncrypter;->publicKey:Ljava/security/interfaces/ECPublicKey;

    return-object v0
.end method

.method public bridge synthetic supportedEllipticCurves()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;->supportedEllipticCurves()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic supportedEncryptionMethods()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;->supportedEncryptionMethods()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic supportedJWEAlgorithms()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDHCryptoProvider;->supportedJWEAlgorithms()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
