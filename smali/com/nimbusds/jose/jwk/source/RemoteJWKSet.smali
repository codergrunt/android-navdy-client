.class public Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;
.super Ljava/lang/Object;
.source "RemoteJWKSet.java"

# interfaces
.implements Lcom/nimbusds/jose/jwk/source/JWKSource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Lcom/nimbusds/jose/proc/SecurityContext;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/nimbusds/jose/jwk/source/JWKSource",
        "<TC;>;"
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final DEFAULT_HTTP_CONNECT_TIMEOUT:I = 0xfa

.field public static final DEFAULT_HTTP_READ_TIMEOUT:I = 0xfa

.field public static final DEFAULT_HTTP_SIZE_LIMIT:I = 0xc800


# instance fields
.field private final cachedJWKSet:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lcom/nimbusds/jose/jwk/JWKSet;",
            ">;"
        }
    .end annotation
.end field

.field private final jwkSetRetriever:Lcom/nimbusds/jose/util/ResourceRetriever;

.field private final jwkSetURL:Ljava/net/URL;


# direct methods
.method public constructor <init>(Ljava/net/URL;)V
    .locals 1
    .param p1, "jwkSetURL"    # Ljava/net/URL;

    .prologue
    .line 82
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;-><init>(Ljava/net/URL;Lcom/nimbusds/jose/util/ResourceRetriever;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lcom/nimbusds/jose/util/ResourceRetriever;)V
    .locals 4
    .param p1, "jwkSetURL"    # Ljava/net/URL;
    .param p2, "resourceRetriever"    # Lcom/nimbusds/jose/util/ResourceRetriever;

    .prologue
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    const/16 v3, 0xfa

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->cachedJWKSet:Ljava/util/concurrent/atomic/AtomicReference;

    .line 99
    if-nez p1, :cond_0

    .line 100
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The JWK set URL must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_0
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetURL:Ljava/net/URL;

    .line 104
    if-eqz p2, :cond_1

    .line 105
    iput-object p2, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetRetriever:Lcom/nimbusds/jose/util/ResourceRetriever;

    .line 110
    :goto_0
    new-instance v0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet$1;

    invoke-direct {v0, p0}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet$1;-><init>(Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;)V

    .line 115
    .local v0, "t":Ljava/lang/Thread;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initial-jwk-set-retriever["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 117
    return-void

    .line 107
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_1
    new-instance v1, Lcom/nimbusds/jose/util/DefaultResourceRetriever;

    const v2, 0xc800

    invoke-direct {v1, v3, v3, v2}, Lcom/nimbusds/jose/util/DefaultResourceRetriever;-><init>(III)V

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetRetriever:Lcom/nimbusds/jose/util/ResourceRetriever;

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;)Lcom/nimbusds/jose/jwk/JWKSet;
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->updateJWKSetFromURL()Lcom/nimbusds/jose/jwk/JWKSet;

    move-result-object v0

    return-object v0
.end method

.method protected static getFirstSpecifiedKeyID(Lcom/nimbusds/jose/jwk/JWKMatcher;)Ljava/lang/String;
    .locals 5
    .param p0, "jwkMatcher"    # Lcom/nimbusds/jose/jwk/JWKMatcher;

    .prologue
    const/4 v2, 0x0

    .line 182
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/JWKMatcher;->getKeyIDs()Ljava/util/Set;

    move-result-object v1

    .line 184
    .local v1, "keyIDs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v0, v2

    .line 193
    :goto_0
    return-object v0

    .line 188
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    move-object v0, v2

    .line 193
    goto :goto_0

    .line 188
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 189
    .local v0, "id":Ljava/lang/String;
    if-eqz v0, :cond_2

    goto :goto_0
.end method

.method private updateJWKSetFromURL()Lcom/nimbusds/jose/jwk/JWKSet;
    .locals 4

    .prologue
    .line 128
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    :try_start_0
    iget-object v2, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetRetriever:Lcom/nimbusds/jose/util/ResourceRetriever;

    iget-object v3, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetURL:Ljava/net/URL;

    invoke-interface {v2, v3}, Lcom/nimbusds/jose/util/ResourceRetriever;->retrieveResource(Ljava/net/URL;)Lcom/nimbusds/jose/util/Resource;

    move-result-object v1

    .line 129
    .local v1, "res":Lcom/nimbusds/jose/util/Resource;
    invoke-virtual {v1}, Lcom/nimbusds/jose/util/Resource;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/nimbusds/jose/jwk/JWKSet;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/JWKSet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133
    .local v0, "jwkSet":Lcom/nimbusds/jose/jwk/JWKSet;
    iget-object v2, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->cachedJWKSet:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 134
    .end local v0    # "jwkSet":Lcom/nimbusds/jose/jwk/JWKSet;
    .end local v1    # "res":Lcom/nimbusds/jose/util/Resource;
    :goto_0
    return-object v0

    .line 131
    :catch_0
    move-exception v2

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method


# virtual methods
.method public get(Lcom/nimbusds/jose/jwk/JWKSelector;Lcom/nimbusds/jose/proc/SecurityContext;)Ljava/util/List;
    .locals 4
    .param p1, "jwkSelector"    # Lcom/nimbusds/jose/jwk/JWKSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/jwk/JWKSelector;",
            "TC;)",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/jwk/JWK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->getJWKSet()Lcom/nimbusds/jose/jwk/JWKSet;

    move-result-object v0

    .line 205
    .local v0, "jwkSet":Lcom/nimbusds/jose/jwk/JWKSet;
    if-nez v0, :cond_1

    .line 207
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 234
    :cond_0
    :goto_0
    return-object v1

    .line 209
    :cond_1
    invoke-virtual {p1, v0}, Lcom/nimbusds/jose/jwk/JWKSelector;->select(Lcom/nimbusds/jose/jwk/JWKSet;)Ljava/util/List;

    move-result-object v1

    .line 211
    .local v1, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/JWK;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 217
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/JWKSelector;->getMatcher()Lcom/nimbusds/jose/jwk/JWKMatcher;

    move-result-object v3

    invoke-static {v3}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->getFirstSpecifiedKeyID(Lcom/nimbusds/jose/jwk/JWKMatcher;)Ljava/lang/String;

    move-result-object v2

    .line 218
    .local v2, "soughtKeyID":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 222
    invoke-virtual {v0, v2}, Lcom/nimbusds/jose/jwk/JWKSet;->getKeyByKeyId(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/JWK;

    move-result-object v3

    if-nez v3, :cond_0

    .line 228
    invoke-direct {p0}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->updateJWKSetFromURL()Lcom/nimbusds/jose/jwk/JWKSet;

    move-result-object v0

    .line 229
    if-nez v0, :cond_2

    .line 231
    const/4 v1, 0x0

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {p1, v0}, Lcom/nimbusds/jose/jwk/JWKSelector;->select(Lcom/nimbusds/jose/jwk/JWKSet;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public getJWKSet()Lcom/nimbusds/jose/jwk/JWKSet;
    .locals 2

    .prologue
    .line 165
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    iget-object v1, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->cachedJWKSet:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nimbusds/jose/jwk/JWKSet;

    .line 166
    .local v0, "jwkSet":Lcom/nimbusds/jose/jwk/JWKSet;
    if-eqz v0, :cond_0

    .line 169
    .end local v0    # "jwkSet":Lcom/nimbusds/jose/jwk/JWKSet;
    :goto_0
    return-object v0

    .restart local v0    # "jwkSet":Lcom/nimbusds/jose/jwk/JWKSet;
    :cond_0
    invoke-direct {p0}, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->updateJWKSetFromURL()Lcom/nimbusds/jose/jwk/JWKSet;

    move-result-object v0

    goto :goto_0
.end method

.method public getJWKSetURL()Ljava/net/URL;
    .locals 1

    .prologue
    .line 144
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetURL:Ljava/net/URL;

    return-object v0
.end method

.method public getResourceRetriever()Lcom/nimbusds/jose/util/ResourceRetriever;
    .locals 1

    .prologue
    .line 155
    .local p0, "this":Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;, "Lcom/nimbusds/jose/jwk/source/RemoteJWKSet<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/source/RemoteJWKSet;->jwkSetRetriever:Lcom/nimbusds/jose/util/ResourceRetriever;

    return-object v0
.end method
