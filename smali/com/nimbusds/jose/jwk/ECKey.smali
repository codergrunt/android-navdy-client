.class public final Lcom/nimbusds/jose/jwk/ECKey;
.super Lcom/nimbusds/jose/jwk/JWK;
.source "ECKey.java"

# interfaces
.implements Lcom/nimbusds/jose/jwk/AssymetricJWK;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nimbusds/jose/jwk/ECKey$Builder;,
        Lcom/nimbusds/jose/jwk/ECKey$Curve;
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/Immutable;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

.field private final d:Lcom/nimbusds/jose/util/Base64URL;

.field private final x:Lcom/nimbusds/jose/util/Base64URL;

.field private final y:Lcom/nimbusds/jose/util/Base64URL;


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 9
    .param p1, "crv"    # Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .param p2, "x"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "y"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p6, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p7, "kid"    # Ljava/lang/String;
    .param p8, "x5u"    # Ljava/net/URI;
    .param p9, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/jwk/ECKey$Curve;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 724
    .local p5, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p10, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    sget-object v1, Lcom/nimbusds/jose/jwk/KeyType;->EC:Lcom/nimbusds/jose/jwk/KeyType;

    move-object v0, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, Lcom/nimbusds/jose/jwk/JWK;-><init>(Lcom/nimbusds/jose/jwk/KeyType;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 726
    if-nez p1, :cond_0

    .line 727
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The curve must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 730
    :cond_0
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    .line 732
    if-nez p2, :cond_1

    .line 733
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'x\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 736
    :cond_1
    iput-object p2, p0, Lcom/nimbusds/jose/jwk/ECKey;->x:Lcom/nimbusds/jose/util/Base64URL;

    .line 738
    if-nez p3, :cond_2

    .line 739
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'y\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 742
    :cond_2
    iput-object p3, p0, Lcom/nimbusds/jose/jwk/ECKey;->y:Lcom/nimbusds/jose/util/Base64URL;

    .line 744
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    .line 745
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 9
    .param p1, "crv"    # Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .param p2, "x"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "y"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "d"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p5, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p7, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p8, "kid"    # Ljava/lang/String;
    .param p9, "x5u"    # Ljava/net/URI;
    .param p10, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/jwk/ECKey$Curve;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 781
    .local p6, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p11, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    sget-object v1, Lcom/nimbusds/jose/jwk/KeyType;->EC:Lcom/nimbusds/jose/jwk/KeyType;

    move-object v0, p0

    move-object v2, p5

    move-object v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    invoke-direct/range {v0 .. v8}, Lcom/nimbusds/jose/jwk/JWK;-><init>(Lcom/nimbusds/jose/jwk/KeyType;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 783
    if-nez p1, :cond_0

    .line 784
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The curve must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 787
    :cond_0
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    .line 789
    if-nez p2, :cond_1

    .line 790
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'x\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 793
    :cond_1
    iput-object p2, p0, Lcom/nimbusds/jose/jwk/ECKey;->x:Lcom/nimbusds/jose/util/Base64URL;

    .line 795
    if-nez p3, :cond_2

    .line 796
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'y\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 799
    :cond_2
    iput-object p3, p0, Lcom/nimbusds/jose/jwk/ECKey;->y:Lcom/nimbusds/jose/util/Base64URL;

    .line 801
    if-nez p4, :cond_3

    .line 802
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'d\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 805
    :cond_3
    iput-object p4, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    .line 806
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Ljava/security/interfaces/ECPublicKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 11
    .param p1, "crv"    # Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .param p2, "pub"    # Ljava/security/interfaces/ECPublicKey;
    .param p3, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p5, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p6, "kid"    # Ljava/lang/String;
    .param p7, "x5u"    # Ljava/net/URI;
    .param p8, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/jwk/ECKey$Curve;",
            "Ljava/security/interfaces/ECPublicKey;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 831
    .line 832
    .local p4, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p9, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    .line 833
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    .line 835
    invoke-direct/range {v0 .. v10}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 836
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Ljava/security/interfaces/ECPublicKey;Ljava/security/interfaces/ECPrivateKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 12
    .param p1, "crv"    # Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .param p2, "pub"    # Ljava/security/interfaces/ECPublicKey;
    .param p3, "priv"    # Ljava/security/interfaces/ECPrivateKey;
    .param p4, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p6, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p7, "kid"    # Ljava/lang/String;
    .param p8, "x5u"    # Ljava/net/URI;
    .param p9, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/jwk/ECKey$Curve;",
            "Ljava/security/interfaces/ECPublicKey;",
            "Ljava/security/interfaces/ECPrivateKey;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 865
    .line 866
    .local p5, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p10, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    .line 867
    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    invoke-interface {p2}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 868
    invoke-interface {p3}, Ljava/security/interfaces/ECPrivateKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    invoke-interface {p3}, Ljava/security/interfaces/ECPrivateKey;->getS()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/nimbusds/jose/jwk/ECKey;->encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    .line 870
    invoke-direct/range {v0 .. v11}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 871
    return-void
.end method

.method public static encodeCoordinate(ILjava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;
    .locals 6
    .param p0, "fieldSize"    # I
    .param p1, "coordinate"    # Ljava/math/BigInteger;

    .prologue
    .line 653
    invoke-static {p1}, Lcom/nimbusds/jose/util/BigIntegerUtils;->toBytesUnsigned(Ljava/math/BigInteger;)[B

    move-result-object v2

    .line 655
    .local v2, "unpadded":[B
    add-int/lit8 v3, p0, 0x7

    div-int/lit8 v0, v3, 0x8

    .line 657
    .local v0, "bytesToOutput":I
    array-length v3, v2

    if-lt v3, v0, :cond_0

    .line 660
    invoke-static {v2}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 667
    :goto_0
    return-object v3

    .line 663
    :cond_0
    new-array v1, v0, [B

    .line 665
    .local v1, "padded":[B
    const/4 v3, 0x0

    array-length v4, v2

    sub-int v4, v0, v4

    array-length v5, v2

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 667
    invoke-static {v1}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    goto :goto_0
.end method

.method public static parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/ECKey;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1178
    invoke-static {p0}, Lcom/nimbusds/jose/util/JSONObjectUtils;->parse(Ljava/lang/String;)Lnet/minidev/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/jwk/ECKey;->parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/ECKey;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/ECKey;
    .locals 15
    .param p0, "jsonObject"    # Lnet/minidev/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x0

    .line 1198
    const-string v0, "crv"

    invoke-static {p0, v0}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v1

    .line 1199
    .local v1, "crv":Lcom/nimbusds/jose/jwk/ECKey$Curve;
    new-instance v2, Lcom/nimbusds/jose/util/Base64URL;

    const-string v0, "x"

    invoke-static {p0, v0}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1200
    .local v2, "x":Lcom/nimbusds/jose/util/Base64URL;
    new-instance v3, Lcom/nimbusds/jose/util/Base64URL;

    const-string v0, "y"

    invoke-static {p0, v0}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1203
    .local v3, "y":Lcom/nimbusds/jose/util/Base64URL;
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyType(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/KeyType;

    move-result-object v13

    .line 1205
    .local v13, "kty":Lcom/nimbusds/jose/jwk/KeyType;
    sget-object v0, Lcom/nimbusds/jose/jwk/KeyType;->EC:Lcom/nimbusds/jose/jwk/KeyType;

    if-eq v13, v0, :cond_0

    .line 1206
    new-instance v0, Ljava/text/ParseException;

    const-string v5, "The key type \"kty\" must be EC"

    invoke-direct {v0, v5, v14}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1210
    :cond_0
    const/4 v4, 0x0

    .line 1211
    .local v4, "d":Lcom/nimbusds/jose/util/Base64URL;
    const-string v0, "d"

    invoke-virtual {p0, v0}, Lnet/minidev/json/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1212
    new-instance v4, Lcom/nimbusds/jose/util/Base64URL;

    .end local v4    # "d":Lcom/nimbusds/jose/util/Base64URL;
    const-string v0, "d"

    invoke-static {p0, v0}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1217
    .restart local v4    # "d":Lcom/nimbusds/jose/util/Base64URL;
    :cond_1
    if-nez v4, :cond_2

    .line 1219
    :try_start_0
    new-instance v0, Lcom/nimbusds/jose/jwk/ECKey;

    .line 1220
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyUse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/KeyUse;

    move-result-object v4

    .line 1221
    .end local v4    # "d":Lcom/nimbusds/jose/util/Base64URL;
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyOperations(Lnet/minidev/json/JSONObject;)Ljava/util/Set;

    move-result-object v5

    .line 1222
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseAlgorithm(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/Algorithm;

    move-result-object v6

    .line 1223
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyID(Lnet/minidev/json/JSONObject;)Ljava/lang/String;

    move-result-object v7

    .line 1224
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertURL(Lnet/minidev/json/JSONObject;)Ljava/net/URI;

    move-result-object v8

    .line 1225
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertThumbprint(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v9

    .line 1226
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertChain(Lnet/minidev/json/JSONObject;)Ljava/util/List;

    move-result-object v10

    .line 1219
    invoke-direct/range {v0 .. v10}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1230
    :goto_0
    return-object v0

    .restart local v4    # "d":Lcom/nimbusds/jose/util/Base64URL;
    :cond_2
    new-instance v0, Lcom/nimbusds/jose/jwk/ECKey;

    .line 1231
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyUse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/KeyUse;

    move-result-object v5

    .line 1232
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyOperations(Lnet/minidev/json/JSONObject;)Ljava/util/Set;

    move-result-object v6

    .line 1233
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseAlgorithm(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/Algorithm;

    move-result-object v7

    .line 1234
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyID(Lnet/minidev/json/JSONObject;)Ljava/lang/String;

    move-result-object v8

    .line 1235
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertURL(Lnet/minidev/json/JSONObject;)Ljava/net/URI;

    move-result-object v9

    .line 1236
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertThumbprint(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v10

    .line 1237
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertChain(Lnet/minidev/json/JSONObject;)Ljava/util/List;

    move-result-object v11

    .line 1230
    invoke-direct/range {v0 .. v11}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1240
    .end local v4    # "d":Lcom/nimbusds/jose/util/Base64URL;
    :catch_0
    move-exception v12

    .line 1243
    .local v12, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v0, Ljava/text/ParseException;

    invoke-virtual {v12}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v14}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method public static bridge synthetic parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/JWK;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/ECKey;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/ECKey;

    move-result-object v0

    return-object v0
.end method

.method public static bridge synthetic parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/JWK;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/ECKey;->parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/ECKey;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    return-object v0
.end method

.method public getD()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getRequiredParams()Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 1115
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1116
    .local v0, "requiredParams":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "crv"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v2}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1117
    const-string v1, "kty"

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getKeyType()Lcom/nimbusds/jose/jwk/KeyType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nimbusds/jose/jwk/KeyType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1118
    const-string v1, "x"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->x:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119
    const-string v1, "y"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->y:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1120
    return-object v0
.end method

.method public getX()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 893
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey;->x:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getY()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey;->y:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public isPrivate()Z
    .locals 1

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toECPrivateKey()Ljava/security/interfaces/ECPrivateKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1001
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/jwk/ECKey;->toECPrivateKey(Ljava/security/Provider;)Ljava/security/interfaces/ECPrivateKey;

    move-result-object v0

    return-object v0
.end method

.method public toECPrivateKey(Ljava/security/Provider;)Ljava/security/interfaces/ECPrivateKey;
    .locals 7
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1022
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    if-nez v4, :cond_0

    .line 1024
    const/4 v4, 0x0

    .line 1044
    :goto_0
    return-object v4

    .line 1027
    :cond_0
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v4}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->toECParameterSpec()Ljava/security/spec/ECParameterSpec;

    move-result-object v3

    .line 1029
    .local v3, "spec":Ljava/security/spec/ECParameterSpec;
    if-nez v3, :cond_1

    .line 1030
    new-instance v4, Lcom/nimbusds/jose/JOSEException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Couldn\'t get EC parameter spec for curve "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1033
    :cond_1
    new-instance v2, Ljava/security/spec/ECPrivateKeySpec;

    iget-object v4, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v4}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v2, v4, v3}, Ljava/security/spec/ECPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/security/spec/ECParameterSpec;)V

    .line 1038
    .local v2, "privateKeySpec":Ljava/security/spec/ECPrivateKeySpec;
    if-nez p1, :cond_2

    .line 1039
    :try_start_0
    const-string v4, "EC"

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 1044
    .local v1, "keyFactory":Ljava/security/KeyFactory;
    :goto_1
    invoke-virtual {v1, v2}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v4

    check-cast v4, Ljava/security/interfaces/ECPrivateKey;

    goto :goto_0

    .line 1041
    .end local v1    # "keyFactory":Ljava/security/KeyFactory;
    :cond_2
    const-string v4, "EC"

    invoke-static {v4, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .restart local v1    # "keyFactory":Ljava/security/KeyFactory;
    goto :goto_1

    .line 1046
    .end local v1    # "keyFactory":Ljava/security/KeyFactory;
    :catch_0
    move-exception v0

    .line 1048
    .local v0, "e":Ljava/security/GeneralSecurityException;
    :goto_2
    new-instance v4, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1046
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public toECPublicKey()Ljava/security/interfaces/ECPublicKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 938
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/jwk/ECKey;->toECPublicKey(Ljava/security/Provider;)Ljava/security/interfaces/ECPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public toECPublicKey(Ljava/security/Provider;)Ljava/security/interfaces/ECPublicKey;
    .locals 8
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 958
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v5}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->toECParameterSpec()Ljava/security/spec/ECParameterSpec;

    move-result-object v3

    .line 960
    .local v3, "spec":Ljava/security/spec/ECParameterSpec;
    if-nez v3, :cond_0

    .line 961
    new-instance v5, Lcom/nimbusds/jose/JOSEException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Couldn\'t get EC parameter spec for curve "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 964
    :cond_0
    new-instance v4, Ljava/security/spec/ECPoint;

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/ECKey;->x:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    iget-object v6, p0, Lcom/nimbusds/jose/jwk/ECKey;->y:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v6}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 966
    .local v4, "w":Ljava/security/spec/ECPoint;
    new-instance v2, Ljava/security/spec/ECPublicKeySpec;

    invoke-direct {v2, v4, v3}, Ljava/security/spec/ECPublicKeySpec;-><init>(Ljava/security/spec/ECPoint;Ljava/security/spec/ECParameterSpec;)V

    .line 971
    .local v2, "publicKeySpec":Ljava/security/spec/ECPublicKeySpec;
    if-nez p1, :cond_1

    .line 972
    :try_start_0
    const-string v5, "EC"

    invoke-static {v5}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 977
    .local v1, "keyFactory":Ljava/security/KeyFactory;
    :goto_0
    invoke-virtual {v1, v2}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v5

    check-cast v5, Ljava/security/interfaces/ECPublicKey;

    return-object v5

    .line 974
    .end local v1    # "keyFactory":Ljava/security/KeyFactory;
    :cond_1
    const-string v5, "EC"

    invoke-static {v5, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .restart local v1    # "keyFactory":Ljava/security/KeyFactory;
    goto :goto_0

    .line 979
    .end local v1    # "keyFactory":Ljava/security/KeyFactory;
    :catch_0
    move-exception v0

    .line 981
    .local v0, "e":Ljava/security/GeneralSecurityException;
    :goto_1
    new-instance v5, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 979
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public toJSONObject()Lnet/minidev/json/JSONObject;
    .locals 3

    .prologue
    .line 1149
    invoke-super {p0}, Lcom/nimbusds/jose/jwk/JWK;->toJSONObject()Lnet/minidev/json/JSONObject;

    move-result-object v0

    .line 1152
    .local v0, "o":Lnet/minidev/json/JSONObject;
    const-string v1, "crv"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->crv:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v2}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1153
    const-string v1, "x"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->x:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1154
    const-string v1, "y"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->y:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1156
    iget-object v1, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v1, :cond_0

    .line 1157
    const-string v1, "d"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/ECKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1160
    :cond_0
    return-object v0
.end method

.method public toKeyPair()Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1085
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/jwk/ECKey;->toKeyPair(Ljava/security/Provider;)Ljava/security/KeyPair;

    move-result-object v0

    return-object v0
.end method

.method public toKeyPair(Ljava/security/Provider;)Ljava/security/KeyPair;
    .locals 3
    .param p1, "provider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1107
    new-instance v0, Ljava/security/KeyPair;

    invoke-virtual {p0, p1}, Lcom/nimbusds/jose/jwk/ECKey;->toECPublicKey(Ljava/security/Provider;)Ljava/security/interfaces/ECPublicKey;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/nimbusds/jose/jwk/ECKey;->toECPrivateKey(Ljava/security/Provider;)Ljava/security/interfaces/ECPrivateKey;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    return-object v0
.end method

.method public toPrivateKey()Ljava/security/PrivateKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1065
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->toECPrivateKey()Ljava/security/interfaces/ECPrivateKey;

    move-result-object v0

    return-object v0
.end method

.method public toPublicJWK()Lcom/nimbusds/jose/jwk/ECKey;
    .locals 11

    .prologue
    .line 1140
    new-instance v0, Lcom/nimbusds/jose/jwk/ECKey;

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getX()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getY()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 1141
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getKeyUse()Lcom/nimbusds/jose/jwk/KeyUse;

    move-result-object v4

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getKeyOperations()Ljava/util/Set;

    move-result-object v5

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getAlgorithm()Lcom/nimbusds/jose/Algorithm;

    move-result-object v6

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getKeyID()Ljava/lang/String;

    move-result-object v7

    .line 1142
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getX509CertURL()Ljava/net/URI;

    move-result-object v8

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getX509CertThumbprint()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v9

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->getX509CertChain()Ljava/util/List;

    move-result-object v10

    .line 1140
    invoke-direct/range {v0 .. v10}, Lcom/nimbusds/jose/jwk/ECKey;-><init>(Lcom/nimbusds/jose/jwk/ECKey$Curve;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic toPublicJWK()Lcom/nimbusds/jose/jwk/JWK;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->toPublicJWK()Lcom/nimbusds/jose/jwk/ECKey;

    move-result-object v0

    return-object v0
.end method

.method public toPublicKey()Ljava/security/PublicKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1057
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/ECKey;->toECPublicKey()Ljava/security/interfaces/ECPublicKey;

    move-result-object v0

    return-object v0
.end method
